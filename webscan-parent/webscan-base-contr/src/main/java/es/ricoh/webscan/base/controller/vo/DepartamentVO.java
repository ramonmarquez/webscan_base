/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.DepartamentVO.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que representa una unidad orgánica a la que
 * pertenece o presta servicio una persona usuaria.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.base.controller.vo;

import java.io.Serializable;

/**
 * Objeto de la capa de presentación que representa una unidad orgánica a la que
 * pertenece o presta servicio una persona usuaria.
 * <p>
 * Clase DepartamentVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class DepartamentVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la unidad orgánica.
	 */
	private String id;

	/**
	 * Denominación social de la unidad orgánica.
	 */
	private String name;

	/**
	 * Constructor sin argumentos.
	 */
	public DepartamentVO() {
		super();
	}

	/**
	 * Obtiene el identificador de la unidad orgánica.
	 * 
	 * @return el identificador de la unidad orgánica.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la unidad orgánica.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la unidad orgánica.
	 */
	public void setId(String anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación de la unidad organizativa.
	 * 
	 * @return la denominación de la unidad organizativa.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación de la unidad organizativa.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación de la unidad organizativa.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

}
