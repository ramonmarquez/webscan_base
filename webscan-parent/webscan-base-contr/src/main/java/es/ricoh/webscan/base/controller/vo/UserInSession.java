/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.UserInSession.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que representa un usuario que ha iniciado sesión en el sistema.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.base.controller.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Objeto de la capa de presentación que representa un usuario que ha iniciado
 * sesión en el sistema.
 * <p>
 * Clase UserInSession.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UserInSession implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador interno del usuario.
	 */
	private Long id;

	/**
	 * Nombre de usuario.
	 */
	private String username;

	/**
	 * Nombre de pila del usuario.
	 */
	private String name;

	/**
	 * Primer apellido del usuario.
	 */
	private String surname;

	/**
	 * Segundo apellido del usuario.
	 */
	private String secondSurname;

	/**
	 * Documento de identificación del usuario.
	 */
	private String document;

	/**
	 * Dirección de correo electrónico del usuario.
	 */
	private String email;

	/**
	 * Indicador de inhabilitación o bloqueo del usuario.
	 */
	private Boolean locked = Boolean.FALSE;

	/**
	 * Relación de configuraciones de perfil de digitalización en unidades
	 * organizativas a las que pertence el usuario.
	 */
	private List<UserOrgUnitScanProfiles> orgUnitScanProfiles = new ArrayList<UserOrgUnitScanProfiles>();

	/**
	 * Constructor sin argumentos.
	 */
	public UserInSession() {
		super();
	}

	/**
	 * Obtiene el identificador interno del usuario.
	 * 
	 * @return el identificador interno del usuario.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador interno del usuario.
	 * 
	 * @param anId
	 *            Nuevo valor para el identificador interno del usuario.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el nombre de usuario del usuario.
	 * 
	 * @return el nombre de usuario del usuario.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece un nuevo valor para el nombre de usuario del usuario.
	 * 
	 * @param anUsername
	 *            Nuevo valor para el nombre de usuario del usuario.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene el nombre de pila del usuario.
	 * 
	 * @return el nombre de pila del usuario.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el nombre de pila del usuario.
	 * 
	 * @param aName
	 *            Nuevo valor para el nombre de pila del usuario.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el primer apellido del usuario.
	 * 
	 * @return el primer apellido del usuario.
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Establece un nuevo valor para el primer apellido del usuario.
	 * 
	 * @param aSurname
	 *            Nuevo valor para el primer apellido del usuario.
	 */
	public void setSurname(String aSurname) {
		this.surname = aSurname;
	}

	/**
	 * Obtiene el segundo apellido del usuario.
	 * 
	 * @return el segundo apellido del usuario.
	 */
	public String getSecondSurname() {
		return secondSurname;
	}

	/**
	 * Establece un nuevo valor para el segundo apellido del usuario.
	 * 
	 * @param aSecondSurname
	 *            Nuevo valor para el segundo apellido del usuario.
	 */
	public void setSecondSurname(String aSecondSurname) {
		this.secondSurname = aSecondSurname;
	}

	/**
	 * Obtiene el documento de identificación del usuario.
	 * 
	 * @return el documento de identificación del usuario.
	 */
	public String getDocument() {
		return document;
	}

	/**
	 * Establece un nuevo valor para el documento de identificación del usuario.
	 * 
	 * @param aDocument
	 *            Nuevo valor para el documento de identificación del usuario.
	 */
	public void setDocument(String aDocument) {
		this.document = aDocument;
	}

	/**
	 * Obtiene la dirección de correo electrónico del usuario.
	 * 
	 * @return la dirección de correo electrónico del usuario.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Establece un nuevo valor para la dirección de correo electrónico del
	 * usuario.
	 * 
	 * @param anEmail
	 *            Nuevo valor para la dirección de correo electrónico del
	 *            usuario.
	 */
	public void setEmail(String anEmail) {
		this.email = anEmail;
	}

	/**
	 * Obtiene el indicador de bloqueo o inhabilitación del usuario.
	 * 
	 * @return el indicador de bloqueo o inhabilitación del usuario.
	 */
	public Boolean getLocked() {
		return locked;
	}

	/**
	 * Establece un nuevo valor para el indicador de bloqueo o inhabilitación
	 * del usuario.
	 * 
	 * @param lockedParam
	 *            Nuevo valor para el indicador de bloqueo o inhabilitación del
	 *            usuario.
	 */
	public void setLocked(Boolean lockedParam) {
		this.locked = lockedParam;
	}

	/**
	 * Obtiene la relación de configuraciones de perfil de digitalización en
	 * unidades organizativas a las que pertence el usuario.
	 * 
	 * @return la relación de unidades organizativas a las que pertenece el
	 *         usuario.
	 */
	public List<UserOrgUnitScanProfiles> getOrgUnitScanProfiles() {
		return orgUnitScanProfiles;
	}

	/**
	 * Establece la nueva relación de configuraciones de perfil de
	 * digitalización en unidades organizativas a las que pertence el usuario.
	 * 
	 * @param anyOrgUnitScanProfiles
	 *            Nueva de configuraciones de perfil de digitalización en
	 *            unidades organizativas a las que pertence el usuario.
	 */
	public void setOrgUnitScanProfiles(List<UserOrgUnitScanProfiles> anyOrgUnitScanProfiles) {
		this.orgUnitScanProfiles.clear();
		if (anyOrgUnitScanProfiles != null && !anyOrgUnitScanProfiles.isEmpty()) {
			this.orgUnitScanProfiles.addAll(anyOrgUnitScanProfiles);
		}
	}

}
