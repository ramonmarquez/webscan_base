/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.base.WebscanBaseConstants.java.</p>
 * <b>Descripción:</b><p> Interfaz que agrupa constantes del componente que implementa la capa de control de la interfaz de usuario base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.base;

import java.text.SimpleDateFormat;

/**
 * Interfaz que agrupa constantes del componente que implementa la capa de
 * control de la interfaz de usuario base de Webscan.
 * <p>
 * Clase WebscanBaseConstants.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface WebscanBaseConstants {

	/**
	 * Nombre del parámetro de sesión que informa sobre la última vez que
	 * accedio al sistema un usuario.
	 */
	String USER_LAST_ACCESS_SESSION_ATT_NAME = "userLastAccess";

	/**
	 * Identificador del parámetro de petición web que agrupa los mensajes
	 * globales presentados en el sistema.
	 */
	String COMMON_MESSAGES_REQUEST_PARAM_NAME = "commonMessages";

	/**
	 * Parámetro que representa el mensaje de error por defecto a presentar en
	 * la capa de presentación del sistema.
	 */
	String DEFAULT_ERROR_MESSAGE = "default.error.message";

	/**
	 * Parámetro que representa el mensaje de correcto por defecto a presentar
	 * en la capa de presentación del sistema.
	 */
	String DEFAULT_RESULT_OK_VALUE = "default.message.ok";

	// Nombre de los parámetros de respuesta incluidos en las peticiones web
	/**
	 * Parámetro de una petición web que informa si se ha ejecutado la operación
	 * solicitada correctamente.
	 */
	String REQUEST_RESULT_ATT_NAME = "reqResult";

	/**
	 * Parámetro de una petición web que informa si se ha visualizado el mensaje
	 * asociado al parámetro de sesión reqResult.
	 */
	String REQUEST_RESULT_COUNTER_ATT_NAME = "reqResultCounter";

	/**
	 * Valor del parámetro de petición web 'reqResult' que informa que se ha
	 * realizado correctamente la operación solicitada.
	 */
	String REQUEST_RESULT_OK_VALUE = "ok";

	/**
	 * Valor del parámetro de petición web 'reqResult' que informa que se ha
	 * realizado con errores la operación solicitada.
	 */
	String REQUEST_RESULT_ERROR_VALUE = "error";

	/**
	 * Parámetro de una petición web que informa que se ha realizado
	 * correctamente la operación solicitada.
	 */
	String REQUEST_RESULT_MSG_ATT_NAME = "resultMsg";

	/**
	 * Prefijo de los parámetros que informan los módulos desplegados.
	 */
	String MODULE_HEADER_PROP_NAME = "module.";

	// Variables de sesion
	/**
	 * Parámetro que representa el nombre del atributo de sesión que almacenará
	 * la información del usuario que ha iniciado sesión en el sistema.
	 */
	String USER_SESSION_ATT_NAME = "user";

	/**
	 * Parámetro que representa el nombre del atributo de sesión que almacenará
	 * la información de la ultima consulta realizada sobre auditorias.
	 */

	String LIST_OP_EVENT_SESSION_ATT_NAME = "listOpEvent";

	/**
	 * Constante identificativa de la etapa de almacenaje o repository.
	 */
	String STORE_STAGE_NAME = "Repository";

	/**
	 * Denominación de la fase de digitalización.
	 */
	String SCAN_STAGE_NAME = "Scan";
	/**
	 * Atributo de sesión que informa si un usuario tiene reservas de trabajos
	 * de digitalización activas.
	 */
	String HAS_BOOKING_SESSION_ATT = "hasBooking";

	/**
	 * Atributo de sesión que aloja las unidades orgánicas a las que pertenece
	 * un usuario.
	 */
	String USER_DPTOS_SESSION_ATT = "userDptos";

	// Nombre de propiedades
	/**
	 * Parámetro que configura el código CATI del Sistema.
	 */
	String SYSTEM_CATI_CODE_PROP_NAME = "system.CATICode";

	/**
	 * Parámetro que configura juego de caracteres del Sistema.
	 */
	String SYSTEM_STR_CHARSET_PROP_NAME = "system.strings.charset";

	// Valores por defecto
	/**
	 * Juego de caracteres empleado por defecto.
	 */
	String SYSTEM_STR_CHARSET_DEFAULT_VALUE = "UTF-8";

	/**
	 * Formato de la fecha incluida en la cabecera SOAP de trazabilidad.
	 */
	SimpleDateFormat ISO_8601_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
}
