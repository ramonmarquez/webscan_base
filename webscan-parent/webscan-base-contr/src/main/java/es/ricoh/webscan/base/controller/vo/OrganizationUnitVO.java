/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.OrganizationUnitVO.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que representa una unidad organizativa.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.base.controller.vo;

import java.io.Serializable;

/**
 * Objeto de la capa de presentación que representa una unidad organizativa.
 * <p>
 * Clase OrganizationUnitVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class OrganizationUnitVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la unidad organizativa.
	 */
	private Long id;

	/**
	 * Identificador externo de la unidad organizativa.
	 */
	private String externalId;

	/**
	 * Denominación de la unidad organizativa..
	 */
	private String name;

	/**
	 * Identificador de la unidad organizativa padre.
	 */
	private Long parentOrgUnitId;

	/**
	 * Constructor sin argumentos.
	 */
	public OrganizationUnitVO() {
		super();
	}

	/**
	 * Obtiene el identificador de la unidad organizativa.
	 * 
	 * @return el identificador de la unidad organizativa.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la unidad organizativa.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la unidad organizativa.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el identificador externo de la unidad organizativa.
	 * 
	 * @return el identificador externo de la unidad organizativa.
	 */
	public String getExternalId() {
		return externalId;
	}

	/**
	 * Establece un nuevo valor para el identificador externo de la unidad
	 * organizativa.
	 * 
	 * @param anExternalId
	 *            nuevo valor para el identificador externo de la unidad
	 *            organizativa.
	 */
	public void setExternalId(String anExternalId) {
		this.externalId = anExternalId;
	}

	/**
	 * Obtiene la denominación de la unidad organizativa.
	 * 
	 * @return la denominación de la unidad organizativa.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación de la unidad organizativa.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación de la unidad organizativa.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el identificador de la unidad organizativa padre.
	 * 
	 * @return el identificador de la unidad organizativa padre.
	 */
	public Long getParentOrgUnitId() {
		return parentOrgUnitId;
	}

	/**
	 * Establece un nuevo identificador de unidad organizativa padre.
	 * 
	 * @param aParentOrgUnitId
	 *            identificador de la unidad organizativa padre.
	 */
	public void setParentOrgUnitId(Long aParentOrgUnitId) {
		this.parentOrgUnitId = aParentOrgUnitId;
	}

}
