/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.base.listener.CommonMessagesListener.java.</p>
 * <b>Descripción:</b><p> Clase responsable de mantener actualizada la variable de sesión
 * "commonMessages" (mensajes en vista principal del sistema), eliminando
 * aquellos mensajes que son de ámbito petición y ya han sido visualizados por
 * el usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.base.listener;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationListener;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.RequestHandledEvent;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.vo.ViewMessage;
import es.ricoh.webscan.base.controller.vo.WebScope;

/**
 * Clase responsable de mantener actualizada la variable de sesión
 * "commonMessages" (mensajes en vista principal del sistema), eliminando
 * aquellos mensajes que son de ámbito petición y ya han sido visualizados por
 * el usuario.
 * <p>
 * Clase CommonMessagesListener.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class CommonMessagesListener implements ApplicationListener<RequestHandledEvent> {

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onApplicationEvent(RequestHandledEvent event) {
		List<ViewMessage> commonMessages, newCommonMessages;

		// RequestContextListener or RequestContextFilter

		ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

		HttpSession session = attrs.getRequest().getSession(false);

		if (session != null && session.getId().equals(event.getSessionId())) {
			commonMessages = (List<ViewMessage>) session.getAttribute(WebscanBaseConstants.COMMON_MESSAGES_REQUEST_PARAM_NAME);
			newCommonMessages = new ArrayList<ViewMessage>();

			if (commonMessages != null) {
				// Se eliminan los mensajes cuyo ambito sea REQUEST y haya sido
				// ya visualizado

				for (ViewMessage msg: commonMessages) {
					if (WebScope.REQUEST.equals(msg.getMsgScope())) {
						if (!msg.getVisualized()) {
							msg.setVisualized(Boolean.TRUE);
							newCommonMessages.add(msg);
						}
					} else {
						if (!newCommonMessages.contains(msg)) {
							newCommonMessages.add(msg);
						}
					}
				}
			}

			session.setAttribute(WebscanBaseConstants.COMMON_MESSAGES_REQUEST_PARAM_NAME, newCommonMessages);
		}

	}

}
