/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>File:</b><p>es.ricoh.webscan.base.controller.ExceptionHandlingController.java.</p>
 * <b>Description:</b><p> .</p>
 * <b>Project:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.base.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>
 * Class ExceptionHandlingController.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@ControllerAdvice
public class ExceptionHandlingController {

	/**
	 * Constante que identifica la vista de default error.
	 */
	private static final String DEFAULT_ERROR_VIEW = "errorPage";
	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandlingController.class);

	// @RequestHandler methods

	// Exception handling methods
	/**
	 * Convert a predefined exception to an HTTP Status code.
	 */
	@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Data integrity violation") // 409
	@ExceptionHandler(DataIntegrityViolationException.class)
	public void conflict() {
		// Nothing to do
	}

	// Total control - setup a model and return the view name yourself. Or
	// consider subclassing ExceptionHandlerExceptionResolver (see below).
	/**
	 * Manejador de error.
	 * 
	 * @param req
	 *            Petición http.
	 * @param ex
	 *            Exception producir.
	 * @return Entidad asociada para mostrar el error en pantalla.
	 */
	@ExceptionHandler(Throwable.class)
	public ModelAndView handleError(HttpServletRequest req, Exception ex) {
		if (LOG.isErrorEnabled()) {
			LOG.error("Request: " + req.getRequestURL() + " raised " + ex);
		}

		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", ex);
		mav.addObject("url", req.getRequestURL());
		mav.setViewName(DEFAULT_ERROR_VIEW);
		return mav;
	}
}
