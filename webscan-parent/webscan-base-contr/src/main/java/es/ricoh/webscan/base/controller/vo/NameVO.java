/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.NameVO.java.</p>
* <b>Descripción:</b><p> Objeto que representa un identificador en notación JSON.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.base.controller.vo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Objeto que representa un nombre o denominación en notación JSON.
 * <p>
 * Clase NameVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name" })
public class NameVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nombre o denominación.
	 */
	@JsonProperty("name")
	private String name = "";

	/**
	 * Constructor sin argumentos.
	 */
	public NameVO() {
		super();
	}

	/**
	 * Obtiene el nombre o denominación.
	 * 
	 * @return el nombre o denominación.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece el nombre o denominación.
	 * 
	 * @param aName
	 *            nombre o denominación.
	 */
	public void setId(String aName) {
		this.name = aName;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String res = null;

		if (name != null && !name.isEmpty()) {
			res = "NameVO [name=" + name + "]";
		}

		return res;
	}

}
