/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.base.context.WebscanApplication.java.</p>
* <b>Descripción:</b><p> Clase responsable del registro de atributos de ámbito aplicación.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/

package es.ricoh.webscan.base.context;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.utilities.Utils;

/**
 * Clase responsable del registro de atributos de ámbito aplicación.
 * <p>
 * Clase .
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Component
public final class WebscanApplication {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WebscanApplication.class);

	/**
	 * Parámetro de configuración que parámetriza la clave del componente
	 * cliente de digitalización.
	 */
	private static final String SCAN_CLIENT_COMP_KEY_PARAM_NAME = "scanClientComponent.key";

	/**
	 * Contexto web a nivel de aplicación.
	 */
	@Autowired
	private ServletContext servletContext;

	/**
	 * Parámetros de configuración que informan sobre los módulos desplegados.
	 */
	@Autowired
	private Properties modulesConfParams;

	/**
	 * Utilidades y acceso a parámetros globales del Sistema.
	 */
	@Autowired
	private Utils webscanUtilParams;

	/**
	 * Relación de módulos webscan desplegados en la que se informa su versión.
	 */
	private Map<String, String> deployedModules = new HashMap<String, String>();

	/**
	 * Clave del componente cliente de digitalización.
	 */
	private String scanClientComponentKey = "";

	/**
	 * Constructor sin argumentos.
	 */
	public WebscanApplication() {
		super();
	}

	/**
	 * Recupera la relación de módulos Webscan desplegados y los almacena en
	 * variables de ámbito de aplicación.
	 */
	public void initDeployedModules() {
		String propName;

		if (deployedModules.isEmpty()) {
			LOG.debug("[WEBSCAN-BASE] Registrando módulos de la solución base desplegados...");

			for (Iterator<String> it = modulesConfParams.stringPropertyNames().iterator(); it.hasNext();) {
				propName = it.next();

				if (propName.indexOf(WebscanBaseConstants.MODULE_HEADER_PROP_NAME) >= 0) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("[WEBSCAN-BASE] Módulo " + propName.substring(WebscanBaseConstants.MODULE_HEADER_PROP_NAME.length()) + " de la solución base desplegados...");
					}
					servletContext.setAttribute(propName, modulesConfParams.getProperty(propName));
					deployedModules.put(propName, modulesConfParams.getProperty(propName));
				}
			}
			LOG.debug("[WEBSCAN-BASE] Se ha finalizado el registro de módulos de la solución base desplegados.");
		}
	}

	/**
	 * Metodo que inicializa el componente cliente de escaneo recuperando la
	 * clave.
	 * 
	 * @throws WebscanException
	 *             Exception producida al no poder recuperar la clave de
	 *             escaneo.
	 */
	public void initScanClientComponent() throws WebscanException {
		String excMsg;
		if (scanClientComponentKey.isEmpty()) {
			LOG.debug("[WEBSCAN-BASE] Recuperando la clave del componente cliente de digitalización ...");

			scanClientComponentKey = webscanUtilParams.getProperty(SCAN_CLIENT_COMP_KEY_PARAM_NAME);

			if (scanClientComponentKey == null || scanClientComponentKey.isEmpty()) {
				excMsg = "[WEBSCAN-BASE] No fue configurada la clave del componente cliente de configuración, parámetro " + SCAN_CLIENT_COMP_KEY_PARAM_NAME + ".";
				LOG.error(excMsg);
				throw new WebscanException(WebscanException.CODE_993, excMsg);
			}

			servletContext.setAttribute(SCAN_CLIENT_COMP_KEY_PARAM_NAME, scanClientComponentKey);
			LOG.debug("[WEBSCAN-BASE] Clave del componente cliente de digitalización recuperada y registrada.");
		}
	}

}
