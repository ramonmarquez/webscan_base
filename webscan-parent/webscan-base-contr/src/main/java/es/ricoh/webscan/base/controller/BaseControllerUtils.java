/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.base.controller.BaseControllerUtils.java.</p>
 * <b>Descripción:</b><p> Clase que agrupa funcionalidades comunes para ser empleadas por los
 * diferentes controladores implantados en el sistema.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.base.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.vo.DepartamentVO;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.UserOrgUnitScanProfiles;
import es.ricoh.webscan.base.controller.vo.ViewMessage;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;

/**
 * Clase que agrupa funcionalidades comunes para ser empleadas por los
 * diferentes controladores implantados en el sistema.
 * <p>
 * Clase BaseControllerUtils.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class BaseControllerUtils {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BaseControllerUtils.class);

	/**
	 * Obtiene y establece como atributo de una petición un mensaje de error
	 * genérico asociado a una excepción.
	 * 
	 * @param e
	 *            Excepción
	 * @param request
	 *            Petición web.
	 * @param messageSource
	 *            Clase que agrupa los mensajes paramterizados en el sistema.
	 * @param storeInCommonMessages
	 *            Indica si el mensaje de error es mostrado en la variable de
	 *            sesión de mensajes (true), o en la petición web (false).
	 */
	public static void setDefaultExceptionInRequest(Exception e, HttpServletRequest request, MessageSource messageSource, Boolean storeInCommonMessages) {
		String errorMessage;
		String[ ] args = new String[1];

		args[0] = e.getMessage();

		if (storeInCommonMessages) {
			addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		} else {
			errorMessage = messageSource.getMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, request.getLocale());
			request.setAttribute(WebscanBaseConstants.REQUEST_RESULT_MSG_ATT_NAME, errorMessage);
		}

		request.setAttribute(WebscanBaseConstants.REQUEST_RESULT_ATT_NAME, WebscanBaseConstants.REQUEST_RESULT_ERROR_VALUE);
	}

	/**
	 * Añade un nuevo mensaje a la variable de sesión "commonMessages".
	 * 
	 * @param msgPropName
	 *            identificador de la propiedad que aloja el texto del mensaje.
	 * @param msgArgs
	 *            Argumentos requeridos para completar el mensaje.
	 * @param msgLevel
	 *            categoría del mensaje: info, aviso o error.
	 * @param webScope
	 *            Ambito del mensaje, petición o sesión.
	 * @param request
	 *            Petición web.
	 */
	@SuppressWarnings("unchecked")
	public static void addCommonMessage(String msgPropName, String[ ] msgArgs, ViewMessageLevel msgLevel, WebScope webScope, HttpServletRequest request) {
		List<ViewMessage> commonMessages;
		ViewMessage msg;

		LOG.debug("[WEBSCAN-BASE-WEB] Añadiendo mensajes a la capa de presentación ...");
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-BASE-WEB] Obteniendo la variable de sesión " + WebscanBaseConstants.COMMON_MESSAGES_REQUEST_PARAM_NAME + " ...");
		}

		commonMessages = (List<ViewMessage>) request.getSession().getAttribute(WebscanBaseConstants.COMMON_MESSAGES_REQUEST_PARAM_NAME);
		if (commonMessages == null) {
			commonMessages = new ArrayList<ViewMessage>();
		}

		LOG.debug("[WEBSCAN-BASE-WEB] Constryendo el mensaje a añadir la capa de presentación ...");
		msg = new ViewMessage();
		if (msgLevel != null) {
			msg.setMsgLevel(msgLevel);
		}

		msg.setMsgText(msgPropName);
		msg.setMsgArgs(msgArgs);

		if (webScope != null) {
			msg.setMsgScope(webScope);
		}
		// para no tener mensajes duplicados
		if (!commonMessages.contains(msg)) {
			commonMessages.add(msg);
		}

		request.getSession().setAttribute(WebscanBaseConstants.COMMON_MESSAGES_REQUEST_PARAM_NAME, commonMessages);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-BASE-WEB] " + commonMessages.size() + " mensajes mostrados en la capa de presentación.");
		}
	}

	/**
	 * Elimina los mensajes que comience por untexto determinado.
	 * 
	 * @param msgPropName
	 *            identificador de la propiedad que aloja el texto del mensaje
	 *            por el que comienza el texto.
	 * @param request
	 *            Petición web.
	 */
	@SuppressWarnings("unchecked")
	public static void deleteCommonMessage(String msgPropName, HttpServletRequest request) {
		List<ViewMessage> commonMessages;
		List<ViewMessage> auxCommonMessages;

		LOG.debug("[WEBSCAN-BASE-WEB] Eliminando mensajes de la capa de presentación de ámbito sesión ...");
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-BASE-WEB] Obteniendo la variable de sesión " + WebscanBaseConstants.COMMON_MESSAGES_REQUEST_PARAM_NAME + " ...");
		}

		commonMessages = (List<ViewMessage>) request.getSession().getAttribute(WebscanBaseConstants.COMMON_MESSAGES_REQUEST_PARAM_NAME);
		if (commonMessages != null && !commonMessages.isEmpty()) {
			auxCommonMessages = new ArrayList<ViewMessage>();
			LOG.debug("[WEBSCAN-BASE-WEB] Buscando el mensaje a eliminar de la capa de presentación ...");

			for (ViewMessage msg: commonMessages) {
				if (!msg.getMsgText().equals(msgPropName) || WebScope.APPLICATION.equals(msg.getMsgScope())) {
					auxCommonMessages.add(msg);
				}
			}

			request.getSession().setAttribute(WebscanBaseConstants.COMMON_MESSAGES_REQUEST_PARAM_NAME, auxCommonMessages);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-BASE-WEB] " + auxCommonMessages.size() + " mensajes mostrados en la capa de presentación.");
			}
		}
	}

	/**
	 * Recupera el usuario que ha iniciado sesión en el sistema.
	 * 
	 * @param request
	 *            Petición web.
	 * @return el usuario que ha iniciado sesión en el sistema.
	 */
	public static UserInSession getUserInSession(HttpServletRequest request) {
		UserInSession res;

		// obtenemos la info para auditoria
		res = (UserInSession) request.getSession(false).getAttribute(WebscanBaseConstants.USER_SESSION_ATT_NAME);

		return res;
	}

	/**
	 * Obtiene la relación de todas las unidades orgánicas a las que pertenece o
	 * presta servicio el usuario.
	 * 
	 * @param userSession
	 *            usuario que ha iniciado sesión en el sistema.
	 * @return cadena que representa la lista de identificadores de unidades
	 *         orgánicas a las que pertenece o presta servicio el usuario,
	 *         separados por el carácter ','. Retorna null, si el usuario no
	 *         tiene asociada ninguna unidad orgánica.
	 */
	public static String getUserFunctionalOrgs(UserInSession userSession) {
		String res = null;
		StringBuilder stringBuilder = new StringBuilder();
		for (UserOrgUnitScanProfiles uousp: userSession.getOrgUnitScanProfiles()) {
			for (DepartamentVO dpto: uousp.getFunctionalOrgs()) {
				stringBuilder.append(dpto.getId()).append(",");
			}
		}
		if (stringBuilder.length() > 0) {
			// Se elimina la última coma
			res = stringBuilder.substring(0, stringBuilder.length() - 1);
		}

		return res;
	}

	/**
	 * Obtiene la relación de las unidades orgánicas a las que pertenece o
	 * presta servicio el usuario asociados a una unidad organizativa.
	 * 
	 * @param userSession
	 *            usuario que ha iniciado sesión en el sistema.
	 * @param orgUnitId
	 *            identificador de unidad organizativa.
	 * @return cadena que representa la lista de identificadores de unidades
	 *         orgánicas a las que pertenece o presta servicio el usuario,
	 *         separados por el carácter ','. Retorna null, si el usuario no
	 *         tiene asociada ninguna unidad orgánica.
	 */
	/*public static String getUserFunctionalOrgsByOrgUnit(UserInSession userSession, Long orgUnitId) {
		Boolean found = Boolean.FALSE;
		String res = null;
		StringBuilder stringBuilder = new StringBuilder();
		UserOrgUnitScanProfiles uousp;
	
		for (Iterator<UserOrgUnitScanProfiles> it = userSession.getOrgUnitScanProfiles().iterator(); !found && it.hasNext();) {
			uousp = it.next();
			if (uousp.getOrganizationUnit().getId().equals(orgUnitId)) {
				for (DepartamentVO dpto: uousp.getFunctionalOrgs()) {
					stringBuilder.append(dpto.getId()).append(",");
				}
				found = Boolean.TRUE;
			}
		}
		if (stringBuilder.length() > 0) {
			// Se elimina la última coma
			res = stringBuilder.substring(0, stringBuilder.length() - 1);
		}
	
		return res;
	}*/

	/**
	 * Obtiene la relación de las unidades orgánicas a las que pertenece o
	 * presta servicio el usuario asociados a una unidad organizativa.
	 * 
	 * @param userSession
	 *            usuario que ha iniciado sesión en el sistema.
	 * @param orgUnitId
	 *            identificador de unidad organizativa.
	 * @return lista de unidades orgánicas a las que pertenece o presta servicio
	 *         el usuario. Retorna lista vacia, si el usuario no tiene asociada
	 *         ninguna unidad orgánica.
	 */
	public static List<DepartamentVO> getUserFunctionalOrgsByOrgUnit(UserInSession userSession, Long orgUnitId) {
		Boolean found = Boolean.FALSE;
		List<DepartamentVO> res = new ArrayList<DepartamentVO>();
		UserOrgUnitScanProfiles uousp;

		for (Iterator<UserOrgUnitScanProfiles> it = userSession.getOrgUnitScanProfiles().iterator(); !found && it.hasNext();) {
			uousp = it.next();
			if (uousp.getOrganizationUnit().getId().equals(orgUnitId)) {
				res = uousp.getFunctionalOrgs();
				found = Boolean.TRUE;
			}
		}

		return res;
	}

}
