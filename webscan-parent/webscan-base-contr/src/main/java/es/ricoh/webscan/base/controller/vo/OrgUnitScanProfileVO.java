/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.fastscandoc.contr.controller.vo.OrgUnitScanProfileVO.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que agrupa la información de identificación
* sobre un perfil de digitalziación, incluyendo las denominaciones de la
* plantilla y colectivo que lo definen.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
* Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.base.controller.vo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ricoh.webscan.base.controller.vo.OrganizationUnitVO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;

/**
 * Objeto de la capa de presentación que agrupa la información de identificación
 * sobre un perfil de digitalziación, incluyendo las denominaciones de la
 * plantilla y colectivo que lo definen.
 * <p>
 * Clase OrgUnitScanProfileVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class OrgUnitScanProfileVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de perfil de digitalización.
	 */
	private Long scanProfileOrgUnitId;

	/**
	 * Colectivo de usuarios.
	 */
	private OrganizationUnitVO organizationUnit;

	/**
	 * Plantilla de digitalización.
	 */
	private ScanProfileDTO scanProfile;

	/**
	 * Definiciones de documentos activas para el perfil de digitalización.
	 */
	private List<Long> docSpecs = new ArrayList<Long>();

	/**
	 * Relación de unidades orgánicas a las que pertenece o presta servicio el
	 * usuario para un colectivo determinado.
	 */
	private List<String> departments = new ArrayList<String>();

	/**
	 * Constructor sin argumentos.
	 */
	public OrgUnitScanProfileVO() {
		super();
	}

	/**
	 * Obtiene el identificador de perfil de digitalización.
	 * 
	 * @return el identificador de perfil de digitalización.
	 */
	public Long getScanProfileOrgUnitId() {
		return scanProfileOrgUnitId;
	}

	/**
	 * Establece un nuevo valor para el identificador de perfil de
	 * digitalización.
	 * 
	 * @param aScanProfileOrgUnitId
	 *            nuevo valor para el identificador de perfil de digitalización.
	 */
	public void setScanProfileOrgUnitId(Long aScanProfileOrgUnitId) {
		this.scanProfileOrgUnitId = aScanProfileOrgUnitId;
	}

	/**
	 * Obtiene el colectivo de usuarios.
	 * 
	 * @return el colectivo de usuarios.
	 */
	public OrganizationUnitVO getOrganizationUnit() {
		return organizationUnit;
	}

	/**
	 * Establece un nuevo valor para el colectivo de usuarios.
	 * 
	 * @param aOrganizationUnit
	 *            nuevo valor para el colectivo de usuarios.
	 */
	public void setOrganizationUnit(OrganizationUnitVO aOrganizationUnit) {
		this.organizationUnit = aOrganizationUnit;
	}

	/**
	 * Obtiene la plantilla de de digitalización.
	 * 
	 * @return la plantilla de digitalización.
	 */
	public ScanProfileDTO getScanProfile() {
		return scanProfile;
	}

	/**
	 * Establece la plantilla de digitalización.
	 * 
	 * @param aScanProfile
	 *            nueva plantilla de digitalización.
	 */
	public void setScanProfile(ScanProfileDTO aScanProfiles) {
		this.scanProfile = aScanProfiles;
	}

	/**
	 * Obtiene las definiciones de documentos activas para el perfil de
	 * digitalización.
	 * 
	 * @return las definiciones de documentos activas para el perfil de
	 *         digitalización.
	 */
	public List<Long> getDocSpecs() {
		return docSpecs;
	}

	/**
	 * Establece las definiciones de documentos activas para el perfil de
	 * digitalización.
	 * 
	 * @param anyDocSpecs
	 *            definiciones de documentos activas para el perfil de
	 *            digitalización.
	 */
	public void setDocSpecs(List<Long> anyDocSpecs) {
		this.docSpecs.clear();

		if (anyDocSpecs != null && !anyDocSpecs.isEmpty()) {
			this.docSpecs.addAll(anyDocSpecs);
		}
	}

	/**
	 * Obtiene la relación de unidades orgánicas a las que pertenece o presta
	 * servicio el usuario para un colectivo determinado.
	 * 
	 * @return la relación de unidades orgánicas a las que pertenece o presta
	 *         servicio el usuario para un colectivo determinado.
	 */
	public List<String> getDepartments() {
		return departments;
	}

	/**
	 * Establece la relación de unidades orgánicas a las que pertenece o presta
	 * servicio el usuario para un colectivo determinado.
	 * 
	 * @param anyDepartments
	 *            relación de unidades orgánicas a las que pertenece o presta
	 *            servicio el usuario para un colectivo determinado.
	 */
	public void setDepartments(List<String> anyDepartments) {
		this.departments.clear();

		if (anyDepartments != null && !anyDepartments.isEmpty()) {
			this.departments.addAll(anyDepartments);
		}
	}

}
