/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2018 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.base.service.AsyncScanProcessDAOManager.java.</p>
* <b>Descripción:</b><p> Servicio de apoyo para las fases de ejecución asíncrona del proceso de
* digitalización de docmentos que agrupa la ejecución de setencias sobre el
* modelo de datos.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH España IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.base.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelConstants;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.OperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.model.service.AuditService;
import es.ricoh.webscan.plugins.Plugin;
import es.ricoh.webscan.plugins.PluginDataModelUtilities;
import es.ricoh.webscan.plugins.PluginException;
import es.ricoh.webscan.plugins.PluginProvider;

/**
 * Servicio de apoyo para las fases de ejecución asíncrona del proceso de
 * digitalización de docmentos que agrupa la ejecución de sentencias sobre el
 * modelo de datos.
 * <p>
 * Clase AsyncScanProcessDAOManager.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class AsyncScanProcessDAOManager {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AsyncScanProcessDAOManager.class);

	/**
	 * Prefijo de los parámetro de configuración que especifican la
	 * implementación de un plugin para la firma de documentos digitalizados.
	 */
	private static final String SIGNATURE_PREFIX_PROP_NAME = "signature";
	/**
	 * Prefijo de los parámetro de configuración que especifican la
	 * implementación de un plugin para el depósito de documentos digitalizados.
	 */
	private static final String DEPOSIT_PREFIX_PROP_NAME = "deposit";
	/**
	 * Prefijo de los parámetro de configuración que especifican la
	 * implementación de un plugin para la realización de operaciones antes de
	 * la firma de documentos digitalizados.
	 */
	private static final String PRESIGNATURE_PREFIX_PROP_NAME = "presign";

	/**
	 * Prefijo de los parámetro de configuración que especifican la
	 * implementación de un plugin para la realización de operaciones después de
	 * la firma de documentos digitalizados.
	 */
	private static final String POSTSIGNATURE_PREFIX_PROP_NAME = "postsign";

	/**
	 * Prefijo empleado en el bundle de mensajes multidioma para identificar las
	 * etiquetas que representan fases del proceso de digitalización.
	 */
	private static final String STAGE_MSG_PREFIX = "stage.";

	/**
	 * Objeto que representa el acceso a los servicio de ejecución de plugins.
	 */
	private PluginProvider pluginProvider;

	/**
	 * Objeto que representa el acceso a los servicio de auditoria.
	 */
	private AuditService auditService;

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos base de Webscan.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Utilidades de acceso al modelo de datos del Sistema para plugins.
	 */
	private PluginDataModelUtilities pluginDataModelUtilities;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Constructor sin argumentos.
	 */
	public AsyncScanProcessDAOManager() {
		super();
	}

	/**
	 * Recupera un documento digitalizado a partir de su identificador.
	 * 
	 * @param scanDocId
	 *            Identificador de documento digitalizado.
	 * @return El documento digitalizado solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la consulta a BBDD.
	 */
	public ScannedDocumentDTO getDocument(Long scanDocId) throws DAOException {
		return webscanModelManager.getDocument(scanDocId);
	}

	/**
	 * Obtiene la última de traza de ejecución asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scanDocId
	 *            Identificador de documento digitalizado.
	 * @return Ultima traza de ejecución asociada al proceso de digitalización
	 *         del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScannedDocLogDTO getLastScannedDocLogsByDocId(Long scanDocId) throws DAOException {
		return webscanModelManager.getLastScannedDocLogsByDocId(scanDocId);
	}

	/**
	 * Obtiene la primera traza de ejecución errónea asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return Primera traza de ejecución errónea asociada al proceso de
	 *         digitalización del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScannedDocLogDTO getFirstFailedScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		return webscanModelManager.getFirstFailedScannedDocLogsByDocId(scannedDocId);
	}

	/**
	 * Obtiene las trazas de ejecución, cuyas fases son reanudables, y han sido
	 * ejeuctadas en el proceso de digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return las trazas de ejecución, cuyas fases son reanudables, y han sido
	 *         ejeuctadas en el proceso de digitalización de un documento
	 *         digitalizado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public List<ScannedDocLogDTO> getRenewableAndPerformedScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		return webscanModelManager.getRenewableAndPerformedScannedDocLogsByDocId(scannedDocId);
	}

	/**
	 * Obtiene la traza de ejecución de una fase asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @param stageName
	 *            Nombre de la fase.
	 * @return Traza de ejecución de una fase asociada al proceso de
	 *         digitalización del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScannedDocLogDTO getScannedDocLogByStageNameAndDocId(Long scannedDocId, String stageName) throws DAOException {
		return webscanModelManager.getScannedDocLogByStageNameAndDocId(scannedDocId, stageName);
	}

	/**
	 * Recupera el perfil de digitalización en una unidad organizativa asociado
	 * al proceso de digitalización del documento.
	 * 
	 * @param scanDocId
	 *            Documento digitalizado.
	 * @return perfil de digitalización en una unidad organizativa asociado al
	 *         proceso de digitalización del documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public ScanProfileOrgUnitDTO getScanProfileOrgUnitByDoc(Long scanDocId) throws DAOException {
		return webscanModelManager.getScanProfileOrgUnitByDoc(scanDocId);
	}

	/**
	 * Obtiene un perfil de digitalización a partir de su identificador.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @return El perfil de digitalización solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil de digitalización, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScanProfileDTO getScanProfile(Long scanProfileId) throws DAOException {
		return webscanModelManager.getScanProfile(scanProfileId);
	}

	/**
	 * Obtiene la relación de fases reanudables de una definición de perfil de
	 * digitalización determinada.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de una definición de perfil de digitalización.
	 * @return relación de fases reanudables de una definición de perfil de
	 *         digitalización determinada ordenadas por orden de ejecución.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición de perfil de digitalización, o se produce algún
	 *             error en la consulta a BBDD.
	 */
	public List<ScanProfileSpecScanProcStageDTO> getRenewableScanProcStagesByScanProfileSpec(Long scanProfileSpecId) throws DAOException {
		return webscanModelManager.getRenewableScanProcStagesByScanProfileSpec(scanProfileSpecId);
	}

	/**
	 * Obtiene las especificaciones de plugins que implementan cada una de las
	 * fases configuradas para una especificación de perfiles de digitalización
	 * determinada, ordenadas por orden de ejecución.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de especificación de perfiles de digitalización.
	 * @return Mapa donde cada entrada es identificada por una configuración de
	 *         fase de proceso de digitalización, y cuyo valor es la relación de
	 *         especificaciones de plugin que implementan la fase que identifica
	 *         la entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe
	 *             especificación de perfiles de digitalización, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> getScanProfileSpecScanProcStagesByScanProfSpecId(Long scanProfileSpecId) throws DAOException {
		return webscanModelManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfileSpecId);
	}

	/**
	 * Obtiene una fase de proceso de digitalización a partir de su
	 * denominación.
	 * 
	 * @param execStageName
	 *            Página de resultados a devolver.
	 * @return fase de proceso de digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición de perfil de digitalización, o se produce algún
	 *             error en la consulta a BBDD.
	 */
	public ScanProcessStageDTO getScanProcStageByName(String execStageName) throws DAOException {
		return webscanModelManager.getScanProcStageByName(execStageName);
	}

	/**
	 * Obtiene la configuración de una fase para un proceso de digitalización.
	 * 
	 * @param mapStagePlugin
	 *            Relación de pares clave /valor, identificada por las fases que
	 *            conforman un proceso de digitalización, y cuyo valor se
	 *            corresponde con la lista de especificaciones de plugins que la
	 *            implementan.
	 * @param scanProfileSpecId
	 *            Identificador de proceso de digitalización.
	 * @param execStageName
	 *            Identificador de fase de proceso de digitalización.
	 * @return configuración de una fase para un proceso de digitalización. En
	 *         caso de no encontrarse, retorna null.
	 */
	public ScanProfileSpecScanProcStageDTO getScanProfileSpecScanProcStageByScanProfileSpecIdAndScanProcStageId(Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> mapStagePlugin, Long scanProfileSpecId, String execStageName) {
		return pluginDataModelUtilities.getScanProfileSpecScanProcStageByScanProfileSpecIdAndScanProcStageId(mapStagePlugin, scanProfileSpecId, execStageName);
	}

	/**
	 * Recupera el plugin establecido para una fase y configuración de perfil de
	 * digitalización en unidad organziativa determinados.
	 * 
	 * @param scanProfileId
	 *            identificador de perfil de digitalización.
	 * @param orgUnitId
	 *            identificador de unidad organizativa.
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @return La configuración de plugin establecida para una fase y
	 *         configuración de perfil de digitalización en unidad organziativa.
	 * @throws DAOException
	 *             Si los parámetros no son válidos, o se produce algún error en
	 *             la ejecución de la consulta de BBDD.
	 */
	public ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(Long scanProfileId, Long orgUnitId, Long stageCaptureId) throws DAOException {
		return webscanModelManager.getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(scanProfileId, orgUnitId, stageCaptureId);
	}

	/**
	 * Obtiene la especificación de plugin que implementa una determinada fase
	 * para un perfil de digitalización configurado en una unidad organizativa.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización
	 *            para unidad organizativa.
	 * @param execStageName
	 *            Denominación de una fase de proceso de digitalziación.
	 * @return La definición de plugin que implementa una determinada fase para
	 *         un perfil de digitalización configurado en una unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public PluginSpecificationDTO getPluginSpecByScanProfileOrgUnitAndStage(Long scanProfileOrgUnitId, String execStageName) throws DAOException {
		return webscanModelManager.getPluginSpecByScanProfileOrgUnitAndStage(scanProfileOrgUnitId, execStageName);
	}

	/**
	 * Ejecuta un determinado plugin para una fase del proceso de digtialziación
	 * de un documento.
	 * 
	 * @param doc
	 *            Documento digitalizado.
	 * @param scanProfileSpecScanProcStage
	 *            configuración de una fase en un proceso de digitalización.
	 * @param plugSpec
	 *            definición de plugin.
	 * @param pluginName
	 *            Denominación del plugin a ejecutar.
	 * @param renewableStageName
	 *            Denominación de una fase del proceso de digitalización.
	 * @param username
	 *            Nombre de usuario del usuario que ejecuta el plguin.
	 * @throws DAOException
	 *             Si se produce algún error al acceder al modelo de datos.
	 * @throws PluginException
	 *             Si se produce algún error en la ejecución del plugin.
	 */
	public void performStagePlugin(ScannedDocumentDTO doc, ScanProfileSpecScanProcStageDTO scanProfileSpecScanProcStage, PluginSpecificationDTO plugSpec, String pluginName, String renewableStageName, String username) throws DAOException, PluginException {
		long t0;
		long tf;
		ScannedDocLogDTO scannedDocLogDto;
		// Tx 1
		// creamos nueva traza, ya que entendemos que la operacion
		// previa se ha ejecutado correctamente
		scannedDocLogDto = new ScannedDocLogDTO();
		scannedDocLogDto.setStartDate(new Date());
		scannedDocLogDto.setScanProcessStageName(renewableStageName);
		scannedDocLogDto.setStatus(OperationStatus.IN_PROCESS);
		// creamos nueva traza
		/* Long scannedDocLogDtoId = */
		webscanModelManager.createScannedDocLog(scannedDocLogDto, doc.getId(), scanProfileSpecScanProcStage.getScanProcessStageId());
		// IN_PROCESS
		scannedDocLogDto = webscanModelManager.getLastScannedDocLogsByDocId(doc.getId());
		// Fin tx 1
		// Tx2
		Plugin plug = pluginProvider.getPlugin(pluginName);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[AsyncScanProcessDAO] Inicializamos plugin: {} - Fase: {}", pluginName, scanProfileSpecScanProcStage.getScanProcessStage());
		}
		// inicializamos el plugin
		t0 = System.currentTimeMillis();
		plug.initialize(scanProfileSpecScanProcStage.getScanProcessStageId(), doc.getId());
		LOG.debug("[AsyncScanProcessDAO] Ejecutamos plugin:{}", plugSpec.getName());
		// ejecutamos el plugin
		plug.performAction();
		tf = System.currentTimeMillis();
		LOG.info("[AsyncScanProcessDAO] Proceso de digitalización - Ejecución plugin {} realizada en {} ms.", pluginName, (tf - t0));
		// obtengo el último
		scannedDocLogDto.setEndDate(new Date());
		scannedDocLogDto.setStatus(OperationStatus.PERFORMED);
		webscanModelManager.closeScannedDocLog(scannedDocLogDto);
		// creamos auditoria
		createPhaseAuditLogs(doc, username, null, renewableStageName);
	}

	/**
	 * Actualiza, de forma transaccional, el registro de trazas de ejecución y
	 * auditoría tras fallar la ejecución de un plugin en una fase sobre un
	 * documento.
	 * 
	 * @param doc
	 *            Documento.
	 * @param scanProfileSpecScanProcStage
	 *            configuración de una fase en un proceso de digitalización.
	 * @param e
	 *            Excepción.
	 * @param renewableStageName
	 *            Denominación de la fase del proceso de digitalziación en la
	 *            que se produce error.
	 * @param username
	 *            Nombre de usuario del usuario que solicita el depósito del
	 *            documento.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	public void performUpdateDocErrorLogs(ScannedDocumentDTO doc, ScanProfileSpecScanProcStageDTO scanProfileSpecScanProcStage, Exception e, String renewableStageName, String username) throws DAOException {
		ScannedDocLogDTO scannedDocLogDto;

		scannedDocLogDto = new ScannedDocLogDTO();
		scannedDocLogDto.setStartDate(new Date());
		scannedDocLogDto.setScanProcessStageName(renewableStageName);
		scannedDocLogDto.setStatus(OperationStatus.IN_PROCESS);
		webscanModelManager.createScannedDocLog(scannedDocLogDto, doc.getId(), scanProfileSpecScanProcStage.getScanProcessStageId());
		scannedDocLogDto = webscanModelManager.getLastScannedDocLogsByDocId(doc.getId());
		// está en error
		scannedDocLogDto.setStatus(OperationStatus.FAILED);
		webscanModelManager.setStatusScannedDocLog(scannedDocLogDto);
		// auditoria
		createPhaseAuditLogs(doc, username, e, renewableStageName);
	}

	/**
	 * Cierra la traza de auditoría de un documento tras realizar su depósito,
	 * registrando adicionalmente el evento de la fase de depósito.
	 * 
	 * @param doc
	 *            documento que es depositado.
	 * @param username
	 *            usuario que efectúa el depósito.
	 * @param exc
	 *            Excepción.
	 * @param renewableStageName
	 *            Denominación de la etapa reanudada.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	private void createPhaseAuditLogs(ScannedDocumentDTO doc, String username, Exception exc, String renewableStageName) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation = auditService.getAuditOperation(doc.getAuditOpId());
		Date now = new Date();
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;
		String stageName = messages.getMessage(STAGE_MSG_PREFIX + renewableStageName, null, renewableStageName, Locale.getDefault());

		auditOperation.setLastUpdateDate(now);
		auditEvent = new AuditEventDTO();
		if (exc != null) {
			// Tamaño máximo del campo 256 caracteres
			additionalInfo = "Error al ejecutar la fase " + stageName + " del proceso de digitalización del documento " + doc.getId() + ". Excepción " + exc.getClass().getSimpleName() + ": " + exc.getMessage();
			auditEvent.setEventResult(AuditOpEventResult.ERROR);
		} else {
			additionalInfo = "Fase " + stageName + " del proceso de digitalización del documento " + doc.getId() + " realizada correctamente.";
			auditEvent.setEventResult(AuditOpEventResult.OK);
		}

		if (additionalInfo.length() > WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
			additionalInfo = additionalInfo.substring(0, WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
		}

		auditEvent.setAdditionalInfo(additionalInfo);
		auditEvent.setEventDate(now);
		auditEvent.setEventName(recoveryAuditEventNameByResumeStage(renewableStageName));
		auditEvent.setEventUser(username);
		auditEvents.add(auditEvent);

		auditService.updateAuditLogs(auditOperation, auditEvents);
	}

	/**
	 * Ejecuta un determinado plugin para una fase del proceso de digtialziación
	 * de un documento.
	 * 
	 * @param doc
	 *            Documento digitalizado.
	 * @param scanProfileSpecScanProcStage
	 *            configuración de una fase en un proceso de digitalización.
	 * @param scannedDocLogDto
	 *            Traza de ejecución de la fase a ejecutar. Puede ser null, si
	 *            no ha sido previamente registrada.
	 * @param plugSpec
	 *            definición de plugin.
	 * @param pluginName
	 *            Denominación del plugin a ejecutar.
	 * @param username
	 *            Nombre de usuario del usuario que ejecuta el plguin.
	 * @return estado final de la fase. Realizada si todo ha ido bien.
	 * @throws DAOException
	 *             Si se produce algún error al acceder al modelo de datos.
	 * @throws PluginException
	 *             Si se produce algún error en la ejecución del plugin.
	 */
	public OperationStatus performResumeStagePlugin(ScannedDocumentDTO doc, ScanProfileSpecScanProcStageDTO scanProfileSpecScanProcStage, ScannedDocLogDTO scannedDocLogDto, PluginSpecificationDTO plugSpec, String pluginName, String username) throws DAOException, PluginException {
		long t0;
		long tf;
		OperationStatus res = OperationStatus.FAILED;

		if (scannedDocLogDto == null) {
			scannedDocLogDto = new ScannedDocLogDTO();
			scannedDocLogDto.setStartDate(new Date());
			scannedDocLogDto.setScanProcessStageName(scanProfileSpecScanProcStage.getScanProcessStage());
			scannedDocLogDto.setStatus(OperationStatus.IN_PROCESS);
			// creamos nueva traza
			webscanModelManager.createScannedDocLog(scannedDocLogDto, doc.getId(), scanProfileSpecScanProcStage.getScanProcessStageId()); // IN_PROCESS
			scannedDocLogDto = webscanModelManager.getLastScannedDocLogsByDocId(doc.getId());
		} else {
			// está en error, obtenemos la traza asociada a la fase
			scannedDocLogDto.setStatus(OperationStatus.IN_PROCESS);
			webscanModelManager.setStatusScannedDocLog(scannedDocLogDto);
		}

		if (pluginName != null && !pluginName.trim().isEmpty()) {

			Plugin plug = pluginProvider.getPlugin(pluginName);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[AsyncScanProcessDAO] Inicializamos plugin:" + plugSpec.getName() + " - Fase:" + scanProfileSpecScanProcStage.getScanProcessStage());
			}
			// inicializamos el plugin
			t0 = System.currentTimeMillis();
			plug.initialize(scanProfileSpecScanProcStage.getScanProcessStageId(), doc.getId());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[AsyncScanProcessDAO] Ejecutamos plugin:" + plugSpec.getName());
			}
			// ejecutamos el plugin
			plug.performAction();
			tf = System.currentTimeMillis();
			LOG.info("[AsyncScanProcessDAO] Reanudación proceso de digitalización - Ejecución plugin {} realizada en {} ms.", pluginName, (tf - t0));
			// obtengo el último
			scannedDocLogDto.setEndDate(new Date());
			scannedDocLogDto.setStatus(OperationStatus.PERFORMED);
			webscanModelManager.closeScannedDocLog(scannedDocLogDto);
			createPhaseAuditLogs(doc, username, null, scanProfileSpecScanProcStage.getScanProcessStage());
			res = OperationStatus.PERFORMED;
		}

		return res;
	}

	/**
	 * Actualiza, de forma transaccional, el registro de trazas de ejecución y
	 * auditoría tras fallar la ejecución de un plugin en una fase sobre un
	 * documento.
	 * 
	 * @param doc
	 *            Documento.
	 * @param scanProfileSpecScanProcStage
	 *            configuración de una fase en un proceso de digitalización.
	 * @param operationStatus
	 *            estado inicial de la fase.
	 * @param e
	 *            Excepción.
	 * @param renewableStageName
	 *            Denominación de la fase del proceso de digitalziación en la
	 *            que se produce error.
	 * @param username
	 *            Nombre de usuario del usuario que solicita el depósito del
	 *            documento.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	public void performResumeDocErrorLogs(ScannedDocumentDTO doc, ScanProfileSpecScanProcStageDTO scanProfileSpecScanProcStage, OperationStatus operationStatus, Exception e, String renewableStageName, String username) throws DAOException {
		ScannedDocLogDTO scannedDocLogDto;

		// Es nueva
		if (operationStatus.equals(OperationStatus.PERFORMED)) {
			scannedDocLogDto = new ScannedDocLogDTO();
			scannedDocLogDto.setStartDate(new Date());
			scannedDocLogDto.setScanProcessStageName(renewableStageName);
			scannedDocLogDto.setStatus(OperationStatus.IN_PROCESS);
			// creamos nueva traza
			webscanModelManager.createScannedDocLog(scannedDocLogDto, doc.getId(), scanProfileSpecScanProcStage.getScanProcessStageId()); // IN_PROCESS
		}
		// está en error, obtengo el último
		scannedDocLogDto = webscanModelManager.getLastScannedDocLogsByDocId(doc.getId());
		scannedDocLogDto.setStatus(OperationStatus.FAILED);
		webscanModelManager.setStatusScannedDocLog(scannedDocLogDto);
		// creamos auditoria
		createPhaseAuditLogs(doc, username, e, renewableStageName);
	}

	/**
	 * recuperamos AuditEventName a través de ResumeStage.
	 * 
	 * @param renewableStageName
	 *            Denominación de la fase reanudable.
	 * @return AuditEventName asociado al ResumeStage
	 */
	private AuditEventName recoveryAuditEventNameByResumeStage(String renewableStageName) {

		AuditEventName res = null;

		switch (renewableStageName) {
			case "Sign":
				res = AuditEventName.SCAN_PROC_DIGITAL_SIGN;
				break;
			case "Metadata":
				res = AuditEventName.SCAN_PROC_METADATA_AUTOMATIC_SET;
				break;

		}

		return res;
	}

	/**
	 * obtiene el nombre de la llamada del plugin según la fase que pasada como
	 * parametro.
	 * 
	 * @param stage
	 *            Fase de ejecución
	 * @param specName
	 *            Nombre de la especificacion del plugin
	 * @return String con la llamada al plugin correspondiente
	 */

	public String getPluginCall(String stage, String specName) {
		String res = null;

		switch (stage) {
			case "Ocr":
				res = PRESIGNATURE_PREFIX_PROP_NAME + "." + specName;
				break;
			case "Sign":
				res = SIGNATURE_PREFIX_PROP_NAME + "." + specName;
				break;
			case "Metadata":
				res = POSTSIGNATURE_PREFIX_PROP_NAME + "." + specName;
				break;
			case "Deposit":
				res = DEPOSIT_PREFIX_PROP_NAME + "." + specName;
				break;

		}

		return res;
	}

	/**
	 * Establece un nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Establece la clase de acceso a la activacion de los plugins del Sistema.
	 * 
	 * @param apluginProvider
	 *            parametro de ejecución de plugin
	 */
	public void setPluginProvider(PluginProvider apluginProvider) {
		this.pluginProvider = apluginProvider;
	}

	/**
	 * Establece la clase de acceso de auditoria del Sistema.
	 * 
	 * @param anAuditService
	 *            Entidad del servicio de auditoria.
	 */
	public void setAuditService(AuditService anAuditService) {
		this.auditService = anAuditService;
	}

	/**
	 * Establece la clase de utilidades para plugins sobre el modelo de datos
	 * del Sistema.
	 * 
	 * @param aPluginDataModelUtilities
	 *            utilidades para plugins sobre el modelo de datos del Sistema.
	 */
	public void setPluginDataModelUtilities(PluginDataModelUtilities aPluginDataModelUtilities) {
		this.pluginDataModelUtilities = aPluginDataModelUtilities;
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Objeto que recopila los mensajes, sujetos a multidioma, que
	 *            son presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}
}
