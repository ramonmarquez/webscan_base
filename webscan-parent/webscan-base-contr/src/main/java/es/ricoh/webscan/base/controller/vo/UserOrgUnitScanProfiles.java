/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.UserOrgUnitScanProfiles.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que representa los perfiles de digitalización configurados para un usuario en una unidad organizativa.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.base.controller.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ricoh.webscan.model.dto.ScanProfileDTO;

/**
 * Objeto de la capa de presentación que representa los perfiles de
 * digitalización configurados para un usuario en una unidad organizativa.
 * <p>
 * Clase UserOrgUnitScanProfiles.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UserOrgUnitScanProfiles implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Unidad organizativa.
	 */
	private OrganizationUnitVO organizationUnit;

	/**
	 * Relación de perfiles de digitalización.
	 */
	private List<ScanProfileDTO> scanProfiles = new ArrayList<ScanProfileDTO>();

	/**
	 * Relación de unidades orgánicas a las que pertenece el usuario para una
	 * unidad organizativa.
	 */
	private List<DepartamentVO> functionalOrgs = new ArrayList<DepartamentVO>();

	/**
	 * Constructor sin argumentos.
	 */
	public UserOrgUnitScanProfiles() {
		super();
	}

	/**
	 * Obtiene la unidad organizativa.
	 * 
	 * @return la unidad organizativa.
	 */
	public OrganizationUnitVO getOrganizationUnit() {
		return organizationUnit;
	}

	/**
	 * Establece un nuevo valor para la unidad organizativa.
	 * 
	 * @param aOrganizationUnit
	 *            nuevo valor para la unidad organizativa.
	 */
	public void setOrganizationUnit(OrganizationUnitVO aOrganizationUnit) {
		this.organizationUnit = aOrganizationUnit;
	}

	/**
	 * Obtiene la relación de perfiles de digitalización.
	 * 
	 * @return la relación de perfiles de digitalización.
	 */
	public List<ScanProfileDTO> getScanProfiles() {
		return scanProfiles;
	}

	/**
	 * Establece un nueva relación de perfiles de digitalización.
	 * 
	 * @param anyScanProfiles
	 *            nueva relación de perfiles de digitalización.
	 */
	public void setScanProfiles(List<ScanProfileDTO> anyScanProfiles) {
		this.scanProfiles.clear();

		if (anyScanProfiles != null && !anyScanProfiles.isEmpty()) {
			this.scanProfiles.addAll(anyScanProfiles);
		}
	}

	/**
	 * Obtiene la relación de unidades orgánicas a las que pertenece el usuario
	 * para una unidad organizativa.
	 * 
	 * @return la relación de unidades orgánicas a las que pertenece el usuario
	 *         para una unidad organizativa.
	 */
	public List<DepartamentVO> getFunctionalOrgs() {
		return functionalOrgs;
	}

	/**
	 * Establece la nueva relación de unidades orgánicas a las que pertenece el
	 * usuario para una unidad organizativa.
	 * 
	 * @param anyFunctionalOrgs
	 *            Nueva relación de unidades orgánicas a las que pertenece el
	 *            usuario para una unidad organizativa.
	 */
	public void setFunctionalOrgs(List<DepartamentVO> anyFunctionalOrgs) {
		this.functionalOrgs.clear();
		if (anyFunctionalOrgs != null && !anyFunctionalOrgs.isEmpty()) {
			this.functionalOrgs.addAll(anyFunctionalOrgs);
		}
	}
}
