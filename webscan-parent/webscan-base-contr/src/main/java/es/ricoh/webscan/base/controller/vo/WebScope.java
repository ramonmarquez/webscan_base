/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.WebScope.java.</p>
 * <b>Descripción:</b><p> Ambito de visualización de un mensaje presentado en la capa de presentación.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.base.controller.vo;

/**
 * Ambito de visualización de un mensaje presentado en la capa de presentación.
 * <p>
 * Clase WebScope.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum WebScope {

	/**
	 * Petición.
	 */
	REQUEST("REQUEST"),
	/**
	 * Sesión.
	 */
	SESSION("SESSION"),
	/**
	 * Aplicación.
	 */
	APPLICATION("APPLICATION");

	/**
	 * Ambito de visualización de un mensaje presentado en la capa de
	 * presentación.
	 */
	private String scope;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aScope
	 *            Ambito de visualización de un mensaje presentado en la capa de
	 *            presentación.
	 */
	WebScope(String aScope) {
		this.scope = aScope;
	}

	/**
	 * Obtiene el ámbito de visualización de un mensaje presentado en la capa de
	 * presentación.
	 * 
	 * @return el ámbito de visualización de un mensaje presentado en la capa de
	 *         presentación.
	 */
	public String getScope() {
		return scope;
	}

}
