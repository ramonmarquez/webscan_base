/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.IdentifierListVO.java.</p>
* <b>Descripción:</b><p> Objeto que representa una lista de identificadores en notación JSON.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.base.controller.vo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Objeto que representa una lista de identificadores en notación JSON.
 * <p>
 * Clase IdentifierListVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "tokens" })
public class IdentifierListVO {

	/**
	 * Lista de identificadores.
	 */
	@JsonProperty("tokens")
	private List<Long> tokens = new ArrayList<Long>();

	/**
	 * Constructor sin argumentos.
	 */
	public IdentifierListVO() {
		super();
	}

	/**
	 * Obtiene la lista de identificadores.
	 * 
	 * @return la lista de identificadores.
	 */
	public List<Long> getTokens() {
		return tokens;
	}

	/**
	 * Establece la lista de identificadores.
	 * 
	 * @param anyTokens
	 *            una lista de identificadores.
	 */
	public void setTokens(List<Long> anyTokens) {
		this.tokens.clear();

		if (anyTokens != null && !anyTokens.isEmpty()) {
			this.tokens.addAll(anyTokens);
		}
	}

}
