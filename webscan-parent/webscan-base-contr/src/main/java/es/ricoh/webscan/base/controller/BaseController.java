/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.base.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.base.context.WebscanApplication;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;

/**
 * Clase que agrupa funcionalidades comunes para ser empleadas por los
 * diferentes controladores implantados en el sistema.
 * <p>
 * Clase BaseController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Controller
public class BaseController {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BaseController.class);

	/**
	 * Caracter por el que el que es sustituido el carácter ',' en la
	 * recuperación de mensajes multidioma.
	 */
	private static final String CHANGEBYCOMA = "%42d";

	/**
	 * conjunto de atributos de respuestas usados en las vistas.
	 */
	private static final String ATTR_VIEW_MESSAGE = "message";

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	@Autowired
	private MessageSource messageSource;
	/**
	 * Objeto de acceso a la aplicación de webscan.
	 */
	@Autowired
	private WebscanApplication webscanApplication;

	/**
	 * Pagina principal.
	 * 
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @param request
	 *            Petición http.
	 * @return nombre de plantilla a mostrar
	 */
	@RequestMapping(value = "/main", method = { RequestMethod.GET, RequestMethod.POST })
	public String main(final Model model, final HttpServletRequest request) {
		LOG.debug("[WEBSCAN-BASE WEB] Presentando vista principal del sistema ...");

		try {
			webscanApplication.initDeployedModules();
			webscanApplication.initScanClientComponent();
		} catch (WebscanException e) {
			if (WebscanException.CODE_993.equals(e.getCode())) {
				BaseControllerUtils.addCommonMessage("scanClientComp.error.msg", null, ViewMessageLevel.ERROR, WebScope.SESSION, request);
			} else {
				BaseControllerUtils.setDefaultExceptionInRequest(e, request, messageSource, Boolean.TRUE);
			}
		}

		return "main";
	}

	/**
	 * Metodo para obtener los mesajes en pantalla.
	 * 
	 * @param model
	 *            Entidad para pasar información entre el controlador y la vista
	 * @return String
	 */
	@RequestMapping(value = "/message", method = { RequestMethod.GET, RequestMethod.POST })
	public String message(final Model model) {
		LOG.debug("[WEBSCAN-BASE WEB] Presentando mensajes ...");

		return "fragments/base/message";
	}

	/**
	 * Método para obtener la traducción de idioma directamente en el front.
	 * 
	 * @param request
	 *            Llamada HTTP realizada
	 * @param model
	 *            Parámetro auxiliar para pasar información a la vista
	 * @param args
	 *            Parametros con los argumentos pasados a través de la llamada,
	 *            desde 1 hasta n.
	 * @return el resultado en una vista, de manera que puede ser usado
	 *         directamente para mostrar info
	 * @throws Exception
	 *             Exception al recuperar la información asociada.
	 */
	@RequestMapping(value = "/getTextLanguage", method = RequestMethod.POST)
	public String getTextLanguage(HttpServletRequest request, ModelMap model, String[ ] args) {
		String res = "textTemplate";
		String mensaje = "";
		String argumentos[] = null;
		if (args != null && args.length > 0) {
			try {
				/*
				 * recuperamos el mensaje base, es decir, el que vamos recuperar
				 * de los properties
				 */
				String menssageBase = args[0];
				// si hay argumentos para rellenar el mensaje base
				if (args.length > 1) {
					argumentos = new String[args.length - 1];
					// preparamos los textos a pasar
					for (int i = 1; i < args.length; i++) {
						// tratamos las comas para sustituirlas
						String aux = args[i].replaceAll(CHANGEBYCOMA, ",");
						argumentos[i - 1] = aux;
					}
				}
				mensaje = messageSource.getMessage(menssageBase, argumentos, request.getLocale());
			} catch (Exception e) {
				LOG.error("[ScanProfileController - getTextLanguage] - Error recuperar mensajes para idioma.Error:", e.getMessage());
			}
		}
		model.addAttribute(ATTR_VIEW_MESSAGE, mensaje);
		return res;
	}
}
