/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.ViewMessageLevel.java.</p>
 * <b>Descripción:</b><p> Categoría o nivel de un mensaje presentado en la capa de presentación.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.base.controller.vo;

/**
 * Categoría o nivel de un mensaje presentado en la capa de presentación.
 * <p>
 * Clase ViewMessageLevel.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum ViewMessageLevel {

	/**
	 * Informativo.
	 */
	INFO("INFO"),
	/**
	 * Aviso.
	 */
	WARN("WARN"),
	/**
	 * Error.
	 */
	ERROR("ERROR");

	/**
	 * Categoría o nivel de un mensaje presentado en la capa de presentación.
	 */
	private String level;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aLevel
	 *            Categoría o nivel de un mensaje presentado en la capa de
	 *            presentación.
	 */
	ViewMessageLevel(String aLevel) {
		this.level = aLevel;
	}

	/**
	 * Obtiene la categoría o nivel de un mensaje presentado en la capa de
	 * presentación.
	 * 
	 * @return la categoría o nivel de un mensaje presentado en la capa de
	 *         presentación.
	 */
	public String getLevel() {
		return level;
	}
}
