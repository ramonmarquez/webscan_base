/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.ViewMessage.java.</p>
 * <b>Descripción:</b><p> Objeto de la capa de presentación que representa los mensajes presentados en la pantalla principal.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.base.controller.vo;

import java.io.Serializable;

/**
 * Objeto de la capa de presentación que representa los mensajes presentados en
 * la pantalla principal.
 * <p>
 * Clase ViewMessage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ViewMessage implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la propiedad que identifica el mensaje.
	 */
	private String msgText;

	/**
	 * Argumentos del mensaje.
	 */
	private String[ ] msgArgs = null;

	/**
	 * Categoría o nivel de un mensaje presentado en la capa de presentación.
	 */
	private ViewMessageLevel msgLevel = ViewMessageLevel.INFO;

	/**
	 * Ambito del mensaje.
	 */
	private WebScope msgScope = WebScope.REQUEST;

	/**
	 * Indicador de visualización del mensaje.
	 */
	private Boolean visualized = Boolean.FALSE;

	/**
	 * Constructor sin argumentos.
	 */
	public ViewMessage() {
		super();
	}

	/**
	 * Obtiene el texto del mensaje.
	 * 
	 * @return el texto del mensaje.
	 */
	public String getMsgText() {
		return msgText;
	}

	/**
	 * Establece un nuevo valor para el texto del mensaje.
	 * 
	 * @param aMsgText
	 *            nuevo valor para el texto del mensaje.
	 */
	public void setMsgText(String aMsgText) {
		this.msgText = aMsgText;
	}

	/**
	 * Obtiene la categoría o nivel de un mensaje presentado en la capa de
	 * presentación.
	 * 
	 * @return la categoría o nivel de un mensaje presentado en la capa de
	 *         presentación.
	 */
	public ViewMessageLevel getMsgLevel() {
		return msgLevel;
	}

	/**
	 * Establece un nuevo valor para la categoría o nivel de un mensaje
	 * presentado en la capa de presentación.
	 * 
	 * @param aMsgLevel
	 *            nuevo valor para la categoría o nivel de un mensaje presentado
	 *            en la capa de presentación.
	 */
	public void setMsgLevel(ViewMessageLevel aMsgLevel) {
		this.msgLevel = aMsgLevel;
	}

	/**
	 * Obtiene el ámbito de visualización de un mensaje en la capa de
	 * presentación.
	 * 
	 * @return el ámbito de visualización de un mensaje en la capa de
	 *         presentación.
	 */
	public WebScope getMsgScope() {
		return msgScope;
	}

	/**
	 * Establece un nuevo valor para el ámbito de visualización de un mensaje en
	 * la capa de presentación.
	 * 
	 * @param aMsgScope
	 *            nuevo valor para el ámbito de visualización de un mensaje en
	 *            la capa de presentación.
	 */
	public void setMsgScope(WebScope aMsgScope) {
		this.msgScope = aMsgScope;
	}

	/**
	 * Obtiene el indicador de visualización del mensaje. Si ha sido
	 * visualizado, valor true.
	 * 
	 * @return el indicador de visualización del mensaje.
	 */
	public Boolean getVisualized() {
		return visualized;
	}

	/**
	 * Establece un nuevo valor para el indicador de visualización del mensaje.
	 * Si ha sido visualizado, valor true.
	 * 
	 * @param visualizedParam
	 *            nuevo valor para el indicador de visualización del mensaje.
	 */
	public void setVisualized(Boolean visualizedParam) {
		this.visualized = visualizedParam;
	}

	/**
	 * Obtiene el valor del campo msgArgs.
	 * 
	 * @return Array de String con los mensajes.
	 */
	public String[ ] getMsgArgs() {
		return msgArgs;
	}

	/**
	 * Modifica el campo msgArgs.
	 * 
	 * @param amsgArgs
	 *            Array de String con mensajes.
	 */
	public void setMsgArgs(String[ ] amsgArgs) {
		this.msgArgs = amsgArgs;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((msgLevel == null) ? 0 : msgLevel.hashCode());
		result = prime * result + ((msgScope == null) ? 0 : msgScope.hashCode());
		result = prime * result + ((msgText == null) ? 0 : msgText.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ViewMessage other = (ViewMessage) obj;
		if (msgLevel != other.msgLevel)
			return false;
		if (msgScope != other.msgScope)
			return false;
		if (msgText == null) {
			if (other.msgText != null)
				return false;
		} else if (!msgText.equals(other.msgText))
			return false;
		return true;
	}

}
