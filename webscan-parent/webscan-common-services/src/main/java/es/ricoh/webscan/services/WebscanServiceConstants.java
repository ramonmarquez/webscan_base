/** 
* <b>Archivo:</b><p>es.ricoh.webscan.services.WebscanGVServiceConstants.java.</p>
* <b>Descripción:</b><p> Constantes específicas de la solución de digitalización de GV.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.services;

import java.text.SimpleDateFormat;

/**
 * Constantes empleadas por los servicios publicados por el sistema.
 * <p>
 * Clase WebscanGVServiceConstants.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public interface WebscanServiceConstants {

	/**
	 * Formato de la fecha incluida en la cabecera SOAP de trazabilidad.
	 */
	SimpleDateFormat ISO_8601_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
}
