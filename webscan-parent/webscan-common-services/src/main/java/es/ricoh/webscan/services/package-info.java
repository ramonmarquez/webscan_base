/**
 * Paquete raíz del componente que agrupa funcionalidades y mensajería común a
 * los servicios publicados por el sistema.
 */
package es.ricoh.webscan.services;