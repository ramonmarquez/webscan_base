/**
 * Interceptores de entrada y salida comunes de los servicios publicados por
 * interfaz SOAP.
 */
package es.ricoh.webscan.services.soap.common.interceptors;