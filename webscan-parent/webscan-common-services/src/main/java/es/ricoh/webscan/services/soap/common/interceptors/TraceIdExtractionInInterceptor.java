package es.ricoh.webscan.services.soap.common.interceptors;
/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.service.impl.soap.interceptors.TraceIdExtractionInInterceptor.java.</p>
* <b>Descripción:</b><p> Interceptor que extrae el identificador de trazabilidad de la cabecera SOAP.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/


import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;

/**
 * Interceptor que extrae el identificador de trazabilidad de la cabecera SOAP.
 * <p>
 * Clase TraceIdExtractionInInterceptor.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class TraceIdExtractionInInterceptor extends AbstractSoapInterceptor {

	/**
	 * Nombre de la cabecera SOAP de trazabilidad.
	 */
	public static final String TRACE_HEADER_NAME = "Id_trazabilidad";

	/**
	 * QName de la cabecera SOAP de trazabilidad.
	 */
	private static final QName TRACE_HEADER_QNAME = new QName("http://dgti.gva.es/interoperabilidad", TRACE_HEADER_NAME);

	public TraceIdExtractionInInterceptor() {
		super(Phase.POST_LOGICAL);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.apache.cxf.interceptor.Interceptor#handleMessage(org.apache.cxf.message.Message)
	 */
	@Override
	public void handleMessage(SoapMessage message) throws Fault {
		Header traceHeader;
		String traceId;

		traceHeader = message.getHeader(TRACE_HEADER_QNAME);

		if (traceHeader != null && traceHeader.getObject() != null) {
			traceId = traceHeader.getObject().toString();
			message.getExchange().put(TRACE_HEADER_NAME, traceId);
		}

	}

}
