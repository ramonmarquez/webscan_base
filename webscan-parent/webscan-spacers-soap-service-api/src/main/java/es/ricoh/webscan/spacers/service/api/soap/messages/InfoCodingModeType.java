
package es.ricoh.webscan.spacers.service.api.soap.messages;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InfoCodingModeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InfoCodingModeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Code QR"/&gt;
 *     &lt;enumeration value="Code 128"/&gt;
 *     &lt;enumeration value="PDF 417"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "InfoCodingModeType")
@XmlEnum
public enum InfoCodingModeType {

    @XmlEnumValue("Code QR")
    CODE_QR("Code QR"),
    @XmlEnumValue("Code 128")
    CODE_128("Code 128"),
    @XmlEnumValue("PDF 417")
    PDF_417("PDF 417");
    private final String value;

    InfoCodingModeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InfoCodingModeType fromValue(String v) {
        for (InfoCodingModeType c: InfoCodingModeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
