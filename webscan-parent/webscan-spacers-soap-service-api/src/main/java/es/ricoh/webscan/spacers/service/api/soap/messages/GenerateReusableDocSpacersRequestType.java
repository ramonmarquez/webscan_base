
package es.ricoh.webscan.spacers.service.api.soap.messages;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Petición de la operación de generación de separadores reutilizables de documentos.
 * 
 * <p>Java class for GenerateReusableDocSpacersRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateReusableDocSpacersRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="spacerGenerationParams" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}SpacerGenerationParamsType"/&gt;
 *         &lt;element name="numberOfReusableDocSpacers" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateReusableDocSpacersRequestType", propOrder = {
    "userId",
    "spacerGenerationParams",
    "numberOfReusableDocSpacers"
})
public class GenerateReusableDocSpacersRequestType {

    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected SpacerGenerationParamsType spacerGenerationParams;
    @XmlElement(required = true)
    protected BigInteger numberOfReusableDocSpacers;

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the spacerGenerationParams property.
     * 
     * @return
     *     possible object is
     *     {@link SpacerGenerationParamsType }
     *     
     */
    public SpacerGenerationParamsType getSpacerGenerationParams() {
        return spacerGenerationParams;
    }

    /**
     * Sets the value of the spacerGenerationParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpacerGenerationParamsType }
     *     
     */
    public void setSpacerGenerationParams(SpacerGenerationParamsType value) {
        this.spacerGenerationParams = value;
    }

    /**
     * Gets the value of the numberOfReusableDocSpacers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfReusableDocSpacers() {
        return numberOfReusableDocSpacers;
    }

    /**
     * Sets the value of the numberOfReusableDocSpacers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfReusableDocSpacers(BigInteger value) {
        this.numberOfReusableDocSpacers = value;
    }

}
