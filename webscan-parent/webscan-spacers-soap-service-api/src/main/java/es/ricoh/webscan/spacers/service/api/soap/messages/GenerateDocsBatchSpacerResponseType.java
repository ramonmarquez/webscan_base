
package es.ricoh.webscan.spacers.service.api.soap.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Resultado de la operación de generación de carátula de lote de documentos.
 * 
 * <p>Java class for GenerateDocsBatchSpacerResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateDocsBatchSpacerResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}result"/&gt;
 *         &lt;element name="resParam" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}SpacersResponseResParamType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateDocsBatchSpacerResponseType", propOrder = {
    "result",
    "resParam"
})
public class GenerateDocsBatchSpacerResponseType {

    @XmlElement(required = true)
    protected OperationResultType result;
    protected SpacersResponseResParamType resParam;

    /**
     * Resultado retornado por el servicio trás la invocación de la operación de generación de carátula de lote de documentos.
     * 
     * @return
     *     possible object is
     *     {@link OperationResultType }
     *     
     */
    public OperationResultType getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationResultType }
     *     
     */
    public void setResult(OperationResultType value) {
        this.result = value;
    }

    /**
     * Gets the value of the resParam property.
     * 
     * @return
     *     possible object is
     *     {@link SpacersResponseResParamType }
     *     
     */
    public SpacersResponseResParamType getResParam() {
        return resParam;
    }

    /**
     * Sets the value of the resParam property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpacersResponseResParamType }
     *     
     */
    public void setResParam(SpacersResponseResParamType value) {
        this.resParam = value;
    }

}
