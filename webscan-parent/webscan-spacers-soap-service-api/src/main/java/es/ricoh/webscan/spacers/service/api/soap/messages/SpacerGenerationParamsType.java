
package es.ricoh.webscan.spacers.service.api.soap.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo que define los parámetros de generación de carátulas y separadores reutilizables de documentos.
 * 
 * <p>Java class for SpacerGenerationParamsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpacerGenerationParamsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}infoCodingMode"/&gt;
 *         &lt;element name="locale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpacerGenerationParamsType", propOrder = {
    "infoCodingMode",
    "locale"
})
public class SpacerGenerationParamsType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected InfoCodingModeType infoCodingMode;
    @XmlElement(defaultValue = "es")
    protected String locale;

    /**
     * Modo en el que es codificada la información en carátulas y separadores.
     * 
     * @return
     *     possible object is
     *     {@link InfoCodingModeType }
     *     
     */
    public InfoCodingModeType getInfoCodingMode() {
        return infoCodingMode;
    }

    /**
     * Sets the value of the infoCodingMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoCodingModeType }
     *     
     */
    public void setInfoCodingMode(InfoCodingModeType value) {
        this.infoCodingMode = value;
    }

    /**
     * Gets the value of the locale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Sets the value of the locale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocale(String value) {
        this.locale = value;
    }

}
