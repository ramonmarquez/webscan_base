
package es.ricoh.webscan.spacers.service.api.soap.messages;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Petición de la operación de generación de carátula de lotes de documentos.
 * 
 * <p>Java class for GenerateDocsBatchSpacerRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateDocsBatchSpacerRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="department" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="spacerGenerationParams" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}SpacerGenerationParamsType"/&gt;
 *         &lt;element name="spacerAdditionalInfoParams" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}SpacerAdditionalInfoType"/&gt;
 *         &lt;element name="spacerDocs" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}SpacerDocsType"/&gt;
 *         &lt;element name="batchSpacerValParams" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}BatchSpacerValParamsType" minOccurs="0"/&gt;
 *         &lt;element name="numberOfReusableDocSpacers" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateDocsBatchSpacerRequestType", propOrder = {
    "userId",
    "department",
    "spacerGenerationParams",
    "spacerAdditionalInfoParams",
    "spacerDocs",
    "batchSpacerValParams",
    "numberOfReusableDocSpacers"
})
public class GenerateDocsBatchSpacerRequestType {

    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected String department;
    @XmlElement(required = true)
    protected SpacerGenerationParamsType spacerGenerationParams;
    @XmlElement(required = true)
    protected SpacerAdditionalInfoType spacerAdditionalInfoParams;
    @XmlElement(required = true)
    protected SpacerDocsType spacerDocs;
    protected BatchSpacerValParamsType batchSpacerValParams;
    protected BigInteger numberOfReusableDocSpacers;

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the department property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets the value of the department property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartment(String value) {
        this.department = value;
    }

    /**
     * Gets the value of the spacerGenerationParams property.
     * 
     * @return
     *     possible object is
     *     {@link SpacerGenerationParamsType }
     *     
     */
    public SpacerGenerationParamsType getSpacerGenerationParams() {
        return spacerGenerationParams;
    }

    /**
     * Sets the value of the spacerGenerationParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpacerGenerationParamsType }
     *     
     */
    public void setSpacerGenerationParams(SpacerGenerationParamsType value) {
        this.spacerGenerationParams = value;
    }

    /**
     * Gets the value of the spacerAdditionalInfoParams property.
     * 
     * @return
     *     possible object is
     *     {@link SpacerAdditionalInfoType }
     *     
     */
    public SpacerAdditionalInfoType getSpacerAdditionalInfoParams() {
        return spacerAdditionalInfoParams;
    }

    /**
     * Sets the value of the spacerAdditionalInfoParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpacerAdditionalInfoType }
     *     
     */
    public void setSpacerAdditionalInfoParams(SpacerAdditionalInfoType value) {
        this.spacerAdditionalInfoParams = value;
    }

    /**
     * Gets the value of the spacerDocs property.
     * 
     * @return
     *     possible object is
     *     {@link SpacerDocsType }
     *     
     */
    public SpacerDocsType getSpacerDocs() {
        return spacerDocs;
    }

    /**
     * Sets the value of the spacerDocs property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpacerDocsType }
     *     
     */
    public void setSpacerDocs(SpacerDocsType value) {
        this.spacerDocs = value;
    }

    /**
     * Gets the value of the batchSpacerValParams property.
     * 
     * @return
     *     possible object is
     *     {@link BatchSpacerValParamsType }
     *     
     */
    public BatchSpacerValParamsType getBatchSpacerValParams() {
        return batchSpacerValParams;
    }

    /**
     * Sets the value of the batchSpacerValParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link BatchSpacerValParamsType }
     *     
     */
    public void setBatchSpacerValParams(BatchSpacerValParamsType value) {
        this.batchSpacerValParams = value;
    }

    /**
     * Gets the value of the numberOfReusableDocSpacers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfReusableDocSpacers() {
        return numberOfReusableDocSpacers;
    }

    /**
     * Sets the value of the numberOfReusableDocSpacers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfReusableDocSpacers(BigInteger value) {
        this.numberOfReusableDocSpacers = value;
    }

}
