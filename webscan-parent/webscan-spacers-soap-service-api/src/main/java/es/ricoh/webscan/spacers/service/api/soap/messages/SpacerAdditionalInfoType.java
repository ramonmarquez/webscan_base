
package es.ricoh.webscan.spacers.service.api.soap.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo que especifica la información adicional para la generación de la carátula y la configuración del proceso de digitalización.
 * 
 * <p>Java class for SpacerAdditionalInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpacerAdditionalInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="scanProfile" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}ScanProfileType"/&gt;
 *         &lt;element name="docSpecName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpacerAdditionalInfoType", propOrder = {
    "scanProfile",
    "docSpecName"
})
public class SpacerAdditionalInfoType {

    @XmlElement(required = true)
    protected ScanProfileType scanProfile;
    @XmlElement(required = true)
    protected String docSpecName;

    /**
     * Gets the value of the scanProfile property.
     * 
     * @return
     *     possible object is
     *     {@link ScanProfileType }
     *     
     */
    public ScanProfileType getScanProfile() {
        return scanProfile;
    }

    /**
     * Sets the value of the scanProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScanProfileType }
     *     
     */
    public void setScanProfile(ScanProfileType value) {
        this.scanProfile = value;
    }

    /**
     * Gets the value of the docSpecName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocSpecName() {
        return docSpecName;
    }

    /**
     * Sets the value of the docSpecName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocSpecName(String value) {
        this.docSpecName = value;
    }

}
