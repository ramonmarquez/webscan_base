
package es.ricoh.webscan.spacers.service.api.soap.messages;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo que define la relación de documentos del lote identificado por la carátula.
 * 
 * <p>Java class for SpacerDocsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpacerDocsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="spacerDoc" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}SpacerDocType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpacerDocsType", propOrder = {
    "spacerDoc"
})
public class SpacerDocsType {

    @XmlElement(required = true)
    protected List<SpacerDocType> spacerDoc;

    /**
     * Gets the value of the spacerDoc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the spacerDoc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpacerDoc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpacerDocType }
     * 
     * 
     */
    public List<SpacerDocType> getSpacerDoc() {
        if (spacerDoc == null) {
            spacerDoc = new ArrayList<SpacerDocType>();
        }
        return this.spacerDoc;
    }

}
