
package es.ricoh.webscan.spacers.service.api.soap.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Perfil de digitalización establecido para el lote identificado mediante la carátula.
 * 
 * <p>Java class for ScanProfileType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScanProfileType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="scanTemplateName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="userGroupExternalId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScanProfileType", propOrder = {
    "scanTemplateName",
    "userGroupExternalId"
})
public class ScanProfileType {

    @XmlElement(required = true)
    protected String scanTemplateName;
    @XmlElement(required = true)
    protected String userGroupExternalId;

    /**
     * Gets the value of the scanTemplateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScanTemplateName() {
        return scanTemplateName;
    }

    /**
     * Sets the value of the scanTemplateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScanTemplateName(String value) {
        this.scanTemplateName = value;
    }

    /**
     * Gets the value of the userGroupExternalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserGroupExternalId() {
        return userGroupExternalId;
    }

    /**
     * Sets the value of the userGroupExternalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserGroupExternalId(String value) {
        this.userGroupExternalId = value;
    }

}
