
package es.ricoh.webscan.spacers.service.api.soap.messages;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the es.ricoh.webscan.spacers.service.api.soap.messages package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Result_QNAME = new QName("urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", "result");
    private final static QName _InfoCodingMode_QNAME = new QName("urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", "infoCodingMode");
    private final static QName _GenerateDocsBatchSpacerRequest_QNAME = new QName("urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", "generateDocsBatchSpacerRequest");
    private final static QName _GenerateDocsBatchSpacerResponse_QNAME = new QName("urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", "generateDocsBatchSpacerResponse");
    private final static QName _GenerateReusableDocSpacersRequest_QNAME = new QName("urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", "generateReusableDocSpacersRequest");
    private final static QName _GenerateReusableDocSpacersResponse_QNAME = new QName("urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", "generateReusableDocSpacersResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: es.ricoh.webscan.spacers.service.api.soap.messages
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OperationResultType }
     * 
     */
    public OperationResultType createOperationResultType() {
        return new OperationResultType();
    }

    /**
     * Create an instance of {@link GenerateDocsBatchSpacerRequestType }
     * 
     */
    public GenerateDocsBatchSpacerRequestType createGenerateDocsBatchSpacerRequestType() {
        return new GenerateDocsBatchSpacerRequestType();
    }

    /**
     * Create an instance of {@link GenerateDocsBatchSpacerResponseType }
     * 
     */
    public GenerateDocsBatchSpacerResponseType createGenerateDocsBatchSpacerResponseType() {
        return new GenerateDocsBatchSpacerResponseType();
    }

    /**
     * Create an instance of {@link GenerateReusableDocSpacersRequestType }
     * 
     */
    public GenerateReusableDocSpacersRequestType createGenerateReusableDocSpacersRequestType() {
        return new GenerateReusableDocSpacersRequestType();
    }

    /**
     * Create an instance of {@link GenerateReusableDocSpacersResponseType }
     * 
     */
    public GenerateReusableDocSpacersResponseType createGenerateReusableDocSpacersResponseType() {
        return new GenerateReusableDocSpacersResponseType();
    }

    /**
     * Create an instance of {@link SpacerType }
     * 
     */
    public SpacerType createSpacerType() {
        return new SpacerType();
    }

    /**
     * Create an instance of {@link SpacersResponseResParamType }
     * 
     */
    public SpacersResponseResParamType createSpacersResponseResParamType() {
        return new SpacersResponseResParamType();
    }

    /**
     * Create an instance of {@link SpacerGenerationParamsType }
     * 
     */
    public SpacerGenerationParamsType createSpacerGenerationParamsType() {
        return new SpacerGenerationParamsType();
    }

    /**
     * Create an instance of {@link BatchSpacerValParamsType }
     * 
     */
    public BatchSpacerValParamsType createBatchSpacerValParamsType() {
        return new BatchSpacerValParamsType();
    }

    /**
     * Create an instance of {@link MetadataType }
     * 
     */
    public MetadataType createMetadataType() {
        return new MetadataType();
    }

    /**
     * Create an instance of {@link MetadataCollectionType }
     * 
     */
    public MetadataCollectionType createMetadataCollectionType() {
        return new MetadataCollectionType();
    }

    /**
     * Create an instance of {@link SpacerDocType }
     * 
     */
    public SpacerDocType createSpacerDocType() {
        return new SpacerDocType();
    }

    /**
     * Create an instance of {@link SpacerDocsType }
     * 
     */
    public SpacerDocsType createSpacerDocsType() {
        return new SpacerDocsType();
    }

    /**
     * Create an instance of {@link ScanProfileType }
     * 
     */
    public ScanProfileType createScanProfileType() {
        return new ScanProfileType();
    }

    /**
     * Create an instance of {@link SpacerAdditionalInfoType }
     * 
     */
    public SpacerAdditionalInfoType createSpacerAdditionalInfoType() {
        return new SpacerAdditionalInfoType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperationResultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", name = "result")
    public JAXBElement<OperationResultType> createResult(OperationResultType value) {
        return new JAXBElement<OperationResultType>(_Result_QNAME, OperationResultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InfoCodingModeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", name = "infoCodingMode")
    public JAXBElement<InfoCodingModeType> createInfoCodingMode(InfoCodingModeType value) {
        return new JAXBElement<InfoCodingModeType>(_InfoCodingMode_QNAME, InfoCodingModeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateDocsBatchSpacerRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", name = "generateDocsBatchSpacerRequest")
    public JAXBElement<GenerateDocsBatchSpacerRequestType> createGenerateDocsBatchSpacerRequest(GenerateDocsBatchSpacerRequestType value) {
        return new JAXBElement<GenerateDocsBatchSpacerRequestType>(_GenerateDocsBatchSpacerRequest_QNAME, GenerateDocsBatchSpacerRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateDocsBatchSpacerResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", name = "generateDocsBatchSpacerResponse")
    public JAXBElement<GenerateDocsBatchSpacerResponseType> createGenerateDocsBatchSpacerResponse(GenerateDocsBatchSpacerResponseType value) {
        return new JAXBElement<GenerateDocsBatchSpacerResponseType>(_GenerateDocsBatchSpacerResponse_QNAME, GenerateDocsBatchSpacerResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateReusableDocSpacersRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", name = "generateReusableDocSpacersRequest")
    public JAXBElement<GenerateReusableDocSpacersRequestType> createGenerateReusableDocSpacersRequest(GenerateReusableDocSpacersRequestType value) {
        return new JAXBElement<GenerateReusableDocSpacersRequestType>(_GenerateReusableDocSpacersRequest_QNAME, GenerateReusableDocSpacersRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateReusableDocSpacersResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", name = "generateReusableDocSpacersResponse")
    public JAXBElement<GenerateReusableDocSpacersResponseType> createGenerateReusableDocSpacersResponse(GenerateReusableDocSpacersResponseType value) {
        return new JAXBElement<GenerateReusableDocSpacersResponseType>(_GenerateReusableDocSpacersResponse_QNAME, GenerateReusableDocSpacersResponseType.class, null, value);
    }

}
