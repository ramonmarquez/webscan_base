
package es.ricoh.webscan.spacers.service.api.soap;

import javax.xml.ws.WebFault;

/**
 * This class was generated by Apache CXF 3.1.12 2017-10-20T11:03:17.484+02:00
 * Generated source version: 3.1.12
 */

@WebFault(name = "Atributos", targetNamespace = "http://intermediacion.redsara.es/scsp/esquemas/V3/soapfaultatributos")
public class WebscanException extends Exception {

	private es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.Atributos atributos;

	public WebscanException() {
		super();
	}

	public WebscanException(String message) {
		super(message);
	}

	public WebscanException(String message, Throwable cause) {
		super(message, cause);
	}

	public WebscanException(String message, es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.Atributos atributos) {
		super(message);
		this.atributos = atributos;
	}

	public WebscanException(String message, es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.Atributos atributos, Throwable cause) {
		super(message, cause);
		this.atributos = atributos;
	}

	public es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.Atributos getFaultInfo() {
		return this.atributos;
	}
}
