
package es.ricoh.webscan.spacers.service.api.soap.messages;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReuseDocsBatchSpacerType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReuseDocsBatchSpacerType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Blank page"/&gt;
 *     &lt;enumeration value="Code QR"/&gt;
 *     &lt;enumeration value="Code 128"/&gt;
 *     &lt;enumeration value="PDF 417"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ReuseDocsBatchSpacerType")
@XmlEnum
public enum ReuseDocsBatchSpacerType {

    @XmlEnumValue("Blank page")
    BLANK_PAGE("Blank page"),
    @XmlEnumValue("Code QR")
    CODE_QR("Code QR"),
    @XmlEnumValue("Code 128")
    CODE_128("Code 128"),
    @XmlEnumValue("PDF 417")
    PDF_417("PDF 417");
    private final String value;

    ReuseDocsBatchSpacerType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReuseDocsBatchSpacerType fromValue(String v) {
        for (ReuseDocsBatchSpacerType c: ReuseDocsBatchSpacerType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
