package es.ricoh.webscan.spacers.service.api.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

@WebService(targetNamespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:1.1.1", name = "SpacersGeneration")
@XmlSeeAlso({ es.ricoh.webscan.spacers.service.api.soap.messages.ObjectFactory.class, es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.ObjectFactory.class })
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface SpacersGeneration {

	@WebResult(name = "generateReusableDocSpacersResponse", targetNamespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", partName = "parameters")
	@WebMethod(action = "urn:es:gv:dgti:webscan:services:generateReusableDocSpacers")
	public es.ricoh.webscan.spacers.service.api.soap.messages.GenerateReusableDocSpacersResponseType generateReusableDocSpacers(@WebParam(partName = "parameters", name = "generateReusableDocSpacersRequest", targetNamespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1") es.ricoh.webscan.spacers.service.api.soap.messages.GenerateReusableDocSpacersRequestType parameters) throws WebscanException;

	@WebResult(name = "generateDocsBatchSpacerResponse", targetNamespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1", partName = "parameters")
	@WebMethod(action = "urn:es:gv:dgti:webscan:services:generateDocsBatchSpacer")
	public es.ricoh.webscan.spacers.service.api.soap.messages.GenerateDocsBatchSpacerResponseType generateDocsBatchSpacer(@WebParam(partName = "parameters", name = "generateDocsBatchSpacerRequest", targetNamespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1") es.ricoh.webscan.spacers.service.api.soap.messages.GenerateDocsBatchSpacerRequestType parameters) throws WebscanException;
}
