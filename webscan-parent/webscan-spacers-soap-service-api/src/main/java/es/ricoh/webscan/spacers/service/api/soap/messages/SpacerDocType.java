
package es.ricoh.webscan.spacers.service.api.soap.messages;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo que define la estrucutura e información de un documento del lote.
 * 			
 * 
 * <p>Java class for SpacerDocType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpacerDocType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="orderNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="metadataCollection" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}MetadataCollectionType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpacerDocType", propOrder = {
    "orderNumber",
    "name",
    "metadataCollection"
})
public class SpacerDocType {

    @XmlElement(required = true)
    protected BigInteger orderNumber;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected MetadataCollectionType metadataCollection;

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOrderNumber(BigInteger value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the metadataCollection property.
     * 
     * @return
     *     possible object is
     *     {@link MetadataCollectionType }
     *     
     */
    public MetadataCollectionType getMetadataCollection() {
        return metadataCollection;
    }

    /**
     * Sets the value of the metadataCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link MetadataCollectionType }
     *     
     */
    public void setMetadataCollection(MetadataCollectionType value) {
        this.metadataCollection = value;
    }

}
