
package es.ricoh.webscan.spacers.service.api.soap.messages;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo que define las validaciones a aplicar sobre el lote de documentos para el cual es generado una carátula.
 * 
 * <p>Java class for BatchSpacerValParamsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BatchSpacerValParamsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reuseDocsBatchSpacer" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}ReuseDocsBatchSpacerType" minOccurs="0"/&gt;
 *         &lt;element name="numberOfDocsInBatch" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="numberOfPagesInBatch" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BatchSpacerValParamsType", propOrder = {
    "reuseDocsBatchSpacer",
    "numberOfDocsInBatch",
    "numberOfPagesInBatch"
})
public class BatchSpacerValParamsType {

    @XmlSchemaType(name = "string")
    protected ReuseDocsBatchSpacerType reuseDocsBatchSpacer;
    protected BigInteger numberOfDocsInBatch;
    protected BigInteger numberOfPagesInBatch;

    /**
     * Gets the value of the reuseDocsBatchSpacer property.
     * 
     * @return
     *     possible object is
     *     {@link ReuseDocsBatchSpacerType }
     *     
     */
    public ReuseDocsBatchSpacerType getReuseDocsBatchSpacer() {
        return reuseDocsBatchSpacer;
    }

    /**
     * Sets the value of the reuseDocsBatchSpacer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReuseDocsBatchSpacerType }
     *     
     */
    public void setReuseDocsBatchSpacer(ReuseDocsBatchSpacerType value) {
        this.reuseDocsBatchSpacer = value;
    }

    /**
     * Gets the value of the numberOfDocsInBatch property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfDocsInBatch() {
        return numberOfDocsInBatch;
    }

    /**
     * Sets the value of the numberOfDocsInBatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfDocsInBatch(BigInteger value) {
        this.numberOfDocsInBatch = value;
    }

    /**
     * Gets the value of the numberOfPagesInBatch property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfPagesInBatch() {
        return numberOfPagesInBatch;
    }

    /**
     * Sets the value of the numberOfPagesInBatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfPagesInBatch(BigInteger value) {
        this.numberOfPagesInBatch = value;
    }

}
