
package es.ricoh.webscan.spacers.service.api.soap.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Carátula y separadores generados.
 * 
 * <p>Java class for SpacersResponseResParamType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpacersResponseResParamType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="spacers" type="{urn:es:gv:dgti:webscan:services:spacersGeneration:messages:1.1.1}SpacerType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpacersResponseResParamType", propOrder = {
    "spacers"
})
public class SpacersResponseResParamType {

    @XmlElement(required = true)
    protected SpacerType spacers;

    /**
     * Gets the value of the spacers property.
     * 
     * @return
     *     possible object is
     *     {@link SpacerType }
     *     
     */
    public SpacerType getSpacers() {
        return spacers;
    }

    /**
     * Sets the value of the spacers property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpacerType }
     *     
     */
    public void setSpacers(SpacerType value) {
        this.spacers = value;
    }

}
