package es.ricoh.webscan.genericstore.integration.storage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.GenericStoreException;
import es.ricoh.webscan.genericstore.integration.GenericStorage;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.plugins.PluginException;

/**
 * Clase encargada de realizar el depósito de documentos via FTP.
 * <p>
 * Clase FTPStorage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */
public class FTPStorage extends AbstractStorage implements GenericStorage {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(FTPStorage.class);

	private String excMsg = new String();

	/**
	 * Método encargado de realizar el depósito del documento, su firma y
	 * metadatos por FTP
	 * 
	 * @param configuration
	 *            datos de los parámetros del plugin.
	 * @param doc
	 *            documento digitalizado
	 * @param metadataCollection
	 *            colección de metadatos del documento
	 * @throws PluginException
	 */
	@Override
	public void depositDocument(Properties configuration, ScannedDocumentDTO doc, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) throws PluginException {
		FTPClient client = null;
		String remotePath=null;
		String xmlMetadataCollection;

		try {
			// Conexión al servidor FTP
			client = new FTPClient();
			client.connect(configuration.getProperty(HOST_CONNECTION), Integer.parseInt(configuration.getProperty(PORT_CONNECTION)));

			//Logamos en FTP, sólo si hemos informado usuario
			if (configuration.getProperty(USER_CONNECTION) != null || !configuration.getProperty(USER_CONNECTION).isEmpty()) {
				client.login(configuration.getProperty(USER_CONNECTION), configuration.getProperty(PASSWORD_CONNECTION));				
			}
			client.enterLocalPassiveMode();
			client.setFileType(FTP.BINARY_FILE_TYPE);

			// Se calculan los metadatos
			xmlMetadataCollection = generateXMLMetadata(metadataCollection);
			
			// Se monta la ruta donde se va a realizar el depósito
			remotePath = getRemotePath(configuration, doc, metadataCollection);
			makeDirectories(client, remotePath);

			//Contenido del documento
			writeFile(client, remotePath, doc.getName() + doc.getMimeType().getExtension(), doc.getContent());
			
			// Firma electrónica.
			if (doc.getSignature() != null) {
				writeFile(client, remotePath, doc.getName() + ".esig", doc.getSignature());
			}

			// Metadatos
			writeFile(client, remotePath, doc.getName() + ".xml", xmlMetadataCollection.getBytes(StandardCharsets.UTF_8));
			client.disconnect();
			LOGGER.info("[WEBSCAN-GENERIC-MOVE] Depósito en servidor FTP realizado correctamente para el documento {}", doc.getId());
		} catch(PluginException e) { 
			throw e;
		} catch (Exception e) {
			excMsg = "El depósito del documento no pudo ser completado, por un error en la conexión FTP: " + e.getMessage();
			deleteDocDir(client,remotePath);
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(excMsg, e);
		} finally {
			try {
				// Cierre de la conexión
				if(client != null && client.isConnected()) {
					//Si nos hemos logado, hay que hacer logout
					if (configuration.getProperty(USER_CONNECTION) != null || !configuration.getProperty(USER_CONNECTION).isEmpty()) {
						client.logout();
					}
					client.disconnect();
				}
			} catch(IOException e) {
				//Nohacer nada
			}
		}

	}

	/**
	 * Método encargado de validar que los parámetros necesarios están
	 * informados
	 * 
	 * @param configuration
	 *            datos de los parámetros del plugin.
	 * @throws PluginException
	 */
	@Override
	public void validateParameters(Properties configuration) throws PluginException {
		if (configuration.getProperty(HOST_CONNECTION) == null) {
			excMsg = "Es necesario que se informe un servidor de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_615, excMsg);
		}
		if (configuration.getProperty(PORT_CONNECTION) == null) {
			excMsg = "Es necesario que se informe un puerto de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_615, excMsg);
		}
		if (configuration.getProperty(USER_CONNECTION) != null && configuration.getProperty(PASSWORD_CONNECTION) == null ) {
			excMsg = "Es necesario que se informe la password de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_615, excMsg);
		}
		if (configuration.getProperty(USER_CONNECTION) == null && configuration.getProperty(PASSWORD_CONNECTION) != null ) {
			excMsg = "Es necesario que se informe el usuario de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_615, excMsg);
		}
	}

	/**
	 * Método encargado de crear la estructura de directorios del path
	 * 
	 * @param client
	 *            datos de la conexión FTP
	 * @parma dirPath
	 * 				cadena donde se indica la ruta del path
	 * @throws PluginException, IOException
	 */
	public static void makeDirectories(FTPClient ftpClient, String dirPath)
            throws PluginException, IOException {
		try {
	        String[] pathElements = dirPath.split("/");
	        
	        if (pathElements != null && pathElements.length > 0) {
	            for (String singleDir : pathElements) {
	                boolean existed = ftpClient.changeWorkingDirectory(singleDir);
	                if (!existed) {
	                    boolean created = ftpClient.makeDirectory(singleDir);
	                    if (created) {
	                        System.out.println("CREATED directory: " + singleDir);
	                        ftpClient.changeWorkingDirectory(singleDir);
	                    } else {
	                        System.out.println("COULD NOT create directory: " + singleDir);
	                        throw new PluginException();
	                    }
	                }
	            }
	        }
		} catch(IOException e) {
			throw new PluginException();
			
		}
    }
	
	/**
	 * Método encargado de escribir el fichero en destion
	 * 
	 * @param client
	 *          datos de la conexión FTP
	 * @parma parentDir
	 * 			cadena donde se indica la ruta del path donde se realiza el depósito
	 * @param fileName
	 * 			cadena que contiene el nombre del fichero que se deposita
	 * @param fileContent
	 * 			contiene el contenido del fichero
	 * @throws PluginException
	 */
	private void writeFile(FTPClient ftpClient, String parentDir, String fileName, byte[ ] fileContent) throws PluginException {
		String absoluteFileName = parentDir + fileName;
		

		try (ByteArrayInputStream bais = new ByteArrayInputStream(fileContent)) {
			ftpClient.storeFile(absoluteFileName, bais);
		} catch (IOException e) {
			String excMsg = "Error en la escritura del fichero.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_616, "Error: " +  e.getMessage());
		}
	}
	/**
	 * Método encargado de eliminar la ruta en caso de error
	 * 
	 * @param docParentDir
	 * 		path donde se han intentado depositar los documetos
	 * @parma docName
	 * 		nombre del documento que se ha intentado depositar
	 * @throws PluginException
	 */
	private void deleteDocDir(FTPClient client, String dirPath) {
		String fileName;
		try {
			for(FTPFile ftpFile: client.listFiles()) {
				fileName=ftpFile.getName();
				client.deleteFile(dirPath + "\\" + fileName);
				
			}
			
			client.removeDirectory(dirPath);
			
		} catch (Exception e) {
			// No hacer nada
		}
	}
	
}
