/** 
* <b>Archivo:</b><p>es.ricoh.webscan.genericstore.integration.GenericStorageOfDocs.java.</p>
* <b>Descripción:</b><p> Implementación del plugin de depósito de documentos mediante distintos protocolos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.genericstore.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.plugins.PluginDataModelUtilities;
import es.ricoh.webscan.plugins.PluginException;
import es.ricoh.webscan.plugins.deposit.DepositPlugin;

/**
 * Implementación del plugin de depósito de documentos mediante distintos protocolos.
 * <p>
 * Clase GenericStorageOfDocs.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class GenericStorageOfDocs implements DepositPlugin {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(GenericStorageOfDocs.class);

	/**
	 * Nombre del parámetro del plugin que especifica el protocolo por el cual 
	 * vamos a conectarnos al destino
	 */
	private static final String DEPOSIT_PROTOCOL_NAME = "protocol.name";

	/**
	 * Nombre del parámetro del plugin que especifica el path de destino.
	 */
	private static final String DEPOSIT_PATH_NAME = "path.root";

	/**
	 * Relación de parámetros de configuración requeridos para el plugin.
	 */
	private static final List<String> REQUIRED_CONF_PARAMS = new ArrayList<String>();

	/**
	 * Utilidades de acceso al modelo de datos del Sistema para plugins.
	 */
	private PluginDataModelUtilities pluginDataModelUtilities;

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos base de Webscan.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Indica si el plugin ha sido configurado para realizar la operación que
	 * implementa.
	 */
	private boolean initialized = Boolean.FALSE;

	/**
	 * Lista de documentos a trasladar al sistema de información responsable de
	 * su gestión.
	 */
	private List<Long> docs = null;

	/**
	 * Parámetros de configuración del plugin.
	 */
	private Properties configuration;

	private GenericStorage genericStorage;
	
	static {
		REQUIRED_CONF_PARAMS.add(DEPOSIT_PROTOCOL_NAME);
		REQUIRED_CONF_PARAMS.add(DEPOSIT_PATH_NAME);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.plugins.Plugin#performAction()
	 */
	@Override
	public void performAction() throws PluginException {
		Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection;
		ScannedDocumentDTO doc;
		String excMsg;
		
		LOGGER.debug("[WEBSCAN-GENERIC-MOVE] Se inicia la ejecución del plugin para el depósito de los documentos {}", (docs != null ? docs : "No inicializado"));

		if (!initialized) {
			excMsg = "Plugin de depósito genérico de documentos no inicializado.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(PluginException.CODE_603, excMsg);
		}

		try {
			LOGGER.debug("[WEBSCAN-GENERIC-MOVE] Obteniendo conexión mediante protocolo {}.", configuration.getProperty(DEPOSIT_PROTOCOL_NAME));
			
			for (Long docId: docs) {
					doc = webscanModelManager.getDocument(docId);
					metadataCollection = webscanModelManager.getDocumentMetadataCollection(doc.getId());
					LOGGER.debug("[WEBSCAN-GENERIC-MOVE] Se ejecuta el procedimiento almacenado para el traslado del documento {}.", doc.getId());
					genericStorage.depositDocument(configuration, doc, metadataCollection);
					LOGGER.debug("[WEBSCAN-GENERIC-MOVE] Documento {} depositado.", doc.getId());
			}

			LOGGER.debug("[WEBSCAN-GENERIC-MOVE] Documentos depositados.");				
			
		} catch (DAOException e) {
			excMsg = "El depósito del documento no pudo ser completado, por un error en el acceso al modelo de datos del sistema: " + e.getMessage();
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(PluginException.CODE_605, excMsg, e);
		} catch (PluginException e) {
			throw e;
		} finally {
			initialized = Boolean.FALSE;
		}
	}
	

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.plugins.Plugin#initialize(java.lang.Long,
	 *      java.lang.Long)
	 */
	@Override
	public void initialize(Long scanProcessStageId, Long docId) throws PluginException {
		// Se recuperan los parámetros de configuración registrados en el
		// modelo.
		configuration = pluginDataModelUtilities.getPluginConfParams(scanProcessStageId, docId);

		validateConfParameters();

		genericStorage = StorageFactory.getGenericStorage(configuration.getProperty(DEPOSIT_PROTOCOL_NAME));
		
		genericStorage.validateParameters(configuration);
		
		// Se cumplimenta la lista de documentos a depositar
		if (docId == null) {
			String excMsg = "El depósito de los documentos no pudo ser completado. No se especificaron los identificadores de los documentos a trasladar.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(PluginException.CODE_605, excMsg);
		}

		docs = new ArrayList<Long>();
		docs.add(docId);

		initialized = Boolean.TRUE;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.plugins.Plugin#initialize(java.lang.Long,
	 *      java.util.List)
	 */
	@Override
	public void initialize(Long scanProcessStageId, List<Long> docIds) throws PluginException {
		// Se recuperan los parámetros de configuración registrados en el
		// modelo.
		configuration = pluginDataModelUtilities.getPluginConfParams(scanProcessStageId, docIds);

		validateConfParameters();

		genericStorage = StorageFactory.getGenericStorage(configuration.getProperty(DEPOSIT_PROTOCOL_NAME));
		
		genericStorage.validateParameters(configuration);
		
		// Se cumplimenta la lista de documentos a depositar
		if (docIds == null || docIds.isEmpty()) {
			String excMsg = "El depósito de los documentos no pudo ser completado. No se especificaron los identificadores de los documentos a trasladar.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(PluginException.CODE_605, excMsg);
		}

		docs = new ArrayList<Long>();
		docs.addAll(docIds);

		initialized = Boolean.TRUE;
	}

	/**
	 * Valida que los parámetros de configuración míninos requeridos hasyan sido
	 * establecidos.
	 * 
	 * @throws PluginException
	 *             Si algún parámetro requerido no ha sido establecido.
	 */
	private void validateConfParameters() throws PluginException {
		for (String confParam: REQUIRED_CONF_PARAMS) {
			if (configuration.get(confParam) == null) {
				configuration.clear();
				throw new PluginException(PluginException.CODE_604, "[WEBSCAN-GENERIC-MOVE] El parámetro de configuración " + confParam + " no ha sido establecido.");
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.plugins.Plugin#getConfiguration()
	 */
	@Override

	public Properties getConfiguration() {
		return configuration;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.plugins.deposit.DepositPlugin#getDepositPath()
	 */
	@Override
	public String getDepositPath() {
		return configuration.getProperty(DEPOSIT_PATH_NAME);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.plugins.deposit.DepositPlugin#getDocument(java.lang.Long)
	 */
	@Override
	public byte[ ] getDocument(Long docId) throws PluginException {
		byte[ ] res = null;
		ScannedDocumentDTO doc;
		try {
			doc = webscanModelManager.getDocument(docId);

			res = doc.getContent();
		} catch (DAOException e) {
			String excMsg = "Se produjo un error recuperando el contenido del documento" + docId + ": " + e.getMessage();
			throw new PluginException(PluginException.CODE_605, excMsg, e);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.plugins.deposit.DepositPlugin#getSignature(java.lang.Long)
	 */
	@Override
	public byte[ ] getSignature(Long docId) throws PluginException {
		byte[ ] res = null;
		ScannedDocumentDTO doc;
		try {
			doc = webscanModelManager.getDocument(docId);
			if (doc.getSignatureType().getFamily().equals("")) {
				res = doc.getContent();
			} else {
				res = doc.getSignature();
			}

		} catch (DAOException e) {
			String excMsg = "Se produjo un error recuperando la firma electrónica del documento" + docId + ": " + e.getMessage();
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(PluginException.CODE_605, excMsg, e);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.plugins.deposit.DepositPlugin#getMetadata(java.lang.Long)
	 */
	@Override
	public Map<String, String> getMetadata(Long docId) throws PluginException {
		Map<String, String> res = new HashMap<String, String>();
		Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection;
		MetadataSpecificationDTO msDto;

		try {
			metadataCollection = webscanModelManager.getDocumentMetadataCollection(docId);

			for (Iterator<MetadataSpecificationDTO> it = metadataCollection.keySet().iterator(); it.hasNext();) {
				msDto = it.next();
				res.put(msDto.getName(), metadataCollection.get(msDto).getValue());
			}
		} catch (DAOException e) {
			String excMsg = "Se produjo un error recuperando los metadatos del documento" + docId + ": " + e.getMessage();
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(PluginException.CODE_605, excMsg, e);
		}

		return res;
	}

	/**
	 * Establece la clase de utilidades para plugins sobre el modelo de datos
	 * del Sistema.
	 * 
	 * @param aPluginDataModelUtilities
	 *            utilidades para plugins sobre el modelo de datos del Sistema.
	 */
	public void setPluginDataModelUtilities(PluginDataModelUtilities aPluginDataModelUtilities) {
		this.pluginDataModelUtilities = aPluginDataModelUtilities;
	}

	/**
	 * Establece un nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}
}
