package es.ricoh.webscan.genericstore.integration.storage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.GenericStoreException;
import es.ricoh.webscan.genericstore.integration.GenericStorage;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.plugins.PluginException;
import jcifs.CIFSContext;
import jcifs.CIFSException;
import jcifs.DialectVersion;
import jcifs.context.SingletonContext;
import jcifs.smb.NtlmPasswordAuthenticator;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

/**
 * Clase encargada de realizar el depósito de documentos via CIFS.
 * <p>
 * Clase CIFSStorage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */
public class CIFSStorage extends AbstractStorage implements GenericStorage {
	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(CIFSStorage.class);
	
	/**
	 * Método encargado de realizar el depósito del documento, su firma y
	 * metadatos
	 * 
	 * @param configuration
	 *            datos de los parámetros del plugin.
	 * @param doc
	 *            documento digitalizado
	 * @param metadataCollection
	 *            colección de metadatos del documento
	 * @throws PluginException
	 */
	@Override
	public void depositDocument(Properties configuration, ScannedDocumentDTO doc, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) throws PluginException {
		
		String url= "smb://";
		String xmlMetadataCollection;
		SmbFile sFile = null;

		Properties cifsProps = new Properties();
		cifsProps.setProperty("jcifs.smb.client.maxVersion",DialectVersion.SMB311.name());
		
		try {	
			CIFSContext cifsContext;
			SingletonContext.init(cifsProps);
			//CIFSContext cifsContext = new BaseContext(config);
			if (configuration.getProperty(DOMAIN_CONNECTION) != null) {
				cifsContext = SingletonContext.getInstance().withCredentials(new NtlmPasswordAuthenticator(configuration.getProperty(DOMAIN_CONNECTION),configuration.getProperty(USER_CONNECTION),
					configuration.getProperty(PASSWORD_CONNECTION)));
			} else {
				cifsContext = SingletonContext.getInstance().withCredentials(new NtlmPasswordAuthenticator("",configuration.getProperty(USER_CONNECTION),
					configuration.getProperty(PASSWORD_CONNECTION)));
			}
			
			// Se calculan los metadatos
			xmlMetadataCollection = generateXMLMetadata(metadataCollection);
			
			url += configuration.getProperty(HOST_CONNECTION) + getRemotePath(configuration, doc, metadataCollection);
			
			sFile = new SmbFile(url,cifsContext);
			
			if (!sFile.exists()) {
				sFile.mkdirs();
			}
			
			// Contenido del documento
			writeSmbFile(sFile, doc.getName() + doc.getMimeType().getExtension(), doc.getContent(), cifsContext);
			
			// Firma electrónica.
			if (doc.getSignature() != null) {
				writeSmbFile(sFile, doc.getName() + ".esig", doc.getSignature(), cifsContext);
			}

			// Metadatos
			writeSmbFile(sFile, doc.getName() + ".xml", xmlMetadataCollection.getBytes(StandardCharsets.UTF_8), cifsContext);
			
			LOGGER.info("[WEBSCAN-GENERIC-MOVE] Depósito en carpeta corporativa CIFS realizado correctamente para el documento {}", doc.getId());
			sFile.close();		
		}  catch (CIFSException e) {
			String excMsg = "El depósito del documento no pudo ser completado. No se pudo conectar al servidor CIFS: " + e.getMessage();
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			deleteDocDir(sFile);
			throw new GenericStoreException(GenericStoreException.CODE_680, excMsg);
		}catch (MalformedURLException e) {
			String excMsg = "El depósito del documento no pudo ser completado. La URL de conexión a CIFS no es correcta: " + e.getMessage();
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_680, excMsg);
		}
		
	}

	/**
	 * Método encargado de validar que los parámetros necesarios están
	 * informados
	 * 
	 * @param configuration
	 *            datos de los parámetros del plugin.
	 * @throws PluginException
	 */
	@Override
	public void validateParameters(Properties configuration) throws PluginException {
		String excMsg;
		if (configuration.getProperty(HOST_CONNECTION) == null) {
			excMsg = "Es necesario que se informe un servidor de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_615, excMsg);
		}

		if (configuration.getProperty(PASSWORD_CONNECTION) == null ) {
			excMsg = "Es necesario que se informe la password de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_615, excMsg);
		}

		if (configuration.getProperty(USER_CONNECTION) == null ) {
			excMsg = "Es necesario que se informe el usuario de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_615, excMsg);
		}
	}

	/**
	 * Método encargado de escribir el fichero en destion
	 * 
	 * @param parentDir
	 *          ruta del servidor SAMBA donde se ubicarán los ficheros
	 * @param fileName
	 * 			cadena que contiene el nombre del fichero que se deposita
	 * @param fileContent
	 * 			contiene el contenido del fichero
	 * @param auth
	 * 			contenido de los parámetros de logado (usuario y passowrd)
	 * @throws PluginException
	 */
	private void writeSmbFile(SmbFile parentDir, String fileName, byte[ ] fileContent, CIFSContext cifsContext) throws PluginException {
		String absoluteFileName = parentDir.getPath() + fileName;
		SmbFile sFile=null;
		SmbFileOutputStream sfos;

		try {
			sFile = new SmbFile(absoluteFileName,cifsContext);
			
			sfos = new SmbFileOutputStream(sFile);
			sfos.write(fileContent);
		
			sfos.close();
			sFile.close();
		} catch (MalformedURLException | SmbException
				| UnknownHostException e) {
			String excMsg = "Error en la escritura del fichero.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_616, "Error: " +  e.getMessage());
		} catch (IOException e) {
			String excMsg = "Error en la escritura del fichero.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			deleteDocDir(sFile);
			throw new GenericStoreException(GenericStoreException.CODE_616, "Error: " +  e.getMessage());		
		}
	}

	/**
	 * Método encargado de eliminar la ruta en caso de error
	 * 
	 * @param docParentDir
	 * 		path donde se han intentado depositar los documetos
	 * @parma docName
	 * 		nombre del documento que se ha intentado depositar
	 * @throws PluginException
	 */
	private void deleteDocDir(SmbFile parentDir) throws PluginException{
		try {
			for (SmbFile remoteFile:parentDir.listFiles()) {
				remoteFile.delete();
			}
			parentDir.delete();
		} catch (Exception e) {
			//no hacer nada		
		} 
		
	}
}
