/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan;

import es.ricoh.webscan.plugins.PluginException;

/**
 * Excepción que encapsula los errores producidos en el componente proovedor de
 * plugins de Webscan.
 * <p>
 * Clase PluginException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.WebscanException
 * @version 2.0.
 */
public class GenericStoreException extends PluginException {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Error en el parámetro de protocolo de conexión para el 
	 * plugin de depósito genérico de documentos.
	 */
	public static final String CODE_612 = "COD_612";

	/**
	 * Error al construir el fichero de metadatos del documento. 
	 */
	public static final String CODE_613 = "COD_613";

	/**
	 * Error al crear la ruta de directorios de depósito. 
	 */
	public static final String CODE_614 = "COD_614";

	/**
	 * Error al validar los parámetros del protocolo. 
	 */
	public static final String CODE_615 = "COD_615";

	/**
	 * Error al escribir el fichero. 
	 */
	public static final String CODE_616 = "COD_616";

	/**
	 * Error al eliminar el fichero. 
	 */
	public static final String CODE_617 = "COD_617";

	/**
	 * Error de configuración de ruta relativa. 
	 */
	public static final String CODE_618 = "COD_618";
	
	/**
	 * Error de acceso a la capa de datos. 
	 */
	public static final String CODE_619 = "COD_619";

	/**
	 * No fue posible depositar un documento.
	 */
	public static final String CODE_680 = "COD_680";

	/**
	 * Constructor sin argumentos.
	 */
	public GenericStoreException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public GenericStoreException(String aCode, String aMessage) {
		super(aCode, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public GenericStoreException(String aMessage) {
		super(CODE_999, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public GenericStoreException(Throwable aCause) {
		super(CODE_999, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public GenericStoreException(final String aMessage, Throwable aCause) {
		super(CODE_999, aMessage, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public GenericStoreException(final String aCode, final String aMessage, Throwable aCause) {
		super(aCode, aMessage, aCause);
	}

}
