package es.ricoh.webscan.genericstore.integration.storage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

import es.ricoh.webscan.GenericStoreException;
import es.ricoh.webscan.genericstore.integration.GenericStorage;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.plugins.PluginException;

/**
 * Clase encargada de realizar el depósito de documentos via SFTP.
 * <p>
 * Clase SFTPStorage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */
public class SFTPStorage extends AbstractStorage implements GenericStorage {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SFTPStorage.class);
	/**
	 *
	 */
	private String excMsg = new String();

	/**
	 * Método encargado de realizar el depósito del documento, su firma y
	 * metadatos por SFTP.
	 * 
	 * @param configuration
	 *            datos de los parámetros del plugin.
	 * @param doc
	 *            documento digitalizado
	 * @param metadataCollection
	 *            colección de metadatos del documento
	 * @throws PluginException
	 */
	@Override
	public void depositDocument(Properties configuration, ScannedDocumentDTO doc, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) throws PluginException {

		String xmlMetadataCollection;
		String remotePath = new String();
		
		Session session = null;
		Channel channel = null;
		ChannelSftp sftp = null;
		
		try {
			// Se calculan los metadatos
			xmlMetadataCollection = generateXMLMetadata(metadataCollection);
			
			final JSch ssh = new JSch();
			Properties config = new Properties();
			
			session = ssh.getSession(configuration.getProperty(USER_CONNECTION), configuration.getProperty(HOST_CONNECTION), Integer.parseInt(configuration.getProperty(PORT_CONNECTION)));
			session.setPassword(configuration.getProperty(PASSWORD_CONNECTION));

			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();

			sftp = (ChannelSftp) channel;
			
			LOGGER.debug("[WEBSCAN-GENERIC-MOVE] Conexión sftp establecida en el host", configuration.getProperty(HOST_CONNECTION));
			
			// Se monta la ruta donde se va a realizar el depósito
			remotePath = getRemotePath(configuration, doc, metadataCollection);
			makeDirectories(sftp, remotePath);
			
			//Contenido del documento
			writeFile(sftp, remotePath, doc.getName() + doc.getMimeType().getExtension(), doc.getContent());
			
			// Firma electrónica.
			if (doc.getSignature() != null) {
				writeFile(sftp, remotePath, doc.getName() + ".esig", doc.getSignature());
			}

			// Metadatos
			writeFile(sftp, remotePath, doc.getName() + ".xml", xmlMetadataCollection.getBytes(StandardCharsets.UTF_8));
			
			LOGGER.info("[WEBSCAN-GENERIC-MOVE] Depósito en servidor FTP realizado correctamente para el documento {}", doc.getId());
			
		} catch (Exception e) {
			excMsg = "El depósito del documento no pudo ser completado, por un error en la conexión SFTP: " + e.getMessage();
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			deleteDocDir(sftp,remotePath);
			throw new PluginException(excMsg, e);
		} finally {
			if (channel != null) {
				channel.disconnect();
			}
			if (session != null) {
				session.disconnect();
			}
		}
	}
	/**
	 * Método encargado de validar que los parámetros necesarios están
	 * informados
	 * 
	 * @param configuration
	 *            datos de los parámetros del plugin.
	 * @throws PluginException
	 */
	public void validateParameters(Properties configuration) throws PluginException {
		if (configuration.getProperty(HOST_CONNECTION) == null) {
			excMsg = "Es necesario que se informe un servidor de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(excMsg);
		}
		if (configuration.getProperty(PORT_CONNECTION) == null) {
			excMsg = "Es necesario que se informe un puerto de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(excMsg);
		}
		if (configuration.getProperty(USER_CONNECTION) == null) {
			excMsg = "Es necesario que se informe un usuario de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(excMsg);
		}
		if (configuration.getProperty(PASSWORD_CONNECTION) == null) {
			excMsg = "Es necesario que se informe una password de conexión.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new PluginException(excMsg);
		}
	}

	/**
	 * Método encargado de escribir el fichero en destion
	 * 
	 * @param client
	 *          datos de la conexión FTP
	 * @parma parentDir
	 * 			cadena donde se indica la ruta del path donde se realiza el depósito
	 * @param fileName
	 * 			cadena que contiene el nombre del fichero que se deposita
	 * @param fileContent
	 * 			contiene el contenido del fichero
	 * @throws PluginException
	 */
	private void writeFile(ChannelSftp sftp, String parentDir, String fileName, byte[ ] fileContent) throws PluginException {
		
		//String absoluteFileName = fileName;

		try (ByteArrayInputStream bais = new ByteArrayInputStream(fileContent)) {
			
			sftp.put(bais,fileName);
		} catch (IOException e) {
			String excMsg = "Error en la escritura del fichero.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_616, "Error: " +  e.getMessage());
		} catch (SftpException e) {
			String excMsg = "Error en la escritura del fichero.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_616, "Error: " +  e.getMessage());
		}
	}
	
	/**
	 * Método encargado de crear la estructura de directorios del path
	 * 
	 * @param sftp
	 *            datos de la conexión sftp
	 * @parma dirPath
	 * 				cadena donde se indica la ruta del path
	 * @throws PluginException, IOException
	 */
	public static void makeDirectories(ChannelSftp sftp, String dirPath)
            throws PluginException, IOException {
		
		SftpATTRS attrs = null;

		try {
			for(String dir : dirPath.split("/")) {
				
				if (!dir.equals("")) {
					try {
						attrs = sftp.stat(dir);
					}catch (SftpException e){
						if(e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
							sftp.mkdir(dir);
							System.out.println("No existe el directorio: " + dir);
						}else {
							String excMsg = "Error en la estructura de directorios.";
							LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
							throw new GenericStoreException(GenericStoreException.CODE_616, "Error: " +  e.getMessage());
						}
					}
					sftp.cd(dir);
				}
			}
		}catch(SftpException e) {
			String excMsg = "Error en la creación estructura de directorios.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_616, "Error: " +  e.getMessage());
		}
    }
	/**
	 * Método encargado de eliminar los ficheros depositados en caso de error
	 * 
	 * @param docParentDir
	 * 		path donde se han intentado depositar los documetos
	 * @parma docName
	 * 		nombre del documento que se ha intentado depositar
	 * @throws PluginException
	 */
	private void deleteDocDir(ChannelSftp sftp, String remotePath) {


		try {
			Vector ls = sftp.ls(remotePath);
			Vector<ChannelSftp.LsEntry> dirList = ls;
			
			for(ChannelSftp.LsEntry entry : dirList) {
				if(!(entry.getFilename().equals(".") || entry.getFilename().equals("..")))
				{
					if(!entry.getAttrs().isDir())
					{
						sftp.rm(remotePath + "/" + entry.getFilename());
					}
				}
			}
			sftp.rmdir(remotePath);
		} catch (Exception e) {
			// No hacer nada
		}

	}
}
