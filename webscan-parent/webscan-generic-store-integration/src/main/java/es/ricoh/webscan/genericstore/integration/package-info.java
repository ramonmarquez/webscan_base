/**
 * Paquete base del plugin de depósito en BBD de la solución de digitalización
 * certificada y copia auténtica de la Generalitat Valenciana.
 */
package es.ricoh.webscan.genericstore.integration;