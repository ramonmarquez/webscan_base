package es.ricoh.webscan.genericstore.integration;

import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.plugins.PluginException;

public interface GenericStorage {
	
	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	public static final Logger LOGGER = LoggerFactory.getLogger(GenericStorage.class);
	
	/**
	 * Nombre del parámetro del plugin que especifica el protocolo por el cual 
	 * vamos a conectarnos al destino
	 */
	public static final String DEPOSIT_PROTOCOL_NAME = "protocol.name";

	/**
	 * Nombre del parámetro del plugin que especifica el path de destino.
	 */
	public static final String DEPOSIT_PATH_NAME = "path.root";
	
	/**
	 * Nombre del parámetro del plugin que especifica el path de destino.
	 */
	public static final String DEPOSIT_RELATIVE_PATH = "path.relative";

	/**
	 * Nombre del parámetro del plugin que especifica la conexión del servidor.
	 */
	public static final String HOST_CONNECTION = "conn.host";
	/**
	 * Nombre del parámetro del plugin que especifica el puerto de conexión.
	 */
	public static final String PORT_CONNECTION = "conn.port";
	
	/**
	 * Nombre del parámetro del plugin que especifica el usuario de conexión.
	 */
	public static final String KEY_FILENAME_CONNECTION = "conn.key.filename";
	
	/**
	 * Nombre del parámetro del plugin que especifica el usuario de conexión.
	 */
	public static final String USER_CONNECTION = "conn.auth.user";
	
	/**
	 * Nombre del parámetro del plugin que especifica el usuario de conexión.
	 */
	public static final String PASSWORD_CONNECTION = "conn.auth.password";
	
	/**
	 * Nombre del parámetro del plugin que especifica el usuario de conexión.
	 */
	public static final String DOMAIN_CONNECTION = "conn.auth.domain";
	
		//TODO - Comentar
	void depositDocument(Properties configuration,ScannedDocumentDTO doc,Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) throws PluginException;
	
	void validateParameters(Properties configuration) throws PluginException;
	
}
