package es.ricoh.webscan.genericstore.integration.storage;

import java.io.StringWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.GenericStoreException;
import es.ricoh.webscan.genericstore.integration.GenericStorage;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.dto.BatchDTO;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.plugins.PluginException;
import es.ricoh.webscan.plugins.deposit.xml.metadata.MetadataCollectionType;
import es.ricoh.webscan.plugins.deposit.xml.metadata.MetadataType;
import es.ricoh.webscan.plugins.deposit.xml.metadata.ObjectFactory;

/**
 * Clase que encapsula los métodos comunes del depósito genérico de documentos.
 * <p>
 * Clase AbstractStorage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */

public abstract class AbstractStorage implements GenericStorage {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractStorage.class);
	
	//TODO - Documentar código
	
	private static JAXBContext JAXB_CONTEXT;

	private static Marshaller MARSHALLER;

	private static ObjectFactory OBJECT_FACTORY = new ObjectFactory();
	/**
	 * Objeto que representa el acceso a los servicio del modelo.
	 */
	private WebscanModelManager webscanModelManager;


	public abstract void depositDocument(Properties configuration, ScannedDocumentDTO doc, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) throws PluginException;


	static {
		try {
			JAXB_CONTEXT = JAXBContext.newInstance(MetadataCollectionType.class);
			MARSHALLER = JAXB_CONTEXT.createMarshaller();
			MARSHALLER.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			MARSHALLER.setProperty(Marshaller.JAXB_ENCODING,"UTF-8");
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
	}

	/**
	 * Devuelve el path donde se depositará el documento digitalizado
	 * 
	 * @param pluginConfiguration
	 *            Parámetros de configuración del plugin.
	 * @param doc Documento digitalizado.
	 * @param metadataCollection Relación de metadatos del documento.
	 * @return Ruta del directorio padre que almacenará los diferentes recursos
	 *         del documento.
	 * @throws PluginException
	 *             TODO - Completar
	 */
	protected String getRemotePath(Properties pluginConfiguration,ScannedDocumentDTO doc, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) throws PluginException {
		String res;
		String directory;
		String pathRelative;
		String excMsg;

		res = pluginConfiguration.getProperty(DEPOSIT_PATH_NAME) + "/";

		pathRelative = pluginConfiguration.getProperty(DEPOSIT_RELATIVE_PATH);
		if (pathRelative != null && !pathRelative.isEmpty()) {
			String[ ] pathDirectories = pathRelative.split("[${}]");
			
			for (String rPath : pathDirectories) {
				if (!rPath.equals("")) {
					
					directory = timeParameters(rPath);
					
					if (rPath.startsWith("doc.")) {
						String docParam = rPath.substring("doc.".length());
						try {
							directory = documentParameters(docParam,doc);
						}catch(DAOException e) {
							excMsg = "El protocolo de conexión para el depósito genérico no está informado en la configuración.";
							LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
							throw new PluginException(GenericStoreException.CODE_619, "Error: " + e.getCode() + " - " + e.getMessage());
						}

					} else if (rPath.startsWith("metadata.")){
						String metadataName = rPath.substring("metadata.".length());

						directory = metadataParameters(metadataName, metadataCollection);
						
					} else if (rPath.equals("/") || rPath.equals("\\")) {
						res += "/";
					}
					if(directory != null) {
						res += directory;
					}	
										
				}
			}
		}
		
		return res;
	}

	/**
	 * Devuelve la carpeta correspondiente al parámetro relativo a la fecha del relativePath que hay que
	 * añadir a la ruta de destino.
	 * 
	 * @param path Parametro para decodificar el directorio.
	 * 
	 * @return Nombre de la carpeta que debe unirse a la ruta relativa
	 * .
	 * 
	 */
	protected String timeParameters (String path) {
		String res=null; 
		Calendar date = new GregorianCalendar();
		
		if (path.equals("YYYY")) {
			res = Integer.toString(date.get(Calendar.YEAR));
		} else if (path.equals("MM")) {
			res = Integer.toString(date.get(Calendar.MONTH) + 1);
		} else if (path.equals("DD")) {
			res = Integer.toString(date.get(Calendar.DAY_OF_MONTH));
		} else if (path.equals("MS")) {
			res = Long.toString(System.currentTimeMillis());
		}
		return res;
	}
	
	/**
	 * Devuelve la carpeta correspondiente al parámetro relativo al documento del relativePath que hay que
	 * añadir a la ruta de destino.
	 * 
	 * @param path Parametro para decodificar el directorio.
	 * 
	 * @param doc objeto con los datos del documento.
	 * 
	 * @return Nombre de la carpeta que debe unirse a la ruta relativa
	 * .
	 * @throws DAOException 
	 * 
	 */
	
	protected String documentParameters (String path, ScannedDocumentDTO doc) throws DAOException {
		String res=null; 
		BatchDTO batch;
		
		if (path.equals("id")) {
			res = doc.getId().toString();
		} else if (path.equals("name")) {
			res = doc.getName();
		} else if (path.equals("batchId")) {
			if(doc.getBatchId() != null) {
				batch = webscanModelManager.getBatch(doc.getBatchId().getId());
				res = batch.getBatchNameId();
			}
		}
		return res;
	}	
	
	/**
	 * Devuelve la carpeta correspondiente al parámetro relativo al metadato del documento del relativePath
	 * que hay que añadir a la ruta de destino.
	 * 
	 * @param metadataName Nombre parametrizado del metadato que se desea mostrar.
	 * 
	 * @param doc objeto con los datos del metadato documento.
	 * 
	 * @return Nombre de la carpeta que debe unirse a la ruta relativa
	 * .
	 * 
	 */
	protected String metadataParameters (String metadataName, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) {
		String res = null; 
		
		Optional<Entry<MetadataSpecificationDTO, MetadataDTO>> metadata = metadataCollection.entrySet().stream().filter(entry -> entry.getKey().getName().equalsIgnoreCase(metadataName)).findFirst();
		
		if(metadata.isPresent()) {
			res = metadata.get().getValue().getValue();
		} else {
			LOGGER.warn("[WEBSCAN-GENERIC-MOVE] No se incluye el metadato {} como parte d la ruta de depósito del documento {}.", metadataName);
		}
		
		return res;
	} 
	
	
	/**
	 * Devuelve el XML generado con los metadatos del documento
	 * 
	 * @param metadataCollection
	 *            metadatos del documento.
	 * @return String string con el contenido del XML generado.
	 */
	protected String generateXMLMetadata(Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) throws PluginException {
		List<MetadataType> xmlMetadataList;
		MetadataCollectionType xmlMetadataCollection;
		StringWriter fileXML = new StringWriter();
		String excMsg = new String();
		String res = null;

		try {
			if (metadataCollection != null) {

				xmlMetadataList = metadataCollection.entrySet().stream().map(e -> buildXmlMetadata(e.getKey(), e.getValue())).collect(Collectors.toList());
				xmlMetadataCollection = OBJECT_FACTORY.createMetadataCollectionType();
				xmlMetadataCollection.getMetadata().addAll(xmlMetadataList);

				MARSHALLER.marshal(OBJECT_FACTORY.createMetadataCollection(xmlMetadataCollection), fileXML);

				res = fileXML.toString();
			}
		} catch (JAXBException e) {
			excMsg = "El protocolo de conexión para el depósito genérico no está informado en la configuración.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_613, "Error: " + e.getErrorCode() + " - " + e.getMessage());
		}

		return res;
	}

	private MetadataType buildXmlMetadata(MetadataSpecificationDTO metSpec, MetadataDTO metadata) {
		MetadataType res;

		res = OBJECT_FACTORY.createMetadataType();
		res.setDescription(metSpec.getDescription());
		res.setName(metSpec.getName());
		res.setValue(metadata.getValue());

		return res;
	}
}
