package es.ricoh.webscan.genericstore.integration;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.GenericStoreException;
import es.ricoh.webscan.genericstore.integration.storage.CIFSStorage;
import es.ricoh.webscan.genericstore.integration.storage.FTPStorage;
import es.ricoh.webscan.genericstore.integration.storage.LocalStorage;
import es.ricoh.webscan.genericstore.integration.storage.SFTPStorage;
import es.ricoh.webscan.plugins.PluginException;
/**
 * Clase Factoria del plugin de depósito.
 * <p>
 * Clase StorageFactory.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */
public class StorageFactory {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StorageFactory.class);
	/**
	 * Nombre del parámetro del plugin que especifica el protocolo por el cual 
	 * vamos a conectarnos al destino
	 */
	private static final String FTP_PROTOCOL = "FTP";
	private static final String SFTP_PROTOCOL = "SFTP";
	private static final String CIFS_PROTOCOL = "CIFS";
	private static final String LOCAL_PROTOCOL = "LOCAL";
	

	public static GenericStorage getGenericStorage(String protocolConnection) throws PluginException {
		GenericStorage res;
		String excMsg = new String();;
		if (protocolConnection == null) {
			excMsg = "El protocolo de conexión para el depósito genérico no está informado en la configuración.";
			LOGGER.error("[WEBSCAN-GENERIC-STORAGE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_612, excMsg);
		}

		if (protocolConnection.equals(FTP_PROTOCOL)) {
			res = new FTPStorage();
		}else if (protocolConnection.equals(SFTP_PROTOCOL)) {
			res = new SFTPStorage();
		}else if (protocolConnection.equals(CIFS_PROTOCOL)) {
			res =  new CIFSStorage();
		} else if (protocolConnection.equals(LOCAL_PROTOCOL)) {
			res =  new LocalStorage();
		} else {
			excMsg = "El protocolo de conexión para el depósito genérico no es válido (FTP, LOCAL, SFTP, CIFS).";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_612, excMsg);
		}
		
		return res;
	}
}
