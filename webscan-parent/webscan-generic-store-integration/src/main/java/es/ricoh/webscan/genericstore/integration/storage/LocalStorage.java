package es.ricoh.webscan.genericstore.integration.storage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.GenericStoreException;
import es.ricoh.webscan.genericstore.integration.GenericStorage;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.plugins.PluginException;

/**
 * Clase encargada de realizar el depósito de documentos en carpeta local.
 * <p>
 * Clase LocalStorage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */
public class LocalStorage extends AbstractStorage implements GenericStorage {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(LocalStorage.class);
	
	/**
	 * Método encargado de realizar el depósito del documento, su firma y
	 * metadatos en carpeta local.
	 * 
	 * @param configuration
	 *            datos de los parámetros del plugin.
	 * @param doc
	 *            documento digitalizado
	 * @param metadataCollection
	 *            colección de metadatos del documento
	 * @throws PluginException
	 */
	@Override
	public void depositDocument(Properties configuration, ScannedDocumentDTO doc, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) throws PluginException {
		File docParentDir = null;
		String excMsg = new String();
		String xmlMetadataCollection;

		LOGGER.debug("[WEBSCAN-GENERIC-MOVE] Se inicia el depósito en el sistema de archivos local para el documento {}", doc.getId());

		try {
			// Se calculan los metadatos
			xmlMetadataCollection = generateXMLMetadata(metadataCollection);

			// Se genera la estructura de directorios que alojará el documento
			docParentDir = new File(getRemotePath(configuration, doc, metadataCollection));
			if (!docParentDir.exists()) {
				if (!docParentDir.mkdirs()) {
					excMsg = "No se ha podido crear la ruta de directorios.";
					LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
					throw new GenericStoreException(GenericStoreException.CODE_614, excMsg);
				}
			}

			// Se escribe en el sistema de archivos
			// Contenido del documento
			writeFile(docParentDir, doc.getName() + doc.getMimeType().getExtension(), doc.getContent());
			LOGGER.info("[WEBSCAN-GENERIC-MOVE] Depósito del documento en el sistema de archivos local realizado correctamente para el documento {}", doc.getId());
			
			// Firma electrónica.
			if (doc.getSignature() != null) {
				writeFile(docParentDir, doc.getName() + ".esig", doc.getSignature());
				LOGGER.info("[WEBSCAN-GENERIC-MOVE] Depósito de la firma en el sistema de archivos local realizado correctamente para el documento {}", doc.getId());
			}
			
			// Metadatos
			writeFile(docParentDir, doc.getName() + ".xml", xmlMetadataCollection.getBytes(StandardCharsets.UTF_8));
			LOGGER.info("[WEBSCAN-GENERIC-MOVE] Depósito del XML de metadatos en el sistema de archivos local realizado correctamente para el documento {}", doc.getId());

		} catch (PluginException e) {
			throw e;
		} catch (SecurityException e) {
			excMsg = "Error al depositar el documento " + doc.getId() + " en el sistema de archivos local: " + e.getMessage();
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			if (docParentDir != null && docParentDir.exists()) {
				deleteDocDir(docParentDir, doc.getName());
			}

			throw new GenericStoreException(GenericStoreException.CODE_680, excMsg);
		}
	}

	/**
	 * Método encargado de validar que los parámetros necesarios están
	 * informados
	 * 
	 * @param configuration
	 *            datos de los parámetros del plugin.
	 * @throws PluginException
	 */
	@Override
	public void validateParameters(Properties configuration) throws PluginException {
		if (configuration.getProperty(DEPOSIT_PATH_NAME) == null) {
			String excMsg = "Es necesario que se informe elparámetro path.root para el depósito Local";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_615, excMsg);
		}

	}

	/**
	 * Método encargado de escribir el fichero en destino
	 * 
	 * @parma parentDir
	 * 			cadena donde se indica la ruta del path donde se realiza el depósito
	 * @param fileName
	 * 			cadena que contiene el nombre del fichero que se deposita
	 * @param fileContent
	 * 			contiene el contenido del fichero
	 * @throws PluginException
	 */
	private void writeFile(File parentDir, String fileName, byte[ ] fileContent) throws PluginException {
		String absoluteFileName = parentDir.getPath() + "/" + fileName;
		FileOutputStream fos;

		try {
			
			fos = new FileOutputStream(absoluteFileName);
			fos.write(fileContent);
			fos.close();
		} catch (FileNotFoundException e) {
			String excMsg = "Error en la escritura del fichero, fichero no encontrado.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_616, "Error: " +  e.getMessage());
		} catch (IOException e) {
			String excMsg = "Error en la escritura del fichero.";
			LOGGER.error("[WEBSCAN-GENERIC-MOVE] {}", excMsg);
			throw new GenericStoreException(GenericStoreException.CODE_616, "Error: " +  e.getMessage());
		}
	}

	/**
	 * Método encargado de eliminar la ruta en caso de error
	 * 
	 * @param docParentDir
	 * 		path donde se han intentado depositar los documetos
	 * @parma docName
	 * 		nombre del documento que se ha intentado depositar
	 * @throws PluginException
	 */
	private void deleteDocDir(File docParentDir, String docName) {
		try (Stream<Path> walk = Files.walk(docParentDir.toPath(), 1)) {
			walk.sorted(Comparator.reverseOrder()).map(Path::toFile).filter(file -> file.isFile() && file.getName().startsWith(docName)).forEach(File::delete);
			docParentDir.delete();
		} catch (IOException e) {
			// No hacer nada
		} catch (Exception e) {
			// No hacer nada
		}

	}
}
