//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.09.12 a las 12:07:52 PM CEST 
//


package es.ricoh.webscan.plugins.deposit.xml.metadata;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the es.ricoh.webscan.plugins.deposit.xml.metadata package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MetadataCollection_QNAME = new QName("urn:es:ricoh:webscan:plugins:deposit:xml:metadata:1.0.0", "metadataCollection");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: es.ricoh.webscan.plugins.deposit.xml.metadata
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MetadataCollectionType }
     * 
     */
    public MetadataCollectionType createMetadataCollectionType() {
        return new MetadataCollectionType();
    }

    /**
     * Create an instance of {@link MetadataType }
     * 
     */
    public MetadataType createMetadataType() {
        return new MetadataType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadataCollectionType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MetadataCollectionType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:es:ricoh:webscan:plugins:deposit:xml:metadata:1.0.0", name = "metadataCollection")
    public JAXBElement<MetadataCollectionType> createMetadataCollection(MetadataCollectionType value) {
        return new JAXBElement<MetadataCollectionType>(_MetadataCollection_QNAME, MetadataCollectionType.class, null, value);
    }

}
