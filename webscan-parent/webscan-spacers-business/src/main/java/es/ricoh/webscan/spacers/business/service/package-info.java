/**
 * Capa de servicios y lógica de negocio de la utilidad de generación de
 * carátulas y separadores reutilizables.
 */
package es.ricoh.webscan.spacers.business.service;