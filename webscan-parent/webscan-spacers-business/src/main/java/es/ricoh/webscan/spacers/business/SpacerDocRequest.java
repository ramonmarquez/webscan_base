/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.business.SpacerDocRequest.java.</p>
* <b>Descripción:</b><p>Agrupa la información de un documento incluido en una petición de generación
* de carátula.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.business;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Agrupa la información de un documento incluido en una petición de generación
 * de carátula.
 * <p>
 * Clase SpacerDocRequest.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.1.0.
 */
public class SpacerDocRequest implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Número de orden del documento en el lote.
	 */
	private Integer order;

	/**
	 * Denominación del documento.
	 */
	private String name;

	/**
	 * Relación de metadatos del documento.
	 */
	private Map<String, String> metadataCollection = new HashMap<String, String>();

	/**
	 * Constructor sin argumentos.
	 */
	public SpacerDocRequest() {
		super();
	}

	/**
	 * Obtiene el número de orden del documento en el lote.
	 * 
	 * @return el número de orden del documento en el lote.
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * Establece un nuevo valor para el número de orden del documento en el
	 * lote.
	 * 
	 * @param anOrder
	 *            nuevo valor para el número de orden del documento en el lote.
	 */
	public void setOrder(Integer anOrder) {
		this.order = anOrder;
	}

	/**
	 * Obtiene la denominación del documento.
	 * 
	 * @return la denominación del documento.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación del documento.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación del documento.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene la relación de metadatos del documento.
	 * 
	 * @return relación de metadatos del documento.
	 */
	public Map<String, String> getMetadataCollection() {
		return metadataCollection;
	}

	/**
	 * Establece la relación de metadatos del documento.
	 * 
	 * @param aMetadataCollection
	 *            relación de metadatos del documento.
	 */
	public void setMetadataCollection(Map<String, String> aMetadataCollection) {
		this.metadataCollection.clear();

		if (aMetadataCollection != null && !aMetadataCollection.isEmpty()) {
			this.metadataCollection.putAll(aMetadataCollection);
		}
	}

}
