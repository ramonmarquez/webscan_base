/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.business.SpacerRequest.java.</p>
* <b>Descripción:</b><p> Agrupa los parámetros que pueden ser especificados para solicitar la
* generación de una carátula de lote de documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.model.domain.DocSpacerType;

/**
 * Agrupa los parámetros que pueden ser especificados para solicitar la
 * generación de una carátula de lote de documentos.
 * <p>
 * Clase SpacerRequest.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.1.0.
 */
public class SpacerRequest implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tipo de código carátula y separadores.
	 */
	private BatchSpacerType spacerType;

	/**
	 * Denominación de plantilla de digitalización que forma parte del
	 * identificador del perfil de digitalización.
	 */
	private String scanTemplateName;

	/**
	 * Identificador externo del colectivo de usuarios para el que es
	 * configurado el perfil de digitalización a utilizar en el proceso de
	 * digitalización del lote identificado mediante la carátula.
	 */
	private String userGroupExternalId;

	/**
	 * Denomiación de definción de documentos.
	 */
	private String docSpecName;

	/**
	 * Tipo de separador de documentos que deben emplearse en el lote.
	 */
	private DocSpacerType docSpacerType;

	/**
	 * Número de documentos que debe tener el lote.
	 */
	private Integer batchDocSize;

	/**
	 * Número total de páginas que debe tener el lote.
	 */
	private Integer batchPages;

	/**
	 * Identificación del usuario que solicita la generación de la carátula.
	 */
	private String reqUserId;

	/**
	 * Identificador de la unidad orgánica a la que serán asociados los
	 * documentos del lote.
	 */
	private String department;

	/**
	 * Relación de documentos de la carátula.
	 */
	private List<SpacerDocRequest> documents = new ArrayList<SpacerDocRequest>();

	/**
	 * Número de separadores reutilizables a generar junto a la carátula.
	 */
	private Integer numberOfReusableDocSpacers;

	/**
	 * Constructor sin argumentos.
	 */
	public SpacerRequest() {
		super();
	}

	/**
	 * Obtiene el tipo de código carátula y separadores.
	 * 
	 * @return el tipo de código carátula y separadores.
	 */
	public BatchSpacerType getSpacerType() {
		return spacerType;
	}

	/**
	 * Establece el tipo de código carátula y separadores.
	 * 
	 * @param aSpacerType
	 *            tipo de código carátula y separadores.
	 */
	public void setSpacerType(BatchSpacerType aSpacerType) {
		this.spacerType = aSpacerType;
	}

	/**
	 * Obtiene la denominación de plantilla de digitalización que forma parte
	 * del identificador del perfil de digitalización.
	 * 
	 * @return la denominación de plantilla de digitalización que forma parte
	 *         del identificador del perfil de digitalización.
	 */
	public String getScanTemplateName() {
		return scanTemplateName;
	}

	/**
	 * Establece la denominación de plantilla de digitalización que forma parte
	 * del identificador del perfil de digitalización.
	 * 
	 * @param aScanTemplateName
	 *            Denominación de plantilla de digitalización que forma parte
	 *            del identificador del perfil de digitalización.
	 */
	public void setScanTemplateName(String aScanTemplateName) {
		this.scanTemplateName = aScanTemplateName;
	}

	/**
	 * Obtiene el identificador externo del colectivo de usuarios para el que es
	 * configurado el perfil de digitalización a utilizar en el proceso de
	 * digitalización del lote identificado mediante la carátula.
	 * 
	 * @return el identificador externo del colectivo de usuarios para el que es
	 *         configurado el perfil de digitalización a utilizar en el proceso
	 *         de digitalización del lote identificado mediante la carátula.
	 */
	public String getUserGroupExternalId() {
		return userGroupExternalId;
	}

	/**
	 * Establece el identificador externo del colectivo de usuarios para el que
	 * es configurado el perfil de digitalización a utilizar en el proceso de
	 * digitalización del lote identificado mediante la carátula.
	 * 
	 * @param aUserGroupExternalId
	 *            Identificador externo del colectivo de usuarios para el que es
	 *            configurado el perfil de digitalización a utilizar en el
	 *            proceso de digitalización del lote identificado mediante la
	 *            carátula.
	 */
	public void setUserGroupExternalId(String aUserGroupExternalId) {
		this.userGroupExternalId = aUserGroupExternalId;
	}

	/**
	 * Obtiene la denominación de definición de documentos.
	 * 
	 * @return la denominación de definición de documentos.
	 */
	public String getDocSpecName() {
		return docSpecName;
	}

	/**
	 * Establece la denominación de definición de documentos.
	 * 
	 * @param aDocSpecName
	 *            denominación de definición de documentos.
	 */
	public void setDocSpecName(String aDocSpecName) {
		this.docSpecName = aDocSpecName;
	}

	/**
	 * Obtiene el tipo de separador de documentos que deben emplearse en el
	 * lote.
	 * 
	 * @return el tipo de separador de documentos que deben emplearse en el
	 *         lote.
	 */
	public DocSpacerType getDocSpacerType() {
		return docSpacerType;
	}

	/**
	 * Establece el tipo de separador de documentos que deben emplearse en el
	 * lote.
	 * 
	 * @param docSpacerType
	 *            el tipo de separador de documentos que deben emplearse en el
	 *            lote.
	 */
	public void setDocSpacerType(DocSpacerType aDocSpacerType) {
		this.docSpacerType = aDocSpacerType;
	}

	/**
	 * Obtiene el número de documentos que debe tener el lote.
	 * 
	 * @return el número de documentos que debe tener el lote.
	 */
	public Integer getBatchDocSize() {
		return batchDocSize;
	}

	/**
	 * Establece el número de documentos que debe tener el lote.
	 * 
	 * @param aBatchDocSize
	 *            número de documentos que debe tener el lote.
	 */
	public void setBatchDocSize(Integer aBatchDocSize) {
		this.batchDocSize = aBatchDocSize;
	}

	/**
	 * Obtiene el número total de páginas que debe tener el lote.
	 * 
	 * @return el número total de páginas que debe tener el lote.
	 */
	public Integer getBatchPages() {
		return batchPages;
	}

	/**
	 * Establece el número total de páginas que debe tener el lote.
	 * 
	 * @param aDocPages
	 *            número total de páginas que debe tener el lote.
	 */
	public void setBatchPages(Integer aBatchPages) {
		this.batchPages = aBatchPages;
	}

	/**
	 * Obtiene la identificación del usuario que solicita la generación de la
	 * carátula.
	 * 
	 * @return la identificación del usuario que solicita la generación de la
	 *         carátula.
	 */
	public String getReqUserId() {
		return reqUserId;
	}

	/**
	 * Establece la identificación del usuario que solicita la generación de la
	 * carátula.
	 * 
	 * @param aReqUserId
	 *            identificación del usuario que solicita la generación de la
	 *            carátula.
	 */
	public void setReqUserId(String aReqUserId) {
		this.reqUserId = aReqUserId;
	}

	/**
	 * Obtiene el identificador de la unidad orgánica a la que serán asociados
	 * los documentos del lote.
	 * 
	 * @return el identificador de la unidad orgánica a la que serán asociados
	 *         los documentos del lote.
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Establece un nuevo valor para el identificador de la unidad orgánica a la
	 * que serán asociados los documentos del lote.
	 * 
	 * @param aDepartment
	 *            nuevo valor para el identificador de la unidad orgánica a la
	 *            que serán asociados los documentos del lote.
	 */
	public void setDepartment(String aDepartment) {
		this.department = aDepartment;
	}

	/**
	 * Obtiene la relación de documentos referenciados en la carátula.
	 * 
	 * @return relación de documentos referenciados en la carátula.
	 */
	public List<SpacerDocRequest> getDocuments() {
		return documents;
	}

	/**
	 * Establece la relación de documentos referenciados en la carátula.
	 * 
	 * @param anyDocuments
	 *            relación de documentos referenciados en la carátula.
	 */
	public void setDocuments(List<SpacerDocRequest> anyDocuments) {
		this.documents.clear();
		if (anyDocuments != null && !anyDocuments.isEmpty()) {
			this.documents.addAll(anyDocuments);
		}
	}

	/**
	 * Obtiene el número de separadores reutilizables a generar junto a la
	 * carátula.
	 * 
	 * @return número de separadores reutilizables a generar junto a la
	 *         carátula.
	 */
	public Integer getNumberOfReusableDocSpacers() {
		return numberOfReusableDocSpacers;
	}

	/**
	 * Establece el número de separadores reutilizables a generar junto a la
	 * carátula.
	 * 
	 * @param aNumberOfReusableDocSpacers
	 *            número de separadores reutilizables a generar junto a la
	 *            carátula.
	 */
	public void setNumberOfReusableDocSpacers(Integer aNumberOfReusableDocSpacers) {
		this.numberOfReusableDocSpacers = aNumberOfReusableDocSpacers;
	}

}
