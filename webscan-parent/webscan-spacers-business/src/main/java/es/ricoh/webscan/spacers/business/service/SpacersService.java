/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.business.service.SpacersService.java.</p>
* <b>Descripción:</b><p> Implementación de la lógica de negocio de la utilidad de generación de
 * carátulas y separadores reutilizables.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.business.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.spacers.business.Spacer;
import es.ricoh.webscan.spacers.business.SpacerDocRequest;
import es.ricoh.webscan.spacers.business.SpacerRequest;
import es.ricoh.webscan.spacers.business.SpacersException;
import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.model.domain.DocSpacerType;
import es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerDocDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerMetadataDTO;
import es.ricoh.webscan.spacers.repository.SpacersDAOException;
import es.ricoh.webscan.spacers.repository.SpacersModelManager;
import es.ricoh.webscan.statistics.model.domain.ScanWorkingMode;
import es.ricoh.webscan.statistics.model.dto.DocScanStatisticsDTO;
import es.ricoh.webscan.statistics.repository.StatisticsDAOException;
import es.ricoh.webscan.statistics.repository.StatisticsModelManager;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;
import es.ricoh.webscan.model.service.AuditService;
import es.ricoh.webscan.utilities.MimeType;

/**
 * Implementación de la lógica de negocio de la utilidad de generación de
 * carátulas y separadores reutilizables.
 * <p>
 * Clase SpacersService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacersService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SpacersService.class);

	/**
	 * Idioma por defecto.
	 */
	private static final Locale LOCALE_DEFAULT_VALUE = new Locale("es");

	/**
	 * Tipo de codificación Código QR.
	 */
	private static final String CODE_QR_CODE_TYPE_VALUE = "Code QR";

	/**
	 * Tipo de codificación Código 128.
	 */
	private static final String CODE_128_CODE_TYPE_VALUE = "Code 128";

	/**
	 * Tipo de codificación PDF 417.
	 */
	private static final String PDF_417_CODE_TYPE_VALUE = "PDF 417";

	/**
	 * Servicio interno para el registro y consulta de trazas y eventos de
	 * auditoría del módulo de gestión de usuarios de Webscan.
	 */
	private AuditService auditService;

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos base de Webscan.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos de generación de carátulas y separadores reutilizables.
	 */
	private SpacersModelManager spacersModelManager;

	/**
	 * Utilidad que implementa la generación de carátulas de lotes de documentos
	 * y separadores reutilizables.
	 */
	private SpacersGenerator spacersGenerator;

	/**
	 * Objeto que representa el acceso a los servicio del modelo de datos de las
	 * estadísticas.
	 */
	private StatisticsModelManager statisticsModelManager;

	/**
	 * Genera un documento que incluye una carátula de lote de documentos y cero
	 * o más separadores reutilizables de documentos, registrando la información
	 * de la carátula en el Sistema.
	 * 
	 * @param spacerRequest
	 *            Petición de generación de carátula que encapsulas datos
	 *            generales de generación de la carátula, relación de metadatos
	 *            y numero de separadores reutilizables a generar.
	 * @param traceId
	 *            Identificador de trazabilidad de la petición SOAP del servicio
	 *            de generación de carátulas y separadores reutilizables de
	 *            documentos.
	 * @param locale
	 *            información de configuración regional e idioma.
	 * @return documento que incluye la carátula y separadores reutilizables
	 *         solicitados.
	 * @throws SpacersException
	 *             Si ocurre algún error en la generación del documento que
	 *             incluye la carátula y separadores reutilizables solicitados.
	 * @throws SpacersDAOException
	 *             Si ocurre algún error en el registro de la información de la
	 *             carátula en el Sistema.
	 */
	public Spacer generateDocsBatchSpacer(SpacerRequest spacerRequest, String traceId, Boolean isRemoteRequest, Locale locale) throws SpacersException, SpacersDAOException {
		byte[ ] spacerContent = null;
		DocsBatchSpacerDTO dto;
		Long auditOpId = null;
		long t0;
		long tf;
		String excMsg;
		Spacer res = null;

		LOG.debug("[WEBSCAN-SPACER-BUSINESS] Se procede a generar una nueva carátula ...");
		t0 = System.currentTimeMillis();
		try {
			if (spacerRequest.getNumberOfReusableDocSpacers() != null && spacerRequest.getNumberOfReusableDocSpacers() < 1) {
				throw new SpacersException(SpacersException.CODE_005, "El número de separadores reutilizables debe ser mayor o igual a 1.");
			}

			dto = validateScanProfileInfo(spacerRequest.getScanTemplateName(), spacerRequest.getUserGroupExternalId(), spacerRequest.getDocSpecName());

			dto.setBatchDocSize(spacerRequest.getBatchDocSize());
			dto.setBatchPages(spacerRequest.getBatchPages());
			dto.setDepartment(spacerRequest.getDepartment());
			dto.setDocSpacerType(spacerRequest.getDocSpacerType());
			dto.setReqUserId(spacerRequest.getReqUserId());
			dto.setSpacerType(spacerRequest.getSpacerType());
			dto.setBatchDocSize(spacerRequest.getDocuments().size());

			LOG.debug("[WEBSCAN-SPACER-BUSINESS] Se invoca la fachada de la cap DAO para registrar la carátula ...");
			dto = spacersModelManager.createDocsBatchSpacer(dto, fillSpacerDocuments(spacerRequest.getDocuments()));
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-SPACER-BUSINESS] Se genera el documento que contiene a la carátula " + dto.getSpacerId() + "...");
			}

			// Invocar generador contenido carátula
			spacerContent = spacersGenerator.createDocsBatchSpacer(dto, spacerRequest.getScanTemplateName() + " (" + spacerRequest.getUserGroupExternalId() + ")", (spacerRequest.getNumberOfReusableDocSpacers() == null ? 0 : spacerRequest.getNumberOfReusableDocSpacers()), locale);

			res = new Spacer(MimeType.PDF_CONTENT.getType(), spacerContent);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-SPACER-BUSINESS] Se registran trazas de auditoria para la generación de la carátula " + dto.getSpacerId() + "...");
			}
			auditOpId = generateDocsBatchSpacerAuditLog(dto, traceId);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-SPACER-BUSINESS] Se actualiza la información de auditoría de la carátula " + dto.getSpacerId() + "...");
			}
			dto.setAuditOpId(auditOpId);
			spacersModelManager.setDocsBatchSpacerAuditInfo(dto.getId(), auditOpId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-SPACER-BUSINESS] Carátula " + dto.getSpacerId() + " generada correctamente.");
			}

			// En caso de que la carátula haya sido solicitada mediante los
			// serviciosInsertamos los datos de uso del sistema asociados a la
			// nueva
			// carátula, que posteriomente
			// actualizaremos con los datos completos una vez se haya realizado
			// el escaneo con esta reserva
			if (isRemoteRequest) {
				initStatistics(spacerRequest, dto);
			}
		} catch (SpacersException e) {
			throw e;
		} catch (SpacersDAOException e) {
			throw e;
		} catch (StatisticsDAOException e) {
			excMsg = "Error realizando el registro de datos de uso del sistema al generar una carátula: " + e.getCode() + " - " + e.getMessage() + ".";
			LOG.error("[WEBSCAN-SPACER-BUSINESS] {}", excMsg);
			throw new SpacersException(SpacersException.CODE_006, excMsg, e);
		} catch (DAOException e) {
			excMsg = "Error realizando el registro de trazas de auditoria al generar una carátula: " + e.getCode() + " - " + e.getMessage() + ".";
			LOG.error("[WEBSCAN-SPACER-BUSINESS] {}", excMsg);
			throw new SpacersException(SpacersException.CODE_007, excMsg, e);
		} catch (Exception e) {
			excMsg = "Error no esperado al generar una carátula: " + e.getMessage() + ".";
			LOG.error("[WEBSCAN-SPACER-BUSINESS] {}", excMsg);
			throw new SpacersException(SpacersException.CODE_999, e.getMessage(), e);
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-SPACER-BUSINESS] Fin generación carátula. Tiempo de ejecución: {} ms.", (tf - t0));
		}

		return res;
	}

	/**
	 * Valida la información de perfil de digitalización incluida en la petición
	 * de generación de carátula, comprobando lo siguiente:
	 * <ul>
	 * <li>Es informado el perfil de digitalización, identificado mediante una
	 * denominación de plantilla de digitalización y un identificador externo de
	 * colectivo de usuarios, y existe en el Sistema.</li>
	 * <li>Es informada la definición de documentos y existe en el Sistema.</li>
	 * <li>La definición de documentos está configurada para el perfil de
	 * digitalización informado en el sistema.</li>
	 * </ul>
	 * 
	 * @param scanTemplateName
	 *            Denominación de la plantilla de digitalización que identifica
	 *            el perfil de digitalización establecido para el lote
	 *            identificado por la carátula.
	 * @param userGroupExternalId
	 *            Identificador externo del colectivo de usuarios para el que es
	 *            definido el perfil de digitalización establecido para el lote
	 *            identificado por la carátula.
	 * @param docSpecName
	 *            Denominación de definición de documentos.
	 * @return objeto DocsBatchSpacerDTO incluyendo los identificadores de
	 *         perfil de digitalización y definición de documentos establecidos.
	 * @throws SpacersException
	 *             Si se produce algún error en la validación de la información
	 *             de digitalización de la petición.
	 */
	private DocsBatchSpacerDTO validateScanProfileInfo(String scanTemplateName, String userGroupExternalId, String docSpecName) throws SpacersException {
		DocsBatchSpacerDTO res = new DocsBatchSpacerDTO();
		DocumentSpecificationDTO docSpec = null;
		ScanProfileOrgUnitDTO scanProfileOrgUnit = null;

		LOG.debug("[WEBSCAN-SPACER-BUSINESS] Se procede a validar la información de digitalización para la generación de la carátula ...");

		if (scanTemplateName == null || scanTemplateName.isEmpty()) {
			throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Plantilla de digitalización no especificada.");
		}

		if (userGroupExternalId == null || userGroupExternalId.isEmpty()) {
			throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Identificador de colectivo de usuarios no especificado.");
		}

		if (docSpecName == null || docSpecName.isEmpty()) {
			throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Definición de documentos no especificada.");
		}

		LOG.debug("[WEBSCAN-SPACER-BUSINESS] Validando plantilla...");
		scanProfileOrgUnit = findScanProfileOrgUnit(scanTemplateName, userGroupExternalId);

		checkActiveScanProfile(scanTemplateName);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACER-BUSINESS] Perfil de digitalización encontrada: " + scanProfileOrgUnit.getId() + ".");
		}

		res.setScanProfileId(scanProfileOrgUnit.getId());

		// Se valida que existe en el sistema la definición de
		// documentos informada
		LOG.debug("[WEBSCAN-SPACER-BUSINESS] Validando definición de documentos ...");
		docSpec = findDocSpecification(docSpecName);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACER-BUSINESS] Definción de documentos " + docSpecName + " encontrada: " + docSpec.getId() + ".");
		}

		checkScanProfileDocSpecRelationship(scanProfileOrgUnit, docSpec);
		res.setDocSpecId(docSpec.getId());

		LOG.debug("[WEBSCAN-SPACER-BUSINESS] Información de perfil de digitalización validada correctamente.");

		return res;
	}

	/**
	 * Recupera del modelo de datos base del sistema el perfil de digitalización
	 * configurado para una plantilla de digitalización, identificada mediante
	 * su denominación, en un colectivo de usuarios, identificado a través de su
	 * identificador externo.
	 * 
	 * @param scanProfileName
	 *            Denominación de plantilla de digitalización.
	 * @param userGroupExternalId
	 *            Identificador externo de unidad organizativa.
	 * @return el perfil de digitalización solicitado.
	 * @throws SpacersException
	 *             Si no existe la entidad en el Sistema, o se produce algún
	 *             error al ejecutar la consulta.
	 */
	public ScanProfileOrgUnitDTO findScanProfileOrgUnit(String scanProfileName, String userGroupExternalId) throws SpacersException {
		ScanProfileOrgUnitDTO res;

		try {
			res = webscanModelManager.getScanProfileOrgUnitByScanProfileNameAndOrgUnitExtId(scanProfileName, userGroupExternalId);
		} catch (DAOException e) {
			if (DAOException.CODE_901.equals(e.getCode())) {
				throw new SpacersException(SpacersException.CODE_001, "El perfil de digitalización informado en la petición de generación de carátula no existe en el Sistema.");
			} else {
				throw new SpacersException(SpacersException.CODE_999, e.getMessage());
			}
		}

		return res;
	}

	/**
	 * Comprueba si la plantilla de digitalización a la que pertenece el perfil
	 * de digitalización se encuentra habilitada para su uso.
	 * 
	 * @param scanProfileName
	 *            Denominación de plantilla de digitalización.
	 * @throws SpacersException
	 *             Si no existe la entidad en el Sistema, no se encuentra
	 *             activo, o se produce algún error al ejecutar la consulta.
	 */
	private void checkActiveScanProfile(String scanProfileName) throws SpacersException {
		ScanProfileDTO scanProfile;

		try {
			scanProfile = webscanModelManager.getScanProfileByName(scanProfileName);

			if (!scanProfile.getActive()) {
				throw new SpacersException(SpacersException.CODE_008, "El perfil de digitalización informado en la petición de genración de carátula se encuentra deshabilitado para su uso.");
			}
		} catch (DAOException e) {
			if (DAOException.CODE_901.equals(e.getCode())) {
				throw new SpacersException(SpacersException.CODE_001, "El perfil de digitalización informado en la petición de generación de carátula no existe en el Sistema.");
			} else {
				throw new SpacersException(SpacersException.CODE_999, e.getMessage());
			}
		} catch (SpacersException e) {
			throw e;
		}

	}

	/**
	 * Recupera del modelo de datos base del sistema una definición de
	 * documentos activa a partir de su denominación.
	 * 
	 * @param docSpecName
	 *            Denominación de definición de documentos.
	 * @return la definición de documentos solicitada.
	 * @throws SpacersException
	 *             Si no existe la entidad en el Sistema, o se produce algún
	 *             error al ejecutar la consulta.
	 */
	private DocumentSpecificationDTO findDocSpecification(String docSpecName) throws SpacersException {
		DocumentSpecificationDTO res;

		try {
			res = webscanModelManager.getDocSpecificationByName(docSpecName);

			if (!res.getActive()) {
				throw new SpacersException(SpacersException.CODE_009, "La definición de documentos informada en la petición de generación de carátula se encuentra deshabilitada para su uso.");
			}
		} catch (DAOException e) {
			if (DAOException.CODE_901.equals(e.getCode())) {
				throw new SpacersException(SpacersException.CODE_002, "La definición de documentos informada en la petición de generación de carátula no existe en el Sistema.");
			} else {
				throw new SpacersException(SpacersException.CODE_999, e.getMessage());
			}
		}

		return res;
	}

	/**
	 * Verifica que una especificación de documentos está establecida en un
	 * perfil de digitalización.
	 * 
	 * @param scanProfileOrgUnit
	 *            Perfil de digitalización.
	 * @param docSpec
	 *            definición de documentos.
	 * @throws SpacersException
	 *             Si no existe relación de asociación entre ambas entidades, o
	 *             se produce algún error al ejecutar las consultas sobre el
	 *             modelo de datos.
	 */
	private void checkScanProfileDocSpecRelationship(ScanProfileOrgUnitDTO scanProfileOrgUnit, DocumentSpecificationDTO docSpec) throws SpacersException {
		Boolean scanProfiledocSpecFound = Boolean.FALSE;
		DocumentSpecificationDTO documentSpecification;
		ScanProfileOrgUnitDocSpecDTO scanProfileOrgUnitDocSpec;
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> docSpecs;

		if (scanProfileOrgUnit != null && docSpec != null) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-SPACER-BUSINESS] Validando relación entre el perfil de digitalización " + scanProfileOrgUnit.getId() + " y la definición de documentos " + docSpec.getId() + " ...");
			}
			// Se verifica que la especificación de documentos esta
			// establecida en alguna configuración del perfil en unidad
			// organizativa.
			scanProfiledocSpecFound = Boolean.FALSE;

			try {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-BOOKING-BUSINESS] Recuperando definiciones de documentos configuradas para el perfil de digitalización " + scanProfileOrgUnit.getId() + " ...");
				}
				docSpecs = webscanModelManager.getDocSpecificationsByScanProfileOrgUnitId(scanProfileOrgUnit.getId());

				for (Iterator<DocumentSpecificationDTO> it = docSpecs.keySet().iterator(); !scanProfiledocSpecFound && it.hasNext();) {
					documentSpecification = it.next();

					if (documentSpecification.equals(docSpec)) {
						scanProfileOrgUnitDocSpec = docSpecs.get(documentSpecification);
						if (!scanProfileOrgUnitDocSpec.getActive()) {
							throw new SpacersException(SpacersException.CODE_009, "La definición de documentos informada en la petición de generación de carátula se encuentra deshabilitada para su uso en el perfil de digitalización establecido.");
						}
						scanProfiledocSpecFound = Boolean.TRUE;
					}
				}
			} catch (DAOException e) {
				if (DAOException.CODE_901.equals(e.getCode())) {
					throw new SpacersException(SpacersException.CODE_001, "El perfil de digitalización informado en la petición de generación de carátula no existe en el Sistema.");
				} else {
					throw new SpacersException(SpacersException.CODE_999, e.getMessage());
				}
			}

			if (!scanProfiledocSpecFound) {
				throw new SpacersException(SpacersException.CODE_003, "La definición de documentos informada no está asociada al perfil de digitalización especificado en la petición de generación de carátula.");
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-SPACER-BUSINESS] Relación entre el perfil de digitalización " + scanProfileOrgUnit.getId() + " y la definición de documentos " + docSpec.getId() + " validada correctamente.");
			}
		}
	}

	/**
	 * Construye la lista de metadatos referenciados de una carátula de lote de
	 * documentos.
	 * 
	 * @param documents
	 *            relación de documentos incluidos en la petición de generación
	 *            de carátula.
	 * @return el lote de documentos referenciados en la carátula y sus
	 *         metadatos.
	 */
	private Map<SpacerDocDTO, List<SpacerMetadataDTO>> fillSpacerDocuments(List<SpacerDocRequest> documents) {
		Map<SpacerDocDTO, List<SpacerMetadataDTO>> res = new HashMap<SpacerDocDTO, List<SpacerMetadataDTO>>();
		SpacerDocDTO docDto;

		for (SpacerDocRequest doc: documents) {
			docDto = new SpacerDocDTO();
			docDto.setOrder(doc.getOrder());
			docDto.setName(doc.getName());

			res.put(docDto, fillMetadataCollection(doc.getMetadataCollection()));
		}

		return res;
	}

	/**
	 * Construye la lista de metadatos referenciados de una carátula de lote de
	 * documentos.
	 * 
	 * @param reqMetadataCollection
	 *            relación de pares clave / valor, donde cada entrada es
	 *            identificada por la denominación de un metadato, y el valor se
	 *            corresponde con el valor informado de dicho metadato.
	 * @return la lista de metadatos referenciados de una carátula de lote de
	 *         documentos.
	 */
	private List<SpacerMetadataDTO> fillMetadataCollection(Map<String, String> reqMetadataCollection) {
		List<SpacerMetadataDTO> res = new ArrayList<SpacerMetadataDTO>();
		SpacerMetadataDTO metadata;
		String key;
		if (reqMetadataCollection != null && !reqMetadataCollection.isEmpty()) {
			for (Iterator<String> it = reqMetadataCollection.keySet().iterator(); it.hasNext();) {
				key = it.next();

				metadata = new SpacerMetadataDTO();
				metadata.setName(key);
				metadata.setValue(reqMetadataCollection.get(key));

				res.add(metadata);
			}
		}
		return res;
	}

	/**
	 * Registra la traza de auditoría correspondiente a la generación de una
	 * carátula de lote de documentos.
	 * 
	 * @param docsBatchSpacer
	 *            carátula generada.
	 * @param traceId
	 *            Identificador de trazabilidad de la petición SOAP del servicio
	 *            de generación de carátulas y separadores reutilizables de
	 *            documentos.
	 * @return identificador de la traza creada.
	 * @throws DAOException
	 *             Si se produce algún error al registrar la traza.
	 */
	private Long generateDocsBatchSpacerAuditLog(DocsBatchSpacerDTO docsBatchSpacer, String traceId) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		Date now = new Date();
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		Long res;
		String additionalInfo;

		// Traza de auditoría
		auditOperation = new AuditOperationDTO();
		auditOperation.setName(AuditOperationName.GENERATE_DOCS_BATCH_SPACER);
		auditOperation.setStartDate(now);
		auditOperation.setLastUpdateDate(now);
		auditOperation.setStatus(AuditOperationStatus.OPEN);
		// Evento de auditoría
		auditEvent = new AuditEventDTO();
		additionalInfo = "Carátula con identificador " + docsBatchSpacer.getSpacerId() + " generada correctamente.";
		if (traceId != null) {
			additionalInfo += " Identificador de trazabilidad: " + traceId + ".";
		}
		auditEvent.setAdditionalInfo(additionalInfo);
		auditEvent.setEventDate(now);
		auditEvent.setEventName(AuditEventName.GENERATE_DOCS_BATCH_SPACER);
		auditEvent.setEventResult(AuditOpEventResult.OK);
		auditEvent.setEventUser(docsBatchSpacer.getReqUserId());
		auditEvents.add(auditEvent);

		// Registro en BBDD
		res = auditService.addNewAuditLogs(auditOperation, auditEvents, Boolean.FALSE);

		return res;
	}

	/**
	 * Genera un documento que incluye uno o más separadores reutilizables de
	 * documentos.
	 * 
	 * @param infoCodingMode
	 *            Tipo de código empleado opr el separador.
	 * @param numberOfReusableDocSpacers
	 *            número de separadores reutilizables a generar.
	 * @param userId
	 *            Identificación del solicitante.
	 * @param traceId
	 *            Identificador de trazabilidad de la petición SOAP del servicio
	 *            de generación de carátulas y separadores reutilizables de
	 *            documentos.
	 * @param locale
	 *            información de configuración regional e idioma.
	 * @return documento que incluye los separadores reutilizables solicitados.
	 * @throws SpacersException
	 *             Si ocurre algún error en la generación del documento que
	 *             incluye separadores reutilizables solicitados.
	 */
	public Spacer generateReusableDocSpacers(BatchSpacerType infoCodingMode, Integer numberOfReusableDocSpacers, String userId, String traceId, Locale locale) throws SpacersException {
		byte[ ] spacerContent = null;
		Long auditOpId = null;
		long t0;
		long tf;
		Spacer res = null;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACER-BUSINESS] Se procede a generar " + numberOfReusableDocSpacers + " separadores reutilizables con código " + infoCodingMode + " ...");
		}
		t0 = System.currentTimeMillis();

		if (infoCodingMode == null) {
			throw new SpacersException(SpacersException.CODE_998, "El tipo de código de barras es requerido.");
		}
		if (userId == null || userId.isEmpty()) {
			throw new SpacersException(SpacersException.CODE_998, "La identificación del usuario solicitante es requerida.");
		}

		if (numberOfReusableDocSpacers == null) {
			throw new SpacersException(SpacersException.CODE_998, "El número de separadores reutilizables es requerido.");
		}

		if (numberOfReusableDocSpacers < 1) {
			throw new SpacersException(SpacersException.CODE_005, "El número de separadores reutilizables debe ser mayor o igual a 1.");
		}

		LOG.debug("[WEBSCAN-SPACER-BUSINESS] Se genera el documento con los separadores ...");
		// Invocar generador documento con sepradores
		spacerContent = spacersGenerator.createReusableDocSpacers(infoCodingMode, numberOfReusableDocSpacers, locale);

		res = new Spacer(MimeType.PDF_CONTENT.getType(), spacerContent);
		LOG.debug("[WEBSCAN-SPACER-BUSINESS] Se registran trazas de auditoria para la generación de los separadores reutilizables ...");
		try {
			auditOpId = generateReusableDocsSpacerAuditLog(infoCodingMode, numberOfReusableDocSpacers, userId, traceId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-SPACER-BUSINESS] Traza de auditoria " + auditOpId + " generada (separadores reutilizables).");
			}
		} catch (DAOException e) {
			throw new SpacersException(SpacersException.CODE_999, e.getMessage());
		} catch (Exception e) {
			throw new SpacersException(SpacersException.CODE_999, e.getMessage());
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACER-BUSINESS] " + numberOfReusableDocSpacers + " separadores reutilizables generados correctamente (" + infoCodingMode.toString() + ").");
		}

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-SPACER-BUSINESS] Fin generación separadores reutilizables. Tiempo de ejecución: {} ms.", (tf - t0));
		return res;
	}

	/**
	 * Registra la traza de auditoría correspondiente a la generación de uno o
	 * más separadores reutilizables de documentos.
	 * 
	 * @param infoCodingMode
	 *            Tipo de código empleado opr el separador.
	 * @param numberOfReusableDocSpacers
	 *            número de separadores reutilizables a generar.
	 * @param userId
	 *            Identificación del solicitante.
	 * @param traceId
	 *            Identificador de trazabilidad de la petición SOAP del servicio
	 *            de generación de carátulas y separadores reutilizables de
	 *            documentos.
	 * @return identificador de la traza creada.
	 * @throws DAOException
	 *             Si se produce algún error al registrar la traza.
	 */
	private Long generateReusableDocsSpacerAuditLog(BatchSpacerType infoCodingMode, Integer numberOfReusableDocSpacers, String userId, String traceId) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		Date now = new Date();
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		Long res;
		String additionalInfo;

		// Traza de auditoría
		auditOperation = new AuditOperationDTO();
		auditOperation.setName(AuditOperationName.GENERATE_REUSABLE_DOCS_SPACERS);
		auditOperation.setStartDate(now);
		auditOperation.setLastUpdateDate(now);
		auditOperation.setStatus(AuditOperationStatus.CLOSED_OK);
		// Evento de auditoría
		auditEvent = new AuditEventDTO();
		additionalInfo = numberOfReusableDocSpacers + " separadores reutilizables generados correctamente (" + infoCodingMode.toString() + ").";
		if (traceId != null) {
			additionalInfo += " Identificador de trazabilidad: " + traceId + ".";
		}
		auditEvent.setAdditionalInfo(additionalInfo);
		auditEvent.setEventDate(now);
		auditEvent.setEventName(AuditEventName.GENERATE_REUSABLE_DOCS_SPACERS);
		auditEvent.setEventResult(AuditOpEventResult.OK);
		auditEvent.setEventUser(userId);
		auditEvents.add(auditEvent);

		// Registro en BBDD
		res = auditService.addNewAuditLogs(auditOperation, auditEvents, Boolean.TRUE);

		return res;
	}

	/**
	 * Recupera una carátula en estado generada a partir de su identificador
	 * normalizado.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador normalizado de carátula.
	 * @return Carátula solicitada.
	 * @throws SpacersDAOException
	 *             Si no existe una carátula en estado generada con el
	 *             identificador especificado, existe más de una carátula, se
	 *             produce algún error en la consulta a BBDD, o los parámetros
	 *             de entrada no son válidos.
	 */
	public DocsBatchSpacerDTO getGeneratedDocsBatchSpacer(String docsBatchSpacerId) throws SpacersDAOException {
		DocsBatchSpacerDTO res;

		res = spacersModelManager.getGeneratedDocsBatchSpacer(docsBatchSpacerId);

		return res;
	}

	/**
	 * Finaliza el trabajo de digitalización asociado a una carátula.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, no se encuentra en el estado
	 *             generada, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void completeDocsBatchSpacer(Long docsBatchSpacerId) throws SpacersDAOException {
		spacersModelManager.completeDocsBatchSpacer(docsBatchSpacerId);
	}

	/**
	 * Obtiene el tipo de código a generar en la carátula de lote de documentos
	 * o separadores reutilizables de documentos.
	 * 
	 * @param batchSpacerType
	 *            Tipo código de barras.
	 * @return tipo de código de barras de carátulas de lote de documentos.
	 * @throws SpacersException
	 *             si el tipo de código de barras no es admitido por el Sistema.
	 */
	public BatchSpacerType getRequestBatchSpacerType(String batchSpacerType) throws SpacersException {
		BatchSpacerType res = null;

		if (batchSpacerType == null || batchSpacerType.isEmpty()) {
			throw new SpacersException(SpacersException.CODE_998, "El tipo de código de barras a emplear es un parámetro requerido por el servicio.");
		}

		switch (batchSpacerType) {
			case CODE_QR_CODE_TYPE_VALUE:
				res = BatchSpacerType.CODE_QR;
				break;
			case CODE_128_CODE_TYPE_VALUE:
				res = BatchSpacerType.CODE_128;
				break;
			case PDF_417_CODE_TYPE_VALUE:
				res = BatchSpacerType.PDF_417;
				break;
			default:
				throw new SpacersException(SpacersException.CODE_998, "Tipo de código de barras de carátulas no admitido por el Sistema.");
		}

		return res;
	}

	/**
	 * Obtiene el tipo de código de barras empleado en el lote para separar los
	 * documentos.
	 * 
	 * @param reusableDocsBatchSpacer
	 *            denominación del código de barras.
	 * @return tipo de código de barras de separadores reutilizables de
	 *         documentos.
	 * @throws SpacersException
	 *             si el tipo de código de barras no es admitido por el Sistema.
	 */
	public DocSpacerType getRequestDocSpacerType(String reusableDocsBatchSpacer) throws SpacersException {
		DocSpacerType res = null;

		if (reusableDocsBatchSpacer != null && !reusableDocsBatchSpacer.isEmpty()) {
			switch (reusableDocsBatchSpacer) {
				case "Blank page":
					res = DocSpacerType.BLANK_PAGE;
					break;
				case CODE_QR_CODE_TYPE_VALUE:
					res = DocSpacerType.CODE_QR;
					break;
				case CODE_128_CODE_TYPE_VALUE:
					res = DocSpacerType.CODE_128;
					break;
				case PDF_417_CODE_TYPE_VALUE:
					res = DocSpacerType.PDF_417;
					break;
				default:
					throw new SpacersException(SpacersException.CODE_998, "Tipo código de barras de separadores de documentos no admitido por el Sistema.");
			}
		}

		return res;
	}

	/**
	 * Transforma una cadena a un objeto Locale.
	 * 
	 * @param str
	 *            cadena que representa un idioma.
	 * @return un objeto Locale.
	 */
	public Locale getLocale(String str) {
		Locale res = LOCALE_DEFAULT_VALUE;

		try {
			if (str != null && !str.isEmpty()) {
				res = new Locale(str);
			}
		} catch (Exception e) {
			// No hacer nada
		}

		return res;
	}

	/**
	 * Recupera del modelo de datos base del sistema una plantilla de
	 * digitalización al cual pertenece un perfil de digitalización.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de perfil de digitalización.
	 * @return la plantilla de digitalización solicitada.
	 * @throws SpacersException
	 *             Si no existe la entidad en el Sistema, o se produce algún
	 *             error al ejecutar la consulta.
	 */
	public ScanProfileDTO findScanProfile(Long scanProfileOrgUnitId) throws SpacersException {
		ScanProfileOrgUnitDTO scanProfileOrgUnit;
		ScanProfileDTO res;

		try {
			scanProfileOrgUnit = webscanModelManager.getScanProfileOrgUnit(scanProfileOrgUnitId);
			res = webscanModelManager.getScanProfile(scanProfileOrgUnit.getScanProfile());
		} catch (DAOException e) {
			if (DAOException.CODE_901.equals(e.getCode())) {
				throw new SpacersException(SpacersException.CODE_001, "El perfil de digitalización informado en la petición de generación de carátula no existe en el Sistema.");
			} else {
				throw new SpacersException(SpacersException.CODE_999, e.getMessage());
			}
		}

		return res;
	}

	/**
	 * Recupera del modelo de datos base del sistema un colectivo de usuarios en
	 * el cual está configurado un perfil de digitalización.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de perfil de digitalización.
	 * @return el colectivo de usuarios solicitado.
	 * @throws SpacersException
	 *             Si no existe la entidad en el Sistema, o se produce algún
	 *             error al ejecutar la consulta.
	 */
	public OrganizationUnitDTO findOrgUnit(Long scanProfileOrgUnitId) throws SpacersException {
		ScanProfileOrgUnitDTO scanProfileOrgUnit;
		OrganizationUnitDTO res;

		try {
			scanProfileOrgUnit = webscanModelManager.getScanProfileOrgUnit(scanProfileOrgUnitId);
			res = webscanModelManager.getOrganizationUnit(scanProfileOrgUnit.getOrgUnit());
		} catch (DAOException e) {
			throw new SpacersException(SpacersException.CODE_999, e.getMessage());
		}

		return res;
	}

	/**
	 * Recupera del modelo de datos base del sistema una definición de
	 * documentos a partir de su identificador.
	 * 
	 * @param docSpecId
	 *            Identificador de definición de documentos.
	 * @return la definición de documentos solicitada.
	 * @throws SpacersException
	 *             Si no existe la entidad en el Sistema, o se produce algún
	 *             error al ejecutar la consulta.
	 */
	public DocumentSpecificationDTO findDocSpecification(Long docSpecId) throws SpacersException {
		DocumentSpecificationDTO res;

		try {
			res = webscanModelManager.getDocSpecification(docSpecId);
		} catch (DAOException e) {
			if (DAOException.CODE_901.equals(e.getCode())) {
				throw new SpacersException(SpacersException.CODE_002, "La definición de documentos informada en la petición de generación de carátula no existe en el Sistema.");
			} else {
				throw new SpacersException(SpacersException.CODE_999, e.getMessage());
			}
		}

		return res;
	}

	/**
	 * Obtenemos la especificacion de documentos según el scanProfile y
	 * Organization Unit
	 * 
	 * @param scanProfileId
	 *            Perfil solicitado
	 * @param OrgUnitId
	 *            Unidad organizativa asociada
	 * @return mapa con la información asociada a las especificaciones de
	 *         documentos y su relacion con el identificador del perfil y la
	 *         unidad organizativa
	 * @throws SpacersException
	 *             Excepción captura al acceder a la información de BBDD
	 */
	public Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getDocSpecificationsByScanProfileidOrgUnitId(Long scanProfileId, Long orgUnitId) throws SpacersException {
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> mapDocSpecifications = new HashMap<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO>();
		try {
			ScanProfileOrgUnitDTO scanProfileOrgUnitDTO = webscanModelManager.getScanProfileOrgUnitByScanProfileAndOrgUnit(scanProfileId, orgUnitId);
			mapDocSpecifications = webscanModelManager.getDocSpecificationsByScanProfileOrgUnitId(scanProfileOrgUnitDTO.getId());
		} catch (DAOException e) {
			throw new SpacersException(SpacersException.CODE_999, e.getMessage());
		}
		return mapDocSpecifications;
	}

	/**
	 * Funcion para recuperar el conjunto de las especificaciones de metadatos
	 * asociados a la especificacion de un documento
	 * 
	 * @param docSpecId
	 *            Identificador de la especificacion de un documento
	 * @return mapa con la informacion asociada a las especificaciones
	 * @throws SpacersException
	 *             Excepción captura al acceder a la información de BBDD
	 */
	public Map<String, List<MetadataSpecificationDTO>> getMetadataSpecByDocSpec(Long docSpecId) throws SpacersException {
		Map<String, List<MetadataSpecificationDTO>> metaSpec = new HashMap<String, List<MetadataSpecificationDTO>>();

		try {
			metaSpec = webscanModelManager.getMetadaSpecificationsByDocSpec(docSpecId);
		} catch (DAOException e) {
			throw new SpacersException(SpacersException.CODE_999, e.getMessage());
		}
		return metaSpec;
	}

	/**
	 * Establece el servicio interno para el registro y consulta de trazas y
	 * eventos de auditoría de Webscan.
	 * 
	 * @param anAuditManager
	 *            nuevo servicio interno para el registro y consulta de trazas y
	 *            eventos de auditoría de Webscan.
	 */
	public void setAuditService(AuditService anAuditService) {
		this.auditService = anAuditService;
	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos de generación de carátulas y
	 * separadores reutilizables.
	 * 
	 * @param aSpacersModelManager
	 *            Fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos de generación de carátulas y
	 *            separadores reutilizables.
	 */
	public void setSpacersModelManager(SpacersModelManager aSpacersModelManager) {
		this.spacersModelManager = aSpacersModelManager;
	}

	/**
	 * Establece la utilidad que implementa la generación de carátulas de lotes
	 * de documentos y separadores reutilizables.
	 * 
	 * @param aSpacersGenerator
	 *            Utilidad que implementa la generación de carátulas de lotes de
	 *            documentos y separadores reutilizables.
	 */
	public void setSpacersGenerator(SpacersGenerator aSpacersGenerator) {
		this.spacersGenerator = aSpacersGenerator;
	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos de las estadísticas de la
	 * digitalización de documentos.
	 * 
	 * @param aStatisticsModelManager
	 */
	public void setStatisticsModelManager(StatisticsModelManager aStatisticsModelManager) {
		this.statisticsModelManager = aStatisticsModelManager;
	}

	/**
	 * Funcion privada que inicia los datos de uso para un proceso de
	 * digitalización en modo de trabajo diferido, a traves de una carátula
	 * solicitada remotamente (mediante servicios web).
	 * 
	 * @param spacerRequest
	 *            Petición de generación de carátula que encapsulas datos
	 *            generales de generación de la carátula, relación de metadatos
	 *            y numero de separadores reutilizables a generar.
	 * @param docsBatchSpacerDTO
	 *            Objeto de la entidad carátula.
	 * @return DocScanStatisticsDTO
	 * @throws StatisticsDAOException
	 *             Si se produce algún error en el registro de la estadística de
	 *             la digitalización de documentos
	 */
	private void initStatistics(SpacerRequest spacerRequest, DocsBatchSpacerDTO docsBatchSpacerDTO) throws StatisticsDAOException {
		String excMsg;
		DocScanStatisticsDTO docScanStatisticsDTO = new DocScanStatisticsDTO();
		docScanStatisticsDTO.setWorkingMode(ScanWorkingMode.REMOTE_SPACER);
		docScanStatisticsDTO.setRemoteReqId(docsBatchSpacerDTO.getSpacerId());

		// Creamos la estadística
		try {
			statisticsModelManager.createDocScanStatistics(docScanStatisticsDTO);
		} catch (StatisticsDAOException e) {
			excMsg = "Error finalizando el registro de datos para la estadística " + e.getCode() + " - " + e.getMessage() + ".";
			LOG.error("[WEBSCAN-GV-STATISTICS] {}", excMsg);
			throw new StatisticsDAOException(StatisticsDAOException.CODE_100, e.getMessage());// NOPMD
		}
	}

}
