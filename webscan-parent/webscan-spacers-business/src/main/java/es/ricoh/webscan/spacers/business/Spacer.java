/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.business.Spacer.java.</p>
* <b>Descripción:</b><p> Carátula o separador reutilizable generado por el Sistema.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.business;

import java.io.Serializable;

import es.ricoh.webscan.utilities.MimeType;

/**
 * Carátula o separador reutilizable generado por el Sistema.
 * <p>
 * Clase Spacer.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class Spacer implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tipo MIME de la carátula o seprador reutilizable. Por defecto
	 * application/pdf.
	 */
	private String mimeType = MimeType.PDF_CONTENT.getType();

	/**
	 * Carátula o seprador reutilizable generado.
	 */
	private byte[ ] content = null;

	/**
	 * Constructor con argumentos de la clase.
	 * 
	 * @param aMimeType
	 *            Tipo MIME.
	 * @param anyContent
	 *            Carátula o seprador reutilizable generado.
	 */
	public Spacer(String aMimeType, byte[ ] anyContent) {
		this.mimeType = aMimeType;
		this.content = anyContent;
	}

	/**
	 * Obtiene el tipo MIME de la carátula o seprador reutilizable.
	 * 
	 * @return el tipo MIME de la carátula o seprador reutilizable.
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Obtiene la carátula o seprador reutilizable generado.
	 * 
	 * @return la carátula o seprador reutilizable generado.
	 */
	public byte[ ] getContent() {
		return content;
	}

}
