/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.business.service.SpacersGenerator.java.</p>
* <b>Descripción:</b><p> Servicio de generación del documento que contiene las
*  carátulas y/o separadores reutilizables de documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.business.service;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.CCITTFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.springframework.context.MessageSource;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.pdf417.PDF417Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import es.ricoh.webscan.spacers.business.SpacersException;
import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO;
import es.ricoh.webscan.WebscanConstants;

/**
 * Servicio de generación del documento que contiene las carátulas y/o
 * separadores reutilizables de documentos.
 * <p>
 * Clase SpacersGenerator.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacersGenerator {

	/**
	 * Parámetros de configuración específicos de la solución de digitalización
	 * de GV.
	 */
	private Properties digitalizationConfParams = new Properties();

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messageSource;

	/**
	 * Nombre del parámetro que especifica el margen en pixels de la página del
	 * documento que incluye una carátulas y/o separadores reutilizables.
	 */
	private static final String TABLE_MARGIN_PROP_NAME = "table.margin";

	/**
	 * Nombre del parámetro que especifica la altura de las filas de las tablas
	 * incluidas en carátulas y separadores reutilizables.
	 */
	private static final String TABLE_ROW_HEIGHT_PROP_NAME = "table.row.height";

	/**
	 * Nombre del parámetro que especifica el padding horizontal de las celdas
	 * de las tablas incluidas en carátulas y separadores reutilizables.
	 */
	private static final String TABLE_PADDING_X_PROP_NAME = "table.padding.x";

	/**
	 * Nombre del parámetro que especifica el padding vertical de las celdas de
	 * las tablas incluidas en carátulas y separadores reutilizables.
	 */
	private static final String TABLE_PADDING_Y_PROP_NAME = "table.padding.y";

	/**
	 * Nombre del parámetro que especifica el tamaño de la fuente empleada en
	 * las cabeceras de las tablas incluidas en carátulas y separadores
	 * reutilizables.
	 */
	private static final String TABLE_HEADER_FONT_SIZE_PROP_NAME = "table.header.font.size";

	/**
	 * Nombre del parámetro que especifica el tamaño de la fuente empleada en el
	 * cuerpo de las tablas incluidas en carátulas y separadores reutilizables.
	 */
	private static final String TABLE_BODY_FONT_SIZE_PROP_NAME = "table.body.font.size";

	/**
	 * Valor por defecto del parámetro que especifica la altura de las filas de
	 * las tablas incluidas en carátulas y separadores reutilizables.
	 */
	private static final float TABLE_ROW_HEIGHT_DEFAULT_VALUE = 16;

	/**
	 * Valor por defecto del parámetro que especifica el padding horizontal de
	 * las celdas de las tablas incluidas en carátulas y separadores
	 * reutilizables.
	 */
	private static final float TABLE_PADDING_X_DEFAULT_VALUE = 3;

	/**
	 * Valor por defecto del parámetro que especifica el padding vertical de las
	 * celdas de las tablas incluidas en carátulas y separadores reutilizables.
	 */
	private static final float TABLE_PADDING_Y_DEFAULT_VALUE = 2;

	/**
	 * Valor por defecto del parámetro que especifica el tamaño de la fuente
	 * empleada en las cabeceras de las tablas incluidas en carátulas y
	 * separadores reutilizables.
	 */
	private static final float TABLE_HEADER_FONT_SIZE_DEFAULT_VALUE = 12;

	/**
	 * Valor por defecto del parámetro que especifica el tamaño de la fuente
	 * empleada en el cuerpo de las tablas incluidas en carátulas y separadores
	 * reutilizables.
	 */
	private static final float TABLE_BODY_FONT_SIZE_DEFAULT_VALUE = 10;

	/**
	 * Valor por defecto del porcentaje de la tabla
	 */
	private static final float TABLE_BODY_PERCENT = 65;

	// Multidioma
	/**
	 * Nombre de la etiqueta spacers.label.user.
	 */
	private static final String USER_LABEL_PROP_NAME = "spacers.label.user";

	/**
	 * Nombre de la etiqueta spacers.label.user.
	 */
	private static final String SCAN_PROFILE_LABEL_PROP_NAME = "spacers.label.scanProfile";

	/**
	 * Nombre de la etiqueta spacers.label.validations.
	 */
	private static final String VAL_LABEL_PROP_NAME = "spacers.label.validations";

	/**
	 * Nombre de la etiqueta spacers.label.generationDate.
	 */
	private static final String GEN_DATE_LABEL_PROP_NAME = "spacers.label.generationDate";

	/**
	 * Nombre de la etiqueta spacers.label.coding.
	 */
	private static final String CODING_LABEL_PROP_NAME = "spacers.label.coding";

	/**
	 * Nombre de la etiqueta spacers.label.id.
	 */
	private static final String ID_LABEL_PROP_NAME = "spacers.label.id";

	/**
	 * Nombre de la etiqueta spacers.label.docsBatch.header.
	 */
	private static final String DB_HEADER_LABEL_PROP_NAME = "spacers.label.docsBatch.header";

	/**
	 * Nombre de la etiqueta spacers.label.reusableDocs.header.
	 */
	private static final String RD_HEADER_LABEL_PROP_NAME = "spacers.label.reusableDocs.header";

	/**
	 * Nombre del parámetro que especifica el formato de fechas empelado.
	 */
	private static final String DATE_PATTERN_PROP_NAME = "spacers.date.pattern";

	/**
	 * Nombre de la etiqueta label.DocSpacerType.
	 */
	private static final String DOC_SPACER_TYPE_LABEL_PROP_NAME = "label.DocSpacerType";

	/**
	 * Nombre de la etiqueta label.BatchDocSize.
	 */
	private static final String BATCH_DOC_SIZE_LABEL_PROP_NAME = "label.BatchDocSize";

	/**
	 * Nombre de la etiqueta label.BatchPages.
	 */
	private static final String BATCH_PAGES_LABEL_PROP_NAME = "label.BatchPages";

	/**
	 * Nombre de la etiqueta label.validations.empty.
	 */
	private static final String NO_VALIDATIONS_LABEL_PROP_NAME = "label.validations.empty";

	/**
	 * Nombre del parámetro que especifica el formato de fechas empelado.
	 */
	private static final String DATE_PATTERN_DEFAULT_VALUE = "dd/MM/yyyy hh:mm";

	/**
	 * Fuente tipo Bold.
	 */
	private static final PDFont FONT_TYPE_BOLD = PDType1Font.HELVETICA_BOLD;
	/**
	 * Fuente normal.
	 */
	private static final PDFont FONT_TYPE_NORMAL = PDType1Font.HELVETICA;

	/**
	 * Cabecera que precede al identificador de una carátula en el código
	 * barras.
	 */
	public static final String BATCH_CODE = "BATCH##";
	/**
	 * Cabecera que precede al identificador de un separador de documentos
	 * reutilizables en el código barras.
	 */
	public static final String DOCUMENT_CODE = "DOCUM##SPACER";

	/**
	 * Número mínimo de pixels del código de barras (alto).
	 */
	private static final int BARCODE_HEIGHT = 120;

	/**
	 * Número mínimo de pixels del código de barras (ancho).
	 */
	private static final int BARCODE_WIDTH = 120;

	/**
	 * Nombre de la etiqueta de multidioma cuyo valor es asignado a la propiedad
	 * que identifica el asunto de los archivos en formato PDF al generar
	 * carátulas.
	 */
	protected static final String BATCH_SUBJECT_PDF_PROPERTY_PARAM_NAME = "pdf.generation.properties.spacers.batch.subject";

	/**
	 * Nombre de la etiqueta de multidioma cuyo valor es asignado a la propiedad
	 * que identifica el asunto de los archivos en formato PDF al generar
	 * separadores reutilizables.
	 */
	protected static final String DOC_SUBJECT_PDF_PROPERTY_PARAM_NAME = "pdf.generation.properties.spacers.doc.subject";

	private File basePDF;

	public SpacersGenerator() throws Exception {
		this(null);
	}

	public SpacersGenerator(File basePdfFile) throws FileNotFoundException {
		if (basePdfFile == null || !basePdfFile.exists()) {
			return;
		}
		basePDF = basePdfFile;
	}

	private PDDocument getSourceDocument(Boolean isBatch, Locale locale) {
		PDDocument res;
		PDDocumentInformation pdd;

		if (basePDF != null && basePDF.exists()) {
			try {
				res = PDDocument.load(basePDF);
			} catch (IOException e) {
				return null;
			}
		} else {
			res = new PDDocument();
			res.addPage(new PDPage(PDRectangle.A4));
		}

		res.setVersion(WebscanConstants.VERSION_PDF);

		pdd = res.getDocumentInformation();
		pdd.setAuthor(messageSource.getMessage(WebscanConstants.CREATOR_PDF_PROPERTY_PARAM_NAME, null, locale));
		Calendar date = new GregorianCalendar();
		pdd.setCreationDate(date);
		pdd.setCreator(messageSource.getMessage(WebscanConstants.CREATOR_PDF_PROPERTY_PARAM_NAME, null, locale));
		pdd.setProducer(messageSource.getMessage(WebscanConstants.CREATOR_PDF_PROPERTY_PARAM_NAME, null, locale));
		if (isBatch) {
			pdd.setSubject(messageSource.getMessage(BATCH_SUBJECT_PDF_PROPERTY_PARAM_NAME, null, locale));
			pdd.setTitle(messageSource.getMessage(BATCH_SUBJECT_PDF_PROPERTY_PARAM_NAME, null, locale));
		} else {
			pdd.setSubject(messageSource.getMessage(DOC_SUBJECT_PDF_PROPERTY_PARAM_NAME, null, locale));
			pdd.setTitle(messageSource.getMessage(DOC_SUBJECT_PDF_PROPERTY_PARAM_NAME, null, locale));
		}
		// Se fuerzan las dimensiones de la página que contiene el código de
		// barras
		res.getPage(0).setMediaBox(PDRectangle.A4);

		return res;
	}

	private void createReusableSpacers(PDDocument src, final BatchSpacerType spacerType, final SpacerCodeAlignment aligment, final float hPercent, final int documentSpacers, final int skipPages, Locale locale) throws WriterException, IOException {
		// float margin = getFloatProperty(TABLE_MARGIN_PROP_NAME,
		// TABLE_MARGIN_DEFAULT_VALUE);
		DocsBatchSpacerDTO tmpDTO = new DocsBatchSpacerDTO();
		tmpDTO.setSpacerType(spacerType);

		HashSet<SpacerIncludedInfo> includes = new HashSet<>();
		includes.add(SpacerIncludedInfo.CODETYPE);
		float height = (float) Math.min(Math.max(hPercent, 0.1), 0.9);

		for (int i = 0; i < documentSpacers; i++) {
			PDPage page = src.getPage(i + skipPages);
			PDRectangle pageSize = page.getMediaBox();

			PDImageXObject pdImage = null;
			BufferedImage code;
			float codeX = 0, codeY = 0;
			code = getBarcode(DOCUMENT_CODE, spacerType, BARCODE_HEIGHT, BARCODE_WIDTH);
			pdImage = CCITTFactory.createFromImage(src, code);

			switch (aligment) {
				case Right:
					codeX = (pageSize.getWidth() - code.getWidth());
					break;
				case Left:
					codeX = 0;
					break;
				default:
					codeX = (pageSize.getWidth() - code.getWidth()) / 2f;
					break;
			}
			codeY = (pageSize.getHeight() - code.getHeight()) * (1 - height);
			PDPageContentStream stream = new PDPageContentStream(src, page, AppendMode.APPEND, false);
			if (pdImage != null) {
				stream.drawImage(pdImage, codeX, codeY, pdImage.getWidth(), pdImage.getHeight());
			}

			stream.close();

			float tableWidth = pageSize.getWidth() * 0.65f;
			float yStart = codeY - code.getHeight();
			float xStart = (pageSize.getWidth() - tableWidth) / 2f;
			createTable(src, page, tmpDTO, null, RD_HEADER_LABEL_PROP_NAME, includes, xStart, yStart, locale);
		}
	}

	public byte[ ] createReusableDocSpacers(BatchSpacerType spacerType, int documentSpacers, Locale locale) throws SpacersException {
		return createReusableDocSpacers(spacerType, SpacerCodeAlignment.Center, 0.5f, documentSpacers, locale);
	}

	public byte[ ] createReusableDocSpacers(BatchSpacerType spacerType, SpacerCodeAlignment aligment, float hPercent, int documentSpacers, Locale locale) throws SpacersException {
		PDDocument document = getSourceDocument(Boolean.FALSE, locale);
		PDPage page = document.getPage(0);

		// Creamos las páginas necesarias
		for (int i = 0; i < documentSpacers - 1; i++) {
			COSDictionary pageDict = page.getCOSObject();
			COSDictionary newPageDict = new COSDictionary(pageDict);
			newPageDict.removeItem(COSName.ANNOTS);
			PDPage newPage = new PDPage(newPageDict);
			document.addPage(newPage);
		}

		try {
			createReusableSpacers(document, spacerType, aligment, hPercent, documentSpacers, 0, locale);
		} catch (WriterException | IOException e1) {
			throw new SpacersException("Error en la creación del separador", e1);
		}

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			document.save(out);
			document.close();
			byte[ ] response = out.toByteArray();
			out.close();
			return response;
		} catch (IOException e) {
			throw new SpacersException("Error en la escritura del PDF", e);
		}
	}

	/**
	 * Crea un separador de lote, permitiendo añadir
	 * <code>documentSpacers</code> separadores de documentos en el mismo PDF.
	 * El código se situa en el centro a una altura del 30% de la página.
	 * 
	 * @param spacerDTO
	 *            Objeto que proporciona la información del separador.
	 * @param scanProfileName
	 *            Denominación del perfil de digitalización que será incluido en
	 *            la carátula.
	 * @param documentSpacers
	 *            Número de separadores reutilizables.
	 * @param locale
	 *            Configuración de idioma.
	 * @return Fichero PDF serializado
	 * @throws SpacersException
	 *             Si se produce algún error en la generación de la carátula.
	 */
	public byte[ ] createDocsBatchSpacer(DocsBatchSpacerDTO spacerDTO, String scanProfileName, int documentSpacers, Locale locale) throws SpacersException {
		return this.createDocsBatchSpacer(spacerDTO, scanProfileName, SpacerCodeAlignment.Center, 0.5f, documentSpacers, locale);
	}

	/**
	 * Crea un separador de lote, permitiendo añadir
	 * <code>documentSpacers</code> separadores de documentos en el mismo PDF
	 * 
	 * @param spacerDTO
	 *            Objeto que proporciona la información del separador.
	 * @param scanProfileName
	 *            Denominación del perfil de digitalización que será incluido en
	 *            la carátula.
	 * @param aligment
	 *            Alineación según {@link CodeAlignment}
	 * @param hPercent
	 *            Altura del código de barras (% de la página). >0.1 y <0.9
	 * @param documentSpacers
	 *            Número de separadores reutilizables.
	 * @param locale
	 *            Configuración de idioma.
	 * @return Fichero PDF serializado.
	 * @throws SpacersException
	 *             Si se produce algún error en la generación de la carátula.
	 */
	public byte[ ] createDocsBatchSpacer(DocsBatchSpacerDTO spacerDTO, String scanProfileName, SpacerCodeAlignment aligment, float hPercent, int documentSpacers, Locale locale) throws SpacersException {
		// float margin = getFloatProperty(TABLE_MARGIN_PROP_NAME,
		// TABLE_MARGIN_DEFAULT_VALUE);
		float percentTable = TABLE_BODY_PERCENT;
		PDDocument document = getSourceDocument(Boolean.TRUE, locale);
		PDPage page = document.getPage(0);
		float hPercentResult;

		// page 0. Batch
		PDRectangle pageSize = PDRectangle.A4;

		// Creamos las páginas necesarias
		for (int i = 0; i < documentSpacers; i++) {
			COSDictionary pageDict = page.getCOSObject();
			COSDictionary newPageDict = new COSDictionary(pageDict);
			newPageDict.removeItem(COSName.ANNOTS);
			PDPage newPage = new PDPage(newPageDict);
			newPage.setMediaBox(pageSize);
			document.addPage(newPage);
		}

		hPercentResult = (float) Math.min(Math.max(hPercent, 0.1), 0.9);
		PDImageXObject pdImage = null;
		BufferedImage code;
		float codeX = 0, codeY = 0;
		try {
			code = getBarcode(BATCH_CODE + spacerDTO.getSpacerId(), spacerDTO.getSpacerType(), BARCODE_HEIGHT, BARCODE_WIDTH);
			pdImage = CCITTFactory.createFromImage(document, code);

			switch (aligment) {
				case Right:
					codeX = (pageSize.getWidth() - code.getWidth());
					break;
				case Left:
					codeX = 0;
					break;
				default:
					codeX = (pageSize.getWidth() - code.getWidth()) / 2f;
					break;
			}
			codeY = (pageSize.getHeight() - code.getHeight()) * (1 - hPercentResult);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (WriterException e2) {
			e2.printStackTrace();
		}

		try {
			PDPageContentStream contents = new PDPageContentStream(document, page, AppendMode.APPEND, false);
			if (pdImage != null) {
				contents.drawImage(pdImage, codeX, codeY, pdImage.getWidth(), pdImage.getHeight());
			}
			contents.close();

			float tableWidth = pageSize.getWidth() * (percentTable / 100);
			float yStart = pageSize.getHeight() * 0.8f;
			float xStart = (pageSize.getWidth() - tableWidth) / 2f;

			HashSet<SpacerIncludedInfo> info = new HashSet<>();
			info.add(SpacerIncludedInfo.UUID);
			info.add(SpacerIncludedInfo.DATE);
			info.add(SpacerIncludedInfo.SCAN_PROFILE);
			info.add(SpacerIncludedInfo.VALIDATIONS);
			info.add(SpacerIncludedInfo.CODETYPE);
			info.add(SpacerIncludedInfo.USER);

			createTable(document, page, spacerDTO, scanProfileName, DB_HEADER_LABEL_PROP_NAME, info, xStart, yStart, locale);
			createReusableSpacers(document, spacerDTO.getSpacerType(), aligment, hPercentResult, documentSpacers, 1, locale);

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			document.save(out);
			document.close();

			byte[ ] response = out.toByteArray();
			out.close();
			return response;
		} catch (IOException | WriterException e) {
			throw new SpacersException("Error al crear los separadores", e);
		}
	}

	/**
	 * Define una tabla en la página <code>page</code> dentro del documento
	 * <code>document</code>.
	 * 
	 * @param document
	 *            Documento PDF.
	 * @param page
	 *            Página del documento.
	 * @param info
	 *            Objeto Carátula.
	 * @param scanProfileName
	 *            Denominación del perfil de digitalización que será incluido en
	 *            la carátula.
	 * @param headerLabel
	 *            propiedad que define el texto de la cabecera de la tabla de la
	 *            carátula.
	 * @param includes
	 *            información incluida en la tabla de la carátula.
	 * @param x0
	 *            Coordenada en el X inicial de la carátula.
	 * @param y0
	 *            Coordenada en el Y inicial de la carátula.
	 * @param locale
	 *            Configuración de idioma.
	 * @throws IOException
	 */
	private void createTable(PDDocument document, PDPage page, final DocsBatchSpacerDTO info, String scanProfileName, String headerLabel, final Set<SpacerIncludedInfo> includes, final float x0, final float y0, Locale locale) throws IOException {
		Boolean includeValidations = Boolean.FALSE;
		float tableRowHeight = getFloatProperty(TABLE_ROW_HEIGHT_PROP_NAME, TABLE_ROW_HEIGHT_DEFAULT_VALUE);
		float tablePaddingX = getFloatProperty(TABLE_PADDING_X_PROP_NAME, TABLE_PADDING_X_DEFAULT_VALUE);
		float tablePaddingY = getFloatProperty(TABLE_PADDING_Y_PROP_NAME, TABLE_PADDING_Y_DEFAULT_VALUE);
		float tableHeaderFontSize = getFloatProperty(TABLE_HEADER_FONT_SIZE_PROP_NAME, TABLE_HEADER_FONT_SIZE_DEFAULT_VALUE);
		float tableBodyFontSize = getFloatProperty(TABLE_BODY_FONT_SIZE_PROP_NAME, TABLE_BODY_FONT_SIZE_DEFAULT_VALUE);
		float percentTable = TABLE_BODY_PERCENT;
		PDRectangle pageSize = page.getMediaBox();

		float width = pageSize.getWidth() * (percentTable / 100);
		float leftCol = width * 0.4f;
		float rightCol = width * 0.6f;

		float y = y0;

		if (includes.isEmpty())
			return;

		PDPageContentStream stream = new PDPageContentStream(document, page, AppendMode.APPEND, false);
		// Header
		// Relleno fila header
		stream.setStrokingColor(Color.BLACK);
		stream.setNonStrokingColor(Color.LIGHT_GRAY);
		stream.addRect(x0, y, width, tableRowHeight);
		stream.fillAndStroke();
		// Texto header
		float headerTextWidth = FONT_TYPE_BOLD.getStringWidth(messageSource.getMessage(headerLabel, null, locale)) / 1000 * tableHeaderFontSize;
		stream.beginText();
		stream.setNonStrokingColor(Color.BLACK);
		stream.newLineAtOffset((pageSize.getWidth() - headerTextWidth) / 2f, (y + tableRowHeight / 2f) - tablePaddingY);
		stream.setFont(FONT_TYPE_BOLD, tableHeaderFontSize);
		stream.showText(messageSource.getMessage(headerLabel, null, locale));
		stream.endText();
		y -= tableRowHeight;

		if (includes.contains(SpacerIncludedInfo.UUID)) {
			// Identificador
			stream.setNonStrokingColor(Color.WHITE);
			stream.addRect(x0, y, leftCol, tableRowHeight);
			stream.stroke();
			stream.addRect(x0 + leftCol, y, rightCol, tableRowHeight);
			stream.stroke();

			stream.setNonStrokingColor(Color.BLACK);
			stream.beginText();
			stream.newLineAtOffset(x0 + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(messageSource.getMessage(ID_LABEL_PROP_NAME, null, locale));
			stream.endText();
			stream.beginText();
			stream.newLineAtOffset(x0 + leftCol + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(info.getSpacerId());
			stream.endText();
			y -= tableRowHeight;
		}
		if (includes.contains(SpacerIncludedInfo.CODETYPE)) {
			// Codificiación
			stream.setNonStrokingColor(Color.WHITE);
			stream.addRect(x0, y, leftCol, tableRowHeight);
			stream.stroke();
			stream.addRect(x0 + leftCol, y, rightCol, tableRowHeight);
			stream.stroke();

			stream.setNonStrokingColor(Color.BLACK);
			stream.beginText();
			stream.newLineAtOffset(x0 + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(messageSource.getMessage(CODING_LABEL_PROP_NAME, null, locale));
			stream.endText();
			stream.beginText();
			stream.newLineAtOffset(x0 + leftCol + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(messageSource.getMessage(info.getSpacerType().name(), null, info.getSpacerType().name(), locale));
			stream.endText();
			// yStart-=(fontSize + 2 * yPadding);
			y -= tableRowHeight;
		}
		if (includes.contains(SpacerIncludedInfo.DATE)) {
			// Fecha generación
			stream.setNonStrokingColor(Color.WHITE);
			stream.addRect(x0, y, leftCol, tableRowHeight);
			stream.stroke();
			stream.addRect(x0 + leftCol, y, rightCol, tableRowHeight);
			stream.stroke();

			stream.setNonStrokingColor(Color.BLACK);
			stream.beginText();
			stream.newLineAtOffset(x0 + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(messageSource.getMessage(GEN_DATE_LABEL_PROP_NAME, null, locale));
			stream.endText();
			stream.beginText();
			stream.newLineAtOffset(x0 + leftCol + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			SimpleDateFormat format = new SimpleDateFormat(messageSource.getMessage(DATE_PATTERN_PROP_NAME, null, DATE_PATTERN_DEFAULT_VALUE, locale));
			stream.showText(format.format(info.getGenerationDate()));
			stream.endText();
			y -= tableRowHeight;
		}
		if (includes.contains(SpacerIncludedInfo.USER)) {
			// Usuario
			stream.setNonStrokingColor(Color.WHITE);
			stream.addRect(x0, y, leftCol, tableRowHeight);
			stream.stroke();
			stream.addRect(x0 + leftCol, y, rightCol, tableRowHeight);
			stream.stroke();

			stream.setNonStrokingColor(Color.BLACK);
			stream.beginText();
			stream.newLineAtOffset(x0 + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(messageSource.getMessage(USER_LABEL_PROP_NAME, null, locale));
			stream.endText();

			stream.beginText();
			stream.newLineAtOffset(x0 + leftCol + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(info.getReqUserId());
			stream.endText();
			y -= tableRowHeight;
		}
		if (includes.contains(SpacerIncludedInfo.SCAN_PROFILE) && (scanProfileName != null && !scanProfileName.isEmpty())) {
			// Perfil de digitalización
			stream.setNonStrokingColor(Color.WHITE);
			stream.addRect(x0, y, leftCol, tableRowHeight);
			stream.stroke();
			stream.addRect(x0 + leftCol, y, rightCol, tableRowHeight);
			stream.stroke();

			stream.setNonStrokingColor(Color.BLACK);
			stream.beginText();
			stream.newLineAtOffset(x0 + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(messageSource.getMessage(SCAN_PROFILE_LABEL_PROP_NAME, null, locale));
			stream.endText();

			stream.beginText();
			stream.newLineAtOffset(x0 + leftCol + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(scanProfileName);
			stream.endText();
			y -= tableRowHeight;
		}
		if (includes.contains(SpacerIncludedInfo.VALIDATIONS)) {
			// Validaciones
			stream.setNonStrokingColor(Color.WHITE);
			stream.addRect(x0, y - tableRowHeight * 2, leftCol, tableRowHeight * 3);
			stream.stroke();
			stream.addRect(x0 + leftCol, y - tableRowHeight * 2, rightCol, tableRowHeight * 3);
			stream.stroke();
			stream.beginText();
			stream.setNonStrokingColor(Color.BLACK);
			stream.newLineAtOffset(x0 + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
			stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
			stream.showText(messageSource.getMessage(VAL_LABEL_PROP_NAME, null, locale));
			stream.endText();

			if (info.getBatchDocSize() != null) {
				includeValidations = Boolean.TRUE;
				stream.beginText();
				stream.newLineAtOffset(x0 + leftCol + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
				stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
				stream.showText(messageSource.getMessage(BATCH_DOC_SIZE_LABEL_PROP_NAME, null, locale) + ": " + info.getBatchDocSize());
				stream.endText();
				y -= tableRowHeight;
			}

			if (info.getBatchPages() != null) {
				includeValidations = Boolean.TRUE;
				stream.beginText();
				stream.newLineAtOffset(x0 + leftCol + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
				stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
				stream.showText(messageSource.getMessage(BATCH_PAGES_LABEL_PROP_NAME, null, locale) + ": " + info.getBatchPages());
				stream.endText();
				y -= tableRowHeight;
			}

			if (info.getDocSpacerType() != null) {
				includeValidations = Boolean.TRUE;
				stream.beginText();
				stream.newLineAtOffset(x0 + leftCol + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
				stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
				stream.showText(messageSource.getMessage(DOC_SPACER_TYPE_LABEL_PROP_NAME, null, locale) + ": " + messageSource.getMessage(info.getDocSpacerType().name(), null, info.getDocSpacerType().name(), locale));
				stream.endText();
				y -= tableRowHeight;
			}

			if (!includeValidations) {
				stream.beginText();
				stream.newLineAtOffset(x0 + leftCol + tablePaddingX, (y + tableRowHeight / 2f) - tablePaddingY);
				stream.setFont(FONT_TYPE_NORMAL, tableBodyFontSize);
				stream.showText(messageSource.getMessage(NO_VALIDATIONS_LABEL_PROP_NAME, null, locale));
				stream.endText();
				y -= tableRowHeight;
			}
		}

		stream.close();
	}

	private BufferedImage getBarcode(String value, BatchSpacerType type, int height, int width) throws WriterException {
		BufferedImage image;

		Hashtable<EncodeHintType, Object> hintMap = new Hashtable<>();
		BitMatrix byteMatrix = null;
		if (type == BatchSpacerType.CODE_QR) {
			QRCodeWriter qrCodeWriter = new QRCodeWriter();
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
			byteMatrix = qrCodeWriter.encode(value, BarcodeFormat.QR_CODE, width, height, hintMap);
		} else if (type == BatchSpacerType.CODE_128) {
			Code128Writer code128Writer = new Code128Writer();
			byteMatrix = code128Writer.encode(value, BarcodeFormat.CODE_128, width, height, hintMap);
		} else {
			PDF417Writer pdf417Writer = new PDF417Writer();
			byteMatrix = pdf417Writer.encode(value, BarcodeFormat.PDF_417, width, height, hintMap);
		}
		int matrixWidth = byteMatrix.getWidth();
		int matrixHeight = byteMatrix.getHeight();
		image = new BufferedImage(matrixWidth, matrixHeight, BufferedImage.TYPE_BYTE_BINARY);
		image.createGraphics();

		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, matrixWidth, matrixWidth);
		graphics.setColor(Color.BLACK);

		for (int i = 0; i < matrixWidth; i++) {
			for (int j = 0; j < matrixHeight; j++) {
				if (byteMatrix.get(i, j)) {
					graphics.fillRect(i, j, 1, 1);
				}
			}
		}
		return image;
	}

	/**
	 * Obtiene el valor de una propiedad de tipo String de la utilidad de
	 * generación de carátulas y separadores reutilizables.
	 * 
	 * @param key
	 *            identificador de un parámetro.
	 * @param defaultValue
	 *            valor por defecto del parámetro.
	 * @return el valor de una propiedad de la utilidad de generación de
	 *         carátulas y separadores reutilizables.
	 */
	public String getStringProperty(String key, String defaultValue) {
		return digitalizationConfParams.getProperty(key, defaultValue);
	}

	/**
	 * Obtiene el valor de una propiedad de tipo float de la utilidad de
	 * generación de carátulas y separadores reutilizables.
	 * 
	 * @param key
	 *            identificador de un parámetro.
	 * @param defaultValue
	 *            valor por defecto del parámetro.
	 * @return el valor de una propiedad de la utilidad de generación de
	 *         carátulas y separadores reutilizables.
	 */
	public float getFloatProperty(String key, float defaultValue) {
		String paramValue;
		float res = defaultValue;
		try {
			paramValue = digitalizationConfParams.getProperty(key);
			res = Float.parseFloat(paramValue);
		} catch (Exception e) {
			// No hacer nada
		}
		return res;
	}

	/**
	 * Obtiene el valor de una propiedad de tipo int de la utilidad de
	 * generación de carátulas y separadores reutilizables.
	 * 
	 * @param key
	 *            identificador de un parámetro.
	 * @param defaultValue
	 *            valor por defecto del parámetro.
	 * @return el valor de una propiedad de la utilidad de generación de
	 *         carátulas y separadores reutilizables.
	 */
	public int getIntegerProperty(String key, int defaultValue) {
		String paramValue;
		int res = defaultValue;
		try {
			paramValue = digitalizationConfParams.getProperty(key);
			res = Integer.parseInt(paramValue);
		} catch (Exception e) {
			// No hacer nada
		}
		return res;
	}

	/**
	 * Establece los parámetros de configuración específicos de la solución de
	 * digitalización.
	 * 
	 * @param anyDigitalizationConfParams
	 *            parámetros de configuración específicos de la solución de
	 *            digitalización.
	 */
	public void setDigitalizationConfParams(Properties anyDigitalizationConfParams) {
		this.digitalizationConfParams.clear();
		if (anyDigitalizationConfParams != null && !anyDigitalizationConfParams.isEmpty()) {
			digitalizationConfParams.putAll(anyDigitalizationConfParams);
		}
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param aMessageSource
	 *            Objeto que recopila los mensajes, sujetos a multidioma, que
	 *            son presentados al usuario.
	 */
	public void setMessageSource(MessageSource aMessageSource) {
		this.messageSource = aMessageSource;
	}

}
