/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.business.service.SpacerIncludedInfo.java.</p>
* <b>Descripción:</b><p> Información mostrada en las carátulas y separadores reutilizables de documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.business.service;

/**
 * Información mostrada en las carátulas y separadores reutilizables de
 * documentos.
 * <p>
 * Clase SpacerIncludedInfo.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum SpacerIncludedInfo {
	/**
	 * UUID carátula.
	 */
	UUID,
	/**
	 * Tipo de código empleado.
	 */
	CODETYPE,
	/**
	 * Fecha de generación.
	 */
	DATE,
	/**
	 * Validaciones a aplicar sobre un lote de documentos.
	 */
	VALIDATIONS,
	/**
	 * Identificación del usuario que solicita la carátula o el separador
	 * reutilizable.
	 */
	USER,
	/**
	 * Denominación del perfil de digitalización de la carátula.
	 */
	SCAN_PROFILE;
}
