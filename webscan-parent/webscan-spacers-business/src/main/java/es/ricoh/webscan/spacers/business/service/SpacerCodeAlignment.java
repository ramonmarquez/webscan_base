/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.business.service.SpacerCodeAlignment.java.</p>
* <b>Descripción:</b><p> Tipos de alineación de códigos de barra en carátulas y sepradores reutilizables.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.business.service;

/**
 * Tipos de alineación de códigos de barra en carátulas y sepradores
 * reutilizables.
 * <p>
 * Clase SpacerCodeAlignment.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum SpacerCodeAlignment {
	/**
	 * Alineación izquierda.
	 */
	Left,
	/**
	 * Alineación centro.
	 */
	Center,
	/**
	 * Alineación derecha.
	 */
	Right;
}
