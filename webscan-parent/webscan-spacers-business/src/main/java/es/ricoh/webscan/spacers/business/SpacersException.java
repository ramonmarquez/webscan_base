/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.business.SpacersException.java.</p>
* <b>Descripción:</b><p> Excepción que encapsula los errores de negocio de la utilidad de generación
 * de carátulas y separadores reutilizables.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.business;

/**
 * Excepción que encapsula los errores de negocio de la utilidad de generación
 * de carátulas y separadores reutilizables.
 * <p>
 * Clase SpacersException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacersException extends Exception {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Perfil de digitalización informado en la petición de generación de
	 * carátula de lote de documentos no registrado en el Sistema.
	 */
	public static final String CODE_001 = "COD_001";

	/**
	 * Definición de documentos informada en la petición de generación de
	 * carátula de lote de documentos no registrada en el Sistema.
	 */
	public static final String CODE_002 = "COD_002";

	/**
	 * Definición de documentos informada no asociada al perfil de
	 * digitalización especificado en la petición de generación de carátula de
	 * lote de documentos.
	 */
	public static final String CODE_003 = "COD_003";

	/**
	 * La relación de metadatos posee metadatos repetidos.
	 */
	public static final String CODE_004 = "COD_004";

	/**
	 * Número de separadores reutilizables inferior a 1.
	 */
	public static final String CODE_005 = "COD_005";

	/**
	 * Error interno al actualizar el modelo de datos para la persistencia de
	 * datos de uso para explotación estadística.
	 */
	public static final String CODE_006 = "COD_006";

	/**
	 * Error interno al actualizar el modelo de datos base al registrar trazas
	 * de auditoria.
	 */
	public static final String CODE_007 = "COD_007";

	/**
	 * Perfil de digitalización informado no activo.
	 */
	public static final String CODE_008 = "COD_008";

	/**
	 * Definición de documentos informada no activa.
	 */
	public static final String CODE_009 = "COD_009";

	/**
	 * Datos o parámetros mal formados.
	 */
	public static final String CODE_998 = "COD_998";

	/**
	 * Causa del error desconocida.
	 */
	public static final String CODE_999 = "COD_999";

	/**
	 * Código del error.
	 */
	protected String code = CODE_999;

	/**
	 * Constructor sin argumentos.
	 */
	public SpacersException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public SpacersException(String aCode, String aMessage) {
		super(aMessage);
		this.code = aCode;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public SpacersException(String aMessage) {
		super(aMessage);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public SpacersException(Throwable aCause) {
		super(aCause);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public SpacersException(final String aMessage, Throwable aCause) {
		super(aMessage, aCause);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public SpacersException(final String aCode, final String aMessage, Throwable aCause) {
		super(aMessage, aCause);
		this.code = CODE_999;
	}

	/**
	 * Obtiene el código de error de la excepción.
	 *
	 * @return código de error de la excepción.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Establece el código de error de la excepción.
	 *
	 * @param aCode
	 *            código de error de la excepción.
	 */
	public void setCode(final String aCode) {
		this.code = aCode;
	}

}
