/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.UsersWebscanModelUtilities.java.</p>
 * <b>Descripción:</b><p> Clase de utilidades del componente que implementa la capa de
 * acceso al modelo de datos de gestión de usuarios de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase de utilidades del componente que implementa la capa de acceso al modelo
 * de datos de gestión de usuarios de Webscan.
 * <p>
 * Clase UsersWebscanModelUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UsersWebscanModelUtilities {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UsersWebscanModelUtilities.class);

	/**
	 * Suma un número de días dado a una fecha determinada.
	 * 
	 * @param date
	 *            Fecha.
	 * @param days
	 *            Número de días a sumar.
	 * @return Nueva fecha resultante de sumar un número de días dado a la fecha
	 *         especificada como parámetro.
	 */
	static Date addDaysToDate(Date date, Integer days) {
		Date res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Calculando una nueva fecha a partir de la fecha " + date + " ...");
			LOG.debug("[WEBSCAN-USERS-MODEL] Número de días a sumar " + days + " ...");
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);

		res = cal.getTime();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Fecha resultante " + res + " ...");
		}
		return res;
	}

}
