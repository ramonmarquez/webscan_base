/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.dao.UserDAO.java.</p>
 * <b>Descripción:</b><p> Interfaz que define la operaciones de la capa DAO sobre las entidades usuario, perfil de usuario y petición de
	 * recuperación de password, pertenecientes al modelo de datos de gestión de usuarios de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model.dao;

import java.util.List;
import java.util.Map;

import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.model.domain.RecoveryUserPasswordRequestStatus;
import es.ricoh.webscan.users.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.users.model.dto.ProfileDTO;
import es.ricoh.webscan.users.model.dto.RecoveryUserPasswordRequestDTO;
import es.ricoh.webscan.users.model.dto.UserDTO;

/**
 * Interfaz que define la operaciones de la capa DAO sobre las entidades
 * usuario, perfil de usuario y petición de recuperación de password,
 * pertenecientes al modelo de datos de gestión de usuarios de Webscan.
 * <p>
 * Clase UserDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface UserDAO {

	// Operaciones sobre usuarios
	/**
	 * Obtiene el listado de usuarios registrados en el sistema, permitiendo la
	 * paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de usuarios.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<UserDTO> listUsers(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws UsersDAOException;

	/**
	 * Obtiene el listado de usuarios registrados en el sistema, y sus perfiles
	 * o roles de usuarios, permitiendo la paginación y ordenación del
	 * resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de usuarios.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	Map<UserDTO, List<ProfileDTO>> listUsersAndProfiles(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws UsersDAOException;

	/**
	 * Obtiene el listado de usuarios pertenecientes a un unidad organizativa
	 * determinada, permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de usuarios.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<UserDTO> getUsersByOrganizationUnit(Long orgUnitId, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws UsersDAOException;

	/**
	 * Obtiene un usuario a partir de su identificador.
	 * 
	 * @param userId
	 *            Identificador de especificación de plugin.
	 * @return La definición de plugin solicitada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	UserDTO getUser(Long userId) throws UsersDAOException;

	/**
	 * Obtiene un usuario a partir de su nombre de usuario.
	 * 
	 * @param username
	 *            nombre de usuario.
	 * @return El usuario solicitado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	UserDTO getUserByUsername(String username) throws UsersDAOException;

	/**
	 * Obtiene un usuario a partir de su documento de identificación.
	 * 
	 * @param document
	 *            documento de identificación.
	 * @return El usuario solicitado.
	 * @throws UsersDAOException
	 *             Si no existe el usuario solicitado, o se produce algún error
	 *             en la consulta a BBDD.
	 */
	UserDTO getUserByDocument(String document) throws UsersDAOException;

	/**
	 * Obtiene un usuario a partir de su dirección de correo.
	 * 
	 * @param email
	 *            dirección de correo.
	 * @return El usuario solicitado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	UserDTO getUserByEmail(String email) throws UsersDAOException;

	/**
	 * Crea un nuevo usuario en el sistema.
	 * 
	 * @param user
	 *            Nueva usuario.
	 * @return Identificador del usuario creado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createUser(UserDTO user) throws UsersDAOException;

	/**
	 * Actualiza la información de un usuario registrado en el sistema. Los
	 * datos actualizados son: documento de identificación, dirección de correo
	 * electrónico, número de intentos consecutivos fallidos de acceso,
	 * indicador de bloqueo, nombre de pila, primer apellido, segundo apellido y
	 * nombre de usuario.
	 * 
	 * @param user
	 *            Usuario actualizado.
	 * @param setUpdateDate
	 *            Indica si se debe actualizar la fecha de actualización de
	 *            información del usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void updateUser(UserDTO user, Boolean setUpdateDate) throws UsersDAOException;

	/**
	 * Elimina un usuario del sistema.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void removeUser(Long userId) throws UsersDAOException;

	// Operaciones sobre perfiles de usuarios
	/**
	 * Obtiene el listado de perfiles de usuarios registrados en el sistema.
	 * 
	 * @return listado de perfiles de usuario registrados en el sistema.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ProfileDTO> listUserProfiles() throws UsersDAOException;

	/**
	 * Obtiene el listado de perfiles configurado para un usuario registrado en
	 * el sistema.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @return listado de perfiles del usuario especificado como parámetro de
	 *         entrada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ProfileDTO> getUserProfilesByUser(Long userId) throws UsersDAOException;

	/**
	 * Establece los perfiles de un usuario registrado en el sistema.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @param userProfiles
	 *            Nueva relación de perfiles del usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	void setUserProfiles(Long userId, List<ProfileDTO> userProfiles) throws UsersDAOException;

	// Operaciones sobre usuarios y unidades organizativas
	/**
	 * Recupera una relación de unidades organizativas a partir de sus
	 * identificadores.
	 * 
	 * @param orgUnitIds
	 *            Relación de identificadores internos de unidades
	 *            oragnizativas.
	 * @return Lista de unidades organizativas a las que pertence un usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<OrganizationUnitDTO> getOrganizationUnitsByIds(List<Long> orgUnitIds) throws UsersDAOException;

	/**
	 * Recupera la relación de unidades organizativas a las que pertence un
	 * usuario.
	 * 
	 * @param userId
	 *            Identificador interno de usuario.
	 * @return Lista de unidades organizativas a las que pertence un usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<OrganizationUnitDTO> getOrganizationUnitsByUserId(Long userId) throws UsersDAOException;

	/**
	 * Establece las unidades organizativas a las que pertenece un usuario
	 * registrado en el sistema.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @param orgUnitIds
	 *            Nueva relación de unidades organizativas a las que pertenece
	 *            un usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	void setUserOrganizationUnits(Long userId, List<OrganizationUnitDTO> orgUnitIds) throws UsersDAOException;

	/**
	 * Establece una nueva relación de usuarios para una unidad organizativa
	 * determinada.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @param users
	 *            Nueva relación de usuarios de la unidad organizativa
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             unidad organizativa, o se produce algún error al ejecutar las
	 *             sentencias SQL de inserción y actualización en BBDD.
	 */
	void setOrganizationUnitUsers(Long orgUnitId, List<UserDTO> users) throws UsersDAOException;

	// Operaciones sobre solicitudes de recuperación de passwords
	/**
	 * Obtiene las peticiones de recuperación de password para un usuario y
	 * estado determinados.
	 * 
	 * @param userEmail
	 *            Dirección de correo de un usuario.
	 * @param status
	 *            Estado de peticiones de recuperación de password.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return La lista de peticiones de recuperación de password solicitadas
	 *         por el usuario especificado como parámetro de entrada. Lista
	 *         vacía si no ha realizado ninguna.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<RecoveryUserPasswordRequestDTO> getRecoveryPassReqByUserAndStatus(String userEmail, RecoveryUserPasswordRequestStatus status, List<EntityOrderByClause> orderByColumns) throws UsersDAOException;

	/**
	 * Obtiene una petición de recuperación de password a partir de la dirección
	 * de correo electrónico del usuario solicitante.
	 * 
	 * @param userEmail
	 *            Dirección de correo electrónico del usuario solicitante.
	 * @return La petición de recuperación de password solicitada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             petición solicitada, o se produce algún error en la consulta
	 *             a BBDD.
	 */
	List<RecoveryUserPasswordRequestDTO> getRecoveryPassReqInProcessByUserEmail(String userEmail) throws UsersDAOException;

	/**
	 * Obtiene las peticiones de recuperación de password para un usuario
	 * determinado.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return La lista de peticiones de recuperación de password solicitadas
	 *         por el usuario especificado como parámetro de entrada. Lista
	 *         vacía si no ha realizado ninguna.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<RecoveryUserPasswordRequestDTO> getRecoveryPassReqsByUserId(Long userId, List<EntityOrderByClause> orderByColumns) throws UsersDAOException;

	/**
	 * Obtiene una petición de recuperación de password a partir de su
	 * identificador.
	 * 
	 * @param recPassReqId
	 *            Identificador de petición de recuperación de password.
	 * @return La petición de recuperación de password solicitada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             petición solicitada, o se produce algún error en la consulta
	 *             a BBDD.
	 */
	RecoveryUserPasswordRequestDTO getRecoveryPassReq(Long recPassReqId) throws UsersDAOException;

	/**
	 * Crea una nueva petición de recuperación de password en el sistema.
	 * 
	 * @param recUserPassReq
	 *            Nueva petición de recuperación de password.
	 * @return Identificador de la petición de recuperación de password creada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createRecoveryUserPasswordRequest(RecoveryUserPasswordRequestDTO recUserPassReq) throws UsersDAOException;

	/**
	 * Actualiza una petición de recuperación de password existente en el
	 * sistema.
	 * 
	 * @param recUserPassReq
	 *            Nueva petición de recuperación de password.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             petición solicitada, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	void updateRecoveryUserPasswordRequest(RecoveryUserPasswordRequestDTO recUserPassReq) throws UsersDAOException;

}
