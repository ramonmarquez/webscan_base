/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.UsersDAOException.java.</p>
 * <b>Descripción:</b><p> Excepción que encapsula los errores producidos en el
 * componente DAO el modelo de datos de gestión de usuarios de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model;

import es.ricoh.webscan.model.DAOException;

/**
 * Excepción que encapsula los errores producidos en el componente DAO el modelo
 * de datos de gestión de usuarios de Webscan.
 * <p>
 * Clase UsersDAOException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UsersDAOException extends DAOException {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Usuario bloqueado o no habilitado.
	 */
	public static final String CODE_800 = "COD_800";

	/**
	 * Credenciales o petición de usuario caducada.
	 */
	public static final String CODE_801 = "COD_801";

	/**
	 * Credenciales incorrectas.
	 */
	public static final String CODE_802 = "COD_802";

	/**
	 * Ya existe un usuario con el mismo nombre de usuario.
	 */
	public static final String CODE_820 = "COD_820";

	/**
	 * Ya existe un usuario con la misma dirección de correo.
	 */
	public static final String CODE_821 = "COD_821";

	/**
	 * Ya existe un usuario con el mismo documento de identificación.
	 */
	public static final String CODE_822 = "COD_822";

	/**
	 * Existe más de una petición de recuperación en curso.
	 */
	public static final String CODE_850 = "COD_850";

	/**
	 * Constructor sin argumentos.
	 */
	public UsersDAOException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public UsersDAOException(String aCode, String aMessage) {
		super(aCode, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public UsersDAOException(String aMessage) {
		super(CODE_999, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public UsersDAOException(Throwable aCause) {
		super(CODE_999, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public UsersDAOException(final String aMessage, Throwable aCause) {
		super(CODE_999, aMessage, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public UsersDAOException(final String aCode, final String aMessage, Throwable aCause) {
		super(aCode, aMessage, aCause);
	}
}
