/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.dto.RecoveryUserPasswordRequestDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA petición de recuperación de password.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model.dto;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.users.model.domain.RecoveryUserPasswordRequestStatus;

/**
 * DTO para la entidad JPA petición de recuperación de password.
 * <p>
 * Clase RecoveryUserPasswordRequestDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class RecoveryUserPasswordRequestDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador del perfil de usuario.
	 */
	private Long id;

	/**
	 * Token de seguridad de la petición.
	 */
	private String securityToken;

	/**
	 * Fecha de envío de la comunicación al usuario.
	 */
	private Date comSendingDate;

	/**
	 * Fecha de finalización de la petición.
	 */
	private Date recoveryDate;

	/**
	 * Estado de la petición.
	 */
	private RecoveryUserPasswordRequestStatus status;

	/**
	 * Identificador del usuario que realiza la petición.
	 */
	private Long userId;

	/**
	 * Identificador de la traza de auditoría asociada a la petición.
	 */
	private Long auditOpId;

	/**
	 * Constructor sin argumentos.
	 */
	public RecoveryUserPasswordRequestDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la petición.
	 * 
	 * @return el identificador de la petición.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la petición.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la petición.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el token de seguridad de la petición.
	 * 
	 * @return el token de seguridad de la petición.
	 */
	public String getSecurityToken() {
		return securityToken;
	}

	/**
	 * Establece un nuevo valor para el token de seguridad de la petición.
	 * 
	 * @param aSecurityToken
	 *            nuevo valor para el token de seguridad de la petición.
	 */
	public void setSecurityToken(String aSecurityToken) {
		this.securityToken = aSecurityToken;
	}

	/**
	 * Obtiene la fecha de envío de la comunicación al usuario.
	 * 
	 * @return la fecha de envío de la comunicación al usuario.
	 */
	public Date getComSendingDate() {
		return comSendingDate;
	}

	/**
	 * Establece una nueva fecha de envío de la comunicación al usuario.
	 * 
	 * @param aComSendingDate
	 *            nuevo fecha de envío de la comunicación al usuario.
	 */
	public void setComSendingDate(Date aComSendingDate) {
		this.comSendingDate = aComSendingDate;
	}

	/**
	 * Obtiene la fecha de finalización de la petición.
	 * 
	 * @return la fecha de finalización de la petición.
	 */
	public Date getRecoveryDate() {
		return recoveryDate;
	}

	/**
	 * Establece una nueva fecha de finalización de la petición.
	 * 
	 * @param aRecoveryDate
	 *            nuevo fecha de de finalización de la petición.
	 */
	public void setRecoveryDate(Date aRecoveryDate) {
		this.recoveryDate = aRecoveryDate;
	}

	/**
	 * Obtiene el estado de la petición.
	 * 
	 * @return el estado de la petición.
	 */
	public RecoveryUserPasswordRequestStatus getStatus() {
		return status;
	}

	/**
	 * Establece el estado de la petición.
	 * 
	 * @param aStatus
	 *            nuevo estado de la petición.
	 */
	public void setStatus(RecoveryUserPasswordRequestStatus aStatus) {
		this.status = aStatus;
	}

	/**
	 * Obtiene el identificador del usuario que realizó de la petición.
	 * 
	 * @return el identificador del usuario que realizó de la petición.
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * Establece el identificador del usuario que realizó de la petición.
	 * 
	 * @param anUserId
	 *            identificador del usuario que realizó de la petición.
	 */
	public void setUserId(Long anUserId) {
		this.userId = anUserId;
	}

	/**
	 * Obtiene el identificador de la traza de auditoría asociada a la petición.
	 * 
	 * @return el identificador de la traza de auditoría asociada a la petición.
	 */
	public Long getAuditOpId() {
		return auditOpId;
	}

	/**
	 * Establece el identificador de la traza de auditoría asociada a la
	 * petición.
	 * 
	 * @param anAuditOpId
	 *            identificador de la traza de auditoría asociada a la petición.
	 */
	public void setAuditOpId(Long anAuditOpId) {
		this.auditOpId = anAuditOpId;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecoveryUserPasswordRequestDTO other = (RecoveryUserPasswordRequestDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
