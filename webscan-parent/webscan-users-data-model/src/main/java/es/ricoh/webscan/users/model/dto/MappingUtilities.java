/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.dto.MappingUtilities.java.</p>
 * <b>Descripción:</b><p> Clase de utilidades para la transformación de entidades JPA, pertenecientes
 * al modelos de gestión de usuarios de Webscan, en objetos DTO, y viceversa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.users.model.domain.OrganizationUnit;
import es.ricoh.webscan.users.model.domain.Profile;
import es.ricoh.webscan.users.model.domain.RecoveryUserPasswordRequest;
import es.ricoh.webscan.users.model.domain.User;

/**
 * Clase de utilidades para la transformación de entidades JPA, pertenecientes
 * al modelos de gestión de usuarios de Webscan, en objetos DTO, y viceversa.
 * <p>
 * Clase MappingUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class MappingUtilities {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MappingUtilities.class);

	/**
	 * Transforma una entidad de BBDD usuario en un objeto UserDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD usuario.
	 * @return Objeto UserDTO.
	 */
	public static UserDTO fillUserDto(User entity) {
		UserDTO res = new UserDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setUsername(entity.getUsername());
			res.setPassword(entity.getPassword());
			res.setName(entity.getName());
			res.setSurname(entity.getSurname());
			res.setSecondSurname(entity.getSecondSurname());
			res.setDocument(entity.getDocument());
			res.setEmail(entity.getEmail());
			res.setLocked(entity.getLocked());
			res.setCreationDate(entity.getCreationDate());
			res.setLastUpdateDate(entity.getLastUpdateDate());
			res.setNextPassUpdateDate(entity.getNextPassUpdateDate());
			res.setFailedAttempts(entity.getFailedAttempts());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto UserDTO en una entidad de BBDD usuario.
	 * 
	 * @param dto
	 *            objeto UserDTO.
	 * @return Objeto entidad de BBDD usuario.
	 */
	public static User fillUserEntity(UserDTO dto) {
		User res = new User();

		if (dto != null) {
			res.setId(dto.getId());
			res.setUsername(dto.getUsername());
			res.setPassword(dto.getPassword());
			res.setName(dto.getName());
			res.setSurname(dto.getSurname());
			res.setSecondSurname(dto.getSecondSurname());
			res.setDocument(dto.getDocument());
			res.setEmail(dto.getEmail());
			res.setLocked(dto.getLocked());
			res.setCreationDate(dto.getCreationDate());
			res.setLastUpdateDate(dto.getLastUpdateDate());
			res.setNextPassUpdateDate(dto.getNextPassUpdateDate());
			res.setFailedAttempts(dto.getFailedAttempts());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD usuario en una lista de objetos
	 * UserDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD usuario.
	 * @return lista de objetos UserDTO.
	 */
	public static List<UserDTO> fillUserListDto(List<User> entities) {
		int queryResultSize = 0;
		List<UserDTO> res = new ArrayList<UserDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (User user: entities) {
				res.add(MappingUtilities.fillUserDto(user));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Usuarios parseados: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD usuario y perfil de usuario en
	 * un mapa de objetos UserDTO y List<ProfileDTO>.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD usuario y perfil de usuario.
	 * @return mapa de objetos UserDTO y List<ProfileDTO>.
	 */
	public static Map<UserDTO, List<ProfileDTO>> fillUserAndProfileListDto(List<Object[ ]> entities) {
		int queryResultSize = 0;
		Map<UserDTO, List<ProfileDTO>> res = new HashMap<UserDTO, List<ProfileDTO>>();
		ProfileDTO profileDto;
		UserDTO userDto;

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (Object[ ] userProfile: entities) {
				userDto = MappingUtilities.fillUserDto((User) userProfile[0]);
				profileDto = MappingUtilities.fillProfileDto((Profile) userProfile[1]);

				if (res.get(userDto) == null) {
					res.put(userDto, new ArrayList<ProfileDTO>());
				}
				res.get(userDto).add(profileDto);
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Usuarios parseados: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD perfil de usuario en un objeto ProfileDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD perfil de usuario.
	 * @return Objeto ProfileDTO.
	 */
	public static ProfileDTO fillProfileDto(Profile entity) {
		ProfileDTO res = new ProfileDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setName(entity.getName());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ProfileDTO en una entidad de BBDD
	 * perfil de usuario.
	 * 
	 * @param dto
	 *            objeto ProfileDTO.
	 * @return Objeto entidad de BBDD perfil de usuario.
	 */
	public static Profile fillProfileEntity(ProfileDTO dto) {
		Profile res = new Profile();

		if (dto != null) {
			res.setId(dto.getId());
			res.setName(dto.getName());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD perfil de usuario en una lista
	 * de objetos UserDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD perfil de usuario.
	 * @return lista de objetos ProfileDTO.
	 */
	public static List<ProfileDTO> fillProfileListDto(List<Profile> entities) {
		int queryResultSize = 0;
		List<ProfileDTO> res = new ArrayList<ProfileDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (Profile profile: entities) {
				res.add(MappingUtilities.fillProfileDto(profile));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Perfiles de usuario parseados: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD petición de recuperación de password en un
	 * objeto RecoveryUserPasswordRequestDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD petición de recuperación de password.
	 * @return Objeto RecoveryUserPasswordRequestDTO.
	 */
	public static RecoveryUserPasswordRequestDTO fillRecoveryUserPasswordRequestDto(RecoveryUserPasswordRequest entity) {
		RecoveryUserPasswordRequestDTO res = new RecoveryUserPasswordRequestDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setComSendingDate(entity.getComSendingDate());
			res.setRecoveryDate(entity.getRecoveryDate());
			res.setSecurityToken(entity.getSecurityToken());
			res.setStatus(entity.getStatus());
			res.setUserId(entity.getUser().getId());
			res.setAuditOpId(entity.getAuditOpId());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto RecoveryUserPasswordRequestDTO en una
	 * entidad de BBDD petición de recuperación de password.
	 * 
	 * @param dto
	 *            objeto RecoveryUserPasswordRequestDTO.
	 * @return Objeto entidad de BBDD petición de recuperación de password.
	 */
	public static RecoveryUserPasswordRequest fillRecoveryUserPasswordRequestEntity(RecoveryUserPasswordRequestDTO dto) {
		RecoveryUserPasswordRequest res = new RecoveryUserPasswordRequest();

		if (dto != null) {
			res.setId(dto.getId());
			res.setComSendingDate(dto.getComSendingDate());
			res.setRecoveryDate(dto.getRecoveryDate());
			res.setSecurityToken(dto.getSecurityToken());
			res.setStatus(dto.getStatus());
			res.setAuditOpId(dto.getAuditOpId());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD petición de recuperación de
	 * password en una lista de objetos RecoveryUserPasswordRequestDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD petición de recuperación de
	 *            password.
	 * @return lista de objetos RecoveryUserPasswordRequestDTO.
	 */
	public static List<RecoveryUserPasswordRequestDTO> fillRecoveryUserPasswordRequestListDto(List<RecoveryUserPasswordRequest> entities) {
		int queryResultSize = 0;
		List<RecoveryUserPasswordRequestDTO> res = new ArrayList<RecoveryUserPasswordRequestDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (RecoveryUserPasswordRequest rupr: entities) {
				res.add(MappingUtilities.fillRecoveryUserPasswordRequestDto(rupr));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Peticiones de recuperación de password parseadas: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD unidad organizativa en un objeto
	 * OrganizationUnitDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD unidad organizativa.
	 * @return Objeto OrganizationUnitDTO.
	 */
	public static OrganizationUnitDTO fillOrganizationUnitDto(OrganizationUnit entity) {
		OrganizationUnitDTO res = new OrganizationUnitDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setExternalId(entity.getExternalId());
			res.setName(entity.getName());

		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD unidad organizativa en una
	 * lista de objetos OrganizationUnitDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD unidad organizativa.
	 * @return lista de objetos OrganizationUnitDTO.
	 */
	public static List<OrganizationUnitDTO> fillOrganizationUnitListDto(List<OrganizationUnit> entities) {
		int queryResultSize = 0;
		List<OrganizationUnitDTO> res = new ArrayList<OrganizationUnitDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (OrganizationUnit orgUnit: entities) {
				res.add(MappingUtilities.fillOrganizationUnitDto(orgUnit));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Unidades organizativas parseadas: {}", queryResultSize);
		return res;
	}
}
