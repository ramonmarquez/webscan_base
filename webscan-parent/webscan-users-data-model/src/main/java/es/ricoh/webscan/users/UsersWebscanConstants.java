/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.UsersWebscanConstants.java.</p>
 * <b>Descripción:</b><p> Interfaz que agrupa constantes del componente que implementa la capa de
 * acceso a datos al modelo de datos de gestión de usuarios de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users;

import es.ricoh.webscan.model.WebscanModelConstants;

/**
 * Interfaz que agrupa constantes del componente que implementa la capa de
 * acceso a datos al modelo de datos de gestión de usuarios de Webscan.
 * <p>
 * Clase UsersWebscanConstants.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.WebscanModelConstants
 * @version 2.0.
 */
public interface UsersWebscanConstants extends WebscanModelConstants {

	// Parametros de configuración
	/**
	 * Parámetro que configura el periodo máximo de validez de las password de
	 * usuario.
	 */
	String PASS_MAX_VAL_PERIOD_PROP_NAME = "user.info.password.maxValidityPeriod";

	/**
	 * Parámetro que configura el periodo previo de información de password
	 * caducada.
	 */
	String PASS_DAYS_WARN_EXPIRED_PROP_NAME = "user.info.password.daysWarnExpiredPass";

	/**
	 * Parámetro que configura el periodo máximo de validez de una petición de
	 * recuperación de password de usuario.
	 */
	String REC_PASS_MAX_VAL_PERIOD_PROP_NAME = "recoveryPassword.request.maxValidityPeriod";

	/**
	 * Parámetro que configura el número máximo de intentos para acceder al
	 * sistema.
	 */
	String LOGIN_MAX_ATTEMPS_PROP_NAME = "login.maxNumberOfRetries";

	/**
	 * Parámetro que configura el patrón de las password de usuario.
	 */
	String PASSWORD_PATTERN_PROP_NAME = "password.pattern";

	/**
	 * Parámetro que establece el número máximo de intentos para acceder al
	 * sistema por defecto.
	 */
	Integer DEFAULT_MAX_ATTEMPS_VALUE = -1;

	/**
	 * Parámetro que establece el número días por defecto a partir del cual se
	 * informará de la caducidad de la password de un usuario.
	 */
	Integer DEFAULT_DAYS_WARN_EXPIRED_PASS = 15;

	// Mensajes de error de autenticación
	/**
	 * Parámetro que representa el mensaje de error por credenciales de usuario
	 * caducadas.
	 */
	String AUTH_CRED_EXPIRED_ERROR_MESSAGE = "auth.error.credentialsExpired.message";

	/**
	 * Parámetro que representa el mensaje de error por el estado de la cuenta
	 * de usuario (bloqueado, inhabilitado).
	 */
	String AUTH_ACCOUNT_STATUS_ERROR_MESSAGE = "auth.error.accountStatus.message";

	/**
	 * Parámetro que representa el mensaje de error por no ser encontradas las
	 * credenciales de un usuario.
	 */
	String AUTH_CRED_NOT_FOUND_ERROR_MESSAGE = "auth.error.authenticationCredentialsNotFound.message";

	/**
	 * Parámetro que representa el mensaje de error por un error interno en el
	 * proceso de autenticación de usuario.
	 */
	String AUTH_SERVICE_ERROR_MESSAGE = "auth.error.authenticationService.message";

	/**
	 * Parámetro que representa el mensaje de error por credenciales de usuario
	 * incorrectas.
	 */
	String AUTH_BAD_CRED_ERROR_MESSAGE = "auth.error.badCredentials.message";

	/**
	 * Parámetro que representa el mensaje de error por sesión caducada o no
	 * válida.
	 */
	String AUTH_SESSION_ERROR_MESSAGE = "auth.error.sessionAuthentication.message";

	/**
	 * Parámetro que representa el mensaje de error por cuenta de usuario no
	 * encontrada.
	 */
	String AUTH_USER_NOT_FOUND_ERROR_MESSAGE = "auth.error.usernameNotFound.message";

	// Mensajes de la solicitud de recuperación de password
	/**
	 * Parámetro que representa el mensaje presentado al finalizar correctamente
	 * la petición de recuperación de password de usuario.
	 */
	String REQ_REC_PASS_MSG_RESULT_OK = "reqRetrievePassword.resultOk";

	// Mensajes de error de actualización de password
	/**
	 * Parámetro que representa el mensaje presentado al finalizar correctamente
	 * la petición de actualización de password de usuario caducada.
	 */
	String UPD_PASS_MSG_RESULT_OK = "updatePassword.resultOk";

	/**
	 * Parámetro que representa el mensaje presentado al producirse un error en
	 * la recuperación de password de usuario por existir más de una petción en
	 * curso.
	 */
	String REQ_REC_PASS_MSG_ANY_REQS_IN_PROC_MESSAGE = "recPass.error.anyReqsInProc.message";

	// Mensajes de error de actualización de password
	/**
	 * Parámetro que representa el mensaje presentado al finalizar correctamente
	 * la petición de actualización de password de usuario caducada.
	 */
	String REC_PASS_MSG_RESULT_OK = "retrievePassword.resultOk";

	// Envío de correos para la recuperación de password
	/**
	 * Parámetro que representa el texto del asunto del correo electrónico
	 * remitido a un usuario que ha solicitado reestablecer su password.
	 */
	String RETRIEVE_PASS_EMAIL_SUBJECT = "retrievePassword.email.subject";

	/**
	 * Parámetro que representa el texto del cuerpo del correo electrónico
	 * remitido a un usuario que ha solicitado reestablecer su password.
	 */
	String RETRIEVE_PASS_EMAIL_BODY = "retrievePassword.email.body";

	/**
	 * Parámetro que representa el texto informativo de cierre de sesión.
	 */
	String END_SESSION_MESSAGE = "label.endSession";

	/**
	 * Parámetro que representa el texto de aviso por caducidad de password.
	 */
	String WARN_EXPIRED_PASS_MESSAGE = "password.warnExpired.message";

	/**
	 * Parámetro que representa el inicio del texto de aviso por caducidad de
	 * password.
	 */
	String WARN_EXPIRED_PASS_HEADER_MESSAGE = "password.warnExpired.header.message";

	/**
	 * Parámetro que representa el texto de error al obtener la información de
	 * sesión de un usuario.
	 */
	String GET_SESSION_USER_ERROR_MESSAGE = "error.session.user.info.message";

	// Variables de request
	/**
	 * Parámetro que representa al usuario en edición en la sección de gestión
	 * de usuario de la consola de administración.
	 */
	String EDIT_USER = "editUser";

	/**
	 * Parámetro que representa al usuario de sesión en edición.
	 */
	String EDIT_USER_IN_SESSION = "editUserInSession";
}
