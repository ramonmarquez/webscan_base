/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.UsersWebscanModelManager.java.</p>
 * <b>Descripción:</b><p> Fachada de gestión y consulta sobre las entidades pertenecientes al modelo de datos de gestión de usuarios de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.security.crypto.password.PasswordEncoder;

import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.WebscanModelUtilities;
import es.ricoh.webscan.users.UsersWebscanConstants;
import es.ricoh.webscan.users.model.dao.UserDAO;
import es.ricoh.webscan.users.model.domain.RecoveryUserPasswordRequestStatus;
import es.ricoh.webscan.users.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.users.model.dto.ProfileDTO;
import es.ricoh.webscan.users.model.dto.RecoveryUserPasswordRequestDTO;
import es.ricoh.webscan.users.model.dto.UserDTO;

/**
 * Fachada de gestión y consulta sobre las entidades pertenecientes al modelo de
 * datos de gestión de usuarios de Webscan.
 * <p>
 * Clase UsersWebscanModelManager.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@PropertySources(@PropertySource(value = "classpath:users_administration.properties"))
public class UsersWebscanModelManager {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UsersWebscanModelManager.class);

	/**
	 * DAO sobre las entidades usuario, perfil de usuario y petición de
	 * recuperación de password.
	 */
	private UserDAO userDAO;

	/**
	 * Utilidad de cifrado de passwords.
	 */
	private PasswordEncoder passwordEncoder;

	/**
	 * Parámetros de configuración del módulo de gestión de usuarios de Webscan.
	 */
	private Properties confParameters = new Properties();

	/**
	 * Constructor sin argumentos.
	 */
	public UsersWebscanModelManager() {
		super();
	}

	/*************************************************************************************/
	/*									Usuarios										 */
	/*************************************************************************************/

	/**
	 * Obtiene el listado de usuarios registrados en el sistema, permitiendo la
	 * paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de usuarios.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<UserDTO> listUsers(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		List<UserDTO> res = new ArrayList<UserDTO>();

		LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación de los usuarios registrados en el sistema ...");
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		UsersDataValidationUtilities.checkUserGroupByClause(orderByColumns);

		res = userDAO.listUsers(pageNumber, elementsPerPage, orderByColumns);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Número de usuarios recuperados: " + res.size() + " ...");
		}
		return res;
	}

	/**
	 * Obtiene el listado de usuarios registrados en el sistema, y sus perfiles
	 * o roles de usuarios, permitiendo la paginación y ordenación del
	 * resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de usuarios.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public Map<UserDTO, List<ProfileDTO>> listUsersAndProfiles(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		Map<UserDTO, List<ProfileDTO>> res = new HashMap<UserDTO, List<ProfileDTO>>();

		LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación de los usuarios registrados en el sistema y sus roles o perfiles de usuario ...");
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		UsersDataValidationUtilities.checkUserGroupByClause(orderByColumns);

		res = userDAO.listUsersAndProfiles(pageNumber, elementsPerPage, orderByColumns);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Número de usuarios recuperados: " + res.size() + " ...");
		}
		return res;
	}

	/**
	 * Obtiene el listado de usuarios pertenecientes a un unidad organizativa
	 * determinada, permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de usuarios.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<UserDTO> getUsersByOrganizationUnit(Long orgUnitId, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		List<UserDTO> res = new ArrayList<UserDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación de los usuarios pertenecientes a la unidad organizativa " + orgUnitId + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(orgUnitId);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}
		UsersDataValidationUtilities.checkUserGroupByClause(orderByColumns);

		res = userDAO.getUsersByOrganizationUnit(orgUnitId, pageNumber, elementsPerPage, orderByColumns);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Número de usuarios recuperados para la unidad organizativa " + orgUnitId + ": " + res.size() + " ...");
		}
		return res;
	}

	/**
	 * Obtiene un usuario a partir de su identificador.
	 * 
	 * @param userId
	 *            Identificador de especificación de plugin.
	 * @return La definición de plugin solicitada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public UserDTO getUser(Long userId) throws UsersDAOException {
		UserDTO res;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación del usuario " + userId + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(userId);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}

		res = userDAO.getUser(userId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Información del usuario " + userId + "recuperada.");
		}

		return res;
	}

	/**
	 * Obtiene un usuario a partir de su nombre de usuario.
	 * 
	 * @param username
	 *            nombre de usuario.
	 * @return El usuario solicitado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public UserDTO getUserByUsername(String username) throws UsersDAOException {
		UserDTO res;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación del usuario " + username + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(username);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}

		res = userDAO.getUserByUsername(username);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Información del usuario " + username + "recuperada.");
		}
		return res;
	}

	/**
	 * Obtiene un usuario a partir de su dirección de correo.
	 * 
	 * @param email
	 *            dirección de correo.
	 * @return El usuario solicitado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public UserDTO getUserByEmail(String email) throws UsersDAOException {
		UserDTO res;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación del usuario cuya dirección de correo electrónico es " + email + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(email);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}

		res = userDAO.getUserByEmail(email);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Información del usuario, cuya dirección de correo electrónico es " + email + ", recuperada.");
		}
		return res;
	}

	/**
	 * Crea un nuevo usuario en el sistema.
	 * 
	 * @param user
	 *            Nueva usuario.
	 * @return Identificador del usuario creado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public Long createUser(UserDTO user) throws UsersDAOException {
		Date nextPassUpdateDate;
		Long res;
		String excMsg;
		UserDTO auxUser = null;

		LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la creación de un nuevo usuario en el sistema ...");
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		UsersDataValidationUtilities.checkUser(user, Boolean.TRUE);

		try {
			auxUser = userDAO.getUserByUsername(user.getUsername());
		} catch (UsersDAOException e) {
			if (!UsersDAOException.CODE_901.equals(e.getCode())) {
				throw e;
			}
		}

		if (auxUser != null) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error creando un nuevo usuario. Ya existe un usuario con el mismo nombre de usuario (" + user.getUsername() + ").";
			LOG.error("[WEBSCAN-USERS-MODEL] {}", excMsg);
			throw new UsersDAOException(UsersDAOException.CODE_820, excMsg);
		}

		try {
			auxUser = userDAO.getUserByEmail(user.getEmail());
		} catch (UsersDAOException e) {
			if (!UsersDAOException.CODE_901.equals(e.getCode())) {
				throw e;
			}
		}

		if (auxUser != null) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error creando un nuevo usuario. Ya existe un usuario con la misma dirección de correo electrónico (" + user.getEmail() + ").";
			LOG.error("[WEBSCAN-USERS-MODEL] {}", excMsg);
			throw new UsersDAOException(UsersDAOException.CODE_821, excMsg);
		}

		try {
			auxUser = userDAO.getUserByDocument(user.getDocument());
		} catch (UsersDAOException e) {
			if (!UsersDAOException.CODE_901.equals(e.getCode())) {
				throw e;
			}
		}

		if (auxUser != null) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error creando un nuevo usuario. Ya existe un usuario con el mismo documento de identificación (" + user.getDocument() + ").";
			LOG.error("[WEBSCAN-USERS-MODEL] {}", excMsg);
			throw new UsersDAOException(UsersDAOException.CODE_822, excMsg);
		}

		LOG.debug("[WEBSCAN-USERS-MODEL] Se cumplimentan datos del usuario ...");

		auxUser = new UserDTO();
		auxUser.setDocument(user.getDocument());
		auxUser.setEmail(user.getEmail());
		auxUser.setLocked(user.getLocked());
		auxUser.setName(user.getName());

		// Calcular fecha a partir de propiedad
		// user.info.passwordValidityMaxPeriod
		try {
			nextPassUpdateDate = WebscanModelUtilities.addDays(new Date(), Integer.parseInt(confParameters.getProperty(UsersWebscanConstants.PASS_MAX_VAL_PERIOD_PROP_NAME)));
			auxUser.setNextPassUpdateDate(nextPassUpdateDate);
			// Se ofusca la password y se establece la próxima fecha de
			// establecimiento de la password
			auxUser.setPassword(passwordEncoder.encode(user.getPassword()));
			auxUser.setSecondSurname(user.getSecondSurname());
			auxUser.setSurname(user.getSurname());
			auxUser.setUsername(user.getUsername());

			LOG.debug("[WEBSCAN-USERS-MODEL] Se ejecuta la inserción en BBDD ...");

			res = userDAO.createUser(auxUser);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Nuevo usuario creado, identificador: " + res + ".");
			}
		} catch (NumberFormatException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error creando un nuevo usuario. Se produjo un error al calcular la fecha de modificación de la password: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Actualiza la información de un usuario registrado en el sistema. Los
	 * datos actualizados son: documento de identificación, dirección de correo
	 * electrónico, número de intentos consecutivos fallidos de acceso,
	 * indicador de bloqueo, nombre de pila, primer apellido, segundo apellido y
	 * nombre de usuario.
	 * 
	 * @param user
	 *            Usuario actualizado.
	 * @param setUpdateDate
	 *            Indica si se debe actualizar la fecha de actualización de
	 *            información del usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updateUser(UserDTO user, Boolean setUpdateDate) throws UsersDAOException {
		UserDTO auxUser = null;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la modificación del usuario " + (user != null ? user.getId() : "No detallado") + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		UsersDataValidationUtilities.checkUser(user, Boolean.FALSE);

		checkModelUserInfo(user);

		LOG.debug("[WEBSCAN-USERS-MODEL] Se cumplimentan datos del usuario ...");
		auxUser = userDAO.getUser(user.getId());

		auxUser.setDocument(user.getDocument());
		auxUser.setEmail(user.getEmail());
		auxUser.setFailedAttempts(user.getFailedAttempts());
		auxUser.setLocked(user.getLocked());
		auxUser.setName(user.getName());
		auxUser.setSecondSurname(user.getSecondSurname());
		auxUser.setSurname(user.getSurname());
		auxUser.setUsername(user.getUsername());

		LOG.debug("[WEBSCAN-USERS-MODEL] Se ejecuta la inserción en BBDD ...");

		userDAO.updateUser(auxUser, setUpdateDate);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Usuario modificado, identificador: " + auxUser.getId() + ".");
		}

	}

	/**
	 * Comprueba que no exista un usuario con el mismo nombre de usuario, cuenta
	 * de correo electrónico o número de identificación.
	 * 
	 * @param user
	 *            Usuario a modificar.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	private void checkModelUserInfo(UserDTO user) throws UsersDAOException {
		UserDTO auxUser = null;
		String excMsg;

		try {
			auxUser = userDAO.getUserByUsername(user.getUsername());
		} catch (UsersDAOException e) {
			if (!UsersDAOException.CODE_901.equals(e.getCode())) {
				throw e;
			}
		}

		if (auxUser != null && !auxUser.getId().equals(user.getId())) {
			excMsg = "Error editando el usuario " + user.getUsername() + ". Ya existe un usuario con el mismo nombre de usuario (" + user.getUsername() + ").";
			LOG.error("[WEBSCAN-USERS-MODEL] {}", excMsg);
			throw new UsersDAOException(UsersDAOException.CODE_820, excMsg);
		}

		try {
			auxUser = userDAO.getUserByEmail(user.getEmail());
		} catch (UsersDAOException e) {
			if (!UsersDAOException.CODE_901.equals(e.getCode())) {
				throw e;
			}
		}

		if (auxUser != null && !auxUser.getId().equals(user.getId())) {
			excMsg = "Error editando el usuario " + user.getUsername() + ". Ya existe un usuario con la misma dirección de correo electrónico (" + user.getEmail() + ").";
			LOG.error("[WEBSCAN-USERS-MODEL] {}", excMsg);
			throw new UsersDAOException(UsersDAOException.CODE_821, excMsg);
		}

		try {
			auxUser = userDAO.getUserByDocument(user.getDocument());
		} catch (UsersDAOException e) {
			if (!UsersDAOException.CODE_901.equals(e.getCode())) {
				throw e;
			}
		}

		if (auxUser != null && !auxUser.getId().equals(user.getId())) {
			excMsg = "Error editando el usuario " + user.getUsername() + ". Ya existe un usuario con el mismo documento de identificación (" + user.getDocument() + ").";
			LOG.error("[WEBSCAN-USERS-MODEL] {}", excMsg);
			throw new UsersDAOException(UsersDAOException.CODE_822, excMsg);
		}
	}

	/**
	 * Actualiza la password de un usuario registrado en el sistema, y la
	 * información asociada a esta modificación (fecha de próxima actualización
	 * de password y fecha de actualización de información de usuario).
	 * 
	 * @param user
	 *            Usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario, se encuentra bloqueado, o se produce algún error en
	 *             la ejecución de la sentencia de BBDD.
	 */
	public void updateUserPassword(UserDTO user) throws UsersDAOException {
		Date nextPassUpdateDate;
		String excMsg;
		UserDTO auxUser;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la modificación de la password del usuario " + (user != null ? user.getId() : "No detallado") + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		UsersDataValidationUtilities.checkUser(user, Boolean.TRUE);
		auxUser = userDAO.getUser(user.getId());
		UsersDataValidationUtilities.checkLockedUser(auxUser);

		LOG.debug("[WEBSCAN-USERS-MODEL] Se cumplimentan datos del usuario ...");

		// Calcular fecha a partir de propiedad
		// user.info.passwordValidityMaxPeriod
		try {
			nextPassUpdateDate = WebscanModelUtilities.addDays(new Date(), Integer.parseInt(confParameters.getProperty(UsersWebscanConstants.PASS_MAX_VAL_PERIOD_PROP_NAME)));
			auxUser.setNextPassUpdateDate(nextPassUpdateDate);
			// Se ofusca la password y se establece la próxima fecha de
			// establecimiento de la password
			auxUser.setPassword(passwordEncoder.encode(user.getPassword()));

			LOG.debug("[WEBSCAN-USERS-MODEL] Se ejecuta la actualización en BBDD ...");

			userDAO.updateUser(auxUser, Boolean.TRUE);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Password de usuario modificado, identificador: " + auxUser.getId() + ".");
			}
		} catch (NumberFormatException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error creando un nuevo usuario. Se produjo un error al calcular la " + "fecha de modificación de la password: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}
	}

	/**
	 * Elimina un usuario del sistema.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void removeUser(Long userId) throws UsersDAOException {
		UserDTO auxUser;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la eliminación del usuario " + (userId != null ? userId : "No detallado") + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(userId);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}
		auxUser = userDAO.getUser(userId);
		UsersDataValidationUtilities.checkLockedUser(auxUser);

		LOG.debug("[WEBSCAN-USERS-MODEL] Se ejecuta el borrado en BBDD ...");

		userDAO.removeUser(userId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Usuario eliminado, identificador: " + userId + ".");
		}
	}

	/**
	 * Establece los usuarios pertenecientes a un conjunto de unidades
	 * organizativas determinado.
	 * 
	 * @param orgUnitsUsers
	 *            Mapa cuyas entradas son identificadas por el identificador de
	 *            las unidades organizativas, y su valor se corresponden con los
	 *            usuarios pertenecientes a cada una de ellas.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public void setOrganizationUnitsUsers(Map<Long, List<UserDTO>> orgUnitsUsers) throws UsersDAOException {
		Long orgUnitId;
		String excMsg;

		LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia el establecimiento de usuarios en unidades organizativas ...");
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		if (orgUnitsUsers == null || orgUnitsUsers.isEmpty()) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error estableciendo usuarios en unidades organizativas: parámetro nulo o vacío.";
			LOG.error(excMsg);
			throw new UsersDAOException(excMsg);
		}

		for (Iterator<Long> it = orgUnitsUsers.keySet().iterator(); it.hasNext();) {
			orgUnitId = it.next();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Estableciendo nueva relación de usuarios para la unidad organizativa " + orgUnitId + ".");
			}
			userDAO.setOrganizationUnitUsers(orgUnitId, orgUnitsUsers.get(orgUnitId));
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Nueva relación de usuarios establecida para la unidad organizativa " + orgUnitId + ".");
			}
		}

	}

	/*************************************************************************************/
	/*									Perfiles de usuario				 				 */
	/*************************************************************************************/

	/**
	 * Obtiene el listado de perfiles de usuarios registrados en el sistema.
	 * 
	 * @return listado de perfiles de usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<ProfileDTO> listUserProfiles() throws UsersDAOException {
		List<ProfileDTO> res = new ArrayList<ProfileDTO>();

		LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación de los perfiles de usuarios registrados en el sistema ...");

		res = userDAO.listUserProfiles();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Número de perfiles de usuario recuperados: " + res.size() + " ...");
		}

		return res;
	}

	/**
	 * Obtiene el listado de perfiles configurado para un usuario registrado en
	 * el sistema.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @return listado de perfiles del usuario especificado como parámetro de
	 *         entrada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<ProfileDTO> getUserProfilesByUser(Long userId) throws UsersDAOException {
		List<ProfileDTO> res;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación de los perfiles del usuario " + userId + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(userId);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}

		res = userDAO.getUserProfilesByUser(userId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Perfiles del usuario " + userId + "recuperada.");
		}

		return res;
	}

	/**
	 * Establece los perfiles de un usuario registrado en el sistema.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @param userProfiles
	 *            Nueva relación de perfiles del usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public void setUserProfiles(Long userId, List<ProfileDTO> userProfiles) throws UsersDAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia el establecimiento de perfiles para el usuario " + userId + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		UsersDataValidationUtilities.checkUserProfiles(userProfiles, userId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Estableciendo nueva relación de perfiles para el usuario " + userId + ".");
		}
		userDAO.setUserProfiles(userId, userProfiles);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Nueva relación de perfiles establecida para el usuario " + userId + ".");
		}
	}

	/**
	 * Establece las unidades organizativas a las que pertenece un usuario
	 * registrado en el sistema.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @param orgUnitIds
	 *            Nueva relación de unidades organizativas a las que pertenece
	 *            un usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public void setUserOrganizationUnits(Long userId, List<OrganizationUnitDTO> orgUnitIds) throws UsersDAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia el establecimiento de unidades organizativas para el usuario " + userId + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(userId);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Estableciendo nueva relación de unidades organizativas para el usuario " + userId + ".");
		}
		userDAO.setUserOrganizationUnits(userId, orgUnitIds);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Nueva relación de unidades organizativas establecida para el usuario " + userId + ".");
		}
	}

	/*************************************************************************************/
	/*					Peticiones de recuperación de password de usuario				 */
	/*************************************************************************************/

	/**
	 * Obtiene una petición de recuperación de password a partir de su
	 * identificador.
	 * 
	 * @param recPassReqId
	 *            Identificador de petición de recuperación de password.
	 * @return La petición de recuperación de password solicitada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             petición solicitada, o se produce algún error en la consulta
	 *             a BBDD.
	 */
	public RecoveryUserPasswordRequestDTO getRecoveryPassRequest(Long recPassReqId) throws UsersDAOException {
		RecoveryUserPasswordRequestDTO res;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación de la petición de recuperación de password " + recPassReqId + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(recPassReqId);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}

		res = userDAO.getRecoveryPassReq(recPassReqId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Recuperada la petición de recuperación de password " + recPassReqId + ". Fecha de emisión: " + res.getComSendingDate());
		}

		return res;
	}

	/**
	 * Obtiene una petición de recuperación de password a partir de la dirección
	 * de correo electrónico del usuario solicitante.
	 * 
	 * @param userEmail
	 *            Dirección de correo electrónico del usuario solicitante.
	 * @return La petición de recuperación de password solicitada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             petición solicitada, o se produce algún error en la consulta
	 *             a BBDD.
	 */
	public RecoveryUserPasswordRequestDTO getRecoveryPassReqInProcessByUserEmail(String userEmail) throws UsersDAOException {
		Date maxValidityPeriodDate;
		List<RecoveryUserPasswordRequestDTO> currentInProcessReqs;
		RecoveryUserPasswordRequestDTO res;
		String excMsg;

		maxValidityPeriodDate = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación de la petición más reciente de recuperación de password para el usuario cuya dirección de correo electrónico es " + userEmail + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(userEmail);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}

		currentInProcessReqs = userDAO.getRecoveryPassReqInProcessByUserEmail(userEmail);

		if (currentInProcessReqs.isEmpty()) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la petición más reciente de recuperación de password para el usuario cuya dirección de correo electrónico es " + userEmail + ". No existen peticiones abiertas o en curso.";
			throw new UsersDAOException(UsersDAOException.CODE_901, excMsg);
		}

		if (currentInProcessReqs.size() > 1) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la petición más reciente de recuperación de password para el usuario cuya dirección de correo electrónico es " + userEmail + ". Existe más de una petición abierta o en curso.";
			throw new UsersDAOException(UsersDAOException.CODE_850, excMsg);
		}

		res = currentInProcessReqs.get(0);

		// Calcular fecha a partir de propiedad
		// user.info.passwordValidityMaxPeriod
		try {
			maxValidityPeriodDate = WebscanModelUtilities.addDays(res.getComSendingDate(), Integer.parseInt(confParameters.getProperty(UsersWebscanConstants.REC_PASS_MAX_VAL_PERIOD_PROP_NAME)));
		} catch (NumberFormatException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la petición más reciente de recuperación de password para el usuario cuya dirección de correo electrónico es " + userEmail + ". Se produjo un error al calcular la fecha de máxima para recuperar la password: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		if (maxValidityPeriodDate.before(new Date())) {
			excMsg = "[WEBSCAN-USERS-MODEL]  Error recuperando la petición más reciente de recuperación de password para el usuario cuya dirección de correo electrónico es " + userEmail + ". Ha expirado el tiempo máximo para recuperar la password.";
			LOG.error(excMsg);
			throw new UsersDAOException(UsersDAOException.CODE_801, excMsg);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Recuperada la petición más reciente de recuperación de password para el usuario cuya dirección de correo electrónico es " + userEmail + ". Fecha de emisión: " + res.getComSendingDate());
		}
		return res;
	}

	/**
	 * Crea una nueva petición de recuperación de password en el sistema,
	 * estableciéndola en proceso
	 * ({@link RecoveryUserPasswordRequestStatus.IN_PROCESS}). Si exitiera
	 * alguna petición en proceso para el usuario solicitante, esta sería
	 * cancelada.
	 * 
	 * @param recUserPassReq
	 *            Nueva petición de recuperación de password.
	 * @return Identificador de la petición de recuperación de password creada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public Long createRecoveryUserPasswordRequest(RecoveryUserPasswordRequestDTO recUserPassReq) throws UsersDAOException {
		Long res;
		RecoveryUserPasswordRequestDTO auxRecUserPassReq;
		List<RecoveryUserPasswordRequestDTO> currentInProcessReqs;
		String excMsg;
		UserDTO user;

		res = null;

		LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la creación de una nueva petición de recuperación de password en el sistema ...");
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		UsersDataValidationUtilities.checkRecoveryUserPasswordRequest(recUserPassReq);

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Se comprueba si existe una petición previa para el usuario " + recUserPassReq.getUserId() + "...");
			}
			user = userDAO.getUser(recUserPassReq.getUserId());

			currentInProcessReqs = userDAO.getRecoveryPassReqInProcessByUserEmail(user.getEmail());
			// Se cancelan las peticiones previas que estuvieran abiertas
			for (RecoveryUserPasswordRequestDTO rupq: currentInProcessReqs) {
				rupq.setStatus(RecoveryUserPasswordRequestStatus.CANCELED);
				rupq.setRecoveryDate(new Date());
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS-MODEL] Se cancela la petición previa existente para el usuario " + rupq.getUserId() + "...");
				}
				userDAO.updateRecoveryUserPasswordRequest(rupq);
			}

			LOG.debug("[WEBSCAN-USERS-MODEL] Se cumplimenta la información para la recuperación de la password de usuario ...");

			auxRecUserPassReq = new RecoveryUserPasswordRequestDTO();
			auxRecUserPassReq.setComSendingDate(recUserPassReq.getComSendingDate());
			auxRecUserPassReq.setSecurityToken(recUserPassReq.getSecurityToken());
			auxRecUserPassReq.setStatus(RecoveryUserPasswordRequestStatus.IN_PROCESS);
			auxRecUserPassReq.setUserId(recUserPassReq.getUserId());
			auxRecUserPassReq.setAuditOpId(recUserPassReq.getAuditOpId());

			LOG.debug("[WEBSCAN-USERS-MODEL] Se ejecuta la inserción en BBDD ...");

			res = userDAO.createRecoveryUserPasswordRequest(auxRecUserPassReq);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Petición de recuperación de password creada, identificador: " + res + ".");
			}
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL]  Error creando una nueva petición de recuperación de password " + "para el usuario " + recUserPassReq.getUserId() + ": " + e.getMessage();
			LOG.error(excMsg);
			throw new UsersDAOException(excMsg);
		}
		return res;
	}

	/**
	 * Actualiza una petición, modificando su fecha de finalización y
	 * estableciéndola como finalizada correctamente
	 * ({@link RecoveryUserPasswordRequestStatus.PERFORMED}).
	 * 
	 * @param recUserPassReq
	 *            Petición de recuperación de password.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public void performRecoveryUserPasswordRequest(RecoveryUserPasswordRequestDTO recUserPassReq) throws UsersDAOException {
		RecoveryUserPasswordRequestDTO auxRecUserPassReq;

		LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la creación de un nuevo usuario en el sistema ...");
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		UsersDataValidationUtilities.checkRecoveryUserPasswordRequest(recUserPassReq);

		LOG.debug("[WEBSCAN-USERS-MODEL] Se cumplimenta la información para la recuperación de la password de usuario ...");
		auxRecUserPassReq = userDAO.getRecoveryPassReq(recUserPassReq.getId());
		auxRecUserPassReq.setRecoveryDate(recUserPassReq.getRecoveryDate());
		auxRecUserPassReq.setStatus(RecoveryUserPasswordRequestStatus.PERFORMED);

		LOG.debug("[WEBSCAN-USERS-MODEL] Se ejecuta la modificación en BBDD ...");

		userDAO.updateRecoveryUserPasswordRequest(auxRecUserPassReq);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Petición de recuperación de password actualizada, identificador: " + recUserPassReq.getId() + ".");
		}
	}

	/*************************************************************************************/
	/*									Unidades organizativas				 			 */
	/*************************************************************************************/

	/**
	 * Recupera la relación de unidades organizativas a las que pertence un
	 * usuario.
	 * 
	 * @param userId
	 *            Identificador interno de usuario
	 * @return Lista de unidades organizativas a las que pertence un usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<OrganizationUnitDTO> getOrganizationUnitsByUserId(Long userId) throws UsersDAOException {
		List<OrganizationUnitDTO> res = new ArrayList<OrganizationUnitDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación de las unidades organizativas a las que pertenece el usuario " + userId + " ...");
		}
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkIdentifier(userId);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}

		res = userDAO.getOrganizationUnitsByUserId(userId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Número de unidades organizativas recuperadas: " + res.size() + " ...");
		}

		return res;
	}

	/**
	 * Recupera una relación de unidades organizativas a partir de sus
	 * identificadores.
	 * 
	 * @param orgUnitIds
	 *            Relación de identificadores internos de unidades
	 *            oragnizativas.
	 * @return Lista de unidades organizativas a las que pertence un usuario.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<OrganizationUnitDTO> getOrganizationUnitsByIds(List<Long> orgUnitIds) throws UsersDAOException {
		List<OrganizationUnitDTO> res = new ArrayList<OrganizationUnitDTO>();

		LOG.debug("[WEBSCAN-USERS-MODEL] Se inicia la recuperación de una relación de unidades organizativas de BBDD ...");
		LOG.debug("[WEBSCAN-USERS-MODEL] Se validan los parámetros de entrada de la petición ...");

		try {
			UsersDataValidationUtilities.checkCollection(orgUnitIds);
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e);
		}

		LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar una relación de unidades organizativas de BBDD ...");
		res = userDAO.getOrganizationUnitsByIds(orgUnitIds);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] " + res.size() + " unidades organizativas recuperadas de BBDD.");
		}

		return res;
	}

	/*************************************************************************************/
	/*			Metodos de acceso y establecimiento de atributos de la clase			 */
	/*************************************************************************************/

	/**
	 * Establece el DAO sobre las entidades usuario, perfil de usuario y
	 * petición de recuperación de password.
	 * 
	 * @param anUserDAO
	 *            nuevo DAO sobre las entidades usuario, perfil de usuario y
	 *            petición de recuperación de password.
	 */
	public void setUserDAO(UserDAO anUserDAO) {
		this.userDAO = anUserDAO;
	}

	/**
	 * Establece la utilidad de cifrado de passwords.
	 * 
	 * @param aPasswordEncoder
	 *            nueva utilidad de cifrado de passwords.
	 */
	public void setPasswordEncoder(PasswordEncoder aPasswordEncoder) {
		this.passwordEncoder = aPasswordEncoder;
	}

	/**
	 * Establece los parámetros de configuración del módulo de gestión de
	 * usuarios de Webscan.
	 * 
	 * @param anyConfParameters
	 *            nuevos parámetros de configuración del módulo de gestión de
	 *            usuarios de Webscan.
	 */
	public void setConfParameters(Properties anyConfParameters) {
		this.confParameters.clear();

		if (anyConfParameters != null && !anyConfParameters.isEmpty()) {
			this.confParameters.putAll(anyConfParameters);
		}
	}
}
