/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.user.model.domain.User.java.</p>
 * <b>Descripción:</b><p> Entidad JPA usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA usuario.
 * <p>
 * Clase User.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "APP_USER", uniqueConstraints = { @UniqueConstraint(columnNames = { "USERNAME" }), @UniqueConstraint(columnNames = { "DOC_ID" }), @UniqueConstraint(columnNames = { "EMAIL" }) })
public class User implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "APP_USER_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "APP_USER_ID_SEQ", sequenceName = "APP_USER_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo USERNAME de la entidad.
	 */
	@Column(name = "USERNAME")
	private String username;

	/**
	 * Atributo PASSWORD de la entidad.
	 */
	@Column(name = "PASSWORD")
	private String password;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Atributo SURNAME_1 de la entidad.
	 */
	@Column(name = "SURNAME_1")
	private String surname;

	/**
	 * Atributo SURNAME_2 de la entidad.
	 */
	@Column(name = "SURNAME_2")
	private String secondSurname;

	/**
	 * Atributo DOC_ID de la entidad.
	 */
	@Column(name = "DOC_ID")
	private String document;

	/**
	 * Atributo EMAIL de la entidad.
	 */
	@Column(name = "EMAIL")
	private String email;

	/**
	 * Atributo LOCKED de la entidad.
	 */
	@Column(name = "LOCKED")
	private Boolean locked = Boolean.FALSE;

	/**
	 * Atributo CREATION_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	/**
	 * Atributo LAST_UPDATE_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATE_DATE")
	private Date lastUpdateDate;

	/**
	 * Atributo NEXT_PASS_UPD_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NEXT_PASS_UPD_DATE")
	private Date nextPassUpdateDate;

	/**
	 * Atributo FAILED_ATTEMPTS de la entidad.
	 */
	@Column(name = "FAILED_ATTEMPTS")
	private Integer failedAttempts;

	/**
	 * Relación de perfiles de usuarios que posee el usuario.
	 */
	@ManyToMany(mappedBy = "users", fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.users.model.domain.Profile.class)
	private List<Profile> profiles = new ArrayList<Profile>();

	/**
	 * Relación de unidades organizativas a las que pertence el usuario.
	 */
	@ManyToMany(mappedBy = "users", fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.users.model.domain.OrganizationUnit.class)
	private List<OrganizationUnit> orgUnits = new ArrayList<OrganizationUnit>();

	/**
	 * Constructor sin argumentos.
	 */
	public User() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo USERNAME de la entidad.
	 * 
	 * @return el atributo USERNAME de la entidad.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece un nuevo valor para el atributo USERNAME de la entidad.
	 * 
	 * @param anUsername
	 *            nuevo valor para el atributo USERNAME de la entidad.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene el atributo PASSWORD de la entidad.
	 * 
	 * @return el atributo PASSWORD de la entidad.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Establece un nuevo valor para el atributo PASSWORD de la entidad.
	 * 
	 * @param aPassword
	 *            nuevo valor para el atributo PASSWORD de la entidad.
	 */
	public void setPassword(String aPassword) {
		this.password = aPassword;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo SURNAME_1 de la entidad.
	 * 
	 * @return el atributo SURNAME_1 de la entidad.
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Establece un nuevo valor para el atributo SURNAME_1 de la entidad.
	 * 
	 * @param aSurname
	 *            nuevo valor para el atributo SURNAME_1 de la entidad.
	 */
	public void setSurname(String aSurname) {
		this.surname = aSurname;
	}

	/**
	 * Obtiene el atributo SURNAME_2 de la entidad.
	 * 
	 * @return el atributo SURNAME_2 de la entidad.
	 */
	public String getSecondSurname() {
		return secondSurname;
	}

	/**
	 * Establece un nuevo valor para el atributo SURNAME_2 de la entidad.
	 * 
	 * @param aSecondSurname
	 *            nuevo valor para el atributo SURNAME_2 de la entidad.
	 */
	public void setSecondSurname(String aSecondSurname) {
		this.secondSurname = aSecondSurname;
	}

	/**
	 * Obtiene el atributo DOC_ID de la entidad.
	 * 
	 * @return el atributo DOC_ID de la entidad.
	 */
	public String getDocument() {
		return document;
	}

	/**
	 * Establece un nuevo valor para el atributo DOC_ID de la entidad.
	 * 
	 * @param aDocument
	 *            nuevo valor para el atributo DOC_ID de la entidad.
	 */
	public void setDocument(String aDocument) {
		this.document = aDocument;
	}

	/**
	 * Obtiene el atributo EMAIL de la entidad.
	 * 
	 * @return el atributo EMAIL de la entidad.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Establece un nuevo valor para el atributo EMAIL de la entidad.
	 * 
	 * @param anEmail
	 *            nuevo valor para el atributo EMAIL de la entidad.
	 */
	public void setEmail(String anEmail) {
		this.email = anEmail;
	}

	/**
	 * Obtiene el atributo LOCKED de la entidad.
	 * 
	 * @return el atributo LOCKED de la entidad.
	 */
	public Boolean getLocked() {
		return locked;
	}

	/**
	 * Establece un nuevo valor para el atributo LOCKED de la entidad.
	 * 
	 * @param lockedParam
	 *            nuevo valor para el atributo LOCKED de la entidad.
	 */
	public void setLocked(Boolean lockedParam) {
		this.locked = lockedParam;
	}

	/**
	 * Obtiene el atributo CREATION_DATE de la entidad.
	 * 
	 * @return el atributo CREATION_DATE de la entidad.
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Establece un nuevo valor para el atributo CREATION_DATE de la entidad.
	 * 
	 * @param aCreationDate
	 *            nuevo valor para el atributo CREATION_DATE de la entidad.
	 */
	public void setCreationDate(Date aCreationDate) {
		this.creationDate = aCreationDate;
	}

	/**
	 * Obtiene el atributo LAST_UPDATE_DATE de la entidad.
	 * 
	 * @return el atributo LAST_UPDATE_DATE de la entidad.
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * Establece un nuevo valor para el atributo LAST_UPDATE_DATE de la entidad.
	 * 
	 * @param theLastUpdateDate
	 *            nuevo valor para el atributo LAST_UPDATE_DATE de la entidad.
	 */
	public void setLastUpdateDate(Date theLastUpdateDate) {
		this.lastUpdateDate = theLastUpdateDate;
	}

	/**
	 * Obtiene el atributo NEXT_PASS_UPD_DATE de la entidad.
	 * 
	 * @return el atributo NEXT_PASS_UPD_DATE de la entidad.
	 */
	public Date getNextPassUpdateDate() {
		return nextPassUpdateDate;
	}

	/**
	 * Establece un nuevo valor para el atributo NEXT_PASS_UPD_DATE de la
	 * entidad.
	 * 
	 * @param theNextPassUpdateDate
	 *            nuevo valor para el atributo NEXT_PASS_UPD_DATE de la entidad.
	 */
	public void setNextPassUpdateDate(Date theNextPassUpdateDate) {
		this.nextPassUpdateDate = theNextPassUpdateDate;
	}

	/**
	 * Obtiene el atributo FAILED_ATTEMPTS de la entidad.
	 * 
	 * @return el atributo FAILED_ATTEMPTS de la entidad.
	 */
	public Integer getFailedAttempts() {
		return failedAttempts;
	}

	/**
	 * Establece un nuevo valor para el atributo FAILED_ATTEMPTS de la entidad.
	 * 
	 * @param failedAttemptsParam
	 *            nuevo valor para el atributo FAILED_ATTEMPTS de la entidad.
	 */
	public void setFailedAttempts(Integer failedAttemptsParam) {
		this.failedAttempts = failedAttemptsParam;
	}

	/**
	 * Obtiene la relación de perfiles de usuarios que posee el usuario.
	 * 
	 * @return la relación de perfiles de usuarios que posee el usuario.
	 */
	public List<Profile> getProfiles() {
		return profiles;
	}

	/**
	 * Establece la relación de perfiles de usuarios que posee el usuario.
	 * 
	 * @param anyProfiles
	 *            relación de perfiles de usuarios que posee el usuario.
	 */
	public void setProfiles(List<Profile> anyProfiles) {
		this.profiles.clear();

		if (anyProfiles != null && !anyProfiles.isEmpty()) {
			this.profiles.addAll(anyProfiles);
		}
	}

	/**
	 * Obtiene la relación de unidades organizativas a las que pertenece el
	 * usuario.
	 * 
	 * @return la relación de unidades organizativas a las que pertenece el
	 *         usuario.
	 */
	public List<OrganizationUnit> getOrgUnits() {
		return orgUnits;
	}

	/**
	 * Establece la relación de unidades organizativas a las que pertenece el
	 * usuario.
	 * 
	 * @param anyOrgUnits
	 *            relación de unidades organizativas a las que pertenece el
	 *            usuario.
	 */
	public void setOrgUnits(List<OrganizationUnit> anyOrgUnits) {
		this.orgUnits.clear();

		if (anyOrgUnits != null && !anyOrgUnits.isEmpty()) {
			this.orgUnits.addAll(anyOrgUnits);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
