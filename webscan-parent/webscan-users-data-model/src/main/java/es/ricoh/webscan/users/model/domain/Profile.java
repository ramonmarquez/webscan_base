/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.user.model.domain.Profile.java.</p>
 * <b>Descripción:</b><p> Entidad JPA perfil de usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA perfil de usuario.
 * <p>
 * Clase Profile.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "PROFILE", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME" }) })
public class Profile implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Relación de usuarios que poseen configurado el perfil de usuario.
	 */
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.REMOVE })
	@JoinTable(name = "APP_USER_PROFILE", joinColumns = @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "APP_USER_ID", referencedColumnName = "ID"))
	private List<User> users = new ArrayList<User>();

	/**
	 * Constructor sin argumentos.
	 */
	public Profile() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene la relación de usuarios que poseen configurado el perfil de
	 * usuario.
	 * 
	 * @return la relación de usuarios que poseen configurado el perfil de
	 *         usuario.
	 */
	public List<User> getUsers() {
		return users;
	}

	/**
	 * Establece la relación de usuarios que poseen configurado el perfil de
	 * usuario.
	 * 
	 * @param anyUsers
	 *            nueva relación de usuarios que poseen configurado el perfil de
	 *            usuario.
	 */
	public void setUsers(List<User> anyUsers) {
		this.users.clear();

		if (anyUsers != null && !anyUsers.isEmpty()) {
			this.users.addAll(anyUsers);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profile other = (Profile) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
