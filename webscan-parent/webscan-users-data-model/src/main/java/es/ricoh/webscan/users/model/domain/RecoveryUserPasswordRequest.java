/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.user.model.domain.RecoveryUserPasswordRequest.java.</p>
 * <b>Descripción:</b><p> Entidad JPA petición de recuperación de password.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad JPA petición de recuperación de password.
 * <p>
 * Clase RecoveryUserPasswordRequest.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "REC_USER_PASS_REQ")
public class RecoveryUserPasswordRequest implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "REC_USER_PASS_REQ_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "REC_USER_PASS_REQ_ID_SEQ", sequenceName = "REC_USER_PASS_REQ_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo TOKEN de la entidad.
	 */
	@Column(name = "TOKEN")
	private String securityToken;

	/**
	 * Atributo SEND_COM_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SEND_COM_DATE")
	private Date comSendingDate;

	/**
	 * Atributo RECOVERY_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RECOVERY_DATE")
	private Date recoveryDate;

	/**
	 * Atributo RECOVERY_STATUS de la entidad.
	 */
	@Column(name = "RECOVERY_STATUS")
	@Enumerated(EnumType.STRING)
	private RecoveryUserPasswordRequestStatus status;

	/**
	 * Atributo AUDIT_OP_ID de la entidad.
	 */
	@Column(name = "AUDIT_OP_ID")
	private Long auditOpId;

	/**
	 * Usuario que realiza la petición.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.users.model.domain.User.class)
	@JoinColumn(name = "APP_USER_ID")
	private User user;

	/**
	 * Constructor sin argumentos.
	 */
	public RecoveryUserPasswordRequest() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo TOKEN de la entidad.
	 * 
	 * @return el atributo TOKEN de la entidad.
	 */
	public String getSecurityToken() {
		return securityToken;
	}

	/**
	 * Establece un nuevo valor para el atributo TOKEN de la entidad.
	 * 
	 * @param aSecurityToken
	 *            nuevo valor para el atributo TOKEN de la entidad.
	 */
	public void setSecurityToken(String aSecurityToken) {
		this.securityToken = aSecurityToken;
	}

	/**
	 * Obtiene el atributo SEND_COM_DATE de la entidad.
	 * 
	 * @return el atributo SEND_COM_DATE de la entidad.
	 */
	public Date getComSendingDate() {
		return comSendingDate;
	}

	/**
	 * Establece un nuevo valor para el atributo SEND_COM_DATE de la entidad.
	 * 
	 * @param aComSendingDate
	 *            nuevo valor para el atributo SEND_COM_DATE de la entidad.
	 */
	public void setComSendingDate(Date aComSendingDate) {
		this.comSendingDate = aComSendingDate;
	}

	/**
	 * Obtiene el atributo RECOVERY_DATE de la entidad.
	 * 
	 * @return el atributo RECOVERY_DATE de la entidad.
	 */
	public Date getRecoveryDate() {
		return recoveryDate;
	}

	/**
	 * Establece un nuevo valor para el atributo RECOVERY_DATE de la entidad.
	 * 
	 * @param aRecoveryDate
	 *            nuevo valor para el atributo RECOVERY_DATE de la entidad.
	 */
	public void setRecoveryDate(Date aRecoveryDate) {
		this.recoveryDate = aRecoveryDate;
	}

	/**
	 * Obtiene el atributo RECOVERY_STATUS de la entidad.
	 * 
	 * @return el atributo RECOVERY_STATUS de la entidad.
	 */
	public RecoveryUserPasswordRequestStatus getStatus() {
		return status;
	}

	/**
	 * Establece un nuevo valor para el atributo RECOVERY_STATUS de la entidad.
	 * 
	 * @param aStatus
	 *            nuevo valor para el atributo RECOVERY_STATUS de la entidad.
	 */
	public void setStatus(RecoveryUserPasswordRequestStatus aStatus) {
		this.status = aStatus;
	}

	/**
	 * Obtiene el atributo AUDIT_OP_ID de la entidad.
	 * 
	 * @return el atributo AUDIT_OP_ID de la entidad.
	 */
	public Long getAuditOpId() {
		return auditOpId;
	}

	/**
	 * Establece un nuevo valor para el atributo AUDIT_OP_ID de la entidad.
	 * 
	 * @param anAuditOpId
	 *            nuevo valor para el atributo AUDIT_OP_ID de la entidad.
	 */
	public void setAuditOpId(Long anAuditOpId) {
		this.auditOpId = anAuditOpId;
	}

	/**
	 * Obtiene el usuario que realizó la petición.
	 * 
	 * @return el usuario que realizó la petición.
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Establece el usuario que realizó la petición.
	 * 
	 * @param anUser
	 *            usuario que realiza la petición.
	 */
	public void setUser(User anUser) {
		this.user = anUser;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecoveryUserPasswordRequest other = (RecoveryUserPasswordRequest) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
