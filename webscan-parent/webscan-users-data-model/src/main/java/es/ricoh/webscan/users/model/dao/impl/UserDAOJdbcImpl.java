/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.dao.impl.UserDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre las entidades usuario, perfil de usuario y petición de recuperación de password, pertenecientes al modelo de datos de gestión de usuarios de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.dao.impl.CommonUtilitiesDAOJdbcImpl;
import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.model.dao.UserDAO;
import es.ricoh.webscan.users.model.domain.OrganizationUnit;
import es.ricoh.webscan.users.model.domain.Profile;
import es.ricoh.webscan.users.model.domain.RecoveryUserPasswordRequest;
import es.ricoh.webscan.users.model.domain.RecoveryUserPasswordRequestStatus;
import es.ricoh.webscan.users.model.domain.User;
import es.ricoh.webscan.users.model.dto.MappingUtilities;
import es.ricoh.webscan.users.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.users.model.dto.ProfileDTO;
import es.ricoh.webscan.users.model.dto.RecoveryUserPasswordRequestDTO;
import es.ricoh.webscan.users.model.dto.UserDTO;

/**
 * Implementación JPA 2.0 de la capa DAO sobre las entidades usuario, perfil de
 * usuario y petición de recuperación de password, pertenecientes al modelo de
 * datos de gestión de usuarios de Webscan.
 * <p>
 * Clase UserDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UserDAOJdbcImpl implements UserDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la manipulación de las entidades de incluidas en el
	 * contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-user-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * Constructor sin argumentos.
	 */
	public UserDAOJdbcImpl() {
		super();
	}

	/**
	 * Establece un nuevo gestor de persistencia JPA.
	 * 
	 * @param aEntityManager
	 *            manager de persistencia.
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		this.entityManager = aEntityManager;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#listUsers(java.lang.Long,
	 *      java.lang.Long, java.util.List)
	 */
	@Override
	public List<UserDTO> listUsers(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		int first, maxResults;
		List<User> ddbbRes = new ArrayList<User>();
		List<UserDTO> res = new ArrayList<UserDTO>();
		Map<String, List<EntityOrderByClause>> userOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {

			LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando los usuarios registrados en el sistema ...");

			if (orderByColumns != null && !orderByColumns.isEmpty()) {
				userOrderByColumns.put("user.", orderByColumns);
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT user FROM User user", userOrderByColumns);

			TypedQuery<User> query = entityManager.createQuery(jpqlQuery, User.class);

			if (elementsPerPage != null) {
				first = 0;
				maxResults = elementsPerPage.intValue();
				if (pageNumber != null) {
					first = (pageNumber.intValue() - 1) * maxResults;
				}

				query.setFirstResult(first);
				query.setMaxResults(maxResults);
			}

			LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar los usuarios registrados en el sistema ...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillUserListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuarios recuperados: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#listUsersAndProfiles(java.lang.Long,
	 *      java.lang.Long, java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<UserDTO, List<ProfileDTO>> listUsersAndProfiles(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		int first, maxResults;
		List<Object[ ]> ddbbRes = new ArrayList<Object[ ]>();
		Map<UserDTO, List<ProfileDTO>> res = new HashMap<UserDTO, List<ProfileDTO>>();
		Map<String, List<EntityOrderByClause>> userOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {

			LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando los usuarios registrados en el sistema ...");

			if (orderByColumns != null && !orderByColumns.isEmpty()) {
				userOrderByColumns.put("user.", orderByColumns);
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT user,profiles FROM User user JOIN user.profiles profiles", userOrderByColumns);

			Query query = entityManager.createQuery(jpqlQuery);

			if (elementsPerPage != null) {
				first = 0;
				maxResults = elementsPerPage.intValue();
				if (pageNumber != null) {
					first = (pageNumber.intValue() - 1) * maxResults;
				}

				query.setFirstResult(first);
				query.setMaxResults(maxResults);
			}

			LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar los usuarios registrados en el sistema ...");

			ddbbRes = (List<Object[ ]>) query.getResultList();

			res = MappingUtilities.fillUserAndProfileListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuarios recuperados: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getUsersByOrganizationUnit(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.util.List)
	 */
	@Override
	public List<UserDTO> getUsersByOrganizationUnit(Long orgUnitId, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		int first, maxResults;
		List<User> ddbbRes = new ArrayList<User>();
		List<UserDTO> res = new ArrayList<UserDTO>();
		Map<String, List<EntityOrderByClause>> userOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando los usuarios perteneciente a la unidad organizativa " + orgUnitId + " ...");
			}

			if (orderByColumns != null && !orderByColumns.isEmpty()) {
				userOrderByColumns.put("users.", orderByColumns);
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT users FROM OrganizationUnit orgUnit JOIN orgUnit.users users WHERE orgUnit.id = :orgUnitId", userOrderByColumns);

			TypedQuery<User> query = entityManager.createQuery(jpqlQuery, User.class);
			query.setParameter("orgUnitId", orgUnitId);

			if (elementsPerPage != null) {
				first = 0;
				maxResults = elementsPerPage.intValue();
				if (pageNumber != null) {
					first = (pageNumber.intValue() - 1) * maxResults;
				}

				query.setFirstResult(first);
				query.setMaxResults(maxResults);
			}

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar los usuarios perteneciente a la unidad organizativa " + orgUnitId + " ...");
			}

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillUserListDto(ddbbRes);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuarios recuperados: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios perteneciente a la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los usuarios perteneciente a la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios perteneciente a la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios perteneciente a la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios perteneciente a la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los usuarios perteneciente a la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios perteneciente a la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getUser(java.lang.Long)
	 */
	@Override
	public UserDTO getUser(Long userId) throws UsersDAOException {
		User entity;
		UserDTO res = null;

		try {
			entity = getUserEntity(userId);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Parseando el usuario " + userId + " recuperado de BBDD...");
			}

			res = MappingUtilities.fillUserDto(entity);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getUserByUsername(java.lang.String)
	 */
	@Override
	public UserDTO getUserByUsername(String username) throws UsersDAOException {
		UserDTO res;
		User entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando el usuario " + username + " de BBDD ...");
			}

			TypedQuery<User> query = entityManager.createQuery("SELECT user FROM User user" + " WHERE user.username = :username", User.class);
			query.setParameter("username", username);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar el usuario " + username + " ...");
			}

			entity = query.getSingleResult();

			if (entity == null) {
				excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + username + " de BBDD. No se encontró la entidad.";
				LOG.error(excMsg);
				throw new UsersDAOException(excMsg);
			}

			res = MappingUtilities.fillUserDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuario " + res.getUsername() + " recuperado: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + username + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar el usuario " + username + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + username + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + username + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + username + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + username + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(UsersDAOException.CODE_901, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + username + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + username + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getUserByEmail(java.lang.String)
	 */
	@Override
	public UserDTO getUserByEmail(String email) throws UsersDAOException {
		UserDTO res;
		User entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando el usuario con dirección de correo " + email + " de BBDD ...");
			}

			TypedQuery<User> query = entityManager.createQuery("SELECT user FROM User user WHERE user.email = :email", User.class);
			query.setParameter("email", email);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar el usuario con dirección de correo " + email + " ...");
			}

			entity = query.getSingleResult();

			res = MappingUtilities.fillUserDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuario " + res.getUsername() + " recuperado: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con dirección de correo " + email + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar el usuario con dirección de correo " + email + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con dirección de correo " + email + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con dirección de correo " + email + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con dirección de correo " + email + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con dirección de correo " + email + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(UsersDAOException.CODE_901, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar el usuario con dirección de correo " + email + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con dirección de correo " + email + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getUserByDocument(java.lang.String)
	 */
	@Override
	public UserDTO getUserByDocument(String document) throws UsersDAOException {
		UserDTO res;
		User entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando el usuario con documento de identificación " + document + " de BBDD ...");
			}

			TypedQuery<User> query = entityManager.createQuery("SELECT user FROM User user WHERE user.document = :document", User.class);
			query.setParameter("document", document);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar el usuario con documento de identificación " + document + " ...");
			}

			entity = query.getSingleResult();

			res = MappingUtilities.fillUserDto(entity);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuario con documento de identificación " + document + " recuperado: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con documento de identificación " + document + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar el usuario con documento de identificación " + document + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con documento de identificación " + document + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con documento de identificación " + document + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con documento de identificación " + document + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con documento de identificación " + document + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(UsersDAOException.CODE_901, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar el usuario con documento de identificación " + document + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario con documento de identificación " + document + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#createUser(es.ricoh.webscan.users.model.dto.UserDTO)
	 */
	@Override
	public Long createUser(UserDTO user) throws UsersDAOException {
		Long res = null;
		Date creationDate;
		User entity;
		String excMsg;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Creando un usuario con dirección de correo " + user.getEmail() + " en BBDD ...");
		}

		entity = MappingUtilities.fillUserEntity(user);
		creationDate = new Date();
		entity.setCreationDate(creationDate);
		entity.setLastUpdateDate(creationDate);

		try {
			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Nueva usuario con dirección de correo " + user.getEmail() + " creado, identificador: " + res + ".");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear usuario con dirección de correo " + user.getEmail() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(UsersDAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear el usuario con dirección de correo " + user.getEmail() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear el usuario con dirección de correo " + user.getEmail() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear el usuario con dirección de correo " + user.getEmail() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear el usuario con dirección de correo " + user.getEmail() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#updateUser(es.ricoh.webscan.users.model.dto.UserDTO,
	 *      java.lang.Boolean)
	 */
	@Override
	public void updateUser(UserDTO user, Boolean setUpdateDate) throws UsersDAOException {
		User entity;
		String excMsg;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Actualizando el usuario " + user.getId() + " ...");
		}

		try {
			entity = getUserEntity(user.getId());

			entity.setDocument(user.getDocument());
			entity.setEmail(user.getEmail());
			entity.setLocked(user.getLocked());
			entity.setName(user.getName());
			entity.setNextPassUpdateDate(user.getNextPassUpdateDate());
			entity.setPassword(user.getPassword());
			entity.setSecondSurname(user.getSecondSurname());
			entity.setSurname(user.getSurname());
			entity.setUsername(user.getUsername());
			entity.setFailedAttempts(user.getFailedAttempts());

			if (setUpdateDate) {
				entity.setLastUpdateDate(new Date());
			}

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Modificando el usuario " + user.getId() + " en BBDD ...");
			}

			entityManager.merge(entity);
			entityManager.flush();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuario " + user.getId() + " actualizado en BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando el usuario " + user.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando el usuario " + user.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando el usuario " + user.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando el usuario " + user.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#removeUser(java.lang.Long)
	 */
	@Override
	public void removeUser(Long userId) throws UsersDAOException {
		User entity;
		String excMsg;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Eliminando el usuario " + userId + " ...");
		}

		try {
			entity = getUserEntity(userId);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Eliminando el usuario " + userId + " de BBDD ...");
			}

			entityManager.remove(entity);
			entityManager.flush();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuario " + userId + " eliminado de BBDD ...");
			}
		} catch (UsersDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error eliminando el usuario " + userId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error eliminando el usuario " + userId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error eliminando el usuario " + userId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error eliminando el usuario " + userId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#listUserProfiles(java.lang.Long,
	 *      java.lang.Long, java.util.List)
	 */
	@Override
	public List<ProfileDTO> listUserProfiles() throws UsersDAOException {
		List<Profile> ddbbRes = new ArrayList<Profile>();
		List<ProfileDTO> res = new ArrayList<ProfileDTO>();
		String excMsg, jpqlQuery;

		try {

			LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando los perfiles de usuario registrados en el sistema ...");

			jpqlQuery = "SELECT profile FROM Profile profile";

			TypedQuery<Profile> query = entityManager.createQuery(jpqlQuery, Profile.class);

			LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar los perfiles de usuario registrados en el sistema ...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillProfileListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Perfiles de usuario recuperados: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles de usuario registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los perfiles de usuario registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles de usuario registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles de usuario registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles de usuario registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los perfiles de usuario registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles de usuario registrados en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getUserProfilesByUser(java.lang.Long)
	 */
	@Override
	public List<ProfileDTO> getUserProfilesByUser(Long userId) throws UsersDAOException {
		List<Profile> ddbbRes = new ArrayList<Profile>();
		List<ProfileDTO> res = new ArrayList<ProfileDTO>();
		String excMsg;
		User entity;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando los perfiles del usuario " + userId + " de BBDD ...");
			}

			entity = getUserEntity(userId);

			ddbbRes = entity.getProfiles();

			res = MappingUtilities.fillProfileListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] " + res.size() + " perfiles recuperados para el usuario " + userId + "  de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles del usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los perfiles del usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles del usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles del usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles del usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar los perfiles del usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles del usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#setUserProfiles(java.lang.Long,
	 *      java.util.List)
	 */
	@Override
	public void setUserProfiles(Long userId, List<ProfileDTO> userProfiles) throws UsersDAOException {
		List<Profile> newProfiles;
		List<ProfileDTO> currentProfiles;
		String excMsg;
		User user;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Estableciendo una nueva lista de perfiles para el usuario " + userId + " en BBDD...");
		}

		try {
			user = getUserEntity(userId);

			currentProfiles = MappingUtilities.fillProfileListDto(user.getProfiles());

			newProfiles = mergeProfileUserList(userProfiles, currentProfiles, user);

			user.setProfiles(newProfiles);
			entityManager.merge(user);
			entityManager.flush();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Perfiles del usuario " + userId + " actualizados en BBDD ...");
			}
		} catch (UsersDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando los perfiles del usuario " + userId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando los perfiles del usuario " + userId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando los perfiles del usuario " + userId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando los perfiles del usuario " + userId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * Efectúa la fusión en BBDD de la relación actual de perfiles y la nueva
	 * relación de perfiles de un usuario.
	 * 
	 * @param newProfiles
	 *            Nueva relación de perfiles de usuario.
	 * @param currentProfiles
	 *            Actual relación de perfiles de usuario almacenada en BBDD.
	 * @param user
	 *            Usuario.
	 * @return Nueva lista de perfiles de usuario.
	 * @throws UsersDAOException
	 *             Si ocurre algún error al obtener los nuevos perfiles de
	 *             usuario en BBDD.
	 */
	private List<Profile> mergeProfileUserList(List<ProfileDTO> newProfiles, List<ProfileDTO> currentProfiles, User user) throws UsersDAOException {
		List<Profile> res = new ArrayList<Profile>();

		res = addProfileUserList(newProfiles, currentProfiles, user);

		removeProfileUserList(newProfiles, currentProfiles, user);

		return res;
	}

	/**
	 * Obtiene la nueva relación de perfiles de usuario al fusionar los nuevos
	 * perfiles con los actualmente registrados en BBDD.
	 * 
	 * @param newProfiles
	 *            Nueva lista de perfiles de usuario.
	 * @param currentProfiles
	 *            Lista actual de perfiles de usuario.
	 * @param user
	 *            Entidad usuario.
	 * @return Nueva lista de perfiles de usuario.
	 * @throws UsersDAOException
	 *             Si ocurre algún error al ejecutar alguna de las sentecias de
	 *             BBDD.
	 */
	private List<Profile> addProfileUserList(List<ProfileDTO> newProfiles, List<ProfileDTO> currentProfiles, User user) throws UsersDAOException {
		Boolean newProfilesFound;
		List<Profile> res = new ArrayList<Profile>();
		Profile profile;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Añadiendo perfiles de usuario al usuario " + user.getUsername() + " en BBDD ...");
			}

			newProfilesFound = newProfiles != null && !newProfiles.isEmpty();

			if (newProfilesFound) {
				// Inserciones y actualizaciones
				LOG.debug("[WEBSCAN-MODEL] Se añaden los nuevos perfiles y mantienen los ya existentes ...");

				for (ProfileDTO profileDto: newProfiles) {
					profile = getProfileEntity(profileDto.getId());

					if (LOG.isDebugEnabled()) {
						LOG.debug("[WEBSCAN-MODEL] Incluyendo el perfil de usuario " + profile.getName() + " ...");
					}
					res.add(profile);

					if (!profile.getUsers().contains(user)) {
						profile.getUsers().add(user);
						entityManager.merge(profile);
					}
				}
			}
		} catch (UsersDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error añadiendo perfiles de usuario al usuario " + user.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error añadiendo perfiles de usuario al usuario " + user.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error añadiendo perfiles de usuario al usuario " + user.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Elimina perfiles de usuario no reflejados en la nueva relación de roles
	 * de un usuario.
	 * 
	 * @param newProfiles
	 *            Nueva lista de perfiles de usuario.
	 * @param currentProfiles
	 *            Lista actual de perfiles de usuario.
	 * @param user
	 *            Entidad usuario.
	 * @throws UsersDAOException
	 *             Si ocurre algún error al ejecutar alguna de las sentecias de
	 *             BBDD.
	 */
	private void removeProfileUserList(List<ProfileDTO> newProfiles, List<ProfileDTO> currentProfiles, User user) throws UsersDAOException {
		Boolean found;
		Profile profile;
		ProfileDTO dto;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Eliminando los perfiles de usuario no reflejados en la nueva relación de perfiles para el suario " + user.getUsername() + " en BBDD ...");
			}
			if (currentProfiles != null && !currentProfiles.isEmpty()) {
				// Borrados

				for (ProfileDTO auxProfileDto: currentProfiles) {
					found = Boolean.FALSE;
					for (Iterator<ProfileDTO> it = newProfiles.iterator(); !found && it.hasNext();) {
						dto = it.next();
						found = dto.getId().equals(auxProfileDto.getId());
					}

					if (!found) {
						// Se elimina relación
						profile = getProfileEntity(auxProfileDto.getId());
						profile.getUsers().remove(user);
						entityManager.merge(profile);
					}
				}

			}
		} catch (UsersDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error eliminando los perfiles de usuario configurados para el usuario " + user.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error eliminando los perfiles de usuario configurados para el usuario " + user.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los perfiles de usuario configurados para el usuario " + user.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getOrganizationUnitsByUserId(java.lang.Long)
	 */
	@Override
	public List<OrganizationUnitDTO> getOrganizationUnitsByUserId(Long userId) throws UsersDAOException {
		List<OrganizationUnitDTO> res = new ArrayList<OrganizationUnitDTO>();
		List<OrganizationUnit> ddbbRes;
		String excMsg;
		User entity;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando las unidades organizativas a las que pertenece el usuario " + userId + " de BBDD ...");
			}
			entity = getUserEntity(userId);

			ddbbRes = entity.getOrgUnits();
			res = MappingUtilities.fillOrganizationUnitListDto(ddbbRes);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] " + res.size() + " unidades organizativas a las que pertenece el usuario " + userId + "  de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las unidades organizativas a las que pertenece el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar las unidades organizativas a las que pertenece el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las unidades organizativas a las que pertenece el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las unidades organizativas a las que pertenece el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las unidades organizativas a las que pertenece el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar las unidades organizativas a las que pertenece el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles del usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getOrganizationUnitsByIds(java.util.List)
	 */
	@Override
	public List<OrganizationUnitDTO> getOrganizationUnitsByIds(List<Long> orgUnitIds) throws UsersDAOException {
		List<OrganizationUnit> ddbbRes = new ArrayList<OrganizationUnit>();
		List<OrganizationUnitDTO> res = new ArrayList<OrganizationUnitDTO>();
		String excMsg, jpqlQuery;

		try {

			LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando una relación de unidades organizativas a partir de sus identificadores de BBDD ...");

			jpqlQuery = "SELECT orgUnit FROM OrganizationUnit orgUnit WHERE orgUnit.id IN :orgUnitIds";

			TypedQuery<OrganizationUnit> query = entityManager.createQuery(jpqlQuery, OrganizationUnit.class);
			query.setParameter("orgUnitIds", orgUnitIds);

			LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar una relación de unidades organizativas a partir de sus identificadores de BBDD ...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillOrganizationUnitListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Unidades organizativas recuperadas: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando una relación de unidades organizativas a partir de sus identificadores de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar una relación de unidades organizativas a partir de sus identificadores de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando una relación de unidades organizativas a partir de sus identificadores de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando una relación de unidades organizativas a partir de sus identificadores de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando una relación de unidades organizativas a partir de sus identificadores de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar una relación de unidades organizativas a partir de sus identificadores de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando una relación de unidades organizativas a partir de sus identificadores de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#setOrganizationUnitUsers(java.lang.Long,
	 *      java.util.List)
	 */
	@Override
	public void setOrganizationUnitUsers(Long orgUnitId, List<UserDTO> users) throws UsersDAOException {
		OrganizationUnit orgUnit;
		List<User> newUserList;
		String excMsg;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Estableciendo una nueva lista de usuarios para la unidad organizativa " + orgUnitId + " en BBDD (" + (users != null ? users.size() : 0) + ")...");
		}

		try {
			orgUnit = getOrganizationUnitEntity(orgUnitId);

			newUserList = mergeOrgUnitUsersList(users, MappingUtilities.fillUserListDto(orgUnit.getUsers()), orgUnit);

			orgUnit.setUsers(newUserList);
			entityManager.merge(orgUnit);
			entityManager.flush();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuarios de la unidad organizativa " + orgUnitId + " actualizados en BBDD ...");
			}

		} catch (UsersDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error incorporando usuarios a la unidad organizativa " + orgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error incorporando usuarios a la unidad organizativa " + orgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error incorporando usuarios a la unidad organizativa " + orgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error incorporando usuarios a la unidad organizativa " + orgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#setUserOrganizationUnits(java.lang.Long,
	 *      java.util.List)
	 */
	@Override
	public void setUserOrganizationUnits(Long userId, List<OrganizationUnitDTO> orgUnits) throws UsersDAOException {
		User user;
		List<OrganizationUnit> newOrgUnitList;
		String excMsg;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Estableciendo una nueva relación de unidades organizativas a las que pertence el usuario " + userId + " en BBDD ...");
		}

		try {
			user = getUserEntity(userId);

			newOrgUnitList = mergeUserOrgUnitsList(orgUnits, MappingUtilities.fillOrganizationUnitListDto(user.getOrgUnits()), user);

			user.setOrgUnits(newOrgUnitList);
			entityManager.merge(user);
			entityManager.flush();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Relación de unidades organizativas actualizadas para le usuario " + userId + " en BBDD ...");
			}

		} catch (UsersDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error estableciendo una nueva relación de unidades organizativas a las que pertence el usuario " + userId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error estableciendo una nueva relación de unidades organizativas a las que pertence el usuario " + userId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error estableciendo una nueva relación de unidades organizativas a las que pertence el usuario " + userId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error estableciendo una nueva relación de unidades organizativas a las que pertence el usuario " + userId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * Efectúa la fusión en BBDD de la relación actual y la nueva relación de
	 * unidades organizativas de un usuario.
	 * 
	 * @param neworgUnits
	 *            Nueva relación de unidades organizativas de un usuario.
	 * @param currentOrgUnits
	 *            Actual relación de unidades organizativas almacenada en BBDD.
	 * @param user
	 *            Entidad usuario.
	 * @return Nueva lista de usuarios de la unidad organizativa.
	 * @throws UsersDAOException
	 *             Si ocurre algún error al obtener los nuevos usuarios de la
	 *             unidad organizativa de BBDD.
	 */
	private List<OrganizationUnit> mergeUserOrgUnitsList(List<OrganizationUnitDTO> neworgUnits, List<OrganizationUnitDTO> currentOrgUnits, User user) throws UsersDAOException {
		List<OrganizationUnit> res;

		res = addUserOrgUnitsList(neworgUnits, currentOrgUnits, user);

		removeUserOrgUnitsList(neworgUnits, currentOrgUnits, user);

		return res;
	}

	/**
	 * Obtiene la nueva relación de unidades organizativas de un usuario al
	 * fusionar las nuevas unidades organizativas con las actualmente
	 * registradas en BBDD.
	 * 
	 * @param neworgUnits
	 *            Nueva lista de unidades organizativas.
	 * @param currentOrgUnits
	 *            Lista actual de unidades organizativas.
	 * @param user
	 *            Entidad usuario.
	 * @return Nueva lista de unidades organizativas.
	 * @throws UsersDAOException
	 *             Si ocurre algún error al ejecutar alguna de las sentecias de
	 *             BBDD.
	 */
	private List<OrganizationUnit> addUserOrgUnitsList(List<OrganizationUnitDTO> neworgUnits, List<OrganizationUnitDTO> currentOrgUnits, User user) throws UsersDAOException {
		Boolean newOrgUnitsFound;
		List<OrganizationUnit> res = new ArrayList<OrganizationUnit>();
		OrganizationUnit orgUnit;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Añadiendo unidades organizativas al usuario " + user.getUsername() + " en BBDD ...");
			}

			newOrgUnitsFound = neworgUnits != null && !neworgUnits.isEmpty();

			if (newOrgUnitsFound) {
				// Inserciones y actualizaciones
				LOG.debug("[WEBSCAN-MODEL] Se añaden los nuevos perfiles y mantienen los ya existentes ...");

				for (OrganizationUnitDTO orgUnitDto: neworgUnits) {
					orgUnit = getOrganizationUnitEntity(orgUnitDto.getId());

					if (LOG.isDebugEnabled()) {
						LOG.debug("[WEBSCAN-MODEL] Incluyendo la unidad organizativa " + orgUnit.getId() + " ...");
					}

					res.add(orgUnit);

					if (!orgUnit.getUsers().contains(user)) {
						orgUnit.getUsers().add(user);
						entityManager.merge(orgUnit);
					}
				}
			}
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error añadiendo perfiles de usuario al usuario " + user.getUsername() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Elimina las unidades organizativas no reflejadas en la nueva relación de
	 * unidades organizativas de un usuario.
	 * 
	 * @param neworgUnits
	 *            Nueva lista de unidades organizativas.
	 * @param currentOrgUnits
	 *            Lista actual de unidades organizativas.
	 * @param user
	 *            Entidad usuario.
	 * @throws UsersDAOException
	 *             Si ocurre algún error al ejecutar alguna de las sentecias de
	 *             BBDD.
	 */
	private void removeUserOrgUnitsList(List<OrganizationUnitDTO> neworgUnits, List<OrganizationUnitDTO> currentOrgUnits, User user) throws UsersDAOException {
		Boolean found;
		OrganizationUnit orgUnit;
		OrganizationUnitDTO dto;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Eliminando las unidades organizativas no reflejadas en la nueva relación de unidades organizativas para el usuario " + user.getUsername() + " en BBDD ...");
			}

			if (currentOrgUnits != null && !currentOrgUnits.isEmpty()) {
				// Borrados

				for (OrganizationUnitDTO auxOrgUnitDto: currentOrgUnits) {
					found = Boolean.FALSE;
					for (Iterator<OrganizationUnitDTO> it = neworgUnits.iterator(); !found && it.hasNext();) {
						dto = it.next();
						found = dto.getId().equals(auxOrgUnitDto.getId());
					}

					if (!found) {
						// Se elimina relación
						orgUnit = getOrganizationUnitEntity(auxOrgUnitDto.getId());
						orgUnit.getUsers().remove(user);
						entityManager.merge(orgUnit);
					}
				}

			}
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las unidades organizativas no reflejadas en la nueva relación de unidades organizativas para el usuario " + user.getUsername() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

	}

	/**
	 * Efectúa la fusión en BBDD de la relación actual de usuarios y la nueva
	 * relación de usuarios de una unidad organizativa.
	 * 
	 * @param newUsers
	 *            Nueva relación de usuarios.
	 * @param currentUsers
	 *            Actual relación de usuarios almacenada en BBDD.
	 * @param orgUnit
	 *            Entidad unidad organizativa.
	 * @return Nueva lista de usuarios de la unidad organizativa.
	 * @throws UsersDAOException
	 *             Si ocurre algún error al obtener los nuevos usuarios de la
	 *             unidad organizativa de BBDD.
	 */
	private List<User> mergeOrgUnitUsersList(List<UserDTO> newUsers, List<UserDTO> currentUsers, OrganizationUnit orgUnit) throws UsersDAOException {
		List<User> res;

		// Se añaden los nuevos y existen
		res = addOrgUnitUsersList(newUsers, currentUsers, orgUnit);

		// Se elimanan los no reflejados en la nueva lista
		removeOrgUnitUsersList(newUsers, currentUsers, orgUnit);
		return res;
	}

	/**
	 * Obtiene la nueva relación de usuarios al fusionar los nuevos usuarios con
	 * los actualmente registrados en BBDD.
	 * 
	 * @param newUsers
	 *            Nueva lista de usuarios.
	 * @param currentUsers
	 *            Lista actual de usuarios.
	 * @param orgUnit
	 *            Entidad unidad organizativa.
	 * @return Nueva lista de usuarios.
	 * @throws UsersDAOException
	 *             Si ocurre algún error al ejecutar alguna de las sentecias de
	 *             BBDD.
	 */
	private List<User> addOrgUnitUsersList(List<UserDTO> newUsers, List<UserDTO> currentUsers, OrganizationUnit orgUnit) throws UsersDAOException {
		Boolean newUsersFound;
		List<User> res = new ArrayList<User>();
		User user;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Añadiendo usuarios a la unidad organizativa " + orgUnit.getId() + " en BBDD ...");
			}

			newUsersFound = newUsers != null && !newUsers.isEmpty();

			if (newUsersFound) {
				// Inserciones y actualizaciones
				LOG.debug("[WEBSCAN-MODEL] Se añaden nuevos usuarios y mantienen los ya existentes ...");

				for (UserDTO userDto: newUsers) {
					user = getUserEntity(userDto.getId());

					if (LOG.isDebugEnabled()) {
						LOG.debug("[WEBSCAN-MODEL] Añadiendo el usuario " + user.getUsername() + " ...");
					}
					res.add(user);

					if (!user.getOrgUnits().contains(orgUnit)) {
						user.getOrgUnits().add(orgUnit);
						entityManager.merge(user);
					}
				}
			}
		} catch (UsersDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error añadiendo usuarios a la unidad organizativa " + orgUnit.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error añadiendo usuarios a la unidad organizativa " + orgUnit.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error añadiendo usuarios a la unidad organizativa " + orgUnit.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Elimina los usuarios no reflejados en la nueva relación de usuarios de
	 * una unidad organizativa.
	 * 
	 * @param newUsers
	 *            Nueva lista de usuarios.
	 * @param currentUsers
	 *            Lista actual de usuarios.
	 * @param orgUnit
	 *            Entidad unidad organizativa.
	 * @throws UsersDAOException
	 *             Si ocurre algún error al ejecutar alguna de las sentecias de
	 *             BBDD.
	 */
	private void removeOrgUnitUsersList(List<UserDTO> newUsers, List<UserDTO> currentUsers, OrganizationUnit orgUnit) throws UsersDAOException {
		Boolean found;
		User user;
		UserDTO dto;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Eliminando los usuarios no reflejados en la nueva relación de usuarios para la unidad organizativa " + orgUnit.getId() + " en BBDD ...");
			}

			if (currentUsers != null && !currentUsers.isEmpty()) {
				// Borrados

				for (UserDTO auxUserDto: currentUsers) {
					found = Boolean.FALSE;
					for (Iterator<UserDTO> it = newUsers.iterator(); !found && it.hasNext();) {
						dto = it.next();
						found = dto.getId().equals(auxUserDto.getId());
					}

					if (!found) {
						// Se elimina relación
						user = getUserEntity(auxUserDto.getId());
						user.getOrgUnits().remove(orgUnit);
						entityManager.merge(user);
					}
				}

			}
		} catch (UsersDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error eliminando asociación de usuarios a la unidad organizativa " + orgUnit.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error eliminando asociación de usuarios a la unidad organizativa " + orgUnit.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando asociación de usuarios a la unidad organizativa " + orgUnit.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getRecoveryPassReqByUserAndStatus(java.lang.String,
	 *      es.ricoh.webscan.users.model.domain.RecoveryUserPasswordRequestStatus,
	 *      java.util.List)
	 */
	@Override
	public List<RecoveryUserPasswordRequestDTO> getRecoveryPassReqByUserAndStatus(String userEmail, RecoveryUserPasswordRequestStatus status, List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		List<RecoveryUserPasswordRequestDTO> res = new ArrayList<RecoveryUserPasswordRequestDTO>();
		List<RecoveryUserPasswordRequest> recPassReqs;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + " de BBDD ...");
			}
			TypedQuery<RecoveryUserPasswordRequest> query = entityManager.createQuery("SELECT recPassReq FROM RecoveryUserPasswordRequest recPassReq JOIN recPassReq.user user " + "WHERE user.email = :userEmail AND recPassReq.status = :status", RecoveryUserPasswordRequest.class);
			query.setParameter("userEmail", userEmail);
			query.setParameter("status", status.getStatus());

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + " de BBDD ...");
			}
			recPassReqs = query.getResultList();

			res = MappingUtilities.fillRecoveryUserPasswordRequestListDto(recPassReqs);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Peticiones de recuperación de password obtenidas: " + res.size());
			}

		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(UsersDAOException.CODE_901, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password del usuario con dirección de correo " + userEmail + " y estado " + status.getStatus() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getRecoveryPassReqInProcessByUserEmail(java.lang.String)
	 */
	@Override
	public List<RecoveryUserPasswordRequestDTO> getRecoveryPassReqInProcessByUserEmail(String userEmail) throws UsersDAOException {
		List<RecoveryUserPasswordRequestDTO> res = new ArrayList<RecoveryUserPasswordRequestDTO>();
		List<RecoveryUserPasswordRequest> ddbbRes;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando las peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + " en proceso de BBDD ...");
			}
			jpqlQuery = "SELECT recPassReq FROM RecoveryUserPasswordRequest recPassReq JOIN recPassReq.user user" + " WHERE user.email = :userEmail AND recPassReq.status = :status ORDER BY recPassReq.comSendingDate DESC";
			TypedQuery<RecoveryUserPasswordRequest> query = entityManager.createQuery(jpqlQuery, RecoveryUserPasswordRequest.class);
			query.setParameter("userEmail", userEmail);
			query.setParameter("status", RecoveryUserPasswordRequestStatus.IN_PROCESS);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar las peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + " en proceso de BBDD ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillRecoveryUserPasswordRequestListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] " + res.size() + " Peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + " obtenidas de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar las peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar las peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password abiertas para el usuario con dirección de correo " + userEmail + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getRecoveryPassReqsByUserId(java.lang.Long,
	 *      java.util.List)
	 */
	@Override
	public List<RecoveryUserPasswordRequestDTO> getRecoveryPassReqsByUserId(Long userId, List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		List<RecoveryUserPasswordRequestDTO> res = new ArrayList<RecoveryUserPasswordRequestDTO>();
		List<RecoveryUserPasswordRequest> recPassReqs;
		Map<String, List<EntityOrderByClause>> rprOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando las peticiones de recuperación de password para el usuario " + userId + " de BBDD ...");
			}

			if (orderByColumns != null && !orderByColumns.isEmpty()) {
				rprOrderByColumns.put("recPassReq.", orderByColumns);
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT recPassReq FROM RecoveryUserPasswordRequest recPassReq JOIN recPassReq.user user WHERE user.id = :userId", rprOrderByColumns);

			TypedQuery<RecoveryUserPasswordRequest> query = entityManager.createQuery(jpqlQuery, RecoveryUserPasswordRequest.class);
			query.setParameter("userId", userId);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Ejecutando consulta para recuperar las peticiones de recuperación de password para el usuario " + userId + " de BBDD ...");
			}
			recPassReqs = query.getResultList();

			res = MappingUtilities.fillRecoveryUserPasswordRequestListDto(recPassReqs);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Peticiones de recuperación de password obtenidas: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password para el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar las peticiones de recuperación de password para el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password para el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password para el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password para el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Se excedio el tiempo máximo para recuperar las peticiones de recuperación de password para el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando las peticiones de recuperación de password para el usuario " + userId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#getRecoveryPassReq(java.lang.Long)
	 */
	@Override
	public RecoveryUserPasswordRequestDTO getRecoveryPassReq(Long recPassReqId) throws UsersDAOException {
		RecoveryUserPasswordRequest entity;
		RecoveryUserPasswordRequestDTO res = null;
		String excMsg;

		try {
			entity = getRecoveryUserPasswordRequestEntity(recPassReqId);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Parseando la petición de recuperación de password " + recPassReqId + " recuperada de BBDD...");
			}

			res = MappingUtilities.fillRecoveryUserPasswordRequestDto(entity);
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la petición de recuperación de password " + recPassReqId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#createRecoveryUserPasswordRequest(es.ricoh.webscan.users.model.dto.RecoveryUserPasswordRequestDTO)
	 */
	@Override
	public Long createRecoveryUserPasswordRequest(RecoveryUserPasswordRequestDTO recUserPassReq) throws UsersDAOException {
		Long res = null;
		RecoveryUserPasswordRequest entity;
		User user;
		String excMsg;

		LOG.debug("[WEBSCAN-USERS-MODEL] Creando una petición de recuperación de password de usuario en BBDD ...");

		if (recUserPassReq.getUserId() == null) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear una petición de recuperación de password de usuario.  No ha sido informado el usuario";
			LOG.error(excMsg);
			throw new UsersDAOException(excMsg);
		}

		user = getUserEntity(recUserPassReq.getUserId());

		entity = MappingUtilities.fillRecoveryUserPasswordRequestEntity(recUserPassReq);
		entity.setUser(user);

		try {
			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Nueva petición de recuperación de password de usuario creada para el usuario " + recUserPassReq.getUserId() + ", identificador: " + res + ".");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear una nueva petición de recuperación de password para el usuario " + recUserPassReq.getUserId() + " en BBDD, la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(UsersDAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear una nueva petición de recuperación de password para el usuario " + recUserPassReq.getUserId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear una nueva petición de recuperación de password para el usuario " + recUserPassReq.getUserId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear una nueva petición de recuperación de password para el usuario " + recUserPassReq.getUserId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear una nueva petición de recuperación de password para el usuario " + recUserPassReq.getUserId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.users.model.dao.UserDAO#updateRecoveryUserPasswordRequest(es.ricoh.webscan.users.model.dto.RecoveryUserPasswordRequestDTO)
	 */
	@Override
	public void updateRecoveryUserPasswordRequest(RecoveryUserPasswordRequestDTO recUserPassReq) throws UsersDAOException {
		RecoveryUserPasswordRequest entity;
		String excMsg;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS-MODEL] Actualizando la petición de recuperación de password " + recUserPassReq.getId() + " en BBDD ...");
		}

		try {
			entity = getRecoveryUserPasswordRequestEntity(recUserPassReq.getId());

			entity.setRecoveryDate(recUserPassReq.getRecoveryDate());
			entity.setStatus(recUserPassReq.getStatus());

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Modificando la petición de recuperación de password " + recUserPassReq.getId() + " en BBDD ...");
			}

			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Petición de recuperación de password " + recUserPassReq.getId() + " actualizada en BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando la petición de recuperación de password " + recUserPassReq.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando la petición de recuperación de password " + recUserPassReq.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando la petición de recuperación de password " + recUserPassReq.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error actualizando la petición de recuperación de password " + recUserPassReq.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * Recupera de BBDD una entidad usuario a partir de su identificador.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @return Entidad de BBDD usuario.
	 * @throws UsersDAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private User getUserEntity(Long userId) throws UsersDAOException {
		User res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando el usuario " + userId + " de BBDD ...");
			}
			res = entityManager.find(User.class, userId);

			if (res == null) {
				excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + userId + " de BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new UsersDAOException(UsersDAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Usuario " + res.getId() + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + userId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el usuario " + userId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad unidad organizativa a partir de su
	 * identificador.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return Entidad de BBDD unidad organizativa.
	 * @throws UsersDAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private OrganizationUnit getOrganizationUnitEntity(Long orgUnitId) throws UsersDAOException {
		OrganizationUnit res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando la unidad organizativa " + orgUnitId + " de BBDD ...");
			}
			res = entityManager.find(OrganizationUnit.class, orgUnitId);

			if (res == null) {
				excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la unidad organizativa " + orgUnitId + " de BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new UsersDAOException(UsersDAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Unidad organizativa " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la unidad organizativa " + orgUnitId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la unidad organizativa " + orgUnitId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad perfil de usuario a partir de su
	 * identificador.
	 * 
	 * @param profileId
	 *            Identificador de perfil de usuario.
	 * @return Entidad de BBDD perfil de usuario.
	 * @throws UsersDAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private Profile getProfileEntity(Long profileId) throws UsersDAOException {
		Profile res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando el perfil de usuario " + profileId + " de BBDD ...");
			}

			res = entityManager.find(Profile.class, profileId);

			if (res == null) {
				excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el perfil de usuario " + profileId + " de BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new UsersDAOException(UsersDAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Perfil de usuario " + res.getId() + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el perfil de usuario " + profileId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando el perfil de usuario " + profileId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad petición de recuperación de password a
	 * partir de su identificador.
	 * 
	 * @param recPassReqId
	 *            Identificador de petición de recuperación de password.
	 * @return Entidad de BBDD petición de recuperación de password.
	 * @throws UsersDAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private RecoveryUserPasswordRequest getRecoveryUserPasswordRequestEntity(Long recPassReqId) throws UsersDAOException {
		RecoveryUserPasswordRequest res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Recuperando la petición de recuperación de password " + recPassReqId + " de BBDD ...");
			}
			res = entityManager.find(RecoveryUserPasswordRequest.class, recPassReqId);

			if (res == null) {
				excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la petición de recuperación de password " + recPassReqId + " de BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new UsersDAOException(UsersDAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS-MODEL] Petición de recuperación de password " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la petición de recuperación de password " + recPassReqId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		} catch (UsersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando la petición de recuperación de password " + recPassReqId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		return res;
	}

}
