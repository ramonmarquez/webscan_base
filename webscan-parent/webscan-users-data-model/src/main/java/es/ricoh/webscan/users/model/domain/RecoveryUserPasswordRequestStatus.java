/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.domain.RecoveryUserPasswordRequestStatus.java.</p>
 * <b>Descripción:</b><p> Clase que define el conjunto de estados de las peticiones de recuperación de password.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model.domain;

/**
 * Clase que define el conjunto de estados de las peticiones de recuperación de
 * password.
 * <p>
 * Clase RecoveryUserPasswordRequestStatus.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum RecoveryUserPasswordRequestStatus {
	/**
	 * Estado PERFORMED.
	 */
	PERFORMED("PERFORMED"),
	/**
	 * Estado CANCELED.
	 */
	CANCELED("CANCELED"),
	/**
	 * Estado IN PROCESS.
	 */
	IN_PROCESS("IN PROCESS"),
	/**
	 * Estado EXPIRED.
	 */
	EXPIRED("EXPIRED");

	/**
	 * Estado de peticiones de recuperación de password.
	 */
	private String status;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aStatus
	 *            Estado de peticiones de recuperación de password.
	 */
	RecoveryUserPasswordRequestStatus(String aStatus) {
		this.status = aStatus;
	}

	/**
	 * Obtiene el atributo status.
	 * 
	 * @return el atributo status.
	 */
	public String getStatus() {
		return status;
	}

}
