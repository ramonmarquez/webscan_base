/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.UsersDataValidationUtilities.java.</p>
 * <b>Descripción:</b><p> Utilidad de comprobación y verificación de objetos DTO intercambiados 
 * con la capa DAO.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.model;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.ValidationUtilities;
import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.users.model.dto.ProfileDTO;
import es.ricoh.webscan.users.model.dto.RecoveryUserPasswordRequestDTO;
import es.ricoh.webscan.users.model.dto.UserDTO;

/**
 * Utilidad de comprobación y verificación de objetos DTO intercambiados con la
 * capa DAO.
 * <p>
 * Clase UsersDataValidationUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
class UsersDataValidationUtilities extends ValidationUtilities {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UsersDataValidationUtilities.class);

	/**
	 * Comprueba una lista de campos de ordenación de consulta sobre la entidad
	 * User de BBDD, verificando cada una de ellas.
	 * 
	 * @param orderByColumns
	 *            Relación de de campos de ordenación de consulta la entidad
	 *            User de BBDD.
	 * @throws UsersDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkUserGroupByClause(List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		String excMsg;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (EntityOrderByClause eobc: orderByColumns) {
				if (!(eobc instanceof UserOrderByClause)) {

					excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los usuarios registrados en el sistema, " + "clausula ORDER BY incorrecta (tipo esperado " + UserOrderByClause.class + ", tipo informado " + orderByColumns.get(0).getClass() + ").";
					LOG.error(excMsg);
					throw new UsersDAOException(excMsg);
				}
			}
		}
	}

	/**
	 * Comprueba que un usuario no sea nulo, y posea nombre de usuario, nombre
	 * de pila, primer apellido, correo electrónico, documento de
	 * identificación, password y un número de intentos fallidos de acceso mayor
	 * o igual a cero.
	 * 
	 * @param user
	 *            Usuario.
	 * @param checkPassword
	 *            Indica si se debe validar la password del usuario.
	 * @throws UsersDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkUser(UserDTO user, Boolean checkPassword) throws UsersDAOException {
		Boolean isNew;
		String excMsg;

		isNew = user != null && user.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear un nuevo usuario.";
		} else {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al actualizar el usuario " + (user != null ? user.getUsername() : "No informado") + ".";
		}

		try {
			checkEmptyObject(user, excMsg + "Usuario nulo o vacío.");
			checkEmptyObject(user.getUsername(), excMsg + "Nombre de usuario nulo o vacío.");
			checkEmptyObject(user.getName(), excMsg + "Nombre de pila nulo o vacío.");
			checkEmptyObject(user.getSurname(), excMsg + "Primer apellido nulo o vacío.");
			checkEmptyObject(user.getDocument(), excMsg + "Documento de identificación nulo o vacío.");
			checkEmptyObject(user.getEmail(), excMsg + "Dirección de correo electrónico nula o vacía.");
			if (checkPassword) {
				checkEmptyObject(user.getPassword(), excMsg + "Password nula o vacía.");
			}
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e.getCause());
		}

		if (user.getFailedAttempts() == null || user.getFailedAttempts() < 0) {
			excMsg += "Número de intentos de inicio sesión fallidos nulo o negativo.";
			LOG.error(excMsg);
			throw new UsersDAOException(excMsg);
		}
	}

	/**
	 * Comprueba si un usuario se encuentra bloqueado.
	 * 
	 * @param user
	 *            Usuario
	 * @throws UsersDAOException
	 *             Si los datos no son válidos o el usuario se encuentra
	 *             bloqueado.
	 */
	static void checkLockedUser(UserDTO user) throws UsersDAOException {
		Boolean isNew;
		String excMsg;

		isNew = user != null && user.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear un nuevo usuario.";
		} else {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al actualizar el usuario " + user.getId() + ".";
		}

		try {
			checkEmptyObject(user, excMsg + "Usuario nulo o vacío.");
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e.getCause());
		}

		if (!isNew && user.getLocked()) {
			excMsg += "Usuario bloqueado.";
			LOG.error(excMsg);
			throw new UsersDAOException(excMsg);
		}
	}

	/**
	 * Comprueba una lista de campos de ordenación de consulta sobre la entidad
	 * Profile de BBDD, verificando cada una de ellas.
	 * 
	 * @param orderByColumns
	 *            Relación de de campos de ordenación de consulta la entidad
	 *            Profile de BBDD.
	 * @throws UsersDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkProfileGroupByClause(List<EntityOrderByClause> orderByColumns) throws UsersDAOException {
		String excMsg;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (EntityOrderByClause eobc: orderByColumns) {
				if (!(eobc instanceof ProfileOrderByClause)) {

					excMsg = "[WEBSCAN-USERS-MODEL] Error recuperando los perfiles de usuario registrados en el sistema, " + "clausula ORDER BY incorrecta (tipo esperado " + ProfileOrderByClause.class + ", tipo informado " + orderByColumns.get(0).getClass() + ").";
					LOG.error(excMsg);
					throw new UsersDAOException(excMsg);
				}
			}
		}
	}

	/**
	 * Comprueba que una lista de perfiles de usuario sea válida.
	 * 
	 * @param profiles
	 *            Lista de perfiles de usuario.
	 * @param userId
	 *            Identificador de usuario.
	 * @throws UsersDAOException
	 *             Si la lista de perfiles es una o vacía, o algún perfil no es
	 *             válido.
	 */
	static void checkUserProfiles(List<ProfileDTO> profiles, Long userId) throws UsersDAOException {
		ProfileDTO profile;

		try {
			checkCollection(profiles);
		} catch (WebscanException e) {
			String excMsg = "[WEBSCAN-USERS-MODEL] Error al actualizar los perfiles del usuario " + userId + ". " + e.getMessage();
			throw new UsersDAOException(e.getCode(), excMsg, e);
		}

		for (Iterator<ProfileDTO> it = profiles.iterator(); it.hasNext();) {
			profile = it.next();

			checkProfile(profile, userId);
		}
	}

	/**
	 * Comprueba que un perfil de usuario no sea nulo, posea identificador y
	 * denominación.
	 * 
	 * @param profile
	 *            Perfil de usuario.
	 * @param userId
	 *            Identificador de usuario.
	 * @throws UsersDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkProfile(ProfileDTO profile, Long userId) throws UsersDAOException {
		String excMsg;

		excMsg = "[WEBSCAN-USERS-MODEL] Error al actualizar los perfiles del usuario " + userId + ".";

		try {
			checkEmptyObject(profile, excMsg + "Perfil de usuario nulo o vacío.");
			checkEmptyObject(profile.getId(), excMsg + "Identificador de perfil nulo o vacío.");
			checkEmptyObject(profile.getName(), excMsg + "Denominación de perfil nulo o vacío.");
		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e.getCause());
		}

	}

	/**
	 * Comprueba que una petición de recuperación de password no sea nulo y
	 * posea fecha de envío.
	 * 
	 * @param recUserPassReq
	 *            petición de recuperación de password
	 * @throws UsersDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkRecoveryUserPasswordRequest(RecoveryUserPasswordRequestDTO recUserPassReq) throws UsersDAOException {
		Boolean isNew;
		String excMsg;

		isNew = recUserPassReq != null && recUserPassReq.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al crear una nueva petición de recuperación de password.";
		} else {
			excMsg = "[WEBSCAN-USERS-MODEL] Error al actualizar la petición de recuperación de password " + (recUserPassReq != null ? recUserPassReq.getId() : "No informada") + ".";
		}

		try {
			checkEmptyObject(recUserPassReq, excMsg + "Objeto nulo o vacío.");
			checkEmptyObject(recUserPassReq.getComSendingDate(), excMsg + "Fecha de envío de la comunicación nula o vacía.");

		} catch (WebscanException e) {
			throw new UsersDAOException(e.getCode(), e.getMessage(), e.getCause());
		}

	}

}
