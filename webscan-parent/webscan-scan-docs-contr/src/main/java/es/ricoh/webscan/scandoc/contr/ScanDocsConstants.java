/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.ScanDocsConstants.java.</p>
 * <b>Descripción:</b><p>Constantes del plugin de captura de imágenes.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.scandoc.contr;

/**
 * Constantes del plugin de captura de imágenes.
 * <p>
 * Class ScanDocsConstants.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public interface ScanDocsConstants {

	/**
	 * respuesta de parametros incorrecto en scanDocs.
	 */
	String SCANDOCS_ERROR_DEFAULT = "default.error.message";

	/**
	 * respuesta de parametros text incorrecto en scanDocs.
	 */
	String SCANDOCS_ERROR_PARAM_TEXT = "scandoc.error.parameters";

	/**
	 * respuesta de plugin no configurado .
	 */
	String SCANDOCS_ERROR_PLUGIN_NOT_CONF = "scandoc.error.pluginNotConfig";

	/**
	 * respuesta de plugin configurado pero no activo.
	 */
	String SCANDOCS_ERROR_PLUGIN_NOT_ACTIVE = "scandoc.error.pluginNotActive";

	/**
	 * respuesta de database error en scanDocs.
	 */
	String SCANDOCS_ERROR_DATA_BASE = "scandoc.error.database";

	/**
	 * Representación en formato cadena del valor booleano true.
	 */
	String TRUE_BOOL_STR_VAL = Boolean.TRUE.toString();

	// Variables de sesion
	/**
	 * Parámetro que representa el nombre del atributo de sesión que almacenará
	 * la información del usuario que ha iniciado sesión en el sistema.
	 */
	String USER_SESSION_ATT_NAME = "user";

	// Parámetros plugin de captura de imágenes
	/**
	 * Parámetro que representa el nombre del parámetro del plugin de captura
	 * que establece el punto inicial del área de búsqueda de separadores en la
	 * coordenada X.
	 */
	String SPACER_SEARCH_AREA_X0 = "scan.spacer.search.area.x.init";

	/**
	 * Parámetro que representa el nombre del parámetro del plugin de captura
	 * que establece el punto inicial del área de búsqueda de separadores en la
	 * coordenada Y.
	 */
	String SPACER_SEARCH_AREA_Y0 = "scan.spacer.search.area.y.init";

	/**
	 * Parámetro que representa el nombre del parámetro del plugin de captura
	 * que establece el tamaño del área de búsqueda de separadores en la
	 * coordenada X.
	 */
	String SPACER_SEARCH_AREA_X_SIZE = "scan.spacer.search.area.x.size";

	/**
	 * Parámetro que representa el nombre del parámetro del plugin de captura
	 * que establece el tamaño del área de búsqueda de separadores en la
	 * coordenada Y.
	 */
	String SPACER_SEARCH_AREA_Y_SIZE = "scan.spacer.search.area.y.size";
}
