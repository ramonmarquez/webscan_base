/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.controller.vo.performScanDocVO.java.</p>
 * <b>Descripción:</b><p> Clase que recoge la información de captura de la pantalla escaneo.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.scandoc.contr.controller.vo;

import java.util.Date;
import java.util.Locale;

/**
 * Clase que recoge la información de captura de la pantalla escaneo.
 * <p>
 * Clase PerformScanDocVO
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class PerformScanDocVO {

	/**
	 * Identificador de la petición a efectos de ofrecer tiempos de ejecución de
	 * las operaciones realizadas.
	 */
	private String requestId;

	/**
	 * Identificador de la unidad organizativa.
	 */
	private Long orgUnitId;

	/**
	 * Identificador de plantilla de digitalización.
	 */
	private Long scanTemplateId;

	/**
	 * Identificador de perfil de digitalización, identificado mediante la
	 * plantilla y la unidad organizativa.
	 */
	private Long scanProfileOrgUnitId;

	/**
	 * Identificador de la reserva, puede ser nulo, si no existe ninguna reserva
	 * activa.
	 */
	private Long scanBookId;

	/**
	 * Identificador del tipo de documento, es númerico y debe existir el tipo
	 * de documento y estar activo.
	 */
	private Long idTypeDoc;

	/**
	 * Identificador de la unidad orgánica a la que estarán asociados los
	 * documentos digitalizados.
	 */
	private String userDptoId;

	/**
	 * Identificador del tipo de documento permitido por el sistema ,PDF, PDF
	 * Multi, JPG, etc....
	 */
	private String idOutputFormat;

	/**
	 * Identificador del valor de resolucion seleccionada 100, 150, etc.
	 */
	private Long idOutputResolution;

	/**
	 * Identificador del tipo de pixel selecciondo BW, GRAY, RGB.
	 */

	private String idOutputPixelType;

	/**
	 * habilitado el procesamiento de lotes.
	 */
	private Boolean idDocBatchEnable;

	/**
	 * Identificador de tipo de código de barras para carátulas a efectos de
	 * validación.
	 */
	private String idDocBatchSpacerType;

	/**
	 * Identificador de separador de documentos.
	 */
	private String idDocSpacerType;
	/**
	 * habilita la validación de números de documentos.
	 */
	private Boolean enableNumberDoc;
	/**
	 * identifiador que indica el valor de número de documentos.
	 */
	private Long idDocBatchValNumDocs;
	/**
	 * habilita la validación del número de páginas de cada lote.
	 */
	private Boolean enableNumberPagDoc;
	/**
	 * identificador que indica el valor de número de páginas de cada lote.
	 */
	private Long idDocValNumPagsDoc;

	/**
	 * fecha de aceptacion de la reserva en la pantalla de captura.
	 */
	private Date dateBookingOK;

	/**
	 * Indica si será deshabilitada la detección de carátulas y separadores.
	 */
	private Boolean spacerSearchDisabled;

	/**
	 * campo que indica que ha habido un error en los datos recibidos.
	 */
	private Boolean error;

	/**
	 * Campo que indica el error producido, será una constante con el valor del
	 * campo.
	 */
	private String fieldError;

	/**
	 * Instante en el que es iniciado el procesamiento de la imagen capturada.
	 */
	private Date startDate = new Date();

	/**
	 * Información regional e idioma de la petición.
	 */
	private Locale locale = Locale.getDefault();

	/**
	 * Constructor method for the class PerformScanDocVO.java.
	 */
	public PerformScanDocVO() {
		super();
	}

	/**
	 * Gets the value of the attribute {@link #requestId}.
	 * 
	 * @return the value of the attribute {@link #requestId}.
	 */
	public String getRequestId() {
		return requestId;
	}

	/**
	 * Sets the value of the attribute {@link #requestId}.
	 * 
	 * @param aRequestId
	 *            The value for the attribute {@link #requestId}.
	 */
	public void setRequestId(String aRequestId) {
		this.requestId = aRequestId;
	}

	/**
	 * Gets the value of the attribute {@link #orgUnitId}.
	 * 
	 * @return the value of the attribute {@link #orgUnitId}.
	 */
	public Long getOrgUnitId() {
		return orgUnitId;
	}

	/**
	 * Sets the value of the attribute {@link #orgUnitId}.
	 * 
	 * @param aorgUnitId
	 *            The value for the attribute {@link #orgUnitId}.
	 */
	public void setOrgUnitId(Long aorgUnitId) {
		this.orgUnitId = aorgUnitId;
	}

	/**
	 * Gets the value of the attribute {@link #scanTemplateId}.
	 * 
	 * @return the value of the attribute {@link #scanTemplateId}.
	 */
	public Long getScanTemplateId() {
		return scanTemplateId;
	}

	/**
	 * Sets the value of the attribute {@link #scanTemplateId}.
	 * 
	 * @param aScanTemplateId
	 *            The value for the attribute {@link #scanTemplateId}.
	 */
	public void setScanTemplateId(Long aScanTemplateId) {
		this.scanTemplateId = aScanTemplateId;
	}

	/**
	 * Gets the value of the attribute {@link #scanProfileOrgUnitId}.
	 * 
	 * @return the value of the attribute {@link #scanProfileOrgUnitId}.
	 */
	public Long getScanProfileOrgUnitId() {
		return scanProfileOrgUnitId;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfileOrgUnitId}.
	 * 
	 * @param aScanProfileOrgUnitId
	 *            The value for the attribute {@link #scanProfileOrgUnitId}.
	 */
	public void setScanProfileOrgUnitId(Long aScanProfileOrgUnitId) {
		this.scanProfileOrgUnitId = aScanProfileOrgUnitId;
	}

	/**
	 * Gets the value of the attribute {@link #scanBookId}.
	 * 
	 * @return the value of the attribute {@link #scanBookId}.
	 */
	public Long getScanBookId() {
		return scanBookId;
	}

	/**
	 * Sets the value of the attribute {@link #scanBookId}.
	 * 
	 * @param ascanBookId
	 *            The value for the attribute {@link #scanBookId}.
	 */
	public void setScanBookId(Long ascanBookId) {
		this.scanBookId = ascanBookId;
	}

	/**
	 * Gets the value of the attribute {@link #idTypeDoc}.
	 * 
	 * @return the value of the attribute {@link #idTypeDoc}.
	 */
	public Long getIdTypeDoc() {
		return idTypeDoc;
	}

	/**
	 * Sets the value of the attribute {@link #idTypeDoc}.
	 * 
	 * @param aidTypeDoc
	 *            The value for the attribute {@link #idTypeDoc}.
	 */
	public void setIdTypeDoc(Long aidTypeDoc) {
		this.idTypeDoc = aidTypeDoc;
	}

	/**
	 * Gets the value of the attribute {@link #userDptoId}.
	 * 
	 * @return the value of the attribute {@link #userDptoId}.
	 */
	public String getUserDptoId() {
		return userDptoId;
	}

	/**
	 * Sets the value of the attribute {@link #userDptoId}.
	 * 
	 * @param aUserDptoId
	 *            The value for the attribute {@link #userDptoId}.
	 */
	public void setUserDptoId(String aUserDptoId) {
		this.userDptoId = aUserDptoId;
	}

	/**
	 * Gets the value of the attribute {@link #idOutputFormat}.
	 * 
	 * @return the value of the attribute {@link #idOutputFormat}.
	 */
	public String getIdOutputFormat() {
		return idOutputFormat;
	}

	/**
	 * Sets the value of the attribute {@link #idOutputFormat}.
	 * 
	 * @param aidOutputFormat
	 *            The value for the attribute {@link #idOutputFormat}.
	 */
	public void setIdOutputFormat(String aidOutputFormat) {
		this.idOutputFormat = aidOutputFormat;
	}

	/**
	 * Gets the value of the attribute {@link #idOutputResolution}.
	 * 
	 * @return the value of the attribute {@link #idOutputResolution}.
	 */
	public Long getIdOutputResolution() {
		return idOutputResolution;
	}

	/**
	 * Sets the value of the attribute {@link #idOutputResolution}.
	 * 
	 * @param aidOutputResolution
	 *            The value for the attribute {@link #idOutputResolution}.
	 */
	public void setIdOutputResolution(Long aidOutputResolution) {
		this.idOutputResolution = aidOutputResolution;
	}

	/**
	 * Gets the value of the attribute {@link #idOutputPixelType}.
	 * 
	 * @return the value of the attribute {@link #idOutputPixelType}.
	 */
	public String getIdOutputPixelType() {
		return idOutputPixelType;
	}

	/**
	 * Sets the value of the attribute {@link #idOutputPixelType}.
	 * 
	 * @param aidOutputPixelType
	 *            The value for the attribute {@link #idOutputPixelType}.
	 */
	public void setIdOutputPixelType(String aidOutputPixelType) {
		this.idOutputPixelType = aidOutputPixelType;
	}

	/**
	 * Gets the value of the attribute {@link #idDocBatchEnable}.
	 * 
	 * @return the value of the attribute {@link #idDocBatchEnable}.
	 */
	public Boolean getIdDocBatchEnable() {
		return idDocBatchEnable;
	}

	/**
	 * Sets the value of the attribute {@link #idDocBatchEnable}.
	 * 
	 * @param aidDocBatchEnable
	 *            The value for the attribute {@link #idDocBatchEnable}.
	 */
	public void setIdDocBatchEnable(Boolean aidDocBatchEnable) {
		this.idDocBatchEnable = aidDocBatchEnable;
	}

	/**
	 * Gets the value of the attribute {@link #idDocBatchSpacerType}.
	 * 
	 * @return the value of the attribute {@link #idDocBatchSpacerType}.
	 */
	public String getIdDocBatchSpacerType() {
		return idDocBatchSpacerType;
	}

	/**
	 * Sets the value of the attribute {@link #idDocBatchSpacerType}.
	 * 
	 * @param aidDocBatchSpacerType
	 *            The value for the attribute {@link #idDocBatchSpacerType}.
	 */
	public void setIdDocBatchSpacerType(String aidDocBatchSpacerType) {
		this.idDocBatchSpacerType = aidDocBatchSpacerType;
	}

	/**
	 * Gets the value of the attribute {@link #idDocSpacerType}.
	 * 
	 * @return the value of the attribute {@link #idDocSpacerType}.
	 */
	public String getIdDocSpacerType() {
		return idDocSpacerType;
	}

	/**
	 * Sets the value of the attribute {@link #idDocSpacerType}.
	 * 
	 * @param aidDocSpacerType
	 *            The value for the attribute {@link #idDocSpacerType}.
	 */
	public void setIdDocSpacerType(String aidDocSpacerType) {
		this.idDocSpacerType = aidDocSpacerType;
	}

	/**
	 * Gets the value of the attribute {@link #enableNumberDoc}.
	 * 
	 * @return the value of the attribute {@link #enableNumberDoc}.
	 */
	public Boolean getEnableNumberDoc() {
		return enableNumberDoc;
	}

	/**
	 * Sets the value of the attribute {@link #enableNumberDoc}.
	 * 
	 * @param aenableNumberDoc
	 *            The value for the attribute {@link #enableNumberDoc}.
	 */
	public void setEnableNumberDoc(Boolean aenableNumberDoc) {
		this.enableNumberDoc = aenableNumberDoc;
	}

	/**
	 * Gets the value of the attribute {@link #idDocBatchValNumDocs}.
	 * 
	 * @return the value of the attribute {@link #idDocBatchValNumDocs}.
	 */
	public Long getIdDocBatchValNumDocs() {
		return idDocBatchValNumDocs;
	}

	/**
	 * Sets the value of the attribute {@link #idDocBatchValNumDocs}.
	 * 
	 * @param aidDocBatchValNumDocs
	 *            The value for the attribute {@link #idDocBatchValNumDocs}.
	 */
	public void setIdDocBatchValNumDocs(Long aidDocBatchValNumDocs) {
		this.idDocBatchValNumDocs = aidDocBatchValNumDocs;
	}

	/**
	 * Gets the value of the attribute {@link #enableNumberPagDoc}.
	 * 
	 * @return the value of the attribute {@link #enableNumberPagDoc}.
	 */
	public Boolean getEnableNumberPagDoc() {
		return enableNumberPagDoc;
	}

	/**
	 * Sets the value of the attribute {@link #enableNumberPagDoc}.
	 * 
	 * @param aenableNumberPagDoc
	 *            The value for the attribute {@link #enableNumberPagDoc}.
	 */
	public void setEnableNumberPagDoc(Boolean aenableNumberPagDoc) {
		this.enableNumberPagDoc = aenableNumberPagDoc;
	}

	/**
	 * Gets the value of the attribute {@link #idDocValNumPagsDoc}.
	 * 
	 * @return the value of the attribute {@link #idDocValNumPagsDoc}.
	 */
	public Long getIdDocValNumPagsDoc() {
		return idDocValNumPagsDoc;
	}

	/**
	 * Sets the value of the attribute {@link #idDocValNumPagsDoc}.
	 * 
	 * @param aidDocValNumPagsDoc
	 *            The value for the attribute {@link #idDocValNumPagsDoc}.
	 */
	public void setIdDocValNumPagsDoc(Long aidDocValNumPagsDoc) {
		this.idDocValNumPagsDoc = aidDocValNumPagsDoc;
	}

	/**
	 * Gets the value of the attribute {@link #spacerSearchDisabled}.
	 * 
	 * @return the value of the attribute {@link #spacerSearchDisabled}.
	 */
	public Boolean getSpacerSearchDisabled() {
		return spacerSearchDisabled;
	}

	/**
	 * Sets the value of the attribute {@link #spacerSearchDisabled}.
	 * 
	 * @param aSpacerSearchDisabled
	 *            The value for the attribute {@link #spacerSearchDisabled}.
	 */
	public void setSpacerSearchDisabled(Boolean aSpacerSearchDisabled) {
		this.spacerSearchDisabled = aSpacerSearchDisabled;
	}

	/**
	 * Gets the value of the attribute {@link #error}.
	 * 
	 * @return the value of the attribute {@link #error}.
	 */
	public Boolean getError() {
		return error;
	}

	/**
	 * Sets the value of the attribute {@link #error}.
	 * 
	 * @param aerror
	 *            The value for the attribute {@link #error}.
	 */
	public void setError(Boolean aerror) {
		this.error = aerror;
	}

	/**
	 * Gets the value of the attribute {@link #fieldError}.
	 * 
	 * @return the value of the attribute {@link #fieldError}.
	 */
	public String getFieldError() {
		return fieldError;
	}

	/**
	 * Sets the value of the attribute {@link #fieldError}.
	 * 
	 * @param afieldError
	 *            The value for the attribute {@link #fieldError}.
	 */
	public void setFieldError(String afieldError) {
		this.fieldError = afieldError;
	}

	/**
	 * Gets the value of the attribute {@link #dateBookingOK}.
	 * 
	 * @return the value of the attribute {@link #dateBookingOK}.
	 */
	public Date getDateBookingOK() {
		return dateBookingOK;
	}

	/**
	 * Sets the value of the attribute {@link #dateBookingOK}.
	 * 
	 * @param adateBookingOK
	 *            The value for the attribute {@link #dateBookingOK}.
	 */
	public void setDateBookingOK(Date adateBookingOK) {
		this.dateBookingOK = adateBookingOK;
	}

	/**
	 * Gets the value of the attribute {@link #startDate}.
	 * 
	 * @return the value of the attribute {@link #startDate}.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the value of the attribute {@link #startDate}.
	 * 
	 * @param aStartDate
	 *            The value for the attribute {@link #startDate}.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}

	/**
	 * Gets the value of the attribute {@link #locale}.
	 * 
	 * @return the value of the attribute {@link #locale}.
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Sets the value of the attribute {@link #locale}.
	 * 
	 * @param alocale
	 *            The value for the attribute {@link #locale}.
	 */
	public void setLocale(Locale alocale) {
		this.locale = alocale;
	}

}
