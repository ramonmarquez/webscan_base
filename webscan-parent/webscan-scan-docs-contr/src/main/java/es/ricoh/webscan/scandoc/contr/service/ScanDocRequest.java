/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.service.ScanDocRequest.java.</p>
 * <b>Descripción:</b><p> .</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.scandoc.contr.service;

import java.util.ArrayList;
import java.util.List;

import es.ricoh.webscan.scandoc.contr.util.BarCodeResult;
import es.ricoh.webscan.scandoc.contr.util.ScannedImageType;

/**
 * Petición de digitalización de documentos.
 * <p>
 * Class ScanDocRequest.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ScanDocRequest {

	/**
	 * Relación de imágenes que componen el documento. Cada posición se
	 * corresponderá con un índice que identifica una de las imágenes
	 * capturadas.
	 */
	private List<Integer> listImage = new ArrayList<Integer>();

	/**
	 * tipo de la imagen escaneada.
	 */
	private ScannedImageType typeImage = ScannedImageType.DOC_CONTENT;

	/**
	 * Resultado de la búsqueda de separadores de documentos y lotes de
	 * documentos.
	 */
	private BarCodeResult barCodeResult;

	/**
	 * Constructor sin argumentos.
	 */
	public ScanDocRequest() {
		super();
	}

	/**
	 * Gets the value of the attribute {@link #typeImage}.
	 * 
	 * @return the value of the attribute {@link #typeImage}.
	 */
	public ScannedImageType getTypeImage() {
		return typeImage;
	}

	/**
	 * Sets the value of the attribute {@link #typeImage}.
	 * 
	 * @param aTypeImage
	 *            The value for the attribute {@link #typeImage}.
	 */
	public void setTypeImage(ScannedImageType aTypeImage) {
		this.typeImage = aTypeImage;
	}

	/**
	 * Gets the value of the attribute {@link #listImage}.
	 * 
	 * @return the value of the attribute {@link #listImage}.
	 */
	public List<Integer> getListImage() {
		return listImage;
	}

	/**
	 * Sets the value of the attribute {@link #listImage}.
	 * 
	 * @param alistImage
	 *            The value for the attribute {@link #listImage}.
	 */
	public void setListImage(List<Integer> alistImage) {
		this.listImage = alistImage;
	}

	/**
	 * Gets the value of the attribute {@link #barCodeResult}.
	 * 
	 * @return the value of the attribute {@link #barCodeResult}.
	 */
	public BarCodeResult getBarCodeResult() {
		return barCodeResult;
	}

	/**
	 * Sets the value of the attribute {@link #barCodeResult}.
	 * 
	 * @param aBarCodeResult
	 *            The value for the attribute {@link #barCodeResult}.
	 */
	public void setBarCodeResult(BarCodeResult aBarCodeResult) {
		this.barCodeResult = aBarCodeResult;
	}

}
