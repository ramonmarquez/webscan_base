/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.ScanDocsException.java.</p>
 * <b>Descripción:</b><p> Excepción que encapsula los errores de de la captura de documentos y su
 * tratamiento.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.scandoc.contr;

/**
 * Excepción que encapsula los errores de de la captura de documentos y su
 * tratamiento.
 * <p>
 * Class ScanDocsException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ScanDocsException extends Exception {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Error escaneado vacio o nulo.
	 */
	public static final String CODE_001 = "COD_001";

	/**
	 * Formato no permitido.
	 */
	public static final String CODE_002 = "COD_002";

	/**
	 * Separador no válido.
	 */
	public static final String CODE_003 = "COD_003";

	/**
	 * Reserva no valida o inexistente.
	 */
	public static final String CODE_004 = "COD_004";

	/**
	 * Error al recuperar la información asociada a los metadatos del lote.
	 */
	public static final String CODE_005 = "COD_005";

	/**
	 * Errores encontrados al validar un lote de documentos.
	 */
	public static final String CODE_006 = "COD_006";

	/**
	 * No encontrado documentos al parsear la información.
	 */
	public static final String CODE_007 = "COD_007";

	/**
	 * Identificador de carátula no encontrado.
	 */
	public static final String CODE_008 = "COD_008";

	/**
	 * Identificador de documento reutilizable no encontrado.
	 */
	public static final String CODE_009 = "COD_009";

	/**
	 * Documento digitalizado no encontrado en reserva o carátula.
	 */
	public static final String CODE_010 = "COD_010";

	/**
	 * Número de documentos digitalizados diferente a los reservados.
	 */
	public static final String CODE_011 = "COD_011";

	/**
	 * Error persitiendo los documentos y metadatos en el modelo de datos.
	 */
	public static final String CODE_012 = "COD_012";

	/**
	 * Error finalizando carátula.
	 */
	public static final String CODE_013 = "COD_013";

	/**
	 * Error finalizando reserva de trabajo de digitalización.
	 */
	public static final String CODE_014 = "COD_014";

	/**
	 * Error de digitalización, procesamiento de lotes de documentos no
	 * admitido.
	 */
	public static final String CODE_015 = "COD_015";

	/**
	 * Error de digitalización, carátula no encontrada.
	 */
	public static final String CODE_016 = "COD_016";

	/**
	 * Error al validar numero de páginas de un documento de un lote.
	 */
	public static final String CODE_017 = "COD_017";

	/**
	 * Error al validar el tipo de código de barras de una carátula de lote de
	 * documentos.
	 */
	public static final String CODE_018 = "COD_018";

	/**
	 * Error al validar el número de documentos de un lote de documentos.
	 */
	public static final String CODE_019 = "COD_019";

	/**
	 * Error recuperando información de perfil de digitalziación asociada a una
	 * reserva o carátula.
	 */
	public static final String CODE_020 = "COD_020";

	/**
	 * Error procesando múltiples lotes de documentos, carátula repetida.
	 */
	public static final String CODE_021 = "COD_021";

	/**
	 * Error obteniendo el área de búsqueda de carátulas y separadores
	 * reutilizables de documentos.
	 */
	public static final String CODE_022 = "COD_022";

	/**
	 * Error transformado las imágenes capturadas para un documento al formato
	 * de archivo solicitado.
	 */
	public static final String CODE_023 = "COD_023";

	/**
	 * Error obteniendo la configuración del plugin de captura.
	 */
	public static final String CODE_024 = "COD_024";

	/**
	 * Un documento del lote ha superado el tamaño máximo configurado.
	 */
	public static final String CODE_025 = "COD_025";

	/**
	 * El usuario no pertenece o presta servicio para la unidad orgánica
	 * establecida en el trabajo de digtalización.
	 */
	public static final String CODE_026 = "COD_026";

	/**
	 * Error al obteniendo listado de definiciones de documentos activas para un
	 * perfil de digitalización dado.
	 */
	public static final String CODE_027 = "COD_027";

	/**
	 * Causa del error desconocida.
	 */
	public static final String CODE_999 = "COD_999";

	/**
	 * Código del error.
	 */
	protected String code = CODE_999;

	/**
	 * Constructor method for the class ScanDocsException.java.
	 */
	public ScanDocsException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public ScanDocsException(final String aCode, final String aMessage) {
		super(aMessage);
		this.code = aCode;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public ScanDocsException(final String aMessage) {
		super(aMessage);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public ScanDocsException(final Throwable aCause) {
		super(aCause);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public ScanDocsException(final String aMessage, final Throwable aCause) {
		super(aMessage, aCause);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public ScanDocsException(final String aCode, final String aMessage, final Throwable aCause) {
		super(aMessage, aCause);
		this.code = aCode;
	}

	/**
	 * Obtiene el código de error de la excepción.
	 *
	 * @return código de error de la excepción.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Establece el código de error de la excepción.
	 *
	 * @param aCode
	 *            código de error de la excepción.
	 */
	public void setCode(final String aCode) {
		this.code = aCode;
	}

}
