/** 
 * <b>File:</b><p>es.ricoh.webscan.scandoc.contr.service.BatchScanDoc.java.</p>
 * <b>Description:</b><p> Agrupa la información extraída tras procesar un conjunto de imágenes
 * capturadas, representándolas como un lote de documentos digitalizados.</p>
 * <b>Project:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.scandoc.contr.service;

import java.util.Date;

import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.model.domain.DocSpacerType;

/**
 * Agrupa la información extraída tras procesar un conjunto de imágenes
 * capturadas, representándolas como un lote de documentos digitalizados.
 * <p>
 * Class BatchScanDoc.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.1.0.
 */
public class BatchScanDoc {

	/**
	 * Identificador de carátula.
	 */
	private String spacerId;

	/**
	 * Unidad orgánica a la que serán asociados los documentos del lote.
	 */
	private String department;

	/**
	 * Identificador de reserva de trabajo de digitalización.
	 */
	private Long scanWorkBookingId;

	/**
	 * Perfil de digitalizacion.
	 */
	private Long scanProfileId;

	/**
	 * Definición de documentos.
	 */
	private Long docSpecId;

	/**
	 * Perfil de digitalizacion.
	 */
	private Long scanProfileIdVal;

	/**
	 * Definición de documentos configurada para validación.
	 */
	private Long docSpecIdVal;

	/**
	 * tipo de lote de documentos.
	 */
	private BatchType batchType;

	/**
	 * tipo de separador de lotes de documentos.
	 */
	private BatchSpacerType batchSpacerType;

	/**
	 * Tipo de separador de documentos configurado para validación.
	 */
	private DocSpacerType docSpacerTypeVal;

	/**
	 * para validacion: numero documentos lote.
	 */

	private Long numDocBatch;

	/**
	 * para validacion: numero de paginas por documento.
	 */
	private Long numPageDoc;

	/**
	 * Tipo de separador de lotes de documentos configurado para validación.
	 */
	private BatchSpacerType batchSpacerTypeVal;

	/**
	 * fecha de aceptacion de reserva.
	 */
	private Date dateBookingOK;

	/**
	 * Constructor method for the class BatchScanDoc.java.
	 */
	public BatchScanDoc() {
	}

	/**
	 * Gets the value of the attribute {@link #spacerId}.
	 * 
	 * @return the value of the attribute {@link #spacerId}.
	 */
	public String getSpacerId() {
		return spacerId;
	}

	/**
	 * Sets the value of the attribute {@link #spacerId}.
	 * 
	 * @param aSpacerId
	 *            The value for the attribute {@link #spacerId}.
	 */
	public void setSpacerId(String aSpacerId) {
		this.spacerId = aSpacerId;
	}

	/**
	 * Gets the value of the attribute {@link #department}.
	 * 
	 * @return the value of the attribute {@link #department}.
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Sets the value of the attribute {@link #department}.
	 * 
	 * @param department
	 *            The value for the attribute {@link #department}.
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * Gets the value of the attribute {@link #scanWorkBookingId}.
	 * 
	 * @return the value of the attribute {@link #scanWorkBookingId}.
	 */
	public Long getScanWorkBookingId() {
		return scanWorkBookingId;
	}

	/**
	 * Sets the value of the attribute {@link #scanWorkBookingId}.
	 * 
	 * @param aScanWorkBookingId
	 *            The value for the attribute {@link #scanWorkBookingId}.
	 */
	public void setScanWorkBookingId(Long aScanWorkBookingId) {
		this.scanWorkBookingId = aScanWorkBookingId;
	}

	/**
	 * Gets the value of the attribute {@link #scanProfileId}.
	 * 
	 * @return the value of the attribute {@link #scanProfileId}.
	 */
	public Long getScanProfileId() {
		return scanProfileId;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfileId}.
	 * 
	 * @param ascanProfileId
	 *            The value for the attribute {@link #scanProfileId}.
	 */
	public void setScanProfileId(Long ascanProfileId) {
		this.scanProfileId = ascanProfileId;
	}

	/**
	 * Gets the value of the attribute {@link #docSpecId}.
	 * 
	 * @return the value of the attribute {@link #docSpecId}.
	 */
	public Long getDocSpecId() {
		return docSpecId;
	}

	/**
	 * Sets the value of the attribute {@link #docSpecId}.
	 * 
	 * @param aDocSpecId
	 *            The value for the attribute {@link #docSpecId}.
	 */
	public void setDocSpecId(Long aDocSpecId) {
		this.docSpecId = aDocSpecId;
	}

	/**
	 * Gets the value of the attribute {@link #scanProfileIdVal}.
	 * 
	 * @return the value of the attribute {@link #scanProfileIdVal}.
	 */
	public Long getScanProfileIdVal() {
		return scanProfileIdVal;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfileIdVal}.
	 * 
	 * @param aScanProfileIdVal
	 *            The value for the attribute {@link #scanProfileIdVal}.
	 */
	public void setScanProfileIdVal(Long aScanProfileIdVal) {
		this.scanProfileIdVal = aScanProfileIdVal;
	}

	/**
	 * Gets the value of the attribute {@link #docSpecIdVal}.
	 * 
	 * @return the value of the attribute {@link #docSpecIdVal}.
	 */
	public Long getDocSpecIdVal() {
		return docSpecIdVal;
	}

	/**
	 * Sets the value of the attribute {@link #docSpecIdVal}.
	 * 
	 * @param aDocSpecIdVal
	 *            The value for the attribute {@link #docSpecIdVal}.
	 */
	public void setDocSpecIdVal(Long aDocSpecIdVal) {
		this.docSpecIdVal = aDocSpecIdVal;
	}

	/**
	 * Gets the value of the attribute {@link #batchType}.
	 * 
	 * @return the value of the attribute {@link #batchType}.
	 */
	public BatchType getBatchType() {
		return batchType;
	}

	/**
	 * Sets the value of the attribute {@link #batchType}.
	 * 
	 * @param aBatchType
	 *            The value for the attribute {@link #batchType}.
	 */
	public void setBatchType(BatchType aBatchType) {
		this.batchType = aBatchType;
	}

	/**
	 * Gets the value of the attribute {@link #batchSpacerType}.
	 * 
	 * @return the value of the attribute {@link #batchSpacerType}.
	 */
	public BatchSpacerType getBatchSpacerType() {
		return batchSpacerType;
	}

	/**
	 * Sets the value of the attribute {@link #batchSpacerType}.
	 * 
	 * @param abatchSpacerType
	 *            The value for the attribute {@link #batchSpacerType}.
	 */
	public void setBatchSpacerType(BatchSpacerType abatchSpacerType) {
		this.batchSpacerType = abatchSpacerType;
	}

	/**
	 * Gets the value of the attribute {@link #docSpacerTypeVal}.
	 * 
	 * @return the value of the attribute {@link #docSpacerTypeVal}.
	 */
	public DocSpacerType getDocSpacerTypeVal() {
		return docSpacerTypeVal;
	}

	/**
	 * Sets the value of the attribute {@link #docSpacerTypeVal}.
	 * 
	 * @param aDocSpacerTypeVal
	 *            The value for the attribute {@link #docSpacerTypeVal}.
	 */
	public void setDocSpacerTypeVal(DocSpacerType aDocSpacerTypeVal) {
		this.docSpacerTypeVal = aDocSpacerTypeVal;
	}

	/**
	 * Gets the value of the attribute {@link #numDocBatch}.
	 * 
	 * @return the value of the attribute {@link #numDocBatch}.
	 */
	public Long getNumDocBatch() {
		return numDocBatch;
	}

	/**
	 * Sets the value of the attribute {@link #numDocBatch}.
	 * 
	 * @param anumDocBatch
	 *            The value for the attribute {@link #numDocBatch}.
	 */
	public void setNumDocBatch(Long anumDocBatch) {
		this.numDocBatch = anumDocBatch;
	}

	/**
	 * Gets the value of the attribute {@link #numPageDoc}.
	 * 
	 * @return the value of the attribute {@link #numPageDoc}.
	 */
	public Long getNumPageDoc() {
		return numPageDoc;
	}

	/**
	 * Sets the value of the attribute {@link #numPageDoc}.
	 * 
	 * @param anumPageDoc
	 *            The value for the attribute {@link #numPageDoc}.
	 */
	public void setNumPageDoc(Long anumPageDoc) {
		this.numPageDoc = anumPageDoc;
	}

	/**
	 * Gets the value of the attribute {@link #dateBookingOK}.
	 * 
	 * @return the value of the attribute {@link #dateBookingOK}.
	 */
	public Date getDateBookingOK() {
		return dateBookingOK;
	}

	/**
	 * Sets the value of the attribute {@link #dateBookingOK}.
	 * 
	 * @param adateBookingOK
	 *            The value for the attribute {@link #dateBookingOK}.
	 */
	public void setDateBookingOK(Date adateBookingOK) {
		this.dateBookingOK = adateBookingOK;
	}

	/**
	 * Gets the value of the attribute {@link #batchSpacerTypeVal}.
	 * 
	 * @return the value of the attribute {@link #batchSpacerTypeVal}.
	 */
	public BatchSpacerType getBatchSpacerTypeVal() {
		return batchSpacerTypeVal;
	}

	/**
	 * Sets the value of the attribute {@link #batchSpacerTypeVal}.
	 * 
	 * @param abatchSpacerTypeVal
	 *            The value for the attribute {@link #batchSpacerTypeVal}.
	 */
	public void setBatchSpacerTypeVal(BatchSpacerType abatchSpacerTypeVal) {
		this.batchSpacerTypeVal = abatchSpacerTypeVal;
	}

}
