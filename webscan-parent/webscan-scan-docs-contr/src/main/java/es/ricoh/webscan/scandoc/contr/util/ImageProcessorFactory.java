/** 
* <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.util.ImageProcessorFactory.java.</p>
* <b>Descripción:</b><p> .</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.scandoc.contr.util;

import java.util.Locale;

import org.springframework.context.MessageSource;

import es.ricoh.webscan.scandoc.contr.ScanDocsException;
import es.ricoh.webscan.scandoc.contr.controller.vo.PixelType;
import es.ricoh.webscan.utilities.MimeType;

/**
 * <p>
 * Clase ImageProcessorFactory.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ImageProcessorFactory {

	/**
	 * Retorna la clase reponsable del procesamiento de las imágenes capturadas.
	 * Actualmente, se empleará el procesador de imágenes TIFF para imágenes en
	 * blanco y negro, y el procesador de imágenes JPEG para es resto de colore
	 * (escala de grises y RGB).
	 * 
	 * @param pixelType
	 *            Tipo de píxel o color de las imágenes capturadas.
	 * @return la clase reponsable del procesamiento de las imágenes capturadas.
	 * @throws ScanDocsException
	 *             Si el tipo de píxel no es soportado.
	 */
	public static ImageProcessor getImageProcessor(PixelType pixelType, MessageSource messages, Locale locale) throws ScanDocsException {
		ImageProcessor res;
		MimeType mimeType;

		mimeType = getImagesContentType(pixelType);
		switch (mimeType) {
			case TIFF_IMG_CONTENT:
				res = new TiffImageProcessor();
				res.setMessages(messages);
				res.setLocale(locale);
				res.initReader();
				break;
			case JPG_IMG_CONTENT:
				// Se informan una o más imágenes
				res = new JpegImageProcessor();
				res.setMessages(messages);
				res.setLocale(locale);
				res.initReader();
				break;
			default:
				// TODO - Código de error por formato incorrecto
				throw new ScanDocsException();
		}

		return res;
	}

	/**
	 * Obtiene el tipo MIME de las imágenes capturadas a partir del tipo de
	 * pixel informado en el formulario de captura. Para imágenes en B/N se
	 * retornará image/tiff, mientras que para el resto image/jpeg.
	 * 
	 * @param pixelType
	 *            Tipo de píxel o color de las imágenes.
	 * @return el tipo MIME de las imágenes capturadas a partir del tipo de
	 *         pixel.
	 */
	private static MimeType getImagesContentType(PixelType pixelType) {
		MimeType res = null;

		switch (pixelType) {
			case BW:
				res = MimeType.TIFF_IMG_CONTENT;
				break;
			case GRAY:
			case RGB:
				res = MimeType.JPG_IMG_CONTENT;
				break;
		}

		return res;
	}
}
