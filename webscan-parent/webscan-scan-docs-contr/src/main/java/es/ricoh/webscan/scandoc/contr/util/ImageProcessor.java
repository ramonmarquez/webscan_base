/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.util.ImageProcessor.java.</p>
 * <b>Descripción:</b><p> Utilidad para el procesamiento de las imagenes de documentos capturadas en la fase de digitalización.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH España IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.scandoc.contr.util;

import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.web.multipart.MultipartFile;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.Code128Reader;
import com.google.zxing.pdf417.PDF417Reader;
import com.google.zxing.qrcode.QRCodeReader;

import es.ricoh.webscan.spacers.model.domain.DocSpacerType;
import es.ricoh.webscan.WebscanConstants;
import es.ricoh.webscan.scandoc.contr.ScanDocsConstants;
import es.ricoh.webscan.scandoc.contr.ScanDocsException;
import es.ricoh.webscan.scandoc.contr.controller.vo.PerformScanDocVO;
import es.ricoh.webscan.scandoc.contr.controller.vo.PixelType;
import es.ricoh.webscan.scandoc.contr.service.ScanDocRequest;
import es.ricoh.webscan.scandoc.contr.service.ScannedDoc;
import es.ricoh.webscan.utilities.MimeType;

/**
 * Utilidad para el procesamiento de las imagenes de documentos capturadas en la
 * fase de digitalización.
 * <p>
 * Clase ImageProcessor.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public abstract class ImageProcessor {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ImageProcessor.class);

	/**
	 * Constante que define point (ADOBE) equivalentes a una pulgada.
	 */
	protected static final Float POINT_PER_INCH = 72f;

	/**
	 * Constante de configuración de la desviación de color blanco MAG.
	 */
	private static final double BLANK_STDDEV_MAG = 1f;
	/**
	 * Constante de configuración de la desviación de color blanco CUTOFF.
	 */
	private static final double BLANK_STDDEV_CUTOFF = 5f;
	/**
	 * Constante de configuración de la variacion de color blanco MAG.
	 */
	private static final double BLANK_VAR_MAG = 10000000f;
	/**
	 * Constante de configuración de la variacion de color blanco CUTOFF.
	 */
	private static final double BLANK_VAR_CUTOFF = 5f;

	/**
	 * Versión formato PNG.
	 */
	public static final String VERSION_PNG = "1.0";

	/**
	 * Versión formato JPEG.
	 */
	public static final String VERSION_JPG = "1.0";

	/**
	 * Versión formato TIFF.
	 */
	public static final String VERSION_TIFF = "6.0";

	/**
	 * Tipo de compresión de documentos con formato TIFF y color en blanco y
	 * negro.
	 */
	protected static final String BLACK_WHITE_TIFF_COMP = "CCITT T.6";

	/**
	 * Tipo de compresión de documentos con formato TIFF y color en escala de
	 * grises. LZW ofrece peores resultados, aumentando el tamaño de la imagen
	 * al no aplicar compresión.
	 */
	protected static final String GRAY_SCALE_TIFF_COMP = "JPEG";

	/**
	 * Tipo de compresión de documentos con formato TIFF y color en escala de
	 * grises.
	 */
	protected static final String RGB_TIFF_COMP = "JPEG";

	/**
	 * Formatos de códigos de barras admitidos por el sistema.
	 */
	private static final Vector<BarcodeFormat> FORMATS = new Vector<BarcodeFormat>();
	/**
	 * Hints usados para la detección de tipo de códigos de barras.
	 */
	private static final Hashtable<DecodeHintType, Object> HINTS = new Hashtable<DecodeHintType, Object>();

	/**
	 * Formatos de códigos de barras admitidos por el sistema.
	 */
	protected static final String REQ_IMAGES_PREFIX = "image_";

	/**
	 * Nombre de la etiqueta de multidioma cuyo valor es asignado a la propiedad
	 * que identifica el asunto de los archivos en formato PDF.
	 */
	protected static final String SUBJECT_PDF_PROPERTY_PARAM_NAME = "pdf.generation.properties.scan.subject";

	/**
	 * Objeto empleado para tratamiento de imagenes.
	 */
	private static MultiFormatReader barCodeReader;
	/**
	 * Lector de códigos QR.
	 */
	private static QRCodeReader qrReader;

	/**
	 * Lector de códigos PDF 417.
	 */
	private static PDF417Reader pdf417Reader;

	/**
	 * Lector de códigos Code 128.
	 */
	private static Code128Reader code128Reader;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	protected MessageSource messages;

	/**
	 * Configuración regional.
	 */
	protected Locale locale;

	static {
		barCodeReader = new MultiFormatReader();
		code128Reader = new Code128Reader();
		qrReader = new QRCodeReader();
		pdf417Reader = new PDF417Reader();

		FORMATS.addElement(BarcodeFormat.CODE_128);
		FORMATS.addElement(BarcodeFormat.QR_CODE);
		FORMATS.addElement(BarcodeFormat.PDF_417);
		HINTS.put(DecodeHintType.POSSIBLE_FORMATS, FORMATS);

		HINTS.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);

		ImageIO.scanForPlugins();
	}

	/**
	 * Funcion para leer los separadores de lotes, pero de manera estricta cada
	 * formato de código de barra es implicitamente indicado en el código.
	 * 
	 * @param src
	 *            imagen.
	 * @return En caso de encontrarse un código de barras de los admitidos por
	 *         el Sistema, objeto que encapsula el tipo de código de barras y el
	 *         texto codificado. En caso contrario, se devuelve null.
	 * @throws ScanDocsException
	 *             Exception producida al leer el separador desde el
	 *             BufferdImage.
	 */
	public static BarCodeResult readSpacer(BufferedImage src) throws ScanDocsException {
		BarCodeResult res = null;
		BinaryBitmap binaryBitmap = null;
		Result readResult = null;
		LOG.debug("readBatchSpacer start");

		try {
			BufferedImageLuminanceSource ls = new BufferedImageLuminanceSource(src);
			Binarizer binarizer = new HybridBinarizer(ls);
			binaryBitmap = new BinaryBitmap(binarizer);

			readResult = barCodeReader.decode(binaryBitmap, HINTS);

			res = result2BarCodeResult(readResult);
		} catch (NotFoundException e) {
			// No hacer nada
		} catch (Exception e) {
			throw new ScanDocsException(ScanDocsException.CODE_999, e.getMessage());
		}

		LOG.debug("readBatchSpacer end");
		return res;
	}

	/**
	 * Obtiene de una imagen el texto codificado en un separador de lotes o
	 * documentos.
	 * 
	 * @param src
	 *            imagen.
	 * @param type
	 *            Tipo de separador.
	 * @return En caso de encontrarse un código de barras de los admitidos por
	 *         el Sistema, objeto que encapsula el tipo de código de barras y el
	 *         texto codificado. En caso contrario, se devuelve null.
	 * @throws ScanDocsException
	 *             si se produce algún error en la lectura de la imágen.
	 */
	public static BarCodeResult readDocSpacer(BufferedImage src, DocSpacerType type) throws ScanDocsException {
		BarCodeResult res = null;
		Result readResult = null;

		try {
			LOG.debug("readDocSpacer start");

			if (!DocSpacerType.BLANK_PAGE.equals(type)) {
				BufferedImageLuminanceSource ls = new BufferedImageLuminanceSource(src);
				Binarizer binarizer = new HybridBinarizer(ls);
				BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);

				if (DocSpacerType.CODE_QR.equals(type)) {
					readResult = qrReader.decode(binaryBitmap);
				} else if (DocSpacerType.CODE_128.equals(type)) {
					readResult = code128Reader.decode(binaryBitmap);
				} else if (DocSpacerType.PDF_417.equals(type)) {
					readResult = pdf417Reader.decode(binaryBitmap);
				}

				res = result2BarCodeResult(readResult);
			}
		} catch (NotFoundException e) {
			// No existe código de barra en la página en la página
		} catch (Exception e) {
			throw new ScanDocsException(ScanDocsException.CODE_999, e.getMessage());
		}

		LOG.debug("readDocSpacer end");
		return res;
	}

	/**
	 * Obtiene de una imagen el texto codificado en un separador de lotes o
	 * documentos.
	 * 
	 * @param src
	 *            imagen.
	 * @return En caso de encontrarse un código de barras de los admitidos por
	 *         el Sistema, objeto que encapsula el tipo de código de barras y el
	 *         texto codificado. En caso contrario, se devuelve null.
	 * @throws ScanDocsException
	 *             si se produce algún error en la lectura de la imágen.
	 */

	public static BarCodeResult readAnyBarCodeSpacer(BufferedImage src) throws ScanDocsException {
		BarCodeResult res = null;
		Result readResult;

		LOG.debug("readDocSpacer start");

		try {
			BufferedImageLuminanceSource ls = new BufferedImageLuminanceSource(src);
			Binarizer binarizer = new HybridBinarizer(ls);
			BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);
			readResult = barCodeReader.decode(binaryBitmap);

			res = result2BarCodeResult(readResult);
		} catch (NotFoundException e) {
			// No existe código de barra en la página en la página
		} catch (Exception e) {
			throw new ScanDocsException(ScanDocsException.CODE_999, e.getMessage());
		}

		LOG.debug("readDocSpacer end");
		return res;
	}

	/**
	 * Trasnsforma un objeto Result en un objeto BarCodeResult.
	 * 
	 * @param readResult
	 *            objeto Result.
	 * @return En caso de encontrarse un código de barras de los admitidos por
	 *         el Sistema, objeto que encapsula el tipo de código de barras y el
	 *         texto codificado. En caso contrario, se devuelve null.
	 */
	private static BarCodeResult result2BarCodeResult(Result readResult) {
		BarCodeResult res = null;
		DocSpacerType batchSpacerType = null;

		if (readResult != null) {
			if (BarcodeFormat.CODE_128.equals(readResult.getBarcodeFormat())) {
				batchSpacerType = DocSpacerType.CODE_128;
			} else if (BarcodeFormat.PDF_417.equals(readResult.getBarcodeFormat())) {
				batchSpacerType = DocSpacerType.PDF_417;
			} else if (BarcodeFormat.QR_CODE.equals(readResult.getBarcodeFormat())) {
				batchSpacerType = DocSpacerType.CODE_QR;
			}

			if (batchSpacerType != null) {
				res = new BarCodeResult();
				res.setBarCodeText(readResult.getText());
				res.setBarCodeType(batchSpacerType);
			}
		}

		return res;
	}

	/**
	 * Función para detectar página en blanco.
	 * 
	 * @param page
	 *            Imagen capturada
	 * @return Boolean indicando si es una página en blanco
	 * @throws Exception
	 *             Exception al procesar la detección de página en blanco
	 */

	public static boolean isBlank(BufferedImage page) throws Exception {
		final int CONST_16 = 16;
		final int CONST_8 = 8;
		final int CONST_OxFF = 0xff;
		final double CONST_RED_PORC = 0.299;
		final double CONST_GREEN_PORC = 0.587;
		final double CONST_BLUE_PORC = 0.114;
		long count = 0;
		double total = 0;
		double var = 0;
		double stdDev = 0;
		double avg = 0;
		int height = page.getHeight();
		int width = page.getWidth();

		int[ ] pixels = new int[width * height];
		PixelGrabber pg = new PixelGrabber(page, 0, 0, width, height, pixels, 0, width);
		pg.grabPixels();

		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				count++;
				int pixel = pixels[j * width + i];
				int red = (pixel >> CONST_16) & CONST_OxFF;
				int green = (pixel >> CONST_8) & CONST_OxFF;
				int blue = (pixel) & CONST_OxFF;
				// int value = new Color(red, green, blue, 0).getRGB();
				double luma = CONST_RED_PORC * red + CONST_GREEN_PORC * green + CONST_BLUE_PORC * blue;
				total += luma;
				avg = total / count;
				var += Math.pow(luma - avg, 2);
				stdDev = Math.sqrt(var / count);
			}
		}
		return (stdDev / BLANK_STDDEV_MAG < BLANK_STDDEV_CUTOFF && var / BLANK_VAR_MAG < BLANK_VAR_CUTOFF);
	}

	/**
	 * Procesa las imágenes capturadas.
	 * 
	 * @param images
	 *            Imágenes capturadas.
	 * @param fieldRequest
	 *            Parámetros informados en la petición de procesamiento.
	 * @param spacerSearchingParams
	 *            parámetros de configuración de la instancia de plugin de
	 *            captura de imágenes para el procesamiento de separadores.
	 * @return Lista con los documentos, separadores y páginas en blanco
	 *         identificados.
	 * @throws ScanDocsException
	 *             si se produce algún error procesando las imágenes capturadas.
	 */
	public abstract List<ScanDocRequest> split2Image(final Map<String, MultipartFile> images, final PerformScanDocVO fieldRequest, final Properties spacerSearchingParams) throws ScanDocsException;

	/**
	 * Transforma las imágenes capturadas de un documentos digitalizado al
	 * formato de salida solicitado (application/pdf, image/tiff, image/png o
	 * image/jpeg). Los formatos de archivo image/png y image/jpeg son válidos
	 * exclusivamente para documentos de una sola página, mientras que
	 * application/pdf y image/tiff soportan documentos de más de una página.
	 * 
	 * @param doc
	 *            Documento digitalizado.
	 * @param images
	 *            imágenes capturadas.
	 * @param fieldRequest
	 *            parámetros de la petición web.
	 * @return Array de bytes que representa el contenido del documento
	 *         digitalizado en el formato de salida solicitado.
	 * @throws ScanDocsException
	 *             Error al tratar la imágenes capturadas o número incorrecto de
	 *             páginas.
	 */
	public abstract byte[ ] transformScannedDocument(ScannedDoc doc, final Map<String, MultipartFile> images, PerformScanDocVO fieldRequest) throws ScanDocsException;

	/**
	 * Función que devuelve el tipo de pixel empleado en la digitalización de un
	 * documento (profundidad de color).
	 * 
	 * @param idOutputPixelType
	 *            tipo de pixel informado en la petición web.
	 * @return tipo de pixel empleado en la digitalización de un documento
	 *         (profundidad de color).
	 */
	public static PixelType getByteDeep(String idOutputPixelType) {
		PixelType res = PixelType.RGB;

		if (idOutputPixelType != null && !idOutputPixelType.isEmpty() && !idOutputPixelType.equals("-1")) {
			res = PixelType.getByDeepBitsName(idOutputPixelType);
		}

		// devuelve el tamaño en byte del pixel type
		return res;
	}

	/**
	 * Obtiene la clase encargada de parsear o decodificar imágenes en un
	 * formato determinado.
	 * 
	 * @param contentType
	 *            tipo MIME de las imágenes a parsear.
	 * @param locale
	 *            configuración regional.
	 * @param messages
	 *            bundle multidioma.
	 * @return la clase encargada de parsear o decodificar imágenes en un
	 *         formato determinado.
	 * @throws ScanDocsException
	 *             si se produce algún error al crear o recuperar el
	 *             ImageReader.
	 */
	protected static ImageReader getImageReader(String contentType, MessageSource messages, Locale locale) throws ScanDocsException {
		ImageReader res;
		MimeType mimeType;
		String excMsg;

		mimeType = MimeType.getMimeTypeByType(contentType);

		if (mimeType == null || (!MimeType.JPG_IMG_CONTENT.equals(mimeType) && !MimeType.TIFF_IMG_CONTENT.equals(mimeType))) {
			excMsg = messages.getMessage("error.imageFormatNotEnable.msg", null, locale);
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] {}", excMsg);
			throw new ScanDocsException(ScanDocsException.CODE_002, excMsg);
		}

		Iterator<ImageReader> iterator = ImageIO.getImageReadersByMIMEType(contentType);
		if (iterator == null || !iterator.hasNext()) {
			excMsg = messages.getMessage("error.imageFormatNotEnable.msg", null, locale);
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] {}", excMsg);
			throw new ScanDocsException(ScanDocsException.CODE_002, excMsg);
		}

		res = (ImageReader) iterator.next();
		iterator = null;

		return res;
	}

	/**
	 * Recorta una imagen a partir de los parámetros de detección de carátulas y
	 * separadores configurados en una isntancia del plugin.
	 * 
	 * @param image
	 *            imagen a recortar.
	 * @param spacerSearchingParams
	 *            Conjunto de parámetros que deteminan el área de la imagen a
	 *            recortar.
	 * @return la clase encargada de parsear o decodificar imágenes en un
	 *         formato determinado.
	 * @throws ScanDocsException
	 *             si se produce algún error al recortar la imagen sobre la que
	 *             se realizará la búsqueda.
	 */
	protected BufferedImage getSpacerSearchingArea(BufferedImage image, Properties spacerSearchingParams) throws ScanDocsException {
		BufferedImage res = null;
		int height;
		int width;
		int srcImageHeight;
		int srcImageWidth;
		int paramValue;
		int x0;
		int y0;
		String excMsg;

		try {
			srcImageWidth = image.getWidth();
			LOG.debug("[WEBSCAN-SCAN-DOCS-IMG-PROC] Area de búsqueda de separadores - Ancho imagen: {}", srcImageWidth);
			srcImageHeight = image.getHeight();
			LOG.debug("[WEBSCAN-SCAN-DOCS-IMG-PROC] Area de búsqueda de separadores - Alto imagen: {}", srcImageHeight);

			// Se calcula el punto inicial en la coordenada X
			paramValue = Integer.parseInt(spacerSearchingParams.getProperty(ScanDocsConstants.SPACER_SEARCH_AREA_X0, "0"));
			x0 = (srcImageWidth * paramValue) / 100;
			LOG.debug("[WEBSCAN-SCAN-DOCS-IMG-PROC] Area de búsqueda de separadores - Punto x0: {}", x0);
			// Se calcula el tamaño del área en la coordenada X
			paramValue = Integer.parseInt(spacerSearchingParams.getProperty(ScanDocsConstants.SPACER_SEARCH_AREA_X_SIZE, "0"));
			width = (srcImageWidth * paramValue) / 100;
			LOG.debug("[WEBSCAN-SCAN-DOCS-IMG-PROC] Area de búsqueda de separadores - Ancho: {}", width);
			// Se verifica que no se sobrepasa el ancho máximo de la imagen
			if (srcImageWidth < (x0 + width)) {
				width = srcImageWidth - x0;
				LOG.warn("[WEBSCAN-SCAN-DOCS-IMG-PROC] La configuración actual del plugin de captura excede el tamaño máximo de la imagen en la coordenada X para calcula el área de búsqueda de separadores. Se establece el máximo permitido {}.", width);
			}

			// Se calcula el punto inicial en la coordenada X
			paramValue = Integer.parseInt(spacerSearchingParams.getProperty(ScanDocsConstants.SPACER_SEARCH_AREA_Y0, "0"));
			y0 = (srcImageHeight * paramValue) / 100;
			LOG.debug("[WEBSCAN-SCAN-DOCS-IMG-PROC] Area de búsqueda de separadores - Punto y0: {}", y0);
			// Se calcula el tamaño del área en la coordenada X
			paramValue = Integer.parseInt(spacerSearchingParams.getProperty(ScanDocsConstants.SPACER_SEARCH_AREA_Y_SIZE, "0"));
			height = (srcImageHeight * paramValue) / 100;
			LOG.debug("[WEBSCAN-SCAN-DOCS-IMG-PROC] Area de búsqueda de separadores - Alto: {}", height);
			// Se verifica que no se sobrepasa el ancho máximo de la imagen
			if (srcImageHeight < (y0 + height)) {
				height = srcImageHeight - y0;
				LOG.warn("[WEBSCAN-SCAN-DOCS-IMG-PROC] La configuración actual del plugin de captura excede el tamaño máximo de la imagen en la coordenada Y para calcula el área de búsqueda de separadores. Se establece el máximo permitido {}.", width);
			}

			res = image.getSubimage(x0, y0, width, height);
		} catch (Exception e) {
			String[ ] args = new String[2];
			args[0] = ScanDocsException.CODE_022;
			args[1] = e.getMessage();
			excMsg = messages.getMessage("error.scanProcess.doc", args, locale);
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Código de error: {}. Excepcion {} - {}.", ScanDocsException.CODE_022, e.getCause(), e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_022, excMsg);
		}

		return res;
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Objeto que recopila los mensajes, sujetos a multidioma, que
	 *            son presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}

	/**
	 * Establece la configuración regional.
	 * 
	 * @param aLocale
	 *            configuración regional.
	 */
	public void setLocale(Locale aLocale) {
		this.locale = aLocale;
	}

	/**
	 * Establece la clase encargada de parsear o decodificar imágenes en un
	 * formato determinado.
	 * 
	 * @throws ScanDocsException
	 *             Si se peroduce algún error en la creación y configuración de
	 *             la clase encargada de parsear o decodificar imágenes en un
	 *             formato determinado.
	 */
	public abstract void initReader() throws ScanDocsException;

	/**
	 * Inicia un nuevo objeto del API PDF BOX que representa un nuevo documento
	 * en formato PDF, estableciendo parámetros del documento, tales como autor,
	 * creador, productor, fecha de creación y asunto.
	 * 
	 * @return objeto del API PDF BOX que representa un nuevo documento en
	 *         formato PDF.
	 */
	protected PDDocument initPdfDocument() {
		PDDocument res;
		PDDocumentInformation pdd;

		res = new PDDocument();
		res.setVersion(WebscanConstants.VERSION_PDF);

		pdd = res.getDocumentInformation();
		pdd.setAuthor(messages.getMessage(WebscanConstants.CREATOR_PDF_PROPERTY_PARAM_NAME, null, locale));
		Calendar date = new GregorianCalendar();
		pdd.setCreationDate(date);
		pdd.setCreator(messages.getMessage(WebscanConstants.CREATOR_PDF_PROPERTY_PARAM_NAME, null, locale));
		pdd.setProducer(messages.getMessage(WebscanConstants.CREATOR_PDF_PROPERTY_PARAM_NAME, null, locale));
		pdd.setSubject(messages.getMessage(SUBJECT_PDF_PROPERTY_PARAM_NAME, null, locale));

		return res;
	}
}
