/** 
* <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.util.JpegImageProcessor.java.</p>
* <b>Descripción:</b><p> .</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.scandoc.contr.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.github.jaiimageio.plugins.tiff.TIFFImageWriteParam;

import es.ricoh.webscan.spacers.model.domain.DocSpacerType;
import es.ricoh.webscan.scandoc.contr.ScanDocsException;
import es.ricoh.webscan.scandoc.contr.controller.vo.PerformScanDocVO;
import es.ricoh.webscan.scandoc.contr.controller.vo.PixelType;
import es.ricoh.webscan.scandoc.contr.service.ScanDocRequest;
import es.ricoh.webscan.scandoc.contr.service.ScannedDoc;
import es.ricoh.webscan.utilities.MimeType;

/**
 * <p>
 * Clase JpegImageProcessor.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class JpegImageProcessor extends ImageProcessor {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(JpegImageProcessor.class);

	/**
	 * Clase responsable de la lectura y decodificación de imágenes.
	 */
	private ImageReader reader;

	public JpegImageProcessor() throws ScanDocsException {
		super();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.scandoc.contr.util.ImageProcessor#split2Image(java.util.Map,
	 *      es.ricoh.webscan.scandoc.contr.controller.vo.PerformScanDocVO,
	 *      java.util.Properties)
	 */
	@Override
	public List<ScanDocRequest> split2Image(Map<String, MultipartFile> images, PerformScanDocVO fieldRequest, Properties spacerSearchingParams) throws ScanDocsException {
		List<ScanDocRequest> res = new ArrayList<ScanDocRequest>();
		Long t0;
		Long tf;

		t0 = System.currentTimeMillis();
		LOG.debug("Se inicia el procesamiento de las imágenes capturadas. Procesamiento de separadores deshabilitado: {}.", fieldRequest.getSpacerSearchDisabled());

		if (fieldRequest.getSpacerSearchDisabled()) {
			// Se ha solicitado que no se detecten carátulas y separadores
			res = split2ImageNoSpacers(images);
		} else {
			res = split2ImageSpacers(images, fieldRequest, spacerSearchingParams);
		}

		tf = System.currentTimeMillis();
		LOG.info("Procesamiento de las imágenes capturadas para la petición {} realizado en {} ms. Procesamiento de separadores deshabilitado: {}.", fieldRequest.getRequestId(), (tf - t0), fieldRequest.getSpacerSearchDisabled());

		return res;
	}

	/**
	 * Procesa las imagen capturada, en formato JPEG, sin detección de
	 * separadores.
	 * 
	 * @param images
	 *            Imágenes en formato JPEG.
	 * @return Lista formada por un único documento.
	 * @throws ScanDocsException
	 *             si se produce algún error procesando la imagen capturada.
	 */
	private List<ScanDocRequest> split2ImageNoSpacers(final Map<String, MultipartFile> images) throws ScanDocsException {
		List<Integer> listImage = new ArrayList<Integer>();
		List<ScanDocRequest> res = new ArrayList<ScanDocRequest>();
		ScanDocRequest currentDocRequest;
		int numPages = images.size();

		for (int page = 0; page < numPages; page++) {
			listImage.add(page);
		}
		currentDocRequest = new ScanDocRequest();
		currentDocRequest.setTypeImage(ScannedImageType.DOC_CONTENT);
		currentDocRequest.setListImage(listImage);
		currentDocRequest.setBarCodeResult(null);

		res.add(currentDocRequest);

		return res;
	}

	/**
	 * Procesa la imagen capturada en formato JPEG con detección de separadores.
	 * 
	 * @param images
	 *            Imágenes capturadas.
	 * @param fieldRequest
	 *            Parámetros informados en la petición de procesamiento.
	 * @param spacerSearchingParams
	 *            parámetros de configuración de la instancia de plugin de
	 *            captura de imágenes para el procesamiento de separadores.
	 * @return Lista con los documentos, separadores y páginas en blanco
	 *         identificados.
	 * @throws ScanDocsException
	 *             si se produce algún error procesando la imagen capturada.
	 */
	private List<ScanDocRequest> split2ImageSpacers(final Map<String, MultipartFile> images, final PerformScanDocVO fieldRequest, final Properties spacerSearchingParams) throws ScanDocsException {
		BarCodeResult currentBarCodeResult = null;
		BarCodeResult loopBarCodeResult;
		Boolean isLoopScannedImageBlank;
		BufferedImage bim;
		BufferedImage spacerBim;
		DocSpacerType reqDocSpacerType;
		ImageInputStream is;
		int blankCounter = 0;
		int numPages;
		List<Integer> listImage = new ArrayList<Integer>();
		List<ScanDocRequest> outListScanDocRequest = new ArrayList<ScanDocRequest>();
		MultipartFile image;
		ScannedImageType currentScannedImageType;
		ScannedImageType loopScannedImageType;
		ScanDocRequest currentDocRequest;
		String imgName;
		String excMsg;
		String[ ] args;

		try {
			reqDocSpacerType = DocSpacerType.getByName(fieldRequest.getIdDocSpacerType());
			currentScannedImageType = null;
			numPages = images.size();
			LOG.debug("Número de imágenes a procesar: {}.", numPages);

			for (int page = 0; page < numPages; page++) {
				imgName = REQ_IMAGES_PREFIX + page;
				LOG.debug("Procesando la imagen {}...", imgName);

				image = images.get(imgName);
				LOG.debug("Se lee la imagen {}...", imgName);
				is = ImageIO.createImageInputStream(image.getInputStream());
				if (is == null || is.length() == 0) {
					// no existe documento o está vacio
					excMsg = messages.getMessage("error.emptyImage.msg", null, locale);
					LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] {}", excMsg);
					throw new ScanDocsException(ScanDocsException.CODE_001, excMsg);
				}
				bim = ImageIO.read(is);

				LOG.debug("Se recorta el área de búsqueda de separadores para la imagen {}...", imgName);
				spacerBim = getSpacerSearchingArea(bim, spacerSearchingParams);
				isLoopScannedImageBlank = Boolean.FALSE;
				LOG.debug("Se buscan separadores en la imagen {}...", imgName);
				loopBarCodeResult = readSpacer(spacerBim);
				LOG.debug("Búsqueda de separadores finalizada para la imagen {}...", imgName);
				loopScannedImageType = ScannedImageType.DOC_CONTENT;

				// buscamos si la página actual es un separador
				if (loopBarCodeResult != null) {
					// ceramos nuevo scanDocRequest y iniciamos la lista de
					// documentos
					// añadimos el separador, que luego trataremos
					if (loopBarCodeResult.getBarCodeText().startsWith(TypeIdentScanDoc.BATCH.getName())) {
						loopScannedImageType = ScannedImageType.SPACER_BATCH;
					} else if (loopBarCodeResult.getBarCodeText().startsWith(TypeIdentScanDoc.DOCUM.getName())) {
						if (!reqDocSpacerType.equals(loopBarCodeResult.getBarCodeType())) {
							args = new String[2];
							args[0] = messages.getMessage(loopBarCodeResult.getBarCodeType().name(), null, locale);
							args[1] = messages.getMessage(reqDocSpacerType.name(), null, locale);
							excMsg = messages.getMessage("error.docSpacerNotMatch.msg", args, locale);
							LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] {}", excMsg);
							throw new ScanDocsException(ScanDocsException.CODE_003, excMsg);
						}
						loopScannedImageType = ScannedImageType.SPACER_DOC;
					}
				} else {
					if (isBlank(bim)) {
						if (blankCounter > 0 && DocSpacerType.BLANK_PAGE.equals(reqDocSpacerType)) {
							loopScannedImageType = ScannedImageType.SPACER_BLANK;
							loopBarCodeResult = new BarCodeResult();
							loopBarCodeResult.setBarCodeText("");
							loopBarCodeResult.setBarCodeType(DocSpacerType.BLANK_PAGE);
						}
						loopScannedImageType = currentScannedImageType;
						isLoopScannedImageBlank = Boolean.TRUE;
						blankCounter++;
					} else {
						blankCounter = 0;
					}
				}

				// Agrupación
				if (currentScannedImageType == null) {
					// Se procesa la primera página
					currentScannedImageType = loopScannedImageType;
					currentBarCodeResult = loopBarCodeResult;
					if (!isLoopScannedImageBlank) {
						listImage.add(page);
					}
				} else {
					// Resto de páginas
					if (!loopScannedImageType.equals(currentScannedImageType)) {
						currentDocRequest = new ScanDocRequest();
						currentDocRequest.setTypeImage(currentScannedImageType);
						currentDocRequest.setListImage(listImage);
						currentDocRequest.setBarCodeResult(currentBarCodeResult);
						outListScanDocRequest.add(currentDocRequest);
						// Se inician las estructuras para el siguiente
						// documento, separador o páginas en blanco
						currentScannedImageType = loopScannedImageType;
						currentBarCodeResult = loopBarCodeResult;
						listImage = new ArrayList<Integer>();
						listImage.add(page);
						blankCounter = 0;
					} else {
						if (isLoopScannedImageBlank) {
							if (ScannedImageType.DOC_CONTENT.equals(loopScannedImageType)) {
								listImage.add(page);
							}
						} else {
							listImage.add(page);
						}
					}
				}
			}

			// Se añade al resultado el último elemento procesado
			currentDocRequest = new ScanDocRequest();
			currentDocRequest.setTypeImage(currentScannedImageType);
			currentDocRequest.setListImage(listImage);
			currentDocRequest.setBarCodeResult(currentBarCodeResult);
			outListScanDocRequest.add(currentDocRequest);
		} catch (ScanDocsException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Error procesando la imagen capturada, obtención de documentos. Error: {}", e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_999, e.getMessage());
		}

		return outListScanDocRequest;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.scandoc.contr.util.ImageProcessor#transformScannedDocument(es.ricoh.webscan.scandoc.contr.service.ScannedDoc,
	 *      java.util.Map,
	 *      es.ricoh.webscan.scandoc.contr.controller.vo.PerformScanDocVO)
	 */
	@Override
	public byte[ ] transformScannedDocument(ScannedDoc doc, Map<String, MultipartFile> images, PerformScanDocVO fieldRequest) throws ScanDocsException {
		byte[ ] res = null;
		long t0;
		long tf;

		t0 = System.currentTimeMillis();
		LOG.debug("Se inicia la transfomación del documento {} (Num. pags.: {}), al formato de archivo de salida seleccionado: {}.", doc.getNumOrder(), doc.getNumPage(), doc.getMimeType().getType());

		switch (doc.getMimeType()) {
			case JPG_IMG_CONTENT:
			case PNG_IMG_CONTENT:
				if (doc.getListImage().size() > 1) {
					throw new ScanDocsException(ScanDocsException.CODE_012, "Conversión de formato no esperada. El formato " + doc.getMimeType().getType() + " sólo admite una página.");
				}

				if (!MimeType.JPG_IMG_CONTENT.equals(doc.getMimeType()) && !MimeType.PNG_IMG_CONTENT.equals(doc.getMimeType())) {
					throw new ScanDocsException(ScanDocsException.CODE_012, "Conversión de formato no esperada. El formato " + doc.getMimeType().getType() + " no es soportado por el sistema.");
				}
				res = transformToImage(doc, images);
				break;
			case PDF_CONTENT:
				res = transform2Pdf(doc, images, fieldRequest);
				break;
			case TIFF_IMG_CONTENT:
				res = transformToTiff(doc, images, fieldRequest);
				break;
			default:
				throw new ScanDocsException(ScanDocsException.CODE_012, "Conversión de formato no esperada. El formato " + doc.getMimeType().getType() + " no es soportado por el sistema.");
		}

		tf = System.currentTimeMillis();
		LOG.info("Transfomación del documento {} de la petición {} realizada en {} ms. Num. pags.: {}; Formato de archivo de salida: {}.", doc.getNumOrder(), fieldRequest.getRequestId(), (tf - t0), doc.getNumPage(), doc.getMimeType().getType());

		return res;
	}

	/**
	 * Transforma una imagen capturada a un formato PNG o JPEG. Solo válido para
	 * documentos de una página.
	 * 
	 * @param doc
	 *            Documento digitalizado.
	 * @param images
	 *            imágenes capturadas.
	 * @return Resultado de la primera página.
	 * @throws ScanDocsException
	 *             Error al tratar la imagen de entrada o número incorrecto de
	 *             páginas.
	 */
	public byte[ ] transformToImage(final ScannedDoc doc, final Map<String, MultipartFile> images) throws ScanDocsException {
		BufferedImage bim;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[ ] res = null;
		String excMsg;
		String imageName;
		String targetFormatName = doc.getMimeType().getExtension().substring(1);

		try {
			// salida, sólo admitimos una pagina por el formato
			imageName = REQ_IMAGES_PREFIX + doc.getListImage().get(0);
			bim = ImageIO.read(images.get(imageName).getInputStream());

			ImageIO.write(bim, targetFormatName, baos);
			res = baos.toByteArray();
		} catch (IOException e) {
			String[ ] args = new String[2];
			args[0] = doc.getMimeType().getType();
			args[1] = e.getMessage();
			excMsg = messages.getMessage("error.scanProcess.transform", args, locale);
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Código de error: {}.Excepción {} - {}.", ScanDocsException.CODE_023, e.getCause(), e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_023, excMsg);
		} finally {
			try {
				baos.close();
			} catch (Exception e) {
				// No hacer nada
			}
		}

		return res;
	}

	/**
	 * Construye un archivo PDF a partir de las imágenes capturadas para un
	 * documento.
	 * 
	 * @param doc
	 *            parámetros de la petición web.
	 * @param images
	 *            imágenes capturadas.
	 * @param fieldRequest
	 *            parámetros de la petición web.
	 * @return contenido del documento en formato PDF.
	 * @throws ScanDocsException
	 * @throws IOException
	 *             si se produce un error en la transofrmación del contenido del
	 *             documento a PDF.
	 */
	private byte[ ] transform2Pdf(ScannedDoc doc, Map<String, MultipartFile> images, PerformScanDocVO fieldRequest) throws ScanDocsException {
		byte[ ] res = null;
		ByteArrayOutputStream baos;
		float height;
		float width;
		MultipartFile image;
		PDDocument document;
		PDImageXObject imgPage;
		PDPage pdfPage;
		PDPageContentStream contentStream;
		String excMsg;
		String imgName;

		try {
			document = initPdfDocument();

			for (int page = 0; page < doc.getListImage().size(); page++) {
				imgName = REQ_IMAGES_PREFIX + doc.getListImage().get(page);
				LOG.debug("[WEBSCAN-SCAN-DOCS-IMG-PROC] Incluyendo la imagen {} en el documento {}", imgName, doc.getNumOrder());
				image = images.get(imgName);

				LOG.debug("Se lee la imagen {}...", imgName);

				imgPage = JPEGFactory.createFromStream(document, image.getInputStream());

				width = (Integer.valueOf(imgPage.getWidth()).floatValue() / fieldRequest.getIdOutputResolution().floatValue()) * POINT_PER_INCH;
				height = (Integer.valueOf(imgPage.getHeight()).floatValue() / fieldRequest.getIdOutputResolution().floatValue()) * POINT_PER_INCH;
				pdfPage = new PDPage(new PDRectangle(width, height));
				document.addPage(pdfPage);
				contentStream = new PDPageContentStream(document, pdfPage);
				contentStream.drawImage(imgPage, 0, 0, width, height);
				contentStream.close();
			}

			baos = new ByteArrayOutputStream();
			document.save(baos);
			document.close();
			res = baos.toByteArray();
			baos.close();
		} catch (IOException e) {
			String[ ] args = new String[2];
			args[0] = "PDF";
			args[1] = e.getMessage();
			excMsg = messages.getMessage("error.scanProcess.transform", args, locale);
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Código de error: {}.Excepción {} - {}.", ScanDocsException.CODE_023, e.getCause(), e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_023, excMsg);
		}

		return res;
	}

	/**
	 * Construye un archivo TIFF a partir de las imágenes capturadas para un
	 * documento.
	 * 
	 * @param doc
	 *            documento digitalizado.
	 * @param images
	 *            imágenes capturadas.
	 * @param fieldRequest
	 *            parámetros de la petición web.
	 * @return contenido del documento en formato PDF.
	 * @throws ScanDocsException
	 * @throws IOException
	 *             si se produce un error en la transformación del contenido del
	 *             documento a formato de archivo TIFF.
	 */
	private byte[ ] transformToTiff(ScannedDoc doc, Map<String, MultipartFile> images, PerformScanDocVO fieldRequest) throws ScanDocsException {
		byte[ ] res;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedImage bim;
		ImageWriter writer = null;
		int pages;
		ImageOutputStream ios = null;
		PixelType docPixelType;
		String imageName;
		String excMsg;
		TIFFImageWriteParam writeParam;

		try {
			writer = ImageIO.getImageWritersByFormatName("TIFF").next();
			docPixelType = ImageProcessor.getByteDeep(fieldRequest.getIdOutputPixelType());
			pages = doc.getListImage().size();
			ios = ImageIO.createImageOutputStream(baos);
			writer.setOutput(ios);

			writeParam = new TIFFImageWriteParam(fieldRequest.getLocale());
			writeParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			if (PixelType.BW.equals(docPixelType)) {
				writeParam.setCompressionType(BLACK_WHITE_TIFF_COMP);
			} else if (PixelType.GRAY.equals(docPixelType)) {
				writeParam.setCompressionType(GRAY_SCALE_TIFF_COMP);
			} else {
				writeParam.setCompressionType(RGB_TIFF_COMP);
			}
			writeParam.setTilingMode(ImageWriteParam.MODE_EXPLICIT);
			writer.prepareWriteSequence(null);

			for (int page = 0; page < pages; page++) {
				imageName = REQ_IMAGES_PREFIX + doc.getListImage().get(page);
				bim = ImageIO.read(images.get(imageName).getInputStream());
				writeParam.setTiling(bim.getWidth(), bim.getHeight(), 0, 0);
				writer.writeToSequence(new IIOImage(bim, null, null), writeParam);
			}

			writer.endWriteSequence();

			ios.close();
			res = baos.toByteArray();

		} catch (IOException e) {
			String[ ] args = new String[2];
			args[0] = "TIFF";
			args[1] = e.getMessage();
			excMsg = messages.getMessage("error.scanProcess.transform", args, locale);
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Código de error: {}.Excepción {} - {}.", ScanDocsException.CODE_023, e.getCause(), e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_023, excMsg);
		} finally {
			try {
				writer.dispose();
			} catch (Exception e) {
				// No hacer nada
			}
			try {
				if (ios != null) {
					ios.close();
				}
			} catch (Exception e) {
				// No hacer nada
			}
			try {
				baos.close();
			} catch (Exception e) {
				// No hacer nada
			}
		}

		return res;

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.scandoc.contr.util.ImageProcessor#initReader()
	 */
	public void initReader() throws ScanDocsException {
		reader = ImageProcessor.getImageReader(MimeType.JPG_IMG_CONTENT.getType(), messages, locale);
	}

}
