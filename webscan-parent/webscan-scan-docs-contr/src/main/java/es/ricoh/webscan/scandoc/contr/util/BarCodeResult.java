/** 
* <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.util.BarCodeResult.java.</p>
* <b>Descripción:</b><p> Resultado obtenido tras leer una imagen de la que se desea conocer si incluye un código de barras.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.scandoc.contr.util;

import java.io.Serializable;

import es.ricoh.webscan.spacers.model.domain.DocSpacerType;

/**
 * Resultado obtenido tras leer una imagen de la que se desea conocer si incluye
 * un código de barras.
 * <p>
 * Clase BarCodeResult.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class BarCodeResult implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Texto codificado.
	 */
	private String barCodeText = null;

	/**
	 * Tipo de código de barras.
	 */
	private DocSpacerType barCodeType;

	/**
	 * Constructor sin argumentos.
	 */
	public BarCodeResult() {
		super();
	}

	/**
	 * Obtiene el texto codificado.
	 * 
	 * @return el texto codificado.
	 */
	public String getBarCodeText() {
		return barCodeText;
	}

	/**
	 * Establece el texto codificado.
	 * 
	 * @param aBarCodeText
	 *            nuevo texto codificado.
	 */
	public void setBarCodeText(String aBarCodeText) {
		this.barCodeText = aBarCodeText;
	}

	/**
	 * Obtiene el tipo de código de barras.
	 * 
	 * @return el tipo de código de barras.
	 */
	public DocSpacerType getBarCodeType() {
		return barCodeType;
	}

	/**
	 * Establece el tipo de código de barras.
	 * 
	 * @param aBarCodeType
	 *            tipo de código de barras.
	 */
	public void setBarCodeType(DocSpacerType aBarCodeType) {
		this.barCodeType = aBarCodeType;
	}

}
