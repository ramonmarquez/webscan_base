/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.controller.vo.PixelType.java.</p>
 * <b>Descripción:</b><p> .</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.scandoc.contr.controller.vo;

/**
 * Representa la relación de tipos de pixeles ( Blanco_Negro, Gris, Color)
 * <p>
 * Class .
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum PixelType {
	/**
	 * B_W.
	 */
	BW("B_W", 1),
	/**
	 * GRAY.
	 */
	GRAY("gray", 8),
	/**
	 * RGB.
	 */
	RGB("RGB", 24);

	/**
	 * Nombre de bits de profundidad.
	 */
	private String deepBitsName;

	/**
	 * Número de bits de profunidad.
	 */
	private int deepBits;

	/**
	 * Constructor con argumentos.
	 *
	 * @param aByteDeepName
	 *            Denominación de la produnfidad de bytes e color.
	 * @param aDeepBits
	 *            Información de la profundidad de bits usada.
	 */
	PixelType(String aByteDeepName, int aDeepBits) {
		this.deepBitsName = aByteDeepName;
		this.deepBits = aDeepBits;

	}

	/**
	 * Gets the value of the attribute {@link #deepBitsName}.
	 * 
	 * @return the value of the attribute {@link #deepBitsName}.
	 */
	public String getDeepBitsName() {
		return deepBitsName;
	}

	/**
	 * Gets the value of the attribute {@link #deepBits}.
	 * 
	 * @return the value of the attribute {@link #deepBits}.
	 */
	public int getDeepBits() {
		return deepBits;
	}

	/**
	 * Obtiene la definición del elemento enumerado por la propiedad.
	 * 
	 * @param value
	 *            nombre de la definición de deepBits.
	 * @return info del formato .
	 */

	public static final PixelType getByDeepBitsName(String value) {
		PixelType res = null;
		for (PixelType aux: PixelType.values()) {
			if (aux.deepBitsName.equalsIgnoreCase(value)) {
				res = aux;
				break;
			}
		}
		return res;
	}

}
