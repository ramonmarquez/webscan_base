/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.controller.scanDocsController.java.</p>
 * <b>Descripción:</b><p> .</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.scandoc.contr.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.DepartamentVO;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.scandoc.contr.ScanDocsConstants;
import es.ricoh.webscan.scandoc.contr.ScanDocsException;
import es.ricoh.webscan.scandoc.contr.controller.vo.EnumErrorPerformScanDocVO;
import es.ricoh.webscan.scandoc.contr.controller.vo.PerformScanDocVO;
import es.ricoh.webscan.scandoc.contr.controller.vo.PixelType;
import es.ricoh.webscan.scandoc.contr.service.ScanDocResult;
import es.ricoh.webscan.scandoc.contr.service.ScanDocsService;
import es.ricoh.webscan.utilities.CryptoUtilities;
import es.ricoh.webscan.utilities.MimeType;

/**
 * Controlador del escaneo de documentos.
 * <p>
 * Class ScanDocsController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
@Controller
public class ScanDocsController {

	/**
	 * Constante que identifica el mensaje de debug en caso error en acceso a
	 * Base de Datos.
	 */
	private static final String ME_ERROR_BBDD = "[WEBSCAN-SCANDOCS-CONTR] Error de Base de Datos ";
	/**
	 * Constante asociada al titulo del modal a mostrar para la reserva.
	 */
	private static final String ME_BOOK_TITLE = "message.book.title";
	/**
	 * Constante que identifica la vista del modal de respuesta del registro de
	 * escaneo.
	 */
	private static final String VIEW_MOD_RESP_REG = "fragments/scanDocs/modalResponseRegistre";
	/**
	 * Constante que identifica la vista de escaneo de documentos.
	 */
	private static final String VIEW_SCAN_DOCS = "scanDocs/scanDocs";
	/**
	 * Constante que identifica la vista vacia.
	 */
	private static final String VIEW_EMPTY = "empty";
	/**
	 * Constante que identifica la vista principal (main.html).
	 */
	private static final String VIEW_MAIN = "main";
	/**
	 * Constante auxiliar para comparar valores nulos de la vista.
	 */
	private static final String NULL = "null";
	/**
	 * Constante error de parámetro no esperado.
	 */
	private static final String MES_PAR_NOT_WAIT = "message.parameter.notWait";
	/**
	 * Constante error de parámetro no válido.
	 */
	private static final String MES_PARVAL_NOTVAL = "message.parameter.value.notValided";
	/**
	 * Constante error de parámetro no válido.
	 */
	private static final String MES_USERDPTO_NOTASSOCIATED = "message.parameter.userDpto.value.notAssociated";
	/**
	 * Constante error de parámetro no válido.
	 */
	private static final String MES_USERDPTO_REQUIRED = "message.parameter.userDpto.value.required";
	/**
	 * Constante error de no recepción de imágenes capturadas.
	 */
	private static final String MES_IMAGES_EMPTY = "message.parameter.images.empty";
	/**
	 * Constante error para informar sobre que se recibio un número incorrecto
	 * de imágenes capturadas.
	 */
	private static final String MES_IMAGES_SIZE_ERROR = "message.parameter.images.size.error";
	/**
	 * Constante error para informar sobre que se recibio un número incorrecto
	 * de imágenes capturadas.
	 */
	private static final String MES_IMAGES_MIMETYPE_ERROR = "message.parameter.images.mimiType.error";
	/**
	 * Constante error proceso digitalizacion.
	 */
	private static final String DIG_DOC_OK = "ok.processDigitalization.doc";
	/**
	 * Constante ok proceso digitalizacion.
	 */
	private static final String DIG_DOC_ER = "error.scanProcess.doc";

	/**
	 * constante que identifica la etapa de escaneo.
	 */
	private static final String SCAN = "Scan";

	/**
	 * Constantes usadas en la validación de parámetros en relación con
	 * FileFormats - scan.fileFormats.enable.
	 */
	private static final String SCAN_FILEFO_EN = "scan.fileFormats.enable";
	/**
	 * Constantes usadas en la validación de parámetros en relación con
	 * FileFormats - scan.fileFormats.values.
	 */
	private static final String SCAN_FILEFO_VAL = "scan.fileFormats.values";
	/**
	 * Constantes usadas en la validación de parámetros en relación con
	 * FileFormats - scan.fileFormats.values.default.
	 */
	private static final String SCAN_FILEFO_VDEF = "scan.fileFormats.values.default";
	/**
	 * Constantes usadas en la validación de parámetros en relación con
	 * FileFormats - label.OutputFormat.
	 */
	private static final String LABEL_OUTFORMAT = "label.OutputFormat";

	/**
	 * Constantes usadas en la validación de parámetros en relación con
	 * Resolution - scan.resolution.enable.
	 */
	private static final String SCAN_RES_EN = "scan.resolution.enable";
	/**
	 * Constantes usadas en la validación de parámetros en relación con
	 * Resolution.
	 */
	private static final String SCAN_RES_VAL = "scan.resolution.values";
	/**
	 * Constantes usadas en la validación de parámetros en relación con
	 * Resolution - scan.resolution.values.default.
	 */
	private static final String SCAN_RES_VAL_DEF = "scan.resolution.values.default";
	/**
	 * Constantes usadas en la validación de parámetros en relación con
	 * Resolution - label.Resolution.
	 */
	private static final String LABEL_RESOLUTION = "label.Resolution";

	/**
	 * Constantes identificativas de las etiquetas de la vista de scanDocs -
	 * label.OrgUnitId.
	 */
	private static final String LABEL_ORG_UNIT_ID = "label.OrgUnitId";
	/**
	 * Constantes identificativas de las etiquetas de la vista de scanDocs -
	 * label.ScanProfileId.
	 */
	private static final String LABEL_SCANPROFID = "label.ScanProfileId";
	/**
	 * Constantes identificativas de las etiquetas de la vista de scanDocs -
	 * label.TypeDoc.
	 */
	private static final String LABEL_TYPE_DOC = "label.TypeDoc";

	/**
	 * Constantes identificativas de las etiquetas de la vista de scanDocs -
	 * label.UserDpto.
	 */
	private static final String LABEL_USER_DPTO = "label.UserDpto";

	/**
	 * Constantes identificativas de las etiquetas de la vista de scanDocs -
	 * label.DocSpacerType.
	 */
	private static final String LABEL_DOCSPATYPE = "label.DocSpacerType";
	/**
	 * Constantes identificativas de las etiquetas de la vista de scanDocs -
	 * label.DocBatchValNumDocs.
	 */
	private static final String LABEL_DBATVNDOCS = "label.DocBatchValNumDocs";
	/**
	 * Constantes identificativas de las etiquetas de la vista de scanDocs -
	 * label.DocValNumPagsDoc.
	 */
	private static final String LABEL_DVNPAGSDOC = "label.DocValNumPagsDoc";
	/**
	 * Constantes identificativas de las etiquetas de la vista de scanDocs -
	 * label.dateBookingOK.
	 */
	private static final String LABEL_DATEBOOKOK = "label.dateBookingOK";
	/**
	 * Constantes identificativas de la vista de scanDocs - idTypeDoc.
	 */
	private static final String ID_TYPE_DOC = "idTypeDoc";

	/**
	 * Constantes identificativas de la vista de scanDocs - userDptoId.
	 */
	private static final String USER_DPTO_ID = "userDptoId";

	/**
	 * Constantes identificativas de la vista de scanDocs - idOutputResolution.
	 */

	private static final String ID_OUTPUTRES = "idOutputResolution";
	/**
	 * Constantes identificativas de la vista de scanDocs - idOutputPixelType.
	 */

	private static final String ID_OUTPUTPIXTYPE = "idOutputPixelType";
	/**
	 * Constantes identificativas de la vista de scanDocs - idOutputFormat.
	 */
	private static final String ID_OUTPUT_FORMAT = "idOutputFormat";
	/**
	 * Constantes identificativas de la vista de scanDocs - dateBookingOK.
	 */
	private static final String DATE_BOOKING_OK = "dateBookingOK";
	/**
	 * Constantes identificativas de la vista de scanDocs - scanBookId.
	 */
	private static final String SCAN_BOOK_ID = "scanBookId";
	/**
	 * Constantes identificativas de la vista de scanDocs -
	 * idDocBatchDocSpacerType.
	 */
	private static final String IDDOCBATDOCSPATY = "idDocBatchDocSpacerType";
	/**
	 * Constantes identificativas de la vista de scanDocs -
	 * spacerSearchDisabled.
	 */
	private static final String SPACER_SEARCH_DISA_INPUT_NAME = "spacerSearchDisabled";

	/**
	 * Constantes identificativas de la vista de scanDocs -
	 * idDocBatchValNumDocsEnable.
	 */
	private static final String IDDCBATVNUMDCSEN = "idDocBatchValNumDocsEnable";
	/**
	 * Constantes identificativas de la vista de scanDocs -
	 * idDocBatchValNumDocs.
	 */
	private static final String IDDCBATVALNUMDCS = "idDocBatchValNumDocs";
	/**
	 * Constantes identificativas de la vista de scanDocs -
	 * scan.docBatch.enable.
	 */
	private static final String SC_DOCBATEN = "scan.docBatch.enable";
	/**
	 * Constantes identificativas del parámetro del plugin de captura
	 * scan.spacer.search.disabled.select.enable.
	 */
	private static final String SPACER_SEARCH_DISABLED_ENA = "scan.spacer.search.disabled.select.enable";
	/**
	 * Constantes identificativas del parámetro del plugin de captura
	 * scan.spacer.search.disabled.value.default.
	 */
	private static final String SPACER_SEARCH_DISABLED_DEF_VALUE = "scan.spacer.search.disabled.value.default";
	/**
	 * Constantes identificativas de la vista de scanDocs -
	 * scan.docBatch.val.numDocs.
	 */
	private static final String SC_DCBATVALNDCS = "scan.docBatch.val.numDocs";
	/**
	 * Constantes identificativas de la vista de scanDocs -
	 * scan.docBatch.docSpacer.types.
	 */
	private static final String SC_DCBATDCSPA_TY = "scan.docBatch.docSpacer.types";
	/**
	 * Constantes identificativas de la vista de scanDocs -
	 * scan.doc.val.numPagsDoc.
	 */
	private static final String SC_DC_VAL_NPAGSDC = "scan.doc.val.numPagsDoc";
	/**
	 * Constantes identificativas de la vista de scanDocs - idDocValNumPagsDoc.
	 */
	private static final String IDDCVALNUMPAGSDC = "idDocValNumPagsDoc";
	/**
	 * Constantes identificativas de la vista de scanDocs -
	 * idDocValNumPagsDocEnable.
	 */
	private static final String IDDCVALNPAGSDCEN = "idDocValNumPagsDocEnable";

	/**
	 * Constantes identificativa de Pixel Type enable.
	 */
	private static final String SC_PT_ENAB = "scan.pixelType.enable";
	/**
	 * Constantes identificativa de Pixel Type value.
	 */

	private static final String SC_PT_VAL = "scan.pixelType.values";
	/**
	 * Constantes identificativa de Pixel Type value default.
	 */

	private static final String SC_PT_VAL_DEF = "scan.pixelType.values.default";
	/**
	 * Constantes identificativa de Pixel Type.
	 */

	private static final String LABEL_PIXELTYPE = "label.PixelType";

	/**
	 * Constantes identificativa de unidad organizativa.
	 */
	private static final String ORG_UNIT_ID = "orgUnitId";
	/**
	 * Constantes identificativa de perfil de escaneo.
	 */
	private static final String SCAN_PROFILE_ID = "scanProfileId";

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScanDocsController.class);

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	@Autowired
	private MessageSource messages;

	/**
	 * Objeto que representa el acceso a los servicio de DocBox.
	 */
	@Autowired
	private ScanDocsService scanDocsService;

	/**
	 * Funcion principal de la ventana de digitalizacion.
	 * 
	 * @param model
	 *            Parametro de entrada salida asociada a la información que
	 *            enviamos a la pantalla
	 * @param orgUnitId
	 *            identificador de la Unidad Organica
	 * @param scanProfileId
	 *            identificador del perfil de escaneo
	 * @param request
	 *            Parametro que identifica la petición
	 * @param typeRedirect
	 *            Parámetro que identifica el tipo de redirección.
	 * @return Cadena que identifica el html de respuesta
	 */

	@RequestMapping(value = "/scanDocs/scanDocs", method = { RequestMethod.GET, RequestMethod.POST })
	public String scanDocs(final Model model, final String orgUnitId, final String scanProfileId, final boolean started, @RequestParam(value = "typeRedirect", required = false) String typeRedirect, HttpServletRequest request) {
		// string para respuesta visual
		long t0;
		long tf;
		String[ ] args = new String[2];

		t0 = System.currentTimeMillis();
		LOG.debug("[WEBSCAN-SCANDOCS] scanDocs ...");
		LOG.debug("[WEBSCAN-SCANDOCS] orgId ...", orgUnitId);
		LOG.debug("[WEBSCAN-SCANDOCS] scanProfileId ...", scanProfileId);
		LOG.debug("[WEBSCAN-SCANDOCS] typeRedirect ...", typeRedirect);
		// si los parametros esperados no existe
		if (orgUnitId.isEmpty() || scanProfileId.isEmpty()) {
			BaseControllerUtils.addCommonMessage(ScanDocsConstants.SCANDOCS_ERROR_PARAM_TEXT, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.error("Error al recibir los parametros de pantalla");
			return VIEW_MAIN;
		}
		try {
			final HttpSession session = request.getSession(false);
			UserInSession user = (UserInSession) session.getAttribute(ScanDocsConstants.USER_SESSION_ATT_NAME);

			// pasamos los parametros a long
			Long scanProfileIdL = Long.valueOf(scanProfileId);
			Long orgUnitIdL = Long.valueOf(orgUnitId);
			// verificamos que la unidad organizativa y el perfil que nos
			// informa este configurado y activo
			ScanProfileOrgUnitPluginDTO scProfOrgUnitPl = scanDocsService.getScanProfileOrgUnitPluginDTOByScan(scanProfileIdL, orgUnitIdL);

			// no tenemos plugin en escaneo disponible
			if (scProfOrgUnitPl == null) {
				BaseControllerUtils.addCommonMessage(ScanDocsConstants.SCANDOCS_ERROR_PLUGIN_NOT_CONF, args, ViewMessageLevel.WARN, WebScope.REQUEST, request);
				LOG.error("No existe plugin configurado para la fase de escaneo");
				return VIEW_MAIN;
			} else if (!scProfOrgUnitPl.getActive()) { // FALSE
				BaseControllerUtils.addCommonMessage(ScanDocsConstants.SCANDOCS_ERROR_PLUGIN_NOT_ACTIVE, args, ViewMessageLevel.WARN, WebScope.REQUEST, request);
				LOG.debug("El perfil y la unidad organizativa no tiene activa la fase de escaneo");
				return VIEW_EMPTY;
			}

			// variables auxiliares
			ScanProfileDTO scanProfile = null;
			List<DocumentSpecificationDTO> listDocSpec = new ArrayList<DocumentSpecificationDTO>();
			Properties pluginParams = new Properties();

			// obtenemos toda la informacion de la base datos
			scanProfile = scanDocsService.getScanProfile(scanProfileIdL);
			listDocSpec = getListDocSpecficationActiveByScanProfileOrgUnitId(scanProfileIdL, orgUnitIdL);
			// extraemos los parametros del plugin de escaneo
			pluginParams = scanDocsService.extractPluginParam(scanProfileIdL, orgUnitIdL);


			// asignamos la respuesta
			String titleMessage = ME_BOOK_TITLE;

			// atributos de la vista
			model.addAttribute("scanProfile", scanProfile);
			model.addAttribute("listDocSpec", listDocSpec);
			model.addAttribute("paramPlugin", pluginParams);
			model.addAttribute("userDptos", BaseControllerUtils.getUserFunctionalOrgsByOrgUnit(user, orgUnitIdL));
			model.addAttribute("titleMessage", titleMessage);
			model.addAttribute("userOrgUnitScanProfiles", scanDocsService.getOrgUnitScanProfiles(user));

			if (typeRedirect != null) {
				model.addAttribute("typeRedirect", typeRedirect);
			}

			model.addAttribute("orgUnitId", orgUnitId);
			model.addAttribute("scanProfileId", scanProfileId);

		} catch (DAOException e) {
			args[0] = e.getCode();
			args[1] = e.getMessage();
			BaseControllerUtils.addCommonMessage(ScanDocsConstants.SCANDOCS_ERROR_DATA_BASE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.error(ME_ERROR_BBDD, e);
			return VIEW_MAIN;
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-SCANDOCS-CONTR] Carga del plugin de captura de imágenes de documentos realizada en {} ms.", (tf - t0));
		}
		return VIEW_SCAN_DOCS;
	}


	/**
	 * Recupera un listado de la especificacion de documentos activos por perfil
	 * de escaneo y unidad organizativa.
	 * 
	 * @param scanProfileId
	 *            Identificador del perfil de escaneo
	 * @param orgUnitId
	 *            Identificador de la unidad organizativa
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 * @return Listado de especificaciones de documentos.
	 */
	private List<DocumentSpecificationDTO> getListDocSpecficationActiveByScanProfileOrgUnitId(final Long scanProfileId, final Long orgUnitId) throws DAOException {
		Long t0;
		Long tf;
		List<DocumentSpecificationDTO> listDocSpec = new ArrayList<DocumentSpecificationDTO>();
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> mapDocSpecs;

		LOG.debug("[WEBSCAN-SCANDOCS-CONTR] getListDocSpecficationActiveByScanProfileOrgUnitId Start");
		t0 = System.currentTimeMillis();

		mapDocSpecs = scanDocsService.getDocSpecificationsByScanProfileOrgUnitId(scanProfileId, orgUnitId);

		// recupero ListDocSpec que estén activos
		for (Entry<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> dats: mapDocSpecs.entrySet()) {
			// si está activo lo añadimos como disponible
			if (dats.getKey().getActive() && dats.getValue().getActive()) {
				listDocSpec.add(dats.getKey());

			}
		}

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-SCANDOCS-CONTR] Listado especificaciones de documentos activos por perfil finalizado en {} ms.", (tf - t0));
		LOG.debug("[WEBSCAN-SCANDOCS-CONTR] getListDocSpecficationActiveByScanProfileOrgUnitId End");
		return listDocSpec;
	}

	/**
	 * Método que realiza la recopilación y tratamiento de la información
	 * capturada en la pantalla de escaneo.
	 * 
	 * @param model
	 *            Parametro para preparar la respuesta de la vista
	 * @param request
	 *            Parametro de la petición
	 * @param file
	 *            Parametro que incorpora la información digitalizada en la
	 *            pantalla en formato multipartFile TIFF
	 * @param allRequestParams
	 *            Map con todos los parametros adicionales incorporados en la
	 *            pantalla de escaneo
	 * @return String vista para tratar la respuesta
	 */

	@RequestMapping(value = "/scanDocs/performScanDoc", method = { RequestMethod.POST })
	public String perfomScanDoc(ModelMap model, HttpServletRequest request, @RequestParam Map<String, String> allRequestParams) {
		Boolean respWithError = Boolean.TRUE;
		Integer numDocs = 0;
		Map<String, String> meErrorValid = new HashMap<String, String>();
		List<String> meRegistreDoc = new ArrayList<String>();
		Long t0;
		Long tf;
		Map<String, MultipartFile> images;
		MultipartHttpServletRequest multipartHttpServletRequest;
		PerformScanDocVO fieldRequest;
		Properties pluginParams;
		ScanProfileOrgUnitDTO scanProfileOrgUnit = null;
		String error;
		String errorMessage;
		String errorField;
		String[ ] args = new String[2];
		UserInSession userSession;

		LOG.debug("[WEBSCAN-SCANDOCS-CONTR] perfomScanDoc Start");
		t0 = System.currentTimeMillis();
		fieldRequest = new PerformScanDocVO();
		fieldRequest.setRequestId(CryptoUtilities.getUID());

		try {
			multipartHttpServletRequest = (MultipartHttpServletRequest) request;
			images = multipartHttpServletRequest.getFileMap();

			// Se obtienen los parámetros de configuración del plugin de captura
			pluginParams = getPluginParamsByRequest(allRequestParams, fieldRequest);
			userSession = (UserInSession) request.getSession(false).getAttribute(WebscanBaseConstants.USER_SESSION_ATT_NAME);

			// comprobamos parametros
			if (!validateParam(allRequestParams, images, meErrorValid, fieldRequest, pluginParams, userSession, request)) {
				if (meErrorValid != null && !meErrorValid.isEmpty()) {
					for (Iterator<String> it = meErrorValid.keySet().iterator(); it.hasNext();) {
						errorMessage = it.next();
						errorField = meErrorValid.get(errorMessage);
						args[0] = null;
						if (!errorField.isEmpty()) {
							args[0] = messages.getMessage(errorField, null, request.getLocale());
						}
						error = messages.getMessage(errorMessage, args, ScanDocsConstants.SCANDOCS_ERROR_DEFAULT, request.getLocale());
						LOG.error(error);
						meRegistreDoc.add(error);
					}
				}
				LOG.error("[WEBSCAN-SCANDOCS-CONTR] Error al validar los datos de captura");

			} else {
				// recuperamos el objecto de la fase de escaneo
				ScanProcessStageDTO stageCapture = scanDocsService.getScanProcStageByName(SCAN);

				// verificamos que la unidad organizativa y el perfil que nos
				// informa este configurado y activo
				ScanProfileOrgUnitPluginDTO scanProfOrgUnitPl = null;
				Long scanTemplateId = fieldRequest.getScanTemplateId();
				Long orgUnitId = fieldRequest.getOrgUnitId();
				try {
					scanProfOrgUnitPl = scanDocsService.getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(scanTemplateId, orgUnitId, stageCapture.getId());
				} catch (DAOException e) {
					if (!e.getCode().equals(DAOException.CODE_901)) {
						throw e;
					}
				}

				// no tenemos plugin en escaneo disponible
				if (scanProfOrgUnitPl == null) {
					error = messages.getMessage(ScanDocsConstants.SCANDOCS_ERROR_PLUGIN_NOT_CONF, args, ScanDocsConstants.SCANDOCS_ERROR_DEFAULT, request.getLocale());
					LOG.error("No existe plugin configurado para la fase de escaneo");
					meRegistreDoc.add(error);
				} else if (scanProfOrgUnitPl.getActive().equals(Boolean.FALSE)) {
					error = messages.getMessage(ScanDocsConstants.SCANDOCS_ERROR_PLUGIN_NOT_ACTIVE, args, ScanDocsConstants.SCANDOCS_ERROR_DEFAULT, request.getLocale());
					LOG.debug("El perfil y la unidad organizativa no tiene activa la fase de escaneo");
					meRegistreDoc.add(error);
				} else {
					fieldRequest.setLocale(request.getLocale());
					scanProfileOrgUnit = scanDocsService.getScanProfileOrgUnitByScanProfileAndOrgUnit(scanTemplateId, orgUnitId);
					fieldRequest.setScanProfileOrgUnitId(scanProfileOrgUnit.getId());

					// todo ha ido bien, montamos el mensaje para mostrar al
					// usuario
					// llamamos al servicio para que nos registre los docs/lotes
					LOG.debug("[WEBSCAN-SCANDOCS-CONTR] Iniciando el procesamiento de las imagenes capturadas ...");
					List<ScanDocResult> listScanDocs = scanDocsService.registreScanDocs(images, fieldRequest, userSession, pluginParams, request);

					LOG.debug("[WEBSCAN-SCANDOCS-CONTR] Iniciando la ejecución asincrona del proceso de digitalización para los documentos digitalizados...");
					scanDocsService.callAsincDocScanProcess(listScanDocs, userSession.getUsername(), fieldRequest.getRequestId());
					LOG.debug("[WEBSCAN-SCANDOCS-CONTR] Construyendo mensaje de respuesta a usuario ...");
					for (ScanDocResult sdr: listScanDocs) {
						args[0] = sdr.getDocId().toString();
						String okStr = messages.getMessage(DIG_DOC_OK, args, args[0], request.getLocale());
						LOG.debug(okStr);
						meRegistreDoc.add(okStr);
					}
					numDocs = listScanDocs.size();
					respWithError = Boolean.FALSE;
				}
			}
		} catch (ScanDocsException e) {
			args[0] = e.getCode();
			args[1] = e.getMessage();

			error = messages.getMessage(DIG_DOC_ER, args, args[1], request.getLocale());
			LOG.error(error);
			meRegistreDoc.add(error);
		} catch (Exception e) {
			args[0] = ScanDocsException.CODE_999;
			args[1] = e.getMessage();

			error = messages.getMessage(DIG_DOC_ER, args, args[1], request.getLocale());
			LOG.error(error);
			meRegistreDoc.add(error);
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-SCANDOCS-CONTR]PerfomScanDoc Petición {}. Proceso de captura de imágenes finalizado en {} ms. Documentos procesados {}", fieldRequest.getRequestId(), (tf - t0), numDocs);
		}

		// preparamos el resultado
		model.addAttribute("resultResponse", meRegistreDoc);
		model.addAttribute("respWithError", respWithError);

		LOG.debug(" [WEBSCAN-SCANDOCS-CONTR] perfomScanDoc End");
		return VIEW_MOD_RESP_REG;
	}


	/**
	 * Recupera la lista de parámetros configurado para el plugin de captura del
	 * perfil de digitalización seleccionado por el usuario.
	 * 
	 * @param allRequestParams
	 *            parámetros de la petición web.
	 * @param fieldRequest
	 *            Objeto de la capa de servicios donde se recopilan los
	 *            parámetros de la petición web.
	 * @return lista de parámetros configurado para el plugin de captura del
	 *         perfil de digitalización seleccionado por el usuario.
	 * @throws DAOException
	 *             Si se produce algún error al consultar en el modelo de datos
	 *             los parámetros del plugin de captura de imágenes.
	 */
	private Properties getPluginParamsByRequest(Map<String, String> allRequestParams, PerformScanDocVO fieldRequest) throws DAOException {
		Long orgUnitId = null;
		Long scanTemplateId = null;
		Properties res = new Properties();
		String fieldStr;

		try {
			// comprobamos la info obtenenida de unidad organizativa
			fieldStr = allRequestParams.get(ORG_UNIT_ID);
			if (fieldStr != null) {
				orgUnitId = Long.valueOf(fieldStr);
				fieldRequest.setOrgUnitId(orgUnitId);
			}
			// comprobamos la info obtenenida de perfil de escaneo
			fieldStr = allRequestParams.get(SCAN_PROFILE_ID);
			if (fieldStr != null) {
				scanTemplateId = Long.valueOf(fieldStr);
				fieldRequest.setScanTemplateId(scanTemplateId);
			}

			// obtenemos los parametros del plugin para chequear, si no existe
			// saltará una exception y será recogida
			if (scanTemplateId != null && orgUnitId != null) {
				res = scanDocsService.extractPluginParam(scanTemplateId, orgUnitId);
			}
		} catch (NumberFormatException e) {
			// No hacer nada, ya que devolveremos properties vacio
			LOG.debug("No existe ningun parametro asociado a {} en la lista de parametros recibidos", fieldRequest);
		}
		return res;
	}

	/**
	 * Funcion para validar los parametros que nos llegan a través de la captura
	 * y rellenar fieldRequest (PerformScanDocVO).
	 *
	 * @param allRequestParams
	 *            parámetros de la petición web.
	 * @param images
	 *            imágenes a procesar.
	 * @param errores
	 *            parámetro de entrada salida donde son informados los errores
	 *            detctados sobre los parámetros de entrada.
	 * @param fieldRequest
	 *            Objeto de la capa de servicios donde se recopilan los
	 *            parámetros de la petición web.
	 * @param pluginParams
	 *            parámetros de configuración del plugin de captura.
	 * @param user
	 *            usuario que solicita el procesamiento.
	 * @param request
	 *            Petición web.
	 * @return Indica si los parametros recibidos son correctos (true). En caso
	 *         contrario, false.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */

	private Boolean validateParam(Map<String, String> allRequestParams, Map<String, MultipartFile> images, Map<String, String> errores, PerformScanDocVO fieldRequest, Properties pluginParams, UserInSession user, HttpServletRequest request) throws DAOException {
		LOG.debug("[WEBSCAN-SCANDOCS-CONTR]validateParam Start");
		Long t0;
		Long tf;
		Boolean found;
		Boolean searchSpacersDisabled = Boolean.FALSE;
		Boolean res = Boolean.FALSE;
		String fieldNow = null;
		String fieldNowValue = null;
		String docBatEnParVal;
		String paramEnable = null;
		String paramValues = null;

		t0 = System.currentTimeMillis();

		// recuperamos todos los campos
		try {
			/* 
			 * Comprobamos por cada campo los elementos obligatorios y que
			 * coincidan con la información previamente configurada en el plugin.
			 * Obtenemos los campos necesarios para poder verificar el resto de contenido de la request.
			 */

			// verificamos unidad organizativa
			if (fieldRequest.getOrgUnitId() == null) {
				infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_ORG_UNIT_ID);
			}

			// verificamos perfil de escaneo
			if (fieldRequest.getScanTemplateId() == null) {
				infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_SCANPROFID);
			}

			// idTypeDoc, tipo de documento
			found = Boolean.FALSE;
			if (fieldRequest.getScanTemplateId() != null && fieldRequest.getOrgUnitId() != null) {
				Long idTypeDoc = Long.valueOf(allRequestParams.get(ID_TYPE_DOC));
				List<DocumentSpecificationDTO> listDocSpecAux = getListDocSpecficationActiveByScanProfileOrgUnitId(fieldRequest.getScanTemplateId(), fieldRequest.getOrgUnitId());

				for (DocumentSpecificationDTO aux: listDocSpecAux) {
					if (aux.getId() == idTypeDoc) {
						found = Boolean.TRUE;
						fieldRequest.setIdTypeDoc(idTypeDoc);
						break;
					}
				}

				// si no encontrado saltamos exception para recoger mensaje
				if (!found) {
					infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_TYPE_DOC);
				}
			}

			String result = null;
			// idOutputFormat, formato de salida
			result = checkInfoParam(allRequestParams.get(ID_OUTPUT_FORMAT), errores, fieldRequest, pluginParams, SCAN_FILEFO_EN, SCAN_FILEFO_VAL, SCAN_FILEFO_VDEF, LABEL_OUTFORMAT);
			if (result != null) {
				fieldRequest.setIdOutputFormat(result);
			}

			// Se comprueba que el usuario solicitante pertenece a una
			// unidad orgánica asociada al colectivo de usuarios
			// informado en la petición.
			List<DepartamentVO> userDptos = (List<DepartamentVO>) request.getSession(false).getAttribute(WebscanBaseConstants.USER_DPTOS_SESSION_ATT);
			if (userDptos != null && !userDptos.isEmpty()) {
				fieldNow = USER_DPTO_ID;
				fieldNowValue = allRequestParams.get(fieldNow);
				if (fieldNowValue != null && !fieldNowValue.isEmpty()) {
					if (fieldNowValue.equals("-1")) {
						infoErrorParamScanDoc(errores, MES_USERDPTO_REQUIRED, LABEL_USER_DPTO);
					} else {
						if (isFormDptoInUserDptos(fieldNowValue, user, fieldRequest.getOrgUnitId())) {
							fieldRequest.setUserDptoId(fieldNowValue);
						} else {
							infoErrorParamScanDoc(errores, MES_USERDPTO_NOTASSOCIATED, LABEL_USER_DPTO);
						}
					}
				}
			}
			// idOutputResolution, resolución de salida
			result = checkInfoParam(allRequestParams.get(ID_OUTPUTRES), errores, fieldRequest, pluginParams, SCAN_RES_EN, SCAN_RES_VAL, SCAN_RES_VAL_DEF, LABEL_RESOLUTION);
			if (result != null) {
				try {
					fieldRequest.setIdOutputResolution(Long.valueOf(result));
				} catch (NumberFormatException n) {
					infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_RESOLUTION);
				}
			}
			// idOutputPixelType, tipo de pixel de salida
			checkInfoParamOutputPixel(allRequestParams.get(ID_OUTPUTPIXTYPE), errores, fieldRequest, pluginParams);

			// Se comprueba que las imágenes capturadas son correctas en número
			// y formato de archivo
			checkAcquiredImages(images, PixelType.getByDeepBitsName(fieldRequest.getIdOutputPixelType()), errores);

			// comprobamos si está habilitada el campo que permite desactivar la
			// detección de carátulas y separadores
			paramEnable = scanDocsService.searchParam(pluginParams, SPACER_SEARCH_DISABLED_ENA);
			if (ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(paramEnable)) {
				fieldNow = SPACER_SEARCH_DISA_INPUT_NAME;
				fieldNowValue = allRequestParams.get(fieldNow);
				searchSpacersDisabled = ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(fieldNowValue);
			} else {
				paramValues = scanDocsService.searchParam(pluginParams, SPACER_SEARCH_DISABLED_DEF_VALUE);
				searchSpacersDisabled = ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(paramValues);
			}
			fieldRequest.setSpacerSearchDisabled(searchSpacersDisabled);

			// comprobar tipo de separador entre documentos
			// No es requerido, si se indica que no se procesen separadores
			fieldNow = IDDOCBATDOCSPATY;
			fieldNowValue = allRequestParams.get(fieldNow);
			paramValues = scanDocsService.searchParam(pluginParams, SC_DCBATDCSPA_TY);
			if (fieldNowValue != null && !fieldNowValue.isEmpty() && !fieldNowValue.equals("-1")) {
				if (!paramValues.contains(fieldNowValue)) {
					infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_DOCSPATYPE);
				} else {
					fieldRequest.setIdDocSpacerType(fieldNowValue);
				}
			} else {
				// No ha sido informado el tipo de separador
				if (!searchSpacersDisabled) {
					infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_DOCSPATYPE);
				}
			}

			// comprobamos si está habilitado el tratamiento por lotes
			docBatEnParVal = scanDocsService.searchParam(pluginParams, SC_DOCBATEN);
			if (ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(docBatEnParVal)) {
				// Si se encuentra habilitado el tratamiento de lotes, se
				// verifican las validaciones sobre lotes:
				// Número documentos del lote y número páginas por documento.

				// enableNumberDoc
				fieldNow = IDDCBATVNUMDCSEN;
				fieldNowValue = allRequestParams.get(fieldNow);
				paramEnable = scanDocsService.searchParam(pluginParams, SC_DCBATVALNDCS);
				fieldRequest.setEnableNumberDoc(Boolean.FALSE);
				if (ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(paramEnable) && ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(fieldNowValue)) {
					/**
					 * Configurada la validación de número de documentos por
					 * lote, se recupera el número de documentos indicado en el
					 * interfaz de captura
					 */
					fieldNow = IDDCBATVALNUMDCS;
					fieldNowValue = allRequestParams.get(fieldNow);
					try {
						Long batchSize = Long.valueOf(fieldNowValue);
						if (batchSize <= 0) {
							throw new NumberFormatException();
						}
						fieldRequest.setIdDocBatchValNumDocs(batchSize);
						fieldRequest.setEnableNumberDoc(Boolean.TRUE);
					} catch (NumberFormatException e) {
						infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_DBATVNDOCS);
					}
				} else if (!ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(paramEnable) && ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(fieldNowValue)) {
					infoErrorParamScanDoc(errores, MES_PAR_NOT_WAIT, LABEL_DBATVNDOCS);
				}

				// idDocValNumPagsDocEnable
				fieldNow = IDDCVALNPAGSDCEN;
				fieldNowValue = allRequestParams.get(fieldNow);
				paramEnable = scanDocsService.searchParam(pluginParams, SC_DC_VAL_NPAGSDC);
				fieldRequest.setEnableNumberPagDoc(Boolean.FALSE);
				if (ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(paramEnable) && ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(fieldNowValue)) {
					fieldNow = IDDCVALNUMPAGSDC; // recuperamos el valor
					fieldNowValue = allRequestParams.get(fieldNow);
					try {
						Long numPages = Long.valueOf(fieldNowValue);

						if (numPages <= 0) {
							throw new NumberFormatException();
						}
						fieldRequest.setIdDocValNumPagsDoc(numPages);
						fieldRequest.setEnableNumberPagDoc(Boolean.TRUE);
					} catch (NumberFormatException e) {
						infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_DVNPAGSDOC);
					}
				} else if (!ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(paramEnable) && ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(fieldNowValue)) {
					infoErrorParamScanDoc(errores, MES_PAR_NOT_WAIT, LABEL_DVNPAGSDOC);
				}
			}
			// comprobamos la información correspondiente a la reserva, si esta
			// es informada
			fieldNow = SCAN_BOOK_ID;
			fieldNowValue = allRequestParams.get(fieldNow);
			if (fieldNowValue != null && !fieldNowValue.equals(NULL)) {
				fieldRequest.setScanBookId(Long.valueOf(fieldNowValue));

				fieldNow = DATE_BOOKING_OK;
				fieldNowValue = allRequestParams.get(fieldNow);
				if (fieldNowValue == null || fieldNowValue.equals(NULL)) {
					infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_DATEBOOKOK);
				} else {
					try {
						fieldRequest.setDateBookingOK(new Date(Long.valueOf(fieldNowValue)));
					} catch (NumberFormatException e) {
						infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_DATEBOOKOK);
					}
				}
			}

			// marcamos error
			if (errores == null || errores.isEmpty()) {
				res = Boolean.TRUE;
			} else {
				// marcamos error
				fieldRequest.setError(Boolean.TRUE);
			}

		} catch (Exception e) {
			fieldRequest.setError(Boolean.TRUE);
			// preparamos el error a informar
			fieldRequest.setFieldError(EnumErrorPerformScanDocVO.getByName(fieldNow).getMessage());
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-SCANDOCS-CONTR]validateParam Petición {}. Validar los parametros que nos llegan a través de la captura finalizado en {} ms.", fieldRequest.getRequestId(), (tf - t0));
		}

		LOG.debug("[WEBSCAN-SCANDOCS-CONTR]validateParam End");
		return res;
	}

	/**
	 * Comprueba si la unidad orgánica informada mediante el formulario se
	 * encuentra en la relación de unidades orgánicas del usuario solicitante,
	 * para un determinado colectivo.
	 * 
	 * @param userDptoId
	 *            identificador unidad orgánica informado en el formulario.
	 * @param user
	 *            usuario solicitante.
	 * @param orgUnitId
	 *            Identificador de colectivo de usuarios.
	 * @return Si la unidad orgánica informada se encuentra en la lista de
	 *         unidades orgánicas a las que pertence el usuario para el
	 *         colectivo especificado como parámetro de entrada, devuelve true.
	 *         En otro caso, devuelve false.
	 */
	private Boolean isFormDptoInUserDptos(String userDptoId, UserInSession user, Long orgUnitId) {
		Boolean res = Boolean.FALSE;

		List<DepartamentVO> userDptos = BaseControllerUtils.getUserFunctionalOrgsByOrgUnit(user, orgUnitId);

		for (Iterator<DepartamentVO> it = userDptos.iterator(); !res && it.hasNext();) {
			DepartamentVO dpto = it.next();
			res = dpto.getId().equals(userDptoId);
		}

		return res;
	}

	/**
	 * Comprueba que las imágenes capturadas son válidas tanto en número (al
	 * menos debe ser una), como formato de archivo. Si el tipo de píxel es B/N
	 * solo debe haber sido informada una imagen en formato TIFF. En otro caso,
	 * pueden ser informadas una o más imágenes en formato JPEG.
	 * 
	 * @param images
	 *            imágenes capturadas.
	 * @param pixelType
	 *            tipo de píxel (B/N, escala de grises o RGB).
	 * @param errores
	 *            lista de errores de validación del formulario.
	 */
	private void checkAcquiredImages(Map<String, MultipartFile> images, PixelType pixelType, Map<String, String> errores) {
		Boolean found;
		MultipartFile image;
		String mimeType;

		if (images == null || images.size() == 0) {
			infoErrorParamScanDoc(errores, MES_IMAGES_EMPTY, null);
		} else {
			switch (pixelType) {
				case BW:
					if (images.size() > 1) {
						infoErrorParamScanDoc(errores, MES_IMAGES_SIZE_ERROR, null);
					}
					// El content-type es application/octet-stream, no se
					// comprueba hasta una etapa posterior que sea una imagen
					// TIFF
					break;
				case GRAY:
				case RGB:
					mimeType = MimeType.JPG_IMG_CONTENT.getType();
					found = Boolean.FALSE;
					for (Iterator<String> it = images.keySet().iterator(); !found && it.hasNext();) {
						image = images.get(it.next());
						found = !mimeType.equals(image.getContentType());
					}

					if (found) {
						// Se encontró una imagen con un formato diferente al
						// esperado (JPEG).
						infoErrorParamScanDoc(errores, MES_IMAGES_MIMETYPE_ERROR, null);
					}
					break;
			}
		}
	}

	/**
	 * Método para validar que la información asociada al parametro es correcta.
	 * 
	 * @param fieldNowValue
	 *            Valor actual del campo.
	 * @param errores
	 *            Listado con los errores previos producidos
	 * @param fieldRequest
	 *            Objeto con la información que obtenemos los parámetros
	 *            validados
	 * @param pluginParams
	 *            Properties con los valores previamente establecido
	 * @param pEnable
	 *            String que identifica la constante para recuperar si el
	 *            parámetro está o no habilitado
	 * @param pValues
	 *            String que identifica la constante para recuperar los posibles
	 *            valores del parámetro
	 * @param pValuesDefault
	 *            String que identifica la constante para recuperar el valor por
	 *            defecto del parámetro
	 * @param pLabel
	 *            String que identifica la constante de la etiqueta de la vista
	 *            del parámetro
	 * @return res String con el valor asignar al parámetro
	 */

	private String checkInfoParam(String fieldNowValue, Map<String, String> errores, PerformScanDocVO fieldRequest, Properties pluginParams, String pEnable, String pValues, String pValuesDefault, String pLabel) {
		String res = null;
		String paramEnable = scanDocsService.searchParam(pluginParams, pEnable);
		String paramValues = scanDocsService.searchParam(pluginParams, pValues);
		String paramDefault = scanDocsService.searchParam(pluginParams, pValuesDefault);

		if (ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(paramEnable)) {
			// comprobamos que no exista
			if (!paramValues.contains(fieldNowValue)) {
				infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, pLabel);
			} else {
				res = fieldNowValue;
			}
		} else {
			// si no está habilitado no debería aparecer en la llamada, en
			// caso contrario lo usamos para poner el valor por defecto
			if (!fieldNowValue.isEmpty()) {
				infoErrorParamScanDoc(errores, MES_PAR_NOT_WAIT, pLabel);
			} else {
				// ponemos el valor por defecto
				if (paramDefault == null) {
					infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, pLabel);
				} else {
					res = paramDefault;
				}

			}
		}
		return res;
	}

	/**
	 * Método para validar que la información asociada al OutputPixel es
	 * correcta.
	 * 
	 * @param fieldNowValue
	 *            Valor actual del campo.
	 * @param errores
	 *            Listado con los errores previos producidos
	 * @param fieldRequest
	 *            Objeto con la información que obtenemos los parámetros
	 *            validados
	 * @param pluginParams
	 *            Properties con los valores previamente establecido
	 */

	private void checkInfoParamOutputPixel(String fieldNowValue, Map<String, String> errores, PerformScanDocVO fieldRequest, Properties pluginParams) {

		// recuperamos la información de Pixel Type y la validamos
		String paramEnable = scanDocsService.searchParam(pluginParams, SC_PT_ENAB);
		String paramValues = scanDocsService.searchParam(pluginParams, SC_PT_VAL);
		String paramDefault = scanDocsService.searchParam(pluginParams, SC_PT_VAL_DEF);
		fieldRequest.setIdOutputPixelType(null);
		if (ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(paramEnable)) {
			// comprobamos que al no ser obligatorio,le ponemos el valor por
			// defecto
			if (fieldNowValue.equals("-1")) {
				// ponemos el valor por defecto
				fieldRequest.setIdOutputPixelType(paramDefault);
			} else if (!paramValues.contains(fieldNowValue)) {
				infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_PIXELTYPE);
			} else {
				fieldRequest.setIdOutputPixelType(fieldNowValue);
			}
		} else {
			// si no está habilitado no debería aparecer en la llamada, en
			// caso contrario lo usamos para poner el valor por defecto
			if (!fieldNowValue.isEmpty()) {
				infoErrorParamScanDoc(errores, MES_PAR_NOT_WAIT, LABEL_PIXELTYPE);
			} else {

				// ponemos el valor por defecto
				if (paramDefault == null) {
					infoErrorParamScanDoc(errores, MES_PARVAL_NOTVAL, LABEL_PIXELTYPE);
				} else {
					fieldRequest.setIdOutputPixelType(paramDefault);
				}
			}
		}

	}

	/**
	 * Funcion auxiliar para lanzar el error en la validación de parametros de
	 * scanDocs.
	 * 
	 * @param errores
	 *            Listado con los errores actuales
	 * @param error
	 *            Nuevo error añadir
	 * @param field
	 *            Campo que provoca el error
	 */
	private void infoErrorParamScanDoc(Map<String, String> errores, final String error, final String field) {
		LOG.debug("[WEBSCAN-SCANDOCS-CONTR]infoErrorParamScanDoc ...");
		/**
		 * añadimos el error y opcionalmente el campo relacionado con el mimsmo
		 */
		errores.put(error, (field == null ? "" : field));

		LOG.debug("[WEBSCAN-SCANDOCS-CONTR]infoErrorParamScanDoc ...");
	}
}
