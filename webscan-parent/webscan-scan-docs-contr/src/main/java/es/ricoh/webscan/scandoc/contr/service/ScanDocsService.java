/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.service.ScanDocsService.java.</p>
 * <b>Descripción:</b><p> Implementación de la lógica de negocio del plugin de captura de imágenes de documentos digitalizados.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH España IT Services.
 * @version 1.0.
 */

package es.ricoh.webscan.scandoc.contr.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.web.multipart.MultipartFile;

import es.ricoh.webscan.spacers.business.service.SpacersGenerator;
import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.model.domain.DocSpacerType;
import es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerDocDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerMetadataDTO;
import es.ricoh.webscan.spacers.repository.SpacersDAOException;
import es.ricoh.webscan.spacers.repository.SpacersModelManager;
import es.ricoh.webscan.statistics.model.domain.ScanWorkingMode;
import es.ricoh.webscan.statistics.model.dto.DocScanStatisticsDTO;
import es.ricoh.webscan.statistics.repository.StatisticsDAOException;
import es.ricoh.webscan.statistics.repository.StatisticsModelManager;
import es.ricoh.webscan.WebscanConstants;
import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.vo.DepartamentVO;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.UserOrgUnitScanProfiles;
import es.ricoh.webscan.base.controller.vo.OrgUnitScanProfileVO;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.domain.OperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.dto.BatchDTO;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.ParameterDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.model.service.AuditService;
import es.ricoh.webscan.scandoc.contr.ScanDocsConstants;
import es.ricoh.webscan.scandoc.contr.ScanDocsException;
import es.ricoh.webscan.scandoc.contr.controller.vo.PerformScanDocVO;
import es.ricoh.webscan.scandoc.contr.controller.vo.PixelType;
import es.ricoh.webscan.scandoc.contr.util.BarCodeResult;
import es.ricoh.webscan.scandoc.contr.util.ImageProcessor;
import es.ricoh.webscan.scandoc.contr.util.ImageProcessorFactory;
import es.ricoh.webscan.scandoc.contr.util.ScannedImageType;
import es.ricoh.webscan.utilities.MimeType;
import es.ricoh.webscan.utilities.Utils;

/**
 * Implementación de la lógica de negocio del plugin de captura de imágenes de
 * documentos digitalizados.
 * <p>
 * Clase ScanDocsService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ScanDocsService {

	/**
	 * Expresión regular que define como es definido el parámetro que especifica
	 * el tamaño máximo de los documentos.
	 */
	private static final Pattern MAX_FILE_DOC_PATTERN = Pattern.compile("^(\\d+)(\\,\\d+)?\\s*([GMK]b)$", java.util.regex.Pattern.CASE_INSENSITIVE);

	/**
	 * Constante identifica el retorno de carro en HTML.
	 */
	private static final String RETURN_HTML = "<br>";

	/**
	 * Constante que identifica los metadatos asociados a la unidad organizativa
	 * en el plugin de escaneo.
	 */
	private static final String SCDC_META_ORG = "scan.doc.metadata.organization";
	/**
	 * Constante que identifica los metadatos asociados a la unidad organizativa
	 * en el plugin de escaneo.
	 */
	private static final String SCDC_META_BDEEP = "scan.doc.metadata.byteDeep";
	/**
	 * Constante que identifica los metadatos asociados a los bytes de
	 * profundida en el plugin de escaneo.
	 */
	private static final String SCDC_META_RES = "scan.doc.metadata.resolution";
	/**
	 * Constante que identifica los metadatos asociados a la resolucion en el
	 * plugin de escaneo.
	 */
	private static final String SCDC_META_VER = "scan.doc.metadata.format.version";
	/**
	 * Constante que identifica los metadatos asociados a la version del formato
	 * en el plugin de escaneo.
	 */
	private static final String SCDC_META_EXT = "scan.doc.metadata.format.extension";
	/**
	 * Constante que identifica los metadatos asociados al nombre del formato en
	 * el plugin de escaneo.
	 */
	private static final String SCDC_META_NAME = "scan.doc.metadata.format.name";
	/**
	 * Constante que identifica los metadatos asociados a la fecha de escaneo en
	 * el plugin de escaneo.
	 */
	private static final String SCDC_META_INDATE = "scan.doc.metadata.inDate";
	/**
	 * Constante que identifica el parámetro que define el tamaño máximo de los
	 * documentos digitalizados.
	 */
	private static final String SCDC_DOC_MAX_SIZE = "scan.doc.size.max";

	/**
	 * Constante que identifica formato de tipo TIFF.
	 */
	private static final String TIFF_FORMAT = "TIFF";
	/**
	 * Constante que identifica formato de tipo PNG.
	 */
	private static final String PNG_FORMAT = "PNG";
	/**
	 * Constante que identifica formato de tipo JPEG.
	 */
	private static final String JPEG_FORMAT = "JPEG";
	/**
	 * Constante que identifica formato de tipo PDF.
	 */
	private static final String PDF_FORMAT = "PDF";
	/**
	 * Constante usada para formar el mensaje de error asociada a errores de
	 * acceso a BBDD normalmente.
	 */
	private static final String ME_WS_GV_SD_SERV = "[WEBSCAN-SCANDOCS-SERV] {}";
	/**
	 * Parámetros por defecto del plugin de digitalización.
	 */
	private Properties scanDocsConfParams;
	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScanDocsService.class);
	/**
	 * Identificativo de la fase de escaneo.
	 */
	private static final String SCAN = "Scan";
	/**
	 * identificativo del parametro que recoge información de los metadatos de
	 * la fase de escaneo.
	 */
	private static final String SCAN_DOC_META = "scan.doc.metadata";

	/**
	 * Constante que representa el mensaje de error por número de documentos del
	 * lote diferente al establecido en la interfaz o carátula.
	 */
	private static final String DOC_BATSIZE_ER = "error.batchSize.msg";

	/**
	 * Constante que representa el mensaje de error por número de páginas por
	 * documento de un lote diferente al establecido en la interfaz o carátula.
	 */
	private static final String BATNUMPAG_ER = "error.batchNumPages.msg";

	/**
	 * Constante que representa el mensaje de error por tipo de separador
	 * diferente al establecido en la carátula o la interfaz.
	 */
	private static final String DC_BATSPATY_ER = "error.batchSpacerType.msg";

	/**
	 * Constante que representa el mensaje de error por perfil de digitalización
	 * diferente al establecido en la carátula o reserva.
	 */
	private static final String DC_BATSCPROF_ER = "error.scanProfileNotMacth.msg";

	/**
	 * Constante que representa el mensaje de error por definición de documentos
	 * diferente a la establecida en la carátula o reserva.
	 */
	private static final String DC_BATDCSP_ER = "error.docSpecNotMacth.msg";

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Objeto que representa el acceso a los servicio del modelo.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Objeto que representa el acceso a los servicio de auditoria.
	 */
	private AuditService auditService;

	/**
	 * Objeto que representa el acceso a los servicio del modelo de datos de los
	 * separadores.
	 */
	private SpacersModelManager spacersModelManager;

	/**
	 * Objeto que habilita la ejecución asíncrona del proceso de digitalización.
	 */
	private AsyncScanProcessManager asyncScanProcessManager;

	/**
	 * Clase de utilidades que habilita el acceso a los parámetros de
	 * configuración del sistema.
	 */
	private Utils webscanUtilParams;

	/**
	 * Objeto que representa el acceso a los servicio del modelo de datos de las
	 * estadísticas.
	 */
	private StatisticsModelManager statisticsModelManager;

	/**
	 * Obtiene un map con la relacion de documentacion especifica y su relacion
	 * de perfil y unidad organizativa.
	 * 
	 * @param scanProfileId
	 *            id del perfil de escaneo
	 * @param orgUnitId
	 *            id de la unidad organizativa
	 * @return Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> mapa
	 *         asociado a la documentacion especifica y el perfil asociado a la
	 *         unidad organizativa y documentacion
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */

	public Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getDocSpecificationsByScanProfileOrgUnitId(Long scanProfileId, Long orgUnitId) throws DAOException {
		// obtenemos la entidad que relaciona perfil y unidad organizativa
		ScanProfileOrgUnitDTO scanProfileOrgUnit = webscanModelManager.getScanProfileOrgUnitByScanProfileAndOrgUnit(scanProfileId, orgUnitId);

		// recuperamos el mapa según el identificativo de la entidad recuperada
		return webscanModelManager.getDocSpecificationsByScanProfileOrgUnitId(scanProfileOrgUnit.getId());

	}

	/**
	 * Obtiene el enumerado asociado al nombre de la etapa pasada como
	 * parámetro.
	 * 
	 * @param name
	 *            Identificador de la etapa.
	 * @return ScanProcessStageDTO Entidad asociada a la etapa de
	 *         digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */

	public ScanProcessStageDTO getScanProcStageByName(final String name) throws DAOException {
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] Se validan los parámetros de entrada ...");
		return webscanModelManager.getScanProcStageByName(name);
	};

	/**
	 * Obtenemos la entidad asociada al perfil de escaneo y unidad organizativa
	 * del plugin configurado.
	 * 
	 * @param scanProfileId
	 *            Identificador del perfil de escaneo.
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @param scanProcessStageId
	 *            Fase de la que deseamos recuperar el plugin.
	 * @return Nos devuelve la entidad asociada al plugin del perfil de escaneo
	 *         y unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(final Long scanProfileId, final Long orgUnitId, final Long scanProcessStageId) throws DAOException {

		LOG.debug("[WEBSCAN-SCANDOCS-SERV] Recuperamos ScanProfileOrgUnitPlugin...");
		return webscanModelManager.getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(scanProfileId, orgUnitId, scanProcessStageId);
	}

	/**
	 * Obtiene la configuración de un perfil de digitalización para una unidad
	 * organizativa.
	 * 
	 * @param scanTemplateId
	 *            Identificador del perfil de digitalización.
	 * @param orgUnitId
	 *            Identificador de la unidad organizativa.
	 * @return La configuración de un perfil de digitalización para una unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public ScanProfileOrgUnitDTO getScanProfileOrgUnitByScanProfileAndOrgUnit(final Long scanTemplateId, final Long orgUnitId) throws DAOException {
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] getScanProfileOrgUnitByScanProfileAndOrgUnit ...");
		return webscanModelManager.getScanProfileOrgUnitByScanProfileAndOrgUnit(scanTemplateId, orgUnitId);
	}

	/**
	 * Método que obtiene la especificación del plugin por perfil de escaneo,
	 * unidad organizativa y etapa.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificativo de la relacion entre perfil de escaneo y unidad
	 *            organizativa.
	 * @param scanProcessStageName
	 *            Cadena con la identificacion correspondiente a la etapa de
	 *            proceso de digitalizacion.
	 * @return Entidad asociada especificacion de perfil correspondiente a la
	 *         relacion de perfil de escaneo y unidad organizativa de la etapa
	 *         especificada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */

	public PluginSpecificationDTO getPluginSpecByScanProfileOrgUnitAndStage(final Long scanProfileOrgUnitId, final String scanProcessStageName) throws DAOException {
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] getPluginSpecByScanProfileOrgUnitAndStage ...");
		return webscanModelManager.getPluginSpecByScanProfileOrgUnitAndStage(scanProfileOrgUnitId, scanProcessStageName);
	}

	/**
	 * Obtenemos el pluginDTO correspondiente a la Fase de Escaneo según el
	 * identificador del perfil de escaneo y la unidad organizativa pasada como
	 * parametros.
	 * 
	 * @param scanProfileIdL
	 *            Identificador del perfil de escaneo.
	 * @param orgUnitIdL
	 *            Identificador de la unidad organizativa.
	 * @return ScanProfileOrgUnitPluginDTO entidad correspondiente a la
	 *         configuración de la fase de escaneo.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de la base de
	 *             datos.
	 */
	public ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPluginDTOByScan(final Long scanProfileIdL, final Long orgUnitIdL) throws DAOException {
		ScanProfileOrgUnitPluginDTO scProfOrgUnitPl = null;
		try {
			// recuperamos el objecto de la fase de escaneo
			ScanProcessStageDTO stageCapture = getScanProcStageByName(SCAN);
			scProfOrgUnitPl = getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(scanProfileIdL, orgUnitIdL, stageCapture.getId());

		} catch (DAOException e) {
			/*
			 * si hay un error de BBDD que no sea que no existe ningún plugin configurado para escaneo
			 */
			if (!DAOException.CODE_901.equals(e.getCode())) {
				throw e;
			}
		}
		return scProfOrgUnitPl;
	}

	/**
	 * Obtiene el listado de parametros del plugin según la etapa y el perfil
	 * con su unidad organizativa, permitiendo la paginación y ordenación del
	 * resultado.
	 * 
	 * @param scanProfileOrgUnit
	 *            perfil y unidad organizativa.
	 * @param scanProcessStageDTO
	 *            etapa del proceso.
	 * @return listado de parametros asociados al plugin de solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */

	public Properties getScanPluginParameters(ScanProfileOrgUnitDTO scanProfileOrgUnit, ScanProcessStageDTO scanProcessStageDTO) throws DAOException {
		List<ParameterDTO> pluginParameters;
		Properties res = new Properties();
		String paramName;
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] getPluginParametersByScanProfileOrgUnit ...");

		res.putAll(scanDocsConfParams);

		pluginParameters = webscanModelManager.getPluginParametersByScanProfileOrgUnit(scanProfileOrgUnit, scanProcessStageDTO);

		if (pluginParameters != null && !pluginParameters.isEmpty()) {
			for (ParameterDTO param: pluginParameters) {
				paramName = param.getSpecificationName();
				if (param.getValue() != null && !param.getValue().isEmpty()) {
					res.setProperty(paramName, param.getValue());
				}
			}
		}

		return res;
	}

	/**
	 * Obtiene el listado de parametros del plugin según la etapa y el perfil
	 * con su unidad organizativa, permitiendo la paginación y ordenación del
	 * resultado.
	 * 
	 * @param scanProfileOrgUnit
	 *            perfil y unidad organizativa
	 * @param scanProcessStageDTO
	 *            etapa del proceso
	 * @return listado de parametros asociados al plugin de solicitado
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */

	private List<ParameterDTO> getPluginParametersByScanProfileOrgUnit(final ScanProfileOrgUnitDTO scanProfileOrgUnit, final ScanProcessStageDTO scanProcessStageDTO) throws DAOException {
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] getPluginParametersByScanProfileOrgUnit ...");
		return webscanModelManager.getPluginParametersByScanProfileOrgUnit(scanProfileOrgUnit, scanProcessStageDTO);
	}

	/**
	 * Obtiene la entidad asociada al perfil de escaneo y unidad organizativa.
	 * 
	 * @param scanProfileId
	 *            id del perfil de escaneo.
	 * @return ScanProfileDTO Entidad asociada perfil.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public ScanProfileDTO getScanProfile(final Long scanProfileId) throws DAOException {
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] getScanProfile ...");
		return webscanModelManager.getScanProfile(scanProfileId);
	}

	/**
	 * Recupera una configuración en unidad organizativa de un perfil de
	 * digitalización a partir de su identificador.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración en unidad organizativa de un
	 *            perfil de digitalización.
	 * @return La configuración en unidad organizativa de un perfil de
	 *         digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             entidad solicitada, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	public ScanProfileOrgUnitDTO getScanProfileOrgUnit(Long scanProfileOrgUnitId) throws DAOException {
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] getScanProfileOrgUnit ...");
		return webscanModelManager.getScanProfileOrgUnit(scanProfileOrgUnitId);
	}


	/**
	 * Funcion que trata la recepción de todos los ficheros recibidos de la
	 * pantalla de captura. Es decir, registrará en Bas.
	 * 
	 * @param images
	 *            Imágenes capturadas.
	 * @param fieldRequest
	 *            Parametros recibidos de la captura.
	 * @param userSession
	 *            usuario que solicita el procesamiento de la imagen capturada.
	 * @param scanPluginParams
	 *            Parámetros de configuración de la instancia del plugin de
	 *            captura de imágenes establecida en el procesamiento actual.
	 * @param request
	 *            Petición web.
	 * @return listado de identificadores de los documentos digitalizados.
	 * @throws ScanDocsException
	 *             Si se produce algún error en el procesamiento de la imagen
	 *             capturada.
	 */

	public List<ScanDocResult> registreScanDocs(final Map<String, MultipartFile> images, final PerformScanDocVO fieldRequest, final UserInSession userSession, final Properties scanPluginParams, final HttpServletRequest request) throws ScanDocsException {
		LOG.debug("[WEBSCAN-GV-SCAN-DOCS] registreScanDocs Start...");
		BatchScanDoc bsd;
		DocsBatchSpacerDTO batchSpacer;
		List<ScanDocResult> res = new ArrayList<ScanDocResult>();
		String excMsg;
		Long t0;
		Long tf;

		t0 = System.currentTimeMillis();

		try {

			ImageProcessor imageProcessor = ImageProcessorFactory.getImageProcessor(ImageProcessor.getByteDeep(fieldRequest.getIdOutputPixelType()), messages, request.getLocale());

			// realizamos la división del fichero, y preparamos nuestro mapa con
			// toda la informacion de documentos
			Map<BatchScanDoc, List<ScannedDoc>> batchDoc = prepareScanDocs(images, fieldRequest, imageProcessor, userSession, scanPluginParams, request);
			// validaciones
			validationDocBatchs(batchDoc, fieldRequest.getRequestId(), request.getLocale());
			// registramos metadatos y documentos asociados, así como trazas y
			// auditorias. Creamos tambien el listado de documentos registrados
			res = registreDocsAndMetadata(batchDoc, images, fieldRequest, imageProcessor, userSession, request.getLocale());
			if (res == null || res.isEmpty()) {
				LOG.error("[WEBSCAN-GV-SCAN-DOCS] No encontrado los documentos");
				throw new ScanDocsException(ScanDocsException.CODE_007, "no encontrados documentos");
			}

			for (Iterator<BatchScanDoc> it = batchDoc.keySet().iterator(); it.hasNext();) {
				bsd = it.next();
				if (BatchType.SPACER.equals(bsd.getBatchType())) {
					LOG.debug("[WEBSCAN-GV-SCAN-DOCS] Finalizando la carátula {} ...", bsd.getSpacerId());
					batchSpacer = spacersModelManager.getGeneratedDocsBatchSpacer(bsd.getSpacerId());
					spacersModelManager.processBatchSpacer(batchSpacer.getId(), fieldRequest.getStartDate());
					spacersModelManager.completeDocsBatchSpacer(batchSpacer.getId());
				}
				registerStatistics(userSession, bsd, fieldRequest, batchDoc.get(bsd));
			}
		} catch (ScanDocsException e) {
			throw e;
		} catch (SpacersDAOException e) {
			excMsg = "Error finalizando el trabajo para una carátula. Error: " + e.getCode() + " - " + e.getMessage() + ".";
			LOG.error("[WEBSCAN-GV-SCAN-DOCS] {}", excMsg);
			throw new ScanDocsException(ScanDocsException.CODE_013, e.getMessage());// NOPMD
		} catch (Exception e) {
			excMsg = "Error inesperado finalizando la fase de captura. Error: " + e.getMessage() + ".";
			LOG.error("[WEBSCAN-GV-SCAN-DOCS] {}", excMsg);
			throw new ScanDocsException(ScanDocsException.CODE_999, excMsg);// NOPMD
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-SCANDOCS-SERV]RegistreScanDocs Petición {}. Proceso de captura de imágenes finalizado en {} ms.", fieldRequest.getRequestId(), (tf - t0));
		}

		LOG.debug("[WEBSCAN-GV-SCAN-DOCS] registreScanDocs End ...");
		return res;
	}

	/**
	 * Método que busca un parámetro de configuración en una lista de parámetros
	 * dada, y si no lo encuentra retorna el valor configurado a nivel de
	 * componente mediante los archivos de configuración.
	 * 
	 * @param listParam
	 *            relación de parámetros configurados para la instancia de
	 *            plugin.
	 * @param strSearch
	 *            denominación del parámetro a buscar.
	 * @return Valor del parámetro buscado si está configurado a nivel de
	 *         instancia de plugin o componente. En caso contrario devuelve
	 *         null.
	 */
	public String searchParam(Properties listParam, final String strSearch) {
		String res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SCANDOCS-SERV] Recuperando parámetro de configuración " + strSearch + " ...");
		}
		// recuperamos las properties del parámetro buscado
		res = listParam.getProperty(strSearch);

		if (res == null) {
			// Si no se encuentra en los parámetros configurados para la
			// instancia del plugin, se buscan en los parámetros por defecto del
			// componente

			res = scanDocsConfParams.getProperty(strSearch);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SCANDOCS-SERV] Valor recuperado para el parámetro " + strSearch + ": " + res + " .");
		}
		return res;
	}

	/**
	 * Método para extraer la información de los parametros del plugin de
	 * escaneo por perfil de escaneo y unidad organizativa.
	 * 
	 * @param scanProfileIdL
	 *            Identificador del perfil de escaneo.
	 * @param orgUnitIdL
	 *            Identificador de la unidad organizativa
	 * @return List<ParameterDTO> listado de parametros asociados al plugin
	 *         identificado por perfil de escaneo y unidad organizativa
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */
	public Properties extractPluginParam(final Long scanProfileId, final Long orgUnitId) throws DAOException {
		Properties res;
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] extractPluginParam Start");

		// obtenemos la etapa
		ScanProcessStageDTO stageCapture = getScanProcStageByName(SCAN);
		// obtenemos la identificacion de perfil y unidad organizativa
		ScanProfileOrgUnitDTO scanProfOrgUnit = webscanModelManager.getScanProfileOrgUnitByScanProfileAndOrgUnit(scanProfileId, orgUnitId);

		// listado de parametros del plugin de escaneo
		res = getScanPluginParameters(scanProfOrgUnit, stageCapture);

		LOG.debug("[WEBSCAN-SCANDOCS-SERV] extractPluginParam End");
		return res;
	}

	/**
	 * Realiza el registro de los documentos capturados en la digitalizacion en
	 * la BBDD del sistema.
	 * 
	 * @param batchDoc
	 *            Mapa con la informacion recibida captura de los documentos.
	 * @param images
	 *            Imágenes capturadas.
	 * @param fieldRequest
	 *            Parametros recibidos de captura.
	 * @param imageProcessor
	 *            Clase responsable del parseo y decodificación de las imágnes
	 *            capturadas.
	 * @param userSession
	 *            Usuario de session.
	 * @param locale
	 *            configuración regional de la petición web.
	 * @return Listado de identificadores de documentos registrados.
	 * @throws ScanDocsException
	 *             Exception producida al procesar el registro de los
	 *             documentos.
	 */
	private List<ScanDocResult> registreDocsAndMetadata(Map<BatchScanDoc, List<ScannedDoc>> batchDoc, final Map<String, MultipartFile> images, final PerformScanDocVO fieldRequest, final ImageProcessor imageProcessor, final UserInSession userSession, final Locale locale) throws ScanDocsException {
		List<ScanDocResult> res = new ArrayList<ScanDocResult>();
		List<ScannedDocumentDTO> newDocs = new ArrayList<ScannedDocumentDTO>();
		Map<Long, ScanDocResult> registerdDocs = new HashMap<Long, ScanDocResult>();
		Long auditOpId;
		long t0;
		long tf;
		long docMaxSize;
		ScannedDocumentDTO doc;
		BatchDTO batch;
		
		String batchReqId;
		String excMsg;

		LOG.debug("[WEBSCAN-GV-SCAN-DOCS] registreDocsAndMetadata Start ...");
		t0 = System.currentTimeMillis();
		try {
			Long scanProfOrgUnitDocSpecId = getScanProfOrgUnitDocSpecId(fieldRequest.getScanTemplateId(), fieldRequest.getOrgUnitId(), fieldRequest.getIdTypeDoc());
			Map<MetadataSpecificationDTO, MetadataDTO> metadataCollectionType = recoveryDocMetadataByTypeId(fieldRequest.getIdTypeDoc());

			// obtenemos la información de los metadatos de la fase de escaneo
			ScanProcessStageDTO stageCapture = getScanProcStageByName(SCAN);
			ScanProfileOrgUnitDTO scanProfileOrgUnit = getScanProfileOrgUnitByScanProfileAndOrgUnit(fieldRequest.getScanTemplateId(), fieldRequest.getOrgUnitId());
			List<ParameterDTO> parameters = getPluginParametersByScanProfileOrgUnit(scanProfileOrgUnit, stageCapture);

			docMaxSize = getDocMaxSizeAllowed(parameters);

			for (BatchScanDoc key: batchDoc.keySet()) {
				// a nivel de lote
				List<ScannedDoc> listScannedDoc = batchDoc.get(key);
				batchReqId = getBatchReqIdValue(key, userSession.getDocument());
				for (ScannedDoc scannedDoc: listScannedDoc) {
					ScanDocResult scanDocResult = new ScanDocResult();
					// preparamos metadataCollection de cada documento
					Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection = fillMetadataCollection(scannedDoc, userSession, metadataCollectionType);
					// rellenamos batch
					batch = fillBatchDTO(key, batchReqId, fieldRequest, scannedDoc, metadataCollection, imageProcessor, parameters, docMaxSize, userSession, locale);
					batch = webscanModelManager.createBatch(batch, scanProfOrgUnitDocSpecId);
					// rellenamos doc
					doc = fillScannedDocumentoDTO(key, batchReqId, images, fieldRequest, scannedDoc, metadataCollection, imageProcessor, parameters, docMaxSize, userSession, locale);
					//, batch);
					// El tamaño del documento es calculado una vez ha sido
					metadataCollection = fillSizeDocMetadata(fieldRequest, doc, metadataCollection, metadataCollectionType, parameters);
					// transformado a su formato final
					// registramos documentos y metadatos
					doc = webscanModelManager.createScannedDocument(doc, metadataCollection, scanProfOrgUnitDocSpecId, batch.getId());
					// registramos el documento como persistido
					scanDocResult.setDocId(doc.getId());
					scanDocResult.setEndDate(new Date());
					registerdDocs.put(doc.getId(), scanDocResult);
					// auditoria
					auditOpId = createScanDocAuditLogs(doc, userSession.getUsername());
					doc.setAuditOpId(auditOpId);
					newDocs.add(doc);
				}
			}
			webscanModelManager.updateScannedDoc(newDocs);
			createScanTraces(newDocs, fieldRequest, registerdDocs);
			res = new ArrayList<ScanDocResult>(registerdDocs.values());
		} catch (DAOException e) {
			excMsg = "Error al registrar los documentos y sus metadatos. Error: " + e.getMessage();
			LOG.error("[WEBSCAN-GV-SCAN_DOCS] {}", excMsg);
			throw new ScanDocsException(ScanDocsException.CODE_012, excMsg);// NOPMD
		} catch (ScanDocsException e) {
			LOG.error("[WEBSCAN-GV-SCAN_DOCS] {}", e.getMessage());
			throw e;// NOPMD
		} catch (Exception e) {
			excMsg = "Error al registrar los documentos y sus metadatos. Error: " + e.getMessage();
			LOG.error("[WEBSCAN-GV-SCAN_DOCS] {}", excMsg);
			throw new ScanDocsException(ScanDocsException.CODE_012, excMsg);// NOPMD
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-SCANDOCS-SERV] Registro en BBDD de los documentos digitalizados en la petición {} realizada en {} ms.", fieldRequest.getRequestId(), (tf - t0));
		}

		LOG.debug("[WEBSCAN-GV-SCAN-DOCS] registreDocsAndMetadata End ...");
		return res;
	}

	/**
	 * Fusiona para un documento digitalizado los metadatos obtenidos del
	 * proceso de digitalización, los establecidos a nivel de lote y los
	 * configurados por defecto.
	 * 
	 * @param scannedDoc
	 *            Documento digitalizado.
	 * @param metadataCollectionType
	 *            Relación de metadatos definidos para el tipo documental del
	 *            documento seleccionado por el usuario de digitalización.
	 * @return Relación actualizada de metadatos del documento, incluyendo los
	 *         metadatos obtenidos del proceso de digitalización, los
	 *         establecidos a nivel de lote y los configurados por defecto.
	 */
	private Map<MetadataSpecificationDTO, MetadataDTO> fillMetadataCollection(ScannedDoc scannedDoc, UserInSession userSession, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollectionType) {
		Map<MetadataSpecificationDTO, MetadataDTO> res = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		Map<String, String> metadata = null;
		MetadataDTO metadataDTO;
		String valor;
		// recuperamos los metadatos del principal
		metadata = scannedDoc.getMetadata();

		// Se inicializa el resultado con los metadatos que poseen valor por
		// defecto
		for (MetadataSpecificationDTO specMetadata: metadataCollectionType.keySet()) {
			if (specMetadata.getDefaultValue() != null && !specMetadata.getDefaultValue().trim().isEmpty()) {
				metadataDTO = metadataCollectionType.get(specMetadata);
				metadataDTO.setValue(specMetadata.getDefaultValue());
				res.put(specMetadata, metadataDTO);
			}
		}

		// Se añaden los obtenidos de la captura
		if (metadata != null) {
			for (String key: metadata.keySet()) {
				valor = metadata.get(key);
				// buscamos si existe
				for (MetadataSpecificationDTO specMetadata: metadataCollectionType.keySet()) {
					if (specMetadata.getName().equals(key)) {
						// añadimos a la nueva metacollection
						metadataDTO = metadataCollectionType.get(specMetadata);
						metadataDTO.setValue(valor);
						res.put(specMetadata, metadataDTO);
						break;
					}
				}
			}
		}

		return res;
	}

	/**
	 * Establece los metadatos correspondientes al tamaño del documento.
	 * 
	 * @param fieldRequest
	 *            parámetros de la petición web.
	 * @param doc
	 *            documento digitalizado.
	 * @param docMetadata
	 *            Relación de metadatos ya calculados para el documento.
	 * @param specMetadataColl
	 *            Relación de definiciones de metados asignables al documento.
	 * @param parameters
	 *            relación de parámetros del plugin de captura de imágenes de
	 *            documentos para el perfil de digitalización establecido.
	 * @return La relación de metadatos del documento, incluyendo el tamaño.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos.
	 */
	private Map<MetadataSpecificationDTO, MetadataDTO> fillSizeDocMetadata(final PerformScanDocVO fieldRequest, final ScannedDocumentDTO doc, Map<MetadataSpecificationDTO, MetadataDTO> docMetadata, Map<MetadataSpecificationDTO, MetadataDTO> specMetadataColl, final List<ParameterDTO> parameters) throws DAOException {
		Boolean found;
		Map<MetadataSpecificationDTO, MetadataDTO> res;
		MetadataSpecificationDTO metadataSpec;
		MetadataDTO metadata;
		String[ ] metadataNames;
		String pluginParamValue;
		String docSize;

		// Se extraen los parámetros scan.doc.metadata....
		Map<String, String> paramScan = getScanMetadataByParam(parameters);

		res = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		if (docMetadata != null && !docMetadata.isEmpty()) {
			res.putAll(docMetadata);
		}

		// scan.doc.metadata.size
		pluginParamValue = paramScan.get("scan.doc.metadata.size");
		docSize = humanReadableByteCount(Long.parseLong(Integer.toString(doc.getContent().length)));

		if (pluginParamValue != null) {
			metadataNames = pluginParamValue.split(",");

			for (int i = 0; i < metadataNames.length; i++) {
				found = Boolean.FALSE;
				for (Iterator<MetadataSpecificationDTO> it = specMetadataColl.keySet().iterator(); !found && it.hasNext();) {
					metadataSpec = it.next();
					if (metadataSpec.getName().equals(metadataNames[i])) {
						metadata = specMetadataColl.get(metadataSpec);
						metadata.setValue(docSize);
						res.put(metadataSpec, metadata);
						found = Boolean.TRUE;
					}
				}
			}
		}

		return res;
	}

	/**
	 * Obtiene la información en formato entendible en bytes del parámetro
	 * pasado en bytes.
	 * 
	 * @param bytes
	 *            valor en bytes.
	 * @return Cadena con la información leible en bytes
	 */
	private static String humanReadableByteCount(final Long bytes) {
		final int MAX_UNIT = 1024;
		int unit = MAX_UNIT;
		String res;
		if (bytes < unit) {
			res = bytes + " B";
		} else {
			int exp = (int) (Math.log(bytes) / Math.log(unit));
			String pre = "KMGTPE".charAt(exp - 1) + "";
			res = String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
		}

		return res;
	}

	/**
	 * Crea un nuevo documento ScannedDocumentDTO a partir de la información
	 * pasada como parámetros.
	 * 
	 * @param batchScanDoc
	 *            Objeto con la información asociada al lote del documento
	 *            escaneado.
	 * @param batchReqId
	 *            Identificador de petición de digitalización de lote de
	 *            documentos (carátula o reserva).
	 * @param images
	 *            Imágenes capturadas.
	 * @param fieldRequest
	 *            Objeto con la información obtenida del formulario de
	 *            escaneado.
	 * @param scannedDoc
	 *            Objeto recibido de la información escaneada.
	 * @param metadataCollection
	 *            Map con los metadatos asociados al documento escaneado.
	 * @param imageProcessor
	 *            Clase responsable del parseo y decodificación de las imágenes
	 *            capturadas.
	 * @param parameters
	 *            relación de parámetros del plugin de captura de imágenes de
	 *            documentos para el perfil de digitalización establecido.
	 * @param docMaxSize
	 *            tamaño máximo de los documentos digitalizados.
	 * @param userSession
	 *            Identificador del nombre de usuario.
	 * @param locale
	 *            configuración regional de la petición web.
	 * @return Objeto ScannedDocumentDTO con la información rellenada.
	 * @throws ScanDocsException
	 *             Exception ocurrida al relleanar la información del objeto
	 *             ScannedDocumentDTO.
	 */
	private ScannedDocumentDTO fillScannedDocumentoDTO(final BatchScanDoc batchScanDoc, final String batchReqId, final Map<String, MultipartFile> images, final PerformScanDocVO fieldRequest, final ScannedDoc scannedDoc, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection, final ImageProcessor imageProcessor, final List<ParameterDTO> parameters, final Long docMaxSize, final UserInSession userSession, final Locale locale) throws ScanDocsException {
		//, final BatchDTO aBatch) throws ScanDocsException {
		ScannedDocumentDTO scannedDocument = new ScannedDocumentDTO();
		String errorMessage;
		String[ ] args;

		// Identificador de lote
		//scannedDocument.setBatchId(aBatchId);

		// Identificador de petición de digitalización de lote (reserva o
		// carátula)
		scannedDocument.setBatchReqId(batchReqId);

		// Fecha de inicio del proceso de validación
		scannedDocument.setDigProcStartDate(fieldRequest.getStartDate());

		// nombre usuario
		scannedDocument.setUsername(userSession.getUsername());
		// unidades funcionales
		if (fieldRequest.getUserDptoId() == null) {
			if (batchScanDoc != null && (BatchType.BOOKING.equals(batchScanDoc.getBatchType()) || BatchType.SPACER.equals(batchScanDoc.getBatchType()))) {
				scannedDocument.setFunctionalOrgs(batchScanDoc.getDepartment());
			}
		} else {
			scannedDocument.setFunctionalOrgs(fieldRequest.getUserDptoId());
		}
		scannedDocument.setMimeType(scannedDoc.getMimeType());

		scannedDocument.setBatchOrderNum(scannedDoc.getNumOrder().intValue());
		scannedDocument.setName(scannedDoc.getName());

		// Información de validación del documento
		scannedDocument.setChecked(Boolean.TRUE);
		scannedDocument.setCheckDate(fieldRequest.getStartDate());

		try {
			byte[ ] docOut = null;

			docOut = imageProcessor.transformScannedDocument(scannedDoc, images, fieldRequest);
			// No es posible verificar restricción de tamaño para docs de más de
			// 2Gb.
			if (docMaxSize > 0 && docOut.length > docMaxSize) {
				args = new String[2];
				args[0] = scannedDocument.getBatchOrderNum().toString();
				args[1] = humanReadableByteCount(docMaxSize);
				errorMessage = messages.getMessage("error.docMaxSizeExceeded.msg", args, locale);
				throw new ScanDocsException(ScanDocsException.CODE_025, errorMessage);// NOPMD
			}
			scannedDocument.setContent(docOut);
		} catch (ScanDocsException e) {
			throw e;
		} catch (Exception e) {
			throw new ScanDocsException(ScanDocsException.CODE_999, "Error al rellenar el objecto de escaneado antes de persistir: " + e.getMessage());// NOPMD
		}

		return scannedDocument;
	}
	/**
	 * Crea un nuevo documento ScannedDocumentDTO a partir de la información
	 * pasada como parámetros.
	 * 
	 * @param batchScanDoc
	 *            Objeto con la información asociada al lote del documento
	 *            escaneado.
	 * @param batchReqId
	 *            Identificador de petición de digitalización de lote de
	 *            documentos (carátula o reserva).
	 * @param fieldRequest
	 *            Objeto con la información obtenida del formulario de
	 *            escaneado.
	 * @param scannedDoc
	 *            Objeto recibido de la información escaneada.
	 * @param metadataCollection
	 *            Map con los metadatos asociados al documento escaneado.
	 * @param imageProcessor
	 *            Clase responsable del parseo y decodificación de las imágenes
	 *            capturadas.
	 * @param parameters
	 *            relación de parámetros del plugin de captura de imágenes de
	 *            documentos para el perfil de digitalización establecido.
	 * @param docMaxSize
	 *            tamaño máximo de los documentos digitalizados.
	 * @param userSession
	 *            Identificador del nombre de usuario.
	 * @param locale
	 *            configuración regional de la petición web.
	 * @return Objeto BatchDTO con la información rellenada.
	 * @throws ScanDocsException
	 *             Exception ocurrida al relleanar la información del objeto
	 *             ScannedDocumentDTO.
	 */

	private BatchDTO fillBatchDTO(final BatchScanDoc batchScanDoc, final String batchReqId, final PerformScanDocVO fieldRequest,final ScannedDoc scannedDoc, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection, final ImageProcessor imageProcessor, final List<ParameterDTO> parameters, final Long docMaxSize, final UserInSession userSession, final Locale locale) {
		BatchDTO batch = new BatchDTO();
		String batchId = null;

		// Identificador de lote
		batchId = getBatchIdValue(metadataCollection);
		if (batchId != null) {
			batch.setBatchNameId(batchId);
		} else {
			batch.setBatchNameId(batchReqId);
		}

		// Identificador de petición de digitalización de lote (carátula)
		batch.setBatchReqId(batchReqId);

		// Fecha de inicio del proceso de validación
		batch.setStartDate(fieldRequest.getStartDate());
		
		// nombre usuario
		// TODO - El username debe ser validado para concatenarlo a la cadena si no existe.
		batch.setUsername(userSession.getUsername());
		// unidades funcionales

		if (fieldRequest.getUserDptoId() == null) {
			if (batchScanDoc != null && BatchType.SPACER.equals(batchScanDoc.getBatchType())) {
				batch.setFunctionalOrgs(batchScanDoc.getDepartment());
			}
		} else {
			batch.setFunctionalOrgs(fieldRequest.getUserDptoId());
		}
		
		return batch;
	}

	/**
	 * Obtiene el tamaño máximo permitido para los documentos digitalizados,
	 * expresado en bytes, a partir de la configuración del plugin de captura.
	 * 
	 * @param parameters
	 *            parámetros del plugin de captura.
	 * @return tamaño máximo permitido para los documentos digitalizados.
	 */
	private static long getDocMaxSizeAllowed(List<ParameterDTO> parameters) {
		Boolean found = Boolean.FALSE;
		long res = -1;
		ParameterDTO param;
		String paramValue = null;

		for (Iterator<ParameterDTO> it = parameters.iterator(); !found && it.hasNext();) {
			param = it.next();
			if (SCDC_DOC_MAX_SIZE.equals(param.getSpecificationName())) {
				paramValue = param.getValue();
				found = Boolean.TRUE;
			}
		}

		if (found && (paramValue != null && !paramValue.isEmpty())) {
			Matcher matcher = MAX_FILE_DOC_PATTERN.matcher(paramValue);
			Map<String, Integer> powerMap = new HashMap<String, Integer>();
			powerMap.put("GB", 3);
			powerMap.put("MB", 2);
			powerMap.put("KB", 1);

			if (matcher.find()) {
				String number = matcher.group(1) + (matcher.group(2) != null ? matcher.group(2) : "");
				number = number.replace(',', '.');
				int pow = powerMap.get(matcher.group(3).toUpperCase());
				BigDecimal bytes = new BigDecimal(number);
				bytes = bytes.multiply(BigDecimal.valueOf(1024).pow(pow));
				res = bytes.longValue();
			}
		}
		return res;
	}

	/**
	 * Obtiene el valor del identificador de lote de documentos a partir de la
	 * definición de documentos. En caso de no haber sido definido ningún
	 * metadato como identificador de lote, retorna null.
	 * 
	 * @param metadataCollection
	 *            colección de metdatos de la especificación de documentos que
	 *            determina el conjunto de metadatos del documento.
	 * @return El identificador de lote de documentos a partir de la definición
	 *         de documentos. En caso de no haber sido definido ningún metadato
	 *         como identificador de lote, retorna null.
	 */
	private String getBatchIdValue(Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) {
		Boolean found = Boolean.FALSE;
		MetadataSpecificationDTO metadataSpec;
		String res = null;

		for (Iterator<MetadataSpecificationDTO> it = metadataCollection.keySet().iterator(); !found && it.hasNext();) {
			metadataSpec = it.next();

			if (metadataSpec.getIsBatchId()) {
				res = metadataCollection.get(metadataSpec).getValue();
				found = Boolean.TRUE;
			}
		}

		return res;
	}

	/**
	 * Recupera el identificador de la reserva o carátula mediante que fue
	 * solicitada la digitalización del lote. En caso de no existir, retorna
	 * null.
	 * 
	 * @param batchScanDoc
	 *            Documento obtenido de la captura de imagen.
	 * @param userDocument
	 *            documento de identificación del usuario solicitante.
	 * @return El identificador de la reserva o carátula mediante que fue
	 *         solicitada la digitalización del lote. En caso de no existir,
	 *         retorna null.
	 */
	private String getBatchReqIdValue(BatchScanDoc batchScanDoc, String userDocument) {
		String res = null;

		if (BatchType.SPACER.equals(batchScanDoc.getBatchType())) {
			res = batchScanDoc.getSpacerId();
		}

		return res;
	}

	/**
	 * Obtenemos el identificador único segun el perfil seleccionado y la unidad
	 * organizativa asociado al tipo de documento.
	 * 
	 * @param scanProfileId
	 *            Identificador del perfil de digitalizacion
	 * @param orgUnitId
	 *            Identificador de la unidad organizativa
	 * @param idTypeDoc
	 *            Identificador del tipo de documento
	 * @return Identificador unico
	 * @throws DAOException
	 *             Exception al acceder a BBDD para recuperar la informacion
	 *             necesaria.
	 */

	private Long getScanProfOrgUnitDocSpecId(final Long scanProfileId, final Long orgUnitId, final Long idTypeDoc) throws DAOException {
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> docSpecificationsByScanProfileOrgUnitId = getDocSpecificationsByScanProfileOrgUnitId(scanProfileId, orgUnitId);
		// obtenemos la relación entre perfil digitalización-organization y
		// tipo documento escaneado
		return getScanProfileOrgUnitDocSpecId(docSpecificationsByScanProfileOrgUnitId, idTypeDoc);
	}

	/**
	 * Realiza las validaciones configuradas para los lotes de documentos.
	 * 
	 * @param docBatchs
	 *            Mapa con la informacion de los lotes de documentos procesados.
	 * @param uid
	 *            Identificador de la petición a efectos de ofrecer tiempos de
	 *            ejecución de las operaciones realizadas.
	 * @param locale
	 *            configuración regional de la petición web.
	 * @throws ScanDocsException
	 *             Si no se cumple alguna de las validaciones solicitadas.
	 */

	private void validationDocBatchs(Map<BatchScanDoc, List<ScannedDoc>> docBatchs, final String uid, final Locale locale) throws ScanDocsException {
		BatchScanDoc batchScanDoc;
		List<ScannedDoc> batchScanDocs;
		List<String> batchValErrors;
		List<String> messageResponse = new ArrayList<String>();
		long t0;
		long tf;
		String batchId;
		String errorMessage;
		String excMsg;
		String[ ] args;

		t0 = System.currentTimeMillis();

		try {
			for (Iterator<BatchScanDoc> it = docBatchs.keySet().iterator(); it.hasNext();) {
				batchScanDoc = it.next();
				batchScanDocs = docBatchs.get(batchScanDoc);

				/*if (BatchType.BOOKING.equals(batchScanDoc.getBatchType())) {
					batchId = "reserva";
				} else if (BatchType.SPACER.equals(batchScanDoc.getBatchType())) {*/
				if (BatchType.SPACER.equals(batchScanDoc.getBatchType())) {
					batchId = batchScanDoc.getSpacerId();
				} else {
					batchId = "no identificado";
				}

				batchValErrors = validationDocBatch(batchScanDoc, batchScanDocs, batchId, locale);

				if (batchValErrors != null && !batchValErrors.isEmpty()) {
					messageResponse.addAll(batchValErrors);
				}

			}
		} catch (DAOException e) {
			// Error no esperado al recuperar un perfil de digitalización o
			// definición de documentos de base de datos.
			excMsg = "Se produjo un error accediendo al modelo de datos del sistema. Error " + e.getCode() + " - " + e.getMessage();
			LOG.error(ME_WS_GV_SD_SERV, excMsg);

			args = new String[2];
			args[0] = e.getCode();
			args[1] = e.getMessage();
			errorMessage = messages.getMessage("error.valDocsNotExpected", args, locale);
			throw new ScanDocsException(ScanDocsException.CODE_999, errorMessage);// NOPMD
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-SCANDOCS-SERV] Validación de lotes de documentos digitalizados en la petición {} realizada en {} ms.", uid, (tf - t0));
		}

		if (!messageResponse.isEmpty()) {
			// lanzamos el error a traves de exception
			StringBuilder mensajeOut = new StringBuilder();
			for (String aux: messageResponse) {
				mensajeOut.append(aux);
				mensajeOut.append(RETURN_HTML);
			}
			throw new ScanDocsException(ScanDocsException.CODE_006, mensajeOut.toString());
		}
	}

	/**
	 * Verifica un lote de documentos procesado.
	 * 
	 * @param batchScanDoc
	 *            Lote de documentos.
	 * @param batchScanDocs
	 *            lista de documentos que componen el lote.
	 * @param batchId
	 *            Identificador del lote de documentos.
	 * @param locale
	 *            Información regional.
	 * @return Si no es correcto alguno de los aspecto configurados para ser
	 *         validados de un lote, devuelve una cadena de texto informando
	 *         sobre los errores encontrados. En caso contrario, retorna lista
	 *         vacía.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos.
	 */
	private List<String> validationDocBatch(final BatchScanDoc batchScanDoc, final List<ScannedDoc> batchScanDocs, final String batchId, final Locale locale) throws DAOException {
		List<String> res = new ArrayList<String>();
		String errorMessage;

		// Se verifica el tipo de separador del lote
		errorMessage = validateBatchSpacerType(batchScanDoc, batchId, locale);
		if (errorMessage != null) {
			res.add(errorMessage);
		}

		// Se verifica el perfil de digitalización
		errorMessage = validateScanProfileInfo(batchScanDoc, batchId, locale);
		if (errorMessage != null) {
			res.add(errorMessage);
		}

		// Se verifica la definición de documentos
		errorMessage = validateDocSpecInfo(batchScanDoc, batchId, locale);
		if (errorMessage != null) {
			res.add(errorMessage);
		}

		// Se verifica el numero de documentos en lote
		errorMessage = validateBatchDocSize(batchScanDoc, batchScanDocs.size(), batchId, locale);
		if (errorMessage != null) {
			res.add(errorMessage);
		}

		// Se verifica el tipo de separador de documentos del lote
		errorMessage = validateDocSpacerType(batchScanDoc, batchScanDocs, batchId, locale);
		if (errorMessage != null) {
			res.add(errorMessage);
		}

		// Se verifica el número de páginas total del lote
		errorMessage = validateBatchTotalPages(batchScanDoc, batchScanDocs, batchId, locale);
		if (errorMessage != null) {
			res.add(errorMessage);
		}

		return res;
	}

	/**
	 * Verifica el tipo de separador del lote.
	 * 
	 * @param batchScanDoc
	 *            Lote de documentos.
	 * @param batchId
	 *            Identificador del lote de documentos.
	 * @param locale
	 *            Información regional.
	 * @return Si no es correcto el tipo de separador, devuelve una cadena de
	 *         texto informando sobre el error. En caso contrario, retorna null.
	 */
	private String validateBatchSpacerType(final BatchScanDoc batchScanDoc, final String batchId, final Locale locale) {
		final int MAX_SIZE = 3;
		String[ ] args;
		String res = null;

		if (batchScanDoc.getBatchSpacerTypeVal() != null && !batchScanDoc.getBatchSpacerTypeVal().equals(batchScanDoc.getBatchSpacerType())) {
			args = new String[MAX_SIZE];
			args[0] = batchId;
			args[1] = messages.getMessage(batchScanDoc.getBatchSpacerType().name(), null, locale);
			args[2] = messages.getMessage(batchScanDoc.getBatchSpacerTypeVal().name(), null, locale);
			res = messages.getMessage(DC_BATSPATY_ER, args, locale);

		}

		return res;
	}

	/**
	 * Verifica el perfil de digitalización, en caso de haber sido establecido
	 * en la carátula.
	 * 
	 * @param batchScanDoc
	 *            Lote de documentos.
	 * @param batchId
	 *            Identificador del lote de documentos.
	 * @param locale
	 *            Información regional.
	 * @return Si no es correcto el perfil de digitalización y debe ser
	 *         validado, devuelve una cadena de texto informando sobre el error.
	 *         En caso contrario, retorna null.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos.
	 */
	private String validateScanProfileInfo(final BatchScanDoc batchScanDoc, final String batchId, final Locale locale) throws DAOException {
		OrganizationUnitDTO orgUnit;
		ScanProfileDTO scanTemplate;
		ScanProfileOrgUnitDTO scanProfile;
		String[ ] args;
		String res = null;

		if (batchScanDoc.getScanProfileIdVal() != null && !batchScanDoc.getScanProfileIdVal().equals(batchScanDoc.getScanProfileId())) {
			// TODO - Añadir info sobre el colectivo
			scanProfile = webscanModelManager.getScanProfileOrgUnit(batchScanDoc.getScanProfileIdVal());
			scanTemplate = webscanModelManager.getScanProfile(scanProfile.getScanProfile());
			orgUnit = webscanModelManager.getOrganizationUnit(scanProfile.getOrgUnit());
			args = new String[3];
			args[0] = batchId;
			args[1] = scanTemplate.getName();
			args[2] = orgUnit.getName();
			res = messages.getMessage(DC_BATSCPROF_ER, args, locale);
		}

		return res;
	}

	/**
	 * Verifica la definición de documentos, en caso de haber sido establecida
	 * en la carátula.
	 * 
	 * @param batchScanDoc
	 *            Lote de documentos.
	 * @param batchId
	 *            Identificador del lote de documentos. @param locale
	 *            Información regional.
	 * @return Si no es correcta la definición de documentos y debe ser
	 *         validada, devuelve una cadena de texto informando sobre el error.
	 *         En caso contrario, retorna null.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos.
	 */
	private String validateDocSpecInfo(final BatchScanDoc batchScanDoc, final String batchId, final Locale locale) throws DAOException {
		Boolean found;
		DocumentSpecificationDTO docSpec;
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> mapDocSpecs;
		String[ ] args;
		String res = null;

		if (batchScanDoc.getDocSpecIdVal() != null && !batchScanDoc.getDocSpecIdVal().equals(batchScanDoc.getDocSpecId())) {
			docSpec = webscanModelManager.getDocSpecification(batchScanDoc.getDocSpecIdVal());
			args = new String[2];
			args[0] = batchId;
			args[1] = docSpec.getName();
			res = messages.getMessage(DC_BATDCSP_ER, args, locale);
		} else {

			mapDocSpecs = webscanModelManager.getDocSpecificationsByScanProfileOrgUnitId(batchScanDoc.getScanProfileId());
			found = Boolean.FALSE;
			if (mapDocSpecs != null && !mapDocSpecs.isEmpty()) {

				for (Iterator<DocumentSpecificationDTO> it = mapDocSpecs.keySet().iterator(); !found && it.hasNext();) {
					docSpec = it.next();
					if (docSpec.getId().equals(batchScanDoc.getDocSpecId())) {
						found = docSpec.getActive() && mapDocSpecs.get(docSpec).getActive();
					}
				}
			}

			if (!found) {
				// TODO - Mostrar mensaje de error
			}
		}
		return res;
	}

	/**
	 * Verifica el número de documentos que componen el lote.
	 * 
	 * @param batchScanDoc
	 *            Lote de documentos.
	 * @param batchDocSize
	 *            número de documentos obtenidos en la captura.
	 * @param batchId
	 *            Identificador del lote de documentos.
	 * @param locale
	 *            Información regional.
	 * @return Si no es correcto el número de documentos que componen el lote y
	 *         debe ser validado, devuelve una cadena de texto informando sobre
	 *         el error. En caso contrario, retorna null.
	 */
	private String validateBatchDocSize(final BatchScanDoc batchScanDoc, final Integer batchDocSize, final String batchId, final Locale locale) {
		String[ ] args;
		String res = null;
		// Se valida el número de documentos del lote
		if (batchScanDoc.getNumDocBatch() != null && batchScanDoc.getNumDocBatch() > 0 && batchScanDoc.getNumDocBatch().intValue() != batchDocSize) {
			args = new String[2];
			args[0] = batchId;
			args[1] = batchScanDoc.getNumDocBatch().toString();
			res = messages.getMessage(DOC_BATSIZE_ER, args, locale);
		}

		return res;
	}

	/**
	 * Verifica el tipo de separador de documentos utilizado en el lote.
	 * 
	 * @param batchScanDoc
	 *            Lote de documentos.
	 * @param batchScanDocs
	 *            lista de documentos que componen el lote.
	 * @param batchId
	 *            Identificador del lote de documentos.
	 * @param locale
	 *            Información regional.
	 * @return Si no es correcto el tipo de separador de documentos utilizado en
	 *         el lote, devuelve una cadena de texto informando sobre el error.
	 *         En caso contrario, retorna null.
	 */
	private String validateDocSpacerType(final BatchScanDoc batchScanDoc, final List<ScannedDoc> batchScanDocs, final String batchId, final Locale locale) {
		final int MAX_SIZE = 3;
		Boolean found = Boolean.FALSE;
		ScannedDoc scannedDoc;
		String[ ] args;
		String res = null;

		for (Iterator<ScannedDoc> itDoc = batchScanDocs.iterator(); !found && itDoc.hasNext();) {
			scannedDoc = itDoc.next();
			if (scannedDoc.getDocSpacerType() != null && !scannedDoc.getDocSpacerType().equals(batchScanDoc.getDocSpacerTypeVal())) {
				args = new String[MAX_SIZE]; // NOPMD
				args[0] = messages.getMessage(scannedDoc.getDocSpacerType().name(), null, locale);
				args[1] = batchId;
				args[2] = messages.getMessage(batchScanDoc.getDocSpacerTypeVal().name(), null, locale);
				res = messages.getMessage("error.docSpacerInBatchNotMatch.msg", args, locale);
				found = Boolean.TRUE;
			}
		}

		return res;
	}

	/**
	 * Verifica el número total de páginas de un lote.
	 * 
	 * @param batchScanDoc
	 *            Lote de documentos.
	 * @param batchScanDocs
	 *            lista de documentos que componen el lote.
	 * @param batchId
	 *            Identificador del lote de documentos.
	 * @param locale
	 *            Información regional.
	 * @return Si no es correcto el número total de páginas de un lote y debe
	 *         ser validado, devuelve una cadena de texto informando sobre el
	 *         error. En caso contrario, retorna null.
	 */
	private String validateBatchTotalPages(final BatchScanDoc batchScanDoc, final List<ScannedDoc> batchScanDocs, final String batchId, final Locale locale) {
		final int MAX_SIZE = 3;
		Long batchTotalPages;
		String[ ] args;
		String res = null;

		if (batchScanDoc.getNumPageDoc() != null && batchScanDoc.getNumPageDoc() > 0) {
			batchTotalPages = 0L;
			for (ScannedDoc doc: batchScanDocs) {
				batchTotalPages += (doc.getNumPage() != null ? doc.getNumPage() : 0L);
			}

			if (batchTotalPages.intValue() != batchScanDoc.getNumPageDoc().intValue()) {
				args = new String[MAX_SIZE];
				args[0] = batchId;
				args[1] = batchTotalPages.toString();
				args[2] = batchScanDoc.getNumPageDoc().toString();
				res = messages.getMessage(BATNUMPAG_ER, args, locale);
			}

		}

		return res;
	}

	/**
	 * Prepara el mapa de la información recibida creando un mapa a partir de la
	 * información capturada.
	 * 
	 * @param images
	 *            Imágenes recibidos de la captura.
	 * @param fieldRequest
	 *            Parametros recuperados en la captura.
	 * @param imageProcessor
	 *            Clase responsable del parseo y decodificación de las imágnes
	 *            capturadas.
	 * @param userSession
	 *            Informacion del usuario de session.
	 * @param scanPluginParams
	 *            Parámetros de configuración de la instancia del plugin de
	 *            captura de imágenes establecida en el procesamiento actual.
	 * @param request
	 *            Petición web.
	 * @return Mapa con la información organizatida por tipo de lote y lista de
	 *         documentos asociados
	 * @throws ScanDocsException
	 *             si se produce algún error en la preparación de los documentos
	 *             para su registro en el sistema.
	 */
	private Map<BatchScanDoc, List<ScannedDoc>> prepareScanDocs(final Map<String, MultipartFile> images, final PerformScanDocVO fieldRequest, ImageProcessor imageProcessor, final UserInSession userSession, final Properties scanPluginParams, final HttpServletRequest request) throws ScanDocsException {
		Boolean batchProcessEnable;
		// BatchScanDoc batchScanDoc;
		List<ScanDocRequest> outListDocument;
		Map<BatchScanDoc, List<ScannedDoc>> res = null;
		String excMsg;
		String docBatEnabParVal;
		// separamos el documento por caratula, separadores, documentos, y
		// blancos

		docBatEnabParVal = searchParam(scanPluginParams, "scan.docBatch.enable");
		batchProcessEnable = ScanDocsConstants.TRUE_BOOL_STR_VAL.equals(docBatEnabParVal);

		outListDocument = imageProcessor.split2Image(images, fieldRequest, extractSpacerSearchingParams(scanPluginParams));

		if (!batchProcessEnable && outListDocument.size() > 1) {
			excMsg = "Procesamiento de lotes de documentos no admitido. Número de lotes procesados en la imagen capturada: " + outListDocument.size();
			LOG.error(ME_WS_GV_SD_SERV, excMsg);
			throw new ScanDocsException(ScanDocsException.CODE_015, excMsg);
		}

		// creamos el map con la información de los documentos recibidos
		// incluidos metadatos
		res = prepareMapDoc(outListDocument, fieldRequest, userSession, request);

		return res;
	}

	/**
	 * Prepara a partir de imagenes recibidas la información para procesar los
	 * lotes y documentos.
	 * 
	 * @param outListDocument
	 *            Listado de imagenes recibidas.
	 * @param fieldRequest
	 *            parámetros de la petición web.
	 * @param userSession
	 *            usuario que solicita la digitalización.
	 * @param request
	 *            Petición web.
	 * @return Mapa con la información organizada según tipo de lote
	 * @throws ScanDocsException
	 *             si se produce algún error al procesar las imágenes
	 *             capturadas.
	 */
	private Map<BatchScanDoc, List<ScannedDoc>> prepareMapDoc(List<ScanDocRequest> outListDocument, final PerformScanDocVO fieldRequest, final UserInSession userSession, final HttpServletRequest request) throws ScanDocsException {
		long t0;
		long tf;
		Map<BatchScanDoc, List<ScannedDoc>> mapBatchDoc = new HashMap<BatchScanDoc, List<ScannedDoc>>();

		t0 = System.currentTimeMillis();
		// obtenemos los metadatos por el tipo, para no recuperarlo en
		// diferentes lugares del proceso
		Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection = recoveryDocMetadataByTypeId(fieldRequest.getIdTypeDoc());

		// para los casos default y spacer
		mapBatchDoc = prepareDocWithoutBooking(outListDocument, fieldRequest, userSession, metadataCollection, request);

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-SCANDOCS-SERV] Extracción de documentos a partir de imágenes de la petición {} realizada en {} ms.", fieldRequest.getRequestId(), (tf - t0));

		return mapBatchDoc;
	}

	/**
	 * Prepara los documentos digitalizados mediante los modos de trabajo
	 * diferido o autónomo.
	 * 
	 * @param outListDocument
	 *            lista de documentos extraidos de la imagen capturada.
	 * @param fieldRequest
	 *            parámetros de la petición web.
	 * @param userSession
	 *            usuario que solicita la digitalización.
	 * @param docSpecMetadataCollection
	 *            relación de metadatos de la definición de documentos
	 *            seleccionada en la interfaz de captura.
	 * @param request
	 *            Petición web.
	 * @return Relación de lotes detectados y documentos que los conforman.
	 * @throws ScanDocsException
	 *             si se produce algún error en el procesamiento de los
	 *             documentos extraídos de las imágenes capturadas.
	 */

	private Map<BatchScanDoc, List<ScannedDoc>> prepareDocWithoutBooking(List<ScanDocRequest> outListDocument, final PerformScanDocVO fieldRequest, final UserInSession userSession, final Map<MetadataSpecificationDTO, MetadataDTO> docSpecMetadataCollection, final HttpServletRequest request) throws ScanDocsException {
		BarCodeResult barCodeResult = null;
		BatchScanDoc batchDoc = null;
		Boolean batchSpacerFound = Boolean.FALSE;
		DocsBatchSpacerDTO docsBatchSpacer = null;
		DocSpacerType docSpacerType = null;
		Integer docNumber;
		List<ScannedDoc> listScannedDoc = new ArrayList<ScannedDoc>();
		List<String> spacerIds = new ArrayList<String>();
		Long nDoc = 0L;
		Map<BatchScanDoc, List<ScannedDoc>> res = new HashMap<BatchScanDoc, List<ScannedDoc>>();
		Map<SpacerDocDTO, List<SpacerMetadataDTO>> batchDocs = null;
		Map<String, String> scanMetadataNames;
		Map<String, String> scanDocMetadataCollection;
		ScannedDoc scannedDoc;
		ScanDocRequest scanDocRequest;
		SpacerDocDTO spacerDoc;
		String docName;
		String docSpacer;

		String excMsg;
		String[ ] args;
		try {

			// Se obtiene el nombre d elos metadatos que deben ser establecidos
			// automáticamente en la fase de captura
			scanMetadataNames = getScanMetadataNamesFromPlugin(fieldRequest.getScanTemplateId(), fieldRequest.getOrgUnitId());

			// recorremos buscando si existe alguna caratula
			for (Iterator<ScanDocRequest> it = outListDocument.iterator(); it.hasNext();) {
				scanDocRequest = it.next();
				barCodeResult = scanDocRequest.getBarCodeResult();
				docNumber = null;
				docName = null;
				scanDocMetadataCollection = null;

				// si tenemos un separador de lotes de documentos
				if (ScannedImageType.SPACER_BATCH.equals(scanDocRequest.getTypeImage())) {
					// el campo idDocBatchSpacerType no siempre es informado,
					// solo en caso que se desee validar
					docSpacer = null;

					if (barCodeResult == null || barCodeResult.getBarCodeText() == null) {
						excMsg = messages.getMessage("error.spacerIdNotFound.msg", null, request.getLocale());
						LOG.error(ME_WS_GV_SD_SERV, excMsg);
						throw new ScanDocsException(ScanDocsException.CODE_008, excMsg);
					}

					docSpacer = barCodeResult.getBarCodeText();
					// Nos quedamos con el identificador externo de la carátula
					docSpacer = docSpacer.substring(SpacersGenerator.BATCH_CODE.length());
					docsBatchSpacer = spacersModelManager.getGeneratedDocsBatchSpacer(docSpacer);

					if (spacerIds.contains(docsBatchSpacer.getSpacerId())) {
						args = new String[1];// NOPMD
						excMsg = messages.getMessage("error.scanProcess.repeatedBatch", args, request.getLocale());
						LOG.error(ME_WS_GV_SD_SERV, excMsg);
						throw new ScanDocsException(ScanDocsException.CODE_021, excMsg);
					}

					checkScanWorkDepartment(docsBatchSpacer.getDepartment(), fieldRequest.getScanProfileOrgUnitId(), getOrgUnitScanProfiles(userSession), request.getSession(false), request.getLocale());

					// Añadimos la carátula procesada
					spacerIds.add(docsBatchSpacer.getSpacerId());

					if (batchSpacerFound) {
						// añadimos al mapa el lote tratado anteriormente
						res.put(batchDoc, listScannedDoc);
					}

					// creamos nuevo lote
					batchDoc = createBatchDoc(fieldRequest, barCodeResult, docsBatchSpacer);
					// si tenemos que validar el numero de documentos
					checkNumDocs(fieldRequest, batchDoc, docsBatchSpacer);
					// si tenemos que validar el número de paginas en cada
					// documento
					checkNumberPagByDoc(fieldRequest, batchDoc, docsBatchSpacer);

					// recuperamos los documentos asociados al lote
					batchDocs = recoveryBatchDocsInfo(docsBatchSpacer.getId());

					// creamos el listado de documentos asociados al nuevo lote
					listScannedDoc = new ArrayList<ScannedDoc>();// NOPMD
					nDoc = 0L;
					batchSpacerFound = Boolean.TRUE;
				} else if (isDocSpacer(scanDocRequest.getTypeImage())) {
					// tratamiento de separadores de documentos de un lote
					nDoc = nDoc + 1L;
					if (ScannedImageType.SPACER_BLANK.equals(scanDocRequest.getTypeImage())) {
						docSpacerType = DocSpacerType.BLANK_PAGE;
					} else {
						docSpacerType = scanDocRequest.getBarCodeResult().getBarCodeType();
					}
				} else if (ScannedImageType.DOC_CONTENT.equals(scanDocRequest.getTypeImage())) {
					// preparamos el documento
					// No llega un separador, sino un documento de inicio
					// tratamiento de separadores de documentos de un lote
					if (nDoc == 0L) {
						nDoc = nDoc + 1L;
						docSpacerType = null;
					}
					// creamos el nuevo documento digitalizado, añadiéndole
					// además sus metadatos
					if (batchDocs != null) {
						// Modo dieferido, mediante carátulas
						spacerDoc = getSpacerDoc(docsBatchSpacer.getSpacerId(), batchDocs, nDoc.intValue(), request.getLocale());
						docNumber = spacerDoc.getOrder();
						docName = spacerDoc.getName();
						scanDocMetadataCollection = extractSpacerDocMetadataCollection(batchDocs.get(spacerDoc));
					} else {
						// Modo autónomo
						docNumber = nDoc.intValue();
						docName = "Documento " + nDoc;
					}
					scannedDoc = createScannedDoc(batchDoc, fieldRequest, docSpacerType, docNumber.longValue(), docName, scanDocRequest, scanMetadataNames, scanDocMetadataCollection, docSpecMetadataCollection, userSession);

					// añadimos el documento a la lista
					listScannedDoc.add(scannedDoc);
				}
			}

			if (!listScannedDoc.isEmpty()) {
				// Ultimo procesado es un sólo documento
				if (batchDoc == null) {
					// entendemos que es una digitalización en modo autónomo,
					// sin carátula
					batchDoc = createBatchScanDocSimple(fieldRequest, barCodeResult);
				}
				Map<BatchScanDoc, List<ScannedDoc>> defaultSet = new HashMap<BatchScanDoc, List<ScannedDoc>>();
				defaultSet.put(batchDoc, listScannedDoc);
				res.putAll(defaultSet);
			}
		} catch (ScanDocsException exc) {
			LOG.error("[WEBSCAN-SCANDOCS-SERV] Error al preparar los documentos asociados a un lote. Error: {} - {}", exc.getCode(), exc.getMessage());
			throw exc;
		} catch (SpacersDAOException exc) {
			String errorCode = ScanDocsException.CODE_005;
			if (SpacersDAOException.CODE_901.equals(exc.getCode())) {
				errorCode = ScanDocsException.CODE_016;
				LOG.error("[WEBSCAN-SCANDOCS-SERV] Error al recuperar la información asociada a una carátula de un lote. Error: {} - {}", exc.getCode(), exc.getMessage());
			} else {
				LOG.error("[WEBSCAN-SCANDOCS-SERV] Error al recuperar la información asociada a un lote. Error: {} - {}", exc.getCode(), exc.getMessage());

			}
			throw new ScanDocsException(errorCode, exc.getMessage());// NOPMD
		} catch (Exception exc) {
			LOG.debug("[WEBSCAN-SCANDOCS-SERV] Error al tratar lote:", exc.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_999, exc.getMessage());// NOPMD
		}

		return res;
	}

	/**
	 * Método que crea un lote simple de documentos digitalizados.
	 * 
	 * @param fieldRequest
	 *            Objeto con la información recuperada del formulario de
	 *            escaneado.
	 * @param barCodeResult
	 *            Objeto con el código de barra recibido.
	 * @return BatchScanDoc Objeto con el lote del documento creado.
	 */
	private BatchScanDoc createBatchScanDocSimple(final PerformScanDocVO fieldRequest, BarCodeResult barCodeResult) {
		BatchScanDoc batchDoc;
		batchDoc = new BatchScanDoc();
		// asignamos tipo
		batchDoc.setBatchType(BatchType.DEFAULT);
		// indicamos perfil seleccionado
		batchDoc.setScanProfileId(fieldRequest.getScanProfileOrgUnitId());
		if (barCodeResult != null) {
			batchDoc.setBatchSpacerType(BatchSpacerType.getByName(barCodeResult.getBarCodeType().getName()));
		}
		// Establecido por interfaz
		if (fieldRequest.getIdDocBatchSpacerType() != null) {
			batchDoc.setBatchSpacerTypeVal(BatchSpacerType.getByName(fieldRequest.getIdDocBatchSpacerType()));
		}
		// Tipo de separador de los documentos del lote
		batchDoc.setDocSpacerTypeVal(DocSpacerType.getByName(fieldRequest.getIdDocSpacerType()));

		// si tenemos que validar el numero de documentos
		if (fieldRequest.getIdDocBatchValNumDocs() != null) {
			batchDoc.setNumDocBatch(fieldRequest.getIdDocBatchValNumDocs());
		}
		// si tenemos que validar el número de paginas en cada
		// documento
		if (fieldRequest.getIdDocValNumPagsDoc() != null) {
			batchDoc.setNumPageDoc(fieldRequest.getIdDocValNumPagsDoc());
		}
		return batchDoc;
	}

	/**
	 * Metodo para crear el nuevo documento digitalizado.
	 * 
	 * @param batchScanDoc
	 *            lote de documentos.
	 * @param fieldRequest
	 *            Entidad con la información recuperada del formulario de
	 *            escaneo.
	 * @param docSpacerType
	 *            Entidad con la información del tipo de separador del
	 *            documento.
	 * @param docNumber
	 *            Número de orden del documento.
	 * @param docName
	 *            Denominación del documento.
	 * @param scanDocRequest
	 *            Entidad con la información del documento escaneado.
	 * @param scanMetadataNames
	 *            Información de los documentos registrada mediante la carátula
	 *            que identifica el lote al que pertenecen.
	 * @param batchMetadataCollection
	 *            metadatos informados para el documento mediante una reserva o
	 *            carátula.
	 * @param docSpecMetadataCollection
	 *            relación de metadatos de la definición de documentos
	 *            seleccionada en la interfaz de captura.
	 * @param userSession
	 *            usuario que solicita la digitalización.
	 * @return Documento digitalizado.
	 * @throws ScanDocsException
	 *             Si no se encuentra información del documento en el lote de
	 *             documentos identificado mediante la carátula.
	 */
	private ScannedDoc createScannedDoc(final BatchScanDoc batchScanDoc, final PerformScanDocVO fieldRequest, final DocSpacerType docSpacerType, final Long docNumber, final String docName, final ScanDocRequest scanDocRequest, final Map<String, String> scanMetadataNames, final Map<String, String> batchMetadataCollection, final Map<MetadataSpecificationDTO, MetadataDTO> docSpecMetadataCollection, final UserInSession userSession) throws ScanDocsException {
		Map<String, String> docMetadataCollection = new HashMap<String, String>();
		Map<String, String> scanStageMetadataCollection;
		ScannedDoc res;

		res = new ScannedDoc();
		res.setListImage(scanDocRequest.getListImage());
		res.setNumOrder(docNumber);
		res.setName(docName);
		res.setMimeType(mimeTypeByDesc(fieldRequest.getIdOutputFormat()));
		res.setNumPage(Long.valueOf(scanDocRequest.getListImage().size()));
		res.setDocSpacerType(docSpacerType);

		// Se añaden los metadatos informados para el documento mediante una
		// reserva o carátula.
		if (batchMetadataCollection != null && !batchMetadataCollection.isEmpty()) {
			docMetadataCollection.putAll(batchMetadataCollection);
		}
		// Se recupera el valor de los metadatos calculados automáticamente para
		// el documento
		scanStageMetadataCollection = getScanMetadataCollection(batchScanDoc, fieldRequest, scanMetadataNames, docSpecMetadataCollection, userSession);
		if (scanStageMetadataCollection != null && !scanStageMetadataCollection.isEmpty()) {
			docMetadataCollection.putAll(scanStageMetadataCollection);
		}
		res.setMetadata(docMetadataCollection);

		return res;
	}

	/**
	 * Transforma una lista de objetos SpacerMetadataDTO en una relación de
	 * pares clave/valor.
	 * 
	 * @param spacerMetadataCollection
	 *            Relación de metadatos informada para un documento pertenciente
	 *            a un lote, identificado por una carátula.
	 * @return Relación de pares clave/valor que representan el conjunto de
	 *         metadatos informados para un documento pertenciente a un lote,
	 *         identificado por una carátula.
	 */
	private Map<String, String> extractSpacerDocMetadataCollection(List<SpacerMetadataDTO> spacerMetadataCollection) {
		Map<String, String> res = new HashMap<String, String>();

		if (spacerMetadataCollection != null) {
			for (SpacerMetadataDTO metadata: spacerMetadataCollection) {
				res.put(metadata.getName(), metadata.getValue());
			}
		}

		return res;
	}

	/**
	 * Metodo que recupera la información de un documento, perteneciente a un
	 * lote identificado por una carátula, a partir de su número de orden en el
	 * lote.
	 * 
	 * @param batchSpacerId
	 *            Idnetificado normalizado de la carátula que identifica el lote
	 *            de documentos.
	 * @param batchScanDocs
	 *            Información de los documentos registrada mediante la carátula
	 *            que identifica el lote al que pertenecen.
	 * @param docNumber
	 *            Número de orden del documento.
	 * @param locale
	 *            Identificador del language de la peticion http.
	 * @return Documento digitalizado.
	 * @throws ScanDocsException
	 *             Si no se encuentra información del documento en el lote de
	 *             documentos identificado mediante la carátula.
	 */
	private SpacerDocDTO getSpacerDoc(final String batchSpacerId, final Map<SpacerDocDTO, List<SpacerMetadataDTO>> batchScanDocs, final Integer docNumber, Locale locale) throws ScanDocsException {
		Boolean found = Boolean.FALSE;
		SpacerDocDTO res = null;
		String excMsg;
		String[ ] args;

		for (Iterator<SpacerDocDTO> itDocs = batchScanDocs.keySet().iterator(); !found && itDocs.hasNext();) {
			res = itDocs.next();

			if (docNumber.intValue() == res.getOrder().intValue()) {
				found = Boolean.TRUE;
			}
		}

		if (!found) {
			if (!found) {
				args = new String[2];// NOPMD
				args[0] = docNumber.toString();
				args[1] = batchSpacerId;
				excMsg = messages.getMessage("error.docNotInBatch.msg", args, locale);
				LOG.error(ME_WS_GV_SD_SERV, excMsg);
				throw new ScanDocsException(ScanDocsException.CODE_010, excMsg);
			}
		}

		return res;
	}

	/**
	 * Metodo para crear un nuevo lote de documentos a partir de la información
	 * recuperada del formulario de escaneo y la carátula.
	 * 
	 * @param fieldRequest
	 *            Entidad con la información recibida del formulario de escaneo.
	 * @param barCodeResult
	 *            Entidad con la información del código de barra del lote.
	 * @param docsBatchSpacer
	 *            Carátula que identifica el lote de documentos en proceso de
	 *            digitalización.
	 * @return Lote de documentos en proceso de digitalización.
	 */
	private BatchScanDoc createBatchDoc(final PerformScanDocVO fieldRequest, BarCodeResult barCodeResult, DocsBatchSpacerDTO docsBatchSpacer) {
		BatchScanDoc batchDoc;
		batchDoc = new BatchScanDoc();// NOPMD
		batchDoc.setSpacerId(docsBatchSpacer.getSpacerId());
		// Indicamos perfil de digitalización seleccionado (plantilla
		// configurada en unidad organizativa)
		batchDoc.setScanProfileId(fieldRequest.getScanProfileOrgUnitId());
		batchDoc.setScanProfileIdVal(docsBatchSpacer.getScanProfileId());

		// indicamos tipo documental seleccionado
		batchDoc.setDocSpecId(fieldRequest.getIdTypeDoc());
		batchDoc.setDocSpecIdVal(docsBatchSpacer.getDocSpecId());

		// asignamos tipo
		batchDoc.setBatchType(BatchType.SPACER);
		batchDoc.setBatchSpacerType(BatchSpacerType.getByName(barCodeResult.getBarCodeType().getName()));

		batchDoc.setDepartment(docsBatchSpacer.getDepartment());

		// separador de lotes
		setBatchSpacerTypeVal(fieldRequest, batchDoc, docsBatchSpacer);
		// Tipo de separador de los documentos del lote
		setDocSpacerTypeVal(fieldRequest, batchDoc, docsBatchSpacer);
		return batchDoc;
	}

	/**
	 * Metodo que setea el tipo del separador de lotes.
	 * 
	 * @param fieldRequest
	 *            Entidad con la información recuperada del formulario de
	 *            escaneo
	 * @param batchDoc
	 *            Entidad que identifica el lote de documentos
	 * @param docsBatchSpacer
	 *            Entidad del separador de tipo de lote
	 */
	private void setBatchSpacerTypeVal(final PerformScanDocVO fieldRequest, BatchScanDoc batchDoc, DocsBatchSpacerDTO docsBatchSpacer) {
		if (docsBatchSpacer.getSpacerType() != null) {
			// Tipo de separador de la carátula
			batchDoc.setBatchSpacerTypeVal(docsBatchSpacer.getSpacerType());
		} else {
			// Establecido por interfaz
			if (fieldRequest.getIdDocBatchSpacerType() != null) {
				batchDoc.setBatchSpacerTypeVal(BatchSpacerType.getByName(fieldRequest.getIdDocBatchSpacerType()));
			}
		}
	}

	/**
	 * Metodo que setea el tipo de separador de documentos.
	 * 
	 * @param fieldRequest
	 *            Entiadd con la información recuperada del formulario de
	 *            escaneo
	 * @param batchDoc
	 *            Entidad que identifica el lote de documentos
	 * @param docsBatchSpacer
	 *            Entidad del separador de tipo de lote
	 */
	private void setDocSpacerTypeVal(final PerformScanDocVO fieldRequest, BatchScanDoc batchDoc, DocsBatchSpacerDTO docsBatchSpacer) {
		if (docsBatchSpacer.getDocSpacerType() != null) {
			// Se establece el registrado en la carátula
			batchDoc.setDocSpacerTypeVal(docsBatchSpacer.getDocSpacerType());
		} else {
			// Se establece el especificado por el usuario en el
			// interfaz de captura
			batchDoc.setDocSpacerTypeVal(DocSpacerType.getByName(fieldRequest.getIdDocSpacerType()));
		}
	}

	/**
	 * Metodo que valida el numero de documentos del lote.
	 * 
	 * @param fieldRequest
	 *            Entidad con la información recuperada del formulario de
	 *            escaneo
	 * @param batchDoc
	 *            Entidad que identifida el lote de documentos
	 * @param docsBatchSpacer
	 *            Entidad del separador de tipo de lote
	 */
	private void checkNumDocs(final PerformScanDocVO fieldRequest, BatchScanDoc batchDoc, DocsBatchSpacerDTO docsBatchSpacer) {
		if (docsBatchSpacer.getBatchDocSize() != null) {
			batchDoc.setNumDocBatch(docsBatchSpacer.getBatchDocSize().longValue());
		} else {
			if (fieldRequest.getIdDocBatchValNumDocs() != null) {
				batchDoc.setNumDocBatch(fieldRequest.getIdDocBatchValNumDocs());
			}
		}
	}

	/**
	 * Metodo que evalida el numero de paginas de cada documento de un lote.
	 * 
	 * @param fieldRequest
	 *            Entidad con la información recuperada del formulario de
	 *            escaneo
	 * @param batchDoc
	 *            Entidad que identifida el lote de documentos
	 * @param docsBatchSpacer
	 *            Entidad del separador de tipo de lote
	 */
	private void checkNumberPagByDoc(final PerformScanDocVO fieldRequest, BatchScanDoc batchDoc, DocsBatchSpacerDTO docsBatchSpacer) {
		if (docsBatchSpacer.getBatchPages() != null) {
			batchDoc.setNumPageDoc(docsBatchSpacer.getBatchPages().longValue());
		} else {
			if (fieldRequest.getIdDocValNumPagsDoc() != null) {
				batchDoc.setNumPageDoc(fieldRequest.getIdDocValNumPagsDoc());
			}
		}
	}

	/**
	 * Funcion auxiliar para obtener el mime type a partir de un string.
	 * 
	 * @param inDesc
	 *            String de la descripción
	 * @return Mime Type con el valor correspondiente
	 */
	private MimeType mimeTypeByDesc(final String inDesc) {
		MimeType res = null;

		if (inDesc.toUpperCase().contains(PDF_FORMAT)) {
			res = MimeType.PDF_CONTENT;
		} else if (inDesc.toUpperCase().contains(JPEG_FORMAT)) {
			res = MimeType.JPG_IMG_CONTENT;
		} else if (inDesc.toUpperCase().contains(PNG_FORMAT)) {
			res = MimeType.PNG_IMG_CONTENT;
		} else if (inDesc.toUpperCase().contains(TIFF_FORMAT)) {
			res = MimeType.TIFF_IMG_CONTENT;
		}

		return res;
	}

	/**
	 * Recupera los documentos de un lote identificado mediante una carátula.
	 * 
	 * @param spacerId
	 *            Identificador interno de la carátula.
	 * @return Relación de documentos de un lote identificado mediante una
	 *         carátula.
	 * @throws SpacersDAOException
	 *             Exception ocurrida al recuperar la información de la BBDD
	 *             sobre la carátula.
	 */

	private Map<SpacerDocDTO, List<SpacerMetadataDTO>> recoveryBatchDocsInfo(final Long spacerId) throws SpacersDAOException {
		Map<SpacerDocDTO, List<SpacerMetadataDTO>> res;

		// Se recupera la carátula y sus metadatos
		res = spacersModelManager.getSpacerDocsAndMetadataCollection(spacerId);

		return res;
	}


	/**
	 * Verifica si la unidad orgánica establecida para el trabajo de
	 * digitalización está asociada al colectivo que identifica el perfil de
	 * digitalización del trabajo de digitalización.
	 * 
	 * @param department
	 *            Unidad orgánica a la que serán asociados los documentos del
	 *            trabajo de digitalización.
	 * @param scanProfileId
	 *            Identificador del perfil de digitalización del trabajo de
	 *            digitalización.
	 * @param userOrgUnitScanProfiles
	 *            Perfiles de digitalización activos para un usuario.
	 * @param session
	 *            Sesión web.
	 * @param locale
	 *            Configuración regional.
	 * @throws WebscanDocBoxException
	 *             Si la unidad orgánica establecida para el trabajo de
	 *             digitalización no está asociada al colectivo que identifica
	 *             el perfil de digitalización del trabajo.
	 */
	@SuppressWarnings("unchecked")
	private void checkScanWorkDepartment(String department, Long scanProfileId, List<OrgUnitScanProfileVO> userOrgUnitScanProfiles, HttpSession session, Locale locale) throws ScanDocsException {
		Boolean found;
		List<DepartamentVO> dptos;
		OrgUnitScanProfileVO orgUnitScanProfile;
		String dptoId;
		String errorMsg;
		String[ ] args;

		// Se comprueba si la unidad orgánica informada para el
		// trabajo de digitalización activo también la tiene
		// configurada el usuario
		dptos = (List<DepartamentVO>) session.getAttribute(WebscanBaseConstants.USER_DPTOS_SESSION_ATT);
		if (department != null && (dptos != null && !dptos.isEmpty())) {
			found = Boolean.FALSE;
			for (Iterator<OrgUnitScanProfileVO> it = userOrgUnitScanProfiles.iterator(); !found && it.hasNext();) {
				orgUnitScanProfile = it.next();
				if (orgUnitScanProfile.getScanProfileOrgUnitId().equals(scanProfileId)) {
					for (Iterator<String> itDptos = orgUnitScanProfile.getDepartments().iterator(); !found && itDptos.hasNext();) {
						dptoId = itDptos.next();
						found = department.equals(dptoId);
					}
				}

			}

			if (!found) {
				args = new String[1];
				args[0] = department;
				errorMsg = messages.getMessage("error.noDptoForScanProfile", args, locale);
				throw new ScanDocsException(ScanDocsException.CODE_026, errorMsg);
			}
		}
	}

	/**
	 * Obtiene los perfiles de digitalización habilitados para un usuario,
	 * asociados a los procesos de digitalización disponibles en el sistema.
	 * 
	 * @param user
	 *            Usuario logado en sesión.
	 * @return Perfiles de digitalización habilitados para un usuario, asociados
	 *         a los procesos de digitalización disponibles en el sistema.
	 * @throws DAOException
	 *             Si se produce un error en el acceso al modelo de datos.
	 */
	public List<OrgUnitScanProfileVO> getOrgUnitScanProfiles(UserInSession user) throws DAOException {
		Boolean found;
		List<Long> docSpecIds;
		List<DocumentSpecificationDTO> docSpecs;
		List<OrgUnitScanProfileVO> res = new ArrayList<OrgUnitScanProfileVO>();
		OrgUnitScanProfileVO ousp;
		ScanProfileDTO scanProfile;
		ScanProfileOrgUnitDTO scanProfileOrgUnit;
		List<UserOrgUnitScanProfiles> userOrgUnitScanProfiles = new ArrayList<UserOrgUnitScanProfiles>();

		userOrgUnitScanProfiles = user.getOrgUnitScanProfiles();

		for (UserOrgUnitScanProfiles uousp: userOrgUnitScanProfiles) {
			found = Boolean.FALSE;
			for (Iterator<ScanProfileDTO> it = uousp.getScanProfiles().iterator(); !found && it.hasNext();) {
				scanProfile = it.next();
				try {
					ousp = new OrgUnitScanProfileVO();
					ousp.setOrganizationUnit(uousp.getOrganizationUnit());
					ousp.setScanProfile(scanProfile);
					scanProfileOrgUnit = getScanProfileOrgUnitByScanProfileAndOrgUnit(scanProfile.getId(), uousp.getOrganizationUnit().getId());
					ousp.setScanProfileOrgUnitId(scanProfileOrgUnit.getId());

					docSpecs = getListDocSpecficationActiveByScanProfileOrgUnitId(scanProfileOrgUnit.getId());
					docSpecIds = new ArrayList<Long>();
					for (DocumentSpecificationDTO docSpec: docSpecs) {
						docSpecIds.add(docSpec.getId());
					}
					ousp.setDocSpecs(docSpecIds);

					for (DepartamentVO dpto: uousp.getFunctionalOrgs()) {
						ousp.getDepartments().add(dpto.getId());
					}

					res.add(ousp);
				} catch (DAOException e) {
					LOG.error("[WEBSCAN-SCANDOCS-SERV] Error al obtiene los perfiles de digitalización rapida");
				} catch (ScanDocsException e) {
					LOG.error("[WEBSCAN-SCANDOCS-SERV] Error al obtiene los perfiles de digitalización rapida");
				}

				found = Boolean.TRUE;

			}
		}

		return res;
	}

	/**
	 * Recupera un listado de definiciones de documentos activas para un perfil
	 * de digitalización dado. La definición estará activa, si ella misma esta
	 * activa, y si su configuración para el perfil está habilitada.
	 * 
	 * @param scanProfileId
	 *            Identificador del perfil de digitalización.
	 * @return Listado de especificaciones de documentos activas para el perfil
	 *         de digitalización identificado mediante una plantilla y un
	 *         colectivo de usuarios.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD, o no
	 *             existir definiciones activas.
	 * @throws WebscanDocBoxException
	 *             producida al no existir definiciones de documentos activas
	 *             para el perfil.
	 */
	private List<DocumentSpecificationDTO> getListDocSpecficationActiveByScanProfileOrgUnitId(final Long scanProfileId) throws DAOException, ScanDocsException {
		Long t0;
		Long tf;
		List<DocumentSpecificationDTO> res = new ArrayList<DocumentSpecificationDTO>();
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> mapDocSpecs;

		LOG.debug("[WEBSCAN-SCANDOCS-SERV] getListDocSpecficationActiveByScanProfileOrgUnitId Start");
		t0 = System.currentTimeMillis();
		// recupero ListDocSpec que estén activos
		mapDocSpecs = getActiveDocSpecificationsByScanProfileOrgUnitId(scanProfileId);
		if (mapDocSpecs == null || mapDocSpecs.isEmpty()) {
			throw new ScanDocsException(ScanDocsException.CODE_027, "error.DocSpecActiveProf");
		}

		res.addAll(mapDocSpecs.keySet());

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-SCANDOCS-SERV] Listado especificaciones de documentos activos por perfil finalizado en {} ms.", (tf - t0));
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] getListDocSpecficationActiveByScanProfileOrgUnitId End");
		return res;
	}

	/**
	 * Obtiene un map con la relacion de definiciones de documentos configuradas
	 * para un perfil de digitalización que se encuentren activas.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @return relacion de definiciones de documentos configuradas para un
	 *         perfil de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	private Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getActiveDocSpecificationsByScanProfileOrgUnitId(Long scanProfileId) throws DAOException {
		return webscanModelManager.getActiveDocSpecificationsByScanProfileOrgUnitId(scanProfileId);
	}


	/**
	 * Procedimiento para realizar la llamada de ejecución del procedimiento
	 * asincrono de los documentos.
	 * 
	 * @param listIdDocReg
	 *            Listado de documentos registrados.
	 * @param username
	 *            Identificativo del usuario que lanza el proceso.
	 * @parm uid Identificador de petición para el registro de tiempos de
	 *       ejecución.
	 */
	public void callAsincDocScanProcess(final List<ScanDocResult> listScanDocs, final String username, String uid) {
		Long t0;
		Long tf;

		t0 = System.currentTimeMillis();

		LOG.debug("[WEBSCAN-SCANDOCS-SERV] callAsincDocScanProcess Start ...");
		for (ScanDocResult sdr: listScanDocs) {
			LOG.debug("Solicitado ejecución proceso de de digitalizacion, scanDoc:{}", sdr.getDocId());
			asyncScanProcessManager.performDocScanProcess(sdr.getDocId(), username);
			LOG.debug("Fin  Solicitado reanudación del proceso de de digitalizacion, scanDoc:{}", sdr.getDocId());
		}
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] callAsincDocScanProcess End ...");

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-SCANDOCS-SERV]CallAsincDocScanProcess Petición {}. Llamada de ejecución del procedimiento asincrono de los documentos finalizado en {} ms.", uid, (tf - t0));
	}

	/**
	 * Crea una traza de ejecución para la fase de captura de los documentos
	 * digitalizados.
	 * 
	 * @param docs
	 *            Documentos digitalizados
	 * @param fieldRequest
	 *            parámetros de la petición web.
	 * @param registerdDocs
	 *            fecha de finalización de los documentos digitalizados.
	 * @throws DAOException
	 *             Excepción al acceder a la BBDD.
	 */

	private void createScanTraces(final List<ScannedDocumentDTO> docs, final PerformScanDocVO fieldRequest, final Map<Long, ScanDocResult> registerdDocs) throws DAOException {
		Long scannedDocLogId;
		ScanProcessStageDTO stageCapture = getScanProcStageByName(SCAN);

		for (ScannedDocumentDTO doc: docs) {
			// Se crea traza
			ScannedDocLogDTO scannedDocLogDto = new ScannedDocLogDTO();// NOPMD
			scannedDocLogDto.setStartDate(fieldRequest.getStartDate());

			scannedDocLogDto.setScanProcessStageName(stageCapture.getName());
			scannedDocLogId = webscanModelManager.createScannedDocLog(scannedDocLogDto, doc.getId(), stageCapture.getId());
			LOG.debug("[WEBSCAN-SCANDOCS-SERV] Traza de ejecución creada para el documento {} en la fase {}.", doc.getId(), scannedDocLogId);
			// Se cierra la traza
			scannedDocLogDto = webscanModelManager.getLastScannedDocLogsByDocId(doc.getId());
			scannedDocLogDto.setEndDate(registerdDocs.get(doc.getId()).getEndDate());
			scannedDocLogDto.setStatus(OperationStatus.PERFORMED);
			webscanModelManager.closeScannedDocLog(scannedDocLogDto);
		}
	}

	/**
	 * Crea la traza de auditoria de digitalizacion.
	 * 
	 * @param doc
	 *            documento que es digitalizado.
	 * @param username
	 *            usuario que efectúa la captura.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 * @return identificador del log de auditoria creado.
	 */
	private Long createScanDocAuditLogs(final ScannedDocumentDTO doc, final String username) throws DAOException {
		Long res = null;
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;

		auditOperation = new AuditOperationDTO();
		auditOperation.setName(AuditOperationName.SCAN_PROCESS);
		auditOperation.setStartDate(doc.getDigProcStartDate());
		auditOperation.setLastUpdateDate(doc.getDigProcStartDate());
		auditEvent = new AuditEventDTO();
		auditOperation.setStatus(AuditOperationStatus.OPEN);
		auditEvent.setEventDate(doc.getDigProcStartDate());
		auditEvent.setEventName(AuditEventName.SCAN_PROC_SCAN_DOC);
		auditEvent.setEventResult(AuditOpEventResult.OK);
		auditEvent.setEventUser(username);
		additionalInfo = "Captura de imagen del documento " + doc.getId() + " realizada correctamente.";
		auditEvent.setAdditionalInfo(additionalInfo);

		auditEvents.add(auditEvent);

		res = auditService.addNewAuditLogs(auditOperation, auditEvents, Boolean.FALSE);

		return res;
	}

	/**
	 * Obtenemos el ScanProfileOrgUnitDocSpecId según el identificativo del
	 * documento escaneado.
	 * 
	 * @param docSpecificationsByScanProfileOrgUnitId
	 *            Mapa que relaciona la especificacion de documentos con las
	 *            unidades organizativas
	 * @param idTypeDoc
	 *            identificador del tipo de documento
	 * @return Id de la entidad ScanProfileOrgUnitDocSpec
	 */
	private Long getScanProfileOrgUnitDocSpecId(final Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> docSpecificationsByScanProfileOrgUnitId, final Long idTypeDoc) {
		Long scanProfileOrgUnitDocSpecId = null;

		for (DocumentSpecificationDTO key: docSpecificationsByScanProfileOrgUnitId.keySet()) {
			if (key.getId() == idTypeDoc) {
				scanProfileOrgUnitDocSpecId = docSpecificationsByScanProfileOrgUnitId.get(key).getId();
			}
		}
		return scanProfileOrgUnitDocSpecId;

	}

	/**
	 * Recupera la especificacion de documentos y relacion de metadataos por
	 * defecto, según el tipo de documento.
	 * 
	 * @param docSpecId
	 *            identificador de definición documental.
	 * @return Map con la especificación de metadatos y metadata relacionado con
	 *         un tipo de documento.
	 * @throws ScanDocsException
	 *             Si se produce algún error al recuperar los metadatos del
	 *             documento según el tipo de documento.
	 */
	private Map<MetadataSpecificationDTO, MetadataDTO> recoveryDocMetadataByTypeId(final Long docSpecId) throws ScanDocsException {
		Map<MetadataSpecificationDTO, MetadataDTO> res = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		MetadataDTO nuevo;
		try {
			Map<String, List<MetadataSpecificationDTO>> mapMetadata = webscanModelManager.getMetadaSpecificationsByDocSpec(docSpecId);
			List<MetadataSpecificationDTO> metadataSpecificationDTO = new ArrayList<MetadataSpecificationDTO>();
			// obtenemos todas las metadataSpecification
			for (String key: mapMetadata.keySet()) {
				metadataSpecificationDTO.addAll(mapMetadata.get(key));
			}

			// vamos recorriendo cada metadataSpecification y asignamos el
			// MetadataDTO por defecto
			for (MetadataSpecificationDTO metadataSpec: metadataSpecificationDTO) {
				// creamos el metadata asociada
				nuevo = new MetadataDTO();
				nuevo.setValue(metadataSpec.getDefaultValue());
				res.put(metadataSpec, nuevo);
			}
		} catch (DAOException e) {
			LOG.debug("Error al recuperar la especificacion de metadatos del tipo de documentos");
			throw new ScanDocsException(ScanDocsException.CODE_999, e.getMessage());// NOPMD
		} catch (Exception e) {
			LOG.debug("Error no esperado al recuperar la especificacion de metadatos del tipo de documentos");
			throw new ScanDocsException(ScanDocsException.CODE_999, e.getMessage());// NOPMD
		}

		return res;
	}

	/**
	 * Función que devuelve el tamaño en byte del pixel type, es decir, byte que
	 * se necesita para almacenar info.
	 * 
	 * @param idOutputPixelType
	 *            Tipo de Pixel del formato del documento escaneado.
	 * @return String con el número de byte definido para recoger profundida en
	 *         byte
	 */
	private String getByteDeep(final String idOutputPixelType) {
		String res = Integer.toString(PixelType.RGB.getDeepBits());

		if (idOutputPixelType != null && !idOutputPixelType.isEmpty() && idOutputPixelType.equals("-1")) {
			res = Integer.toString(PixelType.getByDeepBitsName(idOutputPixelType).getDeepBits());
		}

		// devuelve el tamaño en byte del pixel type
		return res;
	}

	/**
	 * Obtiene la versión del formato de los documentos.
	 * 
	 * @param idOutputFormat
	 *            Formato de salida del documento.
	 * @return versión del formato generada.
	 */
	private String getMetadataVersionImage(final String idOutputFormat) {
		String res = null;

		if (idOutputFormat != null) {
			if (idOutputFormat.toUpperCase().contains(PDF_FORMAT)) {
				res = WebscanConstants.VERSION_PDF.toString();
			} else if (idOutputFormat.toUpperCase().contains(JPEG_FORMAT)) {
				res = ImageProcessor.VERSION_JPG;
			} else if (idOutputFormat.toUpperCase().contains(PNG_FORMAT)) {
				res = ImageProcessor.VERSION_PNG;
			} else if (idOutputFormat.toUpperCase().contains(TIFF_FORMAT)) {
				res = ImageProcessor.VERSION_TIFF;
			}
		}

		return res;
	}

	/**
	 * Obtiene la extensión asociada al formato de los documentos.
	 * 
	 * @param idOutputFormat
	 *            Formato de salida del documento.
	 * @return la extensión asociada al formato de los documentos.
	 */
	private String getMetadataExtensionTypeImage(final String idOutputFormat) {
		String res = null;

		if (idOutputFormat != null) {
			if (idOutputFormat.toUpperCase().contains(PDF_FORMAT)) {
				res = MimeType.PDF_CONTENT.getExtension();
			} else if (idOutputFormat.toUpperCase().contains(JPEG_FORMAT)) {
				res = MimeType.JPG_IMG_CONTENT.getExtension();
			} else if (idOutputFormat.toUpperCase().contains(PNG_FORMAT)) {
				res = MimeType.PNG_IMG_CONTENT.getExtension();
			} else if (idOutputFormat.toUpperCase().contains(TIFF_FORMAT)) {
				res = MimeType.TIFF_IMG_CONTENT.getExtension();
			}
		}

		return res;
	}

	/**
	 * Funcion para recuperar la información parametrizada para los metadatos de
	 * escaneos.
	 * 
	 * @param parameters
	 *            listado de parametros de escaneo.
	 * @return Map con la información asociada a los parametros y metadatos de
	 *         la fase de escaneo
	 */
	private Map<String, String> getScanMetadataByParam(final List<ParameterDTO> parameters) {
		Map<String, String> res = new HashMap<String, String>();
		for (ParameterDTO par: parameters) {
			// scan.doc.metadata
			if (par.getSpecificationName().startsWith(SCAN_DOC_META)) {
				res.put(par.getSpecificationName(), par.getValue());
			}
		}
		return res;
	}

	private Map<String, String> getScanMetadataNamesFromPlugin(Long scanProfileId, Long orgUnitId) throws ScanDocsException {
		Map<String, String> res = new HashMap<String, String>();
		try {
			// obtenemos la información de los metadatos de la fase de captura
			// de imágenes
			ScanProcessStageDTO stageCapture = getScanProcStageByName(SCAN);
			ScanProfileOrgUnitDTO scanProfileOrgUnit = getScanProfileOrgUnitByScanProfileAndOrgUnit(scanProfileId, orgUnitId);
			List<ParameterDTO> parameters = getPluginParametersByScanProfileOrgUnit(scanProfileOrgUnit, stageCapture);
			// me quedo sólo con los parameter scan.doc.metadata....
			res = getScanMetadataByParam(parameters);
		} catch (DAOException e) {
			throw new ScanDocsException(ScanDocsException.CODE_024, "Error al recuperar del modelo de datos el identificador de los metadatos a establecer en la fase de captura: " + e.getMessage());// NOPMD
		}

		return res;
	}

	/**
	 * Método que obtiene el valor de los metadatos calculados automáticamente
	 * en la fase de captura de imágenes.
	 * 
	 * @param batchScanDoc
	 *            lote de documentos.
	 * @param fieldRequest
	 *            Parametros recibidos de la captura
	 * @param scanMetadataNames
	 *            Identificador de los metadatos que deben ser calculado
	 *            automáticamente en la fase de captura de imágenes.
	 * @param metadataCollection
	 *            Collection de metadatas obtenidos previamente
	 * @param userSession
	 *            información de usuario de session
	 * @return Map con la relacion de metadata name-valor
	 * @throws ScanDocsException
	 *             Exception producida al calcular el valor de los metadatos,
	 *             calculados automáticamente en la fase de captura de imágenes,
	 *             para un documento digitalizado.
	 */
	private Map<String, String> getScanMetadataCollection(final BatchScanDoc batchScanDoc, final PerformScanDocVO fieldRequest, final Map<String, String> scanMetadataNames, final Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection, final UserInSession userSession) throws ScanDocsException {
		Map<String, String> res = new HashMap<String, String>();
		Map<String, String> aMetadataValue = new HashMap<String, String>();

		try {
			// recorremos los metadatos y asignamos los valores
			// scan.doc.metadata.inDate
			aMetadataValue = generateInDateMetadata(scanMetadataNames.get(SCDC_META_INDATE), metadataCollection, fieldRequest);
			if (!aMetadataValue.isEmpty()) {
				res.putAll(aMetadataValue);
			}
			// scan.doc.metadata.format.name
			addMetadataValueCommon(res, scanMetadataNames.get(SCDC_META_NAME), fieldRequest.getIdOutputFormat(), metadataCollection);
			// scan.doc.metadata.format.extension
			addMetadataValueCommon(res, scanMetadataNames.get(SCDC_META_EXT), getMetadataExtensionTypeImage(fieldRequest.getIdOutputFormat()), metadataCollection);
			// scan.doc.metadata.format.version
			addMetadataValueCommon(res, scanMetadataNames.get(SCDC_META_VER), getMetadataVersionImage(fieldRequest.getIdOutputFormat()), metadataCollection);
			// scan.doc.metadata.resolution
			addMetadataValueCommon(res, scanMetadataNames.get(SCDC_META_RES), fieldRequest.getIdOutputResolution().toString(), metadataCollection);
			// scan.doc.metadata.byteDeep
			addMetadataValueCommon(res, scanMetadataNames.get(SCDC_META_BDEEP), getByteDeep(fieldRequest.getIdOutputPixelType()), metadataCollection);
			// scan.doc.metadata.organization
			if (fieldRequest.getUserDptoId() != null && !fieldRequest.getUserDptoId().isEmpty()) {
				addMetadataValueCommon(res, scanMetadataNames.get(SCDC_META_ORG), fieldRequest.getUserDptoId(), metadataCollection);
			} else {
				if (batchScanDoc != null && (BatchType.BOOKING.equals(batchScanDoc.getBatchType()) || BatchType.SPACER.equals(batchScanDoc.getBatchType()))) {
					addMetadataValueCommon(res, scanMetadataNames.get(SCDC_META_ORG), batchScanDoc.getDepartment(), metadataCollection);
				}
			}
		} catch (Exception e) {
			throw new ScanDocsException(ScanDocsException.CODE_024, "Error estableciendo los metadatos automáticos de la fase de captura: " + e.getMessage());// NOPMD
		}

		return res;
	}

	/**
	 * Metodo para añadir los metadatos al parámetro res de de la fase de
	 * escaneo, según la metadataCollection y su definición.
	 * 
	 * @param res
	 *            Map resultante de añadir los metadatos asociados al
	 *            pluginParamValue pasado como parámetro
	 * @param pluginParamValue
	 *            Identificador de los metadatos del parametro de plugin
	 * @param value
	 *            Valor asociado al metadato
	 * @param metadataCollection
	 *            Metadata Collection asociada al plugin de escaneo
	 */
	private void addMetadataValueCommon(Map<String, String> res, final String pluginParamValue, final String value, final Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) {
		Map<String, String> aMetadataValue = new HashMap<String, String>();
		if (value != null) {
			aMetadataValue = generateCommonMetadata(pluginParamValue, metadataCollection, value);
			if (!aMetadataValue.isEmpty()) {
				res.putAll(aMetadataValue);
			}
		}
	}

	/**
	 * Establece los valores de los metadatos que especifican la fecha de
	 * entrada del documento.
	 * 
	 * @param pluginParamValue
	 *            lista de nombres de parametros separada por comas.
	 * @param metadataCollection
	 *            Relación de metadatos asociados a la definición de documentos
	 *            seleccionada para los documentos digitalizados.
	 * @param fieldRequest
	 *            Parámetros de la petición web.
	 * @return Relación de metadatos en el que se detalla el nombre y su valor.
	 */
	private Map<String, String> generateInDateMetadata(final String pluginParamValue, final Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection, final PerformScanDocVO fieldRequest) {
		Boolean found;
		Map<String, String> res = new HashMap<String, String>();
		MetadataSpecificationDTO metadataSpec;
		String value;
		String[ ] metadataNames;

		if (pluginParamValue != null) {
			metadataNames = pluginParamValue.split(",");
			// para reserva se coge la fecha de validacion de su uso
			if (fieldRequest.getDateBookingOK() != null) {
				value = webscanUtilParams.formatDateTime(fieldRequest.getDateBookingOK());
			} else {
				value = webscanUtilParams.formatDateTime(fieldRequest.getStartDate());
			}

			for (int i = 0; i < metadataNames.length; i++) {
				found = Boolean.FALSE;
				for (Iterator<MetadataSpecificationDTO> it = metadataCollection.keySet().iterator(); !found && it.hasNext();) {
					metadataSpec = it.next();
					if (metadataSpec.getName().equals(metadataNames[i])) {
						res.put(metadataSpec.getName(), value);
						found = true;
					}
				}
			}
		}

		return res;
	}

	/**
	 * Establece los valores de los metadatos de tipo cadena de caracteres.
	 * 
	 * @param pluginParamValue
	 *            lista de nombres de parametros separada por comas.
	 * @param metadataCollection
	 *            Relación de metadatos asociados a la definición de documentos
	 *            seleccionada para los documentos digitalizados.
	 * @param value
	 *            valor asignado a los metadatos especificados por el parámetro
	 *            pluginParamValue.
	 * @return Relación de metadatos en el que se detalla el nombre y su valor.
	 */
	private Map<String, String> generateCommonMetadata(final String pluginParamValue, final Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection, final String value) {
		Boolean found;
		Map<String, String> res = new HashMap<String, String>();
		MetadataSpecificationDTO metadataSpec;
		String[ ] metadataNames;

		if (pluginParamValue != null) {
			metadataNames = pluginParamValue.split(",");

			for (int i = 0; i < metadataNames.length; i++) {
				found = Boolean.FALSE;
				for (Iterator<MetadataSpecificationDTO> it = metadataCollection.keySet().iterator(); !found && it.hasNext();) {
					metadataSpec = it.next();
					if (metadataSpec.getName().equals(metadataNames[i])) {
						res.put(metadataSpec.getName(), value);
						found = true;
					}
				}
			}
		}

		return res;
	}

	/**
	 * Inidica si un tipo de separador es un separador de documentos
	 * reutilizable (true).
	 * 
	 * @param scannedImageType
	 *            tipo de separador a verificar.
	 * @return Si el tipo de separador es un separador de documentos
	 *         reutilizable, devuelve true.
	 */
	private Boolean isDocSpacer(final ScannedImageType scannedImageType) {
		return ScannedImageType.SPACER_DOC.equals(scannedImageType) || ScannedImageType.SPACER_BLANK.equals(scannedImageType);
	}

	/**
	 * Método que recopila los parámetros implicados en el cálculo del área de
	 * búsqueda de separadores (scan.spacer.search.area.x.init,
	 * scan.spacer.search.area.y.init, scan.spacer.search.area.x.size y
	 * scan.spacer.search.area.y.size).
	 * 
	 * @param scanPluginParams
	 *            Parámetros de la instancia del plugin de captura de imágenes
	 *            configurada para el procesamiento actual.
	 * @return Relación de pares clave/valor que intervienen en la detección de
	 *         separadores.
	 */
	private Properties extractSpacerSearchingParams(Properties scanPluginParams) {
		Properties res = new Properties();
		LOG.debug("[WEBSCAN-SCANDOCS-SERV] Se inicia la recuperación de los parámetros para la detección de separadores...");

		res.setProperty(ScanDocsConstants.SPACER_SEARCH_AREA_X0, searchParam(scanPluginParams, ScanDocsConstants.SPACER_SEARCH_AREA_X0));
		res.setProperty(ScanDocsConstants.SPACER_SEARCH_AREA_Y0, searchParam(scanPluginParams, ScanDocsConstants.SPACER_SEARCH_AREA_Y0));
		res.setProperty(ScanDocsConstants.SPACER_SEARCH_AREA_X_SIZE, searchParam(scanPluginParams, ScanDocsConstants.SPACER_SEARCH_AREA_X_SIZE));
		res.setProperty(ScanDocsConstants.SPACER_SEARCH_AREA_Y_SIZE, searchParam(scanPluginParams, ScanDocsConstants.SPACER_SEARCH_AREA_Y_SIZE));

		LOG.debug("[WEBSCAN-SCANDOCS-SERV] Finalizada la recuperación de los parámetros para la detección de separadores.");
		return res;
	}

	/**
	 * Funcion privada que realiza el registro de una estadística de la
	 * digitalización de documentos, en ella comprobamos si la estadística tiene
	 * una reserva y en ese caso se actualizaría la estadistica creada
	 * anteriormente.
	 * 
	 * @param fieldRequest
	 *            Parametros recibidos de la captura.
	 * @param userSession
	 *            usuario que solicita el procesamiento de la imagen capturada.
	 * @param batchScanDoc
	 *            Objeto con la información asociada al lote del documento
	 *            escaneado.
	 * @param batchDocs
	 *            Documentos pertenecientes al lote.
	 * @return DocScanStatisticsDTO
	 * @throws StatisticsDAOException
	 *             Si se produce algún error en el registro de la estadística de
	 *             la digitalización de documentos
	 */
	private DocScanStatisticsDTO registerStatistics(UserInSession userSession, BatchScanDoc batchScanDoc, PerformScanDocVO fieldRequest, List<ScannedDoc> batchDocs) throws StatisticsDAOException {
		ScanWorkingMode workMode = ScanWorkingMode.STAND_ALONE;
		DocScanStatisticsDTO docScanStatisticsDTO = new DocScanStatisticsDTO();

		// Es un lote de documentos
		if (BatchType.BOOKING.equals(batchScanDoc.getBatchType())) {
			// Reserva de trabajo de digitalización
			workMode = ScanWorkingMode.ON_LINE;
		} else if (BatchType.SPACER.equals(batchScanDoc.getBatchType())) {
			if (batchScanDoc.getSpacerId() != null) {
				// Lote mediante carútula
				// Verificamos si es remota o local
				try {
					docScanStatisticsDTO = statisticsModelManager.getDocScanStatisticsByRemoteReq(batchScanDoc.getSpacerId());
					workMode = ScanWorkingMode.REMOTE_SPACER;
				} catch (StatisticsDAOException e) {
					workMode = ScanWorkingMode.LOCAL_SPACER;
				}
			}
		}

		docScanStatisticsDTO.setScanTemplateId(fieldRequest.getScanTemplateId());
		docScanStatisticsDTO.setOrgUnitId(fieldRequest.getOrgUnitId());
		docScanStatisticsDTO.setUserDpto(fieldRequest.getUserDptoId());
		docScanStatisticsDTO.setWorkingMode(workMode);
		docScanStatisticsDTO.setFileFormat(batchDocs.get(0).getMimeType().getType());
		docScanStatisticsDTO.setResolution(fieldRequest.getIdOutputResolution());
		docScanStatisticsDTO.setPixelType(fieldRequest.getIdOutputPixelType());
		docScanStatisticsDTO.setSpacerProcDisabled(fieldRequest.getSpacerSearchDisabled());
		docScanStatisticsDTO.setScanWorkSize(batchDocs.size());
		Integer numPag = 0;
		for (ScannedDoc scd: batchDocs) {
			numPag += scd.getNumPage().intValue();
		}
		docScanStatisticsDTO.setScanWorkNumPages(numPag);
		docScanStatisticsDTO.setDocScanInitDate(fieldRequest.getStartDate());
		docScanStatisticsDTO.setDocScanEndDate(new Date());

		// Creamos la estadística
		if (ScanWorkingMode.REMOTE_SPACER.equals(workMode)) {
			statisticsModelManager.updateDocScanStatistics(docScanStatisticsDTO);
		} else {
			statisticsModelManager.createDocScanStatistics(docScanStatisticsDTO);
		}

		return docScanStatisticsDTO;
	}

	/**
	 * Establece los parámetros por defecto del plugin de digitalización.
	 * 
	 * @param aScanDocsConfParams
	 *            parámetros por defecto del plugin de digitalización.
	 */
	public void setScanDocsConfParams(Properties aScanDocsConfParams) {
		this.scanDocsConfParams = aScanDocsConfParams;
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Objeto que recopila los mensajes, sujetos a multidioma, que
	 *            son presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Establece el servicio interno para el registro y consulta de trazas y
	 * eventos de auditoría de Webscan.
	 * 
	 * @param anAuditManager
	 *            nuevo servicio interno para el registro y consulta de trazas y
	 *            eventos de auditoría de Webscan.
	 */
	public void setAuditService(AuditService anAuditManager) {
		this.auditService = anAuditManager;
	}

	public void setSpacersModelManager(SpacersModelManager spacersModelManager) {
		this.spacersModelManager = spacersModelManager;
	}

	/**
	 * Establece el objeto que habilita la ejecución asíncrona del proceso de
	 * digitalización.
	 * 
	 * @param anAsyncScanProcessManager
	 *            objeto que habilita la ejecución asíncrona del proceso de
	 *            digitalización.
	 */
	public void setAsyncScanProcessManager(AsyncScanProcessManager anAsyncScanProcessManager) {
		this.asyncScanProcessManager = anAsyncScanProcessManager;
	}

	/**
	 * Establece la clase que habilita el acceso a los parámetros de
	 * configuración del Sistema.
	 * 
	 * @param aWebscanUtilParams
	 *            clase que habilita el acceso a los parámetros de configuración
	 *            del Sistema.
	 */
	public void setWebscanUtilParams(Utils aWebscanUtilParams) {
		this.webscanUtilParams = aWebscanUtilParams;
	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos de las estadísticas de la
	 * digitalización de documentos.
	 * 
	 * @param aStatisticsModelManager
	 */
	public void setStatisticsModelManager(StatisticsModelManager aStatisticsModelManager) {
		this.statisticsModelManager = aStatisticsModelManager;
	}

}
