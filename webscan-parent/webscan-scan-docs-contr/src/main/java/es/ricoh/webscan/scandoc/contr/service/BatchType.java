/** 
* <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.service.BatchType.java.</p>
* <b>Descripción:</b><p> Tipos de lotes de documentos admitidos por el sistema.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.scandoc.contr.service;

/**
 * Tipos de lotes de documentos admitidos por el sistema.
 * <p>
 * Clase BatchType.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum BatchType {
	/**
	 * Lote de documentos por reserva.
	 */
	BOOKING,
	/**
	 * Lote de documentos por carátula.
	 */
	SPACER,
	/**
	 * Lote de documentos por defecto.
	 */
	DEFAULT;
}
