/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.controller.vo.enumErrorPerformScanDocVO.java.</p>
 * <b>Descripción:</b><p> Enumerado que asocia los campos de captura con los errores de pantalla al
 * validar.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.scandoc.contr.controller.vo;

/**
 * Enumerado que asocia los campos de captura con los errores de pantalla al
 * validar.
 * <p>
 * Clase EnumErrorPerformScanDocVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum EnumErrorPerformScanDocVO {
	/**
	 * Enumerados de los campos de captura de errores de la pantalla.
	 */
	orgUnitId("message.performScan.orgUnitId"), scanProfileId("message.performScan.scanProfileId"), scanBookId("message.performScan.scanBookId"), idTypeDoc("message.performScan.idTypeDoc"), idOutputFormat("message.performScan.idOutputFormat"), idOutputResolution("message.performScan.idOutputResolution"), idOutputPixelType("message.performScan.idOutputPixelType"), idDocBatchEnable("message.performScan.idDocBatchEnable"), idDocBatchSpacerType("message.performScan.idDocBatchSpacerType"), idDocBatchDocSpacerType("message.performScan.idDocBatchDocSpacerType"), enableNumberDoc("message.performScan.enableNumberDoc"), idDocBatchValNumDocs("message.performScan.idDocBatchValNumDocs"), enableNumberPagDoc("message.performScan.enableNumberPagDoc"), idDocValNumPagsDoc("message.performScan.idDocValNumPagsDoc");

	/**
	 * String con el mensaje a mostrar.
	 */
	private String message;

	/**
	 * Constructor method for the class EnumErrorPerformScanDocVO.java.
	 * 
	 * @param amessage
	 *            String con el mensaje a mostrar.
	 */
	EnumErrorPerformScanDocVO(String amessage) {
		this.message = amessage;
	}

	/**
	 * Obtiene la cadena del mensaje asociado.
	 * 
	 * @return String con el mensaje.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Obtiene la definición del elemento enumerado por la propiedad.
	 * 
	 * @param value
	 *            String con el nombre a buscar.
	 * @return info del formato .
	 */

	public static final EnumErrorPerformScanDocVO getByName(String value) {
		EnumErrorPerformScanDocVO res = null;
		for (EnumErrorPerformScanDocVO aux: EnumErrorPerformScanDocVO.values()) {
			if (aux.message.equalsIgnoreCase(value)) {
				res = aux;
			}
		}
		return res;
	}

}
