/**
 * Objetos de la capa de presentación del componente de digitalización de
 * documentos de la solución de digitalización certificada y copia auténtica de
 * la Generalitat Valenciana.
 */
package es.ricoh.webscan.scandoc.contr.controller.vo;