/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.controller.vo.CheckWorkingBookingAux.java.</p>
 * <b>Descripción:</b><p> Entidad auxiliar para recuperar la información al comprobar la existencia de
 * reserva.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.scandoc.contr.controller.vo;

/**
 * Entidad auxiliar para recuperar la información al comprobar la existencia de
 * reserva.
 * <p>
 * Class CheckWorkingBookingAux .
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class CheckWorkingBookingAux {

	/**
	 * Identificador del perfil de escaneo de la reserva.
	 */
	private Long scanProfileBookId = null;
	/**
	 * Identificador de la unidad organizativa de la reserva.
	 */
	private Long orgUnitBookId = null;
	/**
	 * Identificador de la especificación de documento de la reserva.
	 */
	private Long docSpecBookId = null;

	/**
	 * Constructor sin parámetros.
	 */
	public CheckWorkingBookingAux() {

	}

	/**
	 * Gets the value of the attribute {@link #scanProfileBookId}.
	 * 
	 * @return the value of the attribute {@link #scanProfileBookId}.
	 */
	public Long getScanProfileBookId() {
		return scanProfileBookId;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfileBookId}.
	 * 
	 * @param aScanProfileBookId
	 *            The value for the attribute {@link #scanProfileBookId}.
	 */
	public void setScanProfileBookId(Long aScanProfileBookId) {
		this.scanProfileBookId = aScanProfileBookId;
	}

	/**
	 * Gets the value of the attribute {@link #orgUnitBookId}.
	 * 
	 * @return the value of the attribute {@link #orgUnitBookId}.
	 */
	public Long getOrgUnitBookId() {
		return orgUnitBookId;
	}

	/**
	 * Sets the value of the attribute {@link #orgUnitBookId}.
	 * 
	 * @param aOrgUnitBookId
	 *            The value for the attribute {@link #orgUnitBookId}.
	 */
	public void setOrgUnitBookId(Long aOrgUnitBookId) {
		this.orgUnitBookId = aOrgUnitBookId;
	}

	/**
	 * Gets the value of the attribute {@link #docSpecBookId}.
	 * 
	 * @return the value of the attribute {@link #docSpecBookId}.
	 */
	public Long getDocSpecBookId() {
		return docSpecBookId;
	}

	/**
	 * Sets the value of the attribute {@link #docSpecBookId}.
	 * 
	 * @param aDocSpecBookId
	 *            The value for the attribute {@link #docSpecBookId}.
	 */
	public void setDocSpecBookId(Long aDocSpecBookId) {
		this.docSpecBookId = aDocSpecBookId;
	}

}
