/** 
* <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.util.ScannedImageType.java.</p>
* <b>Descripción:</b><p> Tipos de imagen capturada.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
* Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.scandoc.contr.util;

/**
 * Tipos de imagen capturada.
 * <p>
 * Clase ScannedImageType.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum ScannedImageType {

	/**
	 * Carátula de lote de documentos.
	 */
	SPACER_BATCH("SPACER BATCH"),
	/**
	 * Separador reutilizable de documentos.
	 */
	SPACER_DOC("SPACER DOC"),
	/**
	 * Página en blanco (carilla).
	 */
	SPACER_BLANK("SPACER BLANK"),
	/**
	 * Contenido de documentos.
	 */
	DOC_CONTENT("DOC CONTENT");

	/**
	 * Denominación de tipo de imagen capturada.
	 */
	private String name;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aName
	 *            Denominación de tipo de imagen capturada.
	 */
	ScannedImageType(String aName) {
		this.name = aName;

	}

	/**
	 * Obtiene la denominación de tipo de imagen capturada.
	 * 
	 * @return la denominación de tipo de imagen capturada.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Obtiene la denominación de tipo de imagen capturada.
	 * 
	 * @param value denominación de tipo de imagen capturada.
	 * 
	 * @return la denominación de tipo de imagen capturada.
	 */

	public static final ScannedImageType getByName(String value) {
		ScannedImageType res = null;
		for (ScannedImageType aux: ScannedImageType.values()) {
			if (aux.name.equalsIgnoreCase(value)) {
				res = aux;
				break;
			}
		}
		return res;
	}

}
