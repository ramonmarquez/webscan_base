/** 
 * <b>File:</b><p>es.ricoh.webscan.scandoc.contr.controller.vo.MessageShowAux.java.</p>
 * <b>Description:</b><p> .</p>
 * <b>Project:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.scandoc.contr.controller.vo;

/**
 * Entidad auxiliar para mostrar la información asociada a la reserva en caso de
 * existir.
 * <p>
 * Class MessageShowAux.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class MessageShowAux {

	/**
	 * String con el tipo de mensaje a mostrar con respecto a la reserva.
	 */
	private String typeMessage = null;
	/**
	 * String con la información del mensaje a mostrar con respecto a la
	 * reserva.
	 */
	private String messageShow = null;

	/**
	 * Constructor method for the class MessageShowAux.java.
	 */
	public MessageShowAux() {
	}

	/**
	 * Gets the value of the attribute {@link #typeMessage}.
	 * 
	 * @return the value of the attribute {@link #typeMessage}.
	 */
	public String getTypeMessage() {
		return typeMessage;
	}

	/**
	 * Sets the value of the attribute {@link #typeMessage}.
	 * 
	 * @param aTypeMessage
	 *            The value for the attribute {@link #typeMessage}.
	 */
	public void setTypeMessage(String aTypeMessage) {
		this.typeMessage = aTypeMessage;
	}

	/**
	 * Gets the value of the attribute {@link #messageShow}.
	 * 
	 * @return the value of the attribute {@link #messageShow}.
	 */
	public String getMessageShow() {
		return messageShow;
	}

	/**
	 * Sets the value of the attribute {@link #messageShow}.
	 * 
	 * @param aMessageShow
	 *            The value for the attribute {@link #messageShow}.
	 */
	public void setMessageShow(String aMessageShow) {
		this.messageShow = aMessageShow;
	}

}
