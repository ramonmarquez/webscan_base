/** 
* <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.util.TypeIdentScanDoc.java.</p>
* <b>Descripción:</b><p> Identificadores de separadores de lotes y documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
* Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.scandoc.contr.util;

import es.ricoh.webscan.spacers.business.service.SpacersGenerator;

/**
 * Identificadores de los separadores de lotes y documentos.
 * <p>
 * Clase TypeIdentScanDoc.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum TypeIdentScanDoc {

	/**
	 * Identificador de lote de documentos.
	 */
	BATCH(SpacersGenerator.BATCH_CODE),
	/**
	 * Identificador de separador de documentos.
	 */
	DOCUM(SpacersGenerator.DOCUMENT_CODE);

	/**
	 * Identificador de separador de lote o documentos.
	 */
	private String name;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aName
	 *            Identificador de separador de lote o documentos.
	 */
	TypeIdentScanDoc(String aName) {
		this.name = aName;

	}

	/**
	 * Obtiene el identificador de separador de lote o documentos.
	 * 
	 * @return el identificador de separador de lote o documentos.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Obtiene el identificador de separador de lote o documentos partir de su denominación.
	 * 
	 * @param value denominación del identificador de separador de lote o documentos.
	 * 
	 * @return el identificador de separador de lote o documentos.
	 */

	public static final TypeIdentScanDoc getByName(String value) {
		TypeIdentScanDoc res = null;
		for (TypeIdentScanDoc aux: TypeIdentScanDoc.values()) {
			if (aux.name.equalsIgnoreCase(value)) {
				res = aux;
				break;
			}
		}
		return res;
	}

}
