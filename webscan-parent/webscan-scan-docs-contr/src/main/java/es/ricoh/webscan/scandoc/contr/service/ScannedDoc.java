/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.service.ScanDocsService.java.</p>
 * <b>Descripción:</b><p> Documento en proceso de digitalización.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH España IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.scandoc.contr.service;

import java.util.List;
import java.util.Map;

import es.ricoh.webscan.spacers.model.domain.DocSpacerType;
import es.ricoh.webscan.utilities.MimeType;

/**
 * Documento en proceso de digitalización.
 * <p>
 * Clasw ScannedDoc.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ScannedDoc {

	/**
	 * Indices de las imágenes capturadas que conforman el documento escaneado.
	 */
	private List<Integer> listImage;

	/**
	 * Conjunto metadatos, identificador por pareja <nombre, valor> si Reserva.
	 */
	private Map<String, String> metadata;
	/**
	 * Numero de paginas del documento.
	 */
	private Long numPage;

	/**
	 * Numero de orden del documento en el lote.
	 */
	private Long numOrder;

	/**
	 * Denominación del documento.
	 */
	private String name;

	/**
	 * Mimetype del documento.
	 */
	private MimeType mimeType;
	/**
	 * Tipo de Separador del documento.
	 */
	private DocSpacerType docSpacerType = null;

	/**
	 * Constructor method for the class ScannedDoc.java.
	 */
	public ScannedDoc() {
		super();
	}

	/**
	 * Gets the value of the attribute {@link #metadata}.
	 * 
	 * @return the value of the attribute {@link #metadata}.
	 */
	public Map<String, String> getMetadata() {
		return metadata;
	}

	/**
	 * Sets the value of the attribute {@link #metadata}.
	 * 
	 * @param ametadata
	 *            The value for the attribute {@link #metadata}.
	 */
	public void setMetadata(Map<String, String> ametadata) {
		this.metadata = ametadata;
	}

	/**
	 * Gets the value of the attribute {@link #numPage}.
	 * 
	 * @return the value of the attribute {@link #numPage}.
	 */
	public Long getNumPage() {
		return numPage;
	}

	/**
	 * Sets the value of the attribute {@link #numPage}.
	 * 
	 * @param anumPage
	 *            The value for the attribute {@link #numPage}.
	 */
	public void setNumPage(Long anumPage) {
		this.numPage = anumPage;
	}

	/**
	 * Gets the value of the attribute {@link #numOrder}.
	 * 
	 * @return the value of the attribute {@link #numOrder}.
	 */
	public Long getNumOrder() {
		return numOrder;
	}

	/**
	 * Sets the value of the attribute {@link #numOrder}.
	 * 
	 * @param anumOrder
	 *            The value for the attribute {@link #numOrder}.
	 */
	public void setNumOrder(Long anumOrder) {
		this.numOrder = anumOrder;
	}

	/**
	 * Gets the value of the attribute {@link #name}.
	 * 
	 * @return the value of the attribute {@link #name}.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the attribute {@link #name}.
	 * 
	 * @param aName
	 *            The value for the attribute {@link #name}.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Gets the value of the attribute {@link #listImage}.
	 * 
	 * @return the value of the attribute {@link #listImage}.
	 */
	public List<Integer> getListImage() {
		return listImage;
	}

	/**
	 * Sets the value of the attribute {@link #listImage}.
	 * 
	 * @param alistImage
	 *            The value for the attribute {@link #listImage}.
	 */
	public void setListImage(List<Integer> alistImage) {
		this.listImage = alistImage;
	}

	/**
	 * Gets the value of the attribute {@link #mimeType}.
	 * 
	 * @return the value of the attribute {@link #mimeType}.
	 */
	public MimeType getMimeType() {
		return mimeType;
	}

	/**
	 * Sets the value of the attribute {@link #mimeType}.
	 * 
	 * @param amimeType
	 *            The value for the attribute {@link #mimeType}.
	 */
	public void setMimeType(MimeType amimeType) {
		this.mimeType = amimeType;
	}

	/**
	 * Gets the value of the attribute {@link #docSpacerType}.
	 * 
	 * @return the value of the attribute {@link #docSpacerType}.
	 */
	public DocSpacerType getDocSpacerType() {
		return docSpacerType;
	}

	/**
	 * Sets the value of the attribute {@link #docSpacerType}.
	 * 
	 * @param aDocSpacerType
	 *            The value for the attribute {@link #docSpacerType}.
	 */
	public void setDocSpacerType(DocSpacerType aDocSpacerType) {
		this.docSpacerType = aDocSpacerType;
	}

}
