/** 
* <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.util.TiffImageProcessor.java.</p>
* <b>Descripción:</b><p> .</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.scandoc.contr.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.CCITTFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import es.ricoh.webscan.spacers.model.domain.DocSpacerType;
import es.ricoh.webscan.scandoc.contr.ScanDocsException;
import es.ricoh.webscan.scandoc.contr.controller.vo.PerformScanDocVO;
import es.ricoh.webscan.scandoc.contr.service.ScanDocRequest;
import es.ricoh.webscan.scandoc.contr.service.ScannedDoc;
import es.ricoh.webscan.utilities.MimeType;

/**
 * <p>
 * Clase TiffImageProcessor.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class TiffImageProcessor extends ImageProcessor {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(TiffImageProcessor.class);

	/**
	 * Clase responsable de la lectura y decodificación de imágenes.
	 */
	private ImageReader reader;

	public TiffImageProcessor() throws ScanDocsException {
		super();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.scandoc.contr.util.ImageProcessor#split2Image(java.util.Map,
	 *      es.ricoh.webscan.scandoc.contr.controller.vo.PerformScanDocVO,
	 *      java.util.Properties)
	 */
	@Override
	public List<ScanDocRequest> split2Image(Map<String, MultipartFile> images, PerformScanDocVO fieldRequest, Properties spacerSearchingParams) throws ScanDocsException {
		List<ScanDocRequest> res = new ArrayList<ScanDocRequest>();
		Long t0;
		Long tf;
		MultipartFile image;

		image = images.get(images.keySet().iterator().next());

		t0 = System.currentTimeMillis();
		LOG.debug("Se inicia el procesamiento de las imágenes capturadas. Procesamiento de separadores deshabilitado: {}.", fieldRequest.getSpacerSearchDisabled());

		if (fieldRequest.getSpacerSearchDisabled()) {
			// Se ha solicitado que no se detecten carátulas y separadores
			res = split2ImageNoSpacers(image);
		} else {
			res = split2ImageSpacers(image, fieldRequest, spacerSearchingParams);
		}

		tf = System.currentTimeMillis();
		LOG.info("Procesamiento de las imágenes capturadas para la petición {} realizado en {} ms. Procesamiento de separadores deshabilitado: {}.", fieldRequest.getRequestId(), (tf - t0), fieldRequest.getSpacerSearchDisabled());

		return res;
	}

	/**
	 * Procesa las imagen capturada, en formato TIFF, sin detección de
	 * separadores.
	 * 
	 * @param image
	 *            Imagen en formato TIFF.
	 * @return Lista formada por un único documento.
	 * @throws ScanDocsException
	 *             si se produce algún error procesando la imagen capturada.
	 */
	private List<ScanDocRequest> split2ImageNoSpacers(final MultipartFile image) throws ScanDocsException {
		ImageInputStream is = null;
		List<Integer> listImage = new ArrayList<Integer>();
		List<ScanDocRequest> res = new ArrayList<ScanDocRequest>();
		ScanDocRequest currentDocRequest;

		try {
			is = readerSetInput(image);
			int numPages = reader.getNumImages(true);

			for (int page = 0; page < numPages; page++) {
				listImage.add(page);
			}
			currentDocRequest = new ScanDocRequest();
			currentDocRequest.setTypeImage(ScannedImageType.DOC_CONTENT);
			currentDocRequest.setListImage(listImage);
			currentDocRequest.setBarCodeResult(null);

			res.add(currentDocRequest);
		} catch (ScanDocsException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Error procesando la imagen capturada, obtención de documentos. Error: {}", e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_999, e.getMessage());
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// No hacer nada
				}
			}
			if (reader != null) {
				reader.dispose();
			}
		}

		return res;
	}

	/**
	 * Procesa la imagen capturada en formato TIFF con detección de separadores.
	 * 
	 * @param images
	 *            Imágenes capturadas.
	 * @param fieldRequest
	 *            Parámetros informados en la petición de procesamiento.
	 * @param spacerSearchingParams
	 *            parámetros de configuración de la instancia de plugin de
	 *            captura de imágenes para el procesamiento de separadores.
	 * @param messages
	 *            bundle multidioma.
	 * @param locale
	 *            configuración regional.
	 * @return Lista con los documentos, separadores y páginas en blanco
	 *         identificados.
	 * @throws ScanDocsException
	 *             si se produce algún error procesando la imagen capturada.
	 */
	private List<ScanDocRequest> split2ImageSpacers(MultipartFile image, final PerformScanDocVO fieldRequest, final Properties spacerSearchingParams) throws ScanDocsException {
		BarCodeResult currentBarCodeResult = null;
		BarCodeResult loopBarCodeResult;
		Boolean isLoopScannedImageBlank;
		BufferedImage bim;
		BufferedImage spacerBim;
		DocSpacerType reqDocSpacerType;
		ImageInputStream is = null;
		int blankCounter = 0;
		List<Integer> listImage = new ArrayList<Integer>();
		List<ScanDocRequest> outListScanDocRequest = new ArrayList<ScanDocRequest>();
		ScannedImageType currentScannedImageType;
		ScannedImageType loopScannedImageType;
		ScanDocRequest currentDocRequest;
		String excMsg;
		String[ ] args;

		try {
			reqDocSpacerType = DocSpacerType.getByName(fieldRequest.getIdDocSpacerType());

			is = readerSetInput(image);
			currentScannedImageType = null;
			int numPages = reader.getNumImages(true);

			for (int page = 0; page < numPages; page++) {
				isLoopScannedImageBlank = Boolean.FALSE;
				bim = reader.read(page);
				LOG.debug("Se recorta el área de búsqueda de separadores para la página {}...", page);
				spacerBim = getSpacerSearchingArea(bim, spacerSearchingParams);
				LOG.debug("Se buscan separadores en la página {}...", page);
				loopBarCodeResult = readSpacer(spacerBim);
				LOG.debug("Búsqueda de separadores finalizada en la página {}...", page);
				loopScannedImageType = ScannedImageType.DOC_CONTENT;

				// buscamos si la página actual es un separador
				if (loopBarCodeResult != null) {
					// ceramos nuevo scanDocRequest y iniciamos la lista de
					// documentos
					// añadimos el separador, que luego trataremos
					if (loopBarCodeResult.getBarCodeText().startsWith(TypeIdentScanDoc.BATCH.getName())) {
						loopScannedImageType = ScannedImageType.SPACER_BATCH;
					} else if (loopBarCodeResult.getBarCodeText().startsWith(TypeIdentScanDoc.DOCUM.getName())) {
						if (!reqDocSpacerType.equals(loopBarCodeResult.getBarCodeType())) {
							args = new String[2];
							args[0] = messages.getMessage(loopBarCodeResult.getBarCodeType().name(), null, locale);
							args[1] = messages.getMessage(reqDocSpacerType.name(), null, locale);
							excMsg = messages.getMessage("error.docSpacerNotMatch.msg", args, locale);
							LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] {}", excMsg);
							throw new ScanDocsException(ScanDocsException.CODE_003, excMsg);
						}
						loopScannedImageType = ScannedImageType.SPACER_DOC;
					}
				} else {
					if (isBlank(bim)) {
						if (blankCounter > 0 && DocSpacerType.BLANK_PAGE.equals(reqDocSpacerType)) {
							loopScannedImageType = ScannedImageType.SPACER_BLANK;
							loopBarCodeResult = new BarCodeResult();
							loopBarCodeResult.setBarCodeText("");
							loopBarCodeResult.setBarCodeType(DocSpacerType.BLANK_PAGE);
						}
						loopScannedImageType = currentScannedImageType;
						isLoopScannedImageBlank = Boolean.TRUE;
						blankCounter++;
					} else {
						blankCounter = 0;
					}
				}

				// Agrupación
				if (currentScannedImageType == null) {
					// Se procesa la primera página
					currentScannedImageType = loopScannedImageType;
					currentBarCodeResult = loopBarCodeResult;
					if (!isLoopScannedImageBlank) {
						listImage.add(page);
					}
				} else {
					// Resto de páginas
					if (!loopScannedImageType.equals(currentScannedImageType)) {
						currentDocRequest = new ScanDocRequest();
						currentDocRequest.setTypeImage(currentScannedImageType);
						currentDocRequest.setListImage(listImage);
						currentDocRequest.setBarCodeResult(currentBarCodeResult);
						outListScanDocRequest.add(currentDocRequest);
						// Se inician las estructuras para el siguiente
						// documento, separador o páginas en blanco
						currentScannedImageType = loopScannedImageType;
						currentBarCodeResult = loopBarCodeResult;
						listImage = new ArrayList<Integer>();
						listImage.add(page);
						blankCounter = 0;
					} else {
						if (isLoopScannedImageBlank) {
							if (ScannedImageType.DOC_CONTENT.equals(loopScannedImageType)) {
								listImage.add(page);
							}
						} else {
							listImage.add(page);
						}
					}
				}
			}

			// Se añade al resultado el último elemento procesado
			currentDocRequest = new ScanDocRequest();
			currentDocRequest.setTypeImage(currentScannedImageType);
			currentDocRequest.setListImage(listImage);
			currentDocRequest.setBarCodeResult(currentBarCodeResult);
			outListScanDocRequest.add(currentDocRequest);
		} catch (ScanDocsException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Error procesando la imagen capturada, obtención de documentos. Error: {}", e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_999, e.getMessage());
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// No hacer nada
				}
			}
			if (reader != null) {
				reader.dispose();
			}
		}

		return outListScanDocRequest;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.scandoc.contr.util.ImageProcessor#transformScannedDocument(es.ricoh.webscan.scandoc.contr.service.ScannedDoc,
	 *      java.util.Map,
	 *      es.ricoh.webscan.scandoc.contr.controller.vo.PerformScanDocVO)
	 */
	@Override
	public byte[ ] transformScannedDocument(ScannedDoc doc, Map<String, MultipartFile> images, PerformScanDocVO fieldRequest) throws ScanDocsException {
		byte[ ] res = null;
		MultipartFile image;
		long t0;
		long tf;

		t0 = System.currentTimeMillis();
		LOG.debug("Se inicia la transfomación del documento {} (Num. pags.: {}), al formato de archivo de salida seleccionado: {}.", doc.getNumOrder(), doc.getNumPage(), doc.getMimeType().getType());

		switch (doc.getMimeType()) {
			case JPG_IMG_CONTENT:
			case PNG_IMG_CONTENT:
				if (doc.getListImage().size() > 1) {
					throw new ScanDocsException(ScanDocsException.CODE_012, "Conversión de formato no esperada. El formato " + doc.getMimeType().getType() + " sólo admite una página.");
				}

				if (!MimeType.JPG_IMG_CONTENT.equals(doc.getMimeType()) && !MimeType.PNG_IMG_CONTENT.equals(doc.getMimeType())) {
					throw new ScanDocsException(ScanDocsException.CODE_012, "Conversión de formato no esperada. El formato " + doc.getMimeType().getType() + " no es soportado por el sistema.");
				}

				image = images.get(images.keySet().iterator().next());
				res = transformToImage(doc, image);
				break;
			case PDF_CONTENT:
				image = images.get(images.keySet().iterator().next());
				res = transform2Pdf(doc, image, fieldRequest);
				break;
			case TIFF_IMG_CONTENT:
				image = images.get(images.keySet().iterator().next());
				res = transformToTiff(doc, image, fieldRequest);
				break;
			default:
				throw new ScanDocsException(ScanDocsException.CODE_012, "Conversión de formato no esperada. El formato " + doc.getMimeType().getType() + " no es soportado por el sistema.");
		}

		tf = System.currentTimeMillis();
		LOG.info("Transfomación del documento {} de la petición {} realizada en {} ms. Num. pags.: {}; Formato de archivo de salida: {}.", doc.getNumOrder(), fieldRequest.getRequestId(), (tf - t0), doc.getNumPage(), doc.getMimeType().getType());

		return res;
	}

	/**
	 * Transforma una imagen capturada a un formato PNG o JPEG. Solo válido para
	 * documentos de una página.
	 * 
	 * @param doc
	 *            Documento digitalizado.
	 * @param images
	 *            imagen en formato TIFF capturada.
	 * @return Resultado de la primera página.
	 * @throws ScanDocsException
	 *             Error al tratar la imagen de entrada o número incorrecto de
	 *             páginas.
	 */
	private byte[ ] transformToImage(final ScannedDoc doc, final MultipartFile image) throws ScanDocsException {
		BufferedImage bim;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[ ] res = null;
		String excMsg;
		String targetFormatName = doc.getMimeType().getExtension().substring(1);

		try {
			bim = ImageIO.read(image.getInputStream());
			ImageIO.write(bim, targetFormatName, baos);
			res = baos.toByteArray();
		} catch (IOException e) {
			String[ ] args = new String[2];
			args[0] = doc.getMimeType().getType();
			args[1] = e.getMessage();
			excMsg = messages.getMessage("error.scanProcess.transform", args, locale);
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Código de error: {}.Excepción {} - {}.", ScanDocsException.CODE_023, e.getCause(), e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_023, excMsg);
		} finally {
			try {
				baos.close();
			} catch (Exception e) {
				// No hacer nada
			}
		}

		return res;
	}

	/**
	 * Construye un archivo PDF a partir de la imagen capturada para un
	 * documento.
	 * 
	 * @param doc
	 *            parámetros de la petición web.
	 * @param image
	 *            imagen en formato TIFF capturada.
	 * @param fieldRequest
	 *            parámetros de la petición web.
	 * @return contenido del documento en formato PDF.
	 * @throws ScanDocsException
	 * @throws IOException
	 *             si se produce un error en la transofrmación del contenido del
	 *             documento a PDF.
	 */
	private byte[ ] transform2Pdf(ScannedDoc doc, MultipartFile image, PerformScanDocVO fieldRequest) throws ScanDocsException {
		BufferedImage bim;
		byte[ ] res = null;
		ByteArrayOutputStream baos;
		float height;
		float width;
		ImageInputStream is = null;
		int docNumPages;
		PDDocument document;
		PDImageXObject imgPage;
		PDPage pdfPage;
		PDPageContentStream contentStream;
		String excMsg;

		try {
			document = initPdfDocument();
			is = readerSetInput(image);

			docNumPages = doc.getListImage().size();
			for (int page = 0; page < docNumPages; page++) {
				LOG.debug("[WEBSCAN-SCAN-DOCS-IMG-PROC] Incluyendo la imagen {} en el documento {}", doc.getListImage().get(page), doc.getNumOrder());
				bim = reader.read(doc.getListImage().get(page));

				LOG.debug("Se lee la página {}...", doc.getListImage().get(page));
				imgPage = CCITTFactory.createFromImage(document, bim);
				width = (Integer.valueOf(bim.getWidth()).floatValue() / fieldRequest.getIdOutputResolution().floatValue()) * POINT_PER_INCH;
				height = (Integer.valueOf(bim.getHeight()).floatValue() / fieldRequest.getIdOutputResolution().floatValue()) * POINT_PER_INCH;
				pdfPage = new PDPage(new PDRectangle(width, height));
				document.addPage(pdfPage);
				contentStream = new PDPageContentStream(document, pdfPage);
				contentStream.drawImage(imgPage, 0, 0, width, height);
				contentStream.close();
			}

			baos = new ByteArrayOutputStream();
			document.save(baos);
			document.close();
			res = baos.toByteArray();
			baos.close();
		} catch (IOException e) {
			String[ ] args = new String[2];
			args[0] = "PDF";
			args[1] = e.getMessage();
			excMsg = messages.getMessage("error.scanProcess.transform", args, locale);
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Código de error: {}.Excepción {} - {}.", ScanDocsException.CODE_023, e.getCause(), e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_023, excMsg);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// No hacer nada
				}
			}
			if (reader != null) {
				reader.dispose();
			}
		}
		return res;
	}

	/**
	 * Construye un archivo TIFF a partir de las imagen capturada para un
	 * documento.
	 * 
	 * @param doc
	 *            documento digitalizado.
	 * @param image
	 *            imagen en formato TIFF capturada.
	 * @param fieldRequest
	 *            parámetros de la petición web.
	 * @return contenido del documento en formato TIFF.
	 * @throws ScanDocsException
	 * @throws IOException
	 *             si se produce un error en la transformación del contenido del
	 *             documento a formato de archivo TIFF.
	 */
	private byte[ ] transformToTiff(ScannedDoc doc, MultipartFile image, PerformScanDocVO fieldRequest) throws ScanDocsException {
		byte[ ] res = null;

		try {
			res = IOUtils.toByteArray(image.getInputStream());
		} catch (IOException e) {
			String[ ] args = new String[2];
			args[0] = "TIFF";
			args[1] = e.getMessage();
			String excMsg = messages.getMessage("error.scanProcess.transform", args, locale);
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Código de error: {}.Excepción {} - {}.", ScanDocsException.CODE_023, e.getCause(), e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_023, excMsg);
		}

		return res;
	}

	/**
	 * Establece en la clase responsable del parseo de imágenes, el contenido de
	 * la imagen/es a procesar.
	 * 
	 * @param image
	 *            imagen a procesar.
	 * @return stream de lectura de la imagen.
	 * @throws ScanDocsException
	 *             sis se produce algún error en la lectura de la imagen.
	 */
	private ImageInputStream readerSetInput(MultipartFile image) throws ScanDocsException {
		String excMsg;
		ImageInputStream res = null;
		try {
			res = ImageIO.createImageInputStream(image.getInputStream());

			if (res == null || res.length() == 0) {
				// no existe documento o está vacio
				excMsg = messages.getMessage("error.emptyImage.msg", null, locale);
				LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] {}", excMsg);
				throw new ScanDocsException(ScanDocsException.CODE_001, excMsg);
			}

			// Se obtiene la clase encargada de procesar y decodificar las
			// imagenes
			reader.reset();
			reader.setInput(res);
		} catch (IOException e) {
			LOG.error("[WEBSCAN-SCAN-DOCS-IMG-PROC] Error procesando la imagen capturada, carga de reader. Error: {}", e.getMessage());
			throw new ScanDocsException(ScanDocsException.CODE_999, e.getMessage());
		} catch (ScanDocsException e) {
			throw e;
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.scandoc.contr.util.ImageProcessor#initReader()
	 */
	public void initReader() throws ScanDocsException {
		reader = ImageProcessor.getImageReader(MimeType.TIFF_IMG_CONTENT.getType(), messages, locale);
	}

}
