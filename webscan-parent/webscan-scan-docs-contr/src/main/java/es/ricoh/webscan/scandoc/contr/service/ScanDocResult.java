/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.service.ScanDocResult.java.</p>
 * <b>Descripción:</b><p> .</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.scandoc.contr.service;

import java.util.Date;

/**
 * Objeto que devuelve el resultado de un documento digitalizado
 * <p>
 * Class ScanDocResult.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ScanDocResult {

	/**
	 * Identificador del documento digitalizado
	 */
	Long docId;

	/**
	 * Fecha que identifica el momento final en el que se digitaliza el
	 * documento
	 */
	Date endDate;

	/**
	 * Constructor sin argumentos.
	 */
	public ScanDocResult() {
		super();
	}

	/**
	 * Gets the value of the attribute {@link #docId}.
	 * 
	 * @return the value of the attribute {@link #docId}.
	 */
	public Long getDocId() {
		return docId;
	}

	/**
	 * Sets the value of the attribute {@link #docId}.
	 * 
	 * @param docId
	 *            The value for the attribute {@link #docId}.
	 */
	public void setDocId(Long docId) {
		this.docId = docId;
	}

	/**
	 * Gets the value of the attribute {@link #endDate}.
	 * 
	 * @return the value of the attribute {@link #endDate}.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the value of the attribute {@link #endDate}.
	 * 
	 * @param endDate
	 *            The value for the attribute {@link #endDate}.
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
