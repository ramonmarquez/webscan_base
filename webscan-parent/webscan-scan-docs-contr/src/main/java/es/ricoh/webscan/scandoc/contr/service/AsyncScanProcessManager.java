/** 
* <b>Archivo:</b><p>es.ricoh.webscan.scandoc.contr.service.AsyncScanProcessManager.java.</p>
* <b>Descripción:</b><p> Clase responsable de llevar a cabo las fases de ejcución automática del proceso de digitalización de documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.scandoc.contr.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import es.ricoh.webscan.ResumeStage;
import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.service.AsyncScanProcessDAOManager;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.domain.OperationStatus;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.plugins.PluginException;
import es.ricoh.webscan.utilities.Utils;

/**
 * Clase responsable de llevar a cabo las fases de ejcución automática del
 * proceso de digitalización de documentos: firma y establecimiento automático
 * de metadatos.
 * <p>
 * Clase AsyncScanProcessManager.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class AsyncScanProcessManager {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AsyncScanProcessManager.class);

	/**
	 * Servicio de apoyo para las fases de ejecución asíncrona del proceso de
	 * digitalización de docmentos que agrupa la ejecución de setencias sobre el
	 * modelo de datos.
	 */
	private AsyncScanProcessDAOManager asyncScanProcessDAOManager;

	/**
	 * Utilidades y acceso a parámetros globales del Sistema.
	 */
	private Utils webscanUtilParams;

	/**
	 * Lleva a cabo las fases de ejcución automática del proceso de
	 * digitalización de documentos: firma y establecimiento automático de
	 * metadatos.
	 * 
	 * @param scanDocId
	 *            identificador interno del documento digitalizado.
	 * @param username
	 *            Identificador del nombre de usuario.
	 */
	@Async("webscanTaskExecutor")
	public void performDocScanProcess(Long scanDocId, String username) {
		Boolean scanProfileOrgUnitPluginFound;
		Boolean executePlugin;
		ResumeStage execStage = null;
		ScannedDocumentDTO doc = null;
		ScanProfileSpecScanProcStageDTO scanProfileSpecScanProcStage = null;
		ScanProfileOrgUnitPluginDTO scanProfileOrgUnitPlugin;

		LOG.debug("[AsyncDocScanProcess] Lanzamos el proceso asincrono:{}", scanDocId);

		try {

			// 1. Recuperamos el documento y la traza de ejecución ultima,
			// entendemos que debe ser SCAN
			doc = asyncScanProcessDAOManager.getDocument(scanDocId);
			LOG.debug("[AsyncDocScanProcess] Recuperamos el documento:{}", scanDocId);

			// ultima traza, debe ser SCAN
			ScannedDocLogDTO ultimaTraza = asyncScanProcessDAOManager.getLastScannedDocLogsByDocId(scanDocId);

			LOG.debug("[AsyncDocScanProcess] Recuperamos traza documento");

			/* validamos si la última fase ejecutada es reanudable y si no está
			 con error */
			/* validamos si la última fase ejecutada es reanudable y si no está
			 con error */
			String scanProcessStageName = ultimaTraza.getScanProcessStageName();

			if (WebscanBaseConstants.SCAN_STAGE_NAME.equals(scanProcessStageName) && OperationStatus.PERFORMED.equals(ultimaTraza.getStatus())) {

				// proxima fase
				execStage = webscanUtilParams.nextEnum(ResumeStage.class, scanProcessStageName.toUpperCase());
				LOG.debug("[AsyncDocScanProcess] La fase a ejecutar es:{}", execStage);

				// recuperamos la relacion de perfil de digitalización y unidad
				// organizativa
				ScanProfileOrgUnitDTO scanProfileOrgUnitDTO = asyncScanProcessDAOManager.getScanProfileOrgUnitByDoc(scanDocId);

				// recuperamos la especificación del perfil de digitalizacion
				// anterior
				ScanProfileDTO scanProfile = asyncScanProcessDAOManager.getScanProfile(scanProfileOrgUnitDTO.getScanProfile());
				// recuperamos el map con las etapas y los plugins asociados
				Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> mapStagePlugin = asyncScanProcessDAOManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfile.getSpecification());
				LOG.debug("[AsyncDocScanProcess] Recuperamos la informacion de base de datos que necesitamos");

				// 3. Verificamos las fases reanudables pdtes + orden

				while (execStage != null) {
					scanProfileOrgUnitPluginFound = Boolean.FALSE;
					executePlugin = Boolean.FALSE;
					scanProfileOrgUnitPlugin = null;
					// ejecutamos la fase actual
					/*
					*	4. Por cada fase pendiente
					*		4.1 obtenemos plugin de fase
					*		4.2 Ejecutamos el plugin (actulizamos trazas de auditorias)
					*		4.3 Registramos/Actualizamos la traza de ejecución 
					*/

					/*verificamos que tenga activo el plugin en la fase actual*/

					// recuperamos la fase a ejecutar
					ScanProcessStageDTO stageCapture = asyncScanProcessDAOManager.getScanProcStageByName(execStage.getName());

					/*obtenemos la especificación del perfil por etapa para comprobar si es obligatorio*/
					scanProfileSpecScanProcStage = asyncScanProcessDAOManager.getScanProfileSpecScanProcStageByScanProfileSpecIdAndScanProcStageId(mapStagePlugin, scanProfile.getSpecification(), execStage.toString());
					if (scanProfileSpecScanProcStage != null) {
						// verificamos que la unidad organizativa y el perfil
						// que
						// nos
						// informa este configurado y activo
						try {
							scanProfileOrgUnitPlugin = asyncScanProcessDAOManager.getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(scanProfileOrgUnitDTO.getScanProfile(), scanProfileOrgUnitDTO.getOrgUnit(), stageCapture.getId());
							scanProfileOrgUnitPluginFound = Boolean.TRUE;
						} catch (DAOException e) {
							// verificamos que el plugin es obligatorio
							if (!e.getCode().equals(DAOException.CODE_901)) {
								throw e;
							}
						}

						if (!scanProfileSpecScanProcStage.getMandatory() && !scanProfileOrgUnitPluginFound) {
							// fase no es requerida, se continua con la
							// siguiente
							// fase
							LOG.debug("[AsynScanProcessResumeManager] Etapa no obligatoria, no ha sido configurada: {}", execStage.toString());
						} else if (!scanProfileSpecScanProcStage.getMandatory() && scanProfileOrgUnitPluginFound) {
							executePlugin = Boolean.TRUE;
						} else if (scanProfileSpecScanProcStage.getMandatory() && !scanProfileOrgUnitPluginFound) {
							LOG.debug("[AsynScanProcessResumeManager] Etapa obligatoria, no ha sido configurada: {}", execStage.toString());
							// no tenemos plugin disponible para la fase actual
							if (scanProfileOrgUnitPlugin == null) {
								LOG.error("No existe plugin configurado para la fase actual ({}).", execStage);
								throw new PluginException(PluginException.CODE_608, "No existe plugin configurado para la fase " + execStage.getName() + " para el perfil " + scanProfileOrgUnitDTO.getId());
							}
						} else if (scanProfileSpecScanProcStage.getMandatory() && scanProfileOrgUnitPluginFound) {
							if (scanProfileOrgUnitPlugin.getActive().equals(Boolean.FALSE)) {
								LOG.debug("El perfil y la unidad organizativa no tiene activo la fase actual");
								throw new PluginException(PluginException.CODE_609, "El perfil y la unidad organizativa con id: " + scanProfileOrgUnitDTO.getId() + " no tiene activo el plugin para la fase " + execStage.getName());
							}
							executePlugin = Boolean.TRUE;
						}

						/*fin verificacion del plugin activo en fase actual*/

						if (executePlugin) {
							// obtenemos el plugin spec activo
							PluginSpecificationDTO plugSpec = asyncScanProcessDAOManager.getPluginSpecByScanProfileOrgUnitAndStage(scanProfileOrgUnitDTO.getId(), execStage.getName());
							LOG.debug("[resumeDocScanProcess] Obtenemos el  plugin: {}", plugSpec.getName());

							// obtenemos el plugin
							String namePlugin = asyncScanProcessDAOManager.getPluginCall(execStage.getName(), plugSpec.getName());

							if (scanProfileSpecScanProcStage.getMandatory() && (namePlugin == null || namePlugin.trim().isEmpty())) {
								throw new PluginException(PluginException.CODE_607, "No fue posible obtener del modelo de datos el plugin que ejecuta la fase " + execStage.getName() + " para el perfil " + scanProfileOrgUnitDTO.getId());
							}

							if (namePlugin != null && !namePlugin.trim().isEmpty()) {
								asyncScanProcessDAOManager.performStagePlugin(doc, scanProfileSpecScanProcStage, plugSpec, namePlugin, execStage.getName(), username);
							}
						}
					}
					// pasamos a la siguiente fase
					execStage = webscanUtilParams.nextEnum(ResumeStage.class, execStage.toString());
					scanProfileSpecScanProcStage = null;
					LOG.debug("[resumeDocScanProcess] Recuperamos proxima etapa del documento:{}", execStage);
				}
			} else {
				if (!WebscanBaseConstants.SCAN_STAGE_NAME.equals(scanProcessStageName) && OperationStatus.PERFORMED.equals(ultimaTraza.getStatus())) {
					LOG.warn("[AsyncDocScanProcess] No es posible lanzar contiunar el proceso de digitalización de forma asíncrona, el documento {} no se encuentra en la fase {}, sino en la fase {}.", scanDocId, WebscanBaseConstants.SCAN_STAGE_NAME, scanProcessStageName);
				} else if (WebscanBaseConstants.SCAN_STAGE_NAME.equals(scanProcessStageName) && !OperationStatus.PERFORMED.equals(ultimaTraza.getStatus())) {
					LOG.warn("[AsyncDocScanProcess] No es posible lanzar contiunar el proceso de digitalización de forma asíncrona, el documento {} no ha finalizado la fase {} correctamente.", scanDocId, WebscanBaseConstants.SCAN_STAGE_NAME);
				}
			}
		} catch (PluginException e) {
			LOG.error(e.getMessage());
			try {
				// Se crea traza auditoria y actualiza la traza de ejecución de
				// la fase
				if (scanProfileSpecScanProcStage != null) {
					asyncScanProcessDAOManager.performUpdateDocErrorLogs(doc, scanProfileSpecScanProcStage, e, execStage.getName(), username);
				}
				// auditoria se registra error
				if (LOG.isErrorEnabled()) {
					LOG.error("[AsyncDocScanProcess] Error general al ejecutar plugin del documento: " + doc.getId() + " Code:" + e.getCode() + "Mensaje:" + e.getMessage());
				}
			} catch (DAOException ex) {
				String excMsg = "[AsyncDocScanProcess] Error ejecutanto plugin: " + ex.getMessage();
				LOG.error(excMsg, ex);
			}

		} catch (DAOException e) {
			// al ser un proceso asincrono sólo queda información en el log
			LOG.error("[AsyncDocScanProcess] Finalizamos la ejecución del proceso ejecución asincrono por error base de datos. Error:", e);
		} catch (Exception e) {
			LOG.error("[AsyncDocScanProcess] Error no esperado. Error:", e);
		}

		LOG.debug("[AsyncDocScanProcess] Fin Lanzamos el proceso asincrono:{}", scanDocId);
	}

	/**
	 * Establece la clase de utilidad para obtener los parametros del Sistema.
	 * 
	 * @param awebscanUtilParams
	 *            utilidades para parametros del Sistema
	 */
	public void setWebscanUtilParams(Utils awebscanUtilParams) {
		this.webscanUtilParams = awebscanUtilParams;
	}

	/**
	 * Establece el servicio de apoyo para las fases de ejecución asíncrona del
	 * proceso de digitalización de docmentos que agrupa la ejecución de
	 * setencias sobre el modelo de datos.
	 * 
	 * @param anAsyncScanProcessDAOManager
	 *            servicio de apoyo para las fases de ejecución asíncrona del
	 *            proceso de digitalización de docmentos que agrupa la ejecución
	 *            de setencias sobre el modelo de datos.
	 */
	public void setAsyncScanProcessDAOManager(AsyncScanProcessDAOManager anAsyncScanProcessDAOManager) {
		this.asyncScanProcessDAOManager = anAsyncScanProcessDAOManager;
	}

}
