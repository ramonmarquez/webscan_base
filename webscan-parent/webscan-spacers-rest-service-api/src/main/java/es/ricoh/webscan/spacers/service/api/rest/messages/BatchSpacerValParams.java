
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "docSeparatorType",
    "numberOfDocsInBatch",
    "numberOfPagesInBatch"
})
public class BatchSpacerValParams implements Serializable
{

    @JsonProperty("docSeparatorType")
    private String docSeparatorType;
    @JsonProperty("numberOfDocsInBatch")
    private Integer numberOfDocsInBatch;
    @JsonProperty("numberOfPagesInBatch")
    private Integer numberOfPagesInBatch;
    private final static long serialVersionUID = 144488079017004554L;

    @JsonProperty("docSeparatorType")
    public String getDocSeparatorType() {
        return docSeparatorType;
    }

    @JsonProperty("docSeparatorType")
    public void setDocSeparatorType(String docSeparatorType) {
        this.docSeparatorType = docSeparatorType;
    }

    @JsonProperty("numberOfDocsInBatch")
    public Integer getNumberOfDocsInBatch() {
        return numberOfDocsInBatch;
    }

    @JsonProperty("numberOfDocsInBatch")
    public void setNumberOfDocsInBatch(Integer numberOfDocsInBatch) {
        this.numberOfDocsInBatch = numberOfDocsInBatch;
    }

    @JsonProperty("numberOfPagesInBatch")
    public Integer getNumberOfPagesInBatch() {
        return numberOfPagesInBatch;
    }

    @JsonProperty("numberOfPagesInBatch")
    public void setNumberOfPagesInBatch(Integer numberOfPagesInBatch) {
        this.numberOfPagesInBatch = numberOfPagesInBatch;
    }

}
