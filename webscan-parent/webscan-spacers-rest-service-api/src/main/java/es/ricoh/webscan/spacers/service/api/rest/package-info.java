/**
 * Paquete donde se agrupan las definiciones de servicios REST publicados por el
 * componente de generación de carátulas y separadores reutilizables.
 */
package es.ricoh.webscan.spacers.service.api.rest;