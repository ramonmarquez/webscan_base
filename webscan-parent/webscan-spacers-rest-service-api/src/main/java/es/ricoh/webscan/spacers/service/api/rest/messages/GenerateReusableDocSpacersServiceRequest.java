
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "generateReusableDocSpacersRequest"
})
public class GenerateReusableDocSpacersServiceRequest implements Serializable
{

    @JsonProperty("generateReusableDocSpacersRequest")
    private GenerateReusableDocSpacersRequest generateReusableDocSpacersRequest;
    private final static long serialVersionUID = -2946218718699808322L;

    @JsonProperty("generateReusableDocSpacersRequest")
    public GenerateReusableDocSpacersRequest getGenerateReusableDocSpacersRequest() {
        return generateReusableDocSpacersRequest;
    }

    @JsonProperty("generateReusableDocSpacersRequest")
    public void setGenerateReusableDocSpacersRequest(GenerateReusableDocSpacersRequest generateReusableDocSpacersRequest) {
        this.generateReusableDocSpacersRequest = generateReusableDocSpacersRequest;
    }

}
