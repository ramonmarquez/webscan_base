
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "orderNumber",
    "name",
    "metadataCollection"
})
public class SpacerDoc implements Serializable
{

    @JsonProperty("orderNumber")
    private Integer orderNumber;
    @JsonProperty("name")
    private String name;
    @JsonProperty("metadataCollection")
    private List<MetadataCollection> metadataCollection = null;
    private final static long serialVersionUID = -6439468077448366960L;

    @JsonProperty("orderNumber")
    public Integer getOrderNumber() {
        return orderNumber;
    }

    @JsonProperty("orderNumber")
    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("metadataCollection")
    public List<MetadataCollection> getMetadataCollection() {
        return metadataCollection;
    }

    @JsonProperty("metadataCollection")
    public void setMetadataCollection(List<MetadataCollection> metadataCollection) {
        this.metadataCollection = metadataCollection;
    }

}
