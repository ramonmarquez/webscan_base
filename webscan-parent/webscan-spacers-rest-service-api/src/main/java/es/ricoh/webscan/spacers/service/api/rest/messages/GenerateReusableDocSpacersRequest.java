
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "userId",
    "spacerGenerationParams",
    "numberOfReusableDocSpacers"
})
public class GenerateReusableDocSpacersRequest implements Serializable
{

    @JsonProperty("userId")
    private String userId;
    @JsonProperty("spacerGenerationParams")
    private SpacerGenerationParams spacerGenerationParams;
    @JsonProperty("numberOfReusableDocSpacers")
    private Integer numberOfReusableDocSpacers;
    private final static long serialVersionUID = -7572281334964709598L;

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("spacerGenerationParams")
    public SpacerGenerationParams getSpacerGenerationParams() {
        return spacerGenerationParams;
    }

    @JsonProperty("spacerGenerationParams")
    public void setSpacerGenerationParams(SpacerGenerationParams spacerGenerationParams) {
        this.spacerGenerationParams = spacerGenerationParams;
    }

    @JsonProperty("numberOfReusableDocSpacers")
    public Integer getNumberOfReusableDocSpacers() {
        return numberOfReusableDocSpacers;
    }

    @JsonProperty("numberOfReusableDocSpacers")
    public void setNumberOfReusableDocSpacers(Integer numberOfReusableDocSpacers) {
        this.numberOfReusableDocSpacers = numberOfReusableDocSpacers;
    }

}
