
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "generateDocsBatchSpacerRequest"
})
public class GenerateDocsBatchSpacerServiceRequest implements Serializable
{

    @JsonProperty("generateDocsBatchSpacerRequest")
    private GenerateDocsBatchSpacerRequest generateDocsBatchSpacerRequest;
    private final static long serialVersionUID = 5820824625766958696L;

    @JsonProperty("generateDocsBatchSpacerRequest")
    public GenerateDocsBatchSpacerRequest getGenerateDocsBatchSpacerRequest() {
        return generateDocsBatchSpacerRequest;
    }

    @JsonProperty("generateDocsBatchSpacerRequest")
    public void setGenerateDocsBatchSpacerRequest(GenerateDocsBatchSpacerRequest generateDocsBatchSpacerRequest) {
        this.generateDocsBatchSpacerRequest = generateDocsBatchSpacerRequest;
    }

}
