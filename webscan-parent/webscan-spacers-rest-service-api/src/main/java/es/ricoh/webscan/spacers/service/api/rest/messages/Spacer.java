
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "binarySpacer",
    "mimeType"
})
public class Spacer implements Serializable
{

    @JsonProperty("binarySpacer")
    private String binarySpacer;
    @JsonProperty("mimeType")
    private String mimeType;
    private final static long serialVersionUID = 2694853396742373647L;

    @JsonProperty("binarySpacer")
    public String getBinarySpacer() {
        return binarySpacer;
    }

    @JsonProperty("binarySpacer")
    public void setBinarySpacer(String binarySpacer) {
        this.binarySpacer = binarySpacer;
    }

    @JsonProperty("mimeType")
    public String getMimeType() {
        return mimeType;
    }

    @JsonProperty("mimeType")
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

}
