
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "scanProfile",
    "docSpecName"
})
public class SpacerAdditionalInfoParams implements Serializable
{

    @JsonProperty("scanProfile")
    private ScanProfile scanProfile;
    @JsonProperty("docSpecName")
    private String docSpecName;
    private final static long serialVersionUID = 6251547520654855013L;

    @JsonProperty("scanProfile")
    public ScanProfile getScanProfile() {
        return scanProfile;
    }

    @JsonProperty("scanProfile")
    public void setScanProfile(ScanProfile scanProfile) {
        this.scanProfile = scanProfile;
    }

    @JsonProperty("docSpecName")
    public String getDocSpecName() {
        return docSpecName;
    }

    @JsonProperty("docSpecName")
    public void setDocSpecName(String docSpecName) {
        this.docSpecName = docSpecName;
    }

}
