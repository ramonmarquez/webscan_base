
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "scanTemplateName",
    "userGroupExternalId"
})
public class ScanProfile implements Serializable
{

    @JsonProperty("scanTemplateName")
    private String scanTemplateName;
    @JsonProperty("userGroupExternalId")
    private String userGroupExternalId;
    private final static long serialVersionUID = 8091629902752473188L;

    @JsonProperty("scanTemplateName")
    public String getScanTemplateName() {
        return scanTemplateName;
    }

    @JsonProperty("scanTemplateName")
    public void setScanTemplateName(String scanTemplateName) {
        this.scanTemplateName = scanTemplateName;
    }

    @JsonProperty("userGroupExternalId")
    public String getUserGroupExternalId() {
        return userGroupExternalId;
    }

    @JsonProperty("userGroupExternalId")
    public void setUserGroupExternalId(String userGroupExternalId) {
        this.userGroupExternalId = userGroupExternalId;
    }

}
