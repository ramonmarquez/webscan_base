
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "spacer"
})
public class ResParam implements Serializable
{

    @JsonProperty("spacer")
    private Spacer spacer;
    private final static long serialVersionUID = -3362574298032701227L;

    @JsonProperty("spacer")
    public Spacer getSpacer() {
        return spacer;
    }

    @JsonProperty("spacer")
    public void setSpacer(Spacer spacer) {
        this.spacer = spacer;
    }

}
