
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "generateDocsBatchSpacerResponse"
})
public class GenerateDocsBatchSpacerServiceResponse implements Serializable
{

    @JsonProperty("generateDocsBatchSpacerResponse")
    private GenerateDocsBatchSpacerResponse generateDocsBatchSpacerResponse;
    private final static long serialVersionUID = -5616474180322873462L;

    @JsonProperty("generateDocsBatchSpacerResponse")
    public GenerateDocsBatchSpacerResponse getGenerateDocsBatchSpacerResponse() {
        return generateDocsBatchSpacerResponse;
    }

    @JsonProperty("generateDocsBatchSpacerResponse")
    public void setGenerateDocsBatchSpacerResponse(GenerateDocsBatchSpacerResponse generateDocsBatchSpacerResponse) {
        this.generateDocsBatchSpacerResponse = generateDocsBatchSpacerResponse;
    }

}
