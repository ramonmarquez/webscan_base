
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "userId",
    "department",
    "spacerGenerationParams",
    "spacerAdditionalInfoParams",
    "spacerDocs",
    "batchSpacerValParams",
    "numberOfReusableDocSpacers"
})
public class GenerateDocsBatchSpacerRequest implements Serializable
{

    @JsonProperty("userId")
    private String userId;
    @JsonProperty("department")
    private String department;
    @JsonProperty("spacerGenerationParams")
    private SpacerGenerationParams spacerGenerationParams;
    @JsonProperty("spacerAdditionalInfoParams")
    private SpacerAdditionalInfoParams spacerAdditionalInfoParams;
    @JsonProperty("spacerDocs")
    private List<SpacerDoc> spacerDocs = null;
    @JsonProperty("batchSpacerValParams")
    private BatchSpacerValParams batchSpacerValParams;
    @JsonProperty("numberOfReusableDocSpacers")
    private Integer numberOfReusableDocSpacers;
    private final static long serialVersionUID = -4460600868127562297L;

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("department")
    public String getDepartment() {
        return department;
    }

    @JsonProperty("department")
    public void setDepartment(String department) {
        this.department = department;
    }

    @JsonProperty("spacerGenerationParams")
    public SpacerGenerationParams getSpacerGenerationParams() {
        return spacerGenerationParams;
    }

    @JsonProperty("spacerGenerationParams")
    public void setSpacerGenerationParams(SpacerGenerationParams spacerGenerationParams) {
        this.spacerGenerationParams = spacerGenerationParams;
    }

    @JsonProperty("spacerAdditionalInfoParams")
    public SpacerAdditionalInfoParams getSpacerAdditionalInfoParams() {
        return spacerAdditionalInfoParams;
    }

    @JsonProperty("spacerAdditionalInfoParams")
    public void setSpacerAdditionalInfoParams(SpacerAdditionalInfoParams spacerAdditionalInfoParams) {
        this.spacerAdditionalInfoParams = spacerAdditionalInfoParams;
    }

    @JsonProperty("spacerDocs")
    public List<SpacerDoc> getSpacerDocs() {
        return spacerDocs;
    }

    @JsonProperty("spacerDocs")
    public void setSpacerDocs(List<SpacerDoc> spacerDocs) {
        this.spacerDocs = spacerDocs;
    }

    @JsonProperty("batchSpacerValParams")
    public BatchSpacerValParams getBatchSpacerValParams() {
        return batchSpacerValParams;
    }

    @JsonProperty("batchSpacerValParams")
    public void setBatchSpacerValParams(BatchSpacerValParams batchSpacerValParams) {
        this.batchSpacerValParams = batchSpacerValParams;
    }

    @JsonProperty("numberOfReusableDocSpacers")
    public Integer getNumberOfReusableDocSpacers() {
        return numberOfReusableDocSpacers;
    }

    @JsonProperty("numberOfReusableDocSpacers")
    public void setNumberOfReusableDocSpacers(Integer numberOfReusableDocSpacers) {
        this.numberOfReusableDocSpacers = numberOfReusableDocSpacers;
    }

}
