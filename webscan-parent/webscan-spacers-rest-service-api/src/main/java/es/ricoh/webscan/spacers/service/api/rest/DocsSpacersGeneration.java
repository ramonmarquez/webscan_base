/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.service.api.rest.DocsSpacersGeneration.java.</p>
* <b>Descripción:</b><p> Especificación REST del servicio de generación de carátulas y separadores
* reutilizables de documentos publicado por el activo Webscan-GV.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateDocsBatchSpacerServiceRequest;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateReusableDocSpacersServiceRequest;

/**
 * Especificación REST del servicio de generación de carátulas y separadores
 * reutilizables de documentos publicado por el activo Webscan-GV.
 * <p>
 * Clase DocsSpacersGeneration.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.1.0.
 */
public interface DocsSpacersGeneration {

	/**
	 * Servicio REST que habilita la generación de una carátula separadora de
	 * lote de documentos.
	 * 
	 * @param generateDocsBatchFrontPageRequest
	 *            Petición para la generación de una carátula separadora de lote
	 *            de documentos.
	 * @return Respuesta HTTP que incluye la carátula generada.
	 */
	@POST
	@Path("/batch")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset= UTF-8")
	public Response generateDocsBatchSpacer(GenerateDocsBatchSpacerServiceRequest generateDocsBatchFrontPageServiceRequest);

	/**
	 * Servicio REST que habilita la generación de uno o más separadores
	 * reutilizables de documentos.
	 * 
	 * @param generateDocsBatchFrontPageRequest
	 *            Petición para la generación de una carátula separadora de lote
	 *            de documentos.
	 * @return Respuesta HTTP que incluye la carátula generada.
	 */
	@POST
	@Path("/reusableDocs")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset= UTF-8")
	public Response generateReusableDocSpacers(GenerateReusableDocSpacersServiceRequest generateReusableDocFrontPagesServiceRequest);
}
