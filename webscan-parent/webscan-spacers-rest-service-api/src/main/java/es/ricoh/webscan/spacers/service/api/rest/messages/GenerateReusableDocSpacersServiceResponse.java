
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "generateReusableDocSpacersResponse"
})
public class GenerateReusableDocSpacersServiceResponse implements Serializable
{

    @JsonProperty("generateReusableDocSpacersResponse")
    private GenerateReusableDocSpacersResponse generateReusableDocSpacersResponse;
    private final static long serialVersionUID = -4378465512607010143L;

    @JsonProperty("generateReusableDocSpacersResponse")
    public GenerateReusableDocSpacersResponse getGenerateReusableDocSpacersResponse() {
        return generateReusableDocSpacersResponse;
    }

    @JsonProperty("generateReusableDocSpacersResponse")
    public void setGenerateReusableDocSpacersResponse(GenerateReusableDocSpacersResponse generateReusableDocSpacersResponse) {
        this.generateReusableDocSpacersResponse = generateReusableDocSpacersResponse;
    }

}
