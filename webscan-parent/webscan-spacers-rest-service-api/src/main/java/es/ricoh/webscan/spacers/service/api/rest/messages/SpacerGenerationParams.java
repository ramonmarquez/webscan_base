
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "infoCodingMode",
    "locale"
})
public class SpacerGenerationParams implements Serializable
{

    @JsonProperty("infoCodingMode")
    private String infoCodingMode;
    @JsonProperty("locale")
    private String locale;
    private final static long serialVersionUID = -2138616737733362547L;

    @JsonProperty("infoCodingMode")
    public String getInfoCodingMode() {
        return infoCodingMode;
    }

    @JsonProperty("infoCodingMode")
    public void setInfoCodingMode(String infoCodingMode) {
        this.infoCodingMode = infoCodingMode;
    }

    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @JsonProperty("locale")
    public void setLocale(String locale) {
        this.locale = locale;
    }

}
