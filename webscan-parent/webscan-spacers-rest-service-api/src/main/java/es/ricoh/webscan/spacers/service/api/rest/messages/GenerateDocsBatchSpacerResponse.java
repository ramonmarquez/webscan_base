
package es.ricoh.webscan.spacers.service.api.rest.messages;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "result",
    "resParam"
})
public class GenerateDocsBatchSpacerResponse implements Serializable
{

    @JsonProperty("result")
    private Result result;
    @JsonProperty("resParam")
    private ResParam resParam;
    private final static long serialVersionUID = -3649855628241211746L;

    @JsonProperty("result")
    public Result getResult() {
        return result;
    }

    @JsonProperty("result")
    public void setResult(Result result) {
        this.result = result;
    }

    @JsonProperty("resParam")
    public ResParam getResParam() {
        return resParam;
    }

    @JsonProperty("resParam")
    public void setResParam(ResParam resParam) {
        this.resParam = resParam;
    }

}
