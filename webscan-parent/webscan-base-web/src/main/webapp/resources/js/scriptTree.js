String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};

$( function() {

	//variables locales
	var ultimoSelect=null;	

	function preguntar (event, titulo, texto, callAceptar, callCancel,datos)
	{
		
		$("#messagePantalla").html(texto);

		$("#messagePantalla").dialog({
			title: titulo,
			draggable: false,
			classes: {"ui-dialog": "highlight"},
			modal : true,
			closeOnEscape: false,
			buttons: [
				{
					text:textAceptar(),
					click:function (event)
					{
						if (datos!=null)
						{
							callAceptar.call(event, datos);
						}
						else
						{
							callAceptar.call();
						}
						
						$( this ).dialog( "close" );
					}
				},		
				{
					text:textCancel(),
					click:function (event)
					{
						if (datos!=null)
						{
							callCancel.call(event, datos);
						}
						else
						{
							callCancel.call();
						}	
						
						
						 $( this ).dialog( "close" );
					}
				}
				  // Uncommenting the following line would hide the text,
				  // resulting in the label being used as a tooltip
				  //showText: false
				
			  ]
		});
		event.stopPropagation();
	}

	
	


	function limpiar()
	{
			$("#identificador").val("");
			$("#nombre").val("");
			try
			{
				$("#idTabla").DataTable().clear().draw();
				$('#idPestanya').tabs('select', 1);
			}
			catch(err)
			{ console.log("No existe la pestanya");}
	}
	
	function cambioUltimo ()
	{
		var vselect =$("#tree").jstree().get_selected();
		ultimoSelect =vselect;
		//obtenemos la inform del nodo seleccionado
		var info = $.jstree.reference('tree').get_node(vselect); 
		console.log("info obtenida:{id:"+info.id + ", text:"+info.text+", data:"+info.data+", idParent:"+info.parent+"}");
		$('#identificador').val(info.text);
		$('#nombre').val(info.data);
		$('#idIdentificador').val(info.id);				
		//obtenemos la informacion del padre
		var infoPadre =obtenerInfoNodo(info.parent);
		var auxPadre=[]
		auxPadre[0] =infoPadre.text;
		auxPadre[1] =infoPadre.data;
		if (infoPadre.text=='#')
		{
			auxPadre[0]="";
		}
		if (infoPadre.data=='#')
		{
			auxPadre[1]="";
		}
		$('#nodoPadre').text(auxPadre[0]+"-"+auxPadre[1]);
		if(info.parent=="#") //nodo raiz
		{	
			$("#parentSelect").val("");
		}
		else
		{
			$("#parentSelect").val(infoPadre.id);
		}
		//actualizamos la vista de los usuarios asociados a la unidad organizativa
		$("#pusuario").load(contexto()+'admin/usersList.html',	{'_csrf':$("input[name='_csrf']").val(),'idIdentificador':$('#idIdentificador').val()});
	
	}



	
	
	function callAceptarCambio()
	{
		//var vselect =$("#tree").jstree().get_selected();
		console.log("Confirmado cambio de unidad organizativa sin guardar");
		$("#cambioRealizado").val("false");
     	cambioUltimo ()
	}
	
	
	function callCancelCambio()
	{
		var vselect =$("#tree").jstree().get_selected();
		console.log("Confirmado no cambio de unidad organizativa sin guardar");
		$('#tree').jstree('select_node',ultimoSelect);
		$('#tree').jstree('deselect_node',vselect);
		return;
	}
	

	
	function obtenerInfoNodo(idnodo)
	{
		var info = $.jstree.reference('tree').get_node(idnodo); 
		if(!info.text)
			info.text="#"
		if(!info.data)
			info.data="#";
		return info;
	}

	$("#addTree").on("click",function(event){
			console.log("Pulsamos añadir nodo arbol");
			//comprobamos que tenemos un elemento seleccionado del arbol
			
			if($("#tree").jstree().get_selected().length ==0)
			{
				$('#idIdentificador').val("");
				
			}
			else
			{
				var padre =$("#tree").jstree().get_selected();
				$('#idIdentificador').val("");				
				var info = $.jstree.reference('tree').get_node(padre); 
				$('#nodoPadre').text(info.text);
				$("#parentSelect").val(info.id);
			}			
			
			//inicializamos los valores de los campos			
			limpiar();	
			
			console.log("Fin añadir info arbol");
			event.preventDefault();
			
	});
	console.log("Eventos del arbol de unidades organizativas");
	$("#removeTree").on("click",function(event){
		if ($("#tree").jstree().get_selected()!=null)
		{
			preguntar (event, "Pregunta", "Est&aacute; seguro quieres eliminar el nodo seleccionado",aceptamosDeleteNode,deshacerMovimientoNode,null);
		}
		event.preventDefault();
	});

	function aceptamosDeleteNode(event, data)
	{
			//verificamos el arbol si tenemos un elemento selecionado
			$('#idIdentificador').val($("#tree").jstree().get_selected());
			$("#action").val("delete");
			//submitFormulario.call();
			submitFormularioContinue.call();
			console.log("Pulsamos eliminar nodo arbol");
			if( event!=null && typeof event != 'undefined' ) 
			{
				event.preventDefault();
			}
		
	}
	
	$("#tree").on("click",function(event){
			console.log("Pulsamos seleccionar nodo arbol scriptTree");
			var vselect =$("#tree").jstree().get_selected();
			if (ultimoSelect!=null && ultimoSelect!=vselect &&  $("#cambioRealizado").val()=="true")
			{
				preguntar (event, "Pregunta", "Esta seguro que quiere cambiar Unidad Organizativa sin guardar?",callAceptarCambio,callCancelCambio);
			}
			else
			{
				 //solo permitimos una seleccionado
				 cambioUltimo ()
			}
			event.preventDefault();
	});
	
	console.log("Eventos del arbol de unidades organizativas");
	
	$("#identificador").on("change",function(event){
		console.log("Elemento identificador cambiado");
		$("#cambioRealizado").val("true");
		console.log("Elemento identificador cambiado");
		event.preventDefault();
		
	});

	
	$("#nombre").on("change",function(event){
		console.log("Elemento nombre cambiado");
		$("#cambioRealizado").val("true");
		console.log("Elemento nombre cambiado");
		event.preventDefault();
	});
	
	
	
	//ARBOL CONSTRUCCION
	//para guardar el primer nodo por defecto seleccionado de la vista
	var obtPrimerNodoSel=null;
	if (nodoSel!=null)
	{
		obtPrimerNodoSel=nodoSel;
	}
	var array = new Array();
	for (var i = 0, len = tamanyo; i < len; i++)
	{
		if (message[i]!=null)
		{
			//console.log(message[i]);
			var parsingMe = JSON.parse(message[i].replaceAll("'","\""));
			console.log("parsingMe"+parsingMe);
			var aux = new Object();
			if (obtPrimerNodoSel==null)
			{
				obtPrimerNodoSel=parsingMe.Id
			}
			//activamos el nodo inicial
			if (obtPrimerNodoSel == parsingMe.Id)
			{
				//seleccionamos el nodo seleccionado del arbol
				aux.state = new Object();
				//aux.state.opened=false;
				aux.state.selected=true;
				//mostramos la información asociada al nodo 
				
				/*
				$('#identificador').val(parsingMe.name);
				$('#nombre').val(parsingMe.externalId);
				*/
				$('#identificador').val(parsingMe.externalId);
				$('#nombre').val(parsingMe.name);
				$('#nodoPadre').val(parsingMe.parentId!='null'?parsingMe.parentId:'Nodo Raiz');								
				$('#parentSelect').val(parsingMe.parentId!="null"?parsingMe.Id:"");
				$('#idIdentificador').val(parsingMe.Id);
				
				console.log("$('#identificador').val:"+$('#identificador').val());
				console.log("$('#nombre').val():"+$('#nombre').val());
				console.log("$('#nodoPadre').val():"+$('#nodoPadre').val());
				console.log("$('#parentSelect').val():"+$('#parentSelect').val());
				console.log("$('#idIdentificador').val():"+$('#idIdentificador').val());
				//recibimos el ultimo seleccionado de la pantalla
				ultimoSelect = parsingMe.Id;
			}
			else
			{
				aux.state = new Object();
				//aux.state.opened=false;
				aux.state.selected=false;
			}
			//si lo queremos abierto o cerrado (true o false)
			aux.state.opened=false;
			aux.id = parsingMe.Id;
			/*
			aux.text = parsingMe.name;
			aux.data = parsingMe.externalId;
			*/
			
			aux.data = parsingMe.name;
			aux.text = parsingMe.externalId;
			aux.parent = parsingMe.parentId!='null'?parsingMe.parentId:'#';
			array.push(aux);
		}						
		//console.log(message[i]);
	}
	datosTree = array;
	console.log(datosTree);
		
	$("#tree").jstree({
		"core" : {
				"data":datosTree,
				"animation" : 0,
				"check_callback" : true,
				"opened":true,
				"multiple":false
			  },
		"checkbox.three_state" :false,			
		"types" :{
					"#" : {
					  "max_children" : 1,
					  "max_depth" : 4,
					  "valid_children" : ["root"]
					},							
					"root" : {
					  "valid_children" : ["default"]
					},
					"default" : {
					  "valid_children" : ["default","file"]
					},
					"file" : {
					  "icon" : "glyphicon glyphicon-file",
					  "valid_children" : []
					}
				},
		"plugins" : [ "dnd"/*, "search", "state", "types"*/, "wholerow" ]
	})					
	.bind("move_node.jstree",
		function (e, data)
		{
			//realizamos un confirm
			preguntar (e, "Pregunta", "Est&aacute; seguro quiere cambiar el padre",aceptamosCambioNode,deshacerMovimientoNode,data);
		}
	);
	
	
	function deshacerMovimientoNode(data)
	{
	
		console.log("Deshacemos el movimiento");
		//location.href="./organizationUnit.html"
		$("#content").load(contexto()+'admin/organizationUnit.html');
	}
	
	function aceptamosCambioNode(data)
	{
		$('#identificador').val(data.node.text);
		$('#nombre').val(data.node.data);
		$('#idIdentificador').val(data.node.id);				
		if (data.node.parent =="#")
		{
			$("#parentSelect").val("");
		}
		else
		{
			$("#parentSelect").val(data.node.parent);
		}
			
		console.log(
			"$('#identificador').val():"+$('#identificador').val()+
			"$('#nombre').val():"+$('#nombre').val()+
			"$('#idIdentificador').val():"+$('#idIdentificador').val()+				
			"$('#parentSelect').val():"+$('#parentSelect').val()
		);
		//$('#formOrg').submit();
		$("#content").load(contexto()+'admin/organizationUnit.html',	$('#formOrg').serializeArray());		
		console.log("Confirmado el movimiento");
		
	}
	
})