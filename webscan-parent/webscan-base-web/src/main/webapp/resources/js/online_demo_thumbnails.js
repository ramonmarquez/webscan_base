

var DWObject;

function Dynamsoft_OnReady() {
	alert("llego");

	DWObject = Dynamsoft.WebTwainEnv.GetWebTwain('dwtcontrolContainer'); // Get the Dynamic Web TWAIN object that is embeded in the div with id 'dwtcontrolContainer'
	if (DWObject) {
		DWObjectLargeViewer = Dynamsoft.WebTwainEnv
				.GetWebTwain('dwtcontrolContainerLargeViewer'); // Get the 2nd Dynamic Web TWAIN object that is embeded in the div with id 'dwtcontrolContainerLargeViewer'

		DWObjectLargeViewer.SetViewMode(-1, -1); // When the view mode is set to -1 by -1, the control only shows the current image. No scroll bar is provided to navigate to other images.
		DWObjectLargeViewer.MaxImagesInBuffer = 1; // Set it to hold one image only
		DWObject.SetViewMode(1, 4);

		var count = DWObject.SourceCount;

		for (var i = 0; i < count; i++) {
			document.getElementById("source").options.add(new Option(DWObject
					.GetSourceNameItems(i), i));
		}

		DWObject.RegisterEvent("OnPostTransfer", Dynamsoft_OnPostTransfer);
		DWObject.RegisterEvent("OnPostLoad", Dynamsoft_OnPostLoad);
		DWObject.RegisterEvent("OnMouseClick", Dynamsoft_OnMouseClick);

	}
}

function AcquireImage() {

	if (DWObject) {
		DWObject
				.SelectSourceByIndex(document.getElementById("source").selectedIndex);
		DWObject.OpenSource();
		DWObject.IfDisableSourceAfterAcquire = true; // Scanner source will be disabled/closed automatically after the scan.
		DWObject.AcquireImage();
	}
}

//Callback functions for async APIs
function OnSuccess() {
	console.log('successful');
}

function OnFailure(errorCode, errorString) {
	alert('errorStrig' + errorString);
}

function LoadImage() {
	if (DWObject) {
		DWObject.IfShowFileDialog = true; // Open the system's file dialog to load image
		DWObject
				.LoadImageEx("", EnumDWT_ImageType.IT_ALL, OnSuccess, OnFailure); // Load images in all supported formats (.bmp, .jpg, .tif, .png, .pdf). OnSuccess or OnFailure will be called after the operation
	}
}

function Dynamsoft_OnPostTransfer() { // The event OnPostTransfer will get fired after a transfer ends.
	updateLargeViewer();
}
function Dynamsoft_OnPostLoad(path, name, type) { // The event OnPostLoad will get fired after the images from a local directory have been loaded into the control.
	updateLargeViewer();
}
function Dynamsoft_OnMouseClick() { // The event OnMouseClick will get fired when the mouse clicks on an image.
	updateLargeViewer();
}
function updateLargeViewer() {
	DWObject.CopyToClipboard(DWObject.CurrentImageIndexInBuffer); // Copy the current image in the thumbnail to clipboard in DIB format.
	DWObjectLargeViewer.LoadDibFromClipboard(); // Load the image from Clipboard into the large viewer.
}
