var submitFormulario;
var submitFormularioContinue;

$( function() {




	function mensaje(titulo, texto)
	{

		$("#messagePantalla").html(texto);

		$("#messagePantalla").dialog({
			title: titulo,
			draggable: false,
			classes: {"ui-dialog": "highlight"},
			modal : true
		});
	}
	
	
	//function submitFormulario()
	submitFormulario = function(ev)
	{
		//comprobamos los campos obligatorios
		if($("#identificador").val()=="")
		{
			mensaje("Error al Guardar", "Es obligatorio el campo Identificador.");
			return;
		}
		//comprobamos los campos obligatorios
		if($("#nombre").val()=="")
		{
			mensaje("Error al Guardar", "Es obligatorio el campo Nombre");
			return;
		}
		//si es nodo padre
		if ($("#esNodoPadre")[0].checked)
		{
			$("#parentSelect").val("");
			$("#idIdentificador").val();
		}

		try{
			preguntar ("Pregunta", "Est&aacute; seguro quiere guardar los cambios realizados",submitFormularioContinue,submitStop,null);
		}catch(e)
		{
			console.log("Error al lanzar pregunta", e);
		}
		//}
		//this.stopPropagation();

	}

	function submitStop()
	{
		console.log("No guardamos de momento los cambios");
	}
	
	//function submitFormularioContinue()
	submitFormularioContinue = function()
	{
		console.log("Valor que nos da de activacion:"+activarUser);
		var informacionForm;
		
		if(activarUser)
		{
			//preparamos la información de los usuarios a enviar
			console.log("usuarios a enviar");
			var usuariosEnv =prepararUsuarios();
			console.log(usuariosEnv);
			$("#orgUnitAppUsers").val(usuariosEnv);
			var informacionForm =$('#formOrg').serializeArray();
			$('#bloqueTablaModal').parent().empty();
			console.log("Información que enviamos para actualizar:", informacionForm);
			showModalWait();
			$("#content").load(contexto()+'admin/organizationUnitUsers.html',	informacionForm, function(){hiddenModalWait();});
		}	
		else
		{
			var informacionForm =$('#formOrg').serializeArray();
			console.log("Información que enviamos para actualizar:", informacionForm);
			showModalWait();
			$("#content").load(contexto()+'admin/organizationUnit.html',	informacionForm, function(){hiddenModalWait();});			
		}
		
	}
	
	
	
	
	
	function prepararUsuarios()
	{
		console.log("prepararUsuarios start");
		//var usuariosArray = new Array();
		var usuariosString="";
		var usuariosAsociados= $("#idTabla").DataTable().rows().data();
		usuariosString+="{";
		if ($("#idIdentificador").val()!=null && $("#idIdentificador").val()!="")
		{
			usuariosString+="idOrg:"+$("#idIdentificador").val()+", ";
		}
		else
		{
			usuariosString+="idOrg:'', ";
		}

		usuariosString+="idUser:[";
		for(i=0;i<usuariosAsociados.length;i++)
		{
			usuariosString+=usuariosAsociados[i][5];
			usuariosString+=",";
		}
		usuariosString+="]}";
		
		return usuariosString;
	}
	
	console.log("Ini establecer los eventos de guardar y cancelar");
	$("#saveOrg").on("click",function(event){
		
		console.log("Pulsamos saveOrg");
		//tendremos que validar la información
		console.log("$('#identificador').val:"+$('#identificador').val());
		console.log("$('#nombre').val():"+$('#nombre').val());
		console.log("$('#nodoPadre').val():"+$('#nodoPadre').val());
		console.log("$('#parentSelect').val():"+$('#parentSelect').val());
		console.log("$('#idIdentificador').val():"+$('#idIdentificador').val());
	
		$("#accion").val("save");
		/*llamamos a nuestra función para guardar los cambios en el formulario*/
		submitFormulario.call(event);
		event.preventDefault();
	});
	$("#cancelOrg").on("click",function(event){
		console.log("Pulsamos cancelOrg");
		showModalWait();
		$("#content").load(contexto()+'admin/organizationUnit.html',  function(){hiddenModalWait();});
		event.preventDefault();
	});
	console.log("Fin establecer los eventos de guardar y cancelar");
	
	
})