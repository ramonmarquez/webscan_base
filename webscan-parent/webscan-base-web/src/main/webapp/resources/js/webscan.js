//constantes javascript

const PROFILEADMIN =1;

(function($) {
	  "use strict"; // Start of use strict

	  // Toggle the side navigation
	  $("#menu-toggle").on('click', function(e) {
	    e.preventDefault();
	    $("body").toggleClass("sidebar-toggled");
	    $(".sidebar").toggleClass("toggled");
	  });

	  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
	  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
	    if ($(window).width() > 768) {
	      var e0 = e.originalEvent,
	        delta = e0.wheelDelta || -e0.detail;
	      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
	      e.preventDefault();
	    }
	  });

	  // Scroll to top button appear
	  $(document).on('scroll', function() {
	    var scrollDistance = $(this).scrollTop();
	    if (scrollDistance > 100) {
	      $('.scroll-to-top').fadeIn();
	    } else {
	      $('.scroll-to-top').fadeOut();
	    }
	  });

	  // Smooth scrolling using jQuery easing
	  $(document).on('click', 'a.scroll-to-top', function(event) {
	    var $anchor = $(this);
	    $('html, body').stop().animate({
	      scrollTop: ($($anchor.attr('href')).offset().top)
	    }, 1000, 'easeInOutExpo');
	    event.preventDefault();
	  });

	})(jQuery); // End of use strict


//funcion para la recarga de mensajes en la pantalla principal.
function recargarMensajes()
{
	$('#commonMessages').load(contexto()+'message.html',{'_csrf':$("input[name='_csrf']").val()});		
}


function preguntar (titulo, texto, callAceptar, callCancel,datos){
	
	$("#messagePantalla").html(texto);

	$("#messagePantalla").dialog({
		title: titulo,
		autoOpen: false,
		draggable: false,
		classes: {"ui-dialog": "highlight"},
		modal : true,
		closeOnEscape: false,
		buttons: [
			{
				text:textAceptar(),
				click:function (event)
				{
					if (datos!=null) {
						callAceptar.call(this, datos);
					} else {
						callAceptar.call();
					}
					$( this ).dialog( 'close' );
				}
			},		
			{
				text:textCancel(),
				click:function (event)
				{
					if (datos!=null){
						callCancel.call(this, datos);
					} else {
						callCancel.call();
					}	
					$( this ).dialog( 'close' );
				}
			}
		]
	}).dialog('open');
}