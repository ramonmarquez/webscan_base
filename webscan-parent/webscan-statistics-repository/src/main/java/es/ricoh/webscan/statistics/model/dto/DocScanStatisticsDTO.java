/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.statistics.model.dto.DocScanStatisticsDTO.java.</p>
 * <b>Descripción:</b><p> Entidad DTO de las estadísticas de la digitalización de documentos.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH España IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.statistics.model.dto;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.statistics.model.domain.ScanWorkingMode;

public class DocScanStatisticsDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador interno de la estadística.
	 */
	private Long id;

	/**
	 * Identificador de la plantilla de digitalización mediante la que es
	 * configurada la digitalización.
	 */
	private Long scanTemplateId;

	/**
	 * Identificador de colectivo de usuarios al que pertenece el usuario que
	 * realiza la digitalización.
	 */
	private Long orgUnitId;

	/**
	 * Unidad orgánica a la que pertenece el usuario que inicia el proceso de
	 * digitalización.
	 */
	private String userDpto;

	/**
	 * Modo de trabajo empleado para la digitalización de los documentos (en
	 * línea, diferido, etc.).
	 */
	private ScanWorkingMode workingMode;

	/**
	 * Tipo MIME del documento digitalizados (PDF, TIFF, etc.).
	 */
	private String fileFormat;

	/**
	 * Resolución con la que se solicito la digitalización de los documentos
	 * (200, 300, etc.).
	 */
	private Long resolution;

	/**
	 * Tipo de pixel con el que se solicito la digitalización de los documentos
	 * (blanco y negro, escala de grises o color).
	 */
	private String pixelType;

	/**
	 * Indica si fue deshabilitada la detección de separadores en el
	 * procesamiento de las imágenes capturadas.
	 */
	private Boolean spacerProcDisabled;

	/**
	 * Número de documentos digitalizados.
	 */
	private Integer scanWorkSize;

	/**
	 * Número de páginas digitalizadas.
	 */
	private Integer scanWorkNumPages;

	/**
	 * Identificador de reserva de trabajo de digitalización o carátula asociado
	 * a la digitalización.
	 */
	private String remoteReqId;

	/**
	 * Momento en el que se inicia el procesamiento de las imágenes capturadas.
	 */
	private Date docScanInitDate;

	/**
	 * Momento en el que se finaliza el procesamiento de las imágenes
	 * capturadas.
	 */
	private Date docScanEndDate;

	/**
	 * Constructor sin argumentos.
	 */
	public DocScanStatisticsDTO() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 */
	public DocScanStatisticsDTO(Long anId, Long aScanTemplateId, Long anOrgUnitId, String aUserDpto, ScanWorkingMode aWorkingMode, String aFileFormat, Long aResolution, String aPixelType, Integer aScanWorkSize, Integer aScanWorkNumPages, String aRemoteReqId, Date aDocScanInitDate, Date aDocScanEndDate) {
		super();
		this.id = anId;
		this.scanTemplateId = aScanTemplateId;
		this.orgUnitId = anOrgUnitId;
		this.userDpto = aUserDpto;
		this.workingMode = aWorkingMode;
		this.fileFormat = aFileFormat;
		this.resolution = aResolution;
		this.pixelType = aPixelType;
		this.scanWorkSize = aScanWorkSize;
		this.scanWorkNumPages = aScanWorkNumPages;
		this.remoteReqId = aRemoteReqId;
		this.docScanInitDate = aDocScanInitDate;
		this.docScanEndDate = aDocScanEndDate;
	}

	/**
	 * Gets the value of the attribute {@link #id}.
	 * 
	 * @return the value of the attribute {@link #id}.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the value of the attribute {@link #id}.
	 * 
	 * @param id
	 *            The value for the attribute {@link #aId}.
	 */
	void setId(Long aId) {
		this.id = aId;
	}

	/**
	 * Gets the value of the attribute {@link #scanTemplateId}.
	 * 
	 * @return the value of the attribute {@link #scanTemplateId}.
	 */
	public Long getScanTemplateId() {
		return scanTemplateId;
	}

	/**
	 * Sets the value of the attribute {@link #scanTemplateId}.
	 * 
	 * @param aScanTemplateId
	 *            The value for the attribute {@link #aScanTemplateId}.
	 */
	public void setScanTemplateId(Long aScanTemplateId) {
		this.scanTemplateId = aScanTemplateId;
	}

	/**
	 * Gets the value of the attribute {@link #orgUnitId}.
	 * 
	 * @return the value of the attribute {@link #orgUnitId}.
	 */
	public Long getOrgUnitId() {
		return orgUnitId;
	}

	/**
	 * Sets the value of the attribute {@link #orgUnitId}.
	 * 
	 * @param orgUnitId
	 *            The value for the attribute {@link #aOrgUnitId}.
	 */
	public void setOrgUnitId(Long aOrgUnitId) {
		this.orgUnitId = aOrgUnitId;
	}

	/**
	 * Gets the value of the attribute {@link #userDpto}.
	 * 
	 * @return the value of the attribute {@link #userDpto}.
	 */
	public String getUserDpto() {
		return userDpto;
	}

	/**
	 * Sets the value of the attribute {@link #userDpto}.
	 * 
	 * @param userDpto
	 *            The value for the attribute {@link #aUserDpto}.
	 */
	public void setUserDpto(String aUserDpto) {
		this.userDpto = aUserDpto;
	}

	/**
	 * Gets the value of the attribute {@link #workingMode}.
	 * 
	 * @return the value of the attribute {@link #workingMode}.
	 */
	public ScanWorkingMode getWorkingMode() {
		return workingMode;
	}

	/**
	 * Sets the value of the attribute {@link #workingMode}.
	 * 
	 * @param workingMode
	 *            The value for the attribute {@link #aWorkingMode}.
	 */
	public void setWorkingMode(ScanWorkingMode aWorkingMode) {
		this.workingMode = aWorkingMode;
	}

	/**
	 * Gets the value of the attribute {@link #fileFormat}.
	 * 
	 * @return the value of the attribute {@link #fileFormat}.
	 */
	public String getFileFormat() {
		return fileFormat;
	}

	/**
	 * Sets the value of the attribute {@link #fileFormat}.
	 * 
	 * @param fileFormat
	 *            The value for the attribute {@link #aFileFormat}.
	 */
	public void setFileFormat(String aFileFormat) {
		this.fileFormat = aFileFormat;
	}

	/**
	 * Gets the value of the attribute {@link #resolution}.
	 * 
	 * @return the value of the attribute {@link #resolution}.
	 */
	public Long getResolution() {
		return resolution;
	}

	/**
	 * Sets the value of the attribute {@link #resolution}.
	 * 
	 * @param resolution
	 *            The value for the attribute {@link #aResolution}.
	 */
	public void setResolution(Long aResolution) {
		this.resolution = aResolution;
	}

	/**
	 * Gets the value of the attribute {@link #pixelType}.
	 * 
	 * @return the value of the attribute {@link #pixelType}.
	 */
	public String getPixelType() {
		return pixelType;
	}

	/**
	 * Sets the value of the attribute {@link #pixelType}.
	 * 
	 * @param pixelType
	 *            The value for the attribute {@link #aPixelType}.
	 */
	public void setPixelType(String aPixelType) {
		this.pixelType = aPixelType;
	}

	/**
	 * Obtiene el indicador que informa si fue deshabilitada la detección de
	 * separadores en el procesamiento de las imágenes capturadas.
	 * 
	 * @return el indicador que informa si fue deshabilitada la detección de
	 *         separadores en el procesamiento de las imágenes capturadas.
	 */
	public Boolean getSpacerProcDisabled() {
		return spacerProcDisabled;
	}

	/**
	 * Establece un nuevo valor para el indicador que informa si fue
	 * deshabilitada la detección de separadores en el procesamiento de las
	 * imágenes capturadas.
	 * 
	 * @param aSpacerProcDisabled
	 *            nuevo valor para el indicador que informa si fue deshabilitada
	 *            la detección de separadores en el procesamiento de las
	 *            imágenes capturadas.
	 */
	public void setSpacerProcDisabled(Boolean aSpacerProcDisabled) {
		this.spacerProcDisabled = aSpacerProcDisabled;
	}

	/**
	 * Gets the value of the attribute {@link #scanWorkSize}.
	 * 
	 * @return the value of the attribute {@link #scanWorkSize}.
	 */
	public Integer getScanWorkSize() {
		return scanWorkSize;
	}

	/**
	 * Sets the value of the attribute {@link #scanWorkSize}.
	 * 
	 * @param scanWorkSize
	 *            The value for the attribute {@link #aScanWorkSize}.
	 */
	public void setScanWorkSize(Integer aScanWorkSize) {
		this.scanWorkSize = aScanWorkSize;
	}

	/**
	 * Gets the value of the attribute {@link #scanWorkNumPages}.
	 * 
	 * @return the value of the attribute {@link #scanWorkNumPages}.
	 */
	public Integer getScanWorkNumPages() {
		return scanWorkNumPages;
	}

	/**
	 * Sets the value of the attribute {@link #scanWorkNumPages}.
	 * 
	 * @param scanWorkNumPages
	 *            The value for the attribute {@link #aScanWorkNumPages}.
	 */
	public void setScanWorkNumPages(Integer aScanWorkNumPages) {
		this.scanWorkNumPages = aScanWorkNumPages;
	}

	/**
	 * Gets the value of the attribute {@link #remoteReqId}.
	 * 
	 * @return the value of the attribute {@link #remoteReqId}.
	 */
	public String getRemoteReqId() {
		return remoteReqId;
	}

	/**
	 * Sets the value of the attribute {@link #remoteReqId}.
	 * 
	 * @param remoteReqId
	 *            The value for the attribute {@link #aRemoteReqId}.
	 */
	public void setRemoteReqId(String aRemoteReqId) {
		this.remoteReqId = aRemoteReqId;
	}

	/**
	 * Gets the value of the attribute {@link #docScanInitDate}.
	 * 
	 * @return the value of the attribute {@link #docScanInitDate}.
	 */
	public Date getDocScanInitDate() {
		return docScanInitDate;
	}

	/**
	 * Sets the value of the attribute {@link #docScanInitDate}.
	 * 
	 * @param docScanInitDate
	 *            The value for the attribute {@link #aDocScanInitDate}.
	 */
	public void setDocScanInitDate(Date aDocScanInitDate) {
		this.docScanInitDate = aDocScanInitDate;
	}

	/**
	 * Gets the value of the attribute {@link #docScanEndDate}.
	 * 
	 * @return the value of the attribute {@link #docScanEndDate}.
	 */
	public Date getDocScanEndDate() {
		return docScanEndDate;
	}

	/**
	 * Sets the value of the attribute {@link #docScanEndDate}.
	 * 
	 * @param docScanEndDate
	 *            The value for the attribute {@link #aDocScanEndDate}.
	 */
	public void setDocScanEndDate(Date aDocScanEndDate) {
		this.docScanEndDate = aDocScanEndDate;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docScanEndDate == null) ? 0 : docScanEndDate.hashCode());
		result = prime * result + ((docScanInitDate == null) ? 0 : docScanInitDate.hashCode());
		result = prime * result + ((fileFormat == null) ? 0 : fileFormat.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((orgUnitId == null) ? 0 : orgUnitId.hashCode());
		result = prime * result + ((pixelType == null) ? 0 : pixelType.hashCode());
		result = prime * result + ((remoteReqId == null) ? 0 : remoteReqId.hashCode());
		result = prime * result + ((resolution == null) ? 0 : resolution.hashCode());
		result = prime * result + ((scanTemplateId == null) ? 0 : scanTemplateId.hashCode());
		result = prime * result + ((scanWorkNumPages == null) ? 0 : scanWorkNumPages.hashCode());
		result = prime * result + ((scanWorkSize == null) ? 0 : scanWorkSize.hashCode());
		result = prime * result + ((userDpto == null) ? 0 : userDpto.hashCode());
		result = prime * result + ((workingMode == null) ? 0 : workingMode.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocScanStatisticsDTO other = (DocScanStatisticsDTO) obj;
		if (docScanEndDate == null) {
			if (other.docScanEndDate != null)
				return false;
		} else if (!docScanEndDate.equals(other.docScanEndDate))
			return false;
		if (docScanInitDate == null) {
			if (other.docScanInitDate != null)
				return false;
		} else if (!docScanInitDate.equals(other.docScanInitDate))
			return false;
		if (fileFormat == null) {
			if (other.fileFormat != null)
				return false;
		} else if (!fileFormat.equals(other.fileFormat))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (orgUnitId == null) {
			if (other.orgUnitId != null)
				return false;
		} else if (!orgUnitId.equals(other.orgUnitId))
			return false;
		if (pixelType == null) {
			if (other.pixelType != null)
				return false;
		} else if (!pixelType.equals(other.pixelType))
			return false;
		if (remoteReqId == null) {
			if (other.remoteReqId != null)
				return false;
		} else if (!remoteReqId.equals(other.remoteReqId))
			return false;
		if (resolution == null) {
			if (other.resolution != null)
				return false;
		} else if (!resolution.equals(other.resolution))
			return false;
		if (scanTemplateId == null) {
			if (other.scanTemplateId != null)
				return false;
		} else if (!scanTemplateId.equals(other.scanTemplateId))
			return false;
		if (scanWorkNumPages == null) {
			if (other.scanWorkNumPages != null)
				return false;
		} else if (!scanWorkNumPages.equals(other.scanWorkNumPages))
			return false;
		if (scanWorkSize == null) {
			if (other.scanWorkSize != null)
				return false;
		} else if (!scanWorkSize.equals(other.scanWorkSize))
			return false;
		if (userDpto == null) {
			if (other.userDpto != null)
				return false;
		} else if (!userDpto.equals(other.userDpto))
			return false;
		if (workingMode == null) {
			if (other.workingMode != null)
				return false;
		} else if (!workingMode.equals(other.workingMode))
			return false;
		return true;
	}

}
