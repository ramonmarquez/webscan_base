/**
 * Definición de las interfaces que conforman la capa de acceso a datos de las
 * diferentes entidades pertenecientes al modelo de datos de persistencia de
 * datos de uso del sistema.
 */
package es.ricoh.webscan.statistics.repository.dao;