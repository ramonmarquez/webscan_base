/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.statistics.model.domain.DocScanStatistics.java.</p>
 * <b>Descripción:</b><p> Entidad JSP de las estadísticas de la digitalización de documentos.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH España IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.statistics.model.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA estadística de la digitalización de documentos.
 * <p>
 * Clase DocScanStatistics.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
@Entity
@Table(name = "DOC_SCAN_STATISTICS", uniqueConstraints = { @UniqueConstraint(columnNames = { "ID" }) })
public class DocScanStatistics implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "DOC_SCAN_STATISTICS_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "DOC_SCAN_STATISTICS_ID_SEQ", sequenceName = "DOC_SCAN_STATISTICS_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo SCAN_PROFILE_ID de la entidad.
	 */
	@Column(name = "SCAN_PROFILE_ID")
	private Long scanTemplateId;

	/**
	 * Atributo ORG_UNIT_ID de la entidad.
	 */
	@Column(name = "ORG_UNIT_ID")
	private Long orgUnitId;

	/**
	 * Atributo USER_DPTO de la entidad.
	 */
	@Column(name = "USER_DPTO")
	private String userDpto;

	/**
	 * Atributo WORKING_MODE de la entidad.
	 */
	@Column(name = "WORKING_MODE")
	@Enumerated(EnumType.STRING)
	private ScanWorkingMode workingMode;

	/**
	 * Atributo FILE_FORMAT de la entidad.
	 */
	@Column(name = "FILE_FORMAT")
	private String fileFormat;

	/**
	 * Atributo RESOLUTION de la entidad.
	 */
	@Column(name = "RESOLUTION")
	private Long resolution;

	/**
	 * Atributo PIXEL_TYPE de la entidad.
	 */
	@Column(name = "PIXEL_TYPE")
	private String pixelType;

	/**
	 * Atributo SPACER_PROC_DISABLED de la entidad.
	 */
	@Column(name = "SPACER_PROC_DISABLED")
	private Boolean spacerProcDisabled;

	/**
	 * Atributo SCAN_WORK_SIZE de la entidad.
	 */
	@Column(name = "SCAN_WORK_SIZE")
	private Integer scanWorkSize;

	/**
	 * Atributo SCAN_WORK_NUM_PAGES de la entidad.
	 */
	@Column(name = "SCAN_WORK_NUM_PAGES")
	private Integer scanWorkNumPages;

	/**
	 * Atributo REMOTE_REQ_ID de la entidad.
	 */
	@Column(name = "REMOTE_REQ_ID")
	private String remoteReqId;

	/**
	 * Atributo DOC_SCAN_INIT_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DOC_SCAN_INIT_DATE")
	private Date docScanInitDate;

	/**
	 * Atributo DOC_SCAN_END_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DOC_SCAN_END_DATE")
	private Date docScanEndDate;

	/**
	 * Constructor sin argumentos.
	 */
	public DocScanStatistics() {

	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param id
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Obtiene el atributo SCAN_PROFILE_ID de la entidad.
	 * 
	 * @return el atributo SCAN_PROFILE_ID de la entidad.
	 */
	public Long getScanTemplateId() {
		return scanTemplateId;
	}

	/**
	 * Establece un nuevo valor para el atributo SCAN_PROFILE_ID de la entidad.
	 * 
	 * @param aScanTemplateId
	 *            nuevo valor para el atributo SCAN_PROFILE_ID de la entidad.
	 */
	public void setScanTemplateId(Long aScanTemplateId) {
		this.scanTemplateId = aScanTemplateId;
	}

	/**
	 * Obtiene el atributo ORG_UNIT_ID de la entidad.
	 * 
	 * @return el atributo ORG_UNIT_ID de la entidad.
	 */
	public Long getOrgUnitId() {
		return orgUnitId;
	}

	/**
	 * Establece un nuevo valor para el atributo ORG_UNIT_ID de la entidad.
	 * 
	 * @param orgUnitId
	 *            nuevo valor para el atributo ORG_UNIT_ID de la entidad.
	 */
	public void setOrgUnitId(Long orgUnitId) {
		this.orgUnitId = orgUnitId;
	}

	/**
	 * Obtiene el atributo USER_DPTO de la entidad.
	 * 
	 * @return el atributo USER_DPTO de la entidad.
	 */
	public String getUserDpto() {
		return userDpto;
	}

	/**
	 * Establece un nuevo valor para el atributo USER_DPTO de la entidad.
	 * 
	 * @param userDpto
	 *            nuevo valor para el atributo USER_DPTO de la entidad.
	 */
	public void setUserDpto(String userDpto) {
		this.userDpto = userDpto;
	}

	/**
	 * Obtiene el atributo WORKING_MODE de la entidad.
	 * 
	 * @return el atributo WORKING_MODE de la entidad.
	 */
	public ScanWorkingMode getWorkingMode() {
		return workingMode;
	}

	/**
	 * Establece un nuevo valor para el atributo WORKING_MODE de la entidad.
	 * 
	 * @param aWorkingMode
	 *            nuevo valor para el atributo WORKING_MODE de la entidad.
	 */
	public void setWorkingMode(ScanWorkingMode aWorkingMode) {
		this.workingMode = aWorkingMode;
	}

	/**
	 * Obtiene el atributo FILE_FORMAT de la entidad.
	 * 
	 * @return el atributo FILE_FORMAT de la entidad.
	 */
	public String getFileFormat() {
		return fileFormat;
	}

	/**
	 * Establece un nuevo valor para el atributo FILE_FORMAT de la entidad.
	 * 
	 * @param fileFormat
	 *            nuevo valor para el atributo FILE_FORMAT de la entidad.
	 */
	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	/**
	 * Obtiene el atributo RESOLUTION de la entidad.
	 * 
	 * @return el atributo RESOLUTION de la entidad.
	 */
	public Long getResolution() {
		return resolution;
	}

	/**
	 * Establece un nuevo valor para el atributo RESOLUTION de la entidad.
	 * 
	 * @param resolution
	 *            nuevo valor para el atributo RESOLUTION de la entidad.
	 */
	public void setResolution(Long resolution) {
		this.resolution = resolution;
	}

	/**
	 * Obtiene el atributo PIXEL_TYPE de la entidad.
	 * 
	 * @return el atributo PIXEL_TYPE de la entidad.
	 */
	public String getPixelType() {
		return pixelType;
	}

	/**
	 * Establece un nuevo valor para el atributo PIXEL_TYPE de la entidad.
	 * 
	 * @param pixelType
	 *            nuevo valor para el atributo PIXEL_TYPE de la entidad.
	 */
	public void setPixelType(String pixelType) {
		this.pixelType = pixelType;
	}

	/**
	 * Obtiene el atributo SPACER_PROC_DISABLED de la entidad.
	 * 
	 * @return el atributo SPACER_PROC_DISABLED de la entidad.
	 */
	public Boolean getSpacerProcDisabled() {
		return spacerProcDisabled;
	}

	/**
	 * Establece un nuevo valor para el atributo SPACER_PROC_DISABLED de la
	 * entidad.
	 * 
	 * @param aSpacerProcDisabled
	 *            nuevo valor para el atributo SPACER_PROC_DISABLED de la
	 *            entidad.
	 */
	public void setSpacerProcDisabled(Boolean aSpacerProcDisabled) {
		this.spacerProcDisabled = aSpacerProcDisabled;
	}

	/**
	 * Obtiene el atributo SCAN_WORK_SIZE de la entidad.
	 * 
	 * @return el atributo SCAN_WORK_SIZE de la entidad.
	 */
	public Integer getScanWorkSize() {
		return scanWorkSize;
	}

	/**
	 * Establece un nuevo valor para el atributo SCAN_WORK_SIZE de la entidad.
	 * 
	 * @param scanWorkSize
	 *            nuevo valor para el atributo SCAN_WORK_SIZE de la entidad.
	 */
	public void setScanWorkSize(Integer scanWorkSize) {
		this.scanWorkSize = scanWorkSize;
	}

	/**
	 * Obtiene el atributo SCAN_WORK_NUM_PAGES de la entidad.
	 * 
	 * @return el atributo SCAN_WORK_NUM_PAGES de la entidad.
	 */
	public Integer getScanWorkNumPages() {
		return scanWorkNumPages;
	}

	/**
	 * Establece un nuevo valor para el atributo SCAN_WORK_NUM_PAGES de la
	 * entidad.
	 * 
	 * @param scanWorkNumPages
	 *            nuevo valor para el atributo SCAN_WORK_NUM_PAGES de la
	 *            entidad.
	 */
	public void setScanWorkNumPages(Integer scanWorkNumPages) {
		this.scanWorkNumPages = scanWorkNumPages;
	}

	/**
	 * Obtiene el atributo REMOTE_REQ_ID de la entidad.
	 * 
	 * @return el atributo REMOTE_REQ_ID de la entidad.
	 */
	public String getRemoteReqId() {
		return remoteReqId;
	}

	/**
	 * Establece un nuevo valor para el atributo REMOTE_REQ_ID de la entidad.
	 * 
	 * @param remoteReqId
	 *            nuevo valor para el atributo REMOTE_REQ_ID de la entidad.
	 */
	public void setRemoteReqId(String remoteReqId) {
		this.remoteReqId = remoteReqId;
	}

	/**
	 * Obtiene el atributo DOC_SCAN_INIT_DATE de la entidad.
	 * 
	 * @return el atributo DOC_SCAN_INIT_DATE de la entidad.
	 */
	public Date getDocScanInitDate() {
		return docScanInitDate;
	}

	/**
	 * Establece un nuevo valor para el atributo DOC_SCAN_INIT_DATE de la
	 * entidad.
	 * 
	 * @param docScanInitDate
	 *            nuevo valor para el atributo DOC_SCAN_INIT_DATE de la entidad.
	 */
	public void setDocScanInitDate(Date docScanInitDate) {
		this.docScanInitDate = docScanInitDate;
	}

	/**
	 * Obtiene el atributo DOC_SCAN_END_DATE de la entidad.
	 * 
	 * @return el atributo DOC_SCAN_END_DATE de la entidad.
	 */
	public Date getDocScanEndDate() {
		return docScanEndDate;
	}

	/**
	 * Establece un nuevo valor para el atributo DOC_SCAN_END_DATE de la
	 * entidad.
	 * 
	 * @param docScanEndDate
	 *            nuevo valor para el atributo DOC_SCAN_END_DATE de la entidad.
	 */
	public void setDocScanEndDate(Date docScanEndDate) {
		this.docScanEndDate = docScanEndDate;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docScanEndDate == null) ? 0 : docScanEndDate.hashCode());
		result = prime * result + ((docScanInitDate == null) ? 0 : docScanInitDate.hashCode());
		result = prime * result + ((fileFormat == null) ? 0 : fileFormat.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((orgUnitId == null) ? 0 : orgUnitId.hashCode());
		result = prime * result + ((pixelType == null) ? 0 : pixelType.hashCode());
		result = prime * result + ((remoteReqId == null) ? 0 : remoteReqId.hashCode());
		result = prime * result + ((resolution == null) ? 0 : resolution.hashCode());
		result = prime * result + ((scanTemplateId == null) ? 0 : scanTemplateId.hashCode());
		result = prime * result + ((scanWorkNumPages == null) ? 0 : scanWorkNumPages.hashCode());
		result = prime * result + ((scanWorkSize == null) ? 0 : scanWorkSize.hashCode());
		result = prime * result + ((userDpto == null) ? 0 : userDpto.hashCode());
		result = prime * result + ((workingMode == null) ? 0 : workingMode.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocScanStatistics other = (DocScanStatistics) obj;
		if (docScanEndDate == null) {
			if (other.docScanEndDate != null)
				return false;
		} else if (!docScanEndDate.equals(other.docScanEndDate))
			return false;
		if (docScanInitDate == null) {
			if (other.docScanInitDate != null)
				return false;
		} else if (!docScanInitDate.equals(other.docScanInitDate))
			return false;
		if (fileFormat == null) {
			if (other.fileFormat != null)
				return false;
		} else if (!fileFormat.equals(other.fileFormat))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (orgUnitId == null) {
			if (other.orgUnitId != null)
				return false;
		} else if (!orgUnitId.equals(other.orgUnitId))
			return false;
		if (pixelType == null) {
			if (other.pixelType != null)
				return false;
		} else if (!pixelType.equals(other.pixelType))
			return false;
		if (remoteReqId == null) {
			if (other.remoteReqId != null)
				return false;
		} else if (!remoteReqId.equals(other.remoteReqId))
			return false;
		if (resolution == null) {
			if (other.resolution != null)
				return false;
		} else if (!resolution.equals(other.resolution))
			return false;
		if (scanTemplateId == null) {
			if (other.scanTemplateId != null)
				return false;
		} else if (!scanTemplateId.equals(other.scanTemplateId))
			return false;
		if (scanWorkNumPages == null) {
			if (other.scanWorkNumPages != null)
				return false;
		} else if (!scanWorkNumPages.equals(other.scanWorkNumPages))
			return false;
		if (scanWorkSize == null) {
			if (other.scanWorkSize != null)
				return false;
		} else if (!scanWorkSize.equals(other.scanWorkSize))
			return false;
		if (userDpto == null) {
			if (other.userDpto != null)
				return false;
		} else if (!userDpto.equals(other.userDpto))
			return false;
		if (workingMode == null) {
			if (other.workingMode != null)
				return false;
		} else if (!workingMode.equals(other.workingMode))
			return false;
		return true;
	}

}
