/** 
* <b>Archivo:</b><p>es.ricoh.webscan.statistics.model.dao.StatisticsDAO.java.</p>
* <b>Descripción:</b><p> Interfaz que define las estadísticas de la digitalización de documentos.
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.statistics.repository.dao;

import es.ricoh.webscan.statistics.model.domain.DocScanStatistics;
import es.ricoh.webscan.statistics.model.dto.DocScanStatisticsDTO;
import es.ricoh.webscan.statistics.repository.StatisticsDAOException;

/**
 * Interfaz que define las estadísticas de la digitalización de documentos
 * <p>
 * Clase DocScanStatisticsDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public interface DocScanStatisticsDAO {

	/**
	 * Recupera una estadística de un documento digitalizado a partir de su
	 * identificador.
	 * 
	 * @param docScanStatisticsId
	 *            Identificador de la estadística del documento digitalizado.
	 * @return DocScanStatisticsDTO.
	 * @throws StatisticsDAOException
	 *             Si no existe la estadística, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	DocScanStatisticsDTO getDocScanStatisticsDTO(Long docScanStatisticsId) throws StatisticsDAOException;

	/**
	 * Recupera una estadística de un documento digitalizado a partir de su
	 * identificador.
	 * 
	 * @param docScanStatisticsDTOId
	 *            Identificador de la estadística del documento digitalizado.
	 * @return DocScanStatisticsDTO.
	 * @throws StatisticsDAOException
	 *             Si no existe la estadística, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	DocScanStatistics getDocScanStatisticsEntity(Long docScanStatisticsDTOId) throws StatisticsDAOException;

	/**
	 * Crea una nueva estadística en el modelo de datos.
	 * 
	 * @param docScanStatisticsDTO
	 *            Objeto DTO de la estadística del documento digitalizado.
	 * @return identificador de la estadística creada.
	 * @throws StatisticsDAOException
	 */
	Long createDocScanStatistics(DocScanStatisticsDTO docScanStatisticsDTO) throws StatisticsDAOException;

	/**
	 * Actualiza una estadística existente en el modelo de datos.
	 * 
	 * @param docScanStatisticsDTO
	 *            Objeto DTO de la estadística del documento digitalizado.
	 * @throws StatisticsDAOException
	 */
	void updateDocScanStatistics(DocScanStatisticsDTO docScanStatisticsDTO) throws StatisticsDAOException;

	/**
	 * Recupera una estadística de un documento digitalizado a partir de su
	 * identificador de reserva de trabajo de digitalización o caratula asociado
	 * a la digitalización.
	 * 
	 * @param remoteReqId
	 *            Identificador de reserva de trabajo de digitalización o
	 *            caratula asociado a la digitalización.
	 * @return DocScanStatisticsDTO.
	 * @throws StatisticsDAOException
	 *             Si no existe la estadística, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	DocScanStatisticsDTO getDocScanStatisticsDTOByRemoteReqId(String remoteReqId) throws StatisticsDAOException;

}
