/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.statistics.model.domain.ScanWorkingMode.java.</p>
 * <b>Descripción:</b><p> Modo en el que se produce la digitalización de documentos.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH España IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.statistics.model.domain;

/**
 * Modo en el que se produce la digitalización de documentos.
 * <p>
 * Clase ScanWorkingMode.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * >Webscan GV - Plataforma de digitalización certificada y copia auténtica de
 * la Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum ScanWorkingMode {
	/**
	 * Modo de trabajo en línea.
	 */
	ON_LINE,
	/**
	 * Modo diferido mediante carátula solicitada remotamente.
	 */
	REMOTE_SPACER,
	/**
	 * Modo diferido mediante carátula solicitada localmente.
	 */
	LOCAL_SPACER,
	/**
	 * Modode trabajo autónomo.
	 */
	STAND_ALONE;
}
