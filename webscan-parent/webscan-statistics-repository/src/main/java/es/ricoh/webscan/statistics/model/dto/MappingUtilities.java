/**
 * <b>Archivo:</b><p>es.ricoh.webscan.statistics.model.dto.MappingUtilities.java.</p>
 * <b>Descripción:</b><p> Clase de utilidades para la transformación de entidades JPA, pertenecientes
 * al modelo de datos de las estadísticas de la digitalización de documentos, en objetos DTO, y viceversa.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */

package es.ricoh.webscan.statistics.model.dto;

import es.ricoh.webscan.statistics.model.domain.DocScanStatistics;

/**
 * Clase de utilidades para la transformación de entidades JPA, pertenecientes
 * al modelo de datos de las estadísticas de la digitalización de documentos, en
 * objetos DTO, y viceversa.
 * <p>
 * Clase MappingUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class MappingUtilities {

	/**
	 * Transforma una entidad de BBDD estadística en un objeto
	 * DocScanStatistics.
	 * 
	 * @param entity
	 *            Entidad de BBDD estadística.
	 * @return Objeto DocScanStatisticsDTO.
	 */
	public static DocScanStatisticsDTO fillDocScanStatisticsDto(DocScanStatistics entity) {
		DocScanStatisticsDTO res = new DocScanStatisticsDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setDocScanEndDate(entity.getDocScanEndDate());
			res.setDocScanInitDate(entity.getDocScanInitDate());
			res.setFileFormat(entity.getFileFormat());
			res.setOrgUnitId(entity.getOrgUnitId());
			res.setPixelType(entity.getPixelType());
			res.setSpacerProcDisabled(entity.getSpacerProcDisabled());
			res.setRemoteReqId(entity.getRemoteReqId());
			res.setResolution(entity.getResolution());
			res.setScanTemplateId(entity.getScanTemplateId());
			res.setScanWorkNumPages(entity.getScanWorkNumPages());
			res.setScanWorkSize(entity.getScanWorkSize());
			res.setSpacerProcDisabled(entity.getSpacerProcDisabled());
			res.setUserDpto(entity.getUserDpto());
			res.setWorkingMode(entity.getWorkingMode());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto DocScanStatisticsDTO en una entidad de
	 * BBDD estadística.
	 * 
	 * @param dto
	 *            objeto DocScanStatisticsDTO.
	 * @return Objeto DocScanStatistics.
	 */
	public static DocScanStatistics fillDocScanStatisticsEntity(DocScanStatisticsDTO dto) {
		DocScanStatistics res = new DocScanStatistics();

		if (dto != null) {
			res.setId(dto.getId());
			res.setDocScanEndDate(dto.getDocScanEndDate());
			res.setDocScanInitDate(dto.getDocScanInitDate());
			res.setFileFormat(dto.getFileFormat());
			res.setOrgUnitId(dto.getOrgUnitId());
			res.setPixelType(dto.getPixelType());
			res.setRemoteReqId(dto.getRemoteReqId());
			res.setResolution(dto.getResolution());
			res.setScanTemplateId(dto.getScanTemplateId());
			res.setScanWorkNumPages(dto.getScanWorkNumPages());
			res.setScanWorkSize(dto.getScanWorkSize());
			res.setSpacerProcDisabled(dto.getSpacerProcDisabled());
			res.setUserDpto(dto.getUserDpto());
			res.setWorkingMode(dto.getWorkingMode());
		}

		return res;
	}

}
