/** 
* <b>Archivo:</b><p>es.ricoh.webscan.statistics.repository.StatisticsModelManager.java.</p>
* <b>Descripción:</b><p> Fachada de gestión y consulta sobre las entidades pertenecientes al modelo de datos 
* de las estadísticas de la digitalización de documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de 
* la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.statistics.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.statistics.model.dto.DocScanStatisticsDTO;
import es.ricoh.webscan.statistics.repository.dao.DocScanStatisticsDAO;
import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.model.DataValidationUtilities;

/**
 * Fachada de gestión y consulta sobre las entidades pertenecientes al modelo de
 * datos de las estadísticas de la digitalización de documentos.
 * <p>
 * Clase StatisticsModelManager.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class StatisticsModelManager {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(StatisticsModelManager.class);

	/**
	 * DAO sobre las entidades pertenecientes al modelo de datos de las
	 * estadísticas de la digitalización de documentos.
	 */
	private DocScanStatisticsDAO docScanStatisticsDAO;

	/**
	 * Recupera una estadística a partir del identificador de reserva de trabajo
	 * de digitalización o carátula asociado a la digitalización.
	 * 
	 * @param remoteReqId
	 *            Identificador de reserva de trabajo de digitalización o
	 *            carátula asociado a la digitalización.
	 * @return DocScanStatisticsDTO.
	 * @throws StatisticsDAOException
	 */
	public DocScanStatisticsDTO getDocScanStatisticsByRemoteReq(String remoteReqId) throws StatisticsDAOException {
		DocScanStatisticsDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-STATISTICS-MODEL] Se inicia la recuperación de la estadística a partir del identificador de reserva de digitalización " + remoteReqId + " ...");
		}
		LOG.debug("[WEBSCAN-STATISTICS-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(remoteReqId);
		} catch (WebscanException e) {
			throw new StatisticsDAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-STATISTICS-MODEL] Ejecutando la consulta en BBDD ...");
		res = docScanStatisticsDAO.getDocScanStatisticsDTOByRemoteReqId(remoteReqId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística " + res.getId() + " recuperada de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera una estadística a partir del identificador interno de la
	 * estadística.
	 * 
	 * @param docScanStatisticsId
	 *            Identificador interno de la estadística.
	 * @return DocScanStatisticsDTO.
	 * @throws StatisticsDAOException
	 */
	public DocScanStatisticsDTO getDocScanStatistics(Long docScanStatisticsId) throws StatisticsDAOException {
		DocScanStatisticsDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-STATISTICS-MODEL] Se inicia la recuperación de la estadística a partir del identificador interno de la estadística " + docScanStatisticsId + " ...");
		}
		LOG.debug("[WEBSCAN-STATISTICS-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(docScanStatisticsId);
		} catch (WebscanException e) {
			throw new StatisticsDAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-STATISTICS-MODEL] Ejecutando la consulta en BBDD ...");
		res = docScanStatisticsDAO.getDocScanStatisticsDTO(docScanStatisticsId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística " + res.getId() + " recuperada de BBDD.");
		}
		return res;
	}

	/**
	 * Crea una nueva estadística de la digitalización de documentos en el
	 * modelo de datos.
	 * 
	 * @param docScanStatisticsDTO
	 *            Estadística de la digitalización de documentos.
	 * @return La estadística de la digitalización de documentos creada.
	 * @throws StatisticsDAOException
	 */
	public DocScanStatisticsDTO createDocScanStatistics(DocScanStatisticsDTO docScanStatisticsDTO) throws StatisticsDAOException {
		Long dbsId;
		DocScanStatisticsDTO res = new DocScanStatisticsDTO();

		LOG.debug("[WEBSCAN-STATISTICS-MODEL] Se inicia el registro de una nueva estadística de la digitalización de documentos ...");

		dbsId = docScanStatisticsDAO.createDocScanStatistics(docScanStatisticsDTO);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística " + dbsId + " registrada en el Sistema ...");
		}

		res = docScanStatisticsDAO.getDocScanStatisticsDTO(dbsId);

		return res;
	}

	/**
	 * Actualiza la estadística de la digitalización de documentos.
	 * 
	 * @param docScanStatisticsDTO
	 *            Estadística de la digitalización de documentos.
	 * @throws StatisticsDAOException
	 */
	public void updateDocScanStatistics(DocScanStatisticsDTO docScanStatisticsDTO) throws StatisticsDAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-STATISTICS-MODEL] Se inicia la actualización de una estadística de la digitalización de documentos ...");
		}

		docScanStatisticsDAO.updateDocScanStatistics(docScanStatisticsDTO);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística " + docScanStatisticsDTO.getId() + " actualizada en el Sistema ...");
		}
	}

	/**
	 * Establece un nuevo DAO sobre las entidades pertenecientes al modelo de
	 * datos de las estadísticas de la digitalización de documentos.
	 * 
	 * @param aDocScanStatisticsDAO
	 *            Nuevo DAO sobre las entidades pertenecientes al modelo de
	 *            datos de las estadísticas de la digitalización de documentos.
	 */
	public void setDocScanStatisticsDAO(DocScanStatisticsDAO aDocScanStatisticsDAO) {
		this.docScanStatisticsDAO = aDocScanStatisticsDAO;
	}

}
