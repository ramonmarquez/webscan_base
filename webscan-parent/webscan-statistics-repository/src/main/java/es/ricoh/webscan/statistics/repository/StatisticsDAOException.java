/**
 * <b>Archivo:</b><p>es.ricoh.webscan.statistics.repository.StatisticsDAOException.java.</p>
 * <b>Descripción:</b><p> Excepción que encapsula los errores producidos en el componente DAO del
* modelo de datos de las estadísticas de la digitalización de documentos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.statistics.repository;

import es.ricoh.webscan.model.DAOException;

/**
 * Excepción que encapsula los errores producidos en el componente DAO del
 * modelo de datos de las estadísticas de la digitalización de documentos.
 * <p>
 * Clase StatisticsDAOException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class StatisticsDAOException extends DAOException {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Parámetro de entrada incorrecto. Tipo de codificación no especificado.
	 */
	public static final String CODE_500 = "COD_500";

	/**
	 * Parámetro de entrada incorrecto. Identificador de usuario no
	 * especificado.
	 */
	public static final String CODE_501 = "COD_501";

	/**
	 * Parámetro de entrada incorrecto. Datos de perfil de digitalización
	 * incompletos, metadatos no especificados.
	 */
	public static final String CODE_502 = "COD_502";

	/**
	 * Parámetro de entrada incorrecto. Datos de perfil de digitalización
	 * incompletos, denominación de metadatos no especificada o con longitud
	 * superior a la admitida.
	 */
	public static final String CODE_503 = "COD_503";

	/**
	 * Parámetro de entrada incorrecto. Datos de perfil de digitalización
	 * incompletos, valor de metadatos no especificado o con longitud superior a
	 * la admitida.
	 */
	public static final String CODE_504 = "COD_504";

	/**
	 * Error registrando la información en el modelo de datos.
	 */
	public static final String CODE_505 = "COD_505";

	/**
	 * Constructor sin argumentos.
	 */
	public StatisticsDAOException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public StatisticsDAOException(String aCode, String aMessage) {
		super(aCode, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public StatisticsDAOException(String aMessage) {
		super(CODE_999, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public StatisticsDAOException(Throwable aCause) {
		super(CODE_999, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public StatisticsDAOException(final String aMessage, Throwable aCause) {
		super(CODE_999, aMessage, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public StatisticsDAOException(final String aCode, final String aMessage, Throwable aCause) {
		super(aCode, aMessage, aCause);
	}

}
