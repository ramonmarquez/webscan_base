/**
 * Capa de acceso a datos al modelo de datos de persistencia de datos de uso del
 * sistema.
 */
package es.ricoh.webscan.statistics.repository;