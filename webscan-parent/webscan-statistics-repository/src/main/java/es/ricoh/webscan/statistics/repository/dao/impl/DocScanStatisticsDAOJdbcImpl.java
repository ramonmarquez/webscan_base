/**
 * <b>Archivo:</b><p>es.ricoh.webscan.statistics.repository.dao.impl.DocScanStatisticsDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre las entidades pertenecientes al
* modelo de datos de las estadísticas de la digitalización de documentos.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */

package es.ricoh.webscan.statistics.repository.dao.impl;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.statistics.model.domain.DocScanStatistics;
import es.ricoh.webscan.statistics.model.dto.DocScanStatisticsDTO;
import es.ricoh.webscan.statistics.model.dto.MappingUtilities;
import es.ricoh.webscan.statistics.repository.StatisticsDAOException;
import es.ricoh.webscan.statistics.repository.dao.DocScanStatisticsDAO;
import es.ricoh.webscan.model.dao.impl.CommonUtilitiesDAOJdbcImpl;

/**
 * Implementación JPA 2.0 de la capa DAO sobre las entidades pertenecientes al
 * modelo de datos de las estadísticas de la digitalización de documentos.
 * <p>
 * Clase DocScanStatisticsDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class DocScanStatisticsDAOJdbcImpl implements DocScanStatisticsDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DocScanStatisticsDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la manipulación de las entidades de incluidas en el
	 * contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-gv-statistics-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.statistics.repository.dao.DocScanStatisticsDAO#getDocScanStatisticsDTO(java.lang.Long)
	 */
	@Override
	public DocScanStatisticsDTO getDocScanStatisticsDTO(Long docScanStatisticsId) throws StatisticsDAOException {
		DocScanStatistics entity;
		DocScanStatisticsDTO res = null;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Recuperando la estadística de digitalización de documentos " + docScanStatisticsId + " ...");
			}
			entity = entityManager.find(DocScanStatistics.class, docScanStatisticsId);

			res = MappingUtilities.fillDocScanStatisticsDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística de digitalización de documentos " + res.getId() + " recuperada de BBDD.");
			}
		} catch (Exception e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error recuperando la estadística de digitalización de documentos " + docScanStatisticsId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.statistics.repository.dao.DocScanStatisticsDAO#getDocScanStatisticsEntity(java.lang.Long)
	 */
	@Override
	public DocScanStatistics getDocScanStatisticsEntity(Long docScanStatisticsDTOId) throws StatisticsDAOException {
		DocScanStatistics entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Recuperando la estadística de digitalización de documentos " + docScanStatisticsDTOId + " ...");
			}
			entity = entityManager.find(DocScanStatistics.class, docScanStatisticsDTOId);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística de digitalización de documentos " + entity.getId() + " recuperada de BBDD.");
			}
		} catch (Exception e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error recuperando la estadística de digitalización de documentos " + docScanStatisticsDTOId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return entity;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.statistics.repository.dao.DocScanStatisticsDAO#createDocScanStatistics(es.ricoh.webscan.statistics.model.dto.DocScanStatistics.java)
	 */
	@Override
	public Long createDocScanStatistics(DocScanStatisticsDTO docScanStatisticsDTO) throws StatisticsDAOException {
		Long res;
		DocScanStatistics entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-STATISTICS-MODEL] Creando la estadística de digitalización de documentos " + docScanStatisticsDTO + " ...");
		}

		try {
			entity = MappingUtilities.fillDocScanStatisticsEntity(docScanStatisticsDTO);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística " + entity.getId() + " registrada, comprobando metadatos ...");
			}

			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística " + entity.getId() + " creada en BBDD.");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error al crear la estadística " + docScanStatisticsDTO + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(StatisticsDAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error al crear la estadística " + docScanStatisticsDTO + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error al crear la estadística " + docScanStatisticsDTO + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error al crear la estadística " + docScanStatisticsDTO + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error al crear la estadística " + docScanStatisticsDTO + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.statistics.repository.dao.DocScanStatisticsDAO#updateDocScanStatistics(es.ricoh.webscan.statistics.model.dto.DocScanStatistics.java)
	 */
	@Override
	public void updateDocScanStatistics(DocScanStatisticsDTO docScanStatisticsDTO) throws StatisticsDAOException {
		DocScanStatistics entity;
		String excMsg;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-STATISTICS-MODEL] Actualizando la estadística de digitalización de documentos " + docScanStatisticsDTO + " ...");
		}
		try {
			entity = getDocScanStatisticsEntity(docScanStatisticsDTO.getId());

			// Se actualizan los datos globales
			entity.setUserDpto(docScanStatisticsDTO.getUserDpto());
			entity.setDocScanEndDate(docScanStatisticsDTO.getDocScanEndDate());
			entity.setDocScanInitDate(docScanStatisticsDTO.getDocScanInitDate());
			entity.setFileFormat(docScanStatisticsDTO.getFileFormat());
			entity.setPixelType(docScanStatisticsDTO.getPixelType());
			entity.setResolution(docScanStatisticsDTO.getResolution());
			entity.setScanWorkNumPages(docScanStatisticsDTO.getScanWorkNumPages());
			entity.setScanWorkSize(docScanStatisticsDTO.getScanWorkSize());
			entity.setWorkingMode(docScanStatisticsDTO.getWorkingMode());
			entity.setScanTemplateId(docScanStatisticsDTO.getScanTemplateId());
			entity.setOrgUnitId(docScanStatisticsDTO.getOrgUnitId());
			entity.setSpacerProcDisabled(docScanStatisticsDTO.getSpacerProcDisabled());

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Modificando la estadística " + docScanStatisticsDTO.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística " + docScanStatisticsDTO.getId() + " actualizada en BBDD ...");
			}
		} catch (StatisticsDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error al actualizar la estadística " + docScanStatisticsDTO + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error al actualizar la estadística " + docScanStatisticsDTO + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error al actualizar la estadística " + docScanStatisticsDTO + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error al actualizar la estadística " + docScanStatisticsDTO + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.statistics.repository.dao.DocScanStatisticsDAO#getDocScanStatisticsDTOByRemoteReqId(String)
	 */
	@Override
	public DocScanStatisticsDTO getDocScanStatisticsDTOByRemoteReqId(String remoteReqId) throws StatisticsDAOException {
		String excMsg;
		String jpqlQuery;
		DocScanStatisticsDTO res;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Recuperando la estadística de digitalización de documentos " + remoteReqId + " ...");
			}

			jpqlQuery = "SELECT new es.ricoh.webscan.statistics.model.dto.DocScanStatisticsDTO(id, scanProfileId, orgUnitId, userDpto, workingMode, fileFormat, resolution, pixelType, scanWorkSize, scanWorkNumPages, remoteReqId, docScanInitDate, docScanEndDate) FROM DocScanStatistics WHERE remoteReqId = :remoteReqId";
			TypedQuery<DocScanStatisticsDTO> query = entityManager.createQuery(jpqlQuery, DocScanStatisticsDTO.class);
			query.setParameter("remoteReqId", remoteReqId);
			res = query.getSingleResult();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-STATISTICS-MODEL] Estadística " + res.getId() + " recuperada de la BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error recuperando la estadística " + remoteReqId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error recuperando la estadística " + remoteReqId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error recuperando la estadística " + remoteReqId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error recuperando la estadística " + remoteReqId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error recuperando la estadística " + remoteReqId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error recuperando la estadística " + remoteReqId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-STATISTICS-MODEL] Error recuperando la estadística " + remoteReqId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new StatisticsDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

}
