/**
 * Entidades JPA 2.0 que representan el modelo de datos de persistencia de datos
 * de uso del sistema.
 */
package es.ricoh.webscan.statistics.model.domain;