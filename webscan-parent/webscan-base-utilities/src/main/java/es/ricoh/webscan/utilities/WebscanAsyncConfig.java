/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.utilities.WebscanAsyncConfig.java.</p>
* <b>Descripción:</b><p> Configuración del gestor de ejecuciones asíncronas del Sistema.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.utilities;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.DefaultManagedAwareThreadFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import es.ricoh.webscan.WebscanConstants;

/**
 * Configuración del gestor de ejecuciones asíncronas del proceso de
 * digitalización.
 * <p>
 * Clase WebscanAsyncConfig.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@EnableAsync
@Configuration("webscanAsyncConfig")
public class WebscanAsyncConfig implements AsyncConfigurer {

	/**
	 * Utilidades globales del Sistema.
	 */
	@Autowired
	private Utils webscanUtilParams;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.scheduling.annotation.AsyncConfigurer#getAsyncExecutor()
	 */
	@Override
	@Bean
	@Qualifier("webscanTaskExecutor")
	public Executor getAsyncExecutor() {
		Integer pValue;
		ThreadPoolTaskExecutor res = new ThreadPoolTaskExecutor();

		pValue = webscanUtilParams.getIntegerParam(webscanUtilParams.getProperty(WebscanConstants.THREAD_POOL_KEEP_ALIVE_PROP_NAME));
		if (pValue != null) {
			res.setKeepAliveSeconds(pValue);
		}
		pValue = webscanUtilParams.getIntegerParam(webscanUtilParams.getProperty(WebscanConstants.THREAD_POOL_MIN_SIZE_PROP_NAME));
		if (pValue != null) {
			res.setCorePoolSize(pValue);
		}

		pValue = webscanUtilParams.getIntegerParam(webscanUtilParams.getProperty(WebscanConstants.THREAD_POOL_MAX_SIZE_PROP_NAME));
		if (pValue != null) {
			res.setMaxPoolSize(pValue);
		}

		pValue = webscanUtilParams.getIntegerParam(webscanUtilParams.getProperty(WebscanConstants.THREAD_POOL_QUEUE_CAP_PROP_NAME));
		if (pValue != null) {
			res.setQueueCapacity(pValue);
		}

		if (webscanUtilParams.getProperty(WebscanConstants.THREAD_POOL_NAME_PREFIX_PROP_NAME) != null) {
			res.setThreadNamePrefix(webscanUtilParams.getProperty(WebscanConstants.THREAD_POOL_NAME_PREFIX_PROP_NAME));
		}

		res.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
		res.setThreadFactory(new DefaultManagedAwareThreadFactory());

		res.initialize();

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.scheduling.annotation.AsyncConfigurer#getAsyncUncaughtExceptionHandler()
	 */
	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new SimpleAsyncUncaughtExceptionHandler();
	}

}
