/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.utilities.FileSystemUtilities.java.</p>
* <b>Descripción:</b><p> Utilidades para la escritura y lectura de archivos localizados en el sistema
* de archivos.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.utilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import es.ricoh.webscan.ValidationUtilities;
import es.ricoh.webscan.WebscanConstants;
import es.ricoh.webscan.WebscanException;

/**
 * Utilidades para la escritura y lectura de archivos localizados en el sistema
 * de archivos.
 * <p>
 * Clase FileSystemUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class FileSystemUtilities {

	/**
	 * Constructor method for the class FileSystemUtilities.java.
	 */
	private FileSystemUtilities() {
	}

	/**
	 * Lee el contenido de un archivo localizado en el sistema de archivos.
	 * 
	 * @param path
	 *            ruta del archivo en el sistema de archivos.
	 * @return contenido del archivo localizado en el sistema de archivos.
	 * @throws WebscanUtilitiesException
	 *             Si el parámetro de entrada es nulo o vacío, el archivo no
	 *             existe, o se produce algún error en la lectura.
	 */
	public static byte[ ] readFile(String path) throws WebscanUtilitiesException {
		FileInputStream fileReader = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[ ] res = null;

		try {
			ValidationUtilities.checkEmptyObject(path, "No se ha especificado la ruta al archivo que se desea leer.");

			fileReader = new FileInputStream(new File(path));
			byte[ ] bs = new byte[WebscanConstants.INT_1024];
			int numRead;
			while ((numRead = fileReader.read(bs, 0, bs.length)) >= 0) {
				baos.write(bs, 0, numRead);
			}
			res = baos.toByteArray();
		} catch (WebscanException e) {
			throw new WebscanUtilitiesException(e.getCode(), e.getMessage());
		} catch (IOException ioe) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_305, "Se produjo un error al intentar leer el archivo " + path + ". Error: " + ioe.getMessage());
		} catch (Exception e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_305, "Se produjo un error al intentar leer el archivo " + path + ". Error: " + e.getMessage());
		} finally {
			try {
				baos.close();
			} catch (Exception e) {}
			closeInputStream(fileReader);
		}

		return res;
	}

	/**
	 * Write data into a file. If file doesn't exist, it is created.
	 * 
	 * @param data
	 *            information to include into file.
	 * @param filename
	 *            name of file to record.
	 * @throws WebscanUtilitiesException
	 *             if a error happens accessing to file.
	 */
	public static void writeFile(byte[ ] data, String filename) throws WebscanUtilitiesException {
		FileOutputStream fos = null;

		try {
			ValidationUtilities.checkEmptyObject(data, "No se han especificado datos para escribir en el archivo " + filename + ".");
			ValidationUtilities.checkEmptyObject(filename, "No se ha especificado la ruta del archivo que se desea escribir.");

			fos = new FileOutputStream(new File(filename));
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			byte[ ] buffer = new byte[WebscanConstants.INT_1024];
			int bytesReaded = 0;
			while ((bytesReaded = bais.read(buffer)) >= 0) {
				fos.write(buffer, 0, bytesReaded);
			}
		} catch (WebscanException e) {
			throw new WebscanUtilitiesException(e.getCode(), e.getMessage());
		} catch (FileNotFoundException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_306, "Se produjo un error al intentar almacenar el archivo " + filename + ". Error: " + e.getMessage());
		} catch (IOException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_306, "Se produjo un error al intentar almacenar el archivo " + filename + ". Error: " + e.getMessage());
		} finally {
			try {
				if (fos != null) {
					fos.flush();
					fos.close();
				}
			} catch (IOException e) {}
		}
	}

	/**
	 * Crea un directorio, y la ruta hasta este, si no existe, a partir de su
	 * ruta en el sistema de archivos.
	 * 
	 * @param dirPath
	 *            Ruta del directorio a crear.
	 * @return Ruta absoluta en el sistema de archivos del directorio creado.
	 * @throws WebscanUtilitiesException
	 *             Si la ruta especificada no es un directorio, no es una ruta
	 *             absoluta, o sucede algún error en la creación del directorio
	 *             y su ruta.
	 */
	public static synchronized String createDirectory(String dirPath) throws WebscanUtilitiesException {
		File dirPathFile;
		String res = null;
		try {
			ValidationUtilities.checkEmptyObject(dirPath, "No se ha especificado la ruta y nombre del directorio a crear.");

			dirPathFile = new File(dirPath);

			if (!dirPathFile.isDirectory() || !dirPathFile.isAbsolute()) {
				throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_307, "Error al crear la estructura de directorios para la ruta " + dirPath + ". La ruta especificada no es un directorio o no es absoluta.");
			}

			if (!dirPathFile.exists()) {
				dirPathFile.mkdirs();
			}

			res = dirPathFile.getAbsolutePath();
		} catch (WebscanUtilitiesException e) {
			throw e;
		} catch (WebscanException e) {
			throw new WebscanUtilitiesException(e.getCode(), e.getMessage());
		} catch (Exception e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_307, "Error al crear la estructura de directorios para la ruta " + dirPath + ". Error: " + e.getMessage());
		}

		return res;
	}

	/**
	 * Elimina un directoriodel sistema de archivos.
	 * 
	 * @param dirPath
	 *            Ruta del directorio a crear.
	 * @throws WebscanUtilitiesException
	 *             Si la ruta especificada no es un directorio, no es una ruta
	 *             absoluta, o sucede algún error en el borrado del directorio.
	 */
	public static synchronized void removeDirectory(String dirPath) throws WebscanUtilitiesException {
		File dirPathFile;
		File childFile;
		try {
			ValidationUtilities.checkEmptyObject(dirPath, "No se ha especificado la ruta y nombre del directorio a crear.");

			dirPathFile = new File(dirPath);

			if (!dirPathFile.isDirectory() || !dirPathFile.isAbsolute()) {
				throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_307, "Error al eliminar la estructura de directorios para la ruta " + dirPath + ". La ruta especificada no es un directorio o no es absoluta.");
			}

			if (!dirPathFile.exists()) {
				throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_307, "Error al crear la estructura de directorios para la ruta " + dirPath + ". La ruta especificada no existe.");
			}

			String[ ] childFiles = dirPathFile.list();
			if (childFiles != null && childFiles.length > 0) {
				for (int i = 0; i < childFiles.length; i++) {
					childFile = new File(childFiles[i]);

					if (childFile.isFile()) {
						childFile.delete();
					} else if (childFile.isDirectory()) {
						removeDirectory(childFiles[i]);
					}
				}
			}

			dirPathFile.delete();
		} catch (WebscanUtilitiesException e) {
			throw e;
		} catch (WebscanException e) {
			throw new WebscanUtilitiesException(e.getCode(), e.getMessage());
		} catch (Exception e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_307, "Error al crear la estructura de directorios para la ruta " + dirPath + ". Error: " + e.getMessage());
		}
	}

	/**
	 * Convierte un input stream en un arra de bytes.
	 * 
	 * @param input
	 *            input stream.
	 * @return byte array.
	 * @throws IOException
	 *             Si se produce un error en la lectura del input sream.
	 */
	public static byte[ ] readDataFromInputStream(final InputStream input) throws IOException {
		final int MAX_BYTE = 4096;
		if (input == null) {
			return new byte[0];
		}
		int nBytes = 0;
		byte[ ] buffer = new byte[MAX_BYTE];
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while ((nBytes = input.read(buffer)) != -1) {
			baos.write(buffer, 0, nBytes);
		}
		return baos.toByteArray();
	}

	/**
	 * Cierra de forma segura un InputStream.
	 * 
	 * @param is
	 *            objeto InputStream.
	 */
	public static void closeInputStream(InputStream is) {
		if (is != null) {
			try {
				is.close();
			} catch (IOException e) {}
		}
	}

}
