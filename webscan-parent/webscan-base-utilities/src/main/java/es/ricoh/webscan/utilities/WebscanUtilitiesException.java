/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.utilities.WebscanUtilitiesException.java.</p>
 * <b>Descripción:</b><p> Excepción que encapsula los errores producidos en el
 * componente de utilidades.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.utilities;

import es.ricoh.webscan.WebscanException;

/**
 * Excepción que encapsula los errores producidos en el componente de
 * utilidades.
 * <p>
 * Clase WebscanUtilitiesException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 *
 * @version 2.0.
 */
public final class WebscanUtilitiesException extends WebscanException {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tipo de almacén de certificados no soportado por el proveedor de
	 * seguridad.
	 */
	public static final String CODE_300 = "COD_300";

	/**
	 * Algoritmo de verificación de integridad del alamacén de certificados no
	 * encontrado.
	 */
	public static final String CODE_301 = "COD_301";

	/**
	 * Uno o más certificados del almacén no pueden ser recuperados.
	 */
	public static final String CODE_302 = "COD_302";

	/**
	 * Errores asociados a la lectura del archivo que aloja el almacén de
	 * certificados.
	 */
	public static final String CODE_303 = "COD_303";

	/**
	 * Errores criptográficos, relacionados por ejemplo con el cálculo de hashs.
	 */
	public static final String CODE_304 = "COD_304";

	/**
	 * Errores asociados a la lectura de un archivo.
	 */
	public static final String CODE_305 = "COD_305";

	/**
	 * Errores asociados a la escritura de un archivo.
	 */
	public static final String CODE_306 = "COD_306";

	/**
	 * Errores asociados a la creación o borrado de una estrucutra de
	 * directorios.
	 */
	public static final String CODE_307 = "COD_307";

	/**
	 * Constructor sin argumentos.
	 */
	public WebscanUtilitiesException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public WebscanUtilitiesException(String aCode, String aMessage) {
		super(aCode, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public WebscanUtilitiesException(String aMessage) {
		super(CODE_999, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public WebscanUtilitiesException(Throwable aCause) {
		super(CODE_999, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public WebscanUtilitiesException(final String aMessage, Throwable aCause) {
		super(CODE_999, aMessage, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public WebscanUtilitiesException(final String aCode, final String aMessage, Throwable aCause) {
		super(aCode, aMessage, aCause);
	}

}
