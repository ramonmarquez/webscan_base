/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.utilities.MimeType.java.</p>
* <b>Descripción:</b><p> Representación de los tipos MIME aceptados para los diferentes contenidos de
* un documento digitalizado.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.utilities;

import es.ricoh.webscan.WebscanConstants;

/**
 * Representación de los tipos MIME aceptados para los diferentes contenidos de
 * un documento digitalizado.
 * <p>
 * Clase MimeType.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum MimeType {

	/**
	 * Tipo MIME application/pkcs7-signature.
	 */
	SIGNATURE(WebscanConstants.SIGNATURE_MIME_TYPE_CONTENT_TYPE, ".p7s", "application/pkcs7-signature"),
	/**
	 * Tipo MIME application/pdf.
	 */
	PDF_CONTENT(WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE, ".pdf", "application/pdf"),
	/**
	 * Tipo MIME image/png.
	 */
	PNG_IMG_CONTENT(WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE, ".png", "image/png"),
	/**
	 * Tipo MIME image/gif.
	 */
	GIF_IMG_CONTENT(WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE, ".gif", "image/gif"),
	/**
	 * Tipo MIME image/jpeg.
	 */
	JPG_IMG_CONTENT(WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE, ".jpg", "image/jpeg"),
	/**
	 * Tipo MIME image/x-tiff.
	 */
	X_TIFF_IMG_CONTENT(WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE, ".tif", "image/x-tiff"),
	/**
	 * Tipo MIME image/tiff.
	 */
	TIFF_IMG_CONTENT(WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE, ".tif", "image/tiff"),
	/**
	 * Tipo MIME text/plain.
	 */
	TEXT_PLAIN_OCR(WebscanConstants.OCR_MIME_TYPE_CONTENT_TYPE, ".txt", "text/plain");

	/**
	 * Tipo de contenido del documento.
	 */
	private String contentType;

	/**
	 * Extensión de archivo asociada al tipo MIME.
	 */
	private String extension;

	/**
	 * Tipo MIME.
	 */
	private String type;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aContentType
	 *            Tipo de contenido del documento.
	 * @param anExtension
	 *            Extensión de archivo asociada al tipo MIME.
	 * @param aType
	 *            Tipo MIME.
	 */
	MimeType(String aContentType, String anExtension, String aType) {
		this.contentType = aContentType;
		this.extension = anExtension;
		this.type = aType;
	}

	/**
	 * Recupera un tipo MIME a partir de su tipo. Si no existe, devuelve null.
	 * 
	 * @param type
	 *            tipo.
	 * @return tipo MIME.
	 */
	public static MimeType getMimeTypeByType(String type) {
		Boolean found = Boolean.FALSE;
		MimeType[ ] values;
		MimeType res = null;

		values = MimeType.values();

		for (int i = 0; !found && i < values.length; i++) {
			if (values[i].getType().equals(type)) {
				res = values[i];
				found = Boolean.TRUE;
			}
		}

		return res;
	}

	/**
	 * Obtiene el tipo de contenido del documento.
	 * 
	 * @return el tipo de contenido del documento.
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * Obtiene la extensión de archivo asociada al tipo MIME.
	 * 
	 * @return la extensión de archivo asociada al tipo MIME.
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * Obtiene el tipo MIME.
	 * 
	 * @return el tipo MIME.
	 */
	public String getType() {
		return type;
	}

}
