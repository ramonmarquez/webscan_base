/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.WebscanException.java.</p>
 * <b>Descripción:</b><p> Excepción genérica que encapsula los errores producidos en los diferentes componentes del sistema.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan;

/**
 * Excepción genérica que encapsula los errores producidos en los diferentes
 * componentes del sistema.
 * <p>
 * Clase WebscanException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 *
 * @version 2.0.
 */
public class WebscanException extends Exception {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Error de configuración del sistema. No fue configurado algún parámetro, o
	 * no fue configurado correctamente.
	 */
	public static final String CODE_993 = "COD_993";

	/**
	 * Error en la generación de informes. No fue posible obtener la información
	 * de base de datos.
	 */
	public static final String CODE_994 = "COD_994";

	/**
	 * Error en la generación de informes. Formato de informe desconocido.
	 */
	public static final String CODE_995 = "COD_995";

	/**
	 * Error en la generación de informes. Error en el proceso interno de
	 * generación en un formato determinado.
	 */
	public static final String CODE_996 = "COD_996";

	/**
	 * Error de comunicación, integración o invocación de servicio o sistema
	 * externo.
	 */
	public static final String CODE_997 = "COD_997";

	/**
	 * Datos o parámetros mal formados.
	 */
	public static final String CODE_998 = "COD_998";

	/**
	 * Causa del error desconocida.
	 */
	public static final String CODE_999 = "COD_999";

	/**
	 * Código del error.
	 */
	protected String code = CODE_999;

	/**
	 * Constructor sin argumentos.
	 */
	public WebscanException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public WebscanException(String aCode, String aMessage) {
		super(aMessage);
		this.code = aCode;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public WebscanException(String aMessage) {
		super(aMessage);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public WebscanException(Throwable aCause) {
		super(aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public WebscanException(final String aMessage, Throwable aCause) {
		super(aMessage, aCause);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public WebscanException(final String aCode, final String aMessage, Throwable aCause) {
		super(aMessage, aCause);
		this.code = aCode;
	}

	/**
	 * Obtiene el código de error de la excepción.
	 *
	 * @return código de error de la excepción.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Establece el código de error de la excepción.
	 *
	 * @param aCode
	 *            código de error de la excepción.
	 */
	public void setCode(final String aCode) {
		this.code = aCode;
	}
}
