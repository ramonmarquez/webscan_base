/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.WebscanConstants.java.</p>
* <b>Descripción:</b><p> .</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan;

/**
 * Constantes de la solución base Webscan.
 * <p>
 * Clase WebscanConstants.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface WebscanConstants {

	// Nombre de propiedades
	/**
	 * Parámetro que configura el modo en el que son persistidos los documentos,
	 * sus firmas y otros contenidos.
	 */
	String SCANNED_DOCS_STORE_MODE_PROP_NAME = "scannedDocs.store.mode";

	/**
	 * Parámetro que configura la ruta raíz donde son almacenados temporalmente
	 * los documentos digitalizados en el modo de almacenamiento fileSystem.
	 */
	String SCANNED_DOCS_FS_STORE_MODE_ROOT_PATH_PROP_NAME = "scannedDocs.store.fileSystem.rootPath";
	/**
	 * Parámetro que configura el algoritmo has que se aplica.
	 */
	String SCANNED_DOCS_HASH_ALG_PROP_NAME = "scannedDocs.hash.algorithm";

	// Propiedades ejecución asíncrona
	/**
	 * Parámetro que configura el número mínimo de hilos para la ejecución de
	 * peticiones asíncronas.
	 */
	String THREAD_POOL_MIN_SIZE_PROP_NAME = "async.thread.pool.size.min";

	/**
	 * Parámetro que configura el número máximo de hilos para la ejecución de
	 * peticiones asíncronas.
	 */
	String THREAD_POOL_MAX_SIZE_PROP_NAME = "async.thread.pool.size.max";

	/**
	 * Parámetro que configura la capacidad máxima de la cola de ejecuciones
	 * asíncrona.
	 */
	String THREAD_POOL_QUEUE_CAP_PROP_NAME = "async.thread.pool.queue.capacity";

	/**
	 * Parámetro que configura el número máximo de segundos que se mantendran
	 * los hilos en pool.
	 */
	String THREAD_POOL_KEEP_ALIVE_PROP_NAME = "async.thread.pool.keepAlive";

	/**
	 * Parámetro que configura el prefijo del nombre de los hilos creados.
	 */
	String THREAD_POOL_NAME_PREFIX_PROP_NAME = "async.thread.pool.name.prefix";

	// Fin Propiedades ejecución asíncrona

	// Constantes
	/**
	 * Tipo de contenido de documento, contenido del documento.
	 */
	String CONTENT_MIME_TYPE_CONTENT_TYPE = "content";
	/**
	 * Tipo de contenido de documento, firma del documento.
	 */
	String SIGNATURE_MIME_TYPE_CONTENT_TYPE = "signature";
	/**
	 * Tipo de contenido de documento, texto OCR del documento.
	 */
	String OCR_MIME_TYPE_CONTENT_TYPE = "ocr";

	/**
	 * Tamaño buffer de lectura o escritura.
	 */
	int INT_1024 = 1024;

	/**
	 * Algoritmo de hash SHA1.
	 */
	String HASH_ALGORITHM_SHA1 = "SHA-1";

	/**
	 * Algoritmo de hash SHA256.
	 */
	String HASH_ALGORITHM_SHA256 = "SHA-256";

	/**
	 * Algoritmo de hash SHA384.
	 */
	String HASH_ALGORITHM_SHA384 = "SHA-384";

	/**
	 * Algoritmo de hash SHA512.
	 */
	String HASH_ALGORITHM_SHA512 = "SHA-512";

	// Valores por defecto

	/**
	 * Modo de almacenamiento por defecto de los documentos digitalizados.
	 */
	ScannedDocsStoreMode SCANNED_DOCS_STORE_MODE_DEFAULT_VALUE = ScannedDocsStoreMode.DDBB;

	/**
	 * Tipo de contenido de documento, texto OCR del documento.
	 */
	String SCANNED_DOCS_HASH_ALG_DEFAULT_VALUE = HASH_ALGORITHM_SHA256;

	/**
	 * Versión formato PDF.
	 */
	Float VERSION_PDF = 1.5f;

	/**
	 * Nombre de la etiqueta de multidioma cuyo valor es asignado a las
	 * propiedades que identifican al creador, productor y aplicación que genera
	 * los archivos en formato PDF.
	 */
	String CREATOR_PDF_PROPERTY_PARAM_NAME = "pdf.generation.properties.application";
}
