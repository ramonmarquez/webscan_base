/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.ResumeStage.java.</p>
 * <b>Descripción:</b><p> Fases del proceso de digitalización que pueden ser reanudadas.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan;

/**
 * Fases del proceso de digitalización que pueden ser reanudadas.
 * <p>
 * Class ResumeStage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum ResumeStage {

	/**
	 * Fase de captura.
	 */
	SCAN("Scan"),
	/**
	 * Fase de firma.
	 */
	SIGN("Sign"),
	/**
	 * Fase de metadatos.
	 */

	METADATA("Metadata");

	/**
	 * Modo de almacenamiento de los documentos digitalizados.
	 */
	private String name;

	/**
	 * Constructor method for the class ResumeStage.java.
	 * 
	 * @param aName
	 *            Nombre
	 */
	ResumeStage(String aName) {
		this.name = aName;

	}

	/**
	 * Gets the value of the attribute {@link #name}.
	 * 
	 * @return the value of the attribute {@link #name}.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Obtiene la fase.
	 * 
	 * @param value
	 *            valor de la fase a buscar.
	 * @return la fase.
	 */
	public static final ResumeStage getResumeStage(String value) {
		ResumeStage res = null;
		for (ResumeStage aux: ResumeStage.values()) {
			if (aux.name.equalsIgnoreCase(value)) {
				res = aux;
				break;
			}
		}
		return res;
	}

}
