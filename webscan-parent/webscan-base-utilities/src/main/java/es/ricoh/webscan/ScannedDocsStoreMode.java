/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.ScannedDocsStoreMode.java.</p>
* <b>Descripción:</b><p> Modo en el que son persistidos los documentos, sus firmas y otros contenidos.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan;

/**
 * Modo en el que son persistidos los documentos, sus firmas y otros contenidos.
 * <p>
 * Clase ScannedDocsStoreMode.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum ScannedDocsStoreMode {

	/**
	 * En base de datos, campos BLOB's.
	 */
	DDBB("ddbb"),
	/**
	 * En sistema de archivos.
	 */
	FILE_SYSTEM("fileSystem");

	/**
	 * Modo de almacenamiento de los documentos digitalizados.
	 */
	private String storeMode;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aStoreMode
	 *            Modo de almacenamiento de los documentos digitalizados.
	 */
	ScannedDocsStoreMode(final String aStoreMode) {
		this.storeMode = aStoreMode;

	}

	/**
	 * Obtiene el modo de almacenamiento de los documentos digitalizados.
	 * 
	 * @return el modo de almacenamiento de los documentos digitalizados.
	 */
	public String getStoreMode() {
		return storeMode;
	}

	/**
	 * Obtiene el modo de almacenamiento de los documentos digitalizados.
	 * 
	 * @param value
	 *            Identificador del nombre del modo a recuperar.
	 * @return el modo de almacenamiento de los documentos digitalizados.
	 */

	public static final ScannedDocsStoreMode getByName(final String value) {
		ScannedDocsStoreMode res = null;
		for (ScannedDocsStoreMode aux: ScannedDocsStoreMode.values()) {
			if (value != null && aux.storeMode != null && aux.storeMode.equalsIgnoreCase(value)) {
				res = aux;
			}
		}
		return res;
	}

}
