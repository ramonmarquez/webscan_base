/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.utilities.mail.MailMessage.java.</p>
 * <b>Descripción:</b><p> Clase que representa un mensaje de correo electrónico.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.utilities.mail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.MimeBodyPart;

/**
 * Clase que representa un mensaje de correo electrónico.
 * <p>
 * Clase MailMessage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class MailMessage implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Destinatarios del correo electrónico.
	 */
	private String toRecipients;

	/**
	 * Destinatarios en copia del correo electrónico.
	 */
	private String ccRecipients;

	/**
	 * Destinatarios en copia oculta del correo electrónico.
	 */
	private String bccRecipients;

	/**
	 * Emisor del correo electrónico.
	 */
	private String sender;

	/**
	 * Asunto del correo electrónico.
	 */
	private String subject;

	/**
	 * Cuerpo del correo electrónico.
	 */
	private String bodyMessage;

	/**
	 * Archivos adjuntos al correo electrónico.
	 */
	private List<MimeBodyPart> attachments = new ArrayList<MimeBodyPart>();

	/**
	 * Constructor sin argumentos.
	 */
	public MailMessage() {
		super();
	}

	/**
	 * Devuelve los destinatarios del correo electrónico.
	 * 
	 * @return Destinatarios del correo electrónico.
	 */
	public String getToRecipients() {
		return toRecipients;
	}

	/**
	 * Establece los destinatarios del correo electrónico.
	 * 
	 * @param aToRecipients
	 *            Nuevos destinatarios del correo electrónico.
	 */
	public void setToRecipients(String aToRecipients) {
		this.toRecipients = aToRecipients;
	}

	/**
	 * Devuelve los destinatarios en copia del correo electrónico.
	 * 
	 * @return Destinatarios en copia del correo electrónico.
	 */
	public String getCcRecipients() {
		return ccRecipients;
	}

	/**
	 * Establece los destinatarios en copia del correo electrónico.
	 * 
	 * @param aCcRecipients
	 *            Nuevos destinatarios en copia del correo electrónico.
	 */
	public void setCcRecipients(String aCcRecipients) {
		this.ccRecipients = aCcRecipients;
	}

	/**
	 * Devuelve los destinatarios en copia oculta del correo electrónico.
	 * 
	 * @return Destinatarios en copia oculta del correo electrónico.
	 */
	public String getBccRecipients() {
		return bccRecipients;
	}

	/**
	 * Establece los destinatarios en copia oculta del correo electrónico.
	 * 
	 * @param aBccRecipients
	 *            Nuevos destinatarios en copia oculta del correo electrónico.
	 */
	public void setBccRecipients(String aBccRecipients) {
		this.bccRecipients = aBccRecipients;
	}

	/**
	 * Devuelve el emisor del correo electrónico.
	 * 
	 * @return Destinatarios en copia oculta del correo electrónico.
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * Establece el emisor del correo electrónico.
	 * 
	 * @param aSender
	 *            Nuevo emisor del correo electrónico.
	 */
	public void setSender(String aSender) {
		this.sender = aSender;
	}

	/**
	 * Devuelve el asunto del correo electrónico.
	 * 
	 * @return Asunto del correo electrónico.
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Establece el asunto del correo electrónico.
	 * 
	 * @param aSubject
	 *            Nuevo asunto del correo electrónico.
	 */
	public void setSubject(String aSubject) {
		this.subject = aSubject;
	}

	/**
	 * Devuelve el cuerpo del correo electrónico.
	 * 
	 * @return Cuerpo del correo electrónico.
	 */
	public String getBodyMessage() {
		return bodyMessage;
	}

	/**
	 * Establece el cuerpo del correo electrónico.
	 * 
	 * @param aBodyMessage
	 *            Nuevo cuerpo del correo electrónico.
	 */
	public void setBodyMessage(String aBodyMessage) {
		this.bodyMessage = aBodyMessage;
	}

	/**
	 * Devuelve la lista de archivos adjuntos al correo electrónico.
	 * 
	 * @return Lista de archivos adjuntos al correo electrónico.
	 */
	public List<MimeBodyPart> getAttachments() {
		return attachments;
	}

	/**
	 * Establece la lista de archivos adjuntos al correo electrónico.
	 * 
	 * @param attachmentsParam
	 *            Nueva lista de archivos adjuntos al correo electrónico.
	 */
	public void setAttachments(List<MimeBodyPart> attachmentsParam) {
		if (attachmentsParam != null && !attachmentsParam.isEmpty()) {
			this.attachments.clear();
			this.attachments.addAll(attachmentsParam);
		}
	}

}
