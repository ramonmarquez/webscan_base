/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.ValidationUtilities.java.</p>
 * <b>Descripción:</b><p> Utilidad de comprobación y verificación de restricciones  genéricas sobre objetos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilidad de comprobación y verificación de restricciones genéricas sobre
 * objetos.
 * <p>
 * Clase ValidationUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ValidationUtilities {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationUtilities.class);

	/**
	 * Comprueba que un identificador no es nulo.
	 * 
	 * @param id
	 *            Identificador.
	 * @throws WebscanException
	 *             Si los datos no son válidos.
	 */
	public static void checkIdentifier(Object id) throws WebscanException {
		String excMsg;

		if (id == null || id.toString().isEmpty()) {
			excMsg = "No es posible llevar a cabo la operación solicitada. Identificador nulo o vacío.";
			LOGGER.error(excMsg);
			throw new WebscanException(WebscanException.CODE_998, excMsg);
		}
	}

	/**
	 * Comprueba que una lista no es nula o vacía.
	 * 
	 * @param collection
	 *            lista.
	 * @param <T>
	 *            tipo de la collección a tratar.
	 * @throws WebscanException
	 *             Si los datos no son válidos.
	 */
	public static <T> void checkCollection(Collection<T> collection) throws WebscanException {
		String excMsg;

		if (collection == null || collection.isEmpty()) {
			excMsg = "No es posible llevar a cabo la operación solicitada. Colección nula o vacía.";
			LOGGER.error(excMsg);
			throw new WebscanException(WebscanException.CODE_998, excMsg);
		}
	}

	/**
	 * Comprueba que un objeto no sea nulo.
	 * 
	 * @param value
	 *            Objeto.
	 * @param excMsg
	 *            Mensaje de error.
	 * @throws WebscanException
	 *             Si los datos no son válidos.
	 */
	public static void checkEmptyObject(Object value, String excMsg) throws WebscanException {
		if (value == null || value.toString().isEmpty()) {
			LOGGER.error(excMsg);
			throw new WebscanException(WebscanException.CODE_998, excMsg);
		}
	}

	/**
	 * Comprueba que una cadena de caracteres no supere un determinado tamaño.
	 * 
	 * @param value
	 *            Cadena de caracteres.
	 * @param maxLength
	 *            tamaño máximo de la cadena de caracteres.
	 * @param excMsg
	 *            Mensaje de error.
	 * @throws WebscanException
	 *             Si los datos no son válidos.
	 */
	public static void checkStringMaxLength(String value, int maxLength, String excMsg) throws WebscanException {
		if (value != null && value.length() > maxLength) {
			LOGGER.error(excMsg);
			throw new WebscanException(WebscanException.CODE_998, excMsg);
		}
	}
}
