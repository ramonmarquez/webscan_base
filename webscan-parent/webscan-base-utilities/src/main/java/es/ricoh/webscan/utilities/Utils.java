/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.utilities.Utils.java.</p>
* <b>Descripción:</b><p> Clase de utilidades que habilita el acceso a los parámetros de configuración
* del sistema.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/

package es.ricoh.webscan.utilities;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import es.ricoh.webscan.ValidationUtilities;
import es.ricoh.webscan.WebscanConstants;
import es.ricoh.webscan.WebscanException;

/**
 * Clase de utilidades que habilita el acceso a los parámetros de configuración
 * del sistema.
 * <p>
 * Clase Utils.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class Utils {

	/**
	 * Nombre de la propiedad que define el formato o patrón de las fechas
	 * incluidas en informes o listados.
	 */
	private static final String DATETIME_FORMAT_PROP_NAME = "reports.datetimeFormat";

	/**
	 * Parámetros de configuración del sistema.
	 */
	private Properties confParameters = new Properties();

	/**
	 * Obtiene el valor de una propiedad determinada del sistema.
	 * 
	 * @param key
	 *            identificador de una propiedad del sistema.
	 * @return el valor de una propiedad del sistema.
	 */
	public String getProperty(String key) {
		return confParameters.getProperty(key);
	}

	/**
	 * Obtiene el valor de una propiedad determinada del sistema, retornando un
	 * valor por defecto en caso de no existir.
	 * 
	 * @param key
	 *            identificador de una propiedad del sistema.
	 * @param def
	 *            valor por defecto de la propiedad del sistema.
	 * @return el valor de una propiedad del sistema, si está configurada. Valor
	 *         por defcto especificado como parámetro de entrada en caso de no
	 *         existir.
	 */
	public String getProperty(final String key, final String def) {
		if (confParameters.containsKey(key))
			return confParameters.getProperty(key);
		return def;
	}

	/**
	 * Función auxiliar para obtener la fecha en formato iso8601.
	 * 
	 * @param d
	 *            Fecha a formatear.
	 * @return String con el resultado de fecha formateada.
	 */
	public String formatDateTime(Date d) {
		SimpleDateFormat iso8601DateTimeFormater = new SimpleDateFormat(confParameters.getProperty(DATETIME_FORMAT_PROP_NAME, "yyyy-MM-dd'T'HH:mm:ssZ"));

		return iso8601DateTimeFormater.format(d);
	}

	/**
	 * Crea el directorio raíz en el sistema de archivos que alojará los
	 * contenidos de un documento. Crea todo
	 * 
	 * @param docId
	 *            Identificador del documento.
	 * @return Ruta del directorio raíz del en el sistema de archivos que
	 *         alojará los contenidos de un documento.
	 * @throws WebscanUtilitiesException
	 *             Si no es informado el identificador del documento, o se
	 *             produce algún error en la creación de la ruta completa del
	 *             directorio.
	 */
	public String createScannedDocumentFileSystemRootPath(Long docId) throws WebscanUtilitiesException {
		Calendar calendar = Calendar.getInstance();
		String res = null;
		String rootPath = getProperty(WebscanConstants.SCANNED_DOCS_FS_STORE_MODE_ROOT_PATH_PROP_NAME);
		String docRootDirPath;

		try {
			ValidationUtilities.checkEmptyObject(docId, "Identificador de documento no especificado.");
			ValidationUtilities.checkEmptyObject(rootPath, "Ruta raíz de almacenamiento de documentos nula o vacía. Configurar en el archivo webscanUtilities.properties.");

			// Se crea la ruta ráiz que alojará los diferentes contenidos del
			// documento digitalizado
			docRootDirPath = rootPath + File.separator + calendar.get(Calendar.YEAR) + File.separator + (calendar.get(Calendar.MONTH) + 1) + File.separator + calendar.get(Calendar.DAY_OF_MONTH) + File.separator + docId.toString();

			res = FileSystemUtilities.createDirectory(docRootDirPath);
		} catch (WebscanException e) {
			throw new WebscanUtilitiesException(e.getCode(), e.getMessage());
		}

		return res;
	}

	/**
	 * Almacena en el sistema de archivos un contenido de un documento
	 * digitalizado.
	 * 
	 * @param docRootPath
	 *            Ruta del directorio raíz del en el sistema de archivos que
	 *            alojará los contenidos de un documento.
	 * @param docId
	 *            Identificador del documento.
	 * @param mimeType
	 *            Tipo MIME del contenido del documento.
	 * @param content
	 *            contenido a persistir.
	 * @return ruta absoluta en el sistema de archivos del contenido guardado.
	 * @throws WebscanUtilitiesException
	 *             Si no es informado el identificador del documento, la ruta
	 *             raíz, el tipo MIME, o el contenido, o se produce algún error
	 *             en la escritura del con tenido en el sistema de archivos.
	 */
	public String persistDocContentInFileSystem(String docRootPath, Long docId, MimeType mimeType, byte[ ] content) throws WebscanUtilitiesException {
		String res = null;

		try {
			ValidationUtilities.checkEmptyObject(docId, "Identificador de documento no especificado.");
			ValidationUtilities.checkEmptyObject(docRootPath, "Ruta raíz de almacenamiento del documento " + docRootPath + " nula o vacía.");
			ValidationUtilities.checkEmptyObject(mimeType, "Tipo MIME del contenido del documento " + docId + " no especificado .");
			ValidationUtilities.checkEmptyObject(content, "Contenido del documento " + docId + " no especificado .");

			// Se crea la ruta ráiz que alojará los diferentes contenidos del
			// documento digitalizado
			res = docRootPath + (docRootPath.endsWith(File.separator) ? "" : File.separator) + getFileName(docId, mimeType);

			FileSystemUtilities.writeFile(content, res);
		} catch (WebscanUtilitiesException e) {
			throw e;
		} catch (WebscanException e) {
			throw new WebscanUtilitiesException(e.getCode(), e.getMessage());
		}

		return res;
	}

	/**
	 * Obtiene la ruta absoluta del directorio raíz donde se encuentran los
	 * archivos que alojan los contenidos de un documento digitalizado.
	 * 
	 * @param docContentPath
	 *            ruta absoluta del contenido del documento.
	 * @return la ruta absoluta del directorio raíz donde se encuentran los
	 *         archivos que alojan los contenidos de un documento digitalizado.
	 */
	public String getDocRootFileSystemPath(String docContentPath) {
		String res;

		res = docContentPath.substring(0, docContentPath.lastIndexOf(File.separator) - 1);

		return res;
	}

	/**
	 * Genera el nombre del archivo done será persisitido un contenido de un
	 * documento digitalizado.
	 * 
	 * @param docId
	 *            Identificador del documento.
	 * @param mimeType
	 *            Tipo MIME del contenido del documento.
	 * @return String con el nombre del fichero.
	 * @throws WebscanUtilitiesException
	 *             Si no es informado el identificador del documento.
	 */
	private String getFileName(Long docId, MimeType mimeType) throws WebscanUtilitiesException {
		String res = "";

		try {
			ValidationUtilities.checkEmptyObject(docId, "Identificador de documento no especificado.");
		} catch (WebscanException e) {
			throw new WebscanUtilitiesException(e.getCode(), e.getMessage());
		}

		if (WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE.equals(mimeType.getContentType())) {
			res = "doc_";
		} else if (WebscanConstants.SIGNATURE_MIME_TYPE_CONTENT_TYPE.equals(mimeType.getContentType())) {
			res = "signature_";
		} else if (WebscanConstants.OCR_MIME_TYPE_CONTENT_TYPE.equals(mimeType.getContentType())) {
			res = "ocr_";
		}

		res += docId.toString() + mimeType.getExtension();

		return res;
	}

	/**
	 * Obtiene un parámetro de configuración cuyo tipo es un número entero.
	 * 
	 * @param paramName
	 *            parámetros de configuración.
	 * @return Un número entero, si no se produce ningún error. En otro caso,
	 *         null.
	 */
	public Integer getIntegerParam(String paramName) {
		Integer res = null;
		try {
			res = Integer.parseInt(getProperty(paramName));
		} catch (Exception e) {
			// No hacer nada
		}

		return res;
	}

	/**
	 * Parámetros de configuración del sistema.
	 *
	 * @return Parámetros de configuración del sistema.
	 */
	public Properties getConfParameters() {
		return confParameters;
	}

	/**
	 * Establece el conjunto de parámetros de configuración del sistema.
	 *
	 * @param aConfParameters
	 *            conjunto de parámetros de configuración para el envío de
	 *            correos electrónicos.
	 */
	public void setConfParameters(final Properties aConfParameters) {
		if (aConfParameters != null && !aConfParameters.isEmpty()) {
			this.confParameters.clear();
			this.confParameters.putAll(aConfParameters);
		}
	}

	/**
	 * Comprueba si la cadena test está dentro de los valores de la clase
	 * enumerada.
	 * 
	 * @param enumData
	 *            Clase enumerada donde deseamos realizar la comprobación
	 * @param test
	 *            Cadena que deseamos verificar que identifica a un enumerado
	 * @param <E>
	 *            tipo de la clase enumerada.
	 * @return Boolean con el resultado
	 */

	public <E extends Enum<E>> boolean contains(Class<E> enumData, String test) {
		for (Enum<E> enumVal: enumData.getEnumConstants()) {
			if (enumVal.name().equals(test)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Comprueba si la cadena enumerado es la última de los enumerados.
	 * 
	 * @param enumData
	 *            Clase enum Test cadena con la información a verificar si está
	 *            contenida dentro de la clase enumerada que deseamos comprobar
	 * @param enumerado
	 *            Cadena que deseamos comprobar si identifica al último
	 *            enumerado
	 * @param <E>
	 *            tipo de la enumerada.
	 * @return Boolean con el resultado
	 */

	public <E extends Enum<E>> boolean isLastEnum(Class<E> enumData, String enumerado) {

		Enum<E> enumLastVal = enumData.getEnumConstants()[enumData.getEnumConstants().length - 1];
		if (enumLastVal.name().equals(enumerado)) {
			return true;
		}

		return false;
	}

	/**
	 * Devuelve el proximo enumerado de la clase enumerada pasada como parametro
	 * segun el valor enumerado.
	 * 
	 * @param enumData
	 *            Clase enum Test cadena con la información a verificar si está
	 *            contenida dentro de la clase enumerada que deseamos comprobar
	 * @param enumerado
	 *            Cadena identificativa del enumerado actual
	 * @param <E>
	 *            tipo de clase enumerado.
	 * @return Enumerado proximo
	 */

	public <E extends Enum<E>> E nextEnum(Class<E> enumData, String enumerado) {
		int a = 0;
		for (Enum<E> actual: enumData.getEnumConstants()) {
			if (actual.name().equals(enumerado) && (a + 1) < enumData.getEnumConstants().length) {
				return enumData.getEnumConstants()[a + 1];
			}
			a++;
		}
		// en caso de no encontrar el enumerado previo se devuelve null
		return null;
	}

}
