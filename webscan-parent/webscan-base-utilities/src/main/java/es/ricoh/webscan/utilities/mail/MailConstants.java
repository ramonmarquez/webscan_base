/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.utilities.mail.MailConstants.java</p>
 * <b>Descripción:</b><p> Constantes de la utilidad de envío de correos electrónicos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.utilities.mail;

/**
 * Constantes de la utilidad de envío de correos electrónicos.
 * <p>
 * Clase MailConstants.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface MailConstants {

	// Nombre de propiedades
	/**
	 * Parámetro que configura el host del servidor de correo.
	 */
	String MAIL_SMTP_HOST_PROP_NAME = "mail.smtp.host";

	/**
	 * Parámetro que configura el puerto por el que escucha el servidor de
	 * correo.
	 */
	String MAIL_SMTP_PORT_PROP_NAME = "mail.smtp.port";

	/**
	 * Parámetro que configura si el servidor de correo requiere autenticación.
	 */
	String MAIL_SMTP_AUTH_PROP_NAME = "mail.smtp.authentication";

	/**
	 * Parámetro que configura si se activa el modo depuración.
	 */
	String MAIL_SMTP_DEBUG_PROP_NAME = "mail.smtp.debug";

	/**
	 * Parámetro que configura el nombre de usuario para la autenticación en el
	 * servidor de correo.
	 */
	String MAIL_SMTP_USER_PROP_NAME = "mail.smtp.user";

	/**
	 * Parámetro que configura la password de usuario para la autenticación en
	 * el servidor de correo.
	 */
	String MAIL_SMTP_PASSWORD_PROP_NAME = "mail.smtp.password";

	/**
	 * Parámetro que configura si la comunicación con el servidor está
	 * securizada (cifrada). Los valores posibles son: none, ssl y tls.
	 */
	String MAIL_SMTP_SECURE_PROP_NAME = "mail.smtp.secure";

	/**
	 * Parámetro que configura la cuenta por defecto para envío de correos
	 * electrónicos.
	 */
	String MAIL_MESSAGE_SENDER_PROP_NAME = "mail.message.sender";

	/**
	 * Parámetro que configura el juego de caracteres de los correos enviados.
	 */
	String MAIL_MESSAGE_CHARSET_PROP_NAME = "mail.message.charset";

	/**
	 * Parámetro que configura el formato del cuerpo del correo electrónico
	 * (html o text).
	 */
	String MAIL_MESSAGE_BODY_TYPE_PROP_NAME = "mail.message.body.type";

	// Valores de propiedades
	/**
	 * Valor que configura la comunicación con el servidor de correo electrónico
	 * como no segura.
	 */
	String MAIL_SMTP_SECURE_NONE_VALUE = "none";

	/**
	 * Valor que establece la comunicación con el servidor de correo electrónico
	 * como segura, mediante el protocolo SSL.
	 */
	String MAIL_SMTP_SECURE_SSL_VALUE = "ssl";

	/**
	 * Valor que establece la comunicación con el servidor de correo electrónico
	 * como segura, mediante el protocolo TLS.
	 */
	String MAIL_SMTP_SECURE_TLS_VALUE = "tls";

	// Valores por defecto
	/**
	 * Puerto por defecto por el que escucha el servidor de correo electrónico.
	 */
	String MAIL_SMTP_PORT_DEFAULT_VALUE = "25";

	/**
	 * Juego de caracteres por defecto de los correos electrónicos.
	 */
	String MAIL_MESSAGE_CHARSET_DEFAULT_VALUE = "UTF-8";

	/**
	 * Formato por defecto del cuerpo de los correos electrónicos.
	 */
	String MAIL_MESSAGE_BODY_TYPE_DEFAULT_VALUE = "html";
}
