/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.utilities.mail.SmtpAuthenticator.java.</p>
 * <b>Descripción:</b><p> Clase empleada para la autenticación mediante usuario / password con el servidor de correo.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.utilities.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Clase empleada para la autenticación mediante usuario / password con el
 * servidor de correo.
 * <p>
 * Clase SmtpAuthenticator.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class SmtpAuthenticator extends Authenticator {

	/**
	 * Credenciales de autenticacion con el servidor de correo.
	 */
	private PasswordAuthentication passwordAuthentication;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param user
	 *            Nombre de usuario
	 * @param password
	 *            Password
	 */
	public SmtpAuthenticator(String user, String password) {
		passwordAuthentication = new PasswordAuthentication(user, password);
	}

	/**
	 * Devuelve las credenciales de autenticacion con el servidor de correo.
	 *
	 * @return credenciales de autenticacion con el servidor de correo.
	 */
	public PasswordAuthentication getPasswordAuthentication() {
		return passwordAuthentication;
	}
}
