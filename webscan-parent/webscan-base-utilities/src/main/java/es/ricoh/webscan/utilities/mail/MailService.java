/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.utilities.mail.MailService.java.</p>
 * <b>Descripción:</b><p> Clase de utilidades mediante la que es posible 
 * realizar el envío de correos electrónicos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.utilities.mail;

import java.util.Iterator;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.utilities.WebscanUtilitiesException;

/**
 * Clase de utilidades mediante la que es posible realizar el envío de correos
 * electrónicos.
 * <p>
 * Clase MailService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class MailService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

	/**
	 * Parámetros de configuración del sistema para el envío de correos
	 * electrónicos.
	 */
	private Properties confParameters = new Properties();

	/**
	 * Constructor sin argumentos.
	 */
	public MailService() {
		super();
	}

	/**
	 * Realiza el envío de un correo electrónico.
	 *
	 * @param mailMessage
	 *            Mensaje o correo electrónico.
	 * @throws WebscanUtilitiesException
	 *             Si ocurre algún error al enviar el correo electrónico.
	 */
	public void sendMail(final MailMessage mailMessage) throws WebscanUtilitiesException {
		Message msg;
		Session session;
		String sender;

		sender = mailMessage.getSender();

		if (sender == null || sender.isEmpty()) {
			sender = confParameters.getProperty(MailConstants.MAIL_MESSAGE_SENDER_PROP_NAME);
		}

		checkMailParameters(sender, mailMessage);

		try {
			session = getMailSession();

			msg = buildMailMessage(sender, mailMessage, session);

			Transport.send(msg);
		} catch (AddressException e) {
			LOGGER.error(e.getMessage(), e);
			throw new WebscanUtilitiesException(e.getMessage(), e);
		} catch (MessagingException e) {
			LOGGER.error(e.getMessage(), e);
			throw new WebscanUtilitiesException(e.getMessage(), e);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new WebscanUtilitiesException(e.getMessage(), e);
		}
	}

	/**
	 * Comprueba si el mensaje de correo electrónico posee los campos mínimos
	 * requeridos para su envío.
	 *
	 * @param sender
	 *            emisor del mensaje.
	 * @param mailMessage
	 *            correo electrónico.
	 * @throws WebscanUtilitiesException
	 *             si se detecta algún error.
	 */
	private void checkMailParameters(final String sender, final MailMessage mailMessage) throws WebscanUtilitiesException {
		Boolean senderOk;
		Boolean toRecipientsOk;
		Boolean bodyMessageOk;
		Boolean msgOk;
		String excMsg;

		senderOk = sender != null && !sender.isEmpty();
		toRecipientsOk = mailMessage.getToRecipients() != null && !mailMessage.getToRecipients().isEmpty();
		bodyMessageOk = mailMessage.getBodyMessage() != null && !mailMessage.getBodyMessage().isEmpty();

		msgOk = senderOk && toRecipientsOk && bodyMessageOk;

		if (!msgOk) {
			excMsg = "Los campos DE, PARA y CUERPO del correo electrónico no pueden ser vacíos o nulos.";
			LOGGER.error(excMsg);
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_998, excMsg);
		}
	}

	/**
	 * Obtiene una nueva sesión para el envío de correos electrónicos.
	 *
	 * @return una nueva sesión para el envío de correos electrónicos.
	 */
	private Session getMailSession() {
		Properties props;
		String pValue;
		Session res;
		props = getMailProperties();

		pValue = confParameters.getProperty(MailConstants.MAIL_SMTP_AUTH_PROP_NAME);
		if (pValue != null && pValue.equalsIgnoreCase(Boolean.TRUE.toString())) {
			res = Session.getInstance(props, new SmtpAuthenticator(confParameters.getProperty(MailConstants.MAIL_SMTP_USER_PROP_NAME), confParameters.getProperty(MailConstants.MAIL_SMTP_PASSWORD_PROP_NAME)));
		} else {
			res = Session.getInstance(props);
		}

		return res;
	}

	/**
	 * Construye el mensaje a enviar (API Mail).
	 *
	 * @param sender
	 *            emisor del mensaje.
	 * @param mailMessage
	 *            correo electrónico.
	 * @param session
	 *            Sesión del API Mail configurada e inicializada.
	 * @return el mensaje a enviar (API Mail).
	 * @throws MessagingException
	 *             si se produce algún error al construir el mensaje.
	 * @throws AddressException
	 *             sis e produce algún error al tratar las direcciones de correo
	 *             del mensaje.
	 */
	private Message buildMailMessage(final String sender, final MailMessage mailMessage, final Session session) throws MessagingException, AddressException {
		Address[ ] recipientAddress;
		final Message res = new MimeMessage(session);
		final Multipart mp = new MimeMultipart();
		final MimeBodyPart mbp = new MimeBodyPart();

		mbp.setText(mailMessage.getBodyMessage(), confParameters.getProperty(MailConstants.MAIL_MESSAGE_CHARSET_PROP_NAME, MailConstants.MAIL_MESSAGE_CHARSET_DEFAULT_VALUE), confParameters.getProperty(MailConstants.MAIL_MESSAGE_BODY_TYPE_PROP_NAME, MailConstants.MAIL_MESSAGE_BODY_TYPE_DEFAULT_VALUE));

		mp.addBodyPart(mbp);

		// Campo FROM
		res.setFrom(new InternetAddress(sender));

		// Campo SUBJECT
		if (mailMessage.getSubject() != null) {
			res.setSubject(mailMessage.getSubject());
		} else {
			res.setSubject("");
		}

		// Campo TO
		recipientAddress = getMailAddresses(mailMessage.getToRecipients());
		if (recipientAddress != null) {
			res.setRecipients(Message.RecipientType.TO, recipientAddress);
		}

		// Campo CC
		recipientAddress = getMailAddresses(mailMessage.getCcRecipients());
		if (recipientAddress != null) {
			res.setRecipients(Message.RecipientType.CC, recipientAddress);
		}

		// Campo CCO
		recipientAddress = getMailAddresses(mailMessage.getBccRecipients());
		if (recipientAddress != null) {
			res.setRecipients(Message.RecipientType.BCC, recipientAddress);
		}

		// Adjuntos
		if (mailMessage.getAttachments() != null && !mailMessage.getAttachments().isEmpty()) {
			for (final Iterator<MimeBodyPart> it = mailMessage.getAttachments().iterator(); it.hasNext();) {
				mp.addBodyPart(it.next());
			}
		}

		res.setContent(mp);

		return res;
	}

	/**
	 * Sustituye el carácter ';' por ',' como separador de direcciones de correo
	 * electrónico.
	 *
	 * @param recipient
	 *            Cadena que incluye direcciones de correo electrónico.
	 * @return Una cadena en la que se ha sustituido el carácter ';' por ','
	 *         como separador de direcciones de correo electrónico. Si el
	 *         parámetro de entrada es null, retorna null.
	 */
	private static String normalizeMessageRecipients(String recipient) {
		String res = null;
		String aux;

		if (recipient != null) {
			aux = recipient.replace(';', ',');
			if (aux.endsWith(",")) {
				res = aux.substring(0, aux.length() - 1);
			}
		}

		return res;
	}

	/**
	 * Obtiene una lista de direcciones de correo electrónico a partir de una
	 * cadena separadas por comas.
	 *
	 * @param recipient
	 *            cadena que incluye direcciones de correo electrónico separadas
	 *            por el caracter ','.
	 * @return Lista de direcciones de correo electrónico.
	 * @throws AddressException
	 *             Si se produce algún error al construir las direcciones de
	 *             correo electrónico.
	 */
	private static Address[ ] getMailAddresses(final String recipient) throws AddressException {
		Address[ ] res = null;
		final String auxRecipient = normalizeMessageRecipients(recipient);
		String[ ] its;

		if (auxRecipient != null && !auxRecipient.isEmpty()) {
			its = auxRecipient.split(",");

			res = new Address[its.length];
			for (int i = 0; i < its.length; ++i) {
				res[i] = new InternetAddress(its[i]);
			}
		}

		return res;
	}

	/**
	 * Obtiene los parámetros de configuración del API Mail requeridos para el
	 * envío de correos electrónicos.
	 *
	 * @return Parámetros de configuración del API Mail requeridos para el envío
	 *         de correos electrónicos.
	 */
	private Properties getMailProperties() {
		Properties res;
		String pValue;

		res = new Properties();

		// Nombre del host de correo
		res.put("mail.smtp.host", confParameters.getProperty(MailConstants.MAIL_SMTP_HOST_PROP_NAME));

		// Puerto del servidor de correo para envio de correos
		pValue = confParameters.getProperty(MailConstants.MAIL_SMTP_PORT_PROP_NAME, MailConstants.MAIL_SMTP_PORT_DEFAULT_VALUE);
		res.put("mail.smtp.port", pValue);

		// Si requiere o no usuario y password para conectarse.
		pValue = confParameters.getProperty(MailConstants.MAIL_SMTP_AUTH_PROP_NAME);
		if (pValue != null && pValue.equalsIgnoreCase(Boolean.TRUE.toString())) {
			res.put("mail.smtp.auth", Boolean.TRUE.toString());
		} else {
			res.put("mail.smtp.auth", Boolean.FALSE.toString());
		}

		pValue = confParameters.getProperty(MailConstants.MAIL_SMTP_SECURE_PROP_NAME);

		if (pValue != null && !MailConstants.MAIL_SMTP_SECURE_NONE_VALUE.equalsIgnoreCase(pValue)) {

			if (MailConstants.MAIL_SMTP_SECURE_SSL_VALUE.equalsIgnoreCase(pValue)) {
				res.put("mail.smtp.ssl.enable", Boolean.TRUE.toString());
				res.put("mail.smtp.socketFactory.port", confParameters.getProperty(MailConstants.MAIL_SMTP_PORT_PROP_NAME, MailConstants.MAIL_SMTP_PORT_DEFAULT_VALUE));
				res.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				res.put("mail.smtp.socketFactory.fallback", Boolean.FALSE.toString());

				res.setProperty("mail.smtp.starttls.enable", Boolean.FALSE.toString());
			} else if (MailConstants.MAIL_SMTP_SECURE_TLS_VALUE.equalsIgnoreCase(pValue)) {
				res.setProperty("mail.smtp.starttls.enable", Boolean.TRUE.toString());
				res.put("mail.smtp.ssl.enable", Boolean.FALSE.toString());
			}

		}

		pValue = confParameters.getProperty(MailConstants.MAIL_SMTP_DEBUG_PROP_NAME);
		if (pValue != null && pValue.equalsIgnoreCase(Boolean.TRUE.toString())) {
			res.put("mail.debug", Boolean.TRUE.toString());
		} else {
			res.put("mail.debug", Boolean.FALSE.toString());
		}

		return res;
	}

	/**
	 * Parámetros de configuración del sistema para el envío de correos
	 * electrónicos.
	 *
	 * @return Parámetros de configuración del sistema para el envío de correos
	 *         electrónicos.
	 */
	public Properties getConfParameters() {
		return confParameters;
	}

	/**
	 * Establece el conjunto de parámetros de configuración del sistema para el
	 * envío de correos electrónicos.
	 *
	 * @param aConfParameters
	 *            conjunto de parámetros de configuración para el envío de
	 *            correos electrónicos.
	 */
	public void setConfParameters(final Properties aConfParameters) {
		if (aConfParameters != null && !aConfParameters.isEmpty()) {
			this.confParameters.clear();
			this.confParameters.putAll(aConfParameters);
		}
	}

}
