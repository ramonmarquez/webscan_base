/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.utilities.CryptoUtilities.java.</p>
* <b>Descripción:</b><p> Utilidades criptográficas.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.utilities;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.UUID;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import es.ricoh.webscan.ValidationUtilities;
import es.ricoh.webscan.WebscanConstants;
import es.ricoh.webscan.WebscanException;

/**
 * Utilidades criptográficas.
 * <p>
 * Clase CryptoUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class CryptoUtilities {

	/**
	 * Constructor method for the class CryptoUtilities.java.
	 */
	private CryptoUtilities() {
	}

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	/**
	 * Genera el hash de un contenido dado.
	 * 
	 * @param algorithm
	 *            algoritmo de hash.
	 * @param data
	 *            contenido
	 * @return hash del contenido en el algoritmo especificado.
	 * @throws WebscanUtilitiesException
	 *             Informa de una exception producida al generar el hash de un
	 *             contenido.
	 */
	public static byte[ ] digest(String algorithm, byte[ ] data) throws WebscanUtilitiesException {
		byte[ ] res = null;

		try {
			ValidationUtilities.checkEmptyObject(algorithm, "No se ha especificado el algoritmo de hash.");
			ValidationUtilities.checkEmptyObject(data, "No se ha especificado el contenido sobre el que aplicar el algoritmo de hash.");

			MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			byte[ ] tmp = new byte[WebscanConstants.INT_1024];
			int length = 0;
			while ((length = bais.read(tmp, 0, tmp.length)) >= 0) {
				messageDigest.update(tmp, 0, length);
			}
			res = messageDigest.digest();
		} catch (WebscanException e) {
			throw new WebscanUtilitiesException(e.getCode(), "Error al gnenerar el hash " + algorithm + " de un contenido: " + e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_304, "Error al gnenerar el hash " + algorithm + " de un contenido: " + e.getMessage(), e);
		}

		return res;
	}

	/**
	 * Genera un UID random
	 * 
	 * @return cadena de caracteres aleatoria generado por UUID
	 */
	public static String getUID() {
		return UUID.randomUUID().toString();
	}
}
