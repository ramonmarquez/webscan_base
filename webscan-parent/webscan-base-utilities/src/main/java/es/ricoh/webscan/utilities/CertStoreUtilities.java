/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.utilities.CertStoreUtilities.java.</p>
* <b>Descripción:</b><p> Utilidades sobre almacenes de certificados..</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.utilities;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

/**
 * Utilidades sobre almacenes de certificados.
 * <p>
 * Clase CertStoreUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class CertStoreUtilities {

	/**
	 * Constructor method for the class CertStoreUtilities.java.
	 */
	private CertStoreUtilities() {
	}

	/**
	 * Tipo de almacén de certificados PKCS#12.
	 */
	public static final String PKCS12 = "PKCS12";

	/**
	 * Tipo de almacén de certificados JCEKS.
	 */
	public static final String JCEKS = "JCEKS";

	/**
	 * Tipo de almacén de certificados JKS.
	 */
	public static final String JKS = "JKS";

	/**
	 * Recupera un almacén de certificados localizado mediente el sistema de
	 * archivos.
	 * 
	 * @param path
	 *            Ruta del sistema de archivos donde se encuentra localizado el
	 *            almacén.
	 * @param password
	 *            Password del almacén de certificados.
	 * @param type
	 *            Tipo de almacén de certificados.
	 * @return el almacén de certificados solicitado.
	 * @throws WebscanUtilitiesException
	 *             Si se produce algún error en la lectura del almacén de
	 *             certificados.
	 */
	public static KeyStore loadKeystore(String path, String password, String type) throws WebscanUtilitiesException {
		InputStream bais = null;
		KeyStore ks = null;
		try {
			ks = KeyStore.getInstance(type);
			bais = new FileInputStream(path);
			ks.load(bais, password.toCharArray());
		} catch (KeyStoreException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_300, e);
		} catch (NoSuchAlgorithmException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_301, e);
		} catch (CertificateException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_302, e);
		} catch (IOException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_303, e);
		} finally {
			FileSystemUtilities.closeInputStream(bais);
		}
		return ks;
	}

	/**
	 * Método que recupera el certificado con un alias dado de un almacén de
	 * certificados.
	 * 
	 * @param alias
	 *            Alias del certificado.
	 * @param path
	 *            Ruta del sistema de archivos donde se encuentra localizado el
	 *            almacén.
	 * @param password
	 *            Password del almacén de certificados.
	 * @param type
	 *            Tipo de almacén de certificados.
	 * @return el certificado registrado en el almacén con un alias determinado.
	 *         En caso de no encontrarse, retorna null.
	 * @throws WebscanUtilitiesException
	 *             Si se produce algún error en la lectura del almacen de
	 *             certificados.
	 */
	public static X509Certificate getCertificate(String alias, String path, String password, String type) throws WebscanUtilitiesException {
		Boolean found;
		ByteArrayInputStream bais = null;
		X509Certificate res = null;
		String certAlias;

		try {
			KeyStore ks = KeyStore.getInstance(type);
			bais = new ByteArrayInputStream(FileSystemUtilities.readDataFromInputStream(new FileInputStream(path)));
			ks.load(bais, password.toCharArray());

			found = Boolean.FALSE;
			Enumeration<String> aliases = ks.aliases();
			while (!found && aliases.hasMoreElements()) {
				certAlias = aliases.nextElement();
				if (ks.isCertificateEntry(certAlias) && certAlias.equals(alias)) {
					res = (X509Certificate) ks.getCertificate(certAlias);
					found = Boolean.TRUE;
				}

			}
		} catch (KeyStoreException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_300, e);
		} catch (NoSuchAlgorithmException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_301, e);
		} catch (CertificateException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_302, e);
		} catch (IOException e) {
			throw new WebscanUtilitiesException(WebscanUtilitiesException.CODE_303, e);
		} finally {
			FileSystemUtilities.closeInputStream(bais);
		}
		return res;
	}
}
