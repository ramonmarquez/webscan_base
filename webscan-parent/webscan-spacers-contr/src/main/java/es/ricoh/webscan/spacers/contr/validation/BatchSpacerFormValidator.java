/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.spacers.contr.validation.BatchSpacerFormValidator.java.</p>
 * <b>Descripción:</b><p> Comprueba que la información aportada por el personal de digitalización para
 * la generación de carátulas de lote de documentos es válida y completa.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH España IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.spacers.contr.validation;

import java.util.Iterator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.ricoh.webscan.spacers.contr.controller.vo.BatchSpacerFormVO;
import es.ricoh.webscan.spacers.contr.controller.vo.SpacerMetadataVO;

/**
 * Comprueba que la información aportada por el personal de digitalización para
 * la generación de carátulas de lote de documentos es válida y completa.
 * <p>
 * Clase BatchSpacerFormValidator.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class BatchSpacerFormValidator implements Validator {

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return BatchSpacerFormVO.class.isAssignableFrom(clazz);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 *      org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		Boolean found = Boolean.FALSE;
		BatchSpacerFormVO formObject = (BatchSpacerFormVO) target;
		SpacerMetadataVO metadata;

		if (formObject != null) {
			// Campos requeridos: tipo código, perfil de digitalización,
			// definición de documentos.
			if (formObject.getSpacerType() == null) {
				errors.rejectValue("spacerType", "error.spacerType.required", "The type of spacer is requiered.");
			}

			if (formObject.getScanProfileId() == null && formObject.getScanProfileId() == -1) {
				errors.rejectValue("scanProfileId", "error.scanTemplate.required", "The scan profile is requiered.");
			}

			if (formObject.getDocSpecId() == null || formObject.getDocSpecId() == -1) {
				errors.rejectValue("docSpecId", "error.docSpec.required", "If the scan profile is setted, document specification is requiered.");
			}

			if (formObject.getMetadataCollection() == null || formObject.getMetadataCollection().isEmpty()) {
				errors.reject("error.metadataColecction.required", "The metadata collection is requiered.");
			} else {

				for (Iterator<SpacerMetadataVO> it = formObject.getMetadataCollection().iterator(); !found && it.hasNext();) {
					metadata = it.next();

					found = metadata.getValue() == null || metadata.getValue().isEmpty();
				}

				if (found) {
					errors.reject("error.metadataColecction.required", "The metadata collection is requiered.");
				}

			}

			// Validación del número de documentos de un lote
			if (formObject.getBatchDocSize() == null) {
				errors.rejectValue("batchDocSize", "error.batchDocSize.required", "Number of documents in batch is requiered.");
			} else {
				if (formObject.getBatchDocSize() < 1) {
					errors.rejectValue("batchDocSize", "error.generationValue.positive", "Number of documents in batch can't be less than 1.");
				}
			}
			// Validación del número total de páginas del lote
			if (formObject.getBatchPages() != null && formObject.getBatchPages() < 1) {
				errors.rejectValue("batchPages", "error.generationValue.positive", "Number of pages of the batch can't be less than 1.");
			}

			// Número de separadores reutilizables
			if (formObject.getNumberOfReusableDocSpacers() != null && formObject.getNumberOfReusableDocSpacers() < 1) {
				errors.rejectValue("numberOfReusableDocSpacers", "error.generationValue.positive", "Number of reusable document spacers can't be less than 1.");
			}
		}

	}

}
