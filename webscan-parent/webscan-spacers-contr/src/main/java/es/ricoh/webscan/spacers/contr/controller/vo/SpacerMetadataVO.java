/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.contr.controller.vo.MetadataVO.java.</p>
* <b>Descripción:</b><p> Metadato referenciado en las carátulas de lote de documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.contr.controller.vo;

import java.io.Serializable;

/**
 * Metadato referenciado en las carátulas de lote de documentos.
 * <p>
 * Clase MetadataVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacerMetadataVO implements Serializable {

	/**
	 * Attribute that represents .
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la especificación de un metadato referenciado en una
	 * carátula.
	 */
	private Long specMetadataId;

	/**
	 * Denominación del metadato referenciado en una carátula.
	 */
	private String name;

	/**
	 * Descripción del metadato referenciado en una carátula.
	 */
	private String description;

	/**
	 * Valor del metadato referenciado en una carátula.
	 */
	private String value;

	/**
	 * Valor por defecto del metadato referenciado en una carátula.
	 */
	private String defaultValue;

	/**
	 * Lista de valores que pueden ser asignados a los metadatos asociados a
	 * esta definición (notación JSON).
	 */
	private String sourceValues;

	/**
	 * Constructor sin argumentos.
	 */
	public SpacerMetadataVO() {
		super();
	}

	/**
	 * Obtiene el identificador de la especificación de un metadato referenciado
	 * en una carátula.
	 * 
	 * @return el identificador de la especificación de un metadato referenciado
	 *         en una carátula.
	 */
	public Long getSpecMetadataId() {
		return specMetadataId;
	}

	/**
	 * Establece el identificador de la especificación de un metadato
	 * referenciado en una carátula.
	 * 
	 * @param aSpecMetadataId
	 *            identificador de la especificación de un metadato referenciado
	 *            en una carátula.
	 */
	public void setSpecMetadataId(Long aSpecMetadataId) {
		this.specMetadataId = aSpecMetadataId;
	}

	/**
	 * Obtiene la denominación del metadato referenciado en una carátula.
	 * 
	 * @return la denominación del metadato referenciado en una carátula.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación del metadato referenciado
	 * en una carátula.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación del metadato referenciado en
	 *            una carátula.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene la descripción del metadato referenciado en una carátula.
	 * 
	 * @return la descripción del metadato referenciado en una carátula.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para la descripción del metadato referenciado en
	 * una carátula.
	 * 
	 * @param aDescription
	 *            nuevo valor para la descripción del metadato referenciado en
	 *            una carátula.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene el valor del metadato referenciado en una carátula.
	 * 
	 * @return el valor del metadato referenciado en una carátula.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Establece un nuevo valor para el metadato referenciado en una carátula.
	 * 
	 * @param aValue
	 *            nuevo valor para el metadato referenciado en una carátula.
	 */
	public void setValue(String aValue) {
		this.value = aValue;
	}

	/**
	 * Obtiene el valor por defecto de los metadatos asociados a esta
	 * definición.
	 * 
	 * @return el valor por defecto de los metadatos asociados a esta
	 *         definición.
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Establece un nuevo valor por defecto de los metadatos asociados a esta
	 * definición.
	 * 
	 * @param aDefaultValue
	 *            nuevo valor por defecto de los metadatos asociados a esta
	 *            definición.
	 */
	public void setDefaultValue(String aDefaultValue) {
		this.defaultValue = aDefaultValue;
	}

	/**
	 * Obtiene la lista de valores que pueden ser asignados a los metadatos
	 * asociados a esta definición.
	 * 
	 * @return la lista de valores que pueden ser asignados a los metadatos
	 *         asociados a esta definición.
	 */
	public String getSourceValues() {
		return sourceValues;
	}

	/**
	 * Establece una nueva lista de valores que pueden ser asignados a los
	 * metadatos asociados a esta definición.
	 * 
	 * @param anySourceValues
	 *            nueva lista de valores que pueden ser asignados a los
	 *            metadatos asociados a esta definición.
	 */
	public void setSourceValues(String anySourceValues) {
		this.sourceValues = anySourceValues;
	}

}
