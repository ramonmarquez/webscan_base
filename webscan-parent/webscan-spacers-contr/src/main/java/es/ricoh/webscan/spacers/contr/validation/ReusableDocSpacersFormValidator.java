/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.spacers.contr.validation.ReusableDocSpacersForm.java.</p>
 * <b>Descripción:</b><p> Comprueba que la información aportada por el personal de digitalización para
 * la generación de separadores reutilizables de documentos es válida y
 * completa.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
 * @author RICOH España IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.spacers.contr.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.ricoh.webscan.spacers.contr.controller.vo.ReusableDocSpacersFormVO;

/**
 * Comprueba que la información aportada por el personal de digitalización para
 * la generación de separadores reutilizables de documentos es válida y
 * completa.
 * <p>
 * Clase ReusableDocSpacersForm.
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ReusableDocSpacersFormValidator implements Validator {

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return ReusableDocSpacersFormVO.class.isAssignableFrom(clazz);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 *      org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		ReusableDocSpacersFormVO formObject = (ReusableDocSpacersFormVO) target;

		if (formObject != null) {
			// Campos requeridos: tipo código y número de separadores (mayor que
			// 0).
			if (formObject.getSpacerType() == null) {
				errors.rejectValue("reusableDocSpacersFormData.spacerType", "error.spacerType.required", "The type of spacer is requiered.");
			}

			if (formObject.getNumberOfReusableDocSpacers() == null || formObject.getNumberOfReusableDocSpacers() < 1) {
				errors.rejectValue("reusableDocSpacersFormData.numberOfReusableDocSpacers", "error.numberOfReusableDocSpacers.positive", "Number of reusable document spacers can't be less than 1.");
			}
		}
	}

}
