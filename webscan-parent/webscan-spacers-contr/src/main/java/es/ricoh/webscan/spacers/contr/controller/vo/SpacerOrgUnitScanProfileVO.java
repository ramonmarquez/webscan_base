/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.contr.controller.vo.SpacerOrgUnitScanProfileVO.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que representa un perfil de
* digitalización que puede ser establecido por un usuario al solicitar la
* generación de una carátula desde la interfaz gráfica de usuario del sistema.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.contr.controller.vo;

import java.io.Serializable;

import es.ricoh.webscan.base.controller.vo.OrganizationUnitVO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;

/**
 * Objeto de la capa de presentación que representa un perfil de digitalización
 * que puede ser establecido por un usuario al solicitar la generación de una
 * carátula desde la interfaz gráfica de usuario del sistema.
 * <p>
 * Clase SpacerOrgUnitScanProfileVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacerOrgUnitScanProfileVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Unidad organizativa.
	 */
	private OrganizationUnitVO organizationUnit;

	/**
	 * Plantilla de digitalización.
	 */
	private ScanProfileDTO scanProfile;

	/**
	 * Perfil de digitalización configurado para la plantilla en la unidad
	 * organizativa.
	 */
	private ScanProfileOrgUnitDTO scanProfileOrgUnit;

	/**
	 * Constructor sin argumentos.
	 */
	public SpacerOrgUnitScanProfileVO() {
		super();
	}

	/**
	 * Gets the value of the attribute {@link #organizationUnit}.
	 * 
	 * @return the value of the attribute {@link #organizationUnit}.
	 */
	public OrganizationUnitVO getOrganizationUnit() {
		return organizationUnit;
	}

	/**
	 * Sets the value of the attribute {@link #organizationUnit}.
	 * 
	 * @param anOrganizationUnit
	 *            The value for the attribute {@link #organizationUnit}.
	 */
	public void setOrganizationUnit(OrganizationUnitVO anOrganizationUnit) {
		this.organizationUnit = anOrganizationUnit;
	}

	/**
	 * Gets the value of the attribute {@link #scanProfile}.
	 * 
	 * @return the value of the attribute {@link #scanProfile}.
	 */
	public ScanProfileDTO getScanProfile() {
		return scanProfile;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfile}.
	 * 
	 * @param aScanProfile
	 *            The value for the attribute {@link #scanProfile}.
	 */
	public void setScanProfile(ScanProfileDTO aScanProfile) {
		this.scanProfile = aScanProfile;
	}

	/**
	 * Gets the value of the attribute {@link #scanProfileOrgUnit}.
	 * 
	 * @return the value of the attribute {@link #scanProfileOrgUnit}.
	 */
	public ScanProfileOrgUnitDTO getScanProfileOrgUnit() {
		return scanProfileOrgUnit;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfileOrgUnit}.
	 * 
	 * @param aScanProfileOrgUnit
	 *            The value for the attribute {@link #scanProfileOrgUnit}.
	 */
	public void setScanProfileOrgUnit(ScanProfileOrgUnitDTO aScanProfileOrgUnit) {
		this.scanProfileOrgUnit = aScanProfileOrgUnit;
	}

}
