/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.contr.controller.SpacersController.java.</p>
* <b>Descripción:</b><p> Controlador que implementa las solicitudes de generación de carátulas y
* separadores reutilizables realizadas de la capa de presentación.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.contr.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.ricoh.webscan.spacers.business.Spacer;
import es.ricoh.webscan.spacers.business.SpacerDocRequest;
import es.ricoh.webscan.spacers.business.SpacerRequest;
import es.ricoh.webscan.spacers.business.SpacersException;
import es.ricoh.webscan.spacers.business.service.SpacersService;
import es.ricoh.webscan.spacers.contr.controller.vo.BatchSpacerFormVO;
import es.ricoh.webscan.spacers.contr.controller.vo.ReusableDocSpacersFormVO;
import es.ricoh.webscan.spacers.contr.controller.vo.SpacerMetadataVO;
import es.ricoh.webscan.spacers.contr.controller.vo.SpacerOrgUnitScanProfileVO;
import es.ricoh.webscan.spacers.contr.validation.BatchSpacerFormValidator;
import es.ricoh.webscan.spacers.contr.validation.ReusableDocSpacersFormValidator;
import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.model.domain.DocSpacerType;
import es.ricoh.webscan.spacers.repository.SpacersDAOException;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.OrganizationUnitVO;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.UserOrgUnitScanProfiles;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;

/**
 * Controlador que implementa las solicitudes de generación de carátulas y
 * separadores reutilizables realizadas de la capa de presentación.
 * <p>
 * Clase SpacersController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
@Controller
public class SpacersController {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SpacersController.class);

	/**
	 * Servicio encargado de la generación de carátulas y separadores
	 * reutilizables.
	 */
	@Autowired
	private SpacersService spacersService;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	@Autowired
	private MessageSource messages;

	/**
	 * Validador del formulario de generación de carátulas.
	 */
	@Autowired
	private BatchSpacerFormValidator batchSpacerFormValidator;

	/**
	 * Validador del formulario de generación de separadores reutilizables.
	 */
	@Autowired
	private ReusableDocSpacersFormValidator reusableDocSpacersFormValidator;

	/**
	 * Método que asocia el formulario de petición de recuperación de password
	 * con su validador.
	 * 
	 * @param binder
	 *            Objeto responsable de enlazar el formulario con su validador.
	 */
	@InitBinder(value = "batchSpacerFormData")
	protected void initBatchSpacerGenBinder(WebDataBinder binder) {
		binder.addValidators(batchSpacerFormValidator);
	}

	/**
	 * Método que asocia el formulario de recuperación de password con su
	 * validador.
	 * 
	 * @param binder
	 *            Objeto responsable de enlazar el formulario con su validador.
	 */
	@InitBinder(value = "reusableDocSpacersFormData")
	protected void initReusableDocSpacersGenBinder(WebDataBinder binder) {
		binder.addValidators(reusableDocSpacersFormValidator);
	}

	/**
	 * Controlador responsable de presentar la vista principal de la utilidad de
	 * generación de carátulas y separadores reutilizables.
	 * 
	 * @param request
	 *            Petición web.
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @return Nombre de la vista donde se muestra la operación solicitada.
	 */
	@RequestMapping(value = "/spacers/spacers", method = { RequestMethod.GET })
	public String spacers(Model model, final HttpServletRequest request) {
		UserInSession userInSession = null;
		List<SpacerOrgUnitScanProfileVO> userOrgUnitScanProfiles = new ArrayList<SpacerOrgUnitScanProfileVO>();
		try {
			// obtenemos userInSession para recuperar las unidades organizativas
			// y perfiles
			userInSession = BaseControllerUtils.getUserInSession(request);
			// obtenemos los ScanProfileOrgUnitDocSpecConf según Perfile y
			// OrgUnit eliminando aquellos que no tengan configuración de
			// especificación de documentos de carátulas
			userOrgUnitScanProfiles = getListUserOrgUnitScanProfiles(userInSession.getOrgUnitScanProfiles());

		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACER-CONTR] Error recuperar la vista inicial de la generacion de caratulas y doc: " + e.getMessage(), e);
			}
			BaseControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.TRUE);
		}
		// creamos los objetos bases
		BatchSpacerFormVO batchSpacerFormData = new BatchSpacerFormVO();
		ReusableDocSpacersFormVO reusableDocSpacersFormData = new ReusableDocSpacersFormVO();
		// preparamos la info a mostrar
		model.addAttribute("batchSpacerFormData", batchSpacerFormData);
		model.addAttribute("reusableDocSpacersFormData", reusableDocSpacersFormData);
		model.addAttribute("batchSpacerType", BatchSpacerType.values());
		model.addAttribute("docSpacerType", DocSpacerType.values());
		model.addAttribute("userOrgUnitScanProfiles", userOrgUnitScanProfiles);
		return "spacers/spacers";
	}

	/**
	 * Obtiene el listado de unidades organizativas relacionadas con perfiles de
	 * escaneo, que tengan al menos una configuración de especificación de
	 * documentos con metadatos asociados para añadir a carátula.
	 * 
	 * @param userOrgUnitScanProfiles
	 *            Listado de unidades organizativas relacionadas con perfiles de
	 *            escaneo del usuario.
	 * @return Relación de perfiles de digitalización que posean configurada y
	 *         habilitada alguna definición de documentos con metadatos que
	 *         puedan ser incorporados a carátulas.
	 * @throws SpacersException
	 *             Exception producida al acceder a la información asociada para
	 *             la generación de separadores, o bien por error al acceder a
	 *             BBDD.
	 */
	private List<SpacerOrgUnitScanProfileVO> getListUserOrgUnitScanProfiles(List<UserOrgUnitScanProfiles> aUserOrgUnitScanProfiles) throws SpacersException {
		Boolean found = Boolean.FALSE;
		DocumentSpecificationDTO docSpec;
		List<SpacerOrgUnitScanProfileVO> res = new ArrayList<SpacerOrgUnitScanProfileVO>();
		List<ScanProfileDTO> listScanProfile = null;
		Long scanProfileId = null;
		Long orgUnitId = null;
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> mapDocSpecifications = null;
		OrganizationUnitVO orgUnit = null;
		ScanProfileOrgUnitDTO scanProfileOrgUnit;
		ScanProfileOrgUnitDocSpecDTO scanProfileOrgUnitDocSpec;
		SpacerOrgUnitScanProfileVO spacerOrgUnitScanProfile;

		LOG.debug("[WEBSCAN-SPACER-CONTR] Start getListUserOrgUnitScanProfiles");

		// recorremos cada perfil y unidades organizativas
		for (UserOrgUnitScanProfiles userOrgUnitScanProfiles: aUserOrgUnitScanProfiles) {
			orgUnit = userOrgUnitScanProfiles.getOrganizationUnit();
			listScanProfile = userOrgUnitScanProfiles.getScanProfiles();
			// recorremos por cada perfil y unidad organizativa para
			// comprobar que exista
			orgUnitId = orgUnit.getId();
			for (ScanProfileDTO scanProfile: listScanProfile) {
				if (scanProfile.getActive()) {
					scanProfileId = scanProfile.getId();
					if (scanProfileId != null && orgUnitId != null) {
						spacerOrgUnitScanProfile = null;
						scanProfileOrgUnit = spacersService.findScanProfileOrgUnit(scanProfile.getName(), orgUnit.getExternalId());
						mapDocSpecifications = spacersService.getDocSpecificationsByScanProfileidOrgUnitId(scanProfileId, orgUnitId);
						// recuperamos solo la definiciones
						found = Boolean.FALSE;
						for (Iterator<DocumentSpecificationDTO> it = mapDocSpecifications.keySet().iterator(); !found && it.hasNext();) {
							docSpec = it.next();
							scanProfileOrgUnitDocSpec = mapDocSpecifications.get(docSpec);
							/*
							  si la especificación no se encuentra activa, habilitada en el perfil de digitalización, o no posee metadatos que
							  puedan ser incorporados a carátulas, no se añade la  definición de documentos
							*/
							if (docSpec.getActive() && scanProfileOrgUnitDocSpec.getActive() && checkDocSpecSpacerMetadata(spacersService.getMetadataSpecByDocSpec(docSpec.getId()))) {
								spacerOrgUnitScanProfile = new SpacerOrgUnitScanProfileVO();
								spacerOrgUnitScanProfile.setOrganizationUnit(orgUnit);
								spacerOrgUnitScanProfile.setScanProfile(scanProfile);
								spacerOrgUnitScanProfile.setScanProfileOrgUnit(scanProfileOrgUnit);
								res.add(spacerOrgUnitScanProfile);
								found = Boolean.TRUE;
							}
						}
					}
				}
			} // fin de recorrido de los perfiles comprobados.
		}

		LOG.debug("[WEBSCAN-SPACER-CONTR] End getListUserOrgUnitScanProfiles");
		return res;
	}

	/**
	 * Obtenemos la especificacion de documentos según el scanProfile y
	 * Organization Unit.
	 * 
	 * @param scanProfileId
	 *            Perfil solicitado
	 * @param orgUnitId
	 *            Unidad organizativa asociada
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @param request
	 *            Petición http.
	 * @return vista con las definiciones
	 */
	@RequestMapping(value = "/spacers/getDocSpecifications", method = { RequestMethod.POST })
	public String getDocSpecificationByScanProfileidOrgUnitId(Model model, Long scanProfileId, Long orgUnitId, final HttpServletRequest request) {
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> mapDocSpecifications = new HashMap<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO>();
		List<DocumentSpecificationDTO> docSpecifications = new ArrayList<DocumentSpecificationDTO>();

		try {
			if (scanProfileId != null && orgUnitId != null) {
				mapDocSpecifications = spacersService.getDocSpecificationsByScanProfileidOrgUnitId(scanProfileId, orgUnitId);
				// recuperamos solo la definiciones
				for (DocumentSpecificationDTO docSpec: mapDocSpecifications.keySet()) {
					// si la especificación no posee metadatos que puedan ser
					// incorporados a carátulas, no se añade la definición de
					// documentos
					if (checkDocSpecSpacerMetadata(spacersService.getMetadataSpecByDocSpec(docSpec.getId()))) {
						docSpecifications.add(docSpec);
					}
				}
			}
		} catch (SpacersException e) {
			setSpacersExceptionInRequest(e, request, messages);
		}

		model.addAttribute("docSpecifications", docSpecifications);

		return "fragments/spacers/batchGeneration:: optionSpecification";
	}

	/**
	 * Comprueba si una definición de documentos posee metadatos que puedan ser
	 * incorporados a carátulas.
	 * 
	 * @param metaSpecs
	 *            Metadatos de una definición, incluidos los metadatos de sus
	 *            definiciones padre.
	 * @return En caso de que se encuentre un metadatos configurado para ser
	 *         incorporado a carátula, retorna true. En caso contrario, devuelve
	 *         false.
	 */
	private Boolean checkDocSpecSpacerMetadata(Map<String, List<MetadataSpecificationDTO>> metaSpecs) {
		Boolean res = Boolean.FALSE;
		List<MetadataSpecificationDTO> docSpecMetadataColl;
		MetadataSpecificationDTO metadataSpec;

		if (metaSpecs != null && !metaSpecs.isEmpty()) {
			for (Iterator<String> it = metaSpecs.keySet().iterator(); !res && it.hasNext();) {
				docSpecMetadataColl = metaSpecs.get(it.next());
				for (Iterator<MetadataSpecificationDTO> itDoc = docSpecMetadataColl.iterator(); !res && itDoc.hasNext();) {
					metadataSpec = itDoc.next();
					res = metadataSpec.getIncludeInBatchSeprator();
				}
			}

		}

		return res;
	}

	/**
	 * Obtiene todas las especificaciones de metadatos correspondiente a la
	 * especificacion de documentos.
	 * 
	 * @param model
	 *            parametro para devolver información a la vista
	 * @param docSpecId
	 *            identificador de la especificacion de documentos
	 * @param request
	 *            peticion http
	 * @return vista que muestra las especificaciones de metadatos asociados al
	 *         documento
	 */
	@RequestMapping(value = "/spacers/getMetaSpecs", method = { RequestMethod.POST })
	public String getMetadataCollection(Model model, Long docSpecId, final HttpServletRequest request) {
		List<Long> includedMetadaIds = new ArrayList<Long>();
		List<SpacerMetadataVO> metadataCollection = new ArrayList<SpacerMetadataVO>();
		Map<String, List<MetadataSpecificationDTO>> metaSpecs = new HashMap<String, List<MetadataSpecificationDTO>>();
		SpacerMetadataVO nuevo;

		try {
			metaSpecs = spacersService.getMetadataSpecByDocSpec(docSpecId);

		} catch (SpacersException e) {
			setSpacersExceptionInRequest(e, request, messages);
		}

		// preparamos los metadatos
		for (List<MetadataSpecificationDTO> listMeta: metaSpecs.values()) {
			// recoremos que no esté previamente ya incorporado
			for (MetadataSpecificationDTO vMetaSpec: listMeta) {

				// if (vMetaSpec.getIncludeInBatchSeprator() &&
				// !listMetadataCollection.contains(vMetaSpec)) {
				if (vMetaSpec.getIncludeInBatchSeprator() && !includedMetadaIds.contains(vMetaSpec.getId())) {
					// listMetadataCollection.add(vMetaSpec);
					nuevo = new SpacerMetadataVO();
					// lo hacemos por id ya que por nombre tenemos problemas con
					// los espacios
					nuevo.setSpecMetadataId(vMetaSpec.getId());
					nuevo.setName(vMetaSpec.getName());
					nuevo.setDescription(vMetaSpec.getDescription());
					nuevo.setValue(vMetaSpec.getDefaultValue());
					nuevo.setDefaultValue(vMetaSpec.getDefaultValue());
					nuevo.setSourceValues(vMetaSpec.getSourceValues());
					metadataCollection.add(nuevo);
					includedMetadaIds.add(vMetaSpec.getId());
				}
			}
		}

		model.addAttribute("metadataCollection", metadataCollection);
		return "fragments/spacers/batchGeneration:: metadatacollection";
	}

	/**
	 * Controlador responsable de generar una carátula de lote de documentos.
	 * 
	 * @param request
	 *            Petición web.
	 * @param response
	 *            respuesta web.
	 * @param objectForm
	 *            Objeto que recoge las entradas del formulario de generación.
	 * @param formDataResult
	 *            Objeto de spring que devuelve si ha habido algún error en la
	 *            validación de los parametros del formulario.
	 */
	@RequestMapping(value = "/spacers/generateBatchSpacer", method = RequestMethod.POST)
	public void performDocsBatchSpacer(Model model, @Valid @ModelAttribute("batchSpacerFormData") BatchSpacerFormVO objectForm, BindingResult formDataResult, final HttpServletRequest request, HttpServletResponse response) {
		SpacerRequest spacerRequest;
		Spacer spacer;
		UserInSession userInSession;

		try {
			if (formDataResult.hasErrors()) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-SPACERS] Se produjeron " + formDataResult.getErrorCount() + " errores en el formulario de generación de carátula de lote de documentos.");
				}
			} else {
				userInSession = BaseControllerUtils.getUserInSession(request);
				// Invocar servicio
				spacerRequest = transformBatchSpacerFormVOToSpacerRequest(objectForm, userInSession);
				spacer = spacersService.generateDocsBatchSpacer(spacerRequest, null, Boolean.FALSE, request.getLocale());
				// Preparar descarga contenido
				downloadSpacer(spacer, messages.getMessage("spacers.docsBatch.filename", null, "caratula.pdf", request.getLocale()), response);
			}
		} catch (SpacersDAOException e) {
			setSpacersDAOExceptionInRequest(e, request, messages);
		} catch (SpacersException e) {
			setSpacersExceptionInRequest(e, request, messages);
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACER-CONTR] Error estableciendo los separadores reutilizables generados en la respuesta: " + e.getMessage(), e);
			}
			BaseControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.TRUE);
		}
	}

	/**
	 * Transforma un objeto de capa de presentación BatchSpacerFormVO en una
	 * petición de generación de carátula de lote de documentos.
	 * 
	 * @param objectForm
	 *            objeto de capa de presentación BatchSpacerFormVO.
	 * @param userInSession
	 *            Usuario en sesión.
	 * @return petición de generación de carátula de lote de documentos.
	 * @throws SpacersException
	 *             Exception producida al generar la carátula de lote.
	 */
	private SpacerRequest transformBatchSpacerFormVOToSpacerRequest(BatchSpacerFormVO objectForm, UserInSession userInSession) throws SpacersException {
		DocumentSpecificationDTO docSpec;
		Map<String, List<MetadataSpecificationDTO>> metaSpecs = new HashMap<String, List<MetadataSpecificationDTO>>();
		OrganizationUnitDTO orgUnit;
		ScanProfileDTO scanProfile;
		SpacerRequest res = new SpacerRequest();

		res.setBatchDocSize(objectForm.getBatchDocSize());
		res.setBatchPages(objectForm.getBatchPages());
		res.setDepartment(objectForm.getDepartment());
		res.setDocSpacerType(objectForm.getDocSpacerType());

		docSpec = spacersService.findDocSpecification(objectForm.getDocSpecId());
		metaSpecs = spacersService.getMetadataSpecByDocSpec(docSpec.getId());
		res.setDocSpecName(docSpec.getName());

		res.setDocuments(fillSpacerDocs(objectForm.getMetadataCollection(), metaSpecs, objectForm.getBatchDocSize()));
		res.setNumberOfReusableDocSpacers(objectForm.getNumberOfReusableDocSpacers());
		res.setReqUserId(userInSession.getDocument());

		scanProfile = spacersService.findScanProfile(objectForm.getScanProfileId());
		res.setScanTemplateName(scanProfile.getName());

		res.setSpacerType(objectForm.getSpacerType());

		orgUnit = spacersService.findOrgUnit(objectForm.getScanProfileId());
		res.setUserGroupExternalId(orgUnit.getExternalId());

		return res;
	}

	/**
	 * Rellena la colección de documentos de la petición de la capa de
	 * servicios.
	 * 
	 * @param spacerMetadataCollection
	 *            colección de metadatos de la capa de presentación.
	 * @param metaSpecs
	 *            definiciones de metadatos del tipo documental seleccionado.
	 * @param numDocs
	 *            Número de documentos del lote identificado por la carátula.
	 * @return colección de documentos de la petición de la capa de servicios.
	 */
	private List<SpacerDocRequest> fillSpacerDocs(ArrayList<SpacerMetadataVO> spacerMetadataCollection, Map<String, List<MetadataSpecificationDTO>> metaSpecs, Integer numDocs) {
		Integer orderNumber;
		List<SpacerDocRequest> res = new ArrayList<SpacerDocRequest>();
		SpacerDocRequest doc;

		for (int i = 0; i < numDocs; i++) {
			orderNumber = i + 1;

			doc = new SpacerDocRequest();
			doc.setOrder(orderNumber);
			doc.setName("Documento " + orderNumber);
			doc.setMetadataCollection(fillMetadataCollection(spacerMetadataCollection, metaSpecs));

			res.add(doc);
		}

		return res;
	}

	/**
	 * Rellena la colección de metadatos de la petición de la capa de servicios.
	 * 
	 * @param spacerMetadataCollection
	 *            colección de metadatos de la capa de presentación.
	 * @param metaSpecs
	 *            definiciones de metadatos del tipo documental seleccionado.
	 * @return colección de metadatos de la petición de la capa de servicios.
	 */
	private Map<String, String> fillMetadataCollection(ArrayList<SpacerMetadataVO> spacerMetadataCollection, Map<String, List<MetadataSpecificationDTO>> metaSpecs) {
		Map<String, String> res = new HashMap<String, String>();
		MetadataSpecificationDTO vMetaSpec;

		if (spacerMetadataCollection != null && !spacerMetadataCollection.isEmpty()) {
			for (SpacerMetadataVO metadata: spacerMetadataCollection) {

				if (metadata.getValue() != null && !metadata.getValue().trim().isEmpty()) {
					vMetaSpec = searchMetadataSpecById(Long.parseLong(metadata.getName()), metaSpecs);
					if (vMetaSpec != null) {
						res.put(vMetaSpec.getName(), metadata.getValue());
					}
				}
			}
		}

		return res;
	}

	/**
	 * Busca una especificación de metadatos en una relación a partir de su
	 * identificador.
	 * 
	 * @param id
	 *            Identificador de definición de metadato.
	 * @param metaSpecs
	 *            Relación de metadatos de una definición de documentos.
	 * @return Si se encuentra, la definición de metadatos búscada. En caso
	 *         contrario, null.
	 */
	private MetadataSpecificationDTO searchMetadataSpecById(Long id, Map<String, List<MetadataSpecificationDTO>> metaSpecs) {
		Boolean found = Boolean.FALSE;
		List<MetadataSpecificationDTO> docSpecMetadataCol;
		MetadataSpecificationDTO res = null;
		MetadataSpecificationDTO vMetaSpec;

		for (Iterator<List<MetadataSpecificationDTO>> specDocsIt = metaSpecs.values().iterator(); !found && specDocsIt.hasNext();) {
			docSpecMetadataCol = specDocsIt.next();
			// recoremos que no esté previamente ya incorporado
			for (Iterator<MetadataSpecificationDTO> it = docSpecMetadataCol.iterator(); !found && it.hasNext();) {
				vMetaSpec = it.next();
				if (vMetaSpec.getId().equals(id)) {
					res = vMetaSpec;
					found = Boolean.TRUE;
				}
			}
		}

		return res;
	}

	/**
	 * Controlador responsable de generar uno o más separadores reutilizables de
	 * documentos.
	 * 
	 * @param request
	 *            Petición web.
	 * @param response
	 *            respuesta web.
	 * @param objectForm
	 *            Objeto que recoge las entradas del formulario de generación.
	 * @param formDataResult
	 *            Objeto de spring que devuelve si ha habido algún error en la
	 *            validación de los parametros del formulario.
	 */
	@RequestMapping(value = "/spacers/generateReusableDocSpacers", method = RequestMethod.POST)
	public void performReusableDocSpacers(@Valid @ModelAttribute("reusableDocSpacersFormData") ReusableDocSpacersFormVO objectForm, BindingResult formDataResult, final HttpServletRequest request, HttpServletResponse response) {
		Spacer spacer;
		UserInSession userInSession;

		try {
			if (formDataResult.hasErrors()) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-SPACERS] Se produjeron " + formDataResult.getErrorCount() + " errores en el formulario de generación de carátula de lote de documentos.");
				}
			} else {
				userInSession = BaseControllerUtils.getUserInSession(request);
				// Invocar servicio
				spacer = spacersService.generateReusableDocSpacers(objectForm.getSpacerType(), objectForm.getNumberOfReusableDocSpacers(), userInSession.getDocument(), null, request.getLocale());
				// Preparar descarga contenido
				downloadSpacer(spacer, messages.getMessage("spacers.reuseDocs.filename", null, "separadores reutilizables", request.getLocale()), response);
			}
		} catch (SpacersException e) {
			setSpacersExceptionInRequest(e, request, messages);
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACER-CONTR] Error estableciendo los separadores reutilizables generados en la respuesta: " + e.getMessage(), e);
			}
			BaseControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.TRUE);
		}
	}

	/**
	 * Método para responder a la petición de la descarga desde la capa de
	 * presentación de los separadores.
	 * 
	 * @param spacer
	 *            Entidad con la información del separador.
	 * @param fileName
	 *            String con el nombre del fichero.
	 * @param response
	 *            Entidad de respuesta http.
	 * @throws IOException
	 *             Exception producida al preparar la descarga de los
	 *             separadores.
	 */
	private void downloadSpacer(Spacer spacer, String fileName, HttpServletResponse response) throws IOException {

		// Se establecen las cabeceras de la respuesta
		response.setContentType("application/force-download");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + fileName + ".pdf\""));

		// Se establecen las cabeceras de la respuesta
		// response.setHeader("Content-Disposition", String.format("inline;
		// filename=\"" + fileName + ".pdf\""));
		response.setContentType(spacer.getMimeType());

		response.setContentLength(spacer.getContent().length);
		// Se incorpora el informe a la respuesta
		FileCopyUtils.copy(spacer.getContent(), response.getOutputStream());

		response.flushBuffer();
	}

	/**
	 * Obtiene y establece como atributo de una petición un mensaje de error
	 * asociado a una excepción SpacersDAOException.
	 * 
	 * @param e
	 *            Excepción SpacersDAOException
	 * @param request
	 *            Petición web.
	 * @param messageSource
	 *            Clase que agrupa los mensajes paramterizados en el sistema.
	 */
	private static void setSpacersDAOExceptionInRequest(SpacersDAOException e, HttpServletRequest request, MessageSource messageSource) {
		String errorPropName;
		String[ ] args = new String[1];

		errorPropName = "spacers.error.bbdd";
		args[0] = e.getMessage();

		BaseControllerUtils.addCommonMessage(errorPropName, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
	}

	/**
	 * Obtiene y establece como atributo de una petición un mensaje de error
	 * asociado a una excepción SpacersException.
	 * 
	 * @param e
	 *            Excepción SpacersException
	 * @param request
	 *            Petición web.
	 * @param messageSource
	 *            Clase que agrupa los mensajes paramterizados en el sistema.
	 */
	private static void setSpacersExceptionInRequest(SpacersException e, HttpServletRequest request, MessageSource messageSource) {
		String errorPropName;
		String[ ] args = new String[1];

		errorPropName = "spacers.error.common";
		args[0] = e.getMessage();

		BaseControllerUtils.addCommonMessage(errorPropName, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
	}
}
