/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.contr.controller.vo.BatchSpacerFormVO.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que agrupa la información aportada por el
* personal de digitalización para la generación de separadores reutilizables de
* documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.contr.controller.vo;

import java.io.Serializable;

import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;

/**
 * Objeto de la capa de presentación que agrupa la información aportada por el
 * personal de digitalización para la generación de separadores reutilizables de
 * documentos.
 * <p>
 * Clase ReusableDocSpacersFormVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ReusableDocSpacersFormVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tipo de código de los sepradores reutilizables.
	 */
	private BatchSpacerType spacerType;

	/**
	 * Número de separadores reutilizables a generar.
	 */
	private Integer numberOfReusableDocSpacers;

	/**
	 * Constructor sin argumentos.
	 */
	public ReusableDocSpacersFormVO() {
		super();
	}

	/**
	 * Obtiene el tipo de código de los sepradores reutilizables.
	 * 
	 * @return el tipo de código de los sepradores reutilizables.
	 */
	public BatchSpacerType getSpacerType() {
		return spacerType;
	}

	/**
	 * Establece un nuevo valor para el tipo de código de los sepradores
	 * reutilizables.
	 * 
	 * @param aSpacerType
	 *            nuevo valor para el tipo de código de los sepradores
	 *            reutilizables.
	 */
	public void setSpacerType(BatchSpacerType aSpacerType) {
		this.spacerType = aSpacerType;
	}

	/**
	 * Obtiene el número de separadores reutilizables a generar.
	 * 
	 * @return número de separadores reutilizables a generar.
	 */
	public Integer getNumberOfReusableDocSpacers() {
		return numberOfReusableDocSpacers;
	}

	/**
	 * Establece el número de separadores reutilizables a generar.
	 * 
	 * @param aNumberOfReusableDocSpacers
	 *            número de separadores reutilizables a generar.
	 */
	public void setNumberOfReusableDocSpacers(Integer aNumberOfReusableDocSpacers) {
		this.numberOfReusableDocSpacers = aNumberOfReusableDocSpacers;
	}

}
