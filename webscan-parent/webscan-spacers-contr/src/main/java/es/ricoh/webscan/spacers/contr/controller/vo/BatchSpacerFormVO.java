/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.contr.controller.vo.BatchSpacerFormVO.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que agrupa la información aportada por el
* personal de digitalización para la generación de carátulas de lote de
* documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.contr.controller.vo;

import java.io.Serializable;
import java.util.ArrayList;

import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.model.domain.DocSpacerType;

/**
 * Objeto de la capa de presentación que agrupa la información aportada por el
 * personal de digitalización para la generación de carátulas de lote de
 * documentos.
 * <p>
 * Clase BatchSpacerFormVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class BatchSpacerFormVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tipo de código de la carátula.
	 */
	private BatchSpacerType spacerType;

	/**
	 * Tipo de separador de documentos que deben emplearse en el lote.
	 */
	private DocSpacerType docSpacerType;

	/**
	 * Número de documentos que debe tener el lote.
	 */
	private Integer batchDocSize;

	/**
	 * Número total de páginas que debe tener el lote.
	 */
	private Integer batchPages;

	/**
	 * Identificador de la plantilla de digtialización informado para la
	 * generación de la carátula.
	 */
	private Long scanProfileId;

	/**
	 * Identificador de la definición de documentos informado para la generación
	 * de la carátula.
	 */
	private Long docSpecId;

	/**
	 * Identificador de la unidad orgánica a la que serán asociados los
	 * documentos del lote.
	 */
	private String department;

	/**
	 * Relación de metadatos informada por el personal de digitalización al
	 * solicitar la generación de una carátula.
	 */
	private ArrayList<SpacerMetadataVO> metadataCollection = new ArrayList<SpacerMetadataVO>();

	/**
	 * Número de separadores reutilizables a generar junto a la carátula.
	 */
	private Integer numberOfReusableDocSpacers;

	/**
	 * Constructor sin argumentos.
	 */
	public BatchSpacerFormVO() {
		super();
	}

	/**
	 * Obtiene el tipo de código de la carátula.
	 * 
	 * @return el tipo de código de la carátula.
	 */
	public BatchSpacerType getSpacerType() {
		return spacerType;
	}

	/**
	 * Establece un nuevo valor para el tipo de código de la carátula.
	 * 
	 * @param aSpacerType
	 *            nuevo valor para el tipo de código de la carátula.
	 */
	public void setSpacerType(BatchSpacerType aSpacerType) {
		this.spacerType = aSpacerType;
	}

	/**
	 * Obtiene el tipo de separador de documentos que deben emplearse en el
	 * lote.
	 * 
	 * @return el tipo de separador de documentos que deben emplearse en el
	 *         lote.
	 */
	public DocSpacerType getDocSpacerType() {
		return docSpacerType;
	}

	/**
	 * Establece un nuevo valor para el tipo de separador de documentos que
	 * deben emplearse en el lote.
	 * 
	 * @param aDocSpacerType
	 *            nuevo valor para el tipo de separador de documentos que deben
	 *            emplearse en el lote.
	 */
	public void setDocSpacerType(DocSpacerType aDocSpacerType) {
		this.docSpacerType = aDocSpacerType;
	}

	/**
	 * Obtiene el número de documentos que debe tener el lote.
	 * 
	 * @return el número de documentos que debe tener el lote.
	 */
	public Integer getBatchDocSize() {
		return batchDocSize;
	}

	/**
	 * Establece un nuevo valor para el número de documentos que debe tener el
	 * lote.
	 * 
	 * @param aBatchDocSize
	 *            nuevo valor para el número de documentos que debe tener el
	 *            lote.
	 */
	public void setBatchDocSize(Integer aBatchDocSize) {
		this.batchDocSize = aBatchDocSize;
	}

	/**
	 * Obtiene el número total de páginas que debe tener el lote.
	 * 
	 * @return el número total de páginas que debe tener el lote.
	 */
	public Integer getBatchPages() {
		return batchPages;
	}

	/**
	 * Establece un nuevo valor para el número total de páginas que debe tener
	 * el lote.
	 * 
	 * @param aBatchPages
	 *            nuevo valor para el número total de páginas que debe tener el
	 *            lote.
	 */
	public void setBatchPages(Integer aBatchPages) {
		this.batchPages = aBatchPages;
	}

	/**
	 * Obtiene el identificador de la plantilla de digtialización informado para
	 * la generación de la carátula.
	 * 
	 * @return el identificador de la plantilla de digtialización informado para
	 *         la generación de la carátula.
	 */
	public Long getScanProfileId() {
		return scanProfileId;
	}

	/**
	 * Establece un nuevo valor para el identificador de la plantilla de
	 * digtialización informado para la generación de la carátula.
	 * 
	 * @param aScanProfileId
	 *            nuevo valor para el identificador de la plantilla de
	 *            digtialización informado para la generación de la carátula.
	 */
	public void setScanProfileId(Long aScanProfileId) {
		this.scanProfileId = aScanProfileId;
	}

	/**
	 * Obtiene el identificador de la definición de documentos informado para la
	 * generación de la carátula.
	 * 
	 * @return el identificador de la definición de documentos informado para la
	 *         generación de la carátula.
	 */
	public Long getDocSpecId() {
		return docSpecId;
	}

	/**
	 * Establece un nuevo valor para el identificador de la definición de
	 * documentos informado para la generación de la carátula.
	 * 
	 * @param aDocSpecId
	 *            nuevo valor para el identificador de la definición de
	 *            documentos informado para la generación de la carátula.
	 */
	public void setDocSpecId(Long aDocSpecId) {
		this.docSpecId = aDocSpecId;
	}

	/**
	 * Obtiene el identificador de la unidad orgánica a la que serán asociados
	 * los documentos del lote.
	 * 
	 * @return el identificador de la unidad orgánica a la que serán asociados
	 *         los documentos del lote.
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Establece un nuevo valor para el identificador de la unidad orgánica a la
	 * que serán asociados los documentos del lote.
	 * 
	 * @param aDepartment
	 *            nuevo valor para el identificador de la unidad orgánica a la
	 *            que serán asociados los documentos del lote.
	 */
	public void setDepartment(String aDepartment) {
		this.department = aDepartment;
	}

	/**
	 * Obtiene la relación de metadatos informada por el personal de
	 * digitalización al solicitar la generación de una carátula.
	 * 
	 * @return la relación de metadatos informada por el personal de
	 *         digitalización al solicitar la generación de una carátula.
	 */
	public ArrayList<SpacerMetadataVO> getMetadataCollection() {
		return metadataCollection;
	}

	/**
	 * Establece una nueva relación de metadatos informada por el personal de
	 * digitalización al solicitar la generación de una carátula.
	 * 
	 * @param aMetadataCollection
	 *            nueva relación de metadatos informada por el personal de
	 *            digitalización al solicitar la generación de una carátula.
	 */
	public void setMetadataCollection(ArrayList<SpacerMetadataVO> aMetadataCollection) {
		this.metadataCollection.clear();

		if (aMetadataCollection != null && !aMetadataCollection.isEmpty()) {
			this.metadataCollection.addAll(aMetadataCollection);
		}

	}

	/**
	 * Obtiene el número de separadores reutilizables a generar junto a la
	 * carátula.
	 * 
	 * @return número de separadores reutilizables a generar junto a la
	 *         carátula.
	 */
	public Integer getNumberOfReusableDocSpacers() {
		return numberOfReusableDocSpacers;
	}

	/**
	 * Establece el número de separadores reutilizables a generar junto a la
	 * carátula.
	 * 
	 * @param aNumberOfReusableDocSpacers
	 *            número de separadores reutilizables a generar junto a la
	 *            carátula.
	 */
	public void setNumberOfReusableDocSpacers(Integer aNumberOfReusableDocSpacers) {
		this.numberOfReusableDocSpacers = aNumberOfReusableDocSpacers;
	}

}
