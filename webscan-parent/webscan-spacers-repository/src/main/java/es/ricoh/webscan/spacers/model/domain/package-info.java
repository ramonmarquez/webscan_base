/**
 * Entidades JPA 2.0 que representan el modelo de datos de generación de
 * carátulas y separadores reutilizables.
 */
package es.ricoh.webscan.spacers.model.domain;