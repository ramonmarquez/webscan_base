/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.dto.SpacerMetadataDTO.java.</p>
* <b>Descripción:</b><p> DTO para la entidad JPA metadato de carátula.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA metadato de carátula.
 * <p>
 * Clase SpacerMetadataDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacerMetadataDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador interno del metadato referenciado en una carátula.
	 */
	private Long id;

	/**
	 * Denominación del metadato referenciado en una carátula.
	 */
	private String name;

	/**
	 * Valor del metadato referenciado en una carátula.
	 */
	private String value;

	/**
	 * Entidad documento a la que pertenece el metadato.
	 */
	private Long spacerDocId;

	/**
	 * Constructor sin argumentos.
	 */
	public SpacerMetadataDTO() {

	}

	/**
	 * Obtiene el identificador interno del metadato referenciado en una
	 * carátula.
	 * 
	 * @return el identificador interno del metadato referenciado en una
	 *         carátula.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador interno del metadato
	 * referenciado en una carátula.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador interno del metadato
	 *            referenciado en una carátula.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación del metadato referenciado en una carátula.
	 * 
	 * @return la denominación del metadato referenciado en una carátula.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación del metadato referenciado
	 * en una carátula.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación del metadato referenciado en
	 *            una carátula.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el valor del metadato referenciado en una carátula.
	 * 
	 * @return el valor del metadato referenciado en una carátula.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Establece un nuevo valor para el metadato referenciado en una carátula.
	 * 
	 * @param aValue
	 *            nuevo valor para el metadato referenciado en una carátula.
	 */
	public void setValue(String aValue) {
		this.value = aValue;
	}

	/**
	 * Obtiene la entidad documento a la que pertenece el metadato.
	 * 
	 * @return la entidad documento a la que pertenece el metadato.
	 */
	public Long getSpacerDocId() {
		return spacerDocId;
	}

	/**
	 * Establece la entidad documento a la que pertenece el metadato.
	 * 
	 * @param aSpacerDocId
	 *            entidad documento a la que pertenece el metadato.
	 */
	public void setSpacerDocId(Long aSpacerDocId) {
		this.spacerDocId = aSpacerDocId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((spacerDocId == null) ? 0 : spacerDocId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpacerMetadataDTO other = (SpacerMetadataDTO) obj;
		if (spacerDocId == null) {
			if (other.spacerDocId != null)
				return false;
		} else if (!spacerDocId.equals(other.spacerDocId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
