/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.domain.SpacerStatus.java.</p>
* <b>Descripción:</b><p> Estados de generación de una carátula.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.model.domain;

/**
 * Estados de generación de una carátula.
 * <p>
 * Clase SpacerStatus.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum SpacerStatus {

	/**
	 * Estado generada.
	 */
	GENERATED("REQUESTED"),
	/**
	 * Estado en proceso.
	 */
	IN_PROCESS("IN PROCESS"),
	/**
	 * Estado completada correctamente.
	 */
	COMPLETED("COMPLETED"),
	/**
	 * Estado cancelada.
	 */
	CANCELED("CANCELED");

	/**
	 * Estado de reserva de trabajo de digitalización.
	 */
	private String status;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aStatus
	 *            Estado de carátulas.
	 */
	SpacerStatus(String aStatus) {
		this.status = aStatus;
	}

	/**
	 * Obtiene el atributo status.
	 * 
	 * @return el atributo status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Recupera un estado de carátula a partir de su denominación.
	 * 
	 * @param aName
	 *            denominación de estado de carátula.
	 * @return un estado de carátula. En caso de no encontrarse, devuelve null.
	 */
	public static SpacerStatus getByName(String aName) {
		Boolean found = Boolean.FALSE;
		SpacerStatus res = null;
		SpacerStatus[ ] values = values();

		for (int i = 0; !found && i < values.length; i++) {
			if (values[i].getStatus().equals(aName)) {
				res = values[i];
				found = Boolean.TRUE;
			}
		}

		return res;
	}
}
