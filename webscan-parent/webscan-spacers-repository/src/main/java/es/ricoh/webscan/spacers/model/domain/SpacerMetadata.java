/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.domain.SpacerMetadata.java.</p>
* <b>Descripción:</b><p> .</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.model.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA metadato de carátula de lote de documentos.
 * <p>
 * Clase SpacerMetadata.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.1.0.
 */
@Entity
@Table(name = "SPACER_METADATA", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME", "SPACER_DOC_ID" }) })
public class SpacerMetadata implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "SPACER_METADATA_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SPACER_METADATA_ID_SEQ", sequenceName = "SPACER_METADATA_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Atributo VALUE de la entidad.
	 */
	@Column(name = "VALUE")
	private String value;

	/**
	 * Entidad documento a la que pertenece el metadato.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SPACER_DOC_ID")
	private SpacerDoc spacerDoc;

	/**
	 * Constructor sin argumentos.
	 */
	public SpacerMetadata() {

	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo VALUE de la entidad.
	 * 
	 * @return el atributo VALUE de la entidad.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Establece un nuevo valor para el atributo VALUE de la entidad.
	 * 
	 * @param aValue
	 *            nuevo valor para el atributo VALUE de la entidad.
	 */
	public void setValue(String aValue) {
		this.value = aValue;
	}

	/**
	 * Obtiene la entidad documento a la que pertenece el metadato.
	 * 
	 * @return la entidad documento a la que pertenece el metadato.
	 */
	public SpacerDoc getSpacerDoc() {
		return spacerDoc;
	}

	/**
	 * Establece la entidad documento a la que pertenece el metadato.
	 * 
	 * @param aSpacerDoc
	 *            entidad documento a la que pertenece el metadato.
	 */
	public void setSpacerDoc(SpacerDoc aSpacerDoc) {
		this.spacerDoc = aSpacerDoc;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((spacerDoc == null) ? 0 : spacerDoc.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpacerMetadata other = (SpacerMetadata) obj;
		if (spacerDoc == null) {
			if (other.spacerDoc != null)
				return false;
		} else if (!spacerDoc.equals(other.spacerDoc))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
