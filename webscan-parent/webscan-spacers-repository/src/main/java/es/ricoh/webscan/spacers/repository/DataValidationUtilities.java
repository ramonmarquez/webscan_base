/**
 * <b>Archivo:</b><p>es.ricoh.webscan.spacers.repository.DataValidationUtilities.java.</p>
 * <b>Descripción:</b><p> Utilidad de comprobación y verificación de objetos DTO intercambiados con la
 * capa DAO del modelo de datos de generación de carátulas y separadores reutilizables.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.spacers.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerDocDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerMetadataDTO;
import es.ricoh.webscan.ValidationUtilities;
import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.model.DAOException;

/**
 * Utilidad de comprobación y verificación de objetos DTO intercambiados con la
 * capa DAO del modelo de datos de generación de carátulas y separadores
 * reutilizables.
 * <p>
 * Clase DataValidationUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class DataValidationUtilities extends ValidationUtilities {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(DataValidationUtilities.class);

	/**
	 * Máximo tamaño del identificador de metadatos referenciados en la
	 * carátula.
	 */
	private static final Integer METADATA_NAME_MAX_LENGTH = 50;

	/**
	 * Máximo tamaño del valor de metadatos referenciados en la carátula.
	 */
	private static final Integer METADATA_VALUE_MAX_LENGTH = 1024;

	/**
	 * Tamaño de 128 caracteres.
	 */
	private static final Integer NAME_128_MAX_LENGTH = 128;

	/**
	 * Comprueba que una carátula no sea nula, posea identificador (si no es
	 * nueva), el nombre del usuario de digitalización, la unidad orgánica, la
	 * fecha de generación (si no es nueva), el estado (si no es un nueva), el
	 * identificador externo (si no es nueva) y el identificador de auditoría
	 * (si no es nueva).
	 * 
	 * @param docsBatchSpacer
	 *            Carátula.
	 * @param isNew
	 *            Indica si la validación sobre un nuevo objeto, o uno ya
	 *            existente.
	 * @throws SpacersDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkDocsBatchSpacer(DocsBatchSpacerDTO docsBatchSpacer, Boolean isNew) throws SpacersDAOException {
		String excMsg;

		if (isNew) {
			excMsg = "Error al crear una nueva carátula. ";
		} else {
			excMsg = "Error al actualizar la carátula " + docsBatchSpacer.getId() + ". ";
		}

		if (docsBatchSpacer == null) {
			excMsg += "Objeto nulo o vacío.";
			LOGGER.error(excMsg);
			throw new SpacersDAOException(DAOException.CODE_998, excMsg);
		}

		if (!isNew && docsBatchSpacer.getId() == null) {
			excMsg += "Identificador nulo.";
			LOGGER.error(excMsg);
			throw new SpacersDAOException(DAOException.CODE_998, excMsg);
		}

		try {
			checkEmptyObject(docsBatchSpacer.getSpacerType(), "No se ha especificado el tipo de codificación empleado en la carátula.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_998, e.getMessage());
		}

		try {
			checkEmptyObject(docsBatchSpacer.getReqUserId(), "No se ha especificado el usuario que solicita la generación de la carátula.");
			checkStringMaxLength(docsBatchSpacer.getReqUserId(), NAME_128_MAX_LENGTH, "El identificador de usuario tiene un tamaño superior al permitido.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_501, e.getMessage());
		}

		try {
			checkEmptyObject(docsBatchSpacer.getScanProfileId(), "No se ha especificado la plantilla de digitalización de la carátula.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_506, e.getMessage());
		}

		try {
			checkEmptyObject(docsBatchSpacer.getDocSpecId(), "No se ha especificado la definición de documentos de la carátula.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_507, e.getMessage());
		}

		try {
			checkEmptyObject(docsBatchSpacer.getBatchDocSize(), excMsg + "No se ha especificado el número de documentos del lote identificado por la carátula.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_998, e.getMessage());
		}

		try {
			if (!isNew) {
				checkEmptyObject(docsBatchSpacer.getGenerationDate(), excMsg + "Fecha de generación de la carátula no especificada.");
				checkEmptyObject(docsBatchSpacer.getSpacerStatus(), excMsg + "Estado de generación de la carátula no especificado.");
				checkEmptyObject(docsBatchSpacer.getSpacerStatusDate(), excMsg + "Fecha de estado de generación de la carátula no especificada.");
				checkEmptyObject(docsBatchSpacer.getSpacerId(), excMsg + "Identificador normalizado no especificado.");
				checkEmptyObject(docsBatchSpacer.getAuditOpId(), excMsg + "Identificador de traza de auditoría no informado.");
			}
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e.getCause());
		}

		try {
			if (!isNew) {
				checkEmptyObject(docsBatchSpacer.getAuditOpId(), excMsg + "Identificador de traza de auditoría no informado.");
			}
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_999, e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba una lista de documentos y sus metadatos, verificando lo
	 * siguiente:
	 * <ul>
	 * <li>El conjunto de documentos: números de orden no repetidos y
	 * consecutivos.</li>
	 * <li>Para cada documento: el número de orden y denominación no nulos, y si
	 * no es nuevo, identificador interno y de reserva no nulos.</li>
	 * <li>Metadatos: nombre y valor no nulos, y si no es nuevo, identificador
	 * interno y de documento no nulos.</li>
	 * </ul>
	 * 
	 * @param scanWorkBookingDocs
	 *            Relación de documentos de una reserva, y sus metadatos.
	 * @param isNew
	 *            Indica si la validación sobre un nuevo objeto, o uno ya
	 *            existente.
	 * @throws ScanWorkBookingDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkSpacerDocsAndMetadataCollection(Map<SpacerDocDTO, List<SpacerMetadataDTO>> spacerDocs, Boolean isNew) throws SpacersDAOException {
		List<Integer> docOrders = new ArrayList<Integer>();
		SpacerDocDTO doc;

		if (isNew && (spacerDocs == null || spacerDocs.isEmpty())) {
			throw new SpacersDAOException(SpacersDAOException.CODE_508, "Relación de documentos de la carátula vacía.");
		}

		for (Iterator<SpacerDocDTO> it = spacerDocs.keySet().iterator(); it.hasNext();) {
			doc = it.next();
			checkSpacerDoc(doc, isNew);

			if (docOrders.contains(doc.getOrder())) {
				throw new SpacersDAOException(SpacersDAOException.CODE_511, "Números de orden de documentos repetidos (" + doc.getOrder() + ").");
			}
			docOrders.add(doc.getOrder());
			checkSpacerMetadataCollection(spacerDocs.get(doc), isNew);
		}

		checkDocOrderConsecutiveNumbers(spacerDocs.keySet());
	}

	/**
	 * Comprueba que los números de orden de los documentos sean consecutivos.
	 * 
	 * @param spacerDocs
	 *            Documentos incluidos en una petición de generación de
	 *            carátula.
	 * @throws SpacersDAOException
	 *             Si los números de orden de los documentos no son
	 *             consecutivos.
	 */
	private static void checkDocOrderConsecutiveNumbers(Collection<SpacerDocDTO> spacerDocs) throws SpacersDAOException {
		Integer orderNumber;
		List<Integer> docOrders = new ArrayList<Integer>();

		LOGGER.debug("Se comprueba que los números de orden de los documentos sean consecutivos.");

		for (SpacerDocDTO doc: spacerDocs) {
			docOrders.add(doc.getOrder());
		}

		Collections.sort(docOrders);
		orderNumber = 1;
		for (Integer i: docOrders) {
			if (i != orderNumber) {
				throw new SpacersDAOException(SpacersDAOException.CODE_512, "Números de orden de documentos del lote no consecutivos.");
			}
			orderNumber++;
		}
	}

	/**
	 * Comprueba un documento perteneciente al lote identificado por una
	 * carátula, verificando que el número de orden y su denominación no sean
	 * nulos, y si no es nuevo, identificador interno y de carátula no nulos.
	 * 
	 * @param doc
	 *            Documentos de una reserva.
	 * @param isNew
	 *            Indica si la validación sobre un nuevo objeto, o uno ya
	 *            existente.
	 * @throws ScanWorkBookingDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkSpacerDoc(SpacerDocDTO doc, Boolean isNew) throws SpacersDAOException {
		String excMsg;

		if (isNew) {
			excMsg = "Error al crear nuevos documentos de una reserva de trabajo de digitalización. ";
		} else {
			excMsg = "Error al actualizar documentos de una reserva de trabajo de digitalización. ";
		}

		try {
			checkEmptyObject(doc.getOrder(), "No se ha especificado el número de orden de uno o más documentos del lote.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_509, e.getMessage());
		}

		try {
			checkEmptyObject(doc.getName(), "No se ha especificado la denominación de uno o más documentos del lote.");
			checkStringMaxLength(doc.getName(), 200, "La denominación de uno o más documentos tiene un tamaño superior al permitido.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_510, e.getMessage());
		}

		try {
			if (!isNew) {
				checkEmptyObject(doc.getId(), excMsg + "Identificador de documento no especificado.");
				checkEmptyObject(doc.getBatchSpacerId(), excMsg + "Identificador de carátula no especificado.");
			}
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba que, en caso de especificarse metadatos de una carátula, estos
	 * tiene establecido nombre y valor.
	 * 
	 * @param metadataCollection
	 *            Metadatos.
	 * @param isNew
	 *            Indica si la validación sobre un nuevo objeto, o uno ya
	 *            existente.
	 * @throws SpacersDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkSpacerMetadataCollection(List<SpacerMetadataDTO> metadataCollection, Boolean isNew) throws SpacersDAOException {
		if (metadataCollection != null && !metadataCollection.isEmpty()) {

			for (SpacerMetadataDTO metadata: metadataCollection) {
				checkSpacerMetadata(metadata, isNew);
			}
		}
	}

	/**
	 * Comprueba un metadato de una carátula, verificando que su nombre y valor
	 * no son nulos, y si no es nuevo, identificador interno y de documento no
	 * nulos.
	 * 
	 * @param metadata
	 *            Metadato de un documento de una reserva de trabajo de
	 *            digitalización.
	 * @param isNew
	 *            Indica si la validación sobre un nuevo objeto, o uno ya
	 *            existente.
	 * @throws SpacersDAOException
	 *             Si los datos no son válidos.
	 */
	static void checkSpacerMetadata(SpacerMetadataDTO metadata, Boolean isNew) throws SpacersDAOException {
		String excMsg;

		try {
			checkEmptyObject(metadata.getName(), "No se ha especificado la denominación de uno o más metadatos.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_503, e.getMessage(), e.getCause());
		}

		if (metadata.getName().length() > METADATA_NAME_MAX_LENGTH) {
			excMsg = "Denominación de metadatos con tamaño superior al admitido (" + METADATA_NAME_MAX_LENGTH + "). Metadato incorrecto: " + metadata.getName();
			throw new SpacersDAOException(SpacersDAOException.CODE_503, excMsg);
		}

		try {
			checkEmptyObject(metadata.getValue(), "No se ha especificado el valor de uno o más metadatos.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(SpacersDAOException.CODE_504, e.getMessage(), e.getCause());
		}

		if (metadata.getValue().length() > METADATA_VALUE_MAX_LENGTH) {
			excMsg = "Valor de metadatos con tamaño superior al admitido (" + METADATA_VALUE_MAX_LENGTH + "). Metadato incorrecto: " + metadata.getName();
			throw new SpacersDAOException(SpacersDAOException.CODE_504, excMsg);
		}

		try {
			if (!isNew) {
				checkEmptyObject(metadata.getId(), "Identificador de metadtado no especificado.");
				checkEmptyObject(metadata.getSpacerDocId(), "Identificador de carátula no especificado.");
			}
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}
}
