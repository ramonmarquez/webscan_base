/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.SpacersDAOException.java.</p>
* <b>Descripción:</b><p> Excepción que encapsula los errores producidos en el componente DAO del
* modelo de datos de generación de carátulas y separadores reutilizables..</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.repository;

import es.ricoh.webscan.model.DAOException;

/**
 * Excepción que encapsula los errores producidos en el componente DAO del
 * modelo de datos de generación de carátulas y separadores reutilizables.
 * <p>
 * Clase SpacersDAOException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacersDAOException extends DAOException {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Parámetro de entrada incorrecto. Tipo de codificación no especificado o
	 * no válido.
	 */
	public static final String CODE_500 = "COD_500";

	/**
	 * Parámetro de entrada incorrecto. Identificador de usuario no
	 * especificado, o con tamaño superior al admitido.
	 */
	public static final String CODE_501 = "COD_501";

	/**
	 * Parámetro de entrada incorrecto. Datos de perfil de digitalización
	 * incompletos, metadatos no especificados.
	 */
	public static final String CODE_502 = "COD_502";

	/**
	 * Parámetro de entrada incorrecto. Datos de perfil de digitalización
	 * incompletos, denominación de metadatos no especificada o con longitud
	 * superior a la admitida.
	 */
	public static final String CODE_503 = "COD_503";

	/**
	 * Parámetro de entrada incorrecto. Datos de perfil de digitalización
	 * incompletos, valor de metadatos no especificado o con longitud superior a
	 * la admitida.
	 */
	public static final String CODE_504 = "COD_504";

	/**
	 * Parámetro de entrada incorrecto. Unidad orgánica no especificada, o con
	 * tamaño superior al admitido.
	 */
	public static final String CODE_505 = "COD_505";

	/**
	 * Parámetro de entrada incorrecto. Plantilla de digitalización no
	 * especificada.
	 */
	public static final String CODE_506 = "COD_506";

	/**
	 * Parámetro de entrada incorrecto. Definición de documentos no
	 * especificada.
	 */
	public static final String CODE_507 = "COD_507";

	/**
	 * Parámetro de entrada incorrecto. Documentos no especificados.
	 */
	public static final String CODE_508 = "COD_508";

	/**
	 * Parámetro de entrada incorrecto. Documentos sin número de orden.
	 */
	public static final String CODE_509 = "COD_509";

	/**
	 * Parámetro de entrada incorrecto. Documentos sin denominación, o con
	 * tamaño superior al admitido.
	 */
	public static final String CODE_510 = "COD_510";

	/**
	 * Parámetro de entrada incorrecto. Documentos con números de orden
	 * repetidos.
	 */
	public static final String CODE_511 = "COD_511";

	/**
	 * Parámetro de entrada incorrecto. Documentos con números de orden no
	 * consecutivos.
	 */
	public static final String CODE_512 = "COD_512";

	/**
	 * Constructor sin argumentos.
	 */
	public SpacersDAOException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public SpacersDAOException(String aCode, String aMessage) {
		super(aCode, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public SpacersDAOException(String aMessage) {
		super(CODE_999, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public SpacersDAOException(Throwable aCause) {
		super(CODE_999, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public SpacersDAOException(final String aMessage, Throwable aCause) {
		super(CODE_999, aMessage, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public SpacersDAOException(final String aCode, final String aMessage, Throwable aCause) {
		super(aCode, aMessage, aCause);
	}

}
