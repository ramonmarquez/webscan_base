/**
 * <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.dto.MappingUtilities.java.</p>
 * <b>Descripción:</b><p> Clase de utilidades para la transformación de entidades JPA, pertenecientes
 * al modelo de datos de generación de carátulas y separadores reutilizables, en objetos DTO, y viceversa.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */

package es.ricoh.webscan.spacers.model.dto;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.spacers.model.domain.DocsBatchSpacer;
import es.ricoh.webscan.spacers.model.domain.SpacerDoc;
import es.ricoh.webscan.spacers.model.domain.SpacerMetadata;

/**
 * Clase de utilidades para la transformación de entidades JPA, pertenecientes
 * al modelo de datos de generación de carátulas y separadores reutilizables, en
 * objetos DTO, y viceversa.
 * <p>
 * Clase MappingUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class MappingUtilities {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MappingUtilities.class);

	/**
	 * Transforma una entidad de BBDD carátula en un objeto DocsBatchSpacer.
	 * 
	 * @param entity
	 *            Entidad de BBDD carátula.
	 * @return Objeto ScanWorkBookingDTO.
	 */
	public static DocsBatchSpacerDTO fillDocsBatchSpacerDto(DocsBatchSpacer entity) {
		DocsBatchSpacerDTO res = new DocsBatchSpacerDTO();

		if (entity != null) {
			res.setAuditOpId(entity.getAuditOpId());
			res.setBatchDocSize(entity.getBatchDocSize());
			res.setBatchPages(entity.getBatchPages());
			res.setDepartment(entity.getDepartment());
			res.setDocSpacerType(entity.getDocSpacerType());
			res.setDocSpecId(entity.getDocSpecId());
			res.setGenerationDate(entity.getGenerationDate());
			res.setId(entity.getId());
			res.setReqUserId(entity.getReqUserId());
			res.setScanProfileId(entity.getScanProfileId());
			res.setSpacerId(entity.getSpacerId());
			res.setSpacerStatus(entity.getSpacerStatus());
			res.setSpacerStatusDate(entity.getSpacerStatusDate());
			res.setSpacerType(entity.getSpacerType());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto DocsBatchSpacerDTO en una entidad de
	 * BBDD carátula.
	 * 
	 * @param dto
	 *            objeto DocsBatchSpacerDTO.
	 * @return Objeto entidad de BBDD reserva de trabajo de digitalización.
	 */
	public static DocsBatchSpacer fillDocsBatchSpacerEntity(DocsBatchSpacerDTO dto) {
		DocsBatchSpacer res = new DocsBatchSpacer();

		if (dto != null) {
			// Datos globales
			res.setAuditOpId(dto.getAuditOpId());
			res.setBatchDocSize(dto.getBatchDocSize());
			res.setBatchPages(dto.getBatchPages());
			res.setDepartment(dto.getDepartment());
			res.setDocSpacerType(dto.getDocSpacerType());
			res.setDocSpecId(dto.getDocSpecId());
			res.setGenerationDate(dto.getGenerationDate());
			res.setId(dto.getId());
			res.setReqUserId(dto.getReqUserId());
			res.setScanProfileId(dto.getScanProfileId());
			res.setSpacerId(dto.getSpacerId());
			res.setSpacerStatus(dto.getSpacerStatus());
			res.setSpacerStatusDate(dto.getSpacerStatusDate());
			res.setSpacerType(dto.getSpacerType());
		}

		return res;
	}

	/**
	 * Transforma una entidad de BBDD documento perteneciente a un lote
	 * identificado por una carátula en un objeto SpacerDocDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD documento perteneciente a un lote identificado
	 *            por una carátula.
	 * @return Objeto SpacerDocDTO.
	 */
	public static SpacerDocDTO fillSpacerDocDto(SpacerDoc entity) {
		SpacerDocDTO res = new SpacerDocDTO();

		if (entity != null) {
			res.setBatchSpacerId(entity.getBatchSpacer().getId());
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setOrder(entity.getOrder());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto SpacerDocDTO en una entidad de BBDD
	 * documento perteneciente a un lote identificado por una carátula.
	 * 
	 * @param dto
	 *            objeto SpacerDocDTO.
	 * @return Objeto entidad de BBDD documento perteneciente a un lote
	 *         identificado por una carátula.
	 */
	public static SpacerDoc fillSpacerDocEntity(SpacerDocDTO dto) {
		SpacerDoc res = new SpacerDoc();

		if (dto != null) {
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setOrder(dto.getOrder());
		}

		return res;
	}

	/**
	 * Transforma una entidad de BBDD metadato referenciado en una carátula en
	 * un objeto SpacerMetadataDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD metadato de documento perteneciente a un lote
	 *            identificado por una carátula.
	 * @return Objeto SpacerMetadataDTO.
	 */
	public static SpacerMetadataDTO fillSpacerMetadataDto(SpacerMetadata entity) {
		SpacerMetadataDTO res = new SpacerMetadataDTO();

		if (entity != null) {
			res.setSpacerDocId(entity.getSpacerDoc().getId());
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setValue(entity.getValue());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto SpacerMetadataDTO en una entidad de
	 * BBDD metadato referenciado en una carátula.
	 * 
	 * @param dto
	 *            objeto SpacerMetadataDTO.
	 * @return Objeto entidad de BBDD metadato referenciado en una carátula.
	 */
	public static SpacerMetadata fillSpacerMetadataEntity(SpacerMetadataDTO dto) {
		SpacerMetadata res = new SpacerMetadata();

		if (dto != null) {
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setValue(dto.getValue());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD metadato referenciado en una
	 * carátula, en una lista de objetos SpacerMetadataDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD metadato referenciado en una
	 *            carátula.
	 * @return lista de objetos SpacerMetadataDTO.
	 */
	public static List<SpacerMetadataDTO> fillSpacerMetadataListDto(List<SpacerMetadata> entities) {
		int queryResultSize = 0;
		List<SpacerMetadataDTO> res = new ArrayList<SpacerMetadataDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (SpacerMetadata metadata: entities) {
				res.add(fillSpacerMetadataDto(metadata));
			}
		}

		LOG.debug("[WEBSCAN-SPACERS-MODEL] Metadatos parseados: {}", queryResultSize);
		return res;
	}
}
