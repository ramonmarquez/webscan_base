/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.dao.SpacersDAO.java.</p>
* <b>Descripción:</b><p> Interfaz que define la operaciones de la capa DAO sobre las entidades
* pertenecientes al modelo de datos de generación de carátulas y separadores
* reutilizables.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.repository.dao;

import java.util.List;
import java.util.Map;

import es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerDocDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerMetadataDTO;
import es.ricoh.webscan.spacers.repository.SpacersDAOException;

/**
 * Interfaz que define la operaciones de la capa DAO sobre las entidades
 * pertenecientes al modelo de datos de generación de carátulas y separadores
 * reutilizables.
 * <p>
 * Clase SpacersDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public interface SpacersDAO {

	/**
	 * Recupera una carátula a partir de su identificador.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @return La carátula solicitada.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	DocsBatchSpacerDTO getDocsBatchSpacer(Long docsBatchSpacerId) throws SpacersDAOException;

	/**
	 * Recupera una carátula a partir de su identificador normalizado.
	 * 
	 * @param spacerId
	 *            Identificador normalizado de carátula.
	 * @return Carátula solicitada.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula solicitada, o se produce algún error
	 *             en la consulta a BBDD.
	 */
	DocsBatchSpacerDTO getDocsBatchSpacerByStandardId(String spacerId) throws SpacersDAOException;

	/**
	 * Obtiene el número de documentos de una carátula.
	 * 
	 * @param spacerId
	 *            Identificador de carátula.
	 * @return Número de documentos de una carátula.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, o se produce algún error al
	 *             ejecutar la consulta en BBDD.
	 */
	Integer getDocsBatchSpacerNumberOfDocs(Long spacerId) throws SpacersDAOException;

	/**
	 * Crea una nueva carátula en el modelo de datos.
	 * 
	 * @param docsBatchSpacer
	 *            Carátula.
	 * @param documents
	 *            Relación de documentos, y sus metadatos, referenciados en la
	 *            carátula.
	 * @return Identificador de la carátula creada.
	 * @throws SpacersDAOException
	 *             Si se produce algún error al crear la nueva carátula.
	 */
	Long createDocsBatchSpacer(DocsBatchSpacerDTO docsBatchSpacer, Map<SpacerDocDTO, List<SpacerMetadataDTO>> documents) throws SpacersDAOException;

	/**
	 * Elimina la información de una carátula del modelo de datos.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @param metadataCollection
	 *            Relación de metadatos referenciados en la carátula.
	 * @throws SpacersDAOException
	 *             Si se produce algún error al eliminar la carátula.
	 */
	void removeDocsBatchSpacer(Long docsBatchSpacerId) throws SpacersDAOException;

	/**
	 * Actualiza una carátula en el modelo de datos.
	 * 
	 * @param docsBatchSpacer
	 *            Carátula.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, o se produce algún error al
	 *             ejecutar la setencia de BBDD.
	 */
	void updateDocsBatchSpacer(DocsBatchSpacerDTO docsBatchSpacer) throws SpacersDAOException;

	/**
	 * Recupera los documentos, y sus metadatos, de una carátula generada por el
	 * Sistema.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @return Relación de documentos, y sus metadatos, referenciados en la
	 *         carátula.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, o se produce algún error al
	 *             ejecutar la consulta en BBDD.
	 */
	Map<SpacerDocDTO, List<SpacerMetadataDTO>> getSpacerDocsAndMetadataCollection(Long docsBatchSpacerId) throws SpacersDAOException;

	/**
	 * Elimina los documentos de una carátula registrada en el modelo de datos.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, o se produce algún error al
	 *             ejecutar la setencia de BBDD.
	 */
	void removeSpacerDocs(Long docsBatchSpacerId) throws SpacersDAOException;

}
