/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.domain.SpacerDoc.java.</p>
* <b>Descripción:</b><p> Entidad JPA documento perteneciente a un lote referenciado por una carátula.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entidad JPA documento perteneciente a un lote referenciado por una carátula.
 * <p>
 * Clase SpacerDoc.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
@Entity
@Table(name = "SPACER_DOC")
public class SpacerDoc implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "SPACER_DOC_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SPACER_DOC_ID_SEQ", sequenceName = "SPACER_DOC_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo BATCH_ORDER_NUMBER de la entidad.
	 */
	@Column(name = "BATCH_ORDER_NUMBER")
	private Integer order;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Entidad carátula a la que pertenece el metadato.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DOCS_BATCH_SPACER_ID")
	private DocsBatchSpacer batchSpacer;

	/**
	 * Relación de metadatos del documento.
	 */
	@OneToMany(mappedBy = "spacerDoc", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.spacers.model.domain.SpacerMetadata.class)
	private List<SpacerMetadata> metadataCollection = new ArrayList<SpacerMetadata>();

	/**
	 * Constructor sin argumentos.
	 */
	public SpacerDoc() {

	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo BATCH_ORDER_NUMBER de la entidad.
	 * 
	 * @return el atributo BATCH_ORDER_NUMBER de la entidad.
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * Establece un nuevo valor para el atributo ORDER_NUMBER de la entidad.
	 * 
	 * @param anOrder
	 *            nuevo valor para el atributo ORDER_NUMBER de la entidad.
	 */
	public void setOrder(Integer anOrder) {
		this.order = anOrder;
	}

	/**
	 * Obtiene la carátula que identifica el lote al que pertenece el documento.
	 * 
	 * @return la carátula que identifica el lote al que pertenece el documento.
	 */
	public DocsBatchSpacer getBatchSpacer() {
		return batchSpacer;
	}

	/**
	 * Establece la carátula que identifica el lote al que pertenece el
	 * documento.
	 * 
	 * @param aBatchSpacer
	 *            carátula que identifica el lote al que pertenece el documento.
	 */
	public void setBatchSpacer(DocsBatchSpacer aBatchSpacer) {
		this.batchSpacer = aBatchSpacer;
	}

	/**
	 * Obtiene la relación de metadatos del documento.
	 * 
	 * @return la relación de metadatos del documento.
	 */
	public List<SpacerMetadata> getMetadataCollection() {
		return metadataCollection;
	}

	/**
	 * Establece la relación de metadatos del documento.
	 * 
	 * @param aMetadataCollection
	 *            nueva relación de metadatos del documento.
	 */
	public void setMetadataCollection(List<SpacerMetadata> aMetadataCollection) {
		this.metadataCollection.clear();

		if (aMetadataCollection != null && !aMetadataCollection.isEmpty()) {
			this.metadataCollection.addAll(aMetadataCollection);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + ((batchSpacer == null) ? 0 : batchSpacer.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpacerDoc other = (SpacerDoc) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (batchSpacer == null) {
			if (other.batchSpacer != null)
				return false;
		} else if (!batchSpacer.equals(other.batchSpacer))
			return false;
		return true;
	}

}
