/**
 * Objetos DTO de las entidades JPA 2.0 que representan el modelo de datos de
 * generación de carátulas y separadores reutilizables, y utilidades de mapeo de
 * ambos tipos de objetos.
 */
package es.ricoh.webscan.spacers.model.dto;