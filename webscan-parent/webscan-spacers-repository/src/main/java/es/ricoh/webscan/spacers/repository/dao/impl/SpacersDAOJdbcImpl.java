/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.dao.impl.SpacersDAOJdbcImpl.java.</p>
* <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre las entidades pertenecientes al
* modelo de datos de generación de carátulas y separadores</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.repository.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.spacers.model.domain.DocsBatchSpacer;
import es.ricoh.webscan.spacers.model.domain.SpacerDoc;
import es.ricoh.webscan.spacers.model.domain.SpacerMetadata;
import es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO;
import es.ricoh.webscan.spacers.model.dto.MappingUtilities;
import es.ricoh.webscan.spacers.model.dto.SpacerDocDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerMetadataDTO;
import es.ricoh.webscan.spacers.repository.SpacersDAOException;
import es.ricoh.webscan.spacers.repository.dao.SpacersDAO;
import es.ricoh.webscan.model.dao.impl.CommonUtilitiesDAOJdbcImpl;

/**
 * Implementación JPA 2.0 de la capa DAO sobre las entidades pertenecientes al
 * modelo de datos de generación de carátulas y separadores reutilizables.
 * <p>
 * Clase SpacersDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacersDAOJdbcImpl implements SpacersDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SpacersDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la manipulación de las entidades de incluidas en el
	 * contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-gv-spacers-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * Constructor sin aegumentos.
	 */
	public SpacersDAOJdbcImpl() {
		super();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.spacers.repository.dao.SpacersDAO#getDocsBatchSpacer(java.lang.Long)
	 */
	@Override
	public DocsBatchSpacerDTO getDocsBatchSpacer(Long docsBatchSpacerId) throws SpacersDAOException {
		DocsBatchSpacer entity;
		DocsBatchSpacerDTO res = null;
		String excMsg;

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Recuperando la carátula " + docsBatchSpacerId + " ...");
			}

			entity = getDocsBatchSpacerEntity(docsBatchSpacerId);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Parseando la carátula " + docsBatchSpacerId + " recuperada de BBDD...");
			}
			res = MappingUtilities.fillDocsBatchSpacerDto(entity);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Carátula " + res.getId() + " recuperada de BBDD.");
			}
		} catch (SpacersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula " + docsBatchSpacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.spacers.repository.dao.SpacersDAO#getDocsBatchSpacerByStandardId(java.lang.String)
	 */
	@Override
	public DocsBatchSpacerDTO getDocsBatchSpacerByStandardId(String spacerId) throws SpacersDAOException {
		DocsBatchSpacerDTO res = null;
		DocsBatchSpacer entity;
		String excMsg;

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Recuperando la carátula con identificador normalizado " + spacerId + " ...");
			}

			TypedQuery<DocsBatchSpacer> query = entityManager.createQuery("SELECT dbs FROM DocsBatchSpacer dbs WHERE dbs.spacerId = :spacerId", DocsBatchSpacer.class);
			query.setParameter("spacerId", spacerId);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Ejecutando consulta para recuperar la carátula con identificador normalizado " + spacerId + " ...");
			}

			entity = query.getSingleResult();

			res = MappingUtilities.fillDocsBatchSpacerDto(entity);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Carátula con identificador normalizado " + spacerId + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula con identificador normalizado " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Se excedio el tiempo máximo para recuperar la carátula con identificador normalizado " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula con identificador normalizado " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula con identificador normalizado " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula con identificador normalizado " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula con identificador normalizado " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(SpacersDAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula con identificador normalizado " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(SpacersDAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula con identificador normalizado " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula con identificador normalizado " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.spacers.repository.dao.SpacersDAO#getDocsBatchSpacerNumberOfDocs(java.lang.Long)
	 */
	@Override
	public Integer getDocsBatchSpacerNumberOfDocs(Long spacerId) throws SpacersDAOException {
		Integer res = 0;
		String excMsg;

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Recuperando el número de documentos de la carátula {} ...", spacerId);
			}
			TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(doc.id) FROM SpacerDoc doc JOIN doc.batchSpacer dbs WHERE dbs.id = :spacerId", Long.class);
			query.setParameter("spacerId", spacerId);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Ejecutando consulta para recuperar el número de documentos de la carátula {} ...", spacerId);
			}
			res = query.getSingleResult().intValue();

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] La carátula {} tiene {} documentos.", spacerId, res);
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando el número de documentos de la carátula " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Se excedio el tiempo máximo para recuperar el número de documentos de la carátula " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando el número de documentos de la carátula " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando el número de documentos de la carátula " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando el número de documentos de la carátula " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Se excedio el tiempo máximo para recuperar el número de documentos de la carátula " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando el número de documentos de la carátula " + spacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.spacers.repository.dao.SpacersDAO#createDocsBatchSpacer(es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO,
	 *      java.util.Map)
	 */
	@Override
	public Long createDocsBatchSpacer(DocsBatchSpacerDTO docsBatchSpacer, Map<SpacerDocDTO, List<SpacerMetadataDTO>> documents) throws SpacersDAOException {
		List<SpacerDoc> entities = new ArrayList<SpacerDoc>();
		Long res;
		DocsBatchSpacer entity;
		SpacerDocDTO doc;
		String excMsg;

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Creando la carátula " + docsBatchSpacer + " ...");
		}

		try {
			entity = MappingUtilities.fillDocsBatchSpacerEntity(docsBatchSpacer);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Carátula " + entity.getId() + " registrada, comprobando documentos ...");
			}

			if (!documents.isEmpty()) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Se registran los documentos de la nueva carátula " + entity.getId() + " ...");
				}

				for (Iterator<SpacerDocDTO> it = documents.keySet().iterator(); it.hasNext();) {
					doc = it.next();
					entities.add(createSpacerDoc(doc, documents.get(doc), entity));
				}
				entity.setDocuments(entities);
			}

			entityManager.persist(entity);
			entityManager.flush();
			res = entity.getId();

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Carátula " + entity.getId() + " creada en BBDD.");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al crear la carátula " + docsBatchSpacer + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(SpacersDAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al crear la carátula " + docsBatchSpacer + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al crear la carátula " + docsBatchSpacer + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al crear la carátula " + docsBatchSpacer + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al crear la carátula " + docsBatchSpacer + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.spacers.repository.dao.SpacersDAO#removeDocsBatchSpacer(java.lang.Long)
	 */
	@Override
	public void removeDocsBatchSpacer(Long docsBatchSpacerId) throws SpacersDAOException {
		DocsBatchSpacer entity;
		String excMsg;

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Eliminando la carátula " + docsBatchSpacerId + " ...");

				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Se recupera la carátula " + docsBatchSpacerId + " de base de datos ...");
			}

			entity = getDocsBatchSpacerEntity(docsBatchSpacerId);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Eliminando " + docsBatchSpacerId + " recuperada de BBDD...");
			}

			entityManager.remove(entity);
			entityManager.flush();

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Carátula " + docsBatchSpacerId + " eliminada de BBDD.");
			}
		} catch (SpacersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula " + docsBatchSpacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.spacers.repository.dao.SpacersDAO#updateDocsBatchSpacer(es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO)
	 */
	@Override
	public void updateDocsBatchSpacer(DocsBatchSpacerDTO docsBatchSpacer) throws SpacersDAOException {
		DocsBatchSpacer entity;
		String excMsg;

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Actualizando la carátula " + docsBatchSpacer.getId() + " ...");
		}

		try {
			entity = getDocsBatchSpacerEntity(docsBatchSpacer.getId());

			// Se actualizan los datos globales
			entity.setSpacerStatus(docsBatchSpacer.getSpacerStatus());
			entity.setSpacerStatusDate(docsBatchSpacer.getSpacerStatusDate());
			entity.setAuditOpId(docsBatchSpacer.getAuditOpId());

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Modificando la carátula " + docsBatchSpacer.getId() + " en BBDD ...");
			}

			entityManager.merge(entity);
			entityManager.flush();

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Carátula " + docsBatchSpacer.getId() + " actualizada en BBDD ...");
			}
		} catch (SpacersDAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error actualizando la carátula " + docsBatchSpacer.getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error actualizando la carátula " + docsBatchSpacer.getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error actualizando la carátula " + docsBatchSpacer.getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error actualizando la carátula " + docsBatchSpacer.getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.spacers.repository.dao.SpacersDAO#getSpacerMetadataCollection(java.lang.Long)
	 */
	@Override
	public Map<SpacerDocDTO, List<SpacerMetadataDTO>> getSpacerDocsAndMetadataCollection(Long docsBatchSpacerId) throws SpacersDAOException {
		List<SpacerMetadata> metadataCollection;
		List<SpacerDoc> documents;
		Map<SpacerDocDTO, List<SpacerMetadataDTO>> res = new HashMap<SpacerDocDTO, List<SpacerMetadataDTO>>();
		DocsBatchSpacer entity;
		String excMsg;

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Recuperando los documentos pertenecientes al lote identificado por la carátula " + docsBatchSpacerId + " ...");
			}

			entity = getDocsBatchSpacerEntity(docsBatchSpacerId);
			documents = entity.getDocuments();

			if (documents != null && !documents.isEmpty()) {

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Parseando los documentos pertenecientes al lote identificado por la carátula " + docsBatchSpacerId + " recuperada de BBDD...");
				}

				for (SpacerDoc doc: documents) {
					metadataCollection = doc.getMetadataCollection();
					res.put(MappingUtilities.fillSpacerDocDto(doc), MappingUtilities.fillSpacerMetadataListDto(metadataCollection));
				}
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] " + res.size() + " documentos recuperados para la carátula " + docsBatchSpacerId + ".");
			}
		} catch (SpacersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando los metadatos referenciados de la carátula " + docsBatchSpacerId + ": " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.spacers.repository.dao.SpacersDAO#removeSpacerDocs(java.lang.Long)
	 */
	@Override
	public void removeSpacerDocs(Long docsBatchSpacerId) throws SpacersDAOException {
		Integer docsSize = 0;
		Query query;
		String excMsg;

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Eliminando los documentos de la carátula " + docsBatchSpacerId + " ...");
		}

		try {
			query = entityManager.createQuery("DELETE FROM SpacerDoc doc JOIN doc.batchSpacer bs WHERE bs.id = :docsBatchSpacerId");
			query.setParameter("docsBatchSpacerId", docsBatchSpacerId);

			docsSize = query.executeUpdate();

			entityManager.flush();
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] " + docsSize + " documentos de la carátula " + docsBatchSpacerId + " eliminados en BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error eliminando los documentos de la carátula " + docsBatchSpacerId + " de BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error eliminando los documentos de la carátula " + docsBatchSpacerId + " de BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error eliminando los documentos de la carátula " + docsBatchSpacerId + " de BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error eliminando los documentos de la carátula " + docsBatchSpacerId + " de BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * Recupera de BBDD una entidad especificación de documento a partir de su
	 * identificador.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @return Entidad de BBDD perfil de digitalización.
	 * @throws SpacersDAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private DocsBatchSpacer getDocsBatchSpacerEntity(Long docsBatchSpacerId) throws SpacersDAOException {
		DocsBatchSpacer res;
		String excMsg;

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Recuperando la carátula " + docsBatchSpacerId + " de BBDD ...");
			}
			res = entityManager.find(DocsBatchSpacer.class, docsBatchSpacerId);

			if (res == null) {
				excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula " + docsBatchSpacerId + " en BBDD. La entidad no existe.";
				LOGGER.error(excMsg);
				throw new SpacersDAOException(SpacersDAOException.CODE_901, excMsg);
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Reserva de trabajo de digitalización " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula " + docsBatchSpacerId + " de BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (SpacersDAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error recuperando la carátula " + docsBatchSpacerId + " de BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Registra en BBDD un nuevo documento, y sus metadatos, para una reserva de
	 * trabajo de digitalización.
	 * 
	 * @param doc
	 *            documento.
	 * @param metatadaCollection
	 *            metadatos del documento.
	 * @param batchSpacer
	 *            carátula.
	 * @return el nuevo documento.
	 * @throws SpacersDAOException
	 *             Si se produce algún error al registrar el documento, o sus
	 *             metadatos en BBDD.
	 */
	private SpacerDoc createSpacerDoc(SpacerDocDTO doc, List<SpacerMetadataDTO> metatadaCollection, DocsBatchSpacer batchSpacer) throws SpacersDAOException {
		SpacerDoc res;
		List<SpacerMetadata> mds = new ArrayList<SpacerMetadata>();
		String excMsg;
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Creando documento para la carátula " + batchSpacer.getId() + " ...");
		}
		try {
			res = MappingUtilities.fillSpacerDocEntity(doc);
			res.setBatchSpacer(batchSpacer);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Documento " + res.getId() + " de la carátula " + batchSpacer.getId() + " creado, se registran sus metadatos ...");
			}
			mds = createSpacerMetadataCollection(metatadaCollection, res);
			res.setMetadataCollection(mds);

			// entityManager.merge(res);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("[WEBSCAN-SPACERS-MODEL] Finalizado el registro en BBDDD del documento " + res.getId() + " de la carátula " + batchSpacer.getId() + ".");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo documento para la carátula " + batchSpacer.getId() + " en BBDD: " + e.getMessage();
			throw new SpacersDAOException(SpacersDAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo documento para la carátula " + batchSpacer.getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo documento para la carátula " + batchSpacer.getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo documento para la carátula " + batchSpacer.getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo documento para la carátula " + batchSpacer.getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Registra en BBDD los metadatos de un documento perteneciente a un lote
	 * identificado por una carátula.
	 * 
	 * @param metatadaCollection
	 *            metadatos a registrar.
	 * @param doc
	 *            documento.
	 * @return Relación de metadatos registrados en BBDD.
	 * @throws SpacersDAOException
	 *             Si se produce algún error al registrar los metadatos en BBDD.
	 */
	private List<SpacerMetadata> createSpacerMetadataCollection(List<SpacerMetadataDTO> metatadaCollection, SpacerDoc doc) throws SpacersDAOException {
		List<SpacerMetadata> res = new ArrayList<SpacerMetadata>();
		SpacerMetadata metadata;
		String excMsg;

		try {
			for (SpacerMetadataDTO dto: metatadaCollection) {
				metadata = MappingUtilities.fillSpacerMetadataEntity(dto);
				metadata.setSpacerDoc(doc);
				res.add(metadata);
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo metadato del documento " + doc.getId() + " de la carátula " + doc.getBatchSpacer().getId() + " en BBDD: " + e.getMessage();
			throw new SpacersDAOException(SpacersDAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo metadato del documento " + doc.getId() + " de la carátula " + doc.getBatchSpacer().getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo metadato del documento " + doc.getId() + " de la carátula " + doc.getBatchSpacer().getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo metadato del documento " + doc.getId() + " de la carátula " + doc.getBatchSpacer().getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-SPACERS-MODEL] Error al registrar un nuevo metadato del documento " + doc.getId() + " de la carátula " + doc.getBatchSpacer().getId() + " en BBDD: " + e.getMessage();
			LOGGER.error(excMsg, e);
			throw new SpacersDAOException(excMsg, e);
		}

		return res;
	}

}
