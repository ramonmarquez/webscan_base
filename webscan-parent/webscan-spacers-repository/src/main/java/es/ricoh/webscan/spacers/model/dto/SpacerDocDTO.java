/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.dto.SpacerDocDTO.java.</p>
* <b>Descripción:</b><p>DTO para la entidad JPA documento perteneciente al lote identificado por una carátula.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA documento perteneciente al lote identificado por una
 * carátula.
 * <p>
 * Clase SpacerDocDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacerDocDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador del documento.
	 */
	private Long id;

	/**
	 * Número de orden del documento en el lote.
	 */
	private Integer order;

	/**
	 * Denominación del documento.
	 */
	private String name;

	/**
	 * Identificador de la carátula a la que pertenece el documento.
	 */
	private Long batchSpacerId;

	/**
	 * Constructor sin argumentos.
	 */
	public SpacerDocDTO() {

	}

	/**
	 * Obtiene el identificador del documento.
	 * 
	 * @return el identificador del documento.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador del documento.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador del documento.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el número de orden del documento en el lote.
	 * 
	 * @return el número de orden del documento en el lote.
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * Establece un nuevo valor para el número de orden del documento en el
	 * lote.
	 * 
	 * @param anOrder
	 *            nuevo valor para el número de orden del documento en el lote.
	 */
	public void setOrder(Integer anOrder) {
		this.order = anOrder;
	}

	/**
	 * Obtiene la denominación del documento.
	 * 
	 * @return la denominación del documento.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación del documento.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación del documento.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el identificador de la carátula a la que pertenece el documento.
	 * 
	 * @return el identificador de la carátula a la que pertenece el documento.
	 */
	public Long getBatchSpacerId() {
		return batchSpacerId;
	}

	/**
	 * Establece el identificador de la carátula a la que pertenece el
	 * documento.
	 * 
	 * @param aBatchSpacerId
	 *            identificador de la carátula a la que pertenece el documento.
	 */
	void setBatchSpacerId(Long aBatchSpacerId) {
		this.batchSpacerId = aBatchSpacerId;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + ((batchSpacerId == null) ? 0 : batchSpacerId.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpacerDocDTO other = (SpacerDocDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (batchSpacerId == null) {
			if (other.batchSpacerId != null)
				return false;
		} else if (!batchSpacerId.equals(other.batchSpacerId))
			return false;
		return true;
	}

}
