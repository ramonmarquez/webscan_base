/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.domain.BatchSpacerType.java.</p>
* <b>Descripción:</b><p> Tipos de codificación de la información incluida en las carátulas generadas.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.model.domain;

/**
 * Tipos de codificación de la información incluida en las carátulas generadas.
 * <p>
 * Clase BatchSpacerType.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum BatchSpacerType {

	/**
	 * Códgio QR.
	 */
	CODE_QR("CODE QR"),
	/**
	 * Código 128.
	 */
	CODE_128("CODE 128"),
	/**
	 * PDF 417.
	 */
	PDF_417("PDF 417");

	/**
	 * Denominación del tipo de codificación.
	 */
	private String name;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aName
	 *            Denominación del tipo de codificación.
	 */
	BatchSpacerType(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo name.
	 * 
	 * @return el atributo name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Recupera un tipo de codificación a partir de su denominación.
	 * 
	 * @param aName
	 *            Denominación de tipo de codificación.
	 * @return un tipo de codificación. En caso de no encontrarse, devuelve
	 *         null.
	 */
	public static BatchSpacerType getByName(String aName) {
		Boolean found = Boolean.FALSE;
		BatchSpacerType res = null;
		BatchSpacerType[ ] codeTypes = values();

		for (int i = 0; !found && i < codeTypes.length; i++) {
			if (codeTypes[i].getName().equals(aName)) {
				res = codeTypes[i];
				found = Boolean.TRUE;
			}
		}

		return res;
	}
}
