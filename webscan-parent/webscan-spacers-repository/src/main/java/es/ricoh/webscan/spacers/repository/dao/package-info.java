/**
 * Definición de las interfaces que conforman la capa de acceso a datos de las
 * diferentes entidades pertenecientes al modelo de datos de generación de
 * carátulas y separadores reutilizables.
 */
package es.ricoh.webscan.spacers.repository.dao;