/**
 * Capa de acceso a datos al modelo de datos de generación de carátulas y
 * separadores reutilizables.
 */
package es.ricoh.webscan.spacers.repository;