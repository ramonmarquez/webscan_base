/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.domain.DocsBatchSpacer.java.</p>
* <b>Descripción:</b><p> Entidad JPA carátula.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA carátula.
 * <p>
 * Clase ScanWorkBooking.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
@Entity
@Table(name = "DOCS_BATCH_SPACER", uniqueConstraints = { @UniqueConstraint(columnNames = { "SPACER_ID" }) })
public class DocsBatchSpacer implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "DOCS_BATCH_SPACER_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "DOCS_BATCH_SPACER_ID_SEQ", sequenceName = "DOCS_BATCH_SPACER_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo SPACER_ID de la entidad.
	 */
	@Column(name = "SPACER_ID")
	private String spacerId;

	/**
	 * Atributo COD_TYPE de la entidad.
	 */
	@Column(name = "COD_TYPE")
	@Enumerated(EnumType.STRING)
	private BatchSpacerType spacerType;

	/**
	 * Atributo SPACER_GEN_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SPACER_GEN_DATE")
	private Date generationDate;

	/**
	 * Atributo SPACER_STATUS de la entidad.
	 */
	@Column(name = "SPACER_STATUS")
	@Enumerated(EnumType.STRING)
	private SpacerStatus spacerStatus;

	/**
	 * Atributo SPACER_STATUS_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SPACER_STATUS_DATE")
	private Date spacerStatusDate;

	/**
	 * Atributo SCAN_PROFILE_ID de la entidad.
	 */
	@Column(name = "SCAN_PROFILE_ID")
	private Long scanProfileId;

	/**
	 * Atributo DOC_SPEC_ID de la entidad.
	 */
	@Column(name = "DOC_SPEC_ID")
	private Long docSpecId;

	/**
	 * Atributo DOC_SPACER_TYPE de la entidad.
	 */
	@Column(name = "DOC_SPACER_TYPE")
	@Enumerated(EnumType.STRING)
	private DocSpacerType docSpacerType;

	/**
	 * Atributo BATCH_DOC_SIZE de la entidad.
	 */
	@Column(name = "BATCH_DOC_SIZE")
	private Integer batchDocSize;

	/**
	 * Atributo BATCH_PAGES de la entidad.
	 */
	@Column(name = "BATCH_PAGES")
	private Integer batchPages;

	/**
	 * Atributo REQ_USER de la entidad.
	 */
	@Column(name = "REQ_USER")
	private String reqUserId;

	/**
	 * Atributo DEPARTMENT de la entidad.
	 */
	@Column(name = "DEPARTMENT")
	private String department;

	/**
	 * Atributo AUDIT_OP_ID de la entidad.
	 */
	@Column(name = "AUDIT_OP_ID")
	private Long auditOpId;

	/**
	 * Relación de entidades documento del lote.
	 */
	@OneToMany(mappedBy = "batchSpacer", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.spacers.model.domain.SpacerDoc.class)
	private List<SpacerDoc> documents = new ArrayList<SpacerDoc>();

	/**
	 * Constructor sin argumentos.
	 */
	public DocsBatchSpacer() {

	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo SPACER_ID de la entidad.
	 * 
	 * @return el atributo SPACER_ID de la entidad.
	 */
	public String getSpacerId() {
		return spacerId;
	}

	/**
	 * Establece un nuevo valor para el atributo SPACER_ID de la entidad.
	 * 
	 * @param aSpacerId
	 *            nuevo valor para el atributo SPACER_ID de la entidad.
	 */
	public void setSpacerId(String aSpacerId) {
		this.spacerId = aSpacerId;
	}

	/**
	 * Obtiene el atributo COD_TYPE de la entidad.
	 * 
	 * @return el atributo COD_TYPE de la entidad.
	 */
	public BatchSpacerType getSpacerType() {
		return spacerType;
	}

	/**
	 * Establece un nuevo valor para el atributo COD_TYPE de la entidad.
	 * 
	 * @param aSpacerType
	 *            nuevo valor para el atributo COD_TYPE de la entidad.
	 */
	public void setSpacerType(BatchSpacerType aSpacerType) {
		this.spacerType = aSpacerType;
	}

	/**
	 * Obtiene el atributo SPACER_GEN_DATE de la entidad.
	 * 
	 * @return el atributo SPACER_GEN_DATE de la entidad.
	 */
	public Date getGenerationDate() {
		return generationDate;
	}

	/**
	 * Establece un nuevo valor para el atributo SPACER_GEN_DATE de la entidad.
	 * 
	 * @param aGenerationDate
	 *            nuevo valor para el atributo SPACER_GEN_DATE de la entidad.
	 */
	public void setGenerationDate(Date aGenerationDate) {
		this.generationDate = aGenerationDate;
	}

	/**
	 * Obtiene el atributo SPACER_STATUS de la entidad.
	 * 
	 * @return el atributo SPACER_STATUS de la entidad.
	 */
	public SpacerStatus getSpacerStatus() {
		return spacerStatus;
	}

	/**
	 * Establece un nuevo valor para el atributo SPACER_STATUS de la entidad.
	 * 
	 * @param aSpacerStatus
	 *            nuevo valor para el atributo SPACER_STATUS de la entidad.
	 */
	public void setSpacerStatus(SpacerStatus aSpacerStatus) {
		this.spacerStatus = aSpacerStatus;
	}

	/**
	 * Obtiene el atributo SPACER_STATUS_DATE de la entidad.
	 * 
	 * @return el atributo SPACER_STATUS_DATE de la entidad.
	 */
	public Date getSpacerStatusDate() {
		return spacerStatusDate;
	}

	/**
	 * Establece un nuevo valor para el atributo SPACER_STATUS_DATE de la
	 * entidad.
	 * 
	 * @param aSpacerStatusDate
	 *            nuevo valor para el atributo SPACER_STATUS_DATE de la entidad.
	 */
	public void setSpacerStatusDate(Date aSpacerStatusDate) {
		this.spacerStatusDate = aSpacerStatusDate;
	}

	/**
	 * Obtiene el atributo SCAN_PROFILE_ID de la entidad.
	 * 
	 * @return el atributo SCAN_PROFILE_ID de la entidad.
	 */
	public Long getScanProfileId() {
		return scanProfileId;
	}

	/**
	 * Establece un nuevo valor para el atributo SCAN_PROFILE_ID de la entidad.
	 * 
	 * @param aScanProfileId
	 *            nuevo valor para el atributo SCAN_PROFILE_ID de la entidad.
	 */
	public void setScanProfileId(Long aScanProfileId) {
		this.scanProfileId = aScanProfileId;
	}

	/**
	 * Obtiene el atributo DOC_SPEC_ID de la entidad.
	 * 
	 * @return el atributo DOC_SPEC_ID de la entidad.
	 */
	public Long getDocSpecId() {
		return docSpecId;
	}

	/**
	 * Establece un nuevo valor para el atributo DOC_SPEC_ID de la entidad.
	 * 
	 * @param aDocSpecId
	 *            nuevo valor para el atributo DOC_SPEC_ID de la entidad.
	 */
	public void setDocSpecId(Long aDocSpecId) {
		this.docSpecId = aDocSpecId;
	}

	/**
	 * Obtiene el atributo DOC_SPACER_TYPE de la entidad.
	 * 
	 * @return el atributo DOC_SPACER_TYPE de la entidad.
	 */
	public DocSpacerType getDocSpacerType() {
		return docSpacerType;
	}

	/**
	 * Establece un nuevo valor para el atributo DOC_SPACER_TYPE de la entidad.
	 * 
	 * @param aDocSpacerType
	 *            nuevo valor para el atributo DOC_SPACER_TYPE de la entidad.
	 */
	public void setDocSpacerType(DocSpacerType aDocSpacerType) {
		this.docSpacerType = aDocSpacerType;
	}

	/**
	 * Obtiene el atributo BATCH_DOC_SIZE de la entidad.
	 * 
	 * @return el atributo BATCH_DOC_SIZE de la entidad.
	 */
	public Integer getBatchDocSize() {
		return batchDocSize;
	}

	/**
	 * Establece un nuevo valor para el atributo BATCH_DOC_SIZE de la entidad.
	 * 
	 * @param aBatchDocSize
	 *            nuevo valor para el atributo BATCH_DOC_SIZE de la entidad.
	 */
	public void setBatchDocSize(Integer aBatchDocSize) {
		this.batchDocSize = aBatchDocSize;
	}

	/**
	 * Obtiene el atributo BATCH_PAGES de la entidad.
	 * 
	 * @return el atributo BATCH_PAGES de la entidad.
	 */
	public Integer getBatchPages() {
		return batchPages;
	}

	/**
	 * Establece un nuevo valor para el atributo BATCH_PAGES de la entidad.
	 * 
	 * @param aBatchPages
	 *            nuevo valor para el atributo BATCH_PAGES de la entidad.
	 */
	public void setBatchPages(Integer aBatchPages) {
		this.batchPages = aBatchPages;
	}

	/**
	 * Obtiene el atributo REQ_USER de la entidad.
	 * 
	 * @return el atributo REQ_USER de la entidad.
	 */
	public String getReqUserId() {
		return reqUserId;
	}

	/**
	 * Establece un nuevo valor para el atributo REQ_USER de la entidad.
	 * 
	 * @param aReqUserId
	 *            nuevo valor para el atributo REQ_USER de la entidad.
	 */
	public void setReqUserId(String aReqUserId) {
		this.reqUserId = aReqUserId;
	}

	/**
	 * Obtiene el atributo DEPARTMENT de la entidad.
	 * 
	 * @return el atributo DEPARTMENT de la entidad.
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Establece un nuevo valor para el atributo DEPARTMENT de la entidad.
	 * 
	 * @param aDepartment
	 *            nuevo valor para el atributo DEPARTMENT de la entidad.
	 */
	public void setDepartment(String aDepartment) {
		this.department = aDepartment;
	}

	/**
	 * Obtiene el atributo AUDIT_OP_ID de la entidad.
	 * 
	 * @return el atributo AUDIT_OP_ID de la entidad.
	 */
	public Long getAuditOpId() {
		return auditOpId;
	}

	/**
	 * Establece un nuevo valor para el atributo AUDIT_OP_ID de la entidad.
	 * 
	 * @param anAuditOpId
	 *            nuevo valor para el atributo AUDIT_OP_ID de la entidad.
	 */
	public void setAuditOpId(Long anAuditOpId) {
		this.auditOpId = anAuditOpId;
	}

	/**
	 * Obtiene la relación de documentos referenciados en la carátula.
	 * 
	 * @return la relación de documentos referenciados en la carátula.
	 */
	public List<SpacerDoc> getDocuments() {
		return documents;
	}

	/**
	 * Establece la relación de documentos referenciados en la carátula.
	 * 
	 * @param anyDocuments
	 *            nueva relación de documentos referenciados en la carátula.
	 */
	public void setDocuments(List<SpacerDoc> anyDocuments) {
		this.documents.clear();

		if (anyDocuments != null && !anyDocuments.isEmpty()) {
			this.documents.addAll(anyDocuments);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocsBatchSpacer other = (DocsBatchSpacer) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
