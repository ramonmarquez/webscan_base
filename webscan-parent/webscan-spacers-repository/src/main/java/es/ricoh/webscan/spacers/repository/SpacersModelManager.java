/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.SpacersModelManager.java.</p>
* <b>Descripción:</b><p> Fachada de gestión y consulta sobre las entidades pertenecientes al modelo de datos de generación de carátulas y
* separadores reutilizables.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.spacers.model.domain.SpacerStatus;
import es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerDocDTO;
import es.ricoh.webscan.spacers.model.dto.SpacerMetadataDTO;
import es.ricoh.webscan.spacers.repository.dao.SpacersDAO;
import es.ricoh.webscan.WebscanException;

/**
 * Fachada de gestión y consulta sobre las entidades pertenecientes al modelo de
 * datos de generación de carátulas y separadores reutilizables.
 * <p>
 * Clase SpacersModelManager.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class SpacersModelManager {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SpacersModelManager.class);

	/**
	 * DAO sobre las entidades pertenecientes al modelo de datos de generación
	 * de carátulas y separadores reutilizables.
	 */
	private SpacersDAO spacersDAO;

	/*************************************************************************************/
	/*						Reservas de trabajo de digitalización				 		 */
	/*************************************************************************************/

	/**
	 * Recupera una carátula en estado generada a partir de su identificador
	 * normalizado.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador normalizado de carátula.
	 * @return Carátula solicitada.
	 * @throws SpacersDAOException
	 *             Si no existe una carátula en estado generada con el
	 *             identificador especificado, existe más de una carátula, se
	 *             produce algún error en la consulta a BBDD, o los parámetros
	 *             de entrada no son válidos.
	 */
	public DocsBatchSpacerDTO getGeneratedDocsBatchSpacer(String docsBatchSpacerId) throws SpacersDAOException {
		DocsBatchSpacerDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia la recuperación de la carátula, con estado generada, con identificador normalizado {} ...", docsBatchSpacerId);
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(docsBatchSpacerId);
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Ejecutando la consulta en BBDD ...");
		res = spacersDAO.getDocsBatchSpacerByStandardId(docsBatchSpacerId);

		if (!SpacerStatus.GENERATED.equals(res.getSpacerStatus()) || !!SpacerStatus.IN_PROCESS.equals(res.getSpacerStatus())) {
			throw new SpacersDAOException(SpacersDAOException.CODE_901, "[WEBSCAN-SPACERS-MODEL] No existe carátula generada con identificador normalizado " + docsBatchSpacerId + ".");
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Carátula {} recuperada de BBDD.", res.getId());
		}
		return res;
	}

	/**
	 * Recupera una carátula activa a partir de su identificador normalizado.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador normalizado de carátula.
	 * @return Carátula solicitada.
	 * @throws SpacersDAOException
	 *             Si no existe una carátula en estado generada con el
	 *             identificador especificado, existe más de una carátula, se
	 *             produce algún error en la consulta a BBDD, o los parámetros
	 *             de entrada no son válidos.
	 */
	public DocsBatchSpacerDTO getActiveDocsBatchSpacer(String docsBatchSpacerId) throws SpacersDAOException {
		DocsBatchSpacerDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia la recuperación de la carátula, con estado generada, con identificador normalizado {} ...", docsBatchSpacerId);
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(docsBatchSpacerId);
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Ejecutando la consulta en BBDD ...");
		res = spacersDAO.getDocsBatchSpacerByStandardId(docsBatchSpacerId);

		if (!SpacerStatus.GENERATED.equals(res.getSpacerStatus()) && !SpacerStatus.IN_PROCESS.equals(res.getSpacerStatus())) {
			throw new SpacersDAOException(SpacersDAOException.CODE_901, "[WEBSCAN-SPACERS-MODEL] No existe carátula activa con identificador normalizado " + docsBatchSpacerId + ".");
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Carátula {} recuperada de BBDD.", res.getId());
		}
		return res;
	}

	/**
	 * Recupera una carátula activa a partir de su identificador normalizado.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @return Carátula solicitada.
	 * @throws SpacersDAOException
	 *             Si no existe una carátula en estado generada con el
	 *             identificador especificado, existe más de una carátula, se
	 *             produce algún error en la consulta a BBDD, o los parámetros
	 *             de entrada no son válidos.
	 */
	public DocsBatchSpacerDTO getActiveDocsBatchSpacer(Long docsBatchSpacerId) throws SpacersDAOException {
		DocsBatchSpacerDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia la recuperación de la carátula, con estado generada, con identificador {} ...", docsBatchSpacerId);
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(docsBatchSpacerId);
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Ejecutando la consulta en BBDD ...");
		res = spacersDAO.getDocsBatchSpacer(docsBatchSpacerId);

		if (!SpacerStatus.GENERATED.equals(res.getSpacerStatus()) && !SpacerStatus.IN_PROCESS.equals(res.getSpacerStatus())) {
			throw new SpacersDAOException(SpacersDAOException.CODE_901, "[WEBSCAN-SPACERS-MODEL] No existe carátula activa con identificador " + docsBatchSpacerId + ".");
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Carátula {} recuperada de BBDD.", res.getId());
		}
		return res;
	}

	/**
	 * Recupera una carátula a partir de su identificador normalizado.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador normalizado de carátula.
	 * @return Carátula solicitada.
	 * @throws SpacersDAOException
	 *             Si no existe una carátula con el identificador especificado,
	 *             existe más de una carátula, se produce algún error en la
	 *             consulta a BBDD, o los parámetros de entrada no son válidos.
	 */
	public DocsBatchSpacerDTO getDocsBatchSpacerBySpacerId(String docsBatchSpacerId) throws SpacersDAOException {
		DocsBatchSpacerDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia la recuperación de la carátula, con estado generada, con identificador normalizado " + docsBatchSpacerId + " ...");
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(docsBatchSpacerId);
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Ejecutando la consulta en BBDD ...");
		res = spacersDAO.getDocsBatchSpacerByStandardId(docsBatchSpacerId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Carátula " + res.getId() + " recuperada de BBDD.");
		}
		return res;
	}

	/**
	 * Obtiene el número de documentos de una carátula.
	 * 
	 * @param spacerId
	 *            Identificador de carátula.
	 * @return Número de documentos de una carátula.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, o se produce algún error al
	 *             ejecutar la consulta en BBDD.
	 */
	public Integer getDocsBatchSpacerNumberOfDocs(Long spacerId) throws SpacersDAOException {
		Integer res = 0;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-BOOKING-MODEL] Se inicia la recuperación del número de documentos de la carátula {} ...", spacerId);
		}

		LOG.debug("[WEBSCAN-BOOKING-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(spacerId);
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}

		LOG.debug("[WEBSCAN-BOOKING-MODEL] Ejecutando la consulta en BBDD ...");
		res = spacersDAO.getDocsBatchSpacerNumberOfDocs(spacerId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-BOOKING-MODEL] La carátula {} tiene {} documentos.", spacerId, res);
		}

		return res;
	}

	/**
	 * Crea una nueva carátula en el modelo de datos, estableciéndole el estado
	 * generada.
	 * 
	 * @param docsBatchSpacer
	 *            Carátula.
	 * @param documents
	 *            Relación de documentos que conforman el lote identificado por
	 *            la carátula.
	 * @return la carátula separadora de lote de documentos creada.
	 * @throws SpacersDAOException
	 *             Si los datos no son válidos, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	public DocsBatchSpacerDTO createDocsBatchSpacer(DocsBatchSpacerDTO docsBatchSpacer, Map<SpacerDocDTO, List<SpacerMetadataDTO>> documents) throws SpacersDAOException {
		Date now;
		Long dbsId;
		DocsBatchSpacerDTO res = new DocsBatchSpacerDTO();

		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia el registro de una nueva carátula en el Sistema ...");
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");

		DataValidationUtilities.checkDocsBatchSpacer(docsBatchSpacer, Boolean.TRUE);
		DataValidationUtilities.checkSpacerDocsAndMetadataCollection(documents, Boolean.TRUE);

		LOG.debug("[WEBSCAN-SPACERS-MODEL] Registrando la carátula y sus metadatos ...");
		now = new Date();

		res.setBatchDocSize(docsBatchSpacer.getBatchDocSize());
		res.setBatchPages(docsBatchSpacer.getBatchPages());
		res.setDepartment(docsBatchSpacer.getDepartment());
		res.setDocSpacerType(docsBatchSpacer.getDocSpacerType());
		res.setDocSpecId(docsBatchSpacer.getDocSpecId());
		res.setGenerationDate(now);
		res.setReqUserId(docsBatchSpacer.getReqUserId());
		res.setScanProfileId(docsBatchSpacer.getScanProfileId());
		res.setSpacerId(UUID.randomUUID().toString());
		res.setSpacerStatus(SpacerStatus.GENERATED);
		res.setSpacerStatusDate(now);
		res.setSpacerType(docsBatchSpacer.getSpacerType());

		dbsId = spacersDAO.createDocsBatchSpacer(res, documents);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Carátula " + dbsId + " registrada en el Sistema ...");
		}

		res = spacersDAO.getDocsBatchSpacer(dbsId);

		return res;
	}

	/**
	 * Inicia el procesamiento de una carátula.
	 * 
	 * @param batchSpacerId
	 *            Identificador de carátula.
	 * @param startDate
	 *            Instante en el que es iniciado el trabajo de digitalización
	 *            mediante el que es procesado el lote identificado por la
	 *            carátula.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, no se encuentra en el estado
	 *             solicitada, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void processBatchSpacer(Long batchSpacerId, Date startDate) throws SpacersDAOException {
		DocsBatchSpacerDTO batchSpacer;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia el procesamiento de la carátula " + batchSpacerId + " ...");
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkIdentifier(batchSpacerId);
			DataValidationUtilities.checkEmptyObject(startDate, "[WEBSCAN-SPACERS-MODEL] Fecha de inicio de procesamiento de la carátula no especificada.");
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Recuperando la carátula " + batchSpacerId + " de BBDD ...");
		}
		batchSpacer = spacersDAO.getDocsBatchSpacer(batchSpacerId);

		if (!SpacerStatus.GENERATED.equals(batchSpacer.getSpacerStatus())) {
			throw new SpacersDAOException(SpacersDAOException.CODE_998, "[WEBSCAN-SPACERS-MODEL] Estado de carátula no admitido para ser procesada (" + batchSpacer.getSpacerStatus() + ").");
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Actualizando la carátula " + batchSpacerId + " en el Sistema ...");
		}

		batchSpacer.setSpacerStatus(SpacerStatus.IN_PROCESS);
		batchSpacer.setSpacerStatusDate(startDate);
		spacersDAO.updateDocsBatchSpacer(batchSpacer);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Carátula " + batchSpacerId + " actualizada.");
		}
	}

	/**
	 * Elimina la información de una carátula del modelo de datos.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @param metadataCollection
	 *            Relación de metadatos referenciados en la carátula.
	 * @throws SpacersDAOException
	 *             Si se produce algún error al eliminar la carátula.
	 */
	public void removeDocsBatchSpacer(Long docsBatchSpacerId) throws SpacersDAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia el borrado de la carátula " + docsBatchSpacerId + " ...");
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(docsBatchSpacerId);
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Ejecutando la consulta en BBDD ...");
		spacersDAO.removeDocsBatchSpacer(docsBatchSpacerId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Carátula " + docsBatchSpacerId + " eliminada de BBDD.");
		}

	}

	/**
	 * Recupera los documentos y metadatos de una carátula generada por el
	 * Sistema.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador interno de carátula.
	 * @return Relación de documentos y metadatos referenciados en la carátula.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, o se produce algún error al
	 *             ejecutar la consulta en BBDD.
	 */
	public Map<SpacerDocDTO, List<SpacerMetadataDTO>> getSpacerDocsAndMetadataCollection(Long docsBatchSpacerId) throws SpacersDAOException {
		Map<SpacerDocDTO, List<SpacerMetadataDTO>> res = new HashMap<SpacerDocDTO, List<SpacerMetadataDTO>>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia la recuperación de los documentos de la carátula con identificador " + docsBatchSpacerId + " ...");
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(docsBatchSpacerId);
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Ejecutando la consulta en BBDD ...");
		res = spacersDAO.getSpacerDocsAndMetadataCollection(docsBatchSpacerId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] " + res.size() + " documentos recuperados para la carátula " + docsBatchSpacerId + ".");
		}

		return res;
	}

	/**
	 * Actualiza la información de auditoría de una carátula.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @param auditOpId
	 *            Identificador de traza de auditoría.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, no se encuentra en el estado
	 *             generada, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void setDocsBatchSpacerAuditInfo(Long docsBatchSpacerId, Long auditOpId) throws SpacersDAOException {
		DocsBatchSpacerDTO dbs;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia la actualización de la información de auditoría de la carátula " + docsBatchSpacerId + " ...");
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkIdentifier(docsBatchSpacerId);
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Recuperando la carátula " + docsBatchSpacerId + " de BBDD ...");
		}
		dbs = spacersDAO.getDocsBatchSpacer(docsBatchSpacerId);

		if (!SpacerStatus.GENERATED.equals(dbs.getSpacerStatus()) && !SpacerStatus.IN_PROCESS.equals(dbs.getSpacerStatus())) {
			throw new SpacersDAOException(SpacersDAOException.CODE_999, "[WEBSCAN-SPACERS-MODEL] Estado de carátula no admitido para modificar la información de auditoría (" + dbs.getSpacerStatus() + ").");
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Actualizando la carátula " + docsBatchSpacerId + " en el Sistema ...");
		}
		dbs.setAuditOpId(auditOpId);
		spacersDAO.updateDocsBatchSpacer(dbs);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Información de auditoría de la carátula " + docsBatchSpacerId + " actualizada.");
		}
	}

	/**
	 * Finaliza el trabajo de digitalización asociado a una carátula.
	 * 
	 * @param docsBatchSpacerId
	 *            Identificador de carátula.
	 * @throws SpacersDAOException
	 *             Si no existe la carátula, no se encuentra en el estado
	 *             generada, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void completeDocsBatchSpacer(Long docsBatchSpacerId) throws SpacersDAOException {
		Date now;
		DocsBatchSpacerDTO dbs;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Se inicia la finalización del trabajo de digitalización de la carátula " + docsBatchSpacerId + " ...");
		}
		LOG.debug("[WEBSCAN-SPACERS-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkIdentifier(docsBatchSpacerId);
		} catch (WebscanException e) {
			throw new SpacersDAOException(e.getCode(), e.getMessage(), e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Recuperando la carátula " + docsBatchSpacerId + " de BBDD ...");
		}
		dbs = spacersDAO.getDocsBatchSpacer(docsBatchSpacerId);

		if (!SpacerStatus.IN_PROCESS.equals(dbs.getSpacerStatus())) {
			throw new SpacersDAOException(SpacersDAOException.CODE_998, "[WEBSCAN-SPACERS-MODEL] Estado de carátula no admitido para finalizacion (" + dbs.getSpacerStatus() + ").");
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Actualizando la carátula " + docsBatchSpacerId + " en el Sistema ...");
		}
		now = new Date();
		dbs.setSpacerStatus(SpacerStatus.COMPLETED);
		dbs.setSpacerStatusDate(now);
		spacersDAO.updateDocsBatchSpacer(dbs);
		spacersDAO.removeDocsBatchSpacer(docsBatchSpacerId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-SPACERS-MODEL] Finalizado el trabajo de digitalización asociado a la carátula " + docsBatchSpacerId + ".");
		}
	}

	/**
	 * Establece un nuevo DAO sobre las entidades pertenecientes al modelo de
	 * datos de generación de carátulas y separadores reutilizables.
	 * 
	 * @param aSpacersDAO
	 *            Nuevo DAO sobre las entidades pertenecientes al modelo de
	 *            datos de generación de carátulas y separadores reutilizables.
	 */
	public void setSpacersDAO(SpacersDAO aSpacersDAO) {
		this.spacersDAO = aSpacersDAO;
	}

}
