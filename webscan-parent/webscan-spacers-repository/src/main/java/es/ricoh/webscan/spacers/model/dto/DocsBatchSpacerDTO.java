/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.dto.DocsBatchSpacerDTO.java.</p>
* <b>Descripción:</b><p> DTO para la entidad JPA carátula.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.model.dto;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.model.domain.DocSpacerType;
import es.ricoh.webscan.spacers.model.domain.SpacerStatus;

/**
 * DTO para la entidad JPA carátula.
 * <p>
 * Clase DocsBatchSpacerDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.1.0.
 */
public class DocsBatchSpacerDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador interno de la carátula.
	 */
	private Long id;

	/**
	 * Identificador normalizado de la carátula.
	 */
	private String spacerId;

	/**
	 * Tipo de código de la carátula.
	 */
	private BatchSpacerType spacerType;

	/**
	 * Fecha de generación de la carátula.
	 */
	private Date generationDate;

	/**
	 * Estado de la carátula.
	 */
	private SpacerStatus spacerStatus;

	/**
	 * Fecha de estado de la carátula.
	 */
	private Date spacerStatusDate;

	/**
	 * Identificador de la plantilla de digtialización informado para la
	 * generación de la carátula.
	 */
	private Long scanProfileId;

	/**
	 * Identificador de la definición de documentos informado para la generación
	 * de la carátula.
	 */
	private Long docSpecId;

	/**
	 * Tipo de separador de documentos que deben emplearse en el lote.
	 */
	private DocSpacerType docSpacerType;

	/**
	 * Número de documentos que debe tener el lote.
	 */
	private Integer batchDocSize;

	/**
	 * Número total de páginas que debe tener el lote.
	 */
	private Integer batchPages;

	/**
	 * Identificación del usuario que solicita la generación de la carátula.
	 */
	private String reqUserId;

	/**
	 * Identificador de la unidad orgánica a la que serán asociados los
	 * documentos del lote.
	 */
	private String department;

	/**
	 * Identificador de la traza de auditoría asociado a la carátula.
	 */
	private Long auditOpId;

	/**
	 * Constructor sin argumentos.
	 */
	public DocsBatchSpacerDTO() {

	}

	/**
	 * Obtiene el identificador interno de la carátula.
	 * 
	 * @return el identificador interno de la carátula.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador interno de la carátula.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador interno de la carátula.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el identificador normalizado de la carátula.
	 * 
	 * @return el identificador normalizado de la carátula.
	 */
	public String getSpacerId() {
		return spacerId;
	}

	/**
	 * Establece un nuevo valor para el identificador normalizado de la
	 * carátula.
	 * 
	 * @param aSpacerId
	 *            nuevo valor para el identificador normalizado de la carátula.
	 */
	public void setSpacerId(String aSpacerId) {
		this.spacerId = aSpacerId;
	}

	/**
	 * Obtiene el tipo de código de la carátula.
	 * 
	 * @return el tipo de código de la carátula.
	 */
	public BatchSpacerType getSpacerType() {
		return spacerType;
	}

	/**
	 * Establece un nuevo valor para el tipo de código de la carátula.
	 * 
	 * @param aSpacerType
	 *            nuevo valor para el tipo de código de la carátula.
	 */
	public void setSpacerType(BatchSpacerType aSpacerType) {
		this.spacerType = aSpacerType;
	}

	/**
	 * Obtiene la fecha de generación de la carátula.
	 * 
	 * @return la fecha de generación de la carátula.
	 */
	public Date getGenerationDate() {
		return generationDate;
	}

	/**
	 * Establece un nuevo valor para la fecha de generación de la carátula.
	 * 
	 * @param aGenerationDate
	 *            nuevo valor para la fecha de generación de la carátula.
	 */
	public void setGenerationDate(Date aGenerationDate) {
		this.generationDate = aGenerationDate;
	}

	/**
	 * Obtiene el estado de la carátula.
	 * 
	 * @return el estado de la carátula.
	 */
	public SpacerStatus getSpacerStatus() {
		return spacerStatus;
	}

	/**
	 * Establece un nuevo valor para el estado de la carátula.
	 * 
	 * @param aSpacerStatus
	 *            nuevo valor para el estado de la carátula.
	 */
	public void setSpacerStatus(SpacerStatus aSpacerStatus) {
		this.spacerStatus = aSpacerStatus;
	}

	/**
	 * Obtiene la fecha de estado de la carátula.
	 * 
	 * @return la fecha de estado de la carátula.
	 */
	public Date getSpacerStatusDate() {
		return spacerStatusDate;
	}

	/**
	 * Establece un nuevo valor para la fecha de estado de la carátula.
	 * 
	 * @param aSpacerStatusDate
	 *            nuevo valor para la fecha de estado de la carátula.
	 */
	public void setSpacerStatusDate(Date aSpacerStatusDate) {
		this.spacerStatusDate = aSpacerStatusDate;
	}

	/**
	 * Obtiene el identificador de la plantilla de digtialización informado para
	 * la generación de la carátula.
	 * 
	 * @return el identificador de la plantilla de digtialización informado para
	 *         la generación de la carátula.
	 */
	public Long getScanProfileId() {
		return scanProfileId;
	}

	/**
	 * Establece un nuevo valor para el identificador de la plantilla de
	 * digtialización informado para la generación de la carátula.
	 * 
	 * @param aScanProfileId
	 *            nuevo valor para el identificador de la plantilla de
	 *            digtialización informado para la generación de la carátula.
	 */
	public void setScanProfileId(Long aScanProfileId) {
		this.scanProfileId = aScanProfileId;
	}

	/**
	 * Obtiene el identificador de la definición de documentos informado para la
	 * generación de la carátula.
	 * 
	 * @return el identificador de la definición de documentos informado para la
	 *         generación de la carátula.
	 */
	public Long getDocSpecId() {
		return docSpecId;
	}

	/**
	 * Establece un nuevo valor para el identificador de la definición de
	 * documentos informado para la generación de la carátula.
	 * 
	 * @param aDocSpecId
	 *            nuevo valor para el identificador de la definición de
	 *            documentos informado para la generación de la carátula.
	 */
	public void setDocSpecId(Long aDocSpecId) {
		this.docSpecId = aDocSpecId;
	}

	/**
	 * Obtiene el tipo de separador de documentos que deben emplearse en el
	 * lote.
	 * 
	 * @return el tipo de separador de documentos que deben emplearse en el
	 *         lote.
	 */
	public DocSpacerType getDocSpacerType() {
		return docSpacerType;
	}

	/**
	 * Establece un nuevo valor para el tipo de separador de documentos que
	 * deben emplearse en el lote.
	 * 
	 * @param aDocSpacerType
	 *            nuevo valor para el tipo de separador de documentos que deben
	 *            emplearse en el lote.
	 */
	public void setDocSpacerType(DocSpacerType aDocSpacerType) {
		this.docSpacerType = aDocSpacerType;
	}

	/**
	 * Obtiene el número de documentos que debe tener el lote.
	 * 
	 * @return el número de documentos que debe tener el lote.
	 */
	public Integer getBatchDocSize() {
		return batchDocSize;
	}

	/**
	 * Establece un nuevo valor para el número de documentos que debe tener el
	 * lote.
	 * 
	 * @param aBatchDocSize
	 *            nuevo valor para el número de documentos que debe tener el
	 *            lote.
	 */
	public void setBatchDocSize(Integer aBatchDocSize) {
		this.batchDocSize = aBatchDocSize;
	}

	/**
	 * Obtiene el número total de páginas que debe tener el lote.
	 * 
	 * @return el número total de páginas que debe tener el lote.
	 */
	public Integer getBatchPages() {
		return batchPages;
	}

	/**
	 * Establece un nuevo valor para el número total de páginas que debe tener
	 * el lote.
	 * 
	 * @param aBatchPages
	 *            nuevo valor para el número total de páginas que debe tener el
	 *            lote.
	 */
	public void setBatchPages(Integer aBatchPages) {
		this.batchPages = aBatchPages;
	}

	/**
	 * Obtiene la identificación del usuario que solicita la generación de la
	 * carátula.
	 * 
	 * @return la identificación del usuario que solicita la generación de la
	 *         carátula.
	 */
	public String getReqUserId() {
		return reqUserId;
	}

	/**
	 * Establece un nuevo valor para la identificación del usuario que solicita
	 * la generación de la carátula.
	 * 
	 * @param aReqUserId
	 *            nuevo valor para la identificación del usuario que solicita la
	 *            generación de la carátula.
	 */
	public void setReqUserId(String aReqUserId) {
		this.reqUserId = aReqUserId;
	}

	/**
	 * Obtiene el identificador de la unidad orgánica a la que serán asociados
	 * los documentos del lote.
	 * 
	 * @return el identificador de la unidad orgánica a la que serán asociados
	 *         los documentos del lote.
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Establece un nuevo valor para el identificador de la unidad orgánica a la
	 * que serán asociados los documentos del lote.
	 * 
	 * @param aDepartment
	 *            nuevo valor para el identificador de la unidad orgánica a la
	 *            que serán asociados los documentos del lote.
	 */
	public void setDepartment(String aDepartment) {
		this.department = aDepartment;
	}

	/**
	 * Obtiene el identificador de la traza de auditoría asociado a la carátula.
	 * 
	 * @return el identificador de la traza de auditoría asociado a la carátula.
	 */
	public Long getAuditOpId() {
		return auditOpId;
	}

	/**
	 * Establece un nuevo valor para el identificador de la traza de auditoría
	 * asociado a la carátula.
	 * 
	 * @param anAuditOpId
	 *            nuevo valor para el identificador de la traza de auditoría
	 *            asociado a la carátula.
	 */
	public void setAuditOpId(Long anAuditOpId) {
		this.auditOpId = anAuditOpId;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocsBatchSpacerDTO other = (DocsBatchSpacerDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
