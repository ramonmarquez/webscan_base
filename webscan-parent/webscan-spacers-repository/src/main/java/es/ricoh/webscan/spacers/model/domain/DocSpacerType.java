/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.model.domain.DocSpacerType.java.</p>
* <b>Descripción:</b><p> Tipos de codificación de la información incluidas en separadores de documentos.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.model.domain;

/**
 * Tipos de codificación de la información incluidas en separadores de
 * documentos.
 * <p>
 * Clase DocSpacerType.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum DocSpacerType {

	/**
	 * Página en blanco.
	 */
	BLANK_PAGE("BLANK PAGE"),
	/**
	 * Códgio QR.
	 */
	CODE_QR("CODE QR"),
	/**
	 * Código 128.
	 */
	CODE_128("CODE 128"),
	/**
	 * PDF 417.
	 */
	PDF_417("PDF 417");

	/**
	 * Denominación del tipo de codificación.
	 */
	private String name;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aStatus
	 *            Estado de reserva de trabajo de digitalización.
	 */
	DocSpacerType(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo status.
	 * 
	 * @return el atributo status.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Recupera un tipo de codificación a partir de su denominación.
	 * 
	 * @param aName
	 *            Denominación de tipo de codificación.
	 * @return un tipo de codificación. En caso de no encontrarse, devuelve
	 *         null.
	 */
	public static DocSpacerType getByName(String aName) {
		Boolean found = Boolean.FALSE;
		DocSpacerType res = null;
		DocSpacerType[ ] codeTypes = values();

		for (int i = 0; !found && i < codeTypes.length; i++) {
			if (codeTypes[i].getName().equals(aName)) {
				res = codeTypes[i];
				found = Boolean.TRUE;
			}
		}

		return res;
	}
}
