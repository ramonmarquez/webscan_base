<!--
  #%L
  Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
  %%
  Copyright (C) 2017 Ricoh Spain IT Services
  %%
  Este fichero forma parte de la solución base de digitalización certificada y 
  copia auténtica, Webscan 2. 
  
  Este fichero se distribuye bajo licencia propia Webscan 2, estando las
  condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
  fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
  #L%
  -->

<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:tx="http://www.springframework.org/schema/tx"
	xmlns:util="http://www.springframework.org/schema/util" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="
		http://www.springframework.org/schema/aop 
		http://www.springframework.org/schema/aop/spring-aop.xsd
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd 
        http://www.springframework.org/schema/tx 
        http://www.springframework.org/schema/tx/spring-tx.xsd 
		http://www.springframework.org/schema/util 
		http://www.springframework.org/schema/util/spring-util.xsd">

	<!-- Parámetros de configuración del componente -->
	<util:properties id="usersParamConfiguration" location="${env.config.users.location}"/>
	<!-- Fin Parámetros de configuración del componente -->

	<!-- Validadores de objetos de la capa de presentación -->
	<bean id="passwordMatchesValidator" class="es.ricoh.webscan.users.validation.PasswordMatchesValidator"/>

	<bean id="passwordPatternValidator" class="es.ricoh.webscan.users.validation.PasswordPatternValidator">
		<property name="passwordPattern" value="${password.pattern}" />
	</bean>
	
	<bean id="retrievePasswordValidator" class="es.ricoh.webscan.users.validation.RetrievePasswordValidator">
		<property name="passwordMatchesValidator" ref="passwordMatchesValidator" />
		<property name="passwordPatternValidator" ref="passwordPatternValidator" />
	</bean>
	
	<bean id="oldPasswordValidator" class="es.ricoh.webscan.users.validation.OldPasswordValidator">
		<property name="loginService" ref="loginService"/>
		<property name="passwordEncoder" ref="passwordEncoder"/>
	</bean>
	
	<bean id="updatePasswordValidator" class="es.ricoh.webscan.users.validation.UpdatePasswordValidator">
		<property name="oldPasswordValidator" ref="oldPasswordValidator"/>
		<property name="passwordMatchesValidator" ref="passwordMatchesValidator" />
		<property name="passwordPatternValidator" ref="passwordPatternValidator" />
	</bean>
	
	<bean id="emailPatternValidator" class="es.ricoh.webscan.users.validation.EmailPatternValidator"/>

	<bean id="reqRetrievePasswordValidator" class="es.ricoh.webscan.users.validation.ReqRetrievePasswordValidator">
		<property name="emailPatternValidator" ref="emailPatternValidator"/>
	</bean>

	<bean id="oldPasswordNotMatchValidator" class="es.ricoh.webscan.users.validation.OldPasswordNotMatchValidator">
		<property name="loginService" ref="loginService"/>
		<property name="passwordEncoder" ref="passwordEncoder"/>
	</bean>

	<bean id="userVoValidator" class="es.ricoh.webscan.users.validation.UserVoValidator">		
		<property name="passwordMatchesValidator" ref="passwordMatchesValidator" />
		<property name="passwordPatternValidator" ref="passwordPatternValidator" />
		<property name="oldPasswordNotMatchValidator" ref="oldPasswordNotMatchValidator" />
		<property name="emailPatternValidator" ref="emailPatternValidator"/>
	</bean>

	<bean id="userInSessionValidator" class="es.ricoh.webscan.users.validation.UserInSessionValidator">		
		<property name="passwordMatchesValidator" ref="passwordMatchesValidator" />
		<property name="passwordPatternValidator" ref="passwordPatternValidator" />
		<property name="oldPasswordValidator" ref="oldPasswordValidator"/>
		<property name="emailPatternValidator" ref="emailPatternValidator"/>
	</bean>

	<!-- Fin Validadores de objetos de la capa de presentación -->

	<!-- Listeners -->
	<bean id="userInSessionListener" class="es.ricoh.webscan.users.listener.UserInSessionListener">
		<property name="userService" ref="userService"/>
	</bean>
    <!-- Fin Listeners -->
    
    <!-- Capa de servicios -->
    <bean id="linkUserService" name="linkUserService" class="es.ricoh.webscan.users.service.LinkUserService">
    	<property name="usersWebscanModelManager" ref="usersWebscanModelManager"/>
    </bean>
    
    <!-- Fin capa de servicios -->
    <!-- Configuración del gestor de transacciones JTA -->

	<aop:config>
		<aop:pointcut expression="execution(public * *..LinkUserService.*(..))" id="linkUserServicePointcut"/>
		<aop:advisor advice-ref="linkUserServiceTxAdvice" pointcut-ref="linkUserServicePointcut"/>
	</aop:config>

	<tx:advice id="linkUserServiceTxAdvice" transaction-manager="webscanTransactionManager" >
		<tx:attributes>
			<!-- Consultas -->
			<tx:method name="getUser*" read-only="true" propagation="REQUIRED" isolation="READ_COMMITTED" no-rollback-for="es.ricoh.webscan.users.model.UsersDAOException"/>			
		</tx:attributes>
	</tx:advice>

	<aop:config>
		<aop:pointcut expression="execution(public * *..LoginService.*(..))" id="loginServicePointcut"/>
		<aop:advisor advice-ref="loginServiceTxAdvice" pointcut-ref="loginServicePointcut"/>
	</aop:config>

	<tx:advice id="loginServiceTxAdvice" transaction-manager="webscanTransactionManager" >
		<tx:attributes>
			<!-- Consultas -->
			<tx:method name="getUser*" read-only="true" propagation="REQUIRED" isolation="READ_COMMITTED" no-rollback-for="es.ricoh.webscan.users.model.UsersDAOException"/>
			<!-- Inserciones, actualizaciones y borrados -->			
			<tx:method name="initiateRecoveryUserPasswordRequest" read-only="false" propagation="REQUIRED" isolation="READ_COMMITTED" rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException,es.ricoh.webscan.utilities.WebscanUtilitiesException"/>
			<tx:method name="logLoginFailed" read-only="false" propagation="REQUIRED" isolation="READ_COMMITTED" rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException"/>
			<tx:method name="performUpdateUserPassword" read-only="false" propagation="REQUIRED" isolation="READ_COMMITTED" rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException"/>
			<tx:method name="performRecoveryUserPasswordRequest" read-only="false" propagation="REQUIRED" isolation="READ_COMMITTED" rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException"/>
			<tx:method name="saveAuthenticationSuccess" read-only="false" propagation="REQUIRED" isolation="READ_COMMITTED" rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException"/>
		</tx:attributes>
	</tx:advice>

	<aop:config>
		<aop:pointcut expression="execution(public * *..UserService.*(..))" id="userServicePointcut"/>
		<aop:advisor advice-ref="userServiceTxAdvice" pointcut-ref="userServicePointcut"/>
	</aop:config>

	<tx:advice id="userServiceTxAdvice" transaction-manager="webscanTransactionManager" >
		<tx:attributes>
			<!-- Consultas -->
			<tx:method name="get*" read-only="true" propagation="REQUIRED" isolation="READ_COMMITTED" no-rollback-for="java.lang.Exception"/>
			<tx:method name="updateUserInSession" read-only="true" propagation="REQUIRED" isolation="READ_COMMITTED" no-rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException"/>
			<!-- Inserciones, actualizaciones y borrados -->
			<tx:method name="performSaveUser" read-only="false" propagation="REQUIRED" isolation="READ_COMMITTED" rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException"/>
			<tx:method name="performUpdateUserSession" read-only="false" propagation="REQUIRED" isolation="READ_COMMITTED" rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException"/>
			<tx:method name="removeUsers" read-only="false" propagation="REQUIRED" isolation="READ_COMMITTED" rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException"/>
			<tx:method name="setUnitOrganization" read-only="false" propagation="REQUIRED" isolation="READ_COMMITTED" rollback-for="es.ricoh.webscan.users.model.UsersDAOException,es.ricoh.webscan.model.DAOException"/>
		</tx:attributes>
	</tx:advice>

	<!-- Fin Configuración gestor de transacciones -->
    
</beans>
