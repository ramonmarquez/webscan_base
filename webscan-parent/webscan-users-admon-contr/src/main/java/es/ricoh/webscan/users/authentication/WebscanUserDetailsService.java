/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.authentication.WebscanUserDetailsService.java.</p>
 * <b>Descripción:</b><p> Servicio de recuperación de información de usuarios registrados en el repositorio interno de
 * usuarios de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.service.LoginService;

/**
 * Servicio de recuperación de información de usuarios registrados en el
 * repositorio interno de usuarios de Webscan.
 * <p>
 * Class WebscanUserDetailsService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class WebscanUserDetailsService implements UserDetailsService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WebscanUserDetailsService.class);

	/**
	 * Servicio que agrupa las distintas funcionalidades de inicio de sesión y
	 * modificación de password de usuario.
	 */
	private LoginService loginService;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetails res = null;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-AUTH] Recuperando credenciales y perfiles del usuario " + username + " de BBDD...");
			}

			res = loginService.getUserDetailsByUsername(username);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-AUTH] Credenciales y perfiles del usuario " + username + " recuperados de BBDD...");
			}
		} catch (UsersDAOException e) {
			String excMsg = "[WEBSCAN-AUTH] Se produjo un error al autenticar el usuario " + username + "(" + e.getCode() + "):" + e.getMessage();
			LOG.warn(excMsg);
			throw new UsernameNotFoundException(excMsg, e);
		}

		return res;
	}

	/**
	 * Establece un nuevo servicio que agrupa las distintas funcionalidades de
	 * inicio de sesión y modificación de password de usuario.
	 * 
	 * @param aLoginService
	 *            nuevo servicio que agrupa las distintas funcionalidades de
	 *            inicio de sesión y modificación de password de usuario.
	 */
	public void setLoginService(LoginService aLoginService) {
		this.loginService = aLoginService;
	}

}
