/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.users.listener.UserInSessionListener.java.</p>
* <b>Descripción:</b><p> Clase responsable de mantener actualizada la información registrada en sesión del usuario, indicador de bloqueo o unidades organizativas.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.users.listener;

import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationListener;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.RequestHandledEvent;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.users.UsersWebscanConstants;
import es.ricoh.webscan.users.service.UserService;

/**
 * Clase responsable de mantener actualizada la información registrada en sesión
 * del usuario, indicador de bloqueo, unidades organizativas y perfiles de
 * digitalización.
 * <p>
 * Clase UserInSessionListener.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UserInSessionListener implements ApplicationListener<RequestHandledEvent> {

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre usuarios del sistema.
	 */
	private UserService userService;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(RequestHandledEvent event) {
		// RequestContextListener or RequestContextFilter

		ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

		HttpSession session = attrs.getRequest().getSession(false);

		try {
			if (session != null && session.getId().equals(event.getSessionId())) {
				UserInSession user = BaseControllerUtils.getUserInSession(attrs.getRequest());

				if (user != null) {
					user = userService.updateUserInSession(user.getId());
					session.setAttribute(WebscanBaseConstants.USER_SESSION_ATT_NAME, user);
				}

			}
		} catch (DAOException e) {
			String[ ] args = new String[1];
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(UsersWebscanConstants.GET_SESSION_USER_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.SESSION, attrs.getRequest());
		}
	}

	/**
	 * Establece el servicio que agrupa operaciones de consulta, creación y
	 * modificación sobre usuarios del sistema.
	 * 
	 * @param anUserService
	 *            nuevo servicio que agrupa operaciones de consulta, creación y
	 *            modificación sobre usuarios del sistema.
	 */
	public void setUserService(UserService anUserService) {
		this.userService = anUserService;
	}

}
