/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.validation.RetrievePasswordValidator.java.</p>
 * <b>Descripción:</b><p> Clase que valida el formulario de confirmación de restablecimiento de password de usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import es.ricoh.webscan.users.controller.vo.RetrievePasswordVO;

/**
 * Clase que valida el formulario de confirmación de restablecimiento de
 * password de usuario.
 * <p>
 * Clase RetrievePasswordValidator.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class RetrievePasswordValidator implements Validator {

	/**
	 * Comprueba que la password y su confirmación coinciden.
	 */
	private PasswordMatchesValidator passwordMatchesValidator;

	/**
	 * Comprueba que la password informada coincide con el patrón configurado a
	 * nivel de Sistema.
	 */
	private PasswordPatternValidator passwordPatternValidator;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return RetrievePasswordVO.class.isAssignableFrom(clazz);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 *      org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		RetrievePasswordVO retrievePassword = (RetrievePasswordVO) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "error.username.required", "Username is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "document", "error.document.required", "Document is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordForm.password", "error.password.required", "Password is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordForm.confirmPassword", "error.confirmPassword.required", "Confirm password is required.");
		ValidationUtils.invokeValidator(passwordMatchesValidator, retrievePassword.getPasswordForm(), errors);
		ValidationUtils.invokeValidator(passwordPatternValidator, retrievePassword.getPasswordForm(), errors);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "securityToken", "error.securityToken.required", "SecurityToken is required.");
	}

	/**
	 * Establece un nuevo validador que comprueba que la password y su
	 * confirmación coinciden.
	 * 
	 * @param aPasswordMatchesValidator
	 *            nuevo validador que comprueba que la password y su
	 *            confirmación coinciden.
	 */
	public void setPasswordMatchesValidator(PasswordMatchesValidator aPasswordMatchesValidator) {
		this.passwordMatchesValidator = aPasswordMatchesValidator;
	}

	/**
	 * Establece un nuevo validador que comprueba que la password informada
	 * coincide con el patrón configurado a nivel de Sistema.
	 * 
	 * @param aPasswordPatternValidator
	 *            nuevo validador comprueba que la password informada coincide
	 *            con el patrón configurado a nivel de Sistema.
	 */
	public void setPasswordPatternValidator(PasswordPatternValidator aPasswordPatternValidator) {
		this.passwordPatternValidator = aPasswordPatternValidator;
	}

}
