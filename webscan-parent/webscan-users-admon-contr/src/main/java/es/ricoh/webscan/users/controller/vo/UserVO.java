/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.users.controller.vo.UserVO.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que representa un usuario registrado en el sistema.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/

package es.ricoh.webscan.users.controller.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.ricoh.webscan.base.controller.vo.OrganizationUnitVO;

/**
 * Objeto de la capa de presentación que representa un usuario registrado en el
 * sistema.
 * <p>
 * Clase UserVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UserVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador del perfil de usuario.
	 */
	private Long id = null;

	/**
	 * Nombre de usuario.
	 */
	private String username;

	/**
	 * Password y confirmación.
	 */
	private PasswordVO passwordForm = new PasswordVO();

	/**
	 * Password actual del usuario.
	 */
	private String oldPassword;
	
	/**
	 * Nombre de pila.
	 */
	private String name;

	/**
	 * Primer apellido.
	 */
	private String surname;

	/**
	 * Segundo apellido.
	 */
	private String secondSurname;

	/**
	 * Documento de identificación.
	 */
	private String document;

	/**
	 * Dirección de correo electrónico.
	 */
	private String email;

	/**
	 * Indicador de cuenta de usuario bloqueada.
	 */
	private Boolean locked = Boolean.FALSE;

	/**
	 * Fecha de creación de la cuenta de usuario.
	 */
	private Date creationDate;

	/**
	 * Fecha de la última actualización de información del usuario.
	 */
	private Date lastUpdateDate;

	/**
	 * Próxima fecha de renovación de la password.
	 */
	private Date nextPassUpdateDate;

	/**
	 * Número de intentos consecutivos fallidos de inicio de sesión.
	 */
	private Integer failedAttempts = 0;

	/**
	 * Indica si el usuario ha sido modificado.
	 */
	private Boolean edited = Boolean.FALSE;

	/**
	 * Perfil o rol del usuario.
	 */
	private ProfileVO profile = new ProfileVO();

	/**
	 * Relación de unidades organizativas a las que pertenece un usuario.
	 */
	private List<OrganizationUnitVO> orgUnits = new ArrayList<OrganizationUnitVO>();

	/**
	 * Constructor sin argumentos.
	 */
	public UserVO() {
		super();
	}

	/**
	 * Obtiene el identificador del usuario.
	 * 
	 * @return el identificador del usuario.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador del usuario.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador del usuario.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el nombre de usuario.
	 * 
	 * @return el nombre de usuario.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece un nuevo valor para el nombre de usuario.
	 * 
	 * @param anUsername
	 *            nuevo valor para el nombre de usuario.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene la password y su confirmación.
	 * 
	 * @return la password y su confirmación.
	 */
	public PasswordVO getPasswordForm() {
		return passwordForm;
	}

	/**
	 * Establece la password y su confirmación.
	 * 
	 * @param aPasswordForm
	 *            password y confirmación.
	 */
	public void setPasswordForm(PasswordVO aPasswordForm) {
		this.passwordForm = aPasswordForm;
	}

	/**
	 * Obtiene la password actual del usuario.
	 * 
	 * @return la password actual del usuario.
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * Establece la password actual del usuario.
	 * 
	 * @param anOldPassword
	 *            una password de usuario.
	 */
	public void setOldPassword(String anOldPassword) {
		this.oldPassword = anOldPassword;
	}
	
	/**
	 * Obtiene el nombre de pila del usuario.
	 * 
	 * @return el nombre de pila del usuario.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece el nombre de pila del usuario.
	 * 
	 * @param aName
	 *            nombre de pila del usuario.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el primer apellido del usuario.
	 * 
	 * @return el primer apellido del usuario.
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Establece el primer apellido del usuario.
	 * 
	 * @param aSurname
	 *            primer apellido del usuario.
	 */
	public void setSurname(String aSurname) {
		this.surname = aSurname;
	}

	/**
	 * Obtiene el segundo apellido del usuario.
	 * 
	 * @return el segundo apellido del usuario.
	 */
	public String getSecondSurname() {
		return secondSurname;
	}

	/**
	 * Establece el segundo apellido del usuario.
	 * 
	 * @param aSecondSurname
	 *            segundo apellido del usuario.
	 */
	public void setSecondSurname(String aSecondSurname) {
		this.secondSurname = aSecondSurname;
	}

	/**
	 * Obtiene el documento de identificación del usuario.
	 * 
	 * @return el documento de identificación del usuario.
	 */
	public String getDocument() {
		return document;
	}

	/**
	 * Establece el documento de identificación del usuario.
	 * 
	 * @param aDocument
	 *            documento de identificación del usuario.
	 */
	public void setDocument(String aDocument) {
		this.document = aDocument;
	}

	/**
	 * Obtiene la dirección de correo electrónico del usuario.
	 * 
	 * @return la dirección de correo electrónico del usuario.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Establece la dirección de correo electrónico del usuario.
	 * 
	 * @param anEmail
	 *            dirección de correo electrónico del usuario.
	 */
	public void setEmail(String anEmail) {
		this.email = anEmail;
	}

	/**
	 * Obtiene el indicador de cuenta de usuario bloqueada.
	 * 
	 * @return el indicador de cuenta de usuario bloqueada.
	 */
	public Boolean getLocked() {
		return locked;
	}

	/**
	 * Establece el valor del indicador de cuenta de usuario bloqueada.
	 * 
	 * @param lockedParam
	 *            nuevo valor del indicador de cuenta de usuario bloqueada.
	 */
	public void setLocked(Boolean lockedParam) {
		this.locked = lockedParam;
	}

	/**
	 * Obtiene la fecha de creación de la cuenta de usuario.
	 * 
	 * @return la fecha de creación de la cuenta de usuario.
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Establece la fecha de creación de la cuenta de usuario.
	 * 
	 * @param aCreationDate
	 *            fecha de creación de la cuenta de usuario.
	 */
	public void setCreationDate(Date aCreationDate) {
		this.creationDate = aCreationDate;
	}

	/**
	 * Obtiene la última fecha de actualización de la información del usuario.
	 * 
	 * @return la última fecha de actualización de la información del usuario.
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * Establece la última fecha de actualización de la información del usuario.
	 * 
	 * @param aLastUpdateDate
	 *            última fecha de actualización de la información del usuario.
	 */
	public void setLastUpdateDate(Date aLastUpdateDate) {
		this.lastUpdateDate = aLastUpdateDate;
	}

	/**
	 * Obtiene la próxima fecha de renovación de la password.
	 * 
	 * @return la próxima fecha de renovación de la password.
	 */
	public Date getNextPassUpdateDate() {
		return nextPassUpdateDate;
	}

	/**
	 * Establece la próxima fecha de renovación de la password.
	 * 
	 * @param aNextPassUpdateDate
	 *            próxima fecha de renovación de la password.
	 */
	public void setNextPassUpdateDate(Date aNextPassUpdateDate) {
		this.nextPassUpdateDate = aNextPassUpdateDate;
	}

	/**
	 * Obtiene el número de intentos consecutivos fallidos de inicio de sesión.
	 * 
	 * @return el número de intentos consecutivos fallidos de inicio de sesión.
	 */
	public Integer getFailedAttempts() {
		return failedAttempts;
	}

	/**
	 * Establece el número de intentos consecutivos fallidos de inicio de
	 * sesión.
	 * 
	 * @param aFailedAttemptsNumber
	 *            nuevo número de intentos consecutivos fallidos de inicio de
	 *            sesión.
	 */
	public void setFailedAttempts(Integer aFailedAttemptsNumber) {
		this.failedAttempts = aFailedAttemptsNumber;
	}

	/**
	 * Obtiene el indicador de edición de un usuario.
	 * 
	 * @return el indicador de edición de un usuario.
	 */
	public Boolean getEdited() {
		return edited;
	}

	/**
	 * Establece el indicador de edición de un usuario (true).
	 * 
	 * @param editedParam
	 *            nuevo valor del indicador de edición de un usuario.
	 */
	public void setEdited(Boolean editedParam) {
		this.edited = editedParam;
	}

	/**
	 * Obtiene el perfil o rol del usuario.
	 * 
	 * @return el perfil o rol del usuario.
	 */
	public ProfileVO getProfile() {
		return profile;
	}

	/**
	 * Establece el perfil o rol del usuario.
	 * 
	 * @param aProfile
	 *            nuevo perfil o rol del usuario.
	 */
	public void setProfile(ProfileVO aProfile) {
		this.profile = aProfile;
	}

	/**
	 * Obtiene la relación de unidades organizativas a las que pertenece un
	 * usuario.
	 * 
	 * @return la relación de unidades organizativas a las que pertenece un
	 *         usuario.
	 */
	public List<OrganizationUnitVO> getOrgUnits() {
		return orgUnits;
	}

	/**
	 * Establece la relación de unidades organizativas a las que pertenece un
	 * usuario.
	 * 
	 * @param anyOrgUnits
	 *            relación de unidades organizativas a las que pertenece un
	 *            usuario.
	 */
	public void setOrgUnits(List<OrganizationUnitVO> anyOrgUnits) {
		this.orgUnits.clear();

		if (anyOrgUnits != null && !anyOrgUnits.isEmpty()) {
			this.orgUnits.addAll(anyOrgUnits);
		}
	}

}
