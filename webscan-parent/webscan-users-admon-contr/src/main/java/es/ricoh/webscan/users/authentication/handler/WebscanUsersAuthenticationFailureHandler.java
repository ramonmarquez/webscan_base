/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.authentication.handlers.WebscanUsersAuthenticationFailureHandler.java.</p>
 * <b>Descripción:</b><p> Clase que implementa las acciones a realizar cuando se produce un error en el
 * acceso al Sistema por parte de un usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.authentication.handler;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.web.servlet.LocaleResolver;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.users.UsersWebscanConstants;

/**
 * Clase que implementa las acciones a realizar cuando se produce un error en el
 * acceso al Sistema por parte de un usuario.
 * <p>
 * Clase WebscanUsersAuthenticationFailureHandler.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class WebscanUsersAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WebscanUsersAuthenticationFailureHandler.class);

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Utilidad para la resolución de la configuración regional y de idioma del
	 * sistema.
	 */
	private LocaleResolver localeResolver;

	@Override
	public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException exception) throws IOException, ServletException {
		Locale locale;
		Object[ ] args = new String[1];
		String errorMessagePropName, errorMessage;

		LOG.debug("[WEBSCAN-AUTH] Inicio de sesión fallido de usuario al sistema. Se obtiene la URL de retorno y completa los datos a presentar...");

		errorMessagePropName = WebscanBaseConstants.DEFAULT_ERROR_MESSAGE;

		if (exception instanceof CredentialsExpiredException) {
			errorMessagePropName = UsersWebscanConstants.AUTH_CRED_EXPIRED_ERROR_MESSAGE;
			setDefaultFailureUrl("/updatePassword.html");
		} else {
			setDefaultFailureUrl("/login.html");

			if (exception instanceof AccountStatusException) {
				errorMessagePropName = UsersWebscanConstants.AUTH_ACCOUNT_STATUS_ERROR_MESSAGE;
			} else if (exception instanceof AuthenticationCredentialsNotFoundException) {
				// No existe usuario en el contexto de seguridad
				errorMessagePropName = UsersWebscanConstants.AUTH_CRED_NOT_FOUND_ERROR_MESSAGE;
			} else if (exception instanceof AuthenticationServiceException) {
				// Error interno del sistema
				errorMessagePropName = UsersWebscanConstants.AUTH_SERVICE_ERROR_MESSAGE;
			} else if (exception instanceof BadCredentialsException) {
				errorMessagePropName = UsersWebscanConstants.AUTH_BAD_CRED_ERROR_MESSAGE;
			} else if (exception instanceof SessionAuthenticationException) {
				errorMessagePropName = UsersWebscanConstants.AUTH_SESSION_ERROR_MESSAGE;
			} else if (exception instanceof UsernameNotFoundException) {
				errorMessagePropName = UsersWebscanConstants.AUTH_USER_NOT_FOUND_ERROR_MESSAGE;
			} else {
				args = new String[1];
				args[0] = exception.getMessage();
			}
		}

		super.onAuthenticationFailure(request, response, exception);

		locale = localeResolver.resolveLocale(request);
		errorMessage = messages.getMessage(errorMessagePropName, args, locale);
		request.getSession().setAttribute(WebscanBaseConstants.REQUEST_RESULT_ATT_NAME, WebscanBaseConstants.REQUEST_RESULT_ERROR_VALUE);
		request.getSession().setAttribute(WebscanBaseConstants.REQUEST_RESULT_COUNTER_ATT_NAME, 0);
		request.getSession().setAttribute(WebscanBaseConstants.REQUEST_RESULT_MSG_ATT_NAME, errorMessage);

		LOG.debug("[WEBSCAN-AUTH] Finalizadas las acciones a realizar por inicio de sesión fallido de usuario al sistema.");
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Objeto que recopila los mensajes, sujetos a multidioma, que
	 *            son presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}

	/**
	 * Establece la utilidad para la resolución de la configuración regional y
	 * de idioma del sistema.
	 * 
	 * @param aLocaleResolver
	 *            Utilidad para la resolución de la configuración regional y de
	 *            idioma del sistema.
	 */
	public void setLocaleResolver(LocaleResolver aLocaleResolver) {
		this.localeResolver = aLocaleResolver;
	}

}
