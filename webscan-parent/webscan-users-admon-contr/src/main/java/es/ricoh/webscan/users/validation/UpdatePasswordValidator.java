/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.validation.UpdatePasswordValidator.java.</p>
 * <b>Descripción:</b><p> Validador que comprueba los datos presentados por un usuario en el formulario
 * de actualización de password por caducidad de su password.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import es.ricoh.webscan.users.controller.vo.UpdatePasswordVO;

/**
 * Validador que comprueba los datos presentados por un usuario en el formulario
 * de actualización de password por caducidad de su password.
 * <p>
 * Clase UpdatePasswordValidator.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UpdatePasswordValidator implements Validator {

	/**
	 * Validador que comprueba que la password y su confirmación son iguales.
	 */
	private PasswordMatchesValidator passwordMatchesValidator;

	/**
	 * Validador que comprueba que la password es robusta y cumple con las
	 * medidas de calidad configuradas en el sistema.
	 */
	private PasswordPatternValidator passwordPatternValidator;

	/**
	 * Validador que comprueba que se ha informado la password actual del
	 * usuario, y que la nueva password no es igual a esta.
	 */
	private OldPasswordValidator oldPasswordValidator;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return UpdatePasswordVO.class.isAssignableFrom(clazz);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 *      org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		UpdatePasswordVO updatePassword = (UpdatePasswordVO) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "error.username.required", "Username is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordForm.password", "error.password.required", "Password is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordForm.confirmPassword", "error.confirmPassword.required", "Confirm password is required.");
		ValidationUtils.invokeValidator(passwordMatchesValidator, updatePassword.getPasswordForm(), errors);
		ValidationUtils.invokeValidator(passwordPatternValidator, updatePassword.getPasswordForm(), errors);
		ValidationUtils.invokeValidator(oldPasswordValidator, updatePassword, errors);
	}

	/**
	 * Establece el validador que comprueba que la password y su confirmación
	 * son iguales.
	 * 
	 * @param aPasswordMatchesValidator
	 *            validador que comprueba que la password y su confirmación son
	 *            iguales.
	 */
	public void setPasswordMatchesValidator(PasswordMatchesValidator aPasswordMatchesValidator) {
		this.passwordMatchesValidator = aPasswordMatchesValidator;
	}

	/**
	 * Establece el validador que comprueba que la password es robusta y cumple
	 * con las medidas de calidad configuradas en el sistema.
	 * 
	 * @param aPasswordPatternValidator
	 *            validador que comprueba que la password es robusta y cumple
	 *            con las medidas de calidad configuradas en el sistema.
	 */
	public void setPasswordPatternValidator(PasswordPatternValidator aPasswordPatternValidator) {
		this.passwordPatternValidator = aPasswordPatternValidator;
	}

	/**
	 * Establece el validador que comprueba que se ha informado la password
	 * actual del usuario, y que la nueva password no es igual a esta.
	 * 
	 * @param anOldPasswordValidator
	 *            validador que comprueba que se ha informado la password actual
	 *            del usuario, y que la nueva password no es igual a esta.
	 */
	public void setOldPasswordValidator(OldPasswordValidator anOldPasswordValidator) {
		this.oldPasswordValidator = anOldPasswordValidator;
	}

}
