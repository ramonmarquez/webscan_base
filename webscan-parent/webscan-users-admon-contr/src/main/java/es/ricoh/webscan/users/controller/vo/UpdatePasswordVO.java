/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.vo.RetrievePasswordVO.java.</p>
 * <b>Descripción:</b><p> Objeto de la capa de presentación para el formulario de actualización de pasword caducada.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.controller.vo;

import java.io.Serializable;

/**
 * Objeto de la capa de presentación para el formulario de actualización de
 * pasword caducada.
 * <p>
 * Clase UpdatePasswordVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UpdatePasswordVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nombre de usuario.
	 */
	private String username;

	/**
	 * Password actual del usuario.
	 */
	private String oldPassword;

	/**
	 * Nueva password del usuario y su confirmación.
	 */
	private PasswordVO passwordForm;

	/**
	 * Constructor sin argumentos.
	 */
	public UpdatePasswordVO() {
		super();
	}

	/**
	 * Obtiene el nombre de usuario.
	 * 
	 * @return el nombre de usuario.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece el nombre de usuario.
	 * 
	 * @param anUsername
	 *            un nombre de usuario.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene la password actual del usuario.
	 * 
	 * @return la password actual del usuario.
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * Establece la password actual del usuario.
	 * 
	 * @param anOldPassword
	 *            una password de usuario.
	 */
	public void setOldPassword(String anOldPassword) {
		this.oldPassword = anOldPassword;
	}

	/**
	 * Obtiene la nueva password del usuario y su confirmación.
	 * 
	 * @return la nueva password del usuario y su confirmación.
	 */
	public PasswordVO getPasswordForm() {
		return passwordForm;
	}

	/**
	 * Establece la nueva password del usuario y su confirmación.
	 * 
	 * @param aPasswordForm
	 *            la nueva password del usuario y su confirmación.
	 */
	public void setPasswordForm(PasswordVO aPasswordForm) {
		this.passwordForm = aPasswordForm;
	}

}
