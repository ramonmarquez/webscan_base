/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.validation.OldPasswordValidator.java.</p>
 * <b>Descripción:</b><p> Validador que comprueba que la nueva password de un usuario, en el formulario
 * de actualización de password, no coincide con la anterior password del
 * usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.validation;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.users.UsersWebscanConstants;
import es.ricoh.webscan.users.controller.vo.UpdatePasswordVO;
import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.model.dto.UserDTO;
import es.ricoh.webscan.users.service.LoginService;

/**
 * Validador que comprueba que la nueva password de un usuario, en el formulario
 * de actualización de password, no coincide con la anterior password del
 * usuario.
 * <p>
 * Clase OldPasswordValidator.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class OldPasswordValidator implements Validator {

	/**
	 * Servicio que agrupa las distintas funcionalidades de inicio de sesión y
	 * modificación de password de usuario.
	 */
	private LoginService loginService;

	/**
	 * Utilidad de cifrado de passwords.
	 */
	private PasswordEncoder passwordEncoder;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return UpdatePasswordVO.class.isAssignableFrom(clazz);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 *      org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		String errorPropName;
		UpdatePasswordVO updatePassword = (UpdatePasswordVO) target;
		Object[ ] args = new String[1];
		UserDTO userDto;

		errorPropName = WebscanBaseConstants.DEFAULT_ERROR_MESSAGE;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oldPassword", "error.password.oldPasswordRequired", "Old password is required.");

		try {
			if (updatePassword != null && (updatePassword.getUsername() != null && !updatePassword.getUsername().isEmpty()) && (updatePassword.getOldPassword() != null && !updatePassword.getOldPassword().isEmpty())) {
				userDto = loginService.getUserByUsername(updatePassword.getUsername());

				if (!passwordEncoder.matches(updatePassword.getOldPassword(), userDto.getPassword())) {
					errors.rejectValue("oldPassword", "error.password.oldPasswordNotMatches", "Old password is not correct.");
				}

				if (updatePassword.getOldPassword().equals(updatePassword.getPasswordForm().getPassword())) {
					errors.rejectValue("passwordForm.password", "error.password.oldPassMatchesNewPass", "Old password is equal than new password.");
				}
			}
		} catch (UsersDAOException e) {
			if (e.getCode().equals(DAOException.CODE_901)) {
				errorPropName = UsersWebscanConstants.AUTH_USER_NOT_FOUND_ERROR_MESSAGE;
			} else {
				if (e.getCode().equals(UsersDAOException.CODE_800)) {
					errorPropName = UsersWebscanConstants.AUTH_ACCOUNT_STATUS_ERROR_MESSAGE;
				} else if (e.getCode().equals(UsersDAOException.CODE_802)) {
					errorPropName = UsersWebscanConstants.AUTH_BAD_CRED_ERROR_MESSAGE;
				} else {
					args[0] = e.getMessage();
				}
			}

			errors.rejectValue("oldPassword", errorPropName, args, "Old password is not correct.");
		}
	}

	/**
	 * Establece el servicio que agrupa las distintas funcionalidades de inicio
	 * de sesión y modificación de password de usuario.
	 * 
	 * @param aLoginService
	 *            nuevo servicio que agrupa las distintas funcionalidades de
	 *            inicio de sesión y modificación de password de usuario.
	 */
	public void setLoginService(LoginService aLoginService) {
		this.loginService = aLoginService;
	}

	/**
	 * Establece la utilidad de cifrado de passwords.
	 * 
	 * @param aPasswordEncoder
	 *            nueva utilidad de cifrado de passwords.
	 */
	public void setPasswordEncoder(PasswordEncoder aPasswordEncoder) {
		this.passwordEncoder = aPasswordEncoder;
	}

}
