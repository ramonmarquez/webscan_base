/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.vo.ReqRetrievePasswordVO.java.</p>
 * <b>Descripción:</b><p> Objeto de la capa de presentación para el formulario de petición de
 * restablecimiento de password de usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.controller.vo;

import java.io.Serializable;

/**
 * Objeto de la capa de presentación para el formulario de petición de
 * restablecimiento de password de usuario.
 * <p>
 * Clase ReqRetrievePasswordVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ReqRetrievePasswordVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nombre de usuario.
	 */
	private String username;

	/**
	 * Dirección de correo electrónico.
	 */
	private String email;

	/**
	 * Constructor sin argumentos.
	 */
	public ReqRetrievePasswordVO() {
		super();
	}

	/**
	 * Obtiene el nombre de usuario.
	 * 
	 * @return el nombre de usuario.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece el nombre de usuario.
	 * 
	 * @param anUsername
	 *            un nombre de usuario.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene la dirección de correo electrónico.
	 * 
	 * @return la dirección de correo electrónico.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Establece la dirección de correo electrónico.
	 * 
	 * @param anEmail
	 *            una dirección de correo electrónico.
	 */
	public void setEmail(String anEmail) {
		this.email = anEmail;
	}

}
