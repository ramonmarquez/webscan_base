/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.UsersControllerUtils.java.</p>
 * <b>Descripción:</b><p> Clase que agrupa funcionalidades comunes para ser empleadas por los
 * diferentes controladores del componente de gestión de usuarios.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.MessageSource;
import org.springframework.security.web.WebAttributes;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.users.UsersWebscanConstants;
import es.ricoh.webscan.users.model.UsersDAOException;

/**
 * Clase que agrupa funcionalidades comunes para ser empleadas por los
 * diferentes controladores del componente de gestión de usuarios.
 * <p>
 * Class UsersControllerUtils.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UsersControllerUtils extends BaseControllerUtils {

	/**
	 * Obtiene y establece como atributo de una petición un mensaje de error
	 * asociado a una excepción UsersDAOException.
	 * 
	 * @param e
	 *            Excepción UsersDAOException.
	 * @param request
	 *            Petición web.
	 * @param messageSource
	 *            Clase que agrupa los mensajes paramterizados en el sistema.
	 * @param storeInCommonMessages
	 *            Indica si el mensaje de error es mostrado en la variable de
	 *            sesión de mensajes (true), o en la petición web (false).
	 */
	public static void setUsersDAOExceptionInRequest(UsersDAOException e, HttpServletRequest request, MessageSource messageSource, Boolean storeInCommonMessages) {
		String errorMessage, errorPropName;
		String[ ] args = new String[1];

		errorPropName = WebscanBaseConstants.DEFAULT_ERROR_MESSAGE;

		if (e.getCode().equals(DAOException.CODE_901)) {
			errorPropName = UsersWebscanConstants.AUTH_USER_NOT_FOUND_ERROR_MESSAGE;
		} else {
			if (e.getCode().equals(UsersDAOException.CODE_800)) {
				errorPropName = UsersWebscanConstants.AUTH_ACCOUNT_STATUS_ERROR_MESSAGE;
			} else if (e.getCode().equals(UsersDAOException.CODE_802)) {
				errorPropName = UsersWebscanConstants.AUTH_BAD_CRED_ERROR_MESSAGE;
			} else if (e.getCode().equals(UsersDAOException.CODE_850)) {
				errorPropName = UsersWebscanConstants.REQ_REC_PASS_MSG_ANY_REQS_IN_PROC_MESSAGE;
			} else {
				args[0] = e.getMessage();
			}
		}

		if (storeInCommonMessages) {
			addCommonMessage(errorPropName, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		} else {
			errorMessage = messageSource.getMessage(errorPropName, args, request.getLocale());
			request.setAttribute(WebscanBaseConstants.REQUEST_RESULT_MSG_ATT_NAME, errorMessage);
		}

		request.setAttribute(WebscanBaseConstants.REQUEST_RESULT_ATT_NAME, WebscanBaseConstants.REQUEST_RESULT_ERROR_VALUE);
	}

	/**
	 * Elimina los atributos de sesión que muestran información de error.
	 * 
	 * @param request
	 *            petición web.
	 */
	public static void clearAuthenticationAttributes(final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		Integer counter = 0;
		if (session != null) {
			counter = (Integer) session.getAttribute(WebscanBaseConstants.REQUEST_RESULT_COUNTER_ATT_NAME);
			if (counter == null) {
				counter = 1;
				session.setAttribute(WebscanBaseConstants.REQUEST_RESULT_COUNTER_ATT_NAME, counter);
			} else if (counter != null && counter > 0) {
				session.removeAttribute(WebscanBaseConstants.REQUEST_RESULT_ATT_NAME);
				session.removeAttribute(WebscanBaseConstants.REQUEST_RESULT_MSG_ATT_NAME);
				session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
			} else {
				counter++;
				session.setAttribute(WebscanBaseConstants.REQUEST_RESULT_COUNTER_ATT_NAME, counter);
			}
		}
	}
}
