/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.validation.PasswordMatchesValidator.java.</p>
 * <b>Descripción:</b><p> Clase que comprueba que la password informada coincide con el patrón
 * configurado a nivel de Sistema.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.ricoh.webscan.users.controller.vo.PasswordVO;

/**
 * Clase que comprueba que la password informada coincide con el patrón
 * configurado a nivel de Sistema.
 * <p>
 * Clase PasswordPatternValidator.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class PasswordPatternValidator implements Validator {

	/**
	 * Patrón de las passwords de usuario.
	 */
	private String passwordPattern;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return PasswordVO.class.isAssignableFrom(clazz);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 *      org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		PasswordVO password = (PasswordVO) target;
		if (passwordPattern != null && password.getPassword() != null) {
			if (!password.getPassword().matches(passwordPattern.trim())) {
				errors.rejectValue("passwordForm.password", "error.password.wordNotMatchPattern", "Password must macth the pattern.");
			}
		}
	}

	/**
	 * Establece un nuevo patrón de las passwords de usuario.
	 * 
	 * @param aPasswordPattern
	 *            nuevo patrón de las passwords de usuario.
	 */
	public void setPasswordPattern(String aPasswordPattern) {
		this.passwordPattern = aPasswordPattern;
	}

}
