/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.service.UserService.java.</p>
 * <b>Descripción:</b><p> Servicio que agrupa operaciones de consulta, creación y modificación sobre
 * usuarios del sistema.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.OrganizationUnitVO;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.UserOrgUnitScanProfiles;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.service.AuditService;
import es.ricoh.webscan.users.UsersWebscanConstants;
import es.ricoh.webscan.users.controller.vo.PasswordVO;
import es.ricoh.webscan.users.controller.vo.ProfileVO;
import es.ricoh.webscan.users.controller.vo.UserVO;
import es.ricoh.webscan.users.model.UserOrderByClause;
import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.model.UsersWebscanModelManager;
import es.ricoh.webscan.users.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.users.model.dto.ProfileDTO;
import es.ricoh.webscan.users.model.dto.UserDTO;

/**
 * Servicio que agrupa operaciones de consulta, creación y modificación sobre
 * usuarios del sistema.
 * <p>
 * Clase UserService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UserService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos base de Webscan.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos de gestión de usuarios de Webscan.
	 */
	private UsersWebscanModelManager usersWebscanModelManager;

	/**
	 * Servicio interno para el registro y consulta de trazas y eventos de
	 * auditoría del módulo de gestión de usuarios de Webscan.
	 */
	private AuditService auditService;

	/**
	 * Recupera los usuarios registrados en el sistema, ordenados
	 * ascendentemente por nombre de usuario y nombre de pila.
	 * 
	 * @return usuarios registrados en el sistema, ordenados ascendentemente por
	 *         nombre de usuario y nombre de pila.
	 * @throws UsersDAOException
	 *             Si sucede algún error al realizar la consulta en BBDD.
	 */
	public List<UserVO> getAllUsers() throws UsersDAOException {
		List<EntityOrderByClause> orderByColumns = new ArrayList<EntityOrderByClause>();
		List<UserVO> res = new ArrayList<UserVO>();
		Map<UserDTO, List<ProfileDTO>> usersAndProfiles = new HashMap<UserDTO, List<ProfileDTO>>();
		UserDTO userDto;

		LOG.debug("[WEBSCAN-USERS] Recuperando los usuarios registrados en el sistema y sus perfiles de usuario ...");

		orderByColumns.add(UserOrderByClause.USERNAME_ASC);
		orderByColumns.add(UserOrderByClause.NAME_ASC);

		usersAndProfiles = usersWebscanModelManager.listUsersAndProfiles(null, null, orderByColumns);

		if (usersAndProfiles != null) {
			LOG.debug("[WEBSCAN-USERS] Parseando los usuarios y perfiles de usuario recuperados de BBDD ...");
			for (Iterator<UserDTO> it = usersAndProfiles.keySet().iterator(); it.hasNext();) {
				userDto = it.next();
				List<ProfileVO> listProfileVO = fillProfileListVo(usersAndProfiles.get(userDto));

				res.add(fillUserVo(userDto, listProfileVO, null));
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] " + res.size() + " usuarios recuperados.");
		}

		return res;
	}

	/**
	 * Recupera la información de un usuario a partir de su identificador.
	 * Información general, perfiles y unidades organizativas a las que
	 * pertenece.
	 * 
	 * @param userId
	 *            Identificador de usuario.
	 * @return Información registrada para un usuario en el sistema.
	 * @throws UsersDAOException
	 *             Si ocurre alún error recuperando la información del usuario
	 *             de BBDD.
	 */
	public UserVO getUser(Long userId) throws UsersDAOException {
		List<OrganizationUnitDTO> orgUnitsDto;
		List<ProfileDTO> profilesDto;
		UserDTO userDto;
		UserVO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Recuperando información para el usuario " + userId + " ...");
		}
		userDto = usersWebscanModelManager.getUser(userId);
		profilesDto = usersWebscanModelManager.getUserProfilesByUser(userId);
		orgUnitsDto = usersWebscanModelManager.getOrganizationUnitsByUserId(userId);

		res = fillUserVo(userDto, fillProfileListVo(profilesDto), orgUnitsDto);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Usuario " + userId + " recuperado de BBDD.");
		}
		return res;
	}

	/**
	 * Registra o actualiza en el sistema la información de un usuario,
	 * incorporando además las traza de auditoría de la operación.
	 * 
	 * @param user
	 *            usuario a crear o modificar.
	 * @param userInSession
	 *            usuario que solicita la operación.
	 * @param request
	 *            petición web.
	 * @return usuario creado o modificado.
	 * @throws UsersDAOException
	 *             Si ocurre alún error registrando el usuario o su información
	 *             en BBDD.
	 * @throws DAOException
	 *             Si ocurre alún error registrando información de auditoría en
	 *             BBDD.
	 */
	public UserVO performSaveUser(UserVO user, UserInSession userInSession, final HttpServletRequest request) throws UsersDAOException, DAOException {
		UserVO res = null;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se actualiza la información del usuario {} ...", user.getUsername());
		}

		if (user.getId() == null) {
			res = createUser(user);
		} else {
			res = updateUser(user, Boolean.FALSE);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Registro de trazas de auditoria tras actualizar la información del usuario {} ...", user.getUsername());
		}
		// Registro de trazas de auditoria
		createEditUserAuditLogs(AuditOpEventResult.OK, user, userInSession, null, request);

		return res;
	}

	/**
	 * Registra un nuevo usuario en el sistema, incluyendo las unidades
	 * organizativas a las que pertence y sus perfiles de usuario.
	 * 
	 * @param user
	 *            Nuevo usuario.
	 * @return Información registrada para el usuario registrado en el sistema.
	 * @throws UsersDAOException
	 *             Si ocurre alún error registrando el usuario o su información
	 *             en BBDD.
	 */
	private UserVO createUser(UserVO user) throws UsersDAOException {
		List<ProfileDTO> profilesDto;
		Long userId;
		UserDTO userDto;
		UserVO res;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Creando el usuario " + user.getUsername() + " ...");
		}
		// Se registra el usuario
		userDto = new UserDTO();
		userDto = fillUserDto(user, Boolean.TRUE);
		userId = usersWebscanModelManager.createUser(userDto);
		userDto = usersWebscanModelManager.getUser(userId);

		// Se registran sus roles o perfiles de usuario
		profilesDto = new ArrayList<ProfileDTO>();
		// preparamos el DTO
		ProfileDTO prof = new ProfileDTO();

		for (ProfileDTO aux: usersWebscanModelManager.listUserProfiles()) {
			if (user.getProfile().getId().equals(aux.getId())) {
				prof = aux;
				break;
			}
		}
		profilesDto.add(prof);

		usersWebscanModelManager.setUserProfiles(userId, profilesDto);

		// Se registran las unidades organizativas
		if (user.getOrgUnits() != null && !user.getOrgUnits().isEmpty()) {
			usersWebscanModelManager.setUserOrganizationUnits(userId, fillOrgUnitListDto(user.getOrgUnits()));
		}

		res = getUser(userId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Usuario " + user.getUsername() + " creado en BBDD.");
		}
		return res;
	}

	/**
	 * Actualiza la información de un usuario registrado en el sistema,
	 * incluyendo las unidades organizativas a las que pertence y sus perfiles
	 * de usuario, si así se ha solicitado.
	 * 
	 * @param user
	 *            usuario.
	 * @param updateOnlyUser
	 *            indica si se debe actualizar solo la información del usuario
	 *            (true), o si, adicionalmente, también se debe actualizar el
	 *            perfil y unidades organizativas.
	 * @return Información registrada para el usuario registrado en el sistema.
	 * @throws UsersDAOException
	 *             Si ocurre alún error registrando el usuario o su información
	 *             en BBDD.
	 */
	private UserVO updateUser(UserVO user, Boolean updateOnlyUser) throws UsersDAOException {
		List<ProfileDTO> profilesDto;
		UserDTO userDto;
		UserVO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Creando el usuario " + user.getUsername() + " ...");
		}
		// Se actualiza la información del usuario
		userDto = new UserDTO();
		userDto = fillUserDto(user, Boolean.FALSE);
		usersWebscanModelManager.updateUser(userDto, Boolean.TRUE);

		if (user.getPasswordForm().getPassword() != null && !user.getPasswordForm().getPassword().isEmpty()) {
			// Si la password ha sido actualizada, se modifica en BBDD
			usersWebscanModelManager.updateUserPassword(userDto);
		}
		userDto = usersWebscanModelManager.getUser(user.getId());

		if (!updateOnlyUser) {
			// Se actualizan sus roles o perfiles de usuarios
			profilesDto = new ArrayList<ProfileDTO>();

			List<ProfileDTO> profiles = usersWebscanModelManager.listUserProfiles();
			// transformamos de VO a DTO
			ProfileDTO aux = null;
			for (ProfileDTO ac: profiles) {
				if (ac.getId().equals(user.getProfile().getId())) {
					aux = ac;
					break;
				}
			}
			profilesDto.add(aux);

			// profilesDto.add(user.getProfile());

			usersWebscanModelManager.setUserProfiles(user.getId(), profilesDto);

			// Se registran las unidades organizativas
			// if (user.getOrgUnits() != null && !user.getOrgUnits().isEmpty())
			// {
			usersWebscanModelManager.setUserOrganizationUnits(user.getId(), fillOrgUnitListDto(user.getOrgUnits()));
		}
		res = getUser(user.getId());

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Usuario " + user.getUsername() + " actualizado en BBDD.");
		}

		return res;
	}

	/**
	 * Actualiza la información de un usuario de session en el sistema,
	 * incluyendo las trazas de auditoría asociadas.
	 * 
	 * @param user
	 *            usuario.
	 * @param request
	 *            petición web.
	 * @return Información registrada
	 * @throws UsersDAOException
	 *             Si ocurre alún error registrando el usuario o su información
	 *             en BBDD.
	 * @throws DAOException
	 *             Si ocurre alún error registrando información de auditoría en
	 *             BBDD.
	 */
	public void performUpdateUserSession(final UserVO user, final HttpServletRequest request) throws UsersDAOException, DAOException {
		HttpSession session;
		UserInSession userInSession;
		UserVO updatedUser;

		updatedUser = updateUserSession(user, request);

		session = request.getSession(false);

		// Actualizar info usuario logado en sesión
		session.setAttribute(WebscanBaseConstants.USER_SESSION_ATT_NAME, updateUserInSession(updatedUser.getId()));

		// obtenemos la info acutalizada
		userInSession = BaseControllerUtils.getUserInSession(request);

		// Registro de trazas de auditoria
		createEditUserAuditLogs(AuditOpEventResult.OK, user, userInSession, null, request);
	}

	/**
	 * Actualiza la información de un usuario de session.
	 * 
	 * @param user
	 *            usuario.
	 * @param request
	 *            petición web.
	 * @return Información registrada
	 * @throws UsersDAOException
	 *             Si ocurre alún error registrando el usuario o su información
	 *             en BBDD.
	 */
	private UserVO updateUserSession(UserVO user, HttpServletRequest request) throws UsersDAOException {
		UserDTO userDto;
		UserVO res;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Actualizamos el usuario " + user.getUsername() + " ...");
		}

		// Se actualiza la información del usuario
		userDto = new UserDTO();
		userDto = fillUserDtoSession(user);
		usersWebscanModelManager.updateUser(userDto, Boolean.TRUE);

		if (user.getPasswordForm().getPassword() != null && !user.getPasswordForm().getPassword().isEmpty()) {
			// Si la password ha sido actualizada, se modifica en BBDD
			usersWebscanModelManager.updateUserPassword(userDto);
			// Se limpia la zona de mensajes, eliminando el mensaje de
			// recordatorio de caducidad de password
			BaseControllerUtils.deleteCommonMessage(UsersWebscanConstants.WARN_EXPIRED_PASS_MESSAGE, request);
		}

		res = getUser(userDto.getId());

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Usuario " + user.getUsername() + " actualizado en BBDD.");
		}

		return res;
	}

	/**
	 * Registra las trazas de auditoria correspondientes al borrado de usuarios
	 * del sistema.
	 * 
	 * @param result
	 *            Resultado de la operación.
	 * @param user
	 *            Usuario creado o modificado.
	 * @param userInSession
	 *            Usuario que solicita la operación.
	 * @param exception
	 *            Excepción producida en caso de error.
	 * @param request
	 *            Petición web.
	 * @throws DAOException
	 *             Si se produce algún error al registrar la traza en BBDD.
	 */
	private void createEditUserAuditLogs(AuditOpEventResult result, UserVO user, UserInSession userInSession, Exception exception, HttpServletRequest request) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		Date auditOpDate = new Date();
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;

		LOG.debug("[WEBSCAN-USERS] Se inicia el registro en BBDD de la traza de auditoria por registro o actualización de usuario ....");
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Formando traza de auditoria con resultado " + result + "...");
		}

		auditOperation = new AuditOperationDTO();
		if (user.getId() == null) {
			auditOperation.setName(AuditOperationName.CREATE_USER);
		} else {
			auditOperation.setName(AuditOperationName.UPDATE_USER);
		}
		auditOperation.setStartDate(auditOpDate);
		auditOperation.setLastUpdateDate(auditOpDate);

		LOG.debug("[WEBSCAN-USERS] Añadiendo eventos a la traza de auditoria ...");

		auditEvent = new AuditEventDTO();
		if (AuditOpEventResult.ERROR.equals(result)) {
			auditOperation.setStatus(AuditOperationStatus.CLOSED_ERROR);
			// Tamaño máximo del campo 256 caracteres
			additionalInfo = "Excepción: " + exception.getClass() + ". Mensaje: " + exception.getMessage();

			if (additionalInfo.length() > UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
				additionalInfo = additionalInfo.substring(0, UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
			}

			auditEvent.setAdditionalInfo(additionalInfo);
		} else {
			additionalInfo = "Usuario " + user.getUsername() + (user.getId() == null ? " creado correctamente" : " actualizado correctamente") + ".";

			if (additionalInfo.length() > UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
				additionalInfo = additionalInfo.substring(0, UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
			}
			auditOperation.setStatus(AuditOperationStatus.CLOSED_OK);
		}
		auditEvent.setEventDate(auditOpDate);

		if (user.getId() == null) {
			auditEvent.setEventName(AuditEventName.CREATE_USER);
		} else {
			auditEvent.setEventName(AuditEventName.UPDATE_USER);
		}
		auditEvent.setEventResult(result);
		auditEvent.setEventUser(userInSession.getUsername());
		auditEvents.add(auditEvent);

		LOG.debug("[WEBSCAN-USERS] Registrando traza de auditoria en BBDD ...");

		Long auditOpId = auditService.addNewAuditLogs(auditOperation, auditEvents, Boolean.TRUE);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Traza de auditoria " + auditOpId + " registrada en BBDD ...");
		}
	}

	/**
	 * Recupera los perfiles de usuario registrados en el sistema.
	 * 
	 * @return perfiles de usuario registrados en el sistema.
	 * @throws UsersDAOException
	 *             Si sucede algún error al realizar la consulta en BBDD.
	 */
	public List<ProfileDTO> getUserProfiles() throws UsersDAOException {
		List<ProfileDTO> res = new ArrayList<ProfileDTO>();

		LOG.debug("[WEBSCAN-USERS] Recuperando los perfiles de usuario registrados en el sistema...");

		res = usersWebscanModelManager.listUserProfiles();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] " + res.size() + " perfiles de usuario recuperados.");
		}

		return res;
	}

	/**
	 * Obtiene el listado de perfiles del usuario.
	 * 
	 * @return Listado de perfiles del usuario.
	 * @throws UsersDAOException
	 *             Exception producida al recuperar la información de perfiles.
	 */
	public List<ProfileVO> getUserProfilesVO() throws UsersDAOException {

		List<ProfileVO> res = new ArrayList<ProfileVO>();

		res = fillProfileListVo(getUserProfiles());

		return res;
	}

	/**
	 * Elimina una lista de usuarios registrados en el sistema.
	 * 
	 * @param removedUserIds
	 *            identificadores de usuarios a eliminar.
	 * @param user
	 *            usuario que solicita la operación.
	 * @param request
	 *            petición web.
	 * @throws UsersDAOException
	 *             Si sucede algún error al ejecutar la sentencia en BBDD.
	 * @throws DAOException
	 *             Si ocurre alún error registrando información de auditoría en
	 *             BBDD.
	 */
	public void removeUsers(List<Long> removedUserIds, final UserInSession user, final HttpServletRequest request) throws UsersDAOException, DAOException {

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Eliminando los usuario registrados en el sistema con los identificadores: " + removedUserIds + ".");
		}

		for (Long userId: removedUserIds) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS] Eliminando el usuario con identificador: " + userId + ".");
			}
			usersWebscanModelManager.removeUser(userId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS] Usuario con identificador " + userId + " eliminado.");
			}
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Los usuarios " + removedUserIds + " han sido eliminados.");
		}

		// Registro de trazas de auditoria
		createRemoveUsersAuditLogs(AuditOpEventResult.OK, removedUserIds, user, null, request);
	}

	/**
	 * Registra las trazas de auditoria correspondientes al borrado de usuarios
	 * del sistema.
	 * 
	 * @param result
	 *            Resultado de la operación.
	 * @param userIds
	 *            Identificadores de los usuarios eliminados.
	 * @param user
	 *            Usuario que solicita la operación.
	 * @param exception
	 *            Excepción producida en caso de error.
	 * @param request
	 *            Petición web.
	 * @throws DAOException
	 *             Si se produce algún error al registrar la traza en BBDD.
	 */
	private void createRemoveUsersAuditLogs(AuditOpEventResult result, List<Long> userIds, UserInSession user, Exception exception, HttpServletRequest request) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		Date auditOpDate = new Date();
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;

		LOG.debug("[WEBSCAN-USERS] Se inicia el registro en BBDD de la traza de auditoria por borrado de usuarios ....");

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Formando traza de auditoria con resultado " + result + "...");
		}

		auditOperation = new AuditOperationDTO();
		auditOperation.setName(AuditOperationName.REMOVE_USERS);
		auditOperation.setStartDate(auditOpDate);
		auditOperation.setLastUpdateDate(auditOpDate);

		LOG.debug("[WEBSCAN-USERS] Añadiendo eventos a la traza de auditoria ...");

		auditEvent = new AuditEventDTO();
		if (AuditOpEventResult.ERROR.equals(result)) {
			auditOperation.setStatus(AuditOperationStatus.CLOSED_ERROR);
			// Tamaño máximo del campo 256 caracteres
			additionalInfo = "Excepción: " + exception.getClass() + ". Mensaje: " + exception.getMessage();

			if (additionalInfo.length() > UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
				additionalInfo = additionalInfo.substring(0, UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
			}

			auditEvent.setAdditionalInfo(additionalInfo);
		} else {
			additionalInfo = "Usuarios eliminados: " + userIds;

			if (additionalInfo.length() > UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
				additionalInfo = additionalInfo.substring(0, UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
			}
			auditOperation.setStatus(AuditOperationStatus.CLOSED_OK);
		}
		auditEvent.setEventDate(auditOpDate);
		auditEvent.setEventName(AuditEventName.REMOVE_USERS);
		auditEvent.setEventResult(result);
		auditEvent.setEventUser(user.getUsername());
		auditEvents.add(auditEvent);

		LOG.debug("[WEBSCAN-USERS] Registrando traza de auditoria en BBDD ...");

		Long auditOpId = auditService.addNewAuditLogs(auditOperation, auditEvents, Boolean.TRUE);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Traza de auditoria " + auditOpId + " registrada en BBDD ...");
		}
	}

	/**
	 * Persiste la información asociada a las unidades organizativas.
	 * 
	 * @param request
	 *            Peticion http
	 * @param unit
	 *            Entidad de la Unidad organizativa
	 * @param usuariosStr
	 *            identificadores de usuarios
	 * @throws DAOException
	 *             Si sucede algún error al ejecutar la sentencia en BBDD.
	 */

	public void setUnitOrganization(HttpServletRequest request, es.ricoh.webscan.model.dto.OrganizationUnitDTO unit, String usuariosStr) throws DAOException, UsersDAOException {
		LOG.debug("OrganizationService setUnitOrganization ini");
		Map<Long, List<UserDTO>> usuariosOrg;
		Exception exception = null;
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";
		AuditOperationName opName;
		AuditEventName evName;

		// obtenemos la info para auditoria
		UserInSession user = BaseControllerUtils.getUserInSession(request);
		username = user.getUsername();

		if (unit.getId() != null) {
			// TODO - Este codigo no deberia estar aqui, revisar
			webscanModelManager.updateOrganizationUnit(unit);
			usuariosOrg = getListUsers(unit.getId(), usuariosStr);
			opName = AuditOperationName.ORG_UNIT_UPDATE;
			evName = AuditEventName.ORG_UNIT_UPDATE;
			LOG.debug("OrganizationService setUnitOrganization ACTUALIZA");
		} else {
			Long unidadNueva = webscanModelManager.createOrganizationUnit(unit);
			usuariosOrg = getListUsers(unidadNueva, usuariosStr);
			opName = AuditOperationName.ORG_UNIT_CREATE;
			evName = AuditEventName.ORG_UNIT_CREATE;
			LOG.debug("OrganizationService setUnitOrganization CREA");
		}
		LOG.debug("salvamos la información en auditoria unidades organizativas");
		auditService.createAuditLogs(username, result, authTimestamp, exception, opName, evName);

		opName = AuditOperationName.ORG_UNIT_UPDATE;
		evName = AuditEventName.ORG_UNIT_UPDATE;
		usersWebscanModelManager.setOrganizationUnitsUsers(usuariosOrg);

		LOG.debug("salvamos la información en auditoria de la relacion de usuarios y unidades organizativas");
		auditService.createAuditLogs(username, result, authTimestamp, exception, opName, evName);

		LOG.debug("OrganizationService setUnitOrganization end");
	}

	/**
	 * Obtiene un listado de UserDTO asociado a los usuarios solicitados a
	 * través de string json.
	 * 
	 * @param unit
	 *            unidad organizativa.
	 * @param unitUsers
	 *            informacion recibida desde la vista en notación JSON.
	 * @return Relación de pares clave / valor, en la que cada entrada es
	 *         identificada por identificador de unidad organizativa, y cuyo
	 *         valor se corresponde con los usuarios asociados a dicha unidad
	 *         organizativa.
	 * @throws UsersDAOException
	 *             si ocurre algún error en la consulta de base de datos
	 *             realizada para recuperar la información de los usuarios.
	 * @throws JSONException
	 *             si ocurre algún error al parsear el parámetro unitUsers.
	 */
	public Map<Long, List<UserDTO>> getListUsers(Long unit, String unitUsers) throws UsersDAOException, JSONException {
		Map<Long, List<UserDTO>> datos = new HashMap<Long, List<UserDTO>>();
		UserDTO user;
		List<UserDTO> usuarios = new ArrayList<UserDTO>();

		// parseamos el String
		JSONObject obj = new JSONObject(unitUsers);
		JSONArray arr = obj.getJSONArray("idUser");
		for (int i = 0; i < arr.length(); i++) {
			Object userA = arr.get(i);
			user = usersWebscanModelManager.getUser(Long.valueOf(userA.toString()));
			usuarios.add(user);
		}
		datos.put(unit, usuarios);

		return datos;
	}

	/**
	 * Convierte un objeto UserDTO, de la capa DAO, en un objeto UserVO de la
	 * capa de presentación, incluyendo perfiles de usuario y unidades
	 * organizativas a las que pertence el usuario.
	 * 
	 * @param userDto
	 *            objeto UserDTO.
	 * @param profilesDto
	 *            perfiles de usuario.
	 * @param orgUnitsDto
	 *            unidades organizativas.
	 * @return objeto UserVO de la capa de presentación.
	 */
	private UserVO fillUserVo(UserDTO userDto, List<ProfileVO> profilesDto, List<OrganizationUnitDTO> orgUnitsDto) {
		OrganizationUnitVO ouVo;
		UserVO res = new UserVO();

		// Se establece la información global del usuario
		res.setCreationDate(userDto.getCreationDate());
		res.setDocument(userDto.getDocument());
		res.setEmail(userDto.getEmail());
		res.setFailedAttempts(userDto.getFailedAttempts());
		res.setId(userDto.getId());
		res.setLastUpdateDate(userDto.getLastUpdateDate());
		res.setLocked(userDto.getLocked());
		res.setName(userDto.getName());
		res.setNextPassUpdateDate(userDto.getNextPassUpdateDate());

		// La password se establece vacía, de forma que si tiene valor es porque
		// se ha solicitado su modificación.
		res.setPasswordForm(new PasswordVO());

		res.setSecondSurname(userDto.getSecondSurname());
		res.setSurname(userDto.getSurname());
		res.setUsername(userDto.getUsername());

		// Se establecen los perfiles del usuario
		if (profilesDto != null && !profilesDto.isEmpty()) {
			res.setProfile(profilesDto.get(0));
		}

		// Se establecen los perfiles del usuario
		if (orgUnitsDto != null) {
			for (OrganizationUnitDTO ouDto: orgUnitsDto) {
				ouVo = new OrganizationUnitVO();
				ouVo.setExternalId(ouDto.getExternalId());
				ouVo.setId(ouDto.getId());
				ouVo.setName(ouDto.getName());

				// No se establece la unidad organizativa padre, en principio no
				// es
				// necesario para la edición de usuarios

				res.getOrgUnits().add(ouVo);
			}
		}

		return res;
	}

	/**
	 * Convierte un objeto ProfileDTO, de la capa DAO, en un objeto ProfileVO de
	 * la capa de presentación.
	 * 
	 * @param profileDto
	 *            objeto ProfileDTO.
	 * @return objeto ProfileVO de la capa de presentación.
	 */
	private ProfileVO fillProfileVo(ProfileDTO profileDto) {
		ProfileVO res = new ProfileVO();

		res.setId(profileDto.getId());
		res.setName(profileDto.getName());

		return res;
	}

	/**
	 * Convierte de List ProfileDTO a List ProfileVO.
	 * 
	 * @param profile
	 *            Listado de objectos ProfileDTO
	 * @return Listado de objetos ProfileVO
	 * @throws UsersDAOException
	 *             Exception al tratar la conversión de objetos
	 */
	private List<ProfileVO> fillProfileListVo(List<ProfileDTO> profile) throws UsersDAOException {
		List<ProfileVO> res = new ArrayList<ProfileVO>();

		for (ProfileDTO profi: profile) {
			res.add(fillProfileVo(profi));
		}
		return res;
	}

	/**
	 * Convierte un objeto UserVO, de la capa de presentación, en un objeto
	 * UserDTO de la capa DAO.
	 * 
	 * @param user
	 *            objeto UserVO.
	 * @param isNew
	 *            indica si es nuevo el usuario (true).
	 * @return objeto UserDTO de la capa DAO.
	 * @throws UsersDAOException
	 *             si ocurre algún error al recuperar al usuario de BBDD, en
	 *             caso de estar ya registrado.
	 */
	private UserDTO fillUserDto(UserVO user, Boolean isNew) throws UsersDAOException {
		UserDTO res = new UserDTO();

		// Se establece la información global del usuario
		if (!isNew) {
			res = usersWebscanModelManager.getUser(user.getId());
		}
		res.setDocument(user.getDocument());
		res.setEmail(user.getEmail());
		res.setLocked(user.getLocked());
		res.setName(user.getName());
		res.setPassword(user.getPasswordForm().getPassword());
		res.setSecondSurname(user.getSecondSurname());
		res.setSurname(user.getSurname());
		res.setUsername(user.getUsername());

		return res;
	}

	/**
	 * Convierte un objeto UserVO, de la capa de presentación, en un objeto
	 * UserDTO de la capa DAO.
	 * 
	 * @param user
	 *            objeto UserVO.
	 * @return objeto UserDTO de la capa DAO.
	 * @throws UsersDAOException
	 *             si ocurre algún error al recuperar al usuario de BBDD, en
	 *             caso de estar ya registrado.
	 */
	private UserDTO fillUserDtoSession(UserVO user) throws UsersDAOException {
		UserDTO res = new UserDTO();

		// Se establece la información global del usuario
		// res = usersWebscanModelManager.getUser(user.getId());
		res = usersWebscanModelManager.getUserByUsername(user.getUsername());

		res.setDocument(user.getDocument());
		res.setEmail(user.getEmail());
		res.setName(user.getName());
		res.setPassword(user.getPasswordForm().getPassword());
		res.setSecondSurname(user.getSecondSurname());
		res.setSurname(user.getSurname());

		return res;
	}

	/**
	 * Metodo para rellenar la información de unidades organizativas. Es decir,
	 * pasamos de OrganizationUnitVO a OrganizationUnitDTO.
	 * 
	 * @param orgUnits
	 *            Listado de OrganizationUnitVO.
	 * @return Listado de OrganizationUnitDTO.
	 * @throws UsersDAOException
	 *             Exception producida al crear los nuevos objetos
	 *             OrganizationUnitDTO.
	 */
	private List<OrganizationUnitDTO> fillOrgUnitListDto(List<OrganizationUnitVO> orgUnits) throws UsersDAOException {
		List<Long> orgUnitIds;
		List<OrganizationUnitDTO> res = new ArrayList<OrganizationUnitDTO>();

		if (orgUnits != null && !orgUnits.isEmpty()) {
			orgUnitIds = new ArrayList<Long>();
			for (OrganizationUnitVO orgUnit: orgUnits) {
				orgUnitIds.add(orgUnit.getId());
			}

			res = usersWebscanModelManager.getOrganizationUnitsByIds(orgUnitIds);
		}

		return res;
	}

	/**
	 * Obtiene la relación de perfiles de digitalización configurados para una
	 * unidad organizativa determinada.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return Relación de perfiles de digitalización que implementan una
	 *         especificación determinada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<ScanProfileDTO> getScanProfilesByOrgUnit(Long orgUnitId) throws DAOException {
		List<ScanProfileDTO> res = new ArrayList<ScanProfileDTO>();

		res = webscanModelManager.getScanProfilesByOrgUnit(orgUnitId);

		return res;
	}

	/**
	 * Actualiza la información de sesión de un usuario.
	 * 
	 * @param userId
	 *            Identificador interno de suario.
	 * @return objeto UserInSession de la capa de presentación.
	 * @throws DAOException
	 *             Excepción producida al acceder a la información del modelo de
	 *             datos base.
	 * @throws UsersDAOException
	 *             Excepción producida al acceder al repositorio interno de
	 *             usuarios.
	 */
	public UserInSession updateUserInSession(Long userId) throws UsersDAOException, DAOException {
		List<es.ricoh.webscan.users.model.dto.OrganizationUnitDTO> orgUnits;
		UserDTO dto;
		UserInSession res = new UserInSession();

		dto = usersWebscanModelManager.getUser(userId);

		// Datos básicos del usuario
		res.setDocument(dto.getDocument());
		res.setEmail(dto.getEmail());
		res.setId(dto.getId());
		res.setLocked(dto.getLocked());
		res.setName(dto.getName());
		res.setSecondSurname(dto.getSecondSurname());
		res.setSurname(dto.getSurname());
		res.setUsername(dto.getUsername());

		// Configuración de unidades organizativas y perfiles de digitalización
		orgUnits = usersWebscanModelManager.getOrganizationUnitsByUserId(dto.getId());
		res.setOrgUnitScanProfiles(getUserOrgUnitScanProfiles(orgUnits));

		return res;
	}

	/**
	 * Genera la relación de perfiles de digitalización configuradas para un
	 * conjunto de unidades organizativas.
	 * 
	 * @param orgUnits
	 *            Lista de de unidades organizativas.
	 * @return la relación de perfiles de digitalización configuradas para un
	 *         conjunto de unidades organizativas.
	 * @throws DAOException
	 *             si ocurre algún error al recuperar de base de datos los
	 *             perfiles configurados para las diferentes unidades
	 *             organizativas especificadas como parámetro de entrada.
	 */
	private List<UserOrgUnitScanProfiles> getUserOrgUnitScanProfiles(List<es.ricoh.webscan.users.model.dto.OrganizationUnitDTO> orgUnits) throws DAOException {
		List<UserOrgUnitScanProfiles> res = new ArrayList<UserOrgUnitScanProfiles>();
		List<ScanProfileDTO> scanProfiles;
		UserOrgUnitScanProfiles uousp;

		for (es.ricoh.webscan.users.model.dto.OrganizationUnitDTO orgUnit: orgUnits) {
			scanProfiles = getScanProfilesByOrgUnit(orgUnit.getId());

			if (!scanProfiles.isEmpty()) {
				uousp = new UserOrgUnitScanProfiles();
				uousp.setOrganizationUnit(transformOrganizationUnitDto(orgUnit));
				uousp.setScanProfiles(scanProfiles);
				res.add(uousp);
			}
		}

		return res;
	}

	/**
	 * Transforma un objeto es.ricoh.webscan.users.model.dto.OrganizationUnitDTO
	 * en un objeto OrganizationUnit de la capa de presentación.
	 * 
	 * @param dto
	 *            objeto es.ricoh.webscan.users.model.dto.OrganizationUnitDTO.
	 * @return objeto OrganizationUnit de la capa de presentación.
	 * @throws UsersDAOException
	 *             Exception producida al realizar la transformación de
	 *             OrganizationUnitDTO a OrganizationUnitVO.
	 */
	private OrganizationUnitVO transformOrganizationUnitDto(es.ricoh.webscan.users.model.dto.OrganizationUnitDTO dto) throws UsersDAOException {
		OrganizationUnitVO res = new OrganizationUnitVO();

		res.setExternalId(dto.getExternalId());
		res.setId(dto.getId());
		res.setName(dto.getName());

		return res;
	}

	/**
	 * Establece un nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Establece un nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos de gestión de usuarios de Webscan.
	 * 
	 * @param aUsersWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos de gestión de usuarios de
	 *            Webscan.
	 */
	public void setUsersWebscanModelManager(UsersWebscanModelManager aUsersWebscanModelManager) {
		this.usersWebscanModelManager = aUsersWebscanModelManager;
	}

	/**
	 * Establece el servicio interno para el registro y consulta de trazas y
	 * eventos de auditoría de Webscan.
	 * 
	 * @param anAuditService
	 *            nuevo servicio interno para el registro y consulta de trazas y
	 *            eventos de auditoría de Webscan.
	 */
	public void setAuditService(AuditService anAuditService) {
		this.auditService = anAuditService;
	}

}
