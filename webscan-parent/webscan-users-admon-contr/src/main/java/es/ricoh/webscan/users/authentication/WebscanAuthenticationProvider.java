/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.authentication.WebscanAuthenticationProvider.java.</p>
 * <b>Descripción:</b><p> Proveedor de autenticación de usuarios del módulo de gestión de usuarios de
 * Webscan mediante credenciales usuario / password.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.authentication;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import es.ricoh.webscan.users.model.UsersDAOException;

/**
 * Proveedor de autenticación de usuarios del módulo de gestión de usuarios de
 * Webscan mediante credenciales usuario / password.
 * <p>
 * Clase WebscanAuthenticationProvider.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class WebscanAuthenticationProvider implements AuthenticationProvider {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WebscanAuthenticationProvider.class);

	/**
	 * Utilidad responsable de obtener la información de los usuarios.
	 */
	private UserDetailsService userDetailsService;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Utilidad de cifrado de passwords.
	 */
	private PasswordEncoder passwordEncoder;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.security.authentication.AuthenticationProvider#authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		Locale locale = Locale.getDefault();
		String username, password;
		UserDetails userDetails;
		UsernamePasswordAuthenticationToken res;

		username = authentication.getPrincipal().toString();
		password = authentication.getCredentials().toString();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-AUTH] Se inicia la autenticación en el sistema del usuario " + username + " ...");
		}

		try {
			LOG.debug("[WEBSCAN-AUTH] Recuperando la información del usuario almacenada en BBDD...");
			userDetails = userDetailsService.loadUserByUsername(username);
			LOG.debug("[WEBSCAN-AUTH] Información del usuario almacenada en BBDD recuperada.");
		} catch (UsernameNotFoundException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn("[WEBSCAN-AUTH] Se ha producido un error recuperando la información del usuario " + username + ": " + e.getMessage());
			}
			if (e.getCause() instanceof UsersDAOException && UsersDAOException.CODE_901.equals(((UsersDAOException) e.getCause()).getCode())) {
				throw new UsernameNotFoundException(messages.getMessage("auth.error.usernameNotFound.message", null, locale), e);
			} else {
				String[ ] args = new String[1];
				args[0] = e.getMessage();
				throw new AuthenticationServiceException(messages.getMessage("auth.error.usernameNotFound.message", args, locale), e);
			}
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-AUTH] Se validan las credenciales y estado del " + username + ".");
		}

		if (userDetails == null) {
			throw new BadCredentialsException(messages.getMessage("auth.error.badCredentials.message", null, locale));
		}

		if (!passwordEncoder.matches(password, userDetails.getPassword())) {
			throw new BadCredentialsException(messages.getMessage("auth.error.badCredentials.message", null, locale));
		}

		if (!userDetails.isEnabled()) {
			throw new DisabledException(messages.getMessage("auth.error.accountStatus.message", null, locale));
		}

		if (!userDetails.isCredentialsNonExpired()) {
			throw new CredentialsExpiredException(messages.getMessage("auth.error.credentialsExpired.message", null, locale));
		}

		if (!userDetails.isAccountNonLocked()) {
			throw new LockedException(messages.getMessage("auth.error.accountStatus.message", null, locale));
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-AUTH] Usuario " + username + " autenticado correctamente.");
		}
		res = new UsernamePasswordAuthenticationToken(username, password, userDetails.getAuthorities());
		res.setDetails(userDetails);

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.security.authentication.AuthenticationProvider#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

	/**
	 * Establece la utilidad responsable de obtener la información de los
	 * usuarios.
	 * 
	 * @param anUserDetailsService
	 *            nueva utilidad responsable de obtener la información de los
	 *            usuarios.
	 */
	public void setUserDetailsService(UserDetailsService anUserDetailsService) {
		this.userDetailsService = anUserDetailsService;
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param anyMessages
	 *            nuevo objeto que recopila los mensajes, sujetos a multidioma,
	 *            que son presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}

	/**
	 * Establece la utilidad de cifrado de passwords.
	 * 
	 * @param aPasswordEncoder
	 *            nueva utilidad de cifrado de passwords.
	 */
	public void setPasswordEncoder(PasswordEncoder aPasswordEncoder) {
		this.passwordEncoder = aPasswordEncoder;
	}

}
