/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.login.LoginService.java.</p>
 * <b>Descripción:</b><p> Servicio que agrupa operaciones asociadas al inicio y finalización de
 * sesión en el sistema por parte de usuarios.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.service;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.WebAttributes;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelUtilities;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.service.AuditService;
import es.ricoh.webscan.users.UsersWebscanConstants;
import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.model.UsersWebscanModelManager;
import es.ricoh.webscan.users.model.dto.ProfileDTO;
import es.ricoh.webscan.users.model.dto.RecoveryUserPasswordRequestDTO;
import es.ricoh.webscan.users.model.dto.UserDTO;
import es.ricoh.webscan.utilities.WebscanUtilitiesException;
import es.ricoh.webscan.utilities.mail.MailMessage;
import es.ricoh.webscan.utilities.mail.MailService;

/**
 * Servicio que agrupa operaciones asociadas al inicio y finalización de sesión
 * en el sistema por parte de usuarios.
 * <p>
 * Clase LoginService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class LoginService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LoginService.class);

	/**
	 * Utilidad para la generación segura de números aleatorios.
	 */
	private static final SecureRandom SECURE_RANDOM = new SecureRandom();

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre usuarios del sistema.
	 */
	private UserService userService;

	/**
	 * Servicio interno para el registro y consulta de trazas y eventos de
	 * auditoría del módulo de gestión de usuarios de Webscan.
	 */
	private AuditService auditService;

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos de gestión de usuarios de Webscan.
	 */
	private UsersWebscanModelManager usersWebscanModelManager;

	/**
	 * Parámetros de configuración del módulo de gestión de usuarios de Webscan.
	 */
	private Properties confParameters = new Properties();

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Servicio responsable del envío de correos electrónicos.
	 */
	private MailService webscanMailService;

	/**
	 * Obtiene un usuario a partir de su nombre de usuario.
	 * 
	 * @param username
	 *            nombre de usuario.
	 * @return El usuario solicitado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public UserDTO getUserByUsername(String username) throws UsersDAOException {
		UserDTO res;

		res = usersWebscanModelManager.getUserByUsername(username);

		return res;
	}

	/**
	 * Recupera la información de un usuario que solicita la recuperación de su
	 * password a partir de su nombre de usuario y dirección de correo
	 * electrónico. El método verificará que el usuario no está bloqueado.
	 * 
	 * @param username
	 *            nombre de usuario.
	 * @param email
	 *            dirección de correo elctrónico.
	 * @return El usuario solicitado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, se encuentra bloqueado, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	private UserDTO getUserToReqRetrievePassword(String username, String email) throws UsersDAOException {
		String excMsg;
		UserDTO res;

		res = usersWebscanModelManager.getUserByUsername(username);

		checkLockedUser(res);

		if (!res.getEmail().equalsIgnoreCase(email)) {
			excMsg = "[WEBSCAN-AUTH] Error al recuperar el password de usuario. Cuenta de correo del usuario " + username + " incorrecta.";
			LOG.error(excMsg);
			throw new UsersDAOException(UsersDAOException.CODE_802, excMsg);
		}

		return res;
	}

	/**
	 * Recupera la información de un usuario que finaliza una petición de
	 * recuperación de password a partir de su nombre de usuario. El método
	 * verificará que el usuario no está bloqueado.
	 * 
	 * @param username
	 *            nombre de usuario.
	 * @return El usuario solicitado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, se encuentra bloqueado, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public UserDTO getUserToRetrievePassword(String username) throws UsersDAOException {
		UserDTO res;

		res = usersWebscanModelManager.getUserByUsername(username);

		checkLockedUser(res);

		return res;
	}

	/**
	 * Obtiene información de un usuario a partir de su nombre de usuario
	 * (credenciales y perfiles).
	 * 
	 * @param username
	 *            nombre de usuario.
	 * @return El usuario solicitado.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             usuario solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public UserDetails getUserDetailsByUsername(String username) throws UsersDAOException {
		Boolean userPasswordExpired, enabledUser, lockedUser;
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		List<ProfileDTO> userProfiles;
		String excMsg;
		UserDetails res = null;
		UserDTO userDto;

		try {
			userDto = getUserByUsername(username);
			userPasswordExpired = isUserPasswordExpired(userDto);
			enabledUser = !userDto.getLocked();
			lockedUser = isLockedUser(userDto);

			if (enabledUser && !lockedUser) {

				userProfiles = usersWebscanModelManager.getUserProfilesByUser(userDto.getId());

				if (userProfiles != null && !userProfiles.isEmpty()) {
					for (ProfileDTO dto: userProfiles) {
						authorities.add(new SimpleGrantedAuthority(dto.getName()));
					}
				}
			}

			// User(String username, String password, boolean enabled, boolean
			// accountNonExpired,
			// boolean credentialsNonExpired, boolean accountNonLocked,
			// Collection<? extends GrantedAuthority> authorities)
			res = new User(userDto.getUsername(), userDto.getPassword(), enabledUser, Boolean.TRUE, !userPasswordExpired, !lockedUser, authorities);
		} catch (UsersDAOException e) {
			throw e;
		} catch (IllegalArgumentException e) {
			excMsg = "[WEBSCAN-AUTH] Se produjo un error al formar el objeto UserDetails a partir de la información del usuario " + username + ":" + e.getMessage();
			LOG.error(excMsg, e);
			throw new UsersDAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Obtiene la última fecha de acceso al sistema de un usuario determinado.
	 * 
	 * @param userDto
	 *            Usuario.
	 * @return Ultima fecha de acceso al sistema de un usuario determinado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public Date getUserLastAccess(UserDTO userDto) throws DAOException {
		Date res = new Date();
		AuditOperationDTO auditOperation = auditService.getLastAuditOperation(AuditOperationName.USER_LOGIN, AuditOperationStatus.CLOSED_OK, userDto.getUsername());
		if (auditOperation != null) {
			res = auditOperation.getLastUpdateDate();
		}

		return res;
	}

	/**
	 * Registra en el modelo de datos base y de auditoría un inicio de sesión
	 * con éxito de un usuario al sistema.
	 * 
	 * @param userDto
	 *            Usuario.
	 * @throws UsersDAOException
	 *             Si se produce algún error al ejecutar la setencia de BBDD.
	 */
	public void saveAuthenticationSuccess(UserDTO userDto) throws UsersDAOException, DAOException {
		LOG.debug("[WEBSCAN-USERS] Actualizando información de acceso al sistema del usuario en BBDD...");
		updateUserLastAccess(userDto);

		LOG.debug("[WEBSCAN-USERS] Registrando traza de auditoría en BBDD...");
		logLoginSucceeded(userDto.getUsername(), new Date());
	}

	/**
	 * Actualiza la información de acceso de un usuario tras iniciar sesión en
	 * el sistema. Se modifica el número de intentos fallidos, reiniciando el
	 * contador.
	 * 
	 * @param userDto
	 *            Usuario.
	 * @throws UsersDAOException
	 *             Si se produce algún error al ejecutar la setencia de BBDD.
	 */
	private void updateUserLastAccess(UserDTO userDto) throws UsersDAOException {

		if (userDto.getFailedAttempts() > 0) {
			// Se actualiza el usuario
			userDto.setFailedAttempts(0);
			usersWebscanModelManager.updateUser(userDto, Boolean.FALSE);
		}
	}

	/**
	 * Registra una nueva petición de recuperación de password de un usuario en
	 * el sistema.
	 * 
	 * @param username
	 *            nombre de usuario.
	 * @param email
	 *            dirección de email del usuario.
	 * @param request
	 *            petición web.
	 * @return Identificador de la petición de recuperación de password
	 *         registrada en el sistema.
	 * @throws DAOException
	 *             Excepción producida al acceder a la información del modelo de
	 *             datos base.
	 * @throws UsersDAOException
	 *             Excepción producida al acceder al repositorio interno de
	 *             usuarios.
	 * @throws WebscanUtilitiesException
	 *             Si se produce un error en el envío del correo electrónico.
	 */
	public Long initiateRecoveryUserPasswordRequest(final String username, final String email, final HttpServletRequest request) throws UsersDAOException, DAOException, WebscanUtilitiesException {
		Date comSendingDate;
		Long auditOpId;
		Long res;
		String secureToken;
		UserDTO userDto;
		// Se recupera el usuario de BBDD
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se recupera información del usuario {} de BBDD.", username);
		}
		userDto = getUserToReqRetrievePassword(username, email);

		// Se envía el correo al usuario
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se procede al envío del correo al usuario {} a la dirección de correo {}.", username, email);
		}
		secureToken = generateSecureToken();
		sendRecoveryPasswordMessage(userDto, secureToken, request);
		comSendingDate = new Date();

		// Se registran pistas de auditoria
		LOG.debug("[WEBSCAN-USERS] Se registra la traza de auditoría...");
		auditOpId = createRecoveryPasswordAuditLogs(userDto.getUsername(), comSendingDate, secureToken);
		// Se crea la petición de recuperación de password
		LOG.debug("[WEBSCAN-USERS] Se genera la petición de recuperación de password en BBDD ...");
		res = createRecoveryUserPasswordRequest(userDto, comSendingDate, secureToken, auditOpId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Petición de recuperación de password {} creada en BBDD ...", res);
		}

		return res;
	}

	/**
	 * Crea una nueva petición de recuperación de password para un usuario
	 * dterminado.
	 * 
	 * @param userDto
	 *            Usuario.
	 * @param comSendingDate
	 *            Fecha de envío de la comunicación de recuperación de password
	 *            a la cuenta de correo del usuario.
	 * @param secureToken
	 *            Token de seguridad asociado a la petición de recuperación de
	 *            password.
	 * @param auditOpId
	 *            Identificador de la traza de auditoría asociada a la petición
	 *            de recuperación de password.
	 * @return Identificador de la petición de recuperación de password creada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error al ejecutar la setencia de BBDD.
	 */
	private Long createRecoveryUserPasswordRequest(UserDTO userDto, Date comSendingDate, String secureToken, Long auditOpId) throws UsersDAOException {
		Long res;
		RecoveryUserPasswordRequestDTO recPassReq;

		recPassReq = new RecoveryUserPasswordRequestDTO();
		recPassReq.setComSendingDate(comSendingDate);
		recPassReq.setSecurityToken(generateSecureToken());
		recPassReq.setUserId(userDto.getId());
		recPassReq.setAuditOpId(auditOpId);

		res = usersWebscanModelManager.createRecoveryUserPasswordRequest(recPassReq);

		return res;
	}

	/**
	 * Envia mensaje de recuperación de password.
	 * 
	 * @param userDto
	 *            Objeto con la información del usuario.
	 * @param secureToken
	 *            Token de seguridad.
	 * @param request
	 *            Petición http.
	 * @throws WebscanUtilitiesException
	 *             Exception producida al preparar la información del mensaje.
	 */
	private void sendRecoveryPasswordMessage(UserDTO userDto, String secureToken, HttpServletRequest request) throws WebscanUtilitiesException {
		String[ ] args = new String[1];
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se construye el correo electrónico para la recuperación de la password del usuario " + userDto.getUsername() + ".");
		}
		String subjectEmailMsg = messages.getMessage(UsersWebscanConstants.RETRIEVE_PASS_EMAIL_SUBJECT, args, request.getLocale());
		args[0] = secureToken;
		String bodyEmailMsg = messages.getMessage(UsersWebscanConstants.RETRIEVE_PASS_EMAIL_BODY, args, request.getLocale());

		MailMessage mailMessage = new MailMessage();
		mailMessage.setBodyMessage(bodyEmailMsg);
		mailMessage.setSubject(subjectEmailMsg);
		mailMessage.setToRecipients(userDto.getEmail());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se procede al envío del correo electrónico para la recuperación de la password del usuario " + userDto.getUsername() + ".");
		}
		webscanMailService.sendMail(mailMessage);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Correo electrónico para la recuperación de la password del usuario " + userDto.getUsername() + " enviado correctamente.");
		}
	}

	/**
	 * Añade log de auditoria al recuperar el password de usuario.
	 * 
	 * @param username
	 *            Identificador del nombre de usuario.
	 * @param comSendingDate
	 *            Fecha de envio de datos.
	 * @param securityToken
	 *            Token de seguridad.
	 * @return Identificador del la nuevo log de auditoria.
	 * @throws DAOException
	 *             Exception producida al acceder a BBDD.
	 */
	private Long createRecoveryPasswordAuditLogs(String username, Date comSendingDate, String securityToken) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		Long res;
		String additionalInfo;

		// Operación de auditoría
		auditOperation = new AuditOperationDTO();
		auditOperation.setName(AuditOperationName.USER_RECOVERY_PASSWORD);
		auditOperation.setStartDate(comSendingDate);
		auditOperation.setLastUpdateDate(comSendingDate);
		auditOperation.setStatus(AuditOperationStatus.OPEN);

		// Evento de auditoria
		auditEvent = new AuditEventDTO();

		additionalInfo = "Usuario: " + username + ". Código seguridad: " + securityToken;
		if (additionalInfo.length() > UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
			additionalInfo = additionalInfo.substring(0, UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
		}
		auditEvent.setAdditionalInfo(additionalInfo);
		auditEvent.setEventDate(comSendingDate);
		auditEvent.setEventName(AuditEventName.USER_RECOVERY_PASSWORD_REQ);
		auditEvent.setEventResult(AuditOpEventResult.OK);
		auditEvent.setEventUser(username);

		auditEvents.add(auditEvent);

		res = auditService.addNewAuditLogs(auditOperation, auditEvents, Boolean.FALSE);

		return res;
	}

	/**
	 * Recupera la petición de recuperación de password en proceso más reciente
	 * para un usuario determinado, identificado mediante su dirección de correo
	 * electrónico.
	 * 
	 * @param userEmail
	 *            dirección de correo electrónico de un usuario.
	 * @return petición de recuperación de password solicitada.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	private RecoveryUserPasswordRequestDTO getRecoveryPassReqInProcess(String userEmail) throws UsersDAOException {
		RecoveryUserPasswordRequestDTO res;

		res = usersWebscanModelManager.getRecoveryPassReqInProcessByUserEmail(userEmail);

		return res;
	}

	/**
	 * Fianliza una petición de recuperación de password, actualizando el
	 * password del usuario y la petición en BBDD.
	 * 
	 * @param username
	 *            Nombre de usuario.
	 * @param password
	 *            Nueva password del usuario.
	 * @param request
	 *            Petición web.
	 * @throws DAOException
	 *             Excepción producida al acceder a la información del modelo de
	 *             datos base.
	 * @throws UsersDAOException
	 *             Excepción producida al acceder al repositorio interno de
	 *             usuarios.
	 */
	public void performRecoveryUserPasswordRequest(final String username, final String password, final HttpServletRequest request) throws UsersDAOException, DAOException {
		RecoveryUserPasswordRequestDTO recPassReq;
		UserDTO userDto;

		LOG.debug("[WEBSCAN-USERS] Se actualiza la password del usuario {} tras una petición de reestablecimiento de password.", username);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se recupera información del usuario {} de BBDD.", username);
		}
		userDto = getUserToRetrievePassword(username);

		// Se recupera la petición de recuperación de password
		LOG.debug("[WEBSCAN-USERS] Se recupera la transacción de recuperación de password de BBDD...");
		recPassReq = getRecoveryPassReqInProcess(userDto.getEmail());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Transacción de recuperación de password de BBDD obtenida: {}.", recPassReq.getId());
		}
		// Se modifica el usuario en BBDD
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se modifica la password del usuario {} y finaliza la transacción de recuperación de password en BBDD.", username);
		}

		userDto.setPassword(password);
		recPassReq.setRecoveryDate(new Date());
		saveRecoveryUserPasswordRequest(userDto, recPassReq);

		// Se actualiza el contexto de seguridad y datos de sesión
		addUserToSessionAndSecurityContext(userDto, request);

		// Se registran pistas de auditoria
		LOG.debug("[WEBSCAN-USERS] Se registra la traza de auditoría...");
		updateRecoveryPasswordAuditLogs(userDto.getUsername(), recPassReq);

		Date loginEventDate = new Date();
		logLoginSucceeded(userDto.getUsername(), loginEventDate);

		LOG.debug("[WEBSCAN-USERS] Password del usuario {} actualizada correctamente tras una petición de reestablecimiento de password.", username);
	}

	/**
	 * Actualiza una petición de recuperación de password, actualizando el
	 * password del usuario y la petición en BBDD.
	 * 
	 * @param userDto
	 *            Usuario.
	 * @param recUserPassReq
	 *            Petición de recuperación de password.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error al ejecutar las setencias de BBDD.
	 */
	private void saveRecoveryUserPasswordRequest(final UserDTO userDto, RecoveryUserPasswordRequestDTO recUserPassReq) throws UsersDAOException {

		// Se modifica el usuario en BBDD
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se actuliza la password del " + userDto.getUsername() + " en BBDD.");
		}
		updateUserPassword(userDto);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se finaliza la de recuperación de password de BBDD obtenida: " + recUserPassReq.getId() + ".");
		}
		usersWebscanModelManager.performRecoveryUserPasswordRequest(recUserPassReq);
	}

	/**
	 * Actualiza el log de auditoria al recuperar el password.
	 * 
	 * @param username
	 *            Identificador del nombre de usuario.
	 * @param recPassReq
	 *            Objeto con la información de la petición de recuperación de
	 *            password de usuario.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */
	private void updateRecoveryPasswordAuditLogs(String username, RecoveryUserPasswordRequestDTO recPassReq) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;

		auditOperation = auditService.getAuditOperation(recPassReq.getAuditOpId());
		auditOperation.setStatus(AuditOperationStatus.CLOSED_OK);
		auditOperation.setLastUpdateDate(new Date());
		auditEvent = new AuditEventDTO();

		additionalInfo = "Password del usuario: " + username + " reestablecida correctamente.";
		if (additionalInfo.length() > UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
			additionalInfo = additionalInfo.substring(0, UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
		}
		auditEvent.setAdditionalInfo(additionalInfo);
		auditEvent.setEventDate(new Date());
		auditEvent.setEventName(AuditEventName.USER_RECOVERY_PASSWORD_CONFIRM);
		auditEvent.setEventResult(AuditOpEventResult.OK);
		auditEvent.setEventUser(username);
		auditEvents.add(auditEvent);

		auditService.closeAuditLogs(auditOperation, auditEvents);
	}

	/**
	 * Genera las trazas de auditoría correspondientes a un inicio de sesión
	 * correcto de un usuario determinado.
	 * 
	 * @param username
	 *            Nombre de usuario del usuario que accede al sistema.
	 * @param authTimestamp
	 *            Instante en el que accede al sistema.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	private void logLoginSucceeded(String username, Date authTimestamp) throws DAOException {
		// Se registran las trazas de auditoría
		auditService.createAuditLogs(username, AuditOpEventResult.OK, authTimestamp, null, AuditOperationName.USER_LOGIN, AuditEventName.USER_LOGIN);
	}

	/**
	 * Genera las trazas de auditoría correspondientes a un inicio de sesión
	 * fallido de un usuario determinado.
	 * 
	 * @param username
	 *            Nombre de usuario del usuario que intentó acceder al sistema.
	 * @param authTimestamp
	 *            Instante en el que intentó acceder al sistema.
	 * @param e
	 *            error de autenticación
	 * @throws DAOException
	 *             Si sucede algún error al registrar la traza de auditoría en
	 *             BBDD.
	 * @throws UsersDAOException
	 *             si no existe el usuario.
	 */
	public void logLoginFailed(String username, Date authTimestamp, AuthenticationException e) throws DAOException, UsersDAOException {
		UserDTO userDto;
		int userAttempts, maxAttemps;

		maxAttemps = getMaxAttemptsAllowed();

		userDto = usersWebscanModelManager.getUserByUsername(username);

		if (!userDto.getLocked() && maxAttemps >= 0) {
			if (e instanceof BadCredentialsException) {
				// Se actualiza el número de intentos fallidos, y se bloquea el
				// usuario en caso de igualarse o superarse el máximo admitido

				if (userDto.getFailedAttempts() == null) {
					userAttempts = 0;
				} else {
					userAttempts = userDto.getFailedAttempts().intValue();
				}
				userAttempts++;
				userDto.setFailedAttempts(userAttempts);

				if (userAttempts >= maxAttemps) {
					userDto.setLocked(Boolean.TRUE);
				}
				usersWebscanModelManager.updateUser(userDto, Boolean.FALSE);
			}
		}

		// Se registran las trazas de auditoría
		auditService.createAuditLogs(username, AuditOpEventResult.ERROR, authTimestamp, e, AuditOperationName.USER_LOGIN, AuditEventName.USER_LOGIN);
	}

	/**
	 * LLeva las operaciones necesarias para actualizar la password de un
	 * usuario registrado en el sistema.
	 * 
	 * @param username
	 *            nombre de usuario.
	 * @param password
	 *            nueva password.
	 * @param upadtePassEventDate
	 *            Instante en el que se registra la solicitud de actualziación
	 *            de password.
	 * @param request
	 *            petición web.
	 * @throws DAOException
	 *             Excepción producida al acceder a la información del modelo de
	 *             datos base.
	 * @throws UsersDAOException
	 *             Excepción producida al acceder al repositorio interno de
	 *             usuarios.
	 */
	public void performUpdateUserPassword(String username, String password, Date upadtePassEventDate, HttpServletRequest request) throws UsersDAOException, DAOException {
		UserDTO userDto;

		// Se recupera el usuario de BBDD
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se recupera información del usuario {} de BBDD.", username);
		}

		userDto = getUserByUsername(username);

		// Se modifica el usuario en BBDD
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se modifica la password del usuario {} en BBDD.", username);
		}
		userDto.setPassword(password);
		userDto = updateUserPassword(userDto);

		// Se actualiza el contexto de seguridad y datos de sesión
		addUserToSessionAndSecurityContext(userDto, request);

		// Se generan trazas de auditoría
		createUpdatePasswordAuditLogs(username, AuditOpEventResult.OK, upadtePassEventDate, null);

		Date loginEventDate = new Date();
		// TODO - Pongo en cuarentena, por si es necesario reutilizar metodo
		logLoginSucceeded(userDto.getUsername(), loginEventDate);
	}

	/**
	 * Añade la información de usuario a sesión y el contexto de seguridad.
	 * 
	 * @param userDto
	 *            Objeto con la información del usuario.
	 * @param request
	 *            Petición http.
	 * @throws UsersDAOException
	 *             Exception producida al recuperar la información del usuario.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de la BBDD.
	 */
	private void addUserToSessionAndSecurityContext(UserDTO userDto, HttpServletRequest request) throws UsersDAOException, DAOException {
		Authentication auth;
		Date lastAccessDate;
		UserDetails userDetails;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se inicia el establecimiento de información en el contexto seguridad y sesión para el usuario " + userDto.getUsername() + " ...");
		}
		userDetails = getUserDetailsByUsername(userDto.getUsername());

		LOG.debug("[WEBSCAN-USERS] Actualizando información de acceso al sistema del usuario en BBDD...");

		lastAccessDate = getUserLastAccess(userDto);
		updateUserLastAccess(userDto);

		LOG.debug("[WEBSCAN-USERS] Actualizando información en sesión ...");
		request.getSession().setAttribute(WebscanBaseConstants.USER_LAST_ACCESS_SESSION_ATT_NAME, lastAccessDate);
		request.getSession().setAttribute(WebscanBaseConstants.USER_SESSION_ATT_NAME, userService.updateUserInSession(userDto.getId()));
		request.getSession().removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);

		LOG.debug("[WEBSCAN-USERS] Actualizando información en el contexto seguridad ...");

		auth = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), null, userDetails.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Finalizado el establecimiento de información en el contexto seguridad y sesión para el usuario " + userDto.getUsername() + " ...");
		}
	}

	/**
	 * Actualiza la password de un usuario, verificando previamente que el
	 * usuario no se encuentra bloqueado.
	 * 
	 * @param userDto
	 *            Usuario.
	 * @return Usuario con información actualizada.
	 * @throws UsersDAOException
	 *             Si el usuario se encuentra bloqueado o no existe.
	 */
	private UserDTO updateUserPassword(UserDTO userDto) throws UsersDAOException {
		UserDTO res = null;

		checkLockedUser(userDto);

		usersWebscanModelManager.updateUserPassword(userDto);
		res = getUserByUsername(userDto.getUsername());

		return res;
	}

	/**
	 * Añade la información a la auditoria al actualizar password de usuario.
	 * 
	 * @param username
	 *            Identificador del nombre de usuario.
	 * @param result
	 *            Objeto AuditOpEventResult con la información del resultado
	 *            obtenido al intentar actualizar o crear password.
	 * @param updatePassDate
	 *            Fecha de la actualización del password.
	 * @param exception
	 *            Exception producida al tratar cambio de password.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */
	private void createUpdatePasswordAuditLogs(String username, AuditOpEventResult result, Date updatePassDate, Exception exception) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se inicia el registro de la traza de auditoria por actualización de la password caducada del usuario " + username + " en BBDD.");
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Formando traza de auditoria con resultado " + result + "...");
		}
		auditOperation = new AuditOperationDTO();
		auditOperation.setName(AuditOperationName.USER_UPDATE_PASSWORD);
		auditOperation.setStartDate(updatePassDate);
		auditOperation.setLastUpdateDate(updatePassDate);

		LOG.debug("[WEBSCAN-USERS] Añadiendo eventos a la traza de auditoria ...");

		auditEvent = new AuditEventDTO();
		if (AuditOpEventResult.ERROR.equals(result)) {
			auditOperation.setStatus(AuditOperationStatus.CLOSED_ERROR);
			// Tamaño máximo del campo 256 caracteres
			additionalInfo = "Excepción: " + exception.getClass() + ". Mensaje: " + exception.getMessage();

			if (additionalInfo.length() > UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
				additionalInfo = additionalInfo.substring(0, UsersWebscanConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
			}

			auditEvent.setAdditionalInfo(additionalInfo);
		} else {
			auditOperation.setStatus(AuditOperationStatus.CLOSED_OK);
		}
		auditEvent.setEventDate(updatePassDate);
		auditEvent.setEventName(AuditEventName.USER_UPDATE_PASSWORD);
		auditEvent.setEventResult(result);
		auditEvent.setEventUser(username);
		auditEvents.add(auditEvent);

		LOG.debug("[WEBSCAN-USERS] Registrando traza de auditoria en BBDD ...");

		Long auditOpId = auditService.addNewAuditLogs(auditOperation, auditEvents, Boolean.TRUE);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Traza de auditoria " + auditOpId + " registrada en BBDD ...");
		}
	}

	/**
	 * Recupera el número de días restantes para que caduque la password de un
	 * usuario determinado.
	 * 
	 * @param nextPassUpdateDate
	 *            Fecha de fin de vigencia de la actual password del usuario.
	 * @return número de días restantes para que caduque la password del usuario
	 *         especificado como parámetro de entrada.
	 */
	public Integer getRemainingDaysWarnUpdatePassword(Date nextPassUpdateDate) {
		Date currentDate, limitWarnDate;
		Integer res = -1;
		Integer warnDays = getDaysWarnExpiredPass();
		currentDate = new Date();

		// Se calcula fecha a partir de la que se ha configurado el aviso a
		// usuarios de password próxima a caducar
		limitWarnDate = WebscanModelUtilities.addDays(nextPassUpdateDate, warnDays * -1);

		if (currentDate.after(limitWarnDate)) {
			res = WebscanModelUtilities.differnceBetweenTwoDates(nextPassUpdateDate, currentDate);
		}

		return res;
	}

	/**
	 * Recupera el valor del parámetro de configuración que establece el número
	 * máximo de intentos consecutivos fallidos de inicio de sesión.
	 * 
	 * @return número máximo de intentos consecutivos fallidos de inicio de
	 *         sesión.
	 */
	private Integer getMaxAttemptsAllowed() {
		Integer res = UsersWebscanConstants.DEFAULT_MAX_ATTEMPS_VALUE;

		try {
			res = Integer.parseInt(confParameters.getProperty(UsersWebscanConstants.LOGIN_MAX_ATTEMPS_PROP_NAME));
		} catch (NumberFormatException exc) {
			// No hacer nada
		}

		return res;
	}

	/**
	 * Recupera el periodo (número de días), en el cual será avisado un usuario
	 * que su password va a caducar.
	 * 
	 * @return Número de días a partir del que se debe avisar a un usario que su
	 *         password va a caducar.
	 */
	private Integer getDaysWarnExpiredPass() {
		Integer res = UsersWebscanConstants.DEFAULT_DAYS_WARN_EXPIRED_PASS;

		try {
			res = Integer.parseInt(confParameters.getProperty(UsersWebscanConstants.PASS_DAYS_WARN_EXPIRED_PROP_NAME));
		} catch (NumberFormatException exc) {
			// No hacer nada
		}

		return res;
	}

	/**
	 * Comprueba si un usuario se encuentra bloqueado o inhabilitado.
	 * 
	 * @param userDto
	 *            Usuario.
	 * @throws UsersDAOException
	 *             Si el usuario se encuentra bloqueado o inhabilitado.
	 */
	private void checkLockedUser(UserDTO userDto) throws UsersDAOException {
		Boolean lockedUser, enabledUser, userPasswordExpired;
		String excMsg;

		userPasswordExpired = isUserPasswordExpired(userDto);
		enabledUser = !userDto.getLocked();
		lockedUser = isLockedUser(userDto);

		if (!enabledUser || lockedUser) {
			excMsg = "[WEBSCAN-AUTH] Usuario " + userDto.getUsername() + " bloqueado o no habilitado.";
			LOG.error(excMsg);
			throw new UsersDAOException(UsersDAOException.CODE_800, excMsg);
		}

		if (userPasswordExpired) {
			excMsg = "[WEBSCAN-AUTH] Usuario " + userDto.getUsername() + " con password caducada.";
			LOG.warn(excMsg);
		}
	}

	/**
	 * Verifica si se ha superado el número máximo de intentos consecutivos
	 * fallidos de inicio de sesión.
	 * 
	 * @param numAttempts
	 *            Número de intentos consecutivos de inicio de sesión.
	 * @return Si se ha superado el número máximo de intentos consecutivos
	 *         fallidos de inicio de sesión, devuelve true. En caso contrario,
	 *         false.
	 */
	private Boolean isMaxAttempsExceeded(Integer numAttempts) {
		Boolean res = Boolean.FALSE;
		Integer maxAttemps = getMaxAttemptsAllowed();

		res = maxAttemps >= 0 && numAttempts >= maxAttemps;

		return res;
	}

	/**
	 * Comprueba si un usuario está bloqueado. Para ello debe estar inhabilitado
	 * y haber superado el número máximo de intentos consecutivos fallidos de
	 * inicio de sesión.
	 * 
	 * @param userDto
	 *            Usuario.
	 * @return Si el usuario está bloqueado, devuelve true. En caso contrario,
	 *         false.
	 */
	private Boolean isLockedUser(UserDTO userDto) {
		Boolean res;

		res = userDto.getLocked() && isMaxAttempsExceeded(userDto.getFailedAttempts());

		return res;
	}

	/**
	 * Verifica que no esté caducada el password de un usuario.
	 * 
	 * @param userDto
	 *            Usuario.
	 * @return Si el usuario tiene la password caducada, devuelve true. En caso
	 *         contrario, false.
	 */
	private Boolean isUserPasswordExpired(UserDTO userDto) {
		Boolean res;
		Date currentDate = new Date();

		res = currentDate.compareTo(userDto.getNextPassUpdateDate()) > 0;

		return res;
	}

	/**
	 * Genera el token de seguridad de una petición de recuperación de password.
	 * 
	 * @return token de seguridad de una petición de recuperación de password.
	 */
	private String generateSecureToken() {
		final int MAX_VAL = 150;
		final int MAX_SIZE = 32;
		String res = new BigInteger(MAX_VAL, SECURE_RANDOM).toString(MAX_SIZE);

		return res;
	}

	/**
	 * Establece la fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos de gestión de usuarios de Webscan.
	 * 
	 * @param anUsersWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos de gestión de usuarios de
	 *            Webscan.
	 */
	public void setUsersWebscanModelManager(UsersWebscanModelManager anUsersWebscanModelManager) {
		this.usersWebscanModelManager = anUsersWebscanModelManager;
	}

	/**
	 * Establece el servicio que agrupa operaciones de consulta, creación y
	 * modificación sobre usuarios del sistema.
	 * 
	 * @param anUserService
	 *            nuevo servicio que agrupa operaciones de consulta, creación y
	 *            modificación sobre usuarios del sistema.
	 */
	public void setUserService(UserService anUserService) {
		this.userService = anUserService;
	}

	/**
	 * Establece los parámetros de configuración del módulo de gestión de
	 * usuarios de Webscan.
	 * 
	 * @param anyConfParameters
	 *            parámetros de configuración del módulo de gestión de usuarios
	 *            de Webscan.
	 */
	public void setConfParameters(Properties anyConfParameters) {
		this.confParameters.clear();

		if (anyConfParameters != null && !anyConfParameters.isEmpty()) {
			this.confParameters.putAll(anyConfParameters);
		}
	}

	/**
	 * Establece el servicio interno para el registro y consulta de trazas y
	 * eventos de auditoría de Webscan.
	 * 
	 * @param anAuditService
	 *            nuevo servicio interno para el registro y consulta de trazas y
	 *            eventos de auditoría de Webscan.
	 */
	public void setAuditService(AuditService anAuditService) {
		this.auditService = anAuditService;
	}

	/**
	 * Establece un nuevo objeto de mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Nuevo objeto de mensajes, sujetos a multidioma, que son
	 *            presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}

	/**
	 * Establece el servicio responsable del envío de correos electrónicos.
	 * 
	 * @param aWebscanMailService
	 *            Servicio responsable del envío de correos electrónicos.
	 */
	public void setWebscanMailService(MailService aWebscanMailService) {
		this.webscanMailService = aWebscanMailService;
	}
}
