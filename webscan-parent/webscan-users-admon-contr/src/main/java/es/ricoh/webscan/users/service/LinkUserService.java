/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.service.UserService.java.</p>
 * <b>Descripción:</b><p> Lógica de negocio implementada para laa operaciones de vinculación y
 * desvinculación de usuarios a unidades organizativas.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.model.UsersWebscanModelManager;
import es.ricoh.webscan.users.model.dto.UserDTO;

/**
 * Lógica de negocio implementada para laa operaciones de vinculación y
 * desvinculación de usuarios a unidades organizativas.
 * <p>
 * Clase LinkUserService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class LinkUserService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LinkUserService.class);

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos de gestión de usuarios de Webscan.
	 */
	private UsersWebscanModelManager usersWebscanModelManager;

	/**
	 * Obtiene el listado de usuarios pertenecientes a un unidad organizativa
	 * determinada.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return listado de usuarios de una unidad organizativa.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<UserDTO> getUserResponse(Long orgUnitId) throws UsersDAOException {
		LOG.debug("OrganizationService getUserResponse start");
		List<UserDTO> usuarios = new ArrayList<UserDTO>();

		// parameters Long orgUnitId, Long pageNumber, Long elementsPerPage,
		// List<EntityOrderByClause> orderByColumns
		usuarios = usersWebscanModelManager.getUsersByOrganizationUnit(orgUnitId, null, null, null);

		LOG.debug("OrganizationService getUserResponse end");
		return usuarios;

	}

	/**
	 * Obtiene el listado de usuarios registrados en el repositorio interno de
	 * usuario de Webscan.
	 * 
	 * @return listado de usuarios.
	 * @throws UsersDAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<UserDTO> getUserListResponse() throws UsersDAOException {
		LOG.debug("OrganizationService getUserResponse start");

		List<UserDTO> usuarios = new ArrayList<UserDTO>();

		usuarios = usersWebscanModelManager.listUsers(null, null, null);

		LOG.debug("OrganizationService getUserResponse end");
		return usuarios;

	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos de gestión de usuarios de Webscan.
	 * 
	 * @param anUsersWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos de gestión de usuarios de
	 *            Webscan.
	 */
	public void setUsersWebscanModelManager(UsersWebscanModelManager anUsersWebscanModelManager) {
		this.usersWebscanModelManager = anUsersWebscanModelManager;
	}

}
