/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.vo.PasswordVO.java.</p>
 * <b>Descripción:</b><p> Objeto de la capa de presentación que habilita la edición de passwords de usuarios.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.controller.vo;

import java.io.Serializable;

/**
 * Objeto de la capa de presentación que habilita la edición de passwords de
 * usuarios.
 * <p>
 * Clase PasswordVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class PasswordVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Password.
	 */
	private String password = "";

	/**
	 * Confirmación de password.
	 */
	private String confirmPassword = "";

	/**
	 * Constructor sin argumentos.
	 */
	public PasswordVO() {
		super();
	}

	/**
	 * Obtiene la password.
	 * 
	 * @return la password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Establece un nuevo valor para la password.
	 * 
	 * @param aPassword
	 *            nuevo valor para la password.
	 */
	public void setPassword(String aPassword) {
		this.password = aPassword;
	}

	/**
	 * Obtiene la confirmación de password.
	 * 
	 * @return la confirmación de password.
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * Establece un nuevo valor para la confirmación de password.
	 * 
	 * @param aConfirmPassword
	 *            nuevo valor para la confirmación de password.
	 */
	public void setConfirmPassword(String aConfirmPassword) {
		this.confirmPassword = aConfirmPassword;
	}

}
