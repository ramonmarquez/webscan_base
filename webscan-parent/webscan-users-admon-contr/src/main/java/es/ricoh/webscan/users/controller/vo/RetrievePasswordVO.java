/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.vo.RetrievePasswordVO.java.</p>
 * <b>Descripción:</b><p> Clase que agrupa la información del formulario de recuperación de password de usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.controller.vo;

import java.io.Serializable;

/**
 * Clase que agrupa la información del formulario de recuperación de password de
 * usuario.
 * <p>
 * Clase RetrievePasswordVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class RetrievePasswordVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nombre de usuario.
	 */
	private String username;

	/**
	 * Documento de identificación del usuario.
	 */
	private String document;

	/**
	 * Token de seguridad.
	 */
	private String securityToken;

	/**
	 * Password de usuario y confirmación.
	 */
	private PasswordVO passwordForm = new PasswordVO();

	/**
	 * Constructor sin argumentos.
	 */
	public RetrievePasswordVO() {
		super();
	}

	/**
	 * Obtiene el nombre de usuario.
	 * 
	 * @return el nombre de usuario.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece el nombre de usuario.
	 * 
	 * @param anUsername
	 *            un nombre de usuario.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene el documento de identificación del usuario.
	 * 
	 * @return el documento de identificación del usuario.
	 */
	public String getDocument() {
		return document;
	}

	/**
	 * Establece el documento de identificación del usuario.
	 * 
	 * @param aDocument
	 *            un documento de identificación de usuario.
	 */
	public void setDocument(String aDocument) {
		this.document = aDocument;
	}

	/**
	 * Obtiene el token de seguridad.
	 * 
	 * @return el token de seguridad.
	 */
	public String getSecurityToken() {
		return securityToken;
	}

	/**
	 * Establece el token de seguridad.
	 * 
	 * @param aSecurityToken
	 *            un token de seguridad.
	 */
	public void setSecurityToken(String aSecurityToken) {
		this.securityToken = aSecurityToken;
	}

	/**
	 * Obtiene la password de usuario y su confirmación.
	 * 
	 * @return la password de usuario y su confirmación.
	 */
	public PasswordVO getPasswordForm() {
		return passwordForm;
	}

	/**
	 * Establece la password de usuario y su confirmación.
	 * 
	 * @param aPasswordForm
	 *            una password de usuario y su confirmación.
	 */
	public void setPasswordForm(PasswordVO aPasswordForm) {
		this.passwordForm = aPasswordForm;
	}

}
