/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.LoginController.java.</p>
 * <b>Descripción:</b><p> Controlador de inicio y cierre de sesión.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controlador de inicio y cierre de sesión.
 * <p>
 * Clase LoginController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Controller
public class LoginController {

	/**
	 * Página de login.
	 * 
	 * @param request
	 *            Petición http.
	 * @return nombre de plantilla a mostrar.
	 */
	@RequestMapping(name = "/login", method = { RequestMethod.GET, RequestMethod.POST }, value = "/login")
	public String login(final HttpServletRequest request) {

		UsersControllerUtils.clearAuthenticationAttributes(request);

		return "login";
	}

	/**
	 * Página de logout.
	 * 
	 * @return nombre de plantilla a mostrar.
	 */
	@RequestMapping(name = "/logout", method = RequestMethod.POST)
	public String logout() {
		return "logout";
	}

	/**
	 * Página de error por sesión inválida.
	 * 
	 * @param locale
	 *            Language de la petición http.
	 * @param model
	 *            Modelo (datos de la vista).
	 * @return nombre de plantilla a mostrar.
	 */
	@RequestMapping("/invalidSession")
	public String invalidSession(final Locale locale, final Model model) {
		return "invalidSession";
	}
}
