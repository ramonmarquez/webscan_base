/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.UserOrgUnitsAdmonController.java.</p>
 * <b>Descripción:</b><p> Controlador que agrupa las funcionalidades ejecutadas sobre los usuarios
 * vinculados a unidades organizativas desde la sección de administración de
 * unidades organizativas.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.controller;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.ricoh.webscan.admon.AdmonWebscanConstants;
import es.ricoh.webscan.admon.orgunit.service.OrgUnitService;
import es.ricoh.webscan.admon.orgunit.vo.FormOrganizationVO;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.users.service.UserService;

/**
 * Controlador que agrupa las funcionalidades ejecutadas sobre los usuarios
 * vinculados a unidades organizativas desde la sección de administración de
 * unidades organizativas.
 * <p>
 * Clase UserOrgUnitsAdmonController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

@Controller
@RequestMapping("/admin")
public class UserOrgUnitsAdmonController {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserOrgUnitsAdmonController.class);

	/**
	 * Fachada de acceso a los servicios de usuarios.
	 */
	@Autowired
	private UserService userService;

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre las operaciones relacionadas con las Unidades Organizativas.
	 */
	@Autowired
	protected OrgUnitService orgUnitService;

	/**
	 * Request para guardar la información asociada a las unidades organizativas
	 * y usuarios.
	 * 
	 * @param request
	 *            Petición que se realiza desde el navegador.
	 * @param model
	 *            parametro de entrada y salida para rellenar la información a
	 *            mostrar en la vista.
	 * @param formData
	 *            parametro con la información recogida de la vista de la
	 *            petición.
	 * @return nombre de plantilla a mostrar.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 * @throws NumberFormatException
	 *             Exception producida al realizar una operación de
	 *             transformación de número.
	 * @throws JSONException
	 *             Exception producida al recuperar valores de un string JSON.
	 */
	@RequestMapping(value = "/organizationUnitUsers", method = RequestMethod.POST)
	public String userOrgParamSaves(HttpServletRequest request, ModelMap model, @ModelAttribute FormOrganizationVO formData) throws DAOException, NumberFormatException, JSONException {
		Boolean error = Boolean.FALSE;
		OrganizationUnitDTO orgUnit;
		LOG.debug("UserEditController userOrgParamSave start");

		try {
			if (formData.getAction().equals("delete")) {

				// verificamos que el valor del identificador sea válido
				if (!StringUtils.isEmpty(formData.getIdIdentificador())) {
					orgUnitService.setUnitOrganizationRemove(request, Long.valueOf(formData.getIdIdentificador()));
					orgUnitService.refreshOrgUnitTree(formData, model);
					orgUnitService.messageSystem(WebScope.REQUEST, ViewMessageLevel.INFO, AdmonWebscanConstants.ADMON_RESULT_OK, "");
				} else {
					// podriamos mostrar un mensaje de error debido a una
					// llamada
					// incoherente
					error = Boolean.TRUE;
				}
			} else {
				orgUnit = orgUnitService.fillOrgUnit(formData);
				if (LOG.isInfoEnabled()) {
					LOG.info("INI Enviamos dato a guardar: id-" + orgUnit.getId() + ", parentID-" + orgUnit.getParentId() + ", idExterno-" + orgUnit.getExternalId() + ", name-" + orgUnit.getName());
				}
				String orgUnitAppUsers = formData.getOrgUnitAppUsers();
				if (LOG.isInfoEnabled()) {
					LOG.info("valor a enviar de usuarios:" + orgUnitAppUsers);
				}
				userService.setUnitOrganization(request, orgUnit, orgUnitAppUsers);
				if (LOG.isInfoEnabled()) {
					LOG.info("FIN Enviamos dato a guardar: id-" + orgUnit.getId() + ", parentID-" + orgUnit.getParentId() + ", idExterno-" + orgUnit.getExternalId() + ", name-" + orgUnit.getName());
				}
				// Actualización de información en pantalla
				if (error) {
					orgUnitService.messageSystem(WebScope.REQUEST, ViewMessageLevel.WARN, AdmonWebscanConstants.ADMON_ORG_UNIT_NO_INFO, "");
				} else {
					orgUnitService.refreshOrgUnitTree(formData, model);
					orgUnitService.messageSystem(WebScope.REQUEST, ViewMessageLevel.INFO, AdmonWebscanConstants.ADMON_RESULT_OK, "");
				}
			}
		} catch (Exception e) {
			orgUnitService.messageSystem(WebScope.REQUEST, ViewMessageLevel.ERROR, AdmonWebscanConstants.ADMON_DEFAULT_ERROR, e.toString());
		}

		LOG.debug("UserEditController userOrgParamSave end");
		return "admin/organizationUnit :: content";

	}

}
