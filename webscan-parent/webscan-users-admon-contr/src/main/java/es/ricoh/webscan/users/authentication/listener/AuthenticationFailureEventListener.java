/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.authentication.listener.AuthenticationFailureEventListener.java.</p>
 * <b>Descripción:</b><p> Listener que anota en auditoría los intentos fallidos de inicio de sesión en
 * el sistema.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.authentication.listener;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.service.LoginService;

/**
 * Listener que anota en auditoría los intentos fallidos de inicio de sesión en
 * el sistema.
 * <p>
 * Clase AuthenticationFailureEventListener.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class AuthenticationFailureEventListener implements ApplicationListener<AbstractAuthenticationFailureEvent> {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationFailureEventListener.class);

	/**
	 * Servicio que agrupa operaciones asociadas al inicio y finalización de
	 * sesión en el sistema por parte de usuarios.
	 */
	private LoginService loginService;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(AbstractAuthenticationFailureEvent event) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Acceso fallido al sistema del usuario " + event.getAuthentication().getName() + ", se registra la traza de auditoria.");
			}

			loginService.logLoginFailed(event.getAuthentication().getName(), new Date(event.getTimestamp()), event.getException());

			LOG.debug("Traza de auditoria registrada.");
		} catch (UsersDAOException e) {

		} catch (DAOException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn("Se produjo un error registrando la traza de auditoria por un acceso faliido al sistema del usuario " + event.getAuthentication().getName(), e);
			}
		}
	}

	/**
	 * Establece el servicio responsable de resolver las operaciones asociadas
	 * al inicio y finalización de sesión en el sistema por parte de usuarios.
	 * 
	 * @param aLoginService
	 *            servicio responsable de resolver las operaciones asociadas al
	 *            inicio y finalización de sesión en el sistema por parte de
	 *            usuarios.
	 */
	public void setLoginService(LoginService aLoginService) {
		this.loginService = aLoginService;
	}

}
