/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.dto.ProfileVO.java.</p>
 * <b>Descripción:</b><p> Objeto de la capa de presentación que representa un perfil de usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.controller.vo;

import java.io.Serializable;

/**
 * Objeto de la capa de presentación que representa un perfil de usuario.
 * <p>
 * Clase ProfileVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class ProfileVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador del perfil de usuario.
	 */
	private Long id;

	/**
	 * Denominación del perfil de usuario.
	 */
	private String name;

	/**
	 * Constructor sin argumentos.
	 */
	public ProfileVO() {
		super();
	}

	/**
	 * Obtiene el identificador del perfil de usuario.
	 * 
	 * @return el identificador del perfil de usuario.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador del perfil de usuario.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador del perfil de usuario.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación del perfil de usuario.
	 * 
	 * @return la denominación del perfil de usuario.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación del perfil de usuario.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación del perfil de usuario.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfileVO other = (ProfileVO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
