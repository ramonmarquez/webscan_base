/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.validation.RetrievePasswordValidator.java.</p>
 * <b>Descripción:</b><p> Clase que valida el formulario de petición de recuperación de password de usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import es.ricoh.webscan.users.controller.vo.ReqRetrievePasswordVO;

/**
 * Clase que valida el formulario de petición de recuperación de password de
 * usuario.
 * <p>
 * Clase ReqRetrievePasswordValidator.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ReqRetrievePasswordValidator implements Validator {

	/**
	 * Validador que comprueba que una de caracteres presente el formato de un
	 * dirección de correo electrónico.
	 */
	private EmailPatternValidator emailPatternValidator;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return ReqRetrievePasswordVO.class.isAssignableFrom(clazz);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 *      org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		ReqRetrievePasswordVO reqRetrievePassword = (ReqRetrievePasswordVO) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "error.username.required", "Username is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "recPass.error.emailRequired", "Email is required.");
		ValidationUtils.invokeValidator(emailPatternValidator, reqRetrievePassword.getEmail(), errors);
	}

	/**
	 * Establece el validador que comprueba que una de caracteres presente el
	 * formato de un dirección de correo electrónico.
	 * 
	 * @param anEmailPatternValidator
	 *            nuevo validador que comprueba que una de caracteres presente
	 *            el formato de un dirección de correo electrónico.
	 */
	public void setEmailPatternValidator(EmailPatternValidator anEmailPatternValidator) {
		this.emailPatternValidator = anEmailPatternValidator;
	}

}
