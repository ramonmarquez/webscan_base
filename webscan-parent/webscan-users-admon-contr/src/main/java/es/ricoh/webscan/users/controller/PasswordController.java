/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.PasswordController.java.</p>
 * <b>Descripción:</b><p> Controlador que implementa las operaciones de restablecimiento de password y
 * modificación de password caducada.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.users.UsersWebscanConstants;
import es.ricoh.webscan.users.controller.vo.ReqRetrievePasswordVO;
import es.ricoh.webscan.users.controller.vo.RetrievePasswordVO;
import es.ricoh.webscan.users.controller.vo.UpdatePasswordVO;
import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.service.LoginService;
import es.ricoh.webscan.users.validation.ReqRetrievePasswordValidator;
import es.ricoh.webscan.users.validation.RetrievePasswordValidator;
import es.ricoh.webscan.users.validation.UpdatePasswordValidator;
import es.ricoh.webscan.utilities.WebscanUtilitiesException;

/**
 * Controlador que implementa las operaciones de restablecimiento de password y
 * modificación de password caducada.
 * <p>
 * Clase PasswordController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Controller
public class PasswordController {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PasswordController.class);

	/**
	 * Servicio que agrupa las distintas funcionalidades de inicio de sesión y
	 * modificación de password de usuario.
	 */
	@Autowired
	private LoginService loginService;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	@Autowired
	private MessageSource messageSource;

	/**
	 * Validador del formulario de petición de recuperación de password.
	 */
	@Autowired
	private ReqRetrievePasswordValidator reqRetrievePasswordValidator;

	/**
	 * Validador del formulario de recuperación de password.
	 */
	@Autowired
	private RetrievePasswordValidator retrievePasswordValidator;

	/**
	 * Validador del formulario de actualización de password caducada.
	 */
	@Autowired
	private UpdatePasswordValidator updatePasswordValidator;

	/**
	 * Método que asocia el formulario de petición de recuperación de password
	 * con su validador.
	 * 
	 * @param binder
	 *            Objeto responsable de enlazar el formulario con su validador.
	 */
	@InitBinder(value = "reqRetrievePassFormData")
	protected void initReqRetrievePassBinder(WebDataBinder binder) {
		binder.addValidators(reqRetrievePasswordValidator);
	}

	/**
	 * Método que asocia el formulario de recuperación de password con su
	 * validador.
	 * 
	 * @param binder
	 *            Objeto responsable de enlazar el formulario con su validador.
	 */
	@InitBinder(value = "retrievePassFormData")
	protected void initRetrievePassBinder(WebDataBinder binder) {
		binder.addValidators(retrievePasswordValidator);
	}

	/**
	 * Método que asocia el formulario de actualización de password caducada con
	 * su validador.
	 * 
	 * @param binder
	 *            Objeto responsable de enlazar el formulario con su validador.
	 */
	@InitBinder(value = "updatePassFormData")
	protected void initUpdatePassBinder(WebDataBinder binder) {
		binder.addValidators(updatePasswordValidator);
	}

	/**
	 * Controlador responsable de presentar la vista de actualización de
	 * password caducada.
	 * 
	 * @param model
	 *            Modelo (datos de la vista).
	 * @return Nombre de la vista donde se muestra la operación solicitada.
	 */
	@RequestMapping(value = "/updatePassword", method = RequestMethod.GET)
	public String updatePassword(final Model model) {
		model.addAttribute("updatePassFormData", new UpdatePasswordVO());
		return "updatePassword";
	}

	/**
	 * Controlador responsable de actualizar la password caducada de un usuario.
	 * 
	 * @param updatePassword
	 *            Datos del formulario.
	 * @param updateResult
	 *            Errores detectados en los datos de la petición (datos del
	 *            formulario).
	 * @param request
	 *            Petición web.
	 * @return Nombre de la vista donde se muestra la respuesta de la operación
	 *         solicitada.
	 */
	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	public String performUpdatePassword(@Valid @ModelAttribute("updatePassFormData") UpdatePasswordVO updatePassword, BindingResult updateResult, final HttpServletRequest request) {
		Date upadtePassEventDate = new Date();
		String res;

		res = "updatePassword";

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se solicita la actualización de la password caducada del usuario " + updatePassword.getUsername() + ".");
		}

		try {
			if (updateResult.hasErrors() && LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS] Se produjeron " + updateResult.getErrorCount() + " errores en el formulario de actualización de password caducada.");
			} else {
				// Se recupera el usuario de BBDD
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se actualiza en el sistema la password del usuario {}.", updatePassword.getUsername());
				}
				loginService.performUpdateUserPassword(updatePassword.getUsername(), updatePassword.getPasswordForm().getPassword(), upadtePassEventDate, request);

				LOG.debug("[WEBSCAN-USERS] Se generan los mensajes de respuesta a mostrar ...");

				UsersControllerUtils.addCommonMessage(UsersWebscanConstants.UPD_PASS_MSG_RESULT_OK, null, ViewMessageLevel.INFO, WebScope.REQUEST, request);

				res = "redirect:/main.html";

				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Password caducada del usuario {} actualizada correctamente.", updatePassword.getUsername());
				}
			}
		} catch (UsersDAOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-USERS] Se produjo un error al actualizar la password caducada del usuario {}, se obtiene el mensaje a presentar al usuario.", updatePassword.getUsername());
			}

			UsersControllerUtils.setUsersDAOExceptionInRequest(e, request, messageSource, Boolean.FALSE);
		} catch (DAOException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn("[WEBSCAN-USERS] Se produjo un error al registrar las trazas de auditoría del proceso de actualización de password para el usuario {}, se obtiene el mensaje a presentar al usuario.", updatePassword.getUsername());
			}

			UsersControllerUtils.setDefaultExceptionInRequest(e, request, messageSource, Boolean.FALSE);
		}

		return res;
	}

	/**
	 * Controlador responsable de presentar la vista de petición de recuperación
	 * de password.
	 * 
	 * @param model
	 *            Modelo (datos de la vista).
	 * @return Nombre de la vista donde se muestra la operación solicitada.
	 */
	@RequestMapping(value = "/reqRetrievePassword", method = RequestMethod.GET)
	public String reqRetrievePassword(final Model model) {
		model.addAttribute("reqRetrievePassFormData", new ReqRetrievePasswordVO());
		return "reqRetrievePassword";
	}

	/**
	 * Controlador responsable de iniciar y registrar una nueva solicitud de
	 * recuperación de password de usuario.
	 * 
	 * @param reqRetrievePassword
	 *            Datos del formulario.
	 * @param reqRetrieveResult
	 *            Errores detectados en los datos de la petición (datos del
	 *            formulario).
	 * @param request
	 *            Petición web.
	 * @return Nombre de la vista donde se muestra la respuesta de la operación
	 *         solicitada.
	 */
	@RequestMapping(value = "/reqRetrievePassword", method = RequestMethod.POST)
	public String performReqRetrievePassword(@Valid @ModelAttribute("reqRetrievePassFormData") ReqRetrievePasswordVO reqRetrievePassword, BindingResult reqRetrieveResult, final HttpServletRequest request) {
		Long recPassReqId;
		String res;

		res = "reqRetrievePassword";

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se solicita el inicio de la transacción de recuperación de password del usuario " + reqRetrievePassword.getUsername() + ".");
		}

		try {
			if (reqRetrieveResult.hasErrors() && LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS] Se produjeron " + reqRetrieveResult.getErrorCount() + " errores en el formulario de solicitud de recuperación de password de usuario.");
			} else {
				// Se recupera el usuario de BBDD
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se inicia la petición de recuperación en el sistema del usuario {}.", reqRetrievePassword.getUsername());
				}

				recPassReqId = loginService.initiateRecoveryUserPasswordRequest(reqRetrievePassword.getUsername(), reqRetrievePassword.getEmail(), request);

				// Se establece el valor de los parámetros retornados
				LOG.debug("[WEBSCAN-USERS] Se forma la respuesta para su presentación en pantalla ...");
				request.setAttribute(WebscanBaseConstants.REQUEST_RESULT_MSG_ATT_NAME, messageSource.getMessage(UsersWebscanConstants.REQ_REC_PASS_MSG_RESULT_OK, null, request.getLocale()));
				request.setAttribute(WebscanBaseConstants.REQUEST_RESULT_ATT_NAME, WebscanBaseConstants.REQUEST_RESULT_OK_VALUE);

				LOG.debug("[WEBSCAN-USERS] Transacción de recuperación de password {} del usuario {} iniciada correctamente.", recPassReqId, reqRetrievePassword.getUsername());
			}
		} catch (UsersDAOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-USERS] Se produjo un error al iniciar una transacción de recuperación de password del usuario " + reqRetrievePassword.getUsername() + ", se obtiene el mensaje a presentar al usuario");
			}
			UsersControllerUtils.setUsersDAOExceptionInRequest(e, request, messageSource, Boolean.FALSE);
		} catch (DAOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-USERS] Se produjo un error al iniciar una transacción de recuperación de password del usuario " + reqRetrievePassword.getUsername() + ", se obtiene el mensaje a presentar al usuario");
			}

			UsersControllerUtils.setDefaultExceptionInRequest(e, request, messageSource, Boolean.FALSE);
		} catch (WebscanUtilitiesException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-USERS] Se produjo un error al iniciar una transacción de recuperación de password del usuario " + reqRetrievePassword.getUsername() + ", se obtiene el mensaje a presentar al usuario");
			}
			UsersControllerUtils.setDefaultExceptionInRequest(e, request, messageSource, Boolean.FALSE);
		}
		return res;
	}

	/**
	 * Controlador responsable de presentar la vista de recuperación de
	 * password.
	 * 
	 * @param model
	 *            Modelo (datos de la vista).
	 * @return Nombre de la vista donde se muestra la operación solicitada.
	 */
	@RequestMapping(value = "/retrievePassword", method = RequestMethod.GET)
	public String retrievePassword(final Model model) {
		model.addAttribute("retrievePassFormData", new RetrievePasswordVO());
		return "retrievePassword";
	}

	/**
	 * Controlador encargado de finalizar una solicitud de recuperación de
	 * password de usuario ya iniciada.
	 * 
	 * @param retrievePassword
	 *            Datos del formulario.
	 * @param retrieveResult
	 *            Errores detectados en los datos de la petición (datos del
	 *            formulario).
	 * @param request
	 *            Petición web.
	 * @return Nombre de la vista donde se muestra la respuesta de la operación
	 *         solicitada.
	 */
	@RequestMapping(value = "/retrievePassword", method = RequestMethod.POST)
	public String performRetrievePassword(@Valid @ModelAttribute("retrievePassFormData") RetrievePasswordVO retrievePassword, BindingResult retrieveResult, final HttpServletRequest request) {
		String res;

		res = "retrievePassword";
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-USERS] Se inicia el reestablecimiento de password del usuario " + retrievePassword.getUsername() + ".");
		}

		try {
			if (retrieveResult.hasErrors() && LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS] Se produjeron " + retrieveResult.getErrorCount() + " errores en el formulario de recuperación de password de usuario.");
			} else {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se reestablece la password del usuario {} en BBDD.", retrievePassword.getUsername());
				}

				loginService.performRecoveryUserPasswordRequest(retrievePassword.getUsername(), retrievePassword.getPasswordForm().getPassword(), request);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se generan los mensajes de respuesta a mostrar ...");
				}
				UsersControllerUtils.addCommonMessage(UsersWebscanConstants.REC_PASS_MSG_RESULT_OK, null, ViewMessageLevel.INFO, WebScope.REQUEST, request);

				res = "redirect:/main.html";
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Password del usuario {} reestablecida correctamente.", retrievePassword.getUsername());
				}
			}
		} catch (UsersDAOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-USERS] Se produjo un error al finalizar una transacción de recuperación de password del usuario {}, se obtiene el mensaje a presentar al usuario", retrievePassword.getUsername());
			}

			UsersControllerUtils.setUsersDAOExceptionInRequest(e, request, messageSource, Boolean.FALSE);
		} catch (DAOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-USERS] Se produjo un error al finalizar una transacción de recuperación de password del usuario {}, se obtiene el mensaje a presentar al usuario", retrievePassword.getUsername());
			}

			UsersControllerUtils.setDefaultExceptionInRequest(e, request, messageSource, Boolean.FALSE);
		}

		return res;
	}

}
