/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.users.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.model.dto.UserDTO;
import es.ricoh.webscan.users.service.LinkUserService;

/**
 * Controlador de enlace de usuarios.
 * <p>
 * Clase UserLinkController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */

@Controller
@RequestMapping("/admin")
public class UserLinkController {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserLinkController.class);
	/**
	 * Objeto que representa el acceso a los servicio de asociación de usuarios.
	 */
	@Autowired
	private LinkUserService vincularUserService;

	/**
	 * Petición inicial para mostrar información de los usuarios.
	 * 
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @param idIdentificador
	 *            Identificador del usuario o vacio.
	 * @return String con la cadena de la vista de usuarios.
	 * @throws UsersDAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */
	@RequestMapping(value = "/usersList", method = RequestMethod.POST)
	public String usuariosUnit(Model model, @RequestParam("idIdentificador") String idIdentificador) throws UsersDAOException {
		LOG.debug("organizationUnitController usuariosUnit start");

		if (!StringUtils.isEmpty(idIdentificador)) {
			List<UserDTO> datos = vincularUserService.getUserResponse(Long.valueOf(idIdentificador));

			List<UserDTO> nuevos = vincularUserService.getUserListResponse();

			model.addAttribute("usuarios", datos);

			model.addAttribute("usuariosAll", nuevos);

			if (LOG.isDebugEnabled()) {
				LOG.debug("organizationUnitController datos recuperados:" + datos.size());
			}

		} else {
			List<UserDTO> nuevos = vincularUserService.getUserListResponse();

			// Recuperar todos los usuarios, independientemente de la UO a la
			// que pertenezcan
			model.addAttribute("usuarios", null);

			model.addAttribute("usuariosAll", nuevos);
		}

		LOG.debug("organizationUnitController usuariosUnit end");
		return "fragments/admin/content_tabla :: content";

	}

}
