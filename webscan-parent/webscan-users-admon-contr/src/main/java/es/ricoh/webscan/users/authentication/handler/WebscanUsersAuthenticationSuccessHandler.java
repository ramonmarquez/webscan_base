/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.authentication.handlers.WebscanUsersAuthenticationSuccessHandler.java.</p>
 * <b>Descripción:</b><p> Clase encargada de realizar los cambios necesarios en el sistema tras el
 * inicio de sesión correcto de un usuario.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.users.authentication.handler;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.servlet.LocaleResolver;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.users.UsersWebscanConstants;
import es.ricoh.webscan.users.controller.UsersControllerUtils;
import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.model.dto.UserDTO;
import es.ricoh.webscan.users.service.LoginService;
import es.ricoh.webscan.users.service.UserService;

/**
 * Clase encargada de realizar los cambios necesarios en el sistema tras el
 * inicio de sesión correcto de un usuario.
 * <p>
 * Clase WebscanUsersAuthenticationSuccessHandler.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class WebscanUsersAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WebscanUsersAuthenticationSuccessHandler.class);

	/**
	 * Servicio que agrupa operaciones asociadas al inicio y finalización de
	 * sesión en el sistema por parte de usuarios.
	 */
	private LoginService loginService;

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre usuarios del sistema.
	 */
	private UserService userService;

	/**
	 * Estrategia de redirección.
	 */
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Utilidad para la resolución de la configuración regional y de idioma del
	 * sistema.
	 */
	private LocaleResolver localeResolver;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.security.web.authentication.AuthenticationSuccessHandler#onAuthenticationSuccess(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse,
	 *      org.springframework.security.core.Authentication)
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		Date lastAccessDate;
		final HttpSession session;
		UserDTO userDto;

		session = request.getSession(false);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-AUTH] Inicio de sesión correcto del usuario " + authentication.getName() + ". Se inicia el registro de la traza de auditoría y completan los datos de sesión...");
		}
		try {
			redirectStrategy.sendRedirect(request, response, "/main.html");

			LOG.debug("[WEBSCAN-AUTH] Recuperando usuario de BBDD...");
			userDto = loginService.getUserByUsername(authentication.getName());
			lastAccessDate = loginService.getUserLastAccess(userDto);

			LOG.debug("[WEBSCAN-AUTH] registrando información de acceso del usuario {} en el sistema...", authentication.getName());
			loginService.saveAuthenticationSuccess(userDto);

			LOG.debug("[WEBSCAN-AUTH] Actualizando información en sesión ...");
			UsersControllerUtils.clearAuthenticationAttributes(request);
			session.setAttribute(WebscanBaseConstants.USER_LAST_ACCESS_SESSION_ATT_NAME, lastAccessDate);
			session.setAttribute(WebscanBaseConstants.USER_SESSION_ATT_NAME, userService.updateUserInSession(userDto.getId()));

			addUpdatePasswordWarnMessages(userDto, request);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-AUTH] Finalizadas las acciones tras inicio de sesión correcto del usuario " + authentication.getName() + ".");
			}
		} catch (UsersDAOException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn("[WEBSCAN-AUTH] Se produjo un error al actualizar información de acceso al sistema del usuario en BBDD " + "tras inicio de sesión correcto del usuario " + authentication.getName() + ": " + e.getMessage());
			}
			setAuthenticationExceptionErrorMsg(request, response, e);
		} catch (DAOException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn("[WEBSCAN-AUTH] Se produjo un error al actualizar información de acceso al sistema del usuario en BBDD " + "tras inicio de sesión correcto del usuario " + authentication.getName() + ": " + e.getMessage());
			}
			if (!DAOException.CODE_901.equals(e.getCode())) {
				// Ha ocurrido un error no controlado al recuperar la última
				// fecha de acceso del usuario
				setAuthenticationExceptionErrorMsg(request, response, e);
			}
		}
	}

	/**
	 * Establece el valor de los atributos de sesión que muestran información de
	 * error.
	 * 
	 * @param request
	 *            petición web.
	 * @param response
	 *            respuesta web.
	 * @param e
	 *            excepción producida.
	 * @throws IOException
	 *             Si ocurre algún error al efectuar la redirección a la página
	 *             de error.
	 */
	protected void setAuthenticationExceptionErrorMsg(HttpServletRequest request, HttpServletResponse response, final Exception e) throws IOException {
		final HttpSession session = request.getSession(false);
		Locale locale;
		Object[ ] args = new String[1];
		String errorMessage;

		locale = localeResolver.resolveLocale(request);
		args[0] = e.getMessage();
		errorMessage = messages.getMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, locale);

		session.setAttribute(WebscanBaseConstants.REQUEST_RESULT_ATT_NAME, WebscanBaseConstants.REQUEST_RESULT_ERROR_VALUE);
		session.setAttribute(WebscanBaseConstants.REQUEST_RESULT_MSG_ATT_NAME, errorMessage);
		redirectStrategy.sendRedirect(request, response, "/login.html");
	}

	/**
	 * Añade nuevos mensajes de aviso, a la zona de mensajes común del sistema,
	 * para informar sobre la proximidad de la fecha de caducidad de la password
	 * del usuario que inicia sesión.
	 * 
	 * @param userDto
	 *            usuario que inicia sesión.
	 * @param request
	 *            petición web.
	 */
	protected void addUpdatePasswordWarnMessages(UserDTO userDto, HttpServletRequest request) {
		Integer remainingDays;
		String[ ] args;

		remainingDays = loginService.getRemainingDaysWarnUpdatePassword(userDto.getNextPassUpdateDate());

		if (remainingDays > 0) {
			args = new String[1];
			args[0] = remainingDays.toString();
			BaseControllerUtils.addCommonMessage(UsersWebscanConstants.WARN_EXPIRED_PASS_MESSAGE, args, ViewMessageLevel.WARN, WebScope.SESSION, request);
		}
	}

	/**
	 * Establece un nuevo servicio que agrupa operaciones asociadas al inicio y
	 * finalización de sesión en el sistema por parte de usuarios.
	 * 
	 * @param aLoginService
	 *            Nuevo servicio que agrupa operaciones asociadas al inicio y
	 *            finalización de sesión en el sistema por parte de usuarios.
	 */
	public void setLoginService(LoginService aLoginService) {
		this.loginService = aLoginService;
	}

	/**
	 * Establece el servicio que agrupa operaciones de consulta, creación y
	 * modificación sobre usuarios del sistema.
	 * 
	 * @param anUserService
	 *            nuevo servicio que agrupa operaciones de consulta, creación y
	 *            modificación sobre usuarios del sistema.
	 */
	public void setUserService(UserService anUserService) {
		this.userService = anUserService;
	}

	/**
	 * Establece una nueva la estrategia de redirección.
	 * 
	 * @param aRedirectStrategy
	 *            Nueva estrategia de redirección.
	 */
	public void setRedirectStrategy(RedirectStrategy aRedirectStrategy) {
		this.redirectStrategy = aRedirectStrategy;
	}

	/**
	 * Establece un nuevo objeto de mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Nuevo objeto de mensajes, sujetos a multidioma, que son
	 *            presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}

	/**
	 * Establece una nueva utilidad para la resolución de la configuración
	 * regional y de idioma del sistema.
	 * 
	 * @param aLocaleResolver
	 *            Nueva utilidad para la resolución de la configuración regional
	 *            y de idioma del sistema.
	 */
	public void setLocaleResolver(LocaleResolver aLocaleResolver) {
		this.localeResolver = aLocaleResolver;
	}

}
