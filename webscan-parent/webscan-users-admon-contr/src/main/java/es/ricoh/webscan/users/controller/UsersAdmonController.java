/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.UsersAdmonController.java.</p>
 * <b>Descripción:</b><p> Controlador que agrupa las funcionalidades ejecutadas sobre los usuarios
 * registrados en el sistema desde la sección de administración de
 * usuarios.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.ricoh.webscan.admon.orgunit.service.OrgUnitService;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.IdentifierListVO;
import es.ricoh.webscan.base.controller.vo.IdentifierVO;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.service.AuditService;
import es.ricoh.webscan.users.UsersWebscanConstants;
import es.ricoh.webscan.users.controller.vo.ProfileVO;
import es.ricoh.webscan.users.controller.vo.UserVO;
import es.ricoh.webscan.users.model.UsersDAOException;
import es.ricoh.webscan.users.service.UserService;
import es.ricoh.webscan.users.validation.UserInSessionValidator;
import es.ricoh.webscan.users.validation.UserVoValidator;

/**
 * Controlador que agrupa las funcionalidades ejecutadas sobre los usuarios
 * vinculados a unidades organizativas desde la sección de administración de
 * unidades organizativas.
 * <p>
 * Class UsersAdmonController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

@Controller
public class UsersAdmonController {

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	@Autowired
	private MessageSource messages;

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UsersAdmonController.class);

	/**
	 * Servicio encargado del registro y actualización de trazas de auditoría.
	 */
	@Autowired
	private AuditService auditManager;

	/**
	 * Fachada de acceso a los servicios de las unidades organizativas.
	 */
	@Autowired
	private OrgUnitService organizationService;

	/**
	 * Fachada de acceso a los servicios de usuarios.
	 */
	@Autowired
	private UserService userService;

	/**
	 * Servicio que agrupa las distintas funcionalidades de validación de los
	 * datos de usuario.
	 */
	@Autowired
	private UserVoValidator userVoValidator;
	/**
	 * Servicio que agrupa las distintas funcionalidades de acceso a la
	 * validación de usuario en sesión.
	 */

	@Autowired
	private UserInSessionValidator userInSessionValidator;

	/**
	 * Método que asocia el formulario de edición y registro de usuario con su
	 * validador.
	 * 
	 * @param binder
	 *            Objeto responsable de enlazar el formulario con su validador.
	 */
	@InitBinder(value = "editUser")
	protected void initAdmonEditUserBinder(WebDataBinder binder) {
		binder.addValidators(userVoValidator);
	}

	/**
	 * Método que asocia el formulario de edición de la información del usuario
	 * en sesión con su validador.
	 * 
	 * @param binder
	 *            Objeto responsable de enlazar el formulario con su validador.
	 */
	@InitBinder(value = "editUserInSession")
	protected void initEditUserInSessionBinder(WebDataBinder binder) {
		binder.addValidators(userInSessionValidator);
	}

	/**
	 * Controlador responsable de iniciar y registrar una nueva solicitud de
	 * recuperación de password de usuario.
	 * 
	 * @param model
	 *            Modelo (datos de la vista).
	 * @param request
	 *            Petición web.
	 * @return Nombre de la vista donde se muestra la respuesta de la operación
	 *         solicitada.
	 */
	@RequestMapping("/admin/users")
	public String usersAdmon(final Model model, final HttpServletRequest request) {
		List<UserVO> users;
		// List<ProfileDTO> userProfiles;
		List<ProfileVO> userProfiles;
		Map<String, String> arbol = new HashMap<String, String>();
		// Se carga el modelo con la información a presentar en pantalla
		LOG.debug("[WEBSCAN-USERS] Accediendo a la administración de usuarios...");
		try {
			LOG.debug("[WEBSCAN-USERS] Obteniendo usuarios registrados en el sistema ...");
			// Listado de usuarios del sistema
			users = userService.getAllUsers();
			request.setAttribute("users", users);

			// Arbol de unidades organizativas
			LOG.debug("[WEBSCAN-USERS] Obteniendo unidades organizativas registradas en el sistema ...");
			organizationService.getTreeResponse(arbol);
			request.setAttribute("datos", arbol);
			request.setAttribute("nodoSelect", null);

			// Relación de perfiles de usuuario
			LOG.debug("[WEBSCAN-USERS] Obteniendo perfiles de usuario registrados en el sistema ...");
			// userProfiles = userService.getUserProfiles();
			userProfiles = userService.getUserProfilesVO();
			request.getSession(false).setAttribute("userProfiles", userProfiles);

			// Se inicializa el usuario en edición a un usuario vacio
			UserVO newUser = new UserVO();
			model.addAttribute(UsersWebscanConstants.EDIT_USER, newUser);

			LOG.debug("[WEBSCAN-USERS] Acceso a la administración de usuarios finalizado.");
		} catch (UsersDAOException e) {
			LOG.error("[WEBSCAN-USERS] Se produjo un error al acceder a la administración de usuarios, se obtiene el mensaje a presentar al usuario");

			UsersControllerUtils.setUsersDAOExceptionInRequest(e, request, messages, Boolean.TRUE);
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-USERS] Se produjo un error al acceder a la administración de usuarios, se obtiene el mensaje a presentar al usuario");

			UsersControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.TRUE);
		}

		return "usersAdmon";
	}

	/**
	 * Controlador responsable de presentar la vista de creación de un nuevo
	 * usuario.
	 * 
	 * @param model
	 *            Modelo (datos de la vista).
	 * @param request
	 *            Petición http.
	 * @return Nombre de la vista donde se muestra la operación solicitada.
	 * @throws DAOException
	 */
	@RequestMapping(value = "/admin/initAddNewUser", method = RequestMethod.GET)
	public String initAddNewUser(final Model model, final HttpServletRequest request) {
		Map<String, String> arbol = new HashMap<String, String>();
		UserVO newUser = new UserVO();
		model.addAttribute(UsersWebscanConstants.EDIT_USER, newUser);
		try {
			// Arbol de unidades organizativas
			LOG.debug("[WEBSCAN-USERS] Obteniendo unidades organizativas registradas en el sistema ...");
			organizationService.getTreeResponse(arbol);
			model.addAttribute("datos", arbol);
			model.addAttribute("nodoSelect", null);

		} catch (DAOException e) {
			LOG.error("[WEBSCAN-USERS] Se produjo un error al acceder a la administración de usuarios, se obtiene el mensaje a presentar al usuario");

			UsersControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.TRUE);
		}

		return "fragments/users/content_userTabs :: content";
	}

	/**
	 * Controlador responsable de presentar la vista para la edición de un
	 * usuario registrado en el sistema.
	 * 
	 * @param id
	 *            Identificador del usuario a editar
	 * @param idResult
	 *            Errores detectados en los datos de la petición
	 * @param model
	 *            Modelo (datos de la vista).
	 * @param request
	 *            Petición web.
	 * @return Nombre de la vista donde se muestra la operación solicitada.
	 * @throws DAOException
	 */
	@RequestMapping(value = "/admin/initEditUser", method = RequestMethod.POST)
	public String initEditUser(@Valid @RequestBody IdentifierVO id, BindingResult idResult, final Model model, final HttpServletRequest request) {
		UserVO user;
		Map<String, String> arbol = new HashMap<String, String>();
		try {
			if (idResult.hasErrors()) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se produjeron " + idResult.getErrorCount() + " errores al solicitar la edición de un usuario.");
				}
				// Se muestra mensaje en pantalla con el resultado de la
				// operación
				UsersControllerUtils.addCommonMessage("error.editUser.notSelected.message", null, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			} else {
				user = userService.getUser(id.getId());
				model.addAttribute(UsersWebscanConstants.EDIT_USER, user);
				// Arbol de unidades organizativas
				LOG.debug("[WEBSCAN-USERS] Obteniendo unidades organizativas registradas en el sistema ...");
				organizationService.getTreeResponse(arbol);
				model.addAttribute("datos", arbol);
				model.addAttribute("nodoSelect", null);
			}
		} catch (UsersDAOException e) {
			LOG.error("[WEBSCAN-USERS] Se produjo un error al recuperar la información de un usuario para su edición desde la administración de usuarios, se obtiene el mensaje a presentar al usuario");

			UsersControllerUtils.setUsersDAOExceptionInRequest(e, request, messages, Boolean.TRUE);
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-USERS] Se produjo un error al acceder a la administración de usuarios, se obtiene el mensaje a presentar al usuario");

			UsersControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.TRUE);
		}

		return "fragments/users/content_userTabs :: content";
	}

	/**
	 * Controlador responsable de presentar la vista para la edición de la
	 * información de identificación, acceso y contacto del usuario logado en el
	 * sistema.
	 * 
	 * @param id
	 *            Identificador del usuario a editar
	 * @param idResult
	 *            Errores detectados en los datos de la petición
	 * @param model
	 *            Modelo (datos de la vista).
	 * @param request
	 *            Petición web.
	 * @return Nombre de la vista donde se muestra la operación solicitada.
	 */
	@RequestMapping(value = "/initEditUserInSession", method = RequestMethod.POST)
	public String initEditUserInSession(@Valid @RequestBody IdentifierVO id, BindingResult idResult, final Model model, final HttpServletRequest request) {
		UserVO user;

		try {
			if (idResult.hasErrors()) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se produjeron " + idResult.getErrorCount() + " errores al solicitar la edición del usuario en sesión.");
				}
				// Se muestra mensaje en pantalla con el resultado de la
				// operación
				UsersControllerUtils.addCommonMessage("error.session.user.notSelected.message", null, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			} else {
				user = userService.getUser(id.getId());
				model.addAttribute(UsersWebscanConstants.EDIT_USER_IN_SESSION, user);
			}
		} catch (UsersDAOException e) {
			LOG.error("[WEBSCAN-USERS] Se produjo un error al recuperar la información de un usuario para su edición desde la administración de usuarios, se obtiene el mensaje a presentar al usuario");

			UsersControllerUtils.setUsersDAOExceptionInRequest(e, request, messages, Boolean.TRUE);
		}

		return "fragments/users/editUserInSession :: contentModalUser";
		// return "fragments/users/editUserInSession";
	}

	/**
	 * Controlador responsable de eliminar un conjunto de usuarios del sistema.
	 * 
	 * @param removedUsers
	 *            Lista de identificadores de usuarios a ser eliminados del
	 *            sistema.
	 * @param removeUsersResult
	 *            Errores detectados en los datos de la petición (datos del
	 *            formulario).
	 * @param model
	 *            Modelo (datos de la vista).
	 * @param request
	 *            Petición web.
	 * @return Nombre de la vista donde se muestra la respuesta de la operación
	 *         solicitada.
	 */
	@RequestMapping(value = "/admin/removeUsers", method = RequestMethod.POST)
	public String removeUsers(@Valid @RequestBody IdentifierListVO removedUsers, BindingResult removeUsersResult, final Model model, final HttpServletRequest request) {
		List<Long> userIds;
		List<UserVO> users;
		UserInSession user = BaseControllerUtils.getUserInSession(request);

		LOG.debug("[WEBSCAN-USERS] Iniciando el borrado de usuarios registrados en el sistema ...");

		try {
			if (removeUsersResult.hasErrors()) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se produjeron " + removeUsersResult.getErrorCount() + " errores al solicitar el borrado de usuarios.");
				}
				UsersControllerUtils.addCommonMessage("error.removeUsers.emptyList.message", null, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			} else {
				LOG.debug("[WEBSCAN-USERS] Eliminando los usuarios registrados en el sistema ...");

				userIds = getFinalRemovedUserList(removedUsers.getTokens(), user.getId(), request);

				userService.removeUsers(userIds, user, request);

				LOG.debug("[WEBSCAN-USERS] Actualizando la lista de usuarios en la capa de presentación ...");
				// Listado de usuarios del sistema
				users = userService.getAllUsers();
				request.setAttribute("users", users);

				// Se muestra mensaje en pantalla con el resultado de la
				// operación
				UsersControllerUtils.addCommonMessage("removeUsers.ok.message", null, ViewMessageLevel.INFO, WebScope.REQUEST, request);
			}
		} catch (UsersDAOException e) {
			LOG.error("[WEBSCAN-USERS] Se produjo un error al acceder a la administración de usuarios, se obtiene el mensaje a presentar al usuario");

			UsersControllerUtils.setUsersDAOExceptionInRequest(e, request, messages, Boolean.TRUE);
		} catch (DAOException e) {
			LOG.warn("[WEBSCAN-USERS] Se produjo un error al registrar las trazas de auditoría asociado a la operación de borrado de usuarios. Se obtiene el mensaje a presentar al usuario.");

			UsersControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.FALSE);
		}

		LOG.debug("[WEBSCAN-USERS] Borrado de usuarios finalizado.");

		// return "fragments/users/content_userTabla :: content";
		return usersAdmon(model, request);
	}

	/**
	 * Obtiene la lista definitiva de usuarios a eliminar del sistema,
	 * excluyendo el usuario que solicita la operación.
	 * 
	 * @param userIds
	 *            lista de identificadores de usuario para los que se solicita
	 *            el borrado.
	 * @param userInSessionId
	 *            Identificador del usuario que solicita la operación.
	 * @param request
	 *            Petición web.
	 * @return la lista definitiva de usuarios a eliminar del sistema,
	 *         excluyendo el usuario que solicita la operación.
	 */
	private List<Long> getFinalRemovedUserList(final List<Long> userIds, final Long userInSessionId, final HttpServletRequest request) {
		Boolean found;
		Integer userIndex;
		List<Long> res;

		res = new ArrayList<Long>(userIds);
		found = Boolean.FALSE;
		userIndex = 0;
		// Se comprueba que el usuario que solicita el borrado no este eincluido
		// en la lista
		for (int i = 0; !found && i < userIds.size(); i++) {
			userIndex = i;
			found = userIds.get(userIndex).equals(userInSessionId);
		}

		if (found) {
			userIds.remove(userIndex);
			UsersControllerUtils.addCommonMessage("error.removeUsers.noSelfRemove.message", null, ViewMessageLevel.WARN, WebScope.REQUEST, request);
		}

		return res;
	}

	/**
	 * Controlador responsable de la edición de usuarios.
	 * 
	 * @param user
	 *            Usuaruio a modificar.
	 * @param editUserResult
	 *            Errores detectados en los datos de la petición (datos del
	 *            formulario).
	 * @param model
	 *            Modelo (datos de la vista).
	 * @param request
	 *            Petición web.
	 * @return Nombre de la vista donde se muestra la respuesta de la operación
	 *         solicitada.
	 */
	@RequestMapping(value = "/admin/saveUser", method = RequestMethod.POST)
	public String saveUser(@Valid @ModelAttribute("editUser") UserVO user, BindingResult editUserResult, final Model model, final HttpServletRequest request) {
		String propNameMessage = null;
		String[ ] args = new String[1];
		Map<String, String> arbol = new HashMap<String, String>();
		String res = "";
		UserVO updatedUser = null;
		UserInSession userInSession = BaseControllerUtils.getUserInSession(request);
		try {
			if (editUserResult.hasErrors()) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se produjeron " + editUserResult.getErrorCount() + " errores al editar la información del usuario " + user.getUsername() + ".");
				}
				FieldError error = editUserResult.getFieldError("orgUnits");

				if (error != null) {
					UsersControllerUtils.addCommonMessage("error.editUser.orgUnits.message", null, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
				} else {
					res = "fragments/users/content_userTabs :: content";
					// Se informa a la capa de presentación que se han
					// encontrado errores en la pestaña de datos generales
					model.addAttribute("errorsInSaveUserForm", "true");
					// Arbol de unidades organizativas
					LOG.debug("[WEBSCAN-USERS] Obteniendo unidades organizativas registradas en el sistema ...");
					organizationService.getTreeResponse(arbol);
					request.setAttribute("datos", arbol);
					request.setAttribute("nodoSelect", null);
					// Se inicializa el usuario en edición a un usuario vacio
					model.addAttribute(UsersWebscanConstants.EDIT_USER, user);
					UsersControllerUtils.addCommonMessage("error.editUser.message", null, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
				}
			} else {

				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se actualiza la información del usuario " + user.getUsername() + " ...");
				}

				updatedUser = userService.performSaveUser(user, userInSession, request);

				if (user.getId() == null) {
					propNameMessage = "newUser.ok.message";
				} else {
					propNameMessage = "updateUser.ok.message";
				}

				request.setAttribute("users", userService.getAllUsers());
				organizationService.getTreeResponse(arbol);
				request.setAttribute("datos", arbol);
				request.setAttribute("nodoSelect", null);
				model.addAttribute(UsersWebscanConstants.EDIT_USER, updatedUser);

				// Se muestra mensaje en pantalla con el resultado de la
				// operación
				args[0] = user.getUsername();
				UsersControllerUtils.addCommonMessage(propNameMessage, args, ViewMessageLevel.INFO, WebScope.REQUEST, request);
				res = "usersAdmon";
			}
		} catch (UsersDAOException e) {
			LOG.error("[WEBSCAN-USERS] Se produjo un error al salvar los cambios realizados sobre un usuario en la administración de usuarios, se obtiene el mensaje a presentar al usuario");
			UsersControllerUtils.setUsersDAOExceptionInRequest(e, request, messages, Boolean.TRUE);
			res = usersAdmon(model, request);
		} catch (DAOException e) {
			LOG.warn("[WEBSCAN-USERS] Se produjo un error al registrar las trazas de auditoría asociado a la operación de creación o actualización de usuario. Se obtiene el mensaje a presentar al usuario.");
			UsersControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.FALSE);
			res = usersAdmon(model, request);
		}
		return res;
	}

	/**
	 * Controlador responsable de la edición de usuarios de session.
	 * 
	 * @param user
	 *            Usuaruio a modificar.
	 * @param editUserResult
	 *            Errores detectados en los datos de la petición (datos del
	 *            formulario).
	 * @param model
	 *            Modelo (datos de la vista).
	 * @param request
	 *            Petición web.
	 * @return Nombre de la vista donde se muestra la respuesta de la operación
	 *         solicitada.
	 */
	@RequestMapping(value = "/saveUserInSession", method = RequestMethod.POST)
	public String saveUserInSession(@Valid @ModelAttribute("editUserInSession") UserVO user, BindingResult editUserResult, final Model model, final HttpServletRequest request) {
		String res = "fragments/users/editUserInSession :: contentModalUser";

		if (editUserResult.hasErrors()) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-USERS] Se produjeron " + editUserResult.getErrorCount() + " errores al editar la información del usuario en sesión, " + user.getUsername() + ".");
			}
			model.addAttribute("hasErrorsUserInSession", "true");
		} else {
			try {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-USERS] Se actualiza la información del usuario {}, logado en sesión, en BBDD ...", user.getUsername());
				}

				userService.performUpdateUserSession(user, request);

				// Se muestra mensaje en pantalla con el resultado de la
				// operación
				UsersControllerUtils.addCommonMessage("updateUserInSession.ok.message", null, ViewMessageLevel.INFO, WebScope.REQUEST, request);
				res = "main";
			} catch (UsersDAOException e) {
				LOG.error("[WEBSCAN-USERS] Se produjo un error al salvar los cambios realizados sobre el usuario en sesión, se obtiene el mensaje a presentar al usuario");
				UsersControllerUtils.setUsersDAOExceptionInRequest(e, request, messages, Boolean.TRUE);
				res = "main";
			} catch (DAOException e) {
				LOG.warn("[WEBSCAN-USERS] Se produjo un error al registrar las trazas de auditoría asociado a la operación de actualización del usuario en sesión. Se obtiene el mensaje a presentar al usuario.");
				UsersControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.FALSE);
				res = "main";
			}
		}

		return res;
	}

}
