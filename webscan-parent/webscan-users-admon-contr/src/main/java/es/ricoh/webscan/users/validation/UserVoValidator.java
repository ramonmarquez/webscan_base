/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.users.controller.validation.UserVoValidator.java.</p>
 * <b>Descripción:</b><p> Clase responsable de validar el formulario de edición de usuarios de la
 * consola de administración.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.users.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import es.ricoh.webscan.users.controller.vo.UserVO;

/**
 * Clase responsable de validar el formulario de edición de usuarios de la
 * consola de administración.
 * <p>
 * Clase UserVoValidator.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class UserVoValidator implements Validator {

	/**
	 * Constante que representa la denominación del perfil de usuario "personal
	 * de digitalización".
	 */
	static final String DIGITALIZATION_STAFF = "Digitalization Staff";

	/**
	 * Validador que comprueba que la password y su confirmación son iguales.
	 */
	private PasswordMatchesValidator passwordMatchesValidator;

	/**
	 * Validador que comprueba que la password es robusta y cumple con las
	 * medidas de calidad configuradas en el sistema.
	 */
	private PasswordPatternValidator passwordPatternValidator;

	/**
	 * Validador que comprueba que la nueva password de un usuario, en los
	 * formulario de edición de usuarios incluidos en el sistema, no coincide
	 * con la anterior password del usuario.
	 */
	private OldPasswordNotMatchValidator oldPasswordNotMatchValidator;

	/**
	 * Validador que comprueba que una de caracteres presente el formato de un
	 * dirección de correo electrónico.
	 */
	private EmailPatternValidator emailPatternValidator;

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return UserVO.class.isAssignableFrom(clazz);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object,
	 *      org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		UserVO userVo = (UserVO) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "document", "error.document.required", "Document is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "error.username.required", "Username is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.name.required", "Name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "error.surname.required", "Surname is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "error.email.required", "Email is required.");
		ValidationUtils.invokeValidator(emailPatternValidator, userVo.getEmail(), errors);
		ValidationUtils.rejectIfEmpty(errors, "profile", "error.userProfile.required", "User profile is required.");
		if (DIGITALIZATION_STAFF.equals(userVo.getProfile().getName()) && (userVo.getOrgUnits() == null || userVo.getOrgUnits().isEmpty())) {
			// Se retorna error si el usuario es digitalizador y no se han
			// asignado unidades organizativas
			errors.rejectValue("orgUnits", "error.orgUnits.empty", "OrgUnit not must empty.");
		}

		// La password solo debe ser comprobada si ha sido
		// actualizada, o el usuario es nuevo
		if (userVo.getId() == null) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordForm.password", "error.password.required", "Password is required.");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordForm.confirmPassword", "error.confirmPassword.required", "Confirm password is required.");
		}

		if (userVo.getPasswordForm().getPassword() != null && !userVo.getPasswordForm().getPassword().isEmpty()) {
			ValidationUtils.invokeValidator(passwordMatchesValidator, userVo.getPasswordForm(), errors);
			ValidationUtils.invokeValidator(passwordPatternValidator, userVo.getPasswordForm(), errors);
			if (userVo.getId() != null) {
				ValidationUtils.invokeValidator(oldPasswordNotMatchValidator, userVo, errors);
			}
		}
	}

	/**
	 * Establece el validador que comprueba que la password y su confirmación
	 * son iguales.
	 * 
	 * @param aPasswordMatchesValidator
	 *            validador que comprueba que la password y su confirmación son
	 *            iguales.
	 */
	public void setPasswordMatchesValidator(PasswordMatchesValidator aPasswordMatchesValidator) {
		this.passwordMatchesValidator = aPasswordMatchesValidator;
	}

	/**
	 * Establece el validador que comprueba que la password es robusta y cumple
	 * con las medidas de calidad configuradas en el sistema.
	 * 
	 * @param aPasswordPatternValidator
	 *            validador que comprueba que la password es robusta y cumple
	 *            con las medidas de calidad configuradas en el sistema.
	 */
	public void setPasswordPatternValidator(PasswordPatternValidator aPasswordPatternValidator) {
		this.passwordPatternValidator = aPasswordPatternValidator;
	}

	/**
	 * Establece el validador que comprueba que la nueva password de un usuario,
	 * en los formulario de edición de usuarios incluidos en el sistema, no
	 * coincide con la anterior password del usuario.
	 * 
	 * @param anOldPasswordNotMatchValidator
	 *            nuevo validador que comprueba que la nueva password de un
	 *            usuario, en los formulario de edición de usuarios incluidos en
	 *            el sistema, no coincide con la anterior password del usuario.
	 */
	public void setOldPasswordNotMatchValidator(OldPasswordNotMatchValidator anOldPasswordNotMatchValidator) {
		this.oldPasswordNotMatchValidator = anOldPasswordNotMatchValidator;
	}

	/**
	 * Establece el validador que comprueba que una de caracteres presente el
	 * formato de un dirección de correo electrónico.
	 * 
	 * @param anEmailPatternValidator
	 *            nuevo validador que comprueba que una de caracteres presente
	 *            el formato de un dirección de correo electrónico.
	 */
	public void setEmailPatternValidator(EmailPatternValidator anEmailPatternValidator) {
		this.emailPatternValidator = anEmailPatternValidator;
	}

}
