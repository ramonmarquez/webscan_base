/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.service.impl.soap.interceptors.HandleExceptionOutInterceptor.java.</p>
* <b>Descripción:</b><p> Interceptor de salida responsable de establecer el código de la exceptción
* SOAP y el código de estado HTTP.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.service.impl.soap.interceptors;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.opensaml.soap.soap11.FaultCode;

import es.ricoh.webscan.spacers.business.SpacersException;
import es.ricoh.webscan.spacers.repository.SpacersDAOException;
import es.ricoh.webscan.spacers.service.api.soap.WebscanException;
import es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.Atributos;
import es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.Estado;

/**
 * Interceptor de salida responsable de establecer el código de la exceptción
 * SOAP y el código de estado HTTP.
 * <p>
 * Clase HandleExceptionOutInterceptor.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class HandleExceptionOutInterceptor extends AbstractSoapInterceptor {

	/**
	 * Código de estado HTTP por petición mal formada.
	 */
	private static final int BAD_REQUEST_HTTP_STATUS_CODE = 400;

	/**
	 * Formato de la fecha incluida en la cabecera SOAP de trazabilidad.
	 */
	private static final SimpleDateFormat ISO_8601_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	/**
	 * Clase encargada de la construcción de la mensajería incluida en el
	 * detalle de soap faults.
	 */
	private static final es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.ObjectFactory FAULT_ATT_OBJECT_FACTORY = new es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.ObjectFactory();

	public HandleExceptionOutInterceptor() {
		super(Phase.PRE_STREAM);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.apache.cxf.interceptor.Interceptor#handleMessage(org.apache.cxf.message.Message)
	 */
	@Override
	public void handleMessage(SoapMessage message) throws Fault {
		Exception exception;
		Fault fault = null;
		Throwable cause;
		WebscanException webscanException;

		exception = (Exception) message.getContent(Exception.class);

		if (exception.getCause() == null) {
			// Si es null, la excepción es java.lang.NullPointerException
			// por petición mal formada
			fault = buildDefaultSoapFault(exception);
		} else {
			if (exception.getCause() instanceof WebscanException) {
				webscanException = (WebscanException) exception.getCause();
				cause = webscanException.getCause();
				fault = new Fault(webscanException);

				if (cause instanceof SpacersDAOException) {
					if (SpacersDAOException.CODE_999.equals(((SpacersDAOException) cause).getCode())) {
						fault.setFaultCode(FaultCode.SERVER);
					} else {
						fault.setFaultCode(FaultCode.CLIENT);
						fault.setStatusCode(BAD_REQUEST_HTTP_STATUS_CODE);
					}
				} else if (cause instanceof SpacersException) {
					if (SpacersException.CODE_999.equals(((SpacersException) cause).getCode())) {
						fault.setFaultCode(FaultCode.SERVER);
					} else if (SpacersException.CODE_998.equals(((SpacersException) cause).getCode())) {
						fault.setFaultCode(FaultCode.CLIENT);
						fault.setStatusCode(BAD_REQUEST_HTTP_STATUS_CODE);
					} else {
						// El resto de códigos de error de SpacersException
						// son
						// errores de negocio
						fault.setFaultCode(FaultCode.SERVER);
					}
				} else {
					fault.setFaultCode(FaultCode.SERVER);
				}
			} else {
				fault = buildDefaultSoapFault(exception);
			}
		}

		message.setContent(Exception.class, fault);
	}

	/**
	 * Construye una SOAP Fault por defecto ante una petición mal formada.
	 * 
	 * @param exception
	 *            excepción.
	 * @return SOAP Fault.
	 */
	private Fault buildDefaultSoapFault(Exception exception) {
		Atributos faultAtts;
		Estado statusFaulAtts;
		Fault res = null;
		String msgExc;
		WebscanException webscanException;

		faultAtts = FAULT_ATT_OBJECT_FACTORY.createAtributos();
		faultAtts.setCodigoCertificado("");
		statusFaulAtts = FAULT_ATT_OBJECT_FACTORY.createEstado();
		statusFaulAtts.setCodigoEstado("0" + SpacersException.CODE_998.substring("COD_".length(), SpacersException.CODE_998.length()));
		statusFaulAtts.setCodigoEstadoSecundario("");
		msgExc = exception.getMessage();
		if (msgExc != null && msgExc.length() > 255) {
			statusFaulAtts.setLiteralError(msgExc.substring(0, 255));
		} else {
			statusFaulAtts.setLiteralError(msgExc);
		}
		statusFaulAtts.setTiempoEstimadoRespuesta(0);
		faultAtts.setEstado(statusFaulAtts);
		faultAtts.setIdPeticion("");
		faultAtts.setNumElementos(1);
		faultAtts.setTimeStamp(ISO_8601_DATE_FORMAT.format(new Date()));
		webscanException = new WebscanException("[" + SpacersException.CODE_998 + "] " + msgExc, faultAtts, exception);

		// Se construye la SOAP fault
		res = new Fault(webscanException);
		res.setFaultCode(FaultCode.CLIENT);
		res.setStatusCode(BAD_REQUEST_HTTP_STATUS_CODE);

		return res;
	}

}
