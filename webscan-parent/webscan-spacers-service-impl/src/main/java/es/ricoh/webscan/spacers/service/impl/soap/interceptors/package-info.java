/**
 * Interceptores de entrada y salida de la interfaz SOAP del servicio de
 * generación de carátulas y separadores reutilizables.
 */
package es.ricoh.webscan.spacers.service.impl.soap.interceptors;