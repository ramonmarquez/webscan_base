/**
 * Proveedores y manejadores del servicio REST de generación de carátulas y
 * separadores de reutilizables de documentos.
 */
package es.ricoh.webscan.spacers.service.impl.rest.providers;