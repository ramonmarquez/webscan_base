/**
 * Paquete que agrupa la implementación REST y SOAP del servicio de generación
 * de carátulas y separadores de reutilizables de documentos.
 */
package es.ricoh.webscan.spacers.service.impl;