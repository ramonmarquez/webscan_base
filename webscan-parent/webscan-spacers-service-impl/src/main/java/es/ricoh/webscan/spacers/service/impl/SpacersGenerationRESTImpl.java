/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.service.impl.SpacersGenerationRESTImpl.java.</p>
* <b>Descripción:</b><p> Implementación del interfaz REST del servicio de generación de carátulas y separadores
* reutilizables de documentos publicado por el activo Webscan-GV.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.spacers.business.Spacer;
import es.ricoh.webscan.spacers.business.SpacerDocRequest;
import es.ricoh.webscan.spacers.business.SpacerRequest;
import es.ricoh.webscan.spacers.business.SpacersException;
import es.ricoh.webscan.spacers.business.service.SpacersService;
import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.repository.SpacersDAOException;
import es.ricoh.webscan.spacers.service.api.rest.DocsSpacersGeneration;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateDocsBatchSpacerRequest;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateDocsBatchSpacerResponse;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateDocsBatchSpacerServiceRequest;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateDocsBatchSpacerServiceResponse;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateReusableDocSpacersRequest;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateReusableDocSpacersResponse;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateReusableDocSpacersServiceRequest;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateReusableDocSpacersServiceResponse;
import es.ricoh.webscan.spacers.service.api.rest.messages.MetadataCollection;
import es.ricoh.webscan.spacers.service.api.rest.messages.ResParam;
import es.ricoh.webscan.spacers.service.api.rest.messages.Result;
import es.ricoh.webscan.spacers.service.api.rest.messages.SpacerDoc;

/**
 * Implementación del interfaz REST del servicio de generación de carátulas y
 * separadores reutilizables de documentos publicado por el activo Webscan-GV.
 * <p>
 * Clase SpacersGenerationRESTImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.1.1.
 */
public class SpacersGenerationRESTImpl implements DocsSpacersGeneration {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SpacersGenerationRESTImpl.class.getName());

	/**
	 * Código de resultado correcto.
	 */
	private static final String OK_RESULT_CODE = "COD_000";

	/**
	 * Fachada de la lógica de negocio de la utilidad de generación de carátulas
	 * y separadores reutilizables.
	 */
	private SpacersService spacersService;

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.gv.dgti.webscan.services.docsspacersgeneration.rest.DocsSpacersGeneration#generateDocsBatchSpacer(es.gv.dgti.webscan.services.docsspacersgeneration.rest.messages.GenerateDocsBatchSpacerServiceRequest)
	 */
	@Override
	public Response generateDocsBatchSpacer(GenerateDocsBatchSpacerServiceRequest generateDocsBatchFrontPageServiceRequest) {
		GenerateDocsBatchSpacerRequest request;
		GenerateDocsBatchSpacerResponse respContent;
		GenerateDocsBatchSpacerServiceResponse resp;
		Response res = null;
		ResParam resParam;
		Result result;
		Spacer spacer;
		es.ricoh.webscan.spacers.service.api.rest.messages.Spacer resSpacer;
		SpacerRequest serviceRequest;

		LOG.info("[WEBSCAN-SPACERS-REST] Recibida petición de generación de carátula de lote de documentos ...");

		try {

			if (generateDocsBatchFrontPageServiceRequest == null || generateDocsBatchFrontPageServiceRequest.getGenerateDocsBatchSpacerRequest() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Petición de generación de carátula de lote de documentos mal formada.");
			}

			request = generateDocsBatchFrontPageServiceRequest.getGenerateDocsBatchSpacerRequest();

			if (request.getSpacerGenerationParams() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Tipo de código no informado en la petición.");
			}

			if (request.getDepartment() == null || request.getDepartment().isEmpty()) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. No se ha especificado la unidad orgánica a la que serán asociados los documentos del lote.");
			} else {
				if (request.getDepartment().length() > 50) {
					throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. El identificador de unidad orgánica tiene un tamaño superior al permitido.");
				}
			}

			if (request.getSpacerGenerationParams() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Tipo de código no informado en la petición.");
			}

			if (request.getSpacerAdditionalInfoParams() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Perfil de digitalización y definición de documentos no especificados.");
			}

			if (request.getSpacerAdditionalInfoParams().getScanProfile() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Perfil de digitalización no especificado.");
			}

			if (request.getSpacerDocs() == null || request.getSpacerDocs().isEmpty()) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Documentos no especificados.");
			}

			// Validación de la petición recibida
			LOG.debug("[WEBSCAN-SPACERS-REST] Parseando la petición de generación de carátula de lote de documentos ...");
			serviceRequest = new SpacerRequest();
			serviceRequest.setDocuments(transformSpacerDocs((request.getSpacerDocs())));

			if (request.getBatchSpacerValParams() != null) {
				serviceRequest.setBatchDocSize(request.getBatchSpacerValParams().getNumberOfPagesInBatch());
				serviceRequest.setBatchPages(request.getBatchSpacerValParams().getNumberOfPagesInBatch());
				serviceRequest.setDocSpacerType(spacersService.getRequestDocSpacerType(request.getBatchSpacerValParams().getDocSeparatorType()));
			}

			serviceRequest.setDocSpecName(request.getSpacerAdditionalInfoParams().getDocSpecName());
			serviceRequest.setScanTemplateName(request.getSpacerAdditionalInfoParams().getScanProfile().getScanTemplateName());
			serviceRequest.setUserGroupExternalId(request.getSpacerAdditionalInfoParams().getScanProfile().getUserGroupExternalId());
			serviceRequest.setSpacerType(spacersService.getRequestBatchSpacerType(request.getSpacerGenerationParams().getInfoCodingMode()));
			serviceRequest.setReqUserId(request.getUserId());
			serviceRequest.setDepartment(request.getDepartment());
			serviceRequest.setNumberOfReusableDocSpacers(request.getNumberOfReusableDocSpacers());

			// Invocando el servicio de generación de carátulas y separadores
			// reutilizables
			LOG.debug("[WEBSCAN-SPACERS-REST] Se genera la carátula solicitada ...");
			spacer = spacersService.generateDocsBatchSpacer(serviceRequest, null, Boolean.TRUE, spacersService.getLocale(request.getSpacerGenerationParams().getLocale()));

			// Se contruye la respuesta
			resp = new GenerateDocsBatchSpacerServiceResponse();
			respContent = new GenerateDocsBatchSpacerResponse();
			result = new Result();
			result.setCode(OK_RESULT_CODE);
			result.setMessage("Carátula de lote de documentos generada correctamente");
			respContent.setResult(result);
			resParam = new ResParam();
			resSpacer = new es.ricoh.webscan.spacers.service.api.rest.messages.Spacer();
			resSpacer.setBinarySpacer(Base64.encodeBase64String(spacer.getContent()));
			resSpacer.setMimeType(spacer.getMimeType());
			resParam.setSpacer(resSpacer);
			respContent.setResParam(resParam);
			resp.setGenerateDocsBatchSpacerResponse(respContent);

			res = Response.ok(resp).build();
			LOG.info("[WEBSCAN-SPACERS-REST] Petición de generación de carátula de lote de documentos realizada correctamente...");
		} catch (SpacersDAOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-REST] Error al realizar una petición de generación de carátula de lote de documentos. \n" + e.getMessage(), e);
			}
			res = buildDAOExceptionErrorResponse(e);
		} catch (SpacersException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-REST] Error al realizar una petición de generación de carátula de lote de documentos. \n" + e.getMessage(), e);
			}
			res = buildSpacersExceptionErrorResponse(e);
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-REST] Error al realizar una petición de generación de carátula de lote de documentos. \n" + e.getMessage(), e);
			}
			res = buildExceptionErrorResponse(e);
		}

		return res;
	}

	/**
	 * Rellena la colección de documentos de la petición de la capa de
	 * servicios.
	 * 
	 * @param documents
	 *            Documentos incluidos en la petición de generación de
	 *            carátulas.
	 * @return colección de documentos de la petición de la capa de servicios.
	 * @throws SpacersDAOException
	 *             En caso de producirse un error al procesar los metadatos de
	 *             los documentos.
	 * @throws SpacersException
	 *             Si no es establecido el número de orden, denominación o
	 *             metadatos de uno o más documentos.
	 */
	private List<SpacerDocRequest> transformSpacerDocs(List<SpacerDoc> documents) throws SpacersDAOException, SpacersException {
		List<SpacerDocRequest> res = new ArrayList<SpacerDocRequest>();
		SpacerDocRequest doc;

		for (SpacerDoc spacerDoc: documents) {

			doc = new SpacerDocRequest();
			doc.setName(spacerDoc.getName());

			if (spacerDoc.getOrderNumber() != null) {
				doc.setOrder(spacerDoc.getOrderNumber());
				doc.setMetadataCollection(transformMetadataCollection(spacerDoc.getMetadataCollection(), spacerDoc.getOrderNumber()));
			}
			res.add(doc);
		}

		return res;
	}

	/**
	 * Transforma la lista de metadatos informados para un documento en la
	 * petición de generación de carátulas en un Map<String, String>.
	 * 
	 * @param metadataCollection
	 *            lista de metadatos informados en la petición para un documento
	 *            en la petición de generación de carátulas.
	 * @return Relación de pares clave / valor, identificados mediante el nombre
	 *         de los metadatos, y cuyo valor se corresponde con el valor
	 *         informado para el metadato en la petición para un documento en la
	 *         petición de generación de carátulas.
	 * @throws SpacersDAOException
	 *             Si no se especifica el identificador de algún metadato.
	 * @throws SpacersException
	 *             Si más de un metadato tiene el mismo identificador.
	 */
	private Map<String, String> transformMetadataCollection(List<MetadataCollection> metadataCollection, Integer docOrderNum) throws SpacersDAOException, SpacersException {
		Map<String, String> res = new HashMap<String, String>();
		if (metadataCollection != null && !metadataCollection.isEmpty()) {
			for (MetadataCollection metadata: metadataCollection) {
				if (metadata.getId() == null || metadata.getId().isEmpty()) {
					throw new SpacersDAOException(SpacersDAOException.CODE_504, "No se ha especificado la denominación de uno o más metadatos para el documento " + docOrderNum + ".");
				}

				if (res.containsKey(metadata.getId())) {
					throw new SpacersException(SpacersException.CODE_004, "El metadato " + metadata.getId() + " ha sido informado más de una vez en la petición para el documento " + docOrderNum + ".");
				}

				res.put(metadata.getId(), metadata.getValue());
			}
		}
		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.gv.dgti.webscan.services.docsspacersgeneration.rest.DocsSpacersGeneration#generateReusableDocFrontPages(es.gv.dgti.webscan.services.docsspacersgeneration.rest.messages.GenerateReusableDocFrontPagesServiceRequest)
	 */
	@Override
	public Response generateReusableDocSpacers(GenerateReusableDocSpacersServiceRequest generateReusableDocSpacersServiceRequest) {
		BatchSpacerType spacerType;
		GenerateReusableDocSpacersRequest request;
		GenerateReusableDocSpacersResponse respContent;
		GenerateReusableDocSpacersServiceResponse resp;
		Response res = null;
		ResParam resParam;
		Result result;
		Spacer spacer;
		es.ricoh.webscan.spacers.service.api.rest.messages.Spacer resSpacer;

		LOG.info("[WEBSCAN-SPACERS-REST] Recibida petición de generación de separadores reutilizables de documentos ...");

		try {

			if (generateReusableDocSpacersServiceRequest == null || generateReusableDocSpacersServiceRequest.getGenerateReusableDocSpacersRequest() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Petición de generación de separadores reutilizables de documentos mal formada.");
			}

			request = generateReusableDocSpacersServiceRequest.getGenerateReusableDocSpacersRequest();
			LOG.debug("[WEBSCAN-SPACERS-REST] Parseando la petición de generación de separadores reutilizables de documentos ...");
			if (request.getSpacerGenerationParams() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Petición de generación de separadores reutilzables de documentos mal formada. No informado el tipo de código.");
			}
			spacerType = spacersService.getRequestBatchSpacerType(request.getSpacerGenerationParams().getInfoCodingMode());

			// Invocando el servicio de generación de carátulas y separadores
			// reutilizables
			LOG.debug("[WEBSCAN-SPACERS-REST] Se generan los separadores reutilizables de documentos solicitados ...");
			spacer = spacersService.generateReusableDocSpacers(spacerType, request.getNumberOfReusableDocSpacers(), request.getUserId(), null, spacersService.getLocale(request.getSpacerGenerationParams().getLocale()));

			// Se contruye la respuesta
			resp = new GenerateReusableDocSpacersServiceResponse();
			respContent = new GenerateReusableDocSpacersResponse();
			result = new Result();
			result.setCode(OK_RESULT_CODE);
			result.setMessage("Separadores reutilizables de documentos generados correctamente");
			respContent.setResult(result);
			resParam = new ResParam();
			resSpacer = new es.ricoh.webscan.spacers.service.api.rest.messages.Spacer();
			resSpacer.setBinarySpacer(Base64.encodeBase64String(spacer.getContent()));
			resSpacer.setMimeType(spacer.getMimeType());
			resParam.setSpacer(resSpacer);
			respContent.setResParam(resParam);
			resp.setGenerateReusableDocSpacersResponse(respContent);

			res = Response.ok(resp).build();
			LOG.info("[WEBSCAN-SPACERS-REST] Petición de generación de separadores reutilizables de documentos realizada correctamente...");
		} catch (SpacersException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-REST] Error al realizar una petición de generación de separadores reutilizables de documentos. \n" + e.getMessage(), e);
			}
			res = buildSpacersExceptionErrorResponse(e);
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-REST] Error al realizar una petición de generación de separadores reutilizables de documentos. \n" + e.getMessage(), e);
			}
			res = buildExceptionErrorResponse(e);
		}

		return res;
	}

	/**
	 * Construye una respuesta HTTP a partir de una excepción
	 * SpacersDAOException.
	 * 
	 * @param e
	 *            excepción SpacersDAOException.
	 * @return respuesta HTTP.
	 */
	private Response buildDAOExceptionErrorResponse(SpacersDAOException e) {
		GenerateDocsBatchSpacerResponse respContent;
		GenerateDocsBatchSpacerServiceResponse resp;
		Response res = null;
		Result result;
		Status status = Status.INTERNAL_SERVER_ERROR;

		resp = new GenerateDocsBatchSpacerServiceResponse();
		respContent = new GenerateDocsBatchSpacerResponse();
		result = new Result();
		result.setCode(e.getCode());
		result.setMessage(e.getMessage());
		respContent.setResult(result);

		resp.setGenerateDocsBatchSpacerResponse(respContent);

		if (SpacersDAOException.CODE_511.equals(e.getCode()) || SpacersDAOException.CODE_512.equals(e.getCode())) {
			status = Status.OK;
		} else {
			if (!SpacersDAOException.CODE_999.equals(e.getCode())) {
				status = Status.BAD_REQUEST;
			}
		}

		res = Response.status(status).entity(resp).build();

		return res;
	}

	/**
	 * Construye una respuesta HTTP a partir de una excepción SpacersException.
	 * 
	 * @param e
	 *            excepción SpacersException.
	 * @return respuesta HTTP.
	 */
	private Response buildSpacersExceptionErrorResponse(SpacersException e) {
		GenerateDocsBatchSpacerResponse respContent;
		GenerateDocsBatchSpacerServiceResponse resp;
		Response res = null;
		Result result;
		Status status = Status.INTERNAL_SERVER_ERROR;

		resp = new GenerateDocsBatchSpacerServiceResponse();
		respContent = new GenerateDocsBatchSpacerResponse();
		result = new Result();
		result.setCode(e.getCode());
		result.setMessage(e.getMessage());
		respContent.setResult(result);

		resp.setGenerateDocsBatchSpacerResponse(respContent);

		if (SpacersException.CODE_998.equals(e.getCode())) {
			status = Status.BAD_REQUEST;
		} else {
			if (!SpacersException.CODE_999.equals(e.getCode())) {
				status = Status.OK;
			}
		}

		res = Response.status(status).entity(resp).build();

		return res;
	}

	/**
	 * Construye una respuesta HTTP a partir de una excepción.
	 * 
	 * @param e
	 *            excepción.
	 * @return respuesta HTTP.
	 */
	private Response buildExceptionErrorResponse(Exception e) {
		GenerateDocsBatchSpacerResponse respContent;
		GenerateDocsBatchSpacerServiceResponse resp;
		Response res = null;
		Result result;

		resp = new GenerateDocsBatchSpacerServiceResponse();
		respContent = new GenerateDocsBatchSpacerResponse();
		result = new Result();
		result.setCode(SpacersException.CODE_999);
		result.setMessage(e.getMessage());
		respContent.setResult(result);

		resp.setGenerateDocsBatchSpacerResponse(respContent);

		res = Response.status(Status.INTERNAL_SERVER_ERROR).entity(resp).build();

		return res;
	}

	/**
	 * Establece la fachada de la lógica de negocio de la utilidad de generación
	 * de carátulas y separadores reutilizables.
	 * 
	 * @param aSpacersService
	 *            Fachada de la lógica de negocio de la utilidad de generación
	 *            de carátulas y separadores reutilizables.
	 */
	public void setSpacersService(SpacersService aSpacersService) {
		this.spacersService = aSpacersService;
	}

}
