/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.service.impl.rest.providers.JacksonProcessingExceptionHandler.java.</p>
* <b>Descripción:</b><p> Manejador de excepciones producidas en el mapeo de texto en notación JSON a
* objetos Java en servicios REST publicados por Webscan-GV.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.service.impl.rest.providers;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.codehaus.jackson.JsonProcessingException;

import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateDocsBatchSpacerResponse;
import es.ricoh.webscan.spacers.service.api.rest.messages.GenerateDocsBatchSpacerServiceResponse;
import es.ricoh.webscan.spacers.service.api.rest.messages.Result;
import es.ricoh.webscan.WebscanException;

/**
 * Manejador de excepciones producidas en el mapeo de texto en notación JSON a
 * objetos Java en servicios REST publicados por Webscan-GV.
 * <p>
 * Clase JacksonProcessingExceptionHandler.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class JacksonProcessingExceptionHandler implements ExceptionMapper<JsonProcessingException> {

	/**
	 * {@inheritDoc}
	 * 
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
	 */
	@Override
	public Response toResponse(JsonProcessingException exception) {
		GenerateDocsBatchSpacerResponse respContent;
		GenerateDocsBatchSpacerServiceResponse resp;
		Response res = null;
		Result result;

		resp = new GenerateDocsBatchSpacerServiceResponse();
		respContent = new GenerateDocsBatchSpacerResponse();
		result = new Result();
		result.setCode(WebscanException.CODE_998);
		result.setMessage(exception.getMessage());
		respContent.setResult(result);

		resp.setGenerateDocsBatchSpacerResponse(respContent);

		res = Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).entity(resp).build();

		return res;
	}

}
