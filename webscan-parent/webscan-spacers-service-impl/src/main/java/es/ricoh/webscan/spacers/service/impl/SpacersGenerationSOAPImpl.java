/** 
* <b>Archivo:</b><p>es.ricoh.webscan.spacers.service.impl.SpacersGenerationSOAPImpl.java.</p>
* <b>Descripción:</b><p> Implementación del interfaz SOAP del servicio de generación de carátulas y separadores
* reutilizables de documentos publicado por el activo Webscan-GV.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.spacers.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

import org.apache.cxf.phase.PhaseInterceptorChain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.services.WebscanServiceConstants;
import es.ricoh.webscan.services.soap.common.interceptors.TraceIdExtractionInInterceptor;
import es.ricoh.webscan.spacers.business.Spacer;
import es.ricoh.webscan.spacers.business.SpacerDocRequest;
import es.ricoh.webscan.spacers.business.SpacerRequest;
import es.ricoh.webscan.spacers.business.SpacersException;
import es.ricoh.webscan.spacers.business.service.SpacersService;
import es.ricoh.webscan.spacers.model.domain.BatchSpacerType;
import es.ricoh.webscan.spacers.repository.SpacersDAOException;
import es.ricoh.webscan.spacers.service.api.soap.SpacersGeneration;
import es.ricoh.webscan.spacers.service.api.soap.WebscanException;
import es.ricoh.webscan.spacers.service.api.soap.messages.GenerateDocsBatchSpacerResponseType;
import es.ricoh.webscan.spacers.service.api.soap.messages.GenerateReusableDocSpacersResponseType;
import es.ricoh.webscan.spacers.service.api.soap.messages.InfoCodingModeType;
import es.ricoh.webscan.spacers.service.api.soap.messages.MetadataCollectionType;
import es.ricoh.webscan.spacers.service.api.soap.messages.MetadataType;
import es.ricoh.webscan.spacers.service.api.soap.messages.OperationResultType;
import es.ricoh.webscan.spacers.service.api.soap.messages.SpacerDocType;
import es.ricoh.webscan.spacers.service.api.soap.messages.SpacerDocsType;
import es.ricoh.webscan.spacers.service.api.soap.messages.SpacerType;
import es.ricoh.webscan.spacers.service.api.soap.messages.SpacersResponseResParamType;
import es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.Atributos;
import es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.Estado;

/**
 * Implementación del interfaz SOAP del servicio de generación de carátulas y
 * separadores reutilizables de documentos publicado por el activo Webscan-GV.
 * <p>
 * Clase SpacersGenerationSOAPImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.1.1.
 */
@javax.jws.WebService(serviceName = "SpacersGeneration", portName = "SpacersGenerationSOAP", targetNamespace = "urn:es:gv:dgti:webscan:services:spacersGeneration:1.1.1", wsdlLocation = "classpath:spacersGeneration.wsdl", endpointInterface = "es.ricoh.webscan.spacers.service.api.soap.SpacersGeneration")
public class SpacersGenerationSOAPImpl implements SpacersGeneration {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SpacersGenerationSOAPImpl.class.getName());

	/**
	 * Clase encargada de la construcción de la mensajería incluida en
	 * peticiones y respuestas del servicio.
	 */
	private static final es.ricoh.webscan.spacers.service.api.soap.messages.ObjectFactory MSG_OBJECT_FACTORY = new es.ricoh.webscan.spacers.service.api.soap.messages.ObjectFactory();

	/**
	 * Clase encargada de la construcción de la mensajería incluida en el
	 * detalle de soap faults.
	 */
	private static final es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.ObjectFactory FAULT_ATT_OBJECT_FACTORY = new es.redsara.intermediacion.scsp.esquemas.v3.soapfaultatributos.ObjectFactory();

	/**
	 * Código de resultado correcto.
	 */
	private static final String OK_RESULT_CODE = "COD_000";

	/**
	 * Fachada de la lógica de negocio de la utilidad de generación de carátulas
	 * y separadores reutilizables.
	 */
	private SpacersService spacersService;

	/* (non-Javadoc)
	 * @see es.ricoh.webscan.spacers.service.api.soap.SpacersGeneration#generateReusableDocSpacers(es.ricoh.webscan.spacers.service.api.soap.messages.GenerateReusableDocSpacersRequestType parameters)*
	 */
	public es.ricoh.webscan.spacers.service.api.soap.messages.GenerateReusableDocSpacersResponseType generateReusableDocSpacers(es.ricoh.webscan.spacers.service.api.soap.messages.GenerateReusableDocSpacersRequestType parameters) throws WebscanException {
		BatchSpacerType spacerType;
		GenerateReusableDocSpacersResponseType res = null;
		Integer numberOfReusableDocSpacers = null;
		OperationResultType result;
		Spacer spacer;
		SpacerType respSpacer;
		SpacersResponseResParamType responseParam;
		String traceId;

		LOG.info("[WEBSCAN-SPACERS-SOAP] Recibida petición de generación de separadores reutilizables de documentos ...");

		try {
			traceId = (String) PhaseInterceptorChain.getCurrentMessage().getExchange().get(TraceIdExtractionInInterceptor.TRACE_HEADER_NAME);
			if (LOG.isInfoEnabled()) {
				LOG.info("[WEBSCAN-SPACERS-SOAP] Identificador de trazabilidad: " + traceId + ".");
			}

			if (parameters == null) {
				throw new SpacersException(SpacersException.CODE_998, "Petición de generación de separadores reutilizables de documentos mal formada.");
			}

			LOG.debug("[WEBSCAN-SPACERS-SOAP] Parseando la petición de generación de separadores reutilizables de documentos ...");

			if (parameters.getSpacerGenerationParams() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Petición de generación de separadores reutilizables de documentos mal formada. No se especificaron los parámetros de generación de los separadores reutilizables.");
			}

			if (parameters.getSpacerGenerationParams().getInfoCodingMode() == null) {
				throw new SpacersException(SpacersException.CODE_998, "El tipo de código de barras a emplear es un parámetro requerido por el servicio.");
			}
			spacerType = spacersService.getRequestBatchSpacerType(parameters.getSpacerGenerationParams().getInfoCodingMode().value());

			// Invocando el servicio de generación de carátulas y separadores
			// reutilizables
			LOG.debug("[WEBSCAN-SPACERS-SOAP] Se generan los separadores reutilizables de documentos solicitados ...");
			if (parameters.getNumberOfReusableDocSpacers() != null) {
				numberOfReusableDocSpacers = parameters.getNumberOfReusableDocSpacers().intValue();
			}
			spacer = spacersService.generateReusableDocSpacers(spacerType, numberOfReusableDocSpacers, parameters.getUserId(), traceId, spacersService.getLocale(parameters.getSpacerGenerationParams().getLocale()));

			// Se contruye la respuesta
			res = MSG_OBJECT_FACTORY.createGenerateReusableDocSpacersResponseType();
			result = MSG_OBJECT_FACTORY.createOperationResultType();
			result.setCode(OK_RESULT_CODE);
			result.setMessage("Separadores reutilizables de documentos generados correctamente");
			res.setResult(result);
			responseParam = MSG_OBJECT_FACTORY.createSpacersResponseResParamType();
			respSpacer = MSG_OBJECT_FACTORY.createSpacerType();
			respSpacer.setMimeType(spacer.getMimeType());
			DataSource source = new ByteArrayDataSource(spacer.getContent(), spacer.getMimeType());
			respSpacer.setBinarySpacer(new DataHandler(source));
			responseParam.setSpacers(respSpacer);
			res.setResParam(responseParam);

			res = MSG_OBJECT_FACTORY.createGenerateReusableDocSpacersResponse(res).getValue();

			LOG.info("[WEBSCAN-SPACERS-SOAP] Petición de generación de separadores reutilizables de documentos realizada correctamente...");
		} catch (SpacersException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-SOAP] Error al realizar una petición de generación de separadores reutilizables de documentos. \n" + e.getMessage(), e);
			}
			res = buildReusableDocsExceptionErrorResponse(e);
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-SOAP] Error al realizar una petición de generación de separadores reutilizables de documentos. \n" + e.getMessage(), e);
			}
			res = buildReusableDocsExceptionErrorResponse(e);
		}

		return res;
	}

	/* (non-Javadoc)
	 * @see es.ricoh.webscan.spacers.service.api.soap.SpacersGeneration#generateDocsBatchSpacer(es.ricoh.webscan.spacers.service.api.soap.messages.GenerateDocsBatchSpacerRequestType parameters)*
	 */
	public es.ricoh.webscan.spacers.service.api.soap.messages.GenerateDocsBatchSpacerResponseType generateDocsBatchSpacer(es.ricoh.webscan.spacers.service.api.soap.messages.GenerateDocsBatchSpacerRequestType parameters) throws WebscanException {
		BatchSpacerType spacerType;
		GenerateDocsBatchSpacerResponseType res = null;
		InfoCodingModeType infoCodingModeType;
		OperationResultType result;
		Spacer spacer;
		SpacerRequest serviceRequest;
		SpacerType respSpacer;
		SpacersResponseResParamType responseParam;
		String traceId;

		LOG.info("[WEBSCAN-SPACERS-SOAP] Recibida petición de generación de carátula de lote de documentos ...");

		try {
			traceId = (String) PhaseInterceptorChain.getCurrentMessage().getExchange().get(TraceIdExtractionInInterceptor.TRACE_HEADER_NAME);
			if (LOG.isInfoEnabled()) {
				LOG.info("[WEBSCAN-SPACERS-SOAP] Identificador de trazabilidad: " + traceId + ".");
			}

			if (parameters == null) {
				throw new SpacersException(SpacersException.CODE_998, "Petición de generación de carátula de lote de documentos mal formada.");
			}

			if (parameters.getDepartment() == null || parameters.getDepartment().isEmpty()) {
				throw new SpacersException(SpacersException.CODE_998, "No se ha especificado la unidad orgánica a la que serán asociados los documentos del lote.");
			} else {
				if (parameters.getDepartment().length() > 50) {
					throw new SpacersException(SpacersException.CODE_998, "El identificador de unidad orgánica tiene un tamaño superior al permitido.");
				}
			}

			if (parameters.getSpacerGenerationParams() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Petición de generación de carátula de lote de documentos mal formada. Tipo de código no informado en la petición.");
			}

			if (parameters.getSpacerAdditionalInfoParams() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Perfil de digitalización y definición de documentos no especificados.");
			}

			if (parameters.getSpacerAdditionalInfoParams().getScanProfile() == null) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Perfil de digitalización no especificado.");
			}

			if (parameters.getSpacerDocs() == null || parameters.getSpacerDocs().getSpacerDoc() == null || parameters.getSpacerDocs().getSpacerDoc().isEmpty()) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Documentos no especificados.");
			}

			// Validación de la petición recibida
			LOG.debug("[WEBSCAN-SPACERS-SOAP] Parseando la petición de generación de carátula de lote de documentos ...");

			infoCodingModeType = parameters.getSpacerGenerationParams().getInfoCodingMode();

			if (infoCodingModeType == null) {
				throw new SpacersException(SpacersException.CODE_998, "Parámetro de entrada incorrecto. Se ha informado un tipo de código de barras no admitido por el sistema.");
			}

			spacerType = spacersService.getRequestBatchSpacerType(infoCodingModeType.value());

			serviceRequest = new SpacerRequest();
			serviceRequest.setDepartment(parameters.getDepartment());
			serviceRequest.setSpacerType(spacerType);
			serviceRequest.setDocSpecName(parameters.getSpacerAdditionalInfoParams().getDocSpecName());
			serviceRequest.setScanTemplateName(parameters.getSpacerAdditionalInfoParams().getScanProfile().getScanTemplateName());
			serviceRequest.setUserGroupExternalId(parameters.getSpacerAdditionalInfoParams().getScanProfile().getUserGroupExternalId());
			serviceRequest.setReqUserId(parameters.getUserId());

			if (parameters.getBatchSpacerValParams() != null) {
				if (parameters.getBatchSpacerValParams().getNumberOfDocsInBatch() != null) {
					serviceRequest.setBatchDocSize(parameters.getBatchSpacerValParams().getNumberOfDocsInBatch().intValue());
				}
				if (parameters.getBatchSpacerValParams().getNumberOfPagesInBatch() != null) {
					serviceRequest.setBatchPages(parameters.getBatchSpacerValParams().getNumberOfPagesInBatch().intValue());
				}
				if (parameters.getBatchSpacerValParams().getReuseDocsBatchSpacer() != null) {
					serviceRequest.setDocSpacerType(spacersService.getRequestDocSpacerType(parameters.getBatchSpacerValParams().getReuseDocsBatchSpacer().value()));
				}
			}

			serviceRequest.setDocuments(transformSpacerDocsType(parameters.getSpacerDocs()));

			if (parameters.getNumberOfReusableDocSpacers() != null) {
				serviceRequest.setNumberOfReusableDocSpacers(parameters.getNumberOfReusableDocSpacers().intValue());
			}

			LOG.debug("[WEBSCAN-SPACERS-SOAP] Se genera la carátula y separadores reutilizables de documentos solicitados ...");
			spacer = spacersService.generateDocsBatchSpacer(serviceRequest, traceId, Boolean.TRUE, spacersService.getLocale(parameters.getSpacerGenerationParams().getLocale()));

			res = MSG_OBJECT_FACTORY.createGenerateDocsBatchSpacerResponseType();
			result = MSG_OBJECT_FACTORY.createOperationResultType();
			result.setCode(OK_RESULT_CODE);
			result.setMessage("Carátula y separadores reutilizables de documentos generados correctamente");
			res.setResult(result);
			responseParam = MSG_OBJECT_FACTORY.createSpacersResponseResParamType();
			respSpacer = MSG_OBJECT_FACTORY.createSpacerType();
			respSpacer.setMimeType(spacer.getMimeType());
			DataSource source = new ByteArrayDataSource(spacer.getContent(), spacer.getMimeType());
			respSpacer.setBinarySpacer(new DataHandler(source));
			responseParam.setSpacers(respSpacer);
			res.setResParam(responseParam);

			res = MSG_OBJECT_FACTORY.createGenerateDocsBatchSpacerResponse(res).getValue();
			LOG.info("[WEBSCAN-SPACERS-SOAP] Petición de generación de carátula de lote de documentos realizada correctamente...");

		} catch (SpacersDAOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-SOAP] Error al realizar una petición de generación de carátula de lote de documentos. \n" + e.getMessage(), e);
			}
			res = buildDocsBatchExceptionErrorResponse(e);
		} catch (SpacersException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-SOAP] Error al realizar una petición de generación de carátula de lote de documentos. \n" + e.getMessage(), e);
			}
			res = buildDocsBatchExceptionErrorResponse(e);
		} catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("[WEBSCAN-SPACERS-SOAP] Error al realizar una petición de generación de carátula de lote de documentos. \n" + e.getMessage(), e);
			}
			res = buildDocsBatchExceptionErrorResponse(e);
		}

		return res;

	}

	/**
	 * Transforma la lista de documentos informados en la petición en un lista
	 * de objetos SpacerDocRequest.
	 * 
	 * @param documents
	 *            lista de documentos informados en la petición.
	 * @return Relación de objetos SpacerDocRequest que incluye para cada
	 *         documento su denominación, número de orden en el lote y
	 *         metadatos.
	 * @throws SpacersDAOException
	 *             En caso de producirse un error al procesar los metadatos de
	 *             los documentos.
	 * @throws SpacersException
	 *             En caso de producirse algún error al procesar la información
	 *             de los documentos.
	 */
	private List<SpacerDocRequest> transformSpacerDocsType(SpacerDocsType documents) throws SpacersDAOException, SpacersException {
		List<SpacerDocRequest> res = new ArrayList<SpacerDocRequest>();
		SpacerDocRequest doc;
		Integer orderNumber;
		for (SpacerDocType document: documents.getSpacerDoc()) {
			doc = new SpacerDocRequest();
			doc.setName(document.getName());

			try {
				orderNumber = Integer.parseInt(document.getOrderNumber().toString());
				doc.setOrder(orderNumber);
				doc.setMetadataCollection(transformMetadataCollectionType(document.getMetadataCollection(), orderNumber));
			} catch (SpacersDAOException e) {
				throw e;
			} catch (SpacersException e) {
				throw e;
			} catch (Exception e) {
				// Do nothing
			}

			res.add(doc);
		}

		return res;
	}

	/**
	 * Transforma la lista de metadatos informados para un documento en la
	 * petición en un Map<String, String>.
	 * 
	 * @param metadataCollection
	 *            lista de metadatos informados en la petición.
	 * @param docOrderNum
	 *            número de orden del documento en el lote.
	 * @return Relación de pares clave / valor, identificados mediante el nombre
	 *         de los metadatos, y cuyo valor se corresponde con el valor
	 *         informado para el metadato de un documento en la petición.
	 * @throws SpacersDAOException
	 *             Si no se especifica el identificador de algún metadato.
	 * @throws SpacersException
	 *             Si más de un metadato tiene el mismo identificador.
	 */
	private Map<String, String> transformMetadataCollectionType(MetadataCollectionType metadataCollection, Integer docOrderNum) throws SpacersDAOException, SpacersException {
		Map<String, String> res = new HashMap<String, String>();

		if (metadataCollection != null && metadataCollection.getMetadata() != null) {
			for (MetadataType metadata: metadataCollection.getMetadata()) {
				if (metadata.getId() == null || metadata.getId().isEmpty()) {
					throw new SpacersDAOException(SpacersDAOException.CODE_504, "No se ha especificado la denominación de uno o más metadatos para el documento " + docOrderNum + ".");
				}

				if (res.containsKey(metadata.getId())) {
					throw new SpacersException(SpacersException.CODE_004, "El metadato " + metadata.getId() + " ha sido informado más de una vez en la petición para el documento " + docOrderNum + ".");
				}

				res.put(metadata.getId(), metadata.getValue());
			}

		}

		return res;
	}

	/**
	 * Construye una respuesta errónea del método de generación de carátulas de
	 * lotes de documentos.
	 * 
	 * @param e
	 *            excepción capturada.
	 * @return respuesta errónea del método de generación de carátulas de lotes
	 *         de documentos.
	 * @throws es.gv.dgti.webscan.services.spacersgeneration.Exception
	 *             Si no es un error de negocio.
	 */
	private GenerateDocsBatchSpacerResponseType buildDocsBatchExceptionErrorResponse(Exception e) throws WebscanException {
		Atributos faultAtts;
		Boolean throwException = Boolean.TRUE;
		Estado statusFaulAtts;
		GenerateDocsBatchSpacerResponseType res = null;
		OperationResultType result;
		String excCode = SpacersException.CODE_999;
		String eCode;
		String msgExc = "";

		if (e instanceof SpacersDAOException) {
			eCode = ((SpacersDAOException) e).getCode();
			if (SpacersDAOException.CODE_511.equals(eCode) || SpacersDAOException.CODE_512.equals(eCode)) {
				// Error de negocio
				throwException = Boolean.FALSE;
				res = MSG_OBJECT_FACTORY.createGenerateDocsBatchSpacerResponseType();
				result = MSG_OBJECT_FACTORY.createOperationResultType();
				result.setCode(eCode);
				result.setMessage(((SpacersDAOException) e).getMessage());
				res.setResult(result);
				res = MSG_OBJECT_FACTORY.createGenerateDocsBatchSpacerResponse(res).getValue();
			} else {
				excCode = eCode;
				msgExc = e.getMessage();
			}
		} else if (e instanceof SpacersException) {
			eCode = ((SpacersException) e).getCode();
			if (!SpacersException.CODE_999.equals(eCode) && !SpacersException.CODE_998.equals(eCode)) {
				throwException = Boolean.FALSE;
				res = MSG_OBJECT_FACTORY.createGenerateDocsBatchSpacerResponseType();
				result = MSG_OBJECT_FACTORY.createOperationResultType();
				result.setCode(eCode);
				result.setMessage(((SpacersException) e).getMessage());
				res.setResult(result);
				res = MSG_OBJECT_FACTORY.createGenerateDocsBatchSpacerResponse(res).getValue();
			} else {
				excCode = ((SpacersException) e).getCode();
				msgExc = e.getMessage();
			}
		} else {
			excCode = SpacersException.CODE_999;
			msgExc = (e.getMessage() != null ? e.getMessage() : e.getClass().getName());
		}

		if (throwException) {
			faultAtts = FAULT_ATT_OBJECT_FACTORY.createAtributos();
			faultAtts.setCodigoCertificado("");
			statusFaulAtts = FAULT_ATT_OBJECT_FACTORY.createEstado();
			statusFaulAtts.setCodigoEstado("0" + excCode.substring("COD_".length(), excCode.length()));
			statusFaulAtts.setCodigoEstadoSecundario("");
			if (msgExc.length() > 255) {
				statusFaulAtts.setLiteralError(msgExc.substring(0, 255));
			}
			statusFaulAtts.setTiempoEstimadoRespuesta(0);
			faultAtts.setEstado(statusFaulAtts);
			faultAtts.setIdPeticion("");
			faultAtts.setNumElementos(1);
			faultAtts.setTimeStamp(WebscanServiceConstants.ISO_8601_DATE_FORMAT.format(new Date()));

			throw new WebscanException("[" + excCode + "] " + msgExc, faultAtts, e);
		}

		return res;
	}

	/**
	 * Construye una respuesta errónea del método de generación de separadores
	 * reutilizables de documentos.
	 * 
	 * @param e
	 *            excepción capturada.
	 * @return respuesta errónea del método de generación de separadores
	 *         reutilizables de documentos.
	 * @throws es.gv.dgti.webscan.services.spacersgeneration.Exception
	 *             Si no es un error de negocio.
	 */
	private GenerateReusableDocSpacersResponseType buildReusableDocsExceptionErrorResponse(Exception e) throws WebscanException {
		Atributos faultAtts;
		Boolean throwException = Boolean.TRUE;
		Estado statusFaulAtts;
		GenerateReusableDocSpacersResponseType res = null;
		OperationResultType result;
		String excCode = SpacersException.CODE_999;
		String msgExc = "";

		if (e instanceof SpacersDAOException) {
			excCode = ((SpacersDAOException) e).getCode();
			msgExc = e.getMessage();
		} else if (e instanceof SpacersException) {
			if (!SpacersException.CODE_999.equals(((SpacersException) e).getCode()) && !SpacersException.CODE_998.equals(((SpacersException) e).getCode())) {
				throwException = Boolean.FALSE;
				res = MSG_OBJECT_FACTORY.createGenerateReusableDocSpacersResponseType();
				result = MSG_OBJECT_FACTORY.createOperationResultType();
				result.setCode(((SpacersException) e).getCode());
				result.setMessage(((SpacersException) e).getMessage());
				res.setResult(result);
				res = MSG_OBJECT_FACTORY.createGenerateReusableDocSpacersResponse(res).getValue();
			} else {
				excCode = ((SpacersException) e).getCode();
				msgExc = e.getMessage();
			}
		} else {
			excCode = SpacersException.CODE_999;
			msgExc = e.getMessage();
		}

		if (throwException) {
			faultAtts = FAULT_ATT_OBJECT_FACTORY.createAtributos();
			faultAtts.setCodigoCertificado("");
			statusFaulAtts = FAULT_ATT_OBJECT_FACTORY.createEstado();
			statusFaulAtts.setCodigoEstado("0" + excCode.substring("COD_".length(), excCode.length()));
			statusFaulAtts.setCodigoEstadoSecundario("");
			if (msgExc.length() > 255) {
				statusFaulAtts.setLiteralError(msgExc.substring(0, 255));
			}
			statusFaulAtts.setTiempoEstimadoRespuesta(0);
			faultAtts.setEstado(statusFaulAtts);
			faultAtts.setIdPeticion("");
			faultAtts.setNumElementos(1);
			faultAtts.setTimeStamp(WebscanServiceConstants.ISO_8601_DATE_FORMAT.format(new Date()));

			throw new WebscanException("[" + excCode + "] " + msgExc, faultAtts, e);
		}

		return res;
	}

	/**
	 * Establece la fachada de la lógica de negocio de la utilidad de generación
	 * de carátulas y separadores reutilizables.
	 * 
	 * @param aSpacersService
	 *            Fachada de la lógica de negocio de la utilidad de generación
	 *            de carátulas y separadores reutilizables.
	 */
	public void setSpacersService(SpacersService aSpacersService) {
		this.spacersService = aSpacersService;
	}

}
