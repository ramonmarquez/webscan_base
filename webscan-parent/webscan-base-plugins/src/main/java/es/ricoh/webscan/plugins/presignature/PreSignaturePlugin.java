/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.plugins.presignature;

import es.ricoh.webscan.plugins.Plugin;

/**
 * Interface de PreSignaturePlugin.
 * <p>
 * Clase PreSignaturePlugin.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface PreSignaturePlugin extends Plugin {

	/**
	 * Setea el documento.
	 * 
	 * @param document
	 *            array de byte con el documento.
	 */
	void setDocument(byte[ ] document);

	/**
	 * Obtiene el documento.
	 * 
	 * @return array de byte con la info del documento.
	 */
	byte[ ] getDocument();

	/**
	 * Indica si el documento ha sido modificado.
	 * 
	 * @return Boolean indicando si ha sido o no modificado el documento.
	 */
	boolean isDocumentModified();

	/**
	 * Obtiene la información adicional del documento.
	 * 
	 * @return Devuelve un objeto con la información adicional.
	 */
	Object getAdditionalInfo();

	/**
	 * Indica la clase del objeto que instancia la información adicional del
	 * documento.
	 * 
	 * @return Clase del objeto de la instancia de la inforamción adicional.
	 */
	Class<?> additionalInfoClazz();
}
