/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.plugins.PluginProvider.java.</p>
 * <b>Descripción:</b><p> Proveedor de beans que implementan las diferentes definiciones de plugins
 * Webscan desplegadas en el Sistema.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.plugins;

import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNotOfRequiredTypeException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import es.ricoh.webscan.plugins.deposit.DepositPlugin;
import es.ricoh.webscan.plugins.postsignature.PostSignaturePlugin;
import es.ricoh.webscan.plugins.presignature.PreSignaturePlugin;
import es.ricoh.webscan.plugins.signature.SignaturePlugin;

/**
 * Proveedor de beans que implementan las diferentes definiciones de plugins
 * Webscan desplegadas en el Sistema.
 * <p>
 * Clase PluginProvider.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Service("pluginProvider")
public class PluginProvider implements ApplicationContextAware {

	/**
	 * Conexto Spring del Sistema.
	 */
	@Autowired
	private ApplicationContext applicationContext;

	/**
	 * Acceso a la configuración de los beans Spring que implementan los
	 * diferentes plugins.
	 */
	@Autowired
	private Properties pluginsConfParams;

	/**
	 * Prefijo de los parámetro de configuración que especifican la
	 * implementación de un plugin para la firma de documentos digitalizados.
	 */
	private static final String SIGNATURE_PREFIX_PROP_NAME = "signature";
	/**
	 * Prefijo de los parámetro de configuración que especifican la
	 * implementación de un plugin para el depósito de documentos digitalizados.
	 */
	private static final String DEPOSIT_PREFIX_PROP_NAME = "deposit";
	/**
	 * Prefijo de los parámetro de configuración que especifican la
	 * implementación de un plugin para la realización de operaciones antes de
	 * la firma de documentos digitalizados.
	 */
	private static final String PRESIGNATURE_PREFIX_PROP_NAME = "presign";

	/**
	 * Prefijo de los parámetro de configuración que especifican la
	 * implementación de un plugin para la realización de operaciones después de
	 * la firma de documentos digitalizados.
	 */
	private static final String POSTSIGNATURE_PREFIX_PROP_NAME = "postsign";

	/**
	 * Constructor sin argumentos de la clase.
	 */
	public PluginProvider() {
		super();
	};

	/**
	 * Obtiene una definición de plugins para la realización de alguna operación
	 * sobre uno o más documentos digitalizados.
	 * 
	 * @param specName
	 *            denominación de la definición de plugin a recuperar.
	 * @return una definición de plugins para la realización de alguna operación
	 *         sobre uno o más documentos digitalizados.
	 * @throws PluginException
	 *             Si no existe el plugin, o el bean recuperado no implementa la
	 *             interfaz Plugin.
	 */
	public Plugin getPlugin(String specName) throws PluginException {
		Plugin res = null;
		String beanName;

		if (specName == null || specName.isEmpty()) {
			throw new PluginException(PluginException.CODE_998, "Denominación de la definición de plugins no especificada.");
		}

		try {
			beanName = pluginsConfParams.getProperty(specName);
			if (beanName == null) {
				throw new NoSuchBeanDefinitionException("beanName is null, compruebe fichero de configuración");
			}
			res = applicationContext.getBean(beanName, Plugin.class);
		} catch (NoSuchBeanDefinitionException e) {
			// if there is no such bean definition
			throw new PluginException(PluginException.CODE_600, e);
		} catch (BeanNotOfRequiredTypeException e) {
			// if the bean is not of the required type
			throw new PluginException(PluginException.CODE_601, e);
		} catch (BeansException e) {
			// if the bean could not be created
			throw new PluginException(PluginException.CODE_602, e);
		}

		return res;
	}

	/**
	 * Obtiene una definición de plugins para la firma de documentos
	 * digitalizados.
	 * 
	 * @param specName
	 *            denominación de la definición de plugin a recuperar.
	 * @return una definición de plugins para la firma de documentos
	 *         digitalizados.
	 * @throws PluginException
	 *             Si no existe el plugin, o el bean recuperado no implementa la
	 *             interfaz SignaturePlugin.
	 */
	public SignaturePlugin getSignaturePlugin(String specName) throws PluginException {
		SignaturePlugin res = null;
		String beanName;

		if (specName == null || specName.isEmpty()) {
			throw new PluginException(PluginException.CODE_998, "Denominación de la definición de plugins no especificada.");
		}

		try {
			beanName = pluginsConfParams.getProperty(SIGNATURE_PREFIX_PROP_NAME + "." + specName);
			res = applicationContext.getBean(beanName, SignaturePlugin.class);
		} catch (NoSuchBeanDefinitionException e) {
			// if there is no such bean definition
			throw new PluginException(PluginException.CODE_600, e);
		} catch (BeanNotOfRequiredTypeException e) {
			// if the bean is not of the required type
			throw new PluginException(PluginException.CODE_601, e);
		} catch (BeansException e) {
			// if the bean could not be created
			throw new PluginException(PluginException.CODE_602, e);
		}

		return res;
	}

	/**
	 * Obtiene una definición de plugins para la realización de operaciones
	 * sobre documentos digitalizados antes de la firma.
	 * 
	 * @param specName
	 *            denominación de la definición de plugin a recuperar.
	 * @return una definición de plugins para la realización de operaciones
	 *         sobre documentos digitalizados antes de la firma.
	 * @throws PluginException
	 *             Si no existe el plugin, o el bean recuperado no implementa la
	 *             interfaz PreSignaturePlugin.
	 */
	public PreSignaturePlugin getPreSignaturePlugin(String specName) throws PluginException {
		PreSignaturePlugin res = null;
		String beanName;

		if (specName == null || specName.isEmpty()) {
			throw new PluginException(PluginException.CODE_998, "Denominación de la definición de plugins no especificada.");
		}

		try {
			beanName = pluginsConfParams.getProperty(PRESIGNATURE_PREFIX_PROP_NAME + "." + specName);
			res = applicationContext.getBean(beanName, PreSignaturePlugin.class);
		} catch (NoSuchBeanDefinitionException e) {
			// if there is no such bean definition
			throw new PluginException(PluginException.CODE_600, e);
		} catch (BeanNotOfRequiredTypeException e) {
			// if the bean is not of the required type
			throw new PluginException(PluginException.CODE_601, e);
		} catch (BeansException e) {
			// if the bean could not be created
			throw new PluginException(PluginException.CODE_602, e);
		}

		return res;
	}

	/**
	 * Obtiene una definición de plugins para la realización de operaciones
	 * sobre documentos digitalizados tras la firma.
	 * 
	 * @param specName
	 *            denominación de la definición de plugin a recuperar.
	 * @return una definición de plugins para la realización de operaciones
	 *         sobre documentos digitalizados tras la firma.
	 * @throws PluginException
	 *             Si no existe el plugin, o el bean recuperado no implementa la
	 *             interfaz PostSignaturePlugin.
	 */
	public PostSignaturePlugin getPostSignaturePlugin(String specName) throws PluginException {
		PostSignaturePlugin res = null;
		String beanName;

		if (specName == null || specName.isEmpty()) {
			throw new PluginException(PluginException.CODE_998, "Denominación de la definición de plugins no especificada.");
		}

		try {
			beanName = pluginsConfParams.getProperty(POSTSIGNATURE_PREFIX_PROP_NAME + "." + specName);
			res = applicationContext.getBean(beanName, PostSignaturePlugin.class);
		} catch (NoSuchBeanDefinitionException e) {
			// if there is no such bean definition
			throw new PluginException(PluginException.CODE_600, e);
		} catch (BeanNotOfRequiredTypeException e) {
			// if the bean is not of the required type
			throw new PluginException(PluginException.CODE_601, e);
		} catch (BeansException e) {
			// if the bean could not be created
			throw new PluginException(PluginException.CODE_602, e);
		}

		return res;
	}

	/**
	 * Obtiene una definición de plugins de depósito de documentos
	 * digitalizados.
	 * 
	 * @param specName
	 *            denominación de la definición de plugin a recuperar.
	 * @return una definición de plugins de depósito de documentos
	 *         digitalizados.
	 * @throws PluginException
	 *             Si no existe el plugin, o el bean recuperado no implementa la
	 *             interfaz DepositPlugin.
	 */
	public DepositPlugin getDepositPlugin(String specName) throws PluginException {
		DepositPlugin res = null;
		String beanName;

		if (specName == null || specName.isEmpty()) {
			throw new PluginException(PluginException.CODE_998, "Denominación de la definición de plugins no especificada.");
		}

		try {
			beanName = pluginsConfParams.getProperty(DEPOSIT_PREFIX_PROP_NAME + "." + specName);
			res = applicationContext.getBean(beanName, DepositPlugin.class);
		} catch (NoSuchBeanDefinitionException e) {
			// if there is no such bean definition
			throw new PluginException(PluginException.CODE_600, e);
		} catch (BeanNotOfRequiredTypeException e) {
			// if the bean is not of the required type
			throw new PluginException(PluginException.CODE_601, e);
		} catch (BeansException e) {
			// if the bean could not be created
			throw new PluginException(PluginException.CODE_602, e);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext anApplicationContext) throws BeansException {
		applicationContext = anApplicationContext;
	}

}
