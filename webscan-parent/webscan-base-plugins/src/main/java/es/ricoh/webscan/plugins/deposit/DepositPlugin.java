/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.plugins.deposit.DepositPlugin.java.</p>
* <b>Descripción:</b><p> Interfaz que define los métodos a implementar por los diferentes plugins de
 * depósito de documentos.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.plugins.deposit;

import java.util.Map;

import es.ricoh.webscan.plugins.Plugin;
import es.ricoh.webscan.plugins.PluginException;

/**
 * Interfaz que define los métodos a implementar por los diferentes plugins de
 * depósito de documentos.
 * <p>
 * Clase DepositPlugin.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface DepositPlugin extends Plugin {

	/**
	 * Obtiene el destino del depósito de los documento.
	 * 
	 * @return el destino del depósito de los documento.
	 */
	String getDepositPath();

	/**
	 * Obtiene el contenido de un documento.
	 * 
	 * @param docId
	 *            Identificador del documento.
	 * @return el contenido del documento.
	 * @throws PluginException
	 *             Si se produce algún error recuperando la información del
	 *             documento.
	 */
	byte[ ] getDocument(Long docId) throws PluginException;

	/**
	 * Obtiene la firma electrónica del documento.
	 * 
	 * @param docId
	 *            Identificador del documento.
	 * @return la firma electrónica del documento.
	 * @throws PluginException
	 *             Si se produce algún error recuperando la información del
	 *             documento.
	 */
	byte[ ] getSignature(Long docId) throws PluginException;

	/**
	 * Obtiene los metadatos del documento.
	 * 
	 * @param docId
	 *            Identificador del documento.
	 * @return los metadatos del documento.
	 * @throws PluginException
	 *             Si se produce algún error recuperando la información del
	 *             documento.
	 */
	Map<String, String> getMetadata(Long docId) throws PluginException;
}
