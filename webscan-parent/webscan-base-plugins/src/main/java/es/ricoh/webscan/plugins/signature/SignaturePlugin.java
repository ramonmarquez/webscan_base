/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.plugins.signature.SignaturePlugin.java.</p>
* <b>Descripción:</b><p> Interfaz a implementar por los plugins de firma.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.plugins.signature;

import es.ricoh.webscan.plugins.Plugin;

/**
 * Interfaz a implementar por los plugins de firma.
 * <p>
 * Clase SignaturePlugin.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface SignaturePlugin extends Plugin {

	/**
	 * Establece el contenido del documento que es firmado.
	 * 
	 * @param aDocument
	 *            Contenido del documento firmado.
	 */
	void setDocument(byte[ ] aDocument);

	/**
	 * Obtiene el contenido del documento que es firmado.
	 * 
	 * @return el contenido del documento que es firmado.
	 */
	byte[ ] getDocument();

	/**
	 * Obtiene la firma del documento.
	 * 
	 * @return la firma del documento.
	 */
	byte[ ] getSignature();

	/**
	 * Indica si el propio documento incluye la firma.
	 * 
	 * @return si el propio documento incluye la firma, devuelve true. En caso
	 *         contrario, retorna false.
	 */
	boolean isDocumentModified();

	/**
	 * Recupera información adicional retornada por el plugin tras la ejecución
	 * de la operación implementada por el plugin.
	 * 
	 * @return información adicional retornada por el plugin tras la ejecución
	 *         de la operación implementada por el plugin.
	 */
	Object getAdditionalInfo();

	/**
	 * Permite conocer la clase empleada para representar la información
	 * adicional retornada por el plugin tras la ejecución de la operación
	 * implementada por el plugin.
	 * 
	 * @return la clase empleada para representar la información adicional
	 *         retornada por el plugin tras la ejecución de la operación
	 *         implementada por el plugin.
	 */
	Class<?> getAdditionalInfoClazz();

}
