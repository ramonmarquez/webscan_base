/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.plugins.deposit.PostSignaturePlugin.java.</p>
* <b>Descripción:</b><p> Interfaz que define los métodos a implementar por los diferentes plugins ejecutados trás la firma de los documentos, y previo a su depósito.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.plugins.postsignature;

import es.ricoh.webscan.plugins.Plugin;

/**
 * Interfaz que define los métodos a implementar por los diferentes plugins
 * ejecutados trás la firma de los documentos, y previo a su depósito.
 * <p>
 * Clase PostSignaturePlugin.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface PostSignaturePlugin extends Plugin {

	/**
	 * Recupera información adicional retornada por el plugin tras la ejecución
	 * de la operación implementada por el plugin.
	 * 
	 * @return información adicional retornada por el plugin tras la ejecución
	 *         de la operación implementada por el plugin.
	 */
	Object getAdditionalInfo();

	/**
	 * Permite conocer la clase empleada para representar la información
	 * adicional retornada por el plugin tras la ejecución de la operación
	 * implementada por el plugin.
	 * 
	 * @return la clase empleada para representar la información adicional
	 *         retornada por el plugin tras la ejecución de la operación
	 *         implementada por el plugin.
	 */
	Class<?> getAdditionalInfoClazz();
}
