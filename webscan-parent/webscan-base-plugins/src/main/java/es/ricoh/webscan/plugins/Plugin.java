/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.plugins;

import java.util.List;
import java.util.Properties;

/**
 * Interfaz genérica de los plugins o componentes Webscan.
 * <p>
 * Clase Plugin.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.WebscanException
 * @version 2.0.
 */
public interface Plugin {

	/**
	 * Lleva a cabo la operación implementada por la definición que especifica
	 * el plugin establecido para la ejecución de una fase del proceso de
	 * digitalización de un perfil de digitalización configurado en una o más
	 * unidades organizativas.
	 * 
	 * @throws PluginException
	 *             Si se procude un error al ejecutar la operación implementada
	 *             por el plugin.
	 */
	void performAction() throws PluginException;

	/**
	 * Inicialización de un plugin a partir de la configuración registrada en la
	 * base de datos del Sistema.
	 * 
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @param docId
	 *            Identificador de documento digitalizado.
	 * @throws PluginException
	 *             Si se procude un error al configurar el plugin.
	 */
	void initialize(Long scanProcessStageId, Long docId) throws PluginException;

	/**
	 * Inicialización de un plugin a partir de la configuración registrada en la
	 * base de datos del Sistema.
	 * 
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @param docIds
	 *            Identificadores de documentos digitalizados.
	 * @throws PluginException
	 *             Si se procude un error al configurar el plugin.
	 */
	void initialize(Long scanProcessStageId, List<Long> docIds) throws PluginException;

	/**
	 * Obtiene los parámetros de configuración del plugin.
	 * 
	 * @return los parámetros de configuración del plugin.
	 */
	Properties getConfiguration();
}
