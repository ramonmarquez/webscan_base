/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.plugins;

import es.ricoh.webscan.WebscanException;

/**
 * Excepción que encapsula los errores producidos en el componente proovedor de
 * plugins de Webscan.
 * <p>
 * Clase PluginException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.WebscanException
 * @version 2.0.
 */
public class PluginException extends WebscanException {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * No existe el bean que implementa el plugin solicitado.
	 */
	public static final String CODE_600 = "COD_600";

	/**
	 * El bean solicitado no es de la clase esperada.
	 */
	public static final String CODE_601 = "COD_601";

	/**
	 * No pudo ser creado el bean solicitado.
	 */
	public static final String CODE_602 = "COD_602";

	/**
	 * Plugin no inicializado, error desconocido.
	 */
	public static final String CODE_603 = "COD_603";

	/**
	 * Plugin no inicializado, no fueron especificados todos los parámetros
	 * requeridos.
	 */
	public static final String CODE_604 = "COD_604";

	/**
	 * Error en ejecución de plugin por fallo en el acceso a la base de datos
	 * del sistema.
	 */
	public static final String CODE_605 = "COD_605";

	/**
	 * Error recuperando los parámetros de un plugin del modelo de datos.
	 */
	public static final String CODE_606 = "COD_606";

	/**
	 * Plugin no configurado para fase de ejecución requerida de un proceso de
	 * digitalización.
	 */
	public static final String CODE_607 = "COD_607";

	/**
	 * No existe plugin configurado para la fase actual.
	 */
	public static final String CODE_608 = "COD_608";

	/**
	 * El perfil y la unidad organizativa no tiene activo el plugin para la fase
	 * de actual.
	 */
	public static final String CODE_609 = "COD_609";

	/**
	 * El plugin no admite el procesamiento de más de un documento.
	 */
	public static final String CODE_610 = "COD_610";

	/**
	 * Error recuperando los parámetros de un plugin del modelo de datos, se
	 * obtuvo más de una configuración para una relación de documentos.
	 */
	public static final String CODE_611 = "COD_611";

	/**
	 * Constructor sin argumentos.
	 */
	public PluginException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public PluginException(String aCode, String aMessage) {
		super(aCode, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public PluginException(String aMessage) {
		super(CODE_999, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public PluginException(Throwable aCause) {
		super(CODE_999, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public PluginException(final String aMessage, Throwable aCause) {
		super(CODE_999, aMessage, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public PluginException(final String aCode, final String aMessage, Throwable aCause) {
		super(aCode, aMessage, aCause);
	}

}
