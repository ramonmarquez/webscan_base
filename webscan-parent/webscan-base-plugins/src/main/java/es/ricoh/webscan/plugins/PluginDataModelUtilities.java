/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.plugins.PluginDataModelUtilities.java.</p>
* <b>Descripción:</b><p> Clase que implementa operaciones, sobre el modelo de datos del Sistema,
* requeridas para la configuración, inicialización y ejecución de los plugins.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.plugins;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.dto.ParameterDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;

/**
 * Clase que implementa operaciones, sobre el modelo de datos del Sistema,
 * requeridas para la configuración, inicialización y ejecución de los plugins.
 * <p>
 * Clase PluginDataModelUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class PluginDataModelUtilities {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PluginDataModelUtilities.class);

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos base de Webscan.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Recupera los parámetros de configuración del plugin configurado para el
	 * perfil de digitación de un documento, en una fase dada.
	 * 
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @param docId
	 *            identificador de documento.
	 * @return relación de parámetros.
	 * @throws PluginException
	 *             Si se produce un error en la ejecución de la consulta sobre
	 *             el modelo de datos.
	 */
	public Properties getPluginConfParams(Long scanProcessStageId, Long docId) throws PluginException {
		List<ParameterDTO> pluginParams;
		Properties res = new Properties();
		String excMsg;

		try {
			pluginParams = webscanModelManager.getScanProfileOrgUnitPluginByStageAndDoc(scanProcessStageId, docId);
			for (ParameterDTO param: pluginParams) {
				res.setProperty(param.getSpecificationName(), param.getValue());
			}
		} catch (DAOException e) {
			excMsg = "Se produjo un error recuperando los parámetros del plugin que implmenta la fase " + scanProcessStageId + " para el perfil de digitalización del documento " + docId + ": " + e.getMessage();
			LOG.error("[WEBSCAN-PLUGIN] {}", excMsg);
			throw new PluginException(PluginException.CODE_606, excMsg);
		}

		return res;
	}

	/**
	 * Recupera los parámetros de configuración del plugin configurado para el
	 * perfil de digitación de una lista de documentos, en una fase dada.
	 * 
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @param docIds
	 *            Identificadores de documentos.
	 * @return relación de parámetros.
	 * @throws PluginException
	 *             Si no existe ningún plugin configurado, existe más de uno
	 *             configurado, o se produce un error en la ejecución de la
	 *             consulta sobre el modelo de datos.
	 */
	public Properties getPluginConfParams(Long scanProcessStageId, List<Long> docIds) throws PluginException {
		Map<ScanProfileOrgUnitPluginDTO, List<ParameterDTO>> pluginConfParams;
		List<ParameterDTO> pluginParams;
		Properties res = new Properties();
		String excMsg;

		try {
			pluginConfParams = webscanModelManager.getScanProfileOrgUnitPluginByStageAndDocs(scanProcessStageId, docIds);

			if (pluginConfParams == null || pluginConfParams.isEmpty()) {
				excMsg = "Se produjo un error recuperando los parámetros del plugin que implementa la fase " + scanProcessStageId + " para el perfil de digitalización de los documentos " + docIds + ". No se encontró ninguna configuración.";
				LOG.error("[WEBSCAN-PLUGIN] {}", excMsg);
				throw new PluginException(PluginException.CODE_606, excMsg);
			}

			if (pluginConfParams.size() > 1) {
				excMsg = "Se produjo un error recuperando los parámetros del plugin que implementa la fase " + scanProcessStageId + " para el perfil de digitalización de los documentos " + docIds + ". Se encontró más de una configuración.";
				LOG.error("[WEBSCAN-PLUGIN] {}", excMsg);
				throw new PluginException(PluginException.CODE_611, excMsg);
			}

			pluginParams = pluginConfParams.get(pluginConfParams.keySet().iterator().next());

			for (ParameterDTO param: pluginParams) {
				res.setProperty(param.getSpecificationName(), param.getValue());
			}
		} catch (DAOException e) {
			excMsg = "Se produjo un error recuperando los parámetros del plugin que implementa la fase " + scanProcessStageId + " para el perfil de digitalización de los documentos " + docIds + ": " + e.getMessage();
			LOG.error("[WEBSCAN-PLUGIN] {}", excMsg);
			throw new PluginException(PluginException.CODE_606, excMsg);
		} catch (PluginException e) {
			throw e;
		}

		return res;
	}

	/**
	 * Recupera la lista de definiciones de plugins que implmentan una
	 * determinada fase para un proceso de digitalización.
	 * 
	 * @param map
	 *            Relación de pares clave /valor, identificada por las fases que
	 *            conforman un proceso de digitalización, y cuyo valor se
	 *            corresponde con la lista de especificaciones de plugins que la
	 *            implementan.
	 * @param scanProfileSpecId
	 *            Identificador de proceso de digitalización.
	 * @param scanProcStage
	 *            Identificador de fase de proceso de digitalización.
	 * @return lista de definiciones de plugins que implmentan una determinada
	 *         fase para un proceso de digitalización. En caso de no
	 *         encontrarse, retorna null.
	 */
	public List<PluginSpecificationDTO> getPluginSpecificationByScanProfileSpecIdAndScanProcStageId(Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> map, Long scanProfileSpecId, String scanProcStage) {
		List<PluginSpecificationDTO> res = null;

		for (Map.Entry<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> act: map.entrySet()) {
			// obtenemos el ScanProfileSpecScanProcStageDTO
			ScanProfileSpecScanProcStageDTO revisar = act.getKey();
			if (revisar.getScanProcessStage().toUpperCase().equals(scanProcStage.toUpperCase()) && revisar.getScanProfileSpecificationId() == scanProfileSpecId) {
				return act.getValue();
			}
		}

		return res;

	}

	/**
	 * Obtiene la configuración de una fase para un proceso de digitalización.
	 * 
	 * @param map
	 *            Relación de pares clave /valor, identificada por las fases que
	 *            conforman un proceso de digitalización, y cuyo valor se
	 *            corresponde con la lista de especificaciones de plugins que la
	 *            implementan.
	 * @param scanProfileSpecId
	 *            Identificador de proceso de digitalización.
	 * @param scanProcStage
	 *            Identificador de fase de proceso de digitalización.
	 * @return configuración de una fase para un proceso de digitalización. En
	 *         caso de no encontrarse, retorna null.
	 */
	public ScanProfileSpecScanProcStageDTO getScanProfileSpecScanProcStageByScanProfileSpecIdAndScanProcStageId(Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> map, Long scanProfileSpecId, String scanProcStage) {
		ScanProfileSpecScanProcStageDTO res = null;

		for (Map.Entry<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> act: map.entrySet()) {
			// obtenemos el ScanProfileSpecScanProcStageDTO
			ScanProfileSpecScanProcStageDTO revisar = act.getKey();
			if (revisar.getScanProcessStage().toUpperCase().equals(scanProcStage.toUpperCase()) && revisar.getScanProfileSpecificationId() == scanProfileSpecId) {
				return revisar;
			}
		}

		return res;

	}

	/**
	 * Establece un nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

}
