<!--
  #%L
  Webscan 2
  %%
  Copyright (C) 2017 Ricoh Spain IT Services
  %%
  Este fichero forma parte de la solución base de digitalización certificada y 
  copia auténtica, Webscan 2. 
  
  Este fichero se distribuye bajo licencia propia Webscan 2, estando las
  condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
  fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
  #L%
  -->
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:tx="http://www.springframework.org/schema/tx"
	xmlns:util="http://www.springframework.org/schema/util" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="
		http://www.springframework.org/schema/aop 
		http://www.springframework.org/schema/aop/spring-aop.xsd
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd 
        http://www.springframework.org/schema/tx 
        http://www.springframework.org/schema/tx/spring-tx.xsd 
		http://www.springframework.org/schema/util 
		http://www.springframework.org/schema/util/spring-util.xsd">
	
	<!-- Definición del Data Source -->

   	<bean id="webscanDataSource" class="bitronix.tm.resource.jdbc.PoolingDataSource" init-method="init" destroy-method="close"> 
		<property name="className" value="${ds.jdbc.className}" /> 
		<property name="uniqueName" value="${ds.uniqueResourceName}" /> 
		<property name="minPoolSize" value="${ds.minPoolSize}"/>
		<property name="maxPoolSize" value="${ds.maxPoolSize}"/>
		<property name="maxIdleTime" value="${ds.maxIdleTime}"/>
		<property name="acquireIncrement" value="${ds.acquireIncrement}"/>
		<property name="acquisitionTimeout" value="${ds.acquisitionTimeout}"/>
		<property name="preparedStatementCacheSize" value="${ds.preparedStatementCacheSize}"/>
		<property name="testQuery" value="${ds.testQuery}"/>
		<property name="allowLocalTransactions" value="${ds.allowLocalTransactions}"/>
		<property name="driverProperties">
			<props>
				<prop key="URL">${ds.jdbc.url}</prop> 
				<prop key="user">${ds.jdbc.username}</prop> 
				<prop key="password">${ds.jdbc.password}</prop> 
			</props>
		</property>       
	</bean>

	<!-- Fin Definición del Data Source -->
	
	<!-- Configuración de EntityManagerFactory -->
	<!-- <bean class="org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor" /> -->

	<bean id="modelEntityManagerFactoryBean" name="modelEntityManagerFactoryBean" class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
      	<property name="dataSource" ref="webscanDataSource" />
      	<!-- This makes /META-INF/persistence.xml is no longer necessary -->
      	<property name="packagesToScan">
	 		<list>
	   			<value>es.ricoh.webscan.model.domain</value> 
	   		</list>
		</property>
		<property name="persistenceUnitName" value="webscan-model-emf"/>
      	<!-- JpaVendorAdapter implementation for Hibernate EntityManager.
           Exposes Hibernate's persistence provider and EntityManager extension interface -->
      	<property name="jpaVendorAdapter">
         	<bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter" />
      	</property>
      	<property name="jpaProperties">
			<props>
	            <prop key="hibernate.dialect">${hibernate.dialect}</prop>
	   			<prop key="hibernate.show_sql">${hibernate.show_sql}</prop>
				<prop key="hibernate.format_sql">${hibernate.format_sql}</prop>
				
				<prop key="hibernate.connection.autocommit">${hibernate.connection.autocommit}</prop>	   			
	   			<prop key="hibernate.connection.release_mode">${hibernate.connection.release_mode}</prop>
	       		<prop key="hibernate.current_session_context_class">${hibernate.current_session_context_class}</prop>
	   			
	   			<prop key="hibernate.jdbc.batch_size">${hibernate.jdbc.batch_size}</prop>
				<prop key="hibernate.jdbc.use_streams_for_binary">${hibernate.jdbc.use_streams_for_binary}</prop>
				<prop key="hibernate.query.substitutions">${hibernate.query.substitutions}</prop>
	   			<prop key="hibernate.id.new_generator_mappings">${hibernate.id.new_generator_mappings}</prop>
	   			<!-- Transaction management -->	   			
	   			<prop key="hibernate.transaction.coordinator_class">${hibernate.transaction.coordinator_class}</prop> 
	   			<prop key="hibernate.transaction.jta.platform">${hibernate.transaction.jta.platform}</prop>
        	</props>
		</property>
   	</bean>

	<!-- Fin Configuración de EntityManagerFactory -->

	<!-- Capas de servicios y acceso a datos -->
	
	<bean id="auditDAO" class="es.ricoh.webscan.model.dao.impl.AuditDAOJdbcImpl" />
	
	<bean id="documentDAO" class="es.ricoh.webscan.model.dao.impl.DocumentDAOJdbcImpl" />
	
	<bean id="batchDAO" class="es.ricoh.webscan.model.dao.impl.BatchDAOJdbcImpl" />

	<bean id="orgUnitDAO" class="es.ricoh.webscan.model.dao.impl.OrganizationUnitDAOJdbcImpl" />
	
	<bean id="pluginDAO" class="es.ricoh.webscan.model.dao.impl.PluginDAOJdbcImpl" />
	
	<bean id="scanProfileDAO" class="es.ricoh.webscan.model.dao.impl.ScanProfileDAOJdbcImpl" />
	
	<bean id="scanProcessStageDAO" class="es.ricoh.webscan.model.dao.impl.ScanProcessStageDAOJdbcImpl" />
	
	<bean id="webscanModelManager" class="es.ricoh.webscan.model.WebscanModelManager">
		<property name="pluginDAO" ref="pluginDAO" />
		<property name="scanProcessStageDAO" ref="scanProcessStageDAO" />
		<property name="scanProfileDAO" ref="scanProfileDAO" />
		<property name="documentDAO" ref="documentDAO" />
		<property name="batchDAO" ref="batchDAO" />
		<property name="orgUnitDAO" ref="orgUnitDAO" />
		<property name="auditDAO" ref="auditDAO" />
		<property name="webscanUtilParams" ref="webscanUtilParams" />
	</bean>

	<!-- Registro y actualización trazas de auditoría -->
	<bean id="auditService" name="auditService" class="es.ricoh.webscan.model.service.AuditService">
		<property name="webscanModelManager" ref="webscanModelManager"/>
	</bean>
	<!-- Fin registro y actualización trazas de auditoría -->
	
	<!-- Fin capas de servicios y acceso a datos -->
	
	<!-- Configuración del gestor de transacciones JTA -->

	<!--  Bitronix Transaction Manager, configuración en archivo bitronix-default-config.properties -->
	<bean id="btmConfig" factory-method="getConfiguration" class="bitronix.tm.TransactionManagerServices" />

	<!-- BTM transaction manager -->
	<bean id="btmTransactionManager" factory-method="getTransactionManager"
	    class="bitronix.tm.TransactionManagerServices" depends-on="btmConfig" destroy-method="shutdown" />

	<!-- JTA Transaction Manager -->	
	<bean id="webscanTransactionManager" class="org.springframework.transaction.jta.JtaTransactionManager">
    	<property name="transactionManager" ref="btmTransactionManager" />
    	<property name="userTransaction" ref="btmTransactionManager" />
    	<property name="allowCustomIsolationLevels" value="true" />
    	<property name="defaultTimeout" value="${bitronix.tm.timer.defaultTransactionTimeout}" />
	</bean>
	
	<!-- Fin Configuración del gestor de transacciones JTA -->
	
	<!-- Configuración transacciones -->
	
	<aop:config>
		<aop:pointcut expression="execution(public * *..WebscanModelManager.*(..))" id="webscanModelPointcut"/>
		<aop:advisor advice-ref="webscanTxAdvice" pointcut-ref="webscanModelPointcut"/>
	</aop:config>

	<tx:advice id="webscanTxAdvice" transaction-manager="webscanTransactionManager" >
		<tx:attributes>
			<!-- El timeout se corresponde con el establecido por defecto en el JTA manager -->
			<!-- Consultas -->
			<tx:method name="count*" read-only="true" propagation="REQUIRED" isolation="READ_COMMITTED" no-rollback-for="es.ricoh.webscan.model.DAOException"/>
			<tx:method name="get*" read-only="true" propagation="REQUIRED" isolation="READ_COMMITTED" no-rollback-for="es.ricoh.webscan.model.DAOException"/>
			<tx:method name="has*" read-only="true" propagation="REQUIRED" isolation="READ_COMMITTED" no-rollback-for="es.ricoh.webscan.model.DAOException"/>
			<tx:method name="list*" read-only="true" propagation="REQUIRED" isolation="READ_COMMITTED" no-rollback-for="es.ricoh.webscan.model.DAOException"/>
			<!-- Inserciones, borrados y actualizaciones en capas de servicios -->			
		</tx:attributes>
	</tx:advice>
	
	<!-- Fin Configuración transacciones -->
</beans>
