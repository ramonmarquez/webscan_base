/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.DocumentSpecification.java.</p>
 * <b>Descripción:</b><p> Entidad JPA definición de documento.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA definición de documento.
 * <p>
 * Clase DocumentSpecification.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "DOC_SPEC", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME" }) })
public class DocumentSpecification implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "DOC_SPEC_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "DOC_SPEC_ID_SEQ", sequenceName = "DOC_SPEC_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Atributo DESCRIPTION de la entidad.
	 */
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * Atributo ACTIVE de la entidad.
	 */
	@Column(name = "ACTIVE")
	private Boolean active = Boolean.TRUE;

	/**
	 * Relación de entidades especificación de metadato de la definición de
	 * documento.
	 */
	@OneToMany(mappedBy = "docSpecification", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.MetadataSpecification.class)
	private List<MetadataSpecification> metadataSpecCollection = new ArrayList<MetadataSpecification>();

	/**
	 * Relación de entidades especificación de documento que heredan de la
	 * definición de documento.
	 */
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.REMOVE })
	@JoinTable(name = "INHERITED_DOC_TYPE_SPEC", joinColumns = @JoinColumn(name = "PARENT_DOC_TYPE_SPEC", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "CHILD_DOC_TYPE_SPEC", referencedColumnName = "ID"))
	private List<DocumentSpecification> childDocs = new ArrayList<DocumentSpecification>();

	/**
	 * Relación de entidades especificación de documento de las que hereda de la
	 * definición de documento.
	 */
	@ManyToMany(mappedBy = "childDocs", fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.DocumentSpecification.class)
	private List<DocumentSpecification> parentDocs = new ArrayList<DocumentSpecification>();

	/**
	 * Relación de entidades configuración de definción de documento en perfil y
	 * unidad organizativa en las que se encuentra la definición de documento.
	 */
	@OneToMany(mappedBy = "docSpecification", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnitDocSpec.class)
	private List<ScanProfileOrgUnitDocSpec> orgScanProfiles = new ArrayList<ScanProfileOrgUnitDocSpec>();

	/**
	 * Constructor sin argumentos.
	 */
	public DocumentSpecification() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo DESCRIPTION de la entidad.
	 * 
	 * @return el atributo DESCRIPTION de la entidad.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para el atributo DESCRIPTION de la entidad.
	 * 
	 * @param aDescription
	 *            nuevo valor para el atributo DESCRIPTION de la entidad.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene el atributo ACTIVE de la entidad.
	 * 
	 * @return el atributo ACTIVE de la entidad.
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Establece un nuevo valor para el atributo ACTIVE de la entidad.
	 * 
	 * @param activeParam
	 *            nuevo valor para el atributo ACTIVE de la entidad.
	 */
	public void setActive(Boolean activeParam) {
		this.active = activeParam;
	}

	/**
	 * Obtiene la relación de entidades especificación de metadato de la
	 * definición de documento.
	 * 
	 * @return la relación de entidades especificación de metadato de la
	 *         definición de documento.
	 */
	public List<MetadataSpecification> getMetadataSpecCollection() {
		return metadataSpecCollection;
	}

	/**
	 * Establece una nueva relación de entidades especificación de metadato de
	 * la definición de documento.
	 * 
	 * @param aMetadataSpecCollection
	 *            Nueva relación de entidades especificación de metadato de la
	 *            definición de documento.
	 */
	public void setMetadataSpecCollection(List<MetadataSpecification> aMetadataSpecCollection) {
		this.metadataSpecCollection.clear();
		if (aMetadataSpecCollection != null && !aMetadataSpecCollection.isEmpty()) {
			this.metadataSpecCollection.addAll(aMetadataSpecCollection);
		}
	}

	/**
	 * Obtiene la relación de entidades especificación de documento que heredan
	 * de la definición de documento.
	 * 
	 * @return la relación de entidades especificación de documento que heredan
	 *         de la definición de documento.
	 */
	public List<DocumentSpecification> getChildDocs() {
		return childDocs;
	}

	/**
	 * Establece una nueva relación de entidades especificación de documento que
	 * heredan de la definición de documento.
	 * 
	 * @param anyChildDocs
	 *            Nueva relación de entidades especificación de documento que
	 *            heredan de la definición de documento.
	 */
	public void setChildDocs(List<DocumentSpecification> anyChildDocs) {
		this.childDocs.clear();
		if (anyChildDocs != null && !anyChildDocs.isEmpty()) {
			this.childDocs.addAll(anyChildDocs);
		}
	}

	/**
	 * Obtiene la relación de entidades especificación de documento de las que
	 * hereda la definición de documento.
	 * 
	 * @return la relación de entidades especificación de documento de las que
	 *         hereda la definición de documento.
	 */
	public List<DocumentSpecification> getParentDocs() {
		return parentDocs;
	}

	/**
	 * Establece una nueva relación de entidades especificación de documento de
	 * las que hereda la definición de documento.
	 * 
	 * @param anyParentDocs
	 *            Nueva relación de entidades especificación de documento de las
	 *            que hereda la definición de documento.
	 */
	public void setParentDocs(List<DocumentSpecification> anyParentDocs) {
		this.parentDocs.clear();
		if (anyParentDocs != null && !anyParentDocs.isEmpty()) {
			this.parentDocs.addAll(anyParentDocs);
		}
	}

	/**
	 * Obtiene la relación de entidades configuración de definción de documento
	 * en perfil y unidad organizativa en las que se encuentra la definición de
	 * documento.
	 * 
	 * @return la relación de entidades configuración de definción de documento
	 *         en perfil y unidad organizativa en las que se encuentra la
	 *         definición de documento.
	 */
	public List<ScanProfileOrgUnitDocSpec> getOrgScanProfiles() {
		return orgScanProfiles;
	}

	/**
	 * Establece una nueva relación de entidades configuración de definción de
	 * documento en perfil y unidad organizativa en las que se encuentra la
	 * definición de documento.
	 * 
	 * @param anyOrgScanProfiles
	 *            Nueva relación de entidades configuración de definción de
	 *            documento en perfil y unidad organizativa en las que se
	 *            encuentra la definición de documento.
	 */
	public void setOrgScanProfiles(List<ScanProfileOrgUnitDocSpec> anyOrgScanProfiles) {
		this.orgScanProfiles.clear();
		if (anyOrgScanProfiles != null && !anyOrgScanProfiles.isEmpty()) {
			this.orgScanProfiles.addAll(anyOrgScanProfiles);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentSpecification other = (DocumentSpecification) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
