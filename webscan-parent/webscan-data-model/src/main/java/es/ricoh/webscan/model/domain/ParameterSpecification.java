/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ParameterSpecification.java.</p>
 * <b>Descripción:</b><p> Entidad JPA definición de parámetro.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA definición de parámetro.
 * <p>
 * Clase ParameterSpecification.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "PARAM_SPEC", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME", "PLUGIN_SPEC_ID" }) })
public class ParameterSpecification implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "PARAM_SPEC_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "PARAM_SPEC_ID_SEQ", sequenceName = "PARAM_SPEC_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Atributo DESCRIPTION de la entidad.
	 */
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * Atributo MANDATORY de la entidad.
	 */
	@Column(name = "MANDATORY")
	private Boolean mandatory = Boolean.FALSE;

	/**
	 * Entidad que especifica el tipo del parámetro.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ParameterType.class)
	@JoinColumn(name = "DATA_TYPE_ID")
	private ParameterType type;

	/**
	 * Entidad especificación de plugin a la que pertenece la definición de
	 * parámetro.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.PluginSpecification.class)
	@JoinColumn(name = "PLUGIN_SPEC_ID")
	private PluginSpecification pluginSpecification;

	/**
	 * Relación de parámetros de plugins especificados mediante esta definición
	 * de parámetro.
	 */
	@OneToMany(mappedBy = "specification", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.Parameter.class)
	private List<Parameter> parameters = new ArrayList<Parameter>();

	/**
	 * Constructor sin argumentos.
	 */
	public ParameterSpecification() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo DESCRIPTION de la entidad.
	 * 
	 * @return el atributo DESCRIPTION de la entidad.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para el atributo DESCRIPTION de la entidad.
	 * 
	 * @param aDescription
	 *            nuevo valor para el atributo DESCRIPTION de la entidad.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene el atributo MANDATORY de la entidad.
	 * 
	 * @return el atributo MANDATORY de la entidad.
	 */
	public Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * Establece un nuevo valor para el atributo MANDATORY de la entidad.
	 * 
	 * @param mandatoryParam
	 *            nuevo valor para el atributo MANDATORY de la entidad.
	 */
	public void setMandatory(Boolean mandatoryParam) {
		this.mandatory = mandatoryParam;
	}

	/**
	 * Obtiene el tipo del parámetro.
	 * 
	 * @return el tipo del parámetro.
	 */
	public ParameterType getType() {
		return type;
	}

	/**
	 * Establece un nuevo tipo de parámetro.
	 * 
	 * @param aType
	 *            Nuevo tipo de parámetro.
	 */
	public void setType(ParameterType aType) {
		this.type = aType;
	}

	/**
	 * Obtiene la especificación de plugin a la que pertenece la definición de
	 * parámetro.
	 * 
	 * @return la especificación de plugin a la que pertenece la definición de
	 *         parámetro.
	 */
	public PluginSpecification getPluginSpecification() {
		return pluginSpecification;
	}

	/**
	 * Establece una nueva especificación de plugin a la que pertenece la
	 * definición de parámetro.
	 * 
	 * @param aPluginSpecification
	 *            Nueva especificación de plugin a la que pertenece la
	 *            definición de parámetro.
	 */
	public void setPluginSpecification(PluginSpecification aPluginSpecification) {
		this.pluginSpecification = aPluginSpecification;
	}

	/**
	 * Obtiene la relación de parámetros de plugins especificados mediante esta
	 * definición de parámetro.
	 * 
	 * @return la relación de parámetros de plugins especificados mediante esta
	 *         definición de parámetro.
	 */
	public List<Parameter> getParameters() {
		return parameters;
	}

	/**
	 * Establece una nueva relación de parámetros de plugins especificados
	 * mediante esta definición de parámetro.
	 * 
	 * @param anyParameters
	 *            Nueva relación de parámetros de plugins especificados mediante
	 *            esta definición de parámetro.
	 */
	public void setParameters(List<Parameter> anyParameters) {
		this.parameters.clear();
		if (anyParameters != null && !anyParameters.isEmpty()) {
			this.parameters.addAll(anyParameters);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pluginSpecification == null) ? 0 : pluginSpecification.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParameterSpecification other = (ParameterSpecification) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pluginSpecification == null) {
			if (other.pluginSpecification != null)
				return false;
		} else if (!pluginSpecification.equals(other.pluginSpecification))
			return false;
		return true;
	}

}
