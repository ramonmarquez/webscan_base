/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScanProfileOrgUnitDocSpec.java.</p>
 * <b>Descripción:</b><p> Entidad JPA configuración de especificación de documento en perfil de digitalización y unidad organizativa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA configuración de especificación de documento en perfil de
 * digitalización y unidad organizativa.
 * <p>
 * Clase ScanProfileOrgUnitDocSpec.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "SPROF_ORG_DOC_SPEC", uniqueConstraints = { @UniqueConstraint(columnNames = { "SPROF_ORG_ID", "DOC_SPEC_ID" }) })
public class ScanProfileOrgUnitDocSpec implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "SPROF_ORG_DSPEC_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SPROF_ORG_DSPEC_ID_SEQ", sequenceName = "SPROF_ORG_DSPEC_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo ACTIVE de la entidad.
	 */
	@Column(name = "ACTIVE")
	private Boolean active = Boolean.TRUE;

	/**
	 * Definición de documentos configurada.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.DocumentSpecification.class)
	@JoinColumn(name = "DOC_SPEC_ID")
	private DocumentSpecification docSpecification;

	/**
	 * Configuración de perfil de digitalización en unidad organizativa.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnit.class)
	@JoinColumn(name = "SPROF_ORG_ID")
	private ScanProfileOrgUnit orgScanProfile;

	/**
	 * Relación de documentos digitalizados para la definición de documentos
	 * establecida en la configuración de perfil de digitalización en unidad
	 * organizativa.
	 */
	@OneToMany(mappedBy = "scanProfileOrgUnitDocSpec", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScannedDocument.class)
	private List<ScannedDocument> documents = new ArrayList<ScannedDocument>();

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfileOrgUnitDocSpec() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo ACTIVE de la entidad.
	 * 
	 * @return el atributo ACTIVE de la entidad.
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Establece un nuevo valor para el atributo ACTIVE de la entidad.
	 * 
	 * @param activeParam
	 *            nuevo valor para el atributo ACTIVE de la entidad.
	 */
	public void setActive(Boolean activeParam) {
		this.active = activeParam;
	}

	/**
	 * Obtiene la entidad definición de documentos configurada.
	 * 
	 * @return la entidad definición de documentos configurada.
	 */
	public DocumentSpecification getDocSpecification() {
		return docSpecification;
	}

	/**
	 * Establece una nueva entidad definición de documentos.
	 * 
	 * @param aDocSpecification
	 *            Nueva entidad definición de documentos.
	 */
	public void setDocSpecification(DocumentSpecification aDocSpecification) {
		this.docSpecification = aDocSpecification;
	}

	/**
	 * Obtiene la entidad configuración de perfil de digitalización en unidad
	 * organizativa.
	 * 
	 * @return la entidad configuración de perfil de digitalización en unidad
	 *         organizativa.
	 */
	public ScanProfileOrgUnit getOrgScanProfile() {
		return orgScanProfile;
	}

	/**
	 * Establece una nueva entidad configuración de perfil de digitalización en
	 * unidad organizativa.
	 * 
	 * @param anOrgScanProfile
	 *            Nueva entidad configuración de perfil de digitalización en
	 *            unidad organizativa.
	 */
	public void setOrgScanProfile(ScanProfileOrgUnit anOrgScanProfile) {
		this.orgScanProfile = anOrgScanProfile;
	}

	/**
	 * Obtiene la relación de documentos digitalizados para la definición de
	 * documentos establecida en la configuración de perfil de digitalización en
	 * unidad organizativa.
	 * 
	 * @return la relación de documentos digitalizados para la definición de
	 *         documentos establecida en la configuración de perfil de
	 *         digitalización en unidad organizativa.
	 */
	public List<ScannedDocument> getDocuments() {
		return documents;
	}

	/**
	 * Establece una nueva relación de documentos digitalizados para la
	 * definición de documentos establecida en la configuración de perfil de
	 * digitalización en unidad organizativa.
	 * 
	 * @param anyDocuments
	 *            Nueva relación de documentos digitalizados para la definición
	 *            de documentos establecida en la configuración de perfil de
	 *            digitalización en unidad organizativa.
	 */
	public void setDocuments(List<ScannedDocument> anyDocuments) {
		this.documents.clear();
		if (anyDocuments != null && !anyDocuments.isEmpty()) {
			this.documents.addAll(anyDocuments);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfileOrgUnitDocSpec other = (ScanProfileOrgUnitDocSpec) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
