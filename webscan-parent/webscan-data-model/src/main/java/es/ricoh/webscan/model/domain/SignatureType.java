/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.SignatureType.java.</p>
 * <b>Descripción:</b><p> Clase que define los formatos de las firmas electrónicas realizadas sobre los documentos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

/**
 * Clase que define los formatos de las firmas electrónicas realizadas sobre los
 * documentos.
 * <p>
 * Clase SignatureType.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum SignatureType {

	/**
	 * Formato CSV.
	 */
	CSV(Integer.valueOf(0), "CSV", "CSV"),
	/**
	 * Formato CAdES-A.
	 */
	CAdES_A(Integer.valueOf(1), "CAdES-A", "CAdES"),
	/**
	 * Formato CAdES-XL.
	 */
	CAdES_XL(Integer.valueOf(2), "CAdES-XL", "CAdES"),
	/**
	 * Formato CAdES-XL1.
	 */
	CAdES_XL1(Integer.valueOf(3), "CAdES-XL1", "CAdES"),
	/**
	 * Formato CAdES-XL2.
	 */
	CAdES_XL2(Integer.valueOf(4), "CAdES-XL2", "CAdES"),
	/**
	 * Formato CAdES-X.
	 */
	CAdES_X(Integer.valueOf(5), "CAdES-X", "CAdES"),
	/**
	 * Formato CAdES-X1.
	 */
	CAdES_X1(Integer.valueOf(6), "CAdES-X1", "CAdES"),
	/**
	 * Formato CAdES-X2.
	 */
	CAdES_X2(Integer.valueOf(7), "CAdES-X2", "CAdES"),
	/**
	 * Formato CAdES-C.
	 */
	CAdES_C(Integer.valueOf(8), "CAdES-C", "CAdES"),
	/**
	 * Formato CAdES-T.
	 */
	CAdES_T(Integer.valueOf(9), "CAdES-T", "CAdES"),
	/**
	 * Formato CAdES-EPES.
	 */
	CAdES_EPES(Integer.valueOf(10), "CAdES-EPES", "CAdES"),
	/**
	 * Formato CAdES-BES.
	 */
	CAdES_BES(Integer.valueOf(11), "CAdES-BES", "CAdES"),
	/**
	 * Formato CAdES.
	 */
	CAdES(Integer.valueOf(12), "CAdES", "CAdES"),
	/**
	 * Formato PAdES-LTV.
	 */
	PAdES_LTV(Integer.valueOf(13), "PAdES-LTV", "PAdES"),
	/**
	 * Formato PAdES-EPES.
	 */
	PAdES_EPES(Integer.valueOf(14), "PAdES-EPES", "PAdES"),
	/**
	 * Formato PAdES-BES.
	 */
	PAdES_BES(Integer.valueOf(15), "PAdES-BES", "PAdES"),
	/**
	 * Formato PAdES.
	 */
	PAdES(Integer.valueOf(16), "PAdES", "PAdES"),
	/**
	 * Formato XAdES-A.
	 */
	XAdES_A(Integer.valueOf(17), "XAdES-A", "XAdES"),
	/**
	 * Formato XAdES-XL.
	 */
	XAdES_XL(Integer.valueOf(18), "XAdES-XL", "XAdES"),
	/**
	 * Formato XAdES-XL1.
	 */
	XAdES_XL1(Integer.valueOf(19), "XAdES-XL1", "XAdES"),
	/**
	 * Formato CAdES-XL2.
	 */
	XAdES_XL2(Integer.valueOf(20), "XAdES-XL2", "XAdES"),
	/**
	 * Formato XAdES-X.
	 */
	XAdES_X(Integer.valueOf(21), "XAdES-X", "XAdES"),
	/**
	 * Formato XAdES-X1.
	 */
	XAdES_X1(Integer.valueOf(22), "XAdES-X1", "XAdES"),
	/**
	 * Formato XAdES-X2.
	 */
	XAdES_X2(Integer.valueOf(23), "XAdES-X2", "XAdES"),
	/**
	 * Formato XAdES-C.
	 */
	XAdES_C(Integer.valueOf(24), "XAdES-C", "XAdES"),
	/**
	 * Formato XAdES-T.
	 */
	XAdES_T(Integer.valueOf(25), "XAdES-T", "XAdES"),
	/**
	 * Formato XAdES-EPES.
	 */
	XAdES_EPES(Integer.valueOf(26), "XAdES-EPES", "XAdES"),
	/**
	 * Formato XAdES-BES.
	 */
	XAdES_BES(Integer.valueOf(27), "XAdES-BES", "XAdES"),
	/**
	 * Formato XAdES.
	 */
	XAdES(Integer.valueOf(28), "XAdES", "XAdES");

	/**
	 * Identificador de formato de firma.
	 */
	private Integer signatureId;

	/**
	 * Denominación de formato de firma.
	 */
	private String signatureType;

	/**
	 * Familia de formatos de firma.
	 */
	private String family;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aSignatureId
	 *            Identificador de formato de firma.
	 * @param aSignatureType
	 *            Denominación de formato de firma.
	 * @param aFamily
	 *            Familia de formatos de firma.
	 */
	SignatureType(Integer aSignatureId, String aSignatureType, String aFamily) {
		this.signatureId = aSignatureId;
		this.signatureType = aSignatureType;
		this.family = aFamily;
	}

	/**
	 * Obtiene el identificador de formato de firma.
	 * 
	 * @return el identificador de formato de firma.
	 */
	public Integer getSignatureId() {
		return signatureId;
	}

	/**
	 * Obtiene la denominación de formato de firma.
	 * 
	 * @return la denominación de formato de firma.
	 */
	public String getSignatureType() {
		return signatureType;
	}

	/**
	 * Obtiene la familia de formatos de firma.
	 * 
	 * @return la familia de formatos de firma.
	 */
	public String getFamily() {
		return family;
	}

	/**
	 * Recupera un formato de firma a partir de su identificador.
	 * 
	 * @param id
	 *            Identificador de formato de firma.
	 * @return El formato de firma correspondiente al identificador especificado
	 *         como parámetro de entrada. En caso de no encontrarse, devuelve
	 *         null.
	 */
	public static SignatureType getById(Integer id) {

		for (SignatureType e: SignatureType.values()) {
			if (e.getSignatureId().equals(id)) {
				return e;
			}
		}

		return null;
	}
}
