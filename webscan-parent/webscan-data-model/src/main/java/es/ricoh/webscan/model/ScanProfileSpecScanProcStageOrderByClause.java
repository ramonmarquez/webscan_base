/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.ScanProfileSpecScanProcStageOrderByClause.java.</p>
 * <b>Descripción:</b><p> Clase que representa la clausula ORDER BY sobre la entidad ScanProfileSpecScanProcStage.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model;

/**
 * Clase que representa la clausula ORDER BY sobre la entidad
 * ScanProfileSpecScanProcStage.
 * <p>
 * Clase ScanProfileSpecScanProcStageOrderByClause.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.EntityOrderByClause
 * @version 2.0.
 */
public enum ScanProfileSpecScanProcStageOrderByClause implements EntityOrderByClause {

	/**
	 * Ordenación ascendente sobre el atributo order.
	 */
	ORDER_ASC("order", OrderByClause.ASC),
	/**
	 * Ordenación descendente sobre el atributo order.
	 */
	ORDER_DESC("order", OrderByClause.DESC);

	/**
	 * Nombre de un atributo de la entidad AuditOperation.
	 */
	private String attName;

	/**
	 * Tipo de ordenación.
	 */
	private OrderByClause orderByClause;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param anAttName
	 *            Nombre de un atributo de la entidad AuditOperation.
	 * @param anOrderByClause
	 *            Tipo de ordenación.
	 */
	ScanProfileSpecScanProcStageOrderByClause(String anAttName, OrderByClause anOrderByClause) {
		this.attName = anAttName;
		this.orderByClause = anOrderByClause;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.EntityOrderByClause#getAttName()
	 */
	public String getAttName() {
		return attName;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.EntityOrderByClause#getOrderByClause()
	 */
	public OrderByClause getOrderByClause() {
		return orderByClause;
	}

}
