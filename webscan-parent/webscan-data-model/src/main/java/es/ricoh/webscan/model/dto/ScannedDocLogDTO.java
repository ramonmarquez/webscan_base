/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ScannedDocLogDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA traza o fase de ejecución asociada al proceso de digitalización
 * de un documento.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.model.domain.OperationStatus;

/**
 * DTO para la entidad JPA traza o fase de ejecución asociada al proceso de
 * digitalización de un documento.
 * <p>
 * Clase ScannedDocLogDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class ScannedDocLogDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Estado de ejecución de la fase de digitalización para un documento.
	 */
	private OperationStatus status = OperationStatus.IN_PROCESS;

	/**
	 * Instante en el que se inicio una fase del proceso de digitalización para
	 * un documento.
	 */
	private Date startDate;

	/**
	 * Instante en el que finalizó una fase del proceso de digitalización para
	 * un documento.
	 */
	private Date endDate;

	/**
	 * Identificador de documento.
	 */
	private Long scannedDocId;

	/**
	 * Identificador de fase de proceso de digitalización.
	 */
	private Long scanProcessStageId;

	/**
	 * Denominación de la fase de proceso de digitalización.
	 */
	private String scanProcessStageName;

	/**
	 * Constructor sin argumentos.
	 */
	public ScannedDocLogDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el estado de ejecución de la fase de digitalización para un
	 * documento.
	 * 
	 * @return el estado de ejecución de la fase de digitalización para un
	 *         documento.
	 */
	public OperationStatus getStatus() {
		return status;
	}

	/**
	 * Establece el estado de ejecución de la fase de digitalización para un
	 * documento.
	 * 
	 * @param aStatus
	 *            estado de ejecución de la fase de digitalización para un
	 *            documento.
	 */
	public void setStatus(OperationStatus aStatus) {
		this.status = aStatus;
	}

	/**
	 * Obtiene el instante en el que se inicio una fase del proceso de
	 * digitalización para un documento.
	 * 
	 * @return el instante en el que se inicio una fase del proceso de
	 *         digitalización para un documento.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Establece un nuevo instante en el que se inicio una fase del proceso de
	 * digitalización para un documento.
	 * 
	 * @param aStartDate
	 *            nuevo instante en el que se inicio una fase del proceso de
	 *            digitalización para un documento.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}

	/**
	 * Obtiene el instante en el que se finalizó una fase del proceso de
	 * digitalización para un documento.
	 * 
	 * @return el instante en el que se finalizó una fase del proceso de
	 *         digitalización para un documento.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Establece un nuevo instante en el que se finalizó una fase del proceso de
	 * digitalización para un documento.
	 * 
	 * @param anEndDate
	 *            nuevo instante en el que se finalizó una fase del proceso de
	 *            digitalización para un documento.
	 */
	public void setEndDate(Date anEndDate) {
		this.endDate = anEndDate;
	}

	/**
	 * Obtiene el identificador de documento.
	 * 
	 * @return el identificador de documento.
	 */
	public Long getScannedDocId() {
		return scannedDocId;
	}

	/**
	 * Establece un nuevo identificador de documento.
	 * 
	 * @param aScannedDocId
	 *            nuevo identificador de documento.
	 */
	void setScannedDocId(Long aScannedDocId) {
		this.scannedDocId = aScannedDocId;
	}

	/**
	 * Obtiene el identificador de fase de proceso de digitalización.
	 * 
	 * @return el identificador de fase de proceso de digitalización.
	 */
	public Long getScanProcessStageId() {
		return scanProcessStageId;
	}

	/**
	 * Establece un nuevo identificador de fase de proceso de digitalización.
	 * 
	 * @param aScanProcessStageId
	 *            nuevo identificador de fase de proceso de digitalización.
	 */
	void setScanProcessStageId(Long aScanProcessStageId) {
		this.scanProcessStageId = aScanProcessStageId;
	}

	/**
	 * Obtiene la denominación de fase de proceso de digitalización.
	 * 
	 * @return la denominación de fase de proceso de digitalización.
	 */
	public String getScanProcessStageName() {
		return scanProcessStageName;
	}

	/**
	 * Establece una nueva denominación de fase de proceso de digitalización.
	 * 
	 * @param aScanProcessStageName
	 *            nueva denominación de fase de proceso de digitalización.
	 */
	public void setScanProcessStageName(String aScanProcessStageName) {
		this.scanProcessStageName = aScanProcessStageName;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((scanProcessStageId == null) ? 0 : scanProcessStageId.hashCode());
		result = prime * result + ((scannedDocId == null) ? 0 : scannedDocId.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScannedDocLogDTO other = (ScannedDocLogDTO) obj;
		if (scanProcessStageId == null) {
			if (other.scanProcessStageId != null)
				return false;
		} else if (!scanProcessStageId.equals(other.scanProcessStageId))
			return false;
		if (scannedDocId == null) {
			if (other.scannedDocId != null)
				return false;
		} else if (!scannedDocId.equals(other.scannedDocId))
			return false;
		return true;
	}

}
