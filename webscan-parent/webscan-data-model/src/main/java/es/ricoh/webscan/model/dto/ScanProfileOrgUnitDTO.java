/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA configuración de perfil de digitalización para unidad organizativa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA configuración de perfil de digitalización para unidad
 * organizativa.
 * <p>
 * Clase ScanProfileOrgUnitDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class ScanProfileOrgUnitDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Identificador de perfil de digitalización.
	 */
	private Long scanProfile;

	/**
	 * Identificador de unidad organizativa.
	 */
	private Long orgUnit;

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfileOrgUnitDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el identificador de perfil de digitalización.
	 * 
	 * @return el identificador de perfil de digitalización.
	 */
	public Long getScanProfile() {
		return scanProfile;
	}

	/**
	 * Establece un nuevo identificador de perfil de digitalización.
	 * 
	 * @param aScanProfile
	 *            nuevo identificador de perfil de digitalización.
	 */
	public void setScanProfile(Long aScanProfile) {
		this.scanProfile = aScanProfile;
	}

	/**
	 * Obtiene el identificador de unidad organizativa.
	 * 
	 * @return el identificador de unidad organizativa.
	 */
	public Long getOrgUnit() {
		return orgUnit;
	}

	/**
	 * Establece un nuevo identificador de unidad organizativa.
	 * 
	 * @param anOrgUnit
	 *            nuevo identificador de unidad organizativa.
	 */
	public void setOrgUnit(Long anOrgUnit) {
		this.orgUnit = anOrgUnit;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orgUnit == null) ? 0 : orgUnit.hashCode());
		result = prime * result + ((scanProfile == null) ? 0 : scanProfile.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfileOrgUnitDTO other = (ScanProfileOrgUnitDTO) obj;
		if (orgUnit == null) {
			if (other.orgUnit != null)
				return false;
		} else if (!orgUnit.equals(other.orgUnit))
			return false;
		if (scanProfile == null) {
			if (other.scanProfile != null)
				return false;
		} else if (!scanProfile.equals(other.scanProfile))
			return false;
		return true;
	}

}
