/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.OrganizationUnitDAO.java.</p>
 * <b>Descripción:</b><p> Interfaz que define la operaciones de la capa DAO sobre la entidad unidad organizativa, perteneciente al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao;

import java.util.List;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitTreeDTO;

/**
 * Interfaz que define la operaciones de la capa DAO sobre la entidad unidad
 * organizativa, perteneciente al modelo de datos base de Webscan.
 * <p>
 * Clase OrganizationUnitDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface OrganizationUnitDAO {

	/**
	 * Recupera el conjunto global de unidades organizativas registradas en el
	 * sistema.
	 * 
	 * @return Conjunto global de unidades organizativas registradas en el
	 *         sistema.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	List<OrganizationUnitDTO> getRootOrganizationUnits() throws DAOException;

	/**
	 * Recupera el conjunto de unidades organizativas hija de una unidad
	 * organizativa registradas en el sistema.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa
	 * @return Conjunto global de unidades organizativas registradas en el
	 *         sistema.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             unidad organizativa, o se produce algún error en la consulta
	 *             a BBDD.
	 */
	List<OrganizationUnitDTO> getChildOrganizationUnits(Long orgUnitId) throws DAOException;

	/**
	 * Recupera una unidad organizativa a partir de su identificador.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return La unidad organizativa solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no se encuentra
	 *             la unidad organizativa, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	OrganizationUnitDTO getOrganizationUnit(Long orgUnitId) throws DAOException;

	/**
	 * Recupera una unidad organizativa a partir de su identificador externo.
	 * 
	 * @param externalId
	 *            Identificador externo.
	 * @return La unidad organizativa solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no se encuentra
	 *             la unidad organizativa, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	OrganizationUnitDTO getOrgUnitByExternalId(String externalId) throws DAOException;

	/**
	 * Recupera el conjunto global de unidades organizativas registradas en el
	 * sistema, retornando para cada una de ellas además el listado de unidades
	 * organizativas hija. Pueden existir varias unidades organizativas raíz.
	 * 
	 * @return Conjunto global de unidades organizativas registradas en el
	 *         sistema.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	List<OrganizationUnitTreeDTO> getOrganizationUnitsTree() throws DAOException;

	/**
	 * Crea un nueva unidad organizativa en el sistema.
	 * 
	 * @param orgUnit
	 *            Nueva unidad organizativa.
	 * @return El identificador de la unidad organizativa creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createOrganizationUnit(OrganizationUnitDTO orgUnit) throws DAOException;

	/**
	 * Actualiza una unidad organizativa registrado en el sistema. Los datos que
	 * pueden ser actualizados son la denominación, el identificador externo y
	 * la unidad organizativa padre.
	 * 
	 * @param orgUnit
	 *            Unidad organizativa.
	 * @throws DAOException
	 *             Si no existe la unidad organizativa, o se produce algún error
	 *             en la ejecución de la sentencia de BBDD.
	 */
	void updateOrganizationUnit(OrganizationUnitDTO orgUnit) throws DAOException;

	/**
	 * Elimina una unidad organizativa registrada en el sistema, borrando además
	 * todas sus unidades organizativas hija.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @throws DAOException
	 *             Si no existe la unidad organizativa, o se produce algún error
	 *             en la ejecución de la sentencia de BBDD.
	 */
	void removeOrganizationUnit(Long orgUnitId) throws DAOException;

	/**
	 * Comprueba si una unidad organizativa, o alguna de sus hijas poseen
	 * documentos en proceso de digitalización.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si no existe la unidad organizativa, o se produce algún error
	 *             en la ejecución de la sentencia de BBDD.
	 */
	Boolean hasUnitOrgDocsInScanProccess(Long orgUnitId) throws DAOException;
}
