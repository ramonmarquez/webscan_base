/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.ScanProcessStageDAO.java.</p>
 * <b>Descripción:</b><p> Interfaz que define la operaciones de la capa DAO sobre la entidad fase de
 * proceso de digitalización, perteneciente al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao;

import java.util.List;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;

/**
 * Interfaz que define la operaciones de la capa DAO sobre la entidad fase de
 * proceso de digitalización, perteneciente al modelo de datos base de Webscan.
 * <p>
 * Clase ScanProcessStageDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface ScanProcessStageDAO {

	/**
	 * Obtiene el listado de fases que conforman el proceso de digitalización de
	 * una determinada especificación de perfil de digitalización, ordenado por
	 * orden de ejecución.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de especificación de perfil de digitalización.
	 * @return listado de fases que conforman un determinada especificación de
	 *         perfil de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ScanProfileSpecScanProcStageDTO> getScanProcessStagesByScanProfileSpecId(Long scanProfileSpecId) throws DAOException;

	/**
	 * Obtiene la relación de fases reanudables de una definición de perfil de
	 * digitalización determinada.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de una definición de perfil de digitalización.
	 * @return relación de fases reanudables de una definición de perfil de
	 *         digitalización determinada ordenadas por orden de ejecución.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición de perfil de digitalización, o se produce algún
	 *             error en la consulta a BBDD.
	 */
	List<ScanProfileSpecScanProcStageDTO> getRenewableScanProcessStagesByScanProfileSpecId(Long scanProfileSpecId) throws DAOException;

	/**
	 * Obtiene una fase de proceso de digitalización a partir de su
	 * denominación.
	 * 
	 * @param scanProcessStageName
	 *            Página de resultados a devolver.
	 * @return fase de proceso de digitalización solicitada.
	 * @throws DAOException
	 *             Si no existe la definición de perfil de digitalización, o se
	 *             produce algún error en la consulta a BBDD.
	 */
	ScanProcessStageDTO getScanProcStageByName(String scanProcessStageName) throws DAOException;

	/**
	 * Recupera una fase de proceso de digitalización a partir de su
	 * identificador.
	 * 
	 * @param scanProcessStageId
	 *            Identificador de fase de proceso de digitalización.
	 * @return La fase de proceso de digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe fase
	 *             de proceso de digitalización para los parámetros
	 *             especificados, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	ScanProcessStageDTO getScanProcessStage(Long scanProcessStageId) throws DAOException;

	/**
	 * Recupera la configuración de una fase de proceso de digitalización a
	 * partir de su denominación y una especificación de perfiles de
	 * digitalización.
	 * 
	 * @param scanProcessStageName
	 *            Denominación de fase de proceso de digitalización.
	 * @param scanProfileSpecId
	 *            Identificador de especificación de perfiles de digitalización.
	 * @return La configuración de una fase de proceso de digitalización
	 *         solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe
	 *             configuración de fase de proceso de digitalización para los
	 *             parámetros especificados, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	ScanProfileSpecScanProcStageDTO getScanProcessStageByNameAndScanProfileSpecId(String scanProcessStageName, Long scanProfileSpecId) throws DAOException;
}
