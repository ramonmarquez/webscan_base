/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScanProfile.java.</p>
 * <b>Descripción:</b><p> Entidad JPA perfil de digitalización.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA perfil de digitalización.
 * <p>
 * Clase ScanProfile.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "SCAN_PROFILE", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME" }) })
public class ScanProfile implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "SCAN_PROFILE_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SCAN_PROFILE_ID_SEQ", sequenceName = "SCAN_PROFILE_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Atributo DESCRIPTION de la entidad.
	 */
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * Atributo ACTIVE de la entidad.
	 */
	@Column(name = "ACTIVE")
	private Boolean active = Boolean.FALSE;

	/**
	 * Relación de unidades organizativas donde se encuentra configurado el
	 * perfil de digitalización.
	 */
	@OneToMany(mappedBy = "scanProfile", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnit.class)
	private List<ScanProfileOrgUnit> organizations = new ArrayList<ScanProfileOrgUnit>();

	/**
	 * Entidad que define el perfil de digitalización.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProfileSpecification.class)
	@JoinColumn(name = "SPROFILE_SPEC_ID")
	private ScanProfileSpecification specification;

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfile() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo DESCRIPTION de la entidad.
	 * 
	 * @return el atributo DESCRIPTION de la entidad.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para el atributo DESCRIPTION de la entidad.
	 * 
	 * @param aDescription
	 *            nuevo valor para el atributo DESCRIPTION de la entidad.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene el atributo ACTIVE de la entidad.
	 * 
	 * @return el atributo ACTIVE de la entidad.
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Establece un nuevo valor para el atributo ACTIVE de la entidad.
	 * 
	 * @param activeParam
	 *            nuevo valor para el atributo ACTIVE de la entidad.
	 */
	public void setActive(Boolean activeParam) {
		this.active = activeParam;
	}

	/**
	 * Obtiene la relación de unidades organizativas donde se encuentra
	 * configurado el perfil de digitalización.
	 * 
	 * @return la relación de unidades organizativas donde se encuentra
	 *         configurado el perfil de digitalización.
	 */
	public List<ScanProfileOrgUnit> getOrganizations() {
		return organizations;
	}

	/**
	 * Establece una nueva relación de unidades organizativas donde se encuentra
	 * configurado el perfil de digitalización.
	 * 
	 * @param anyOrganizations
	 *            Nueva relación de unidades organizativas donde se encuentra
	 *            configurado el perfil de digitalización.
	 */
	public void setOrganizations(List<ScanProfileOrgUnit> anyOrganizations) {
		this.organizations.clear();
		if (anyOrganizations != null && !anyOrganizations.isEmpty()) {
			this.organizations.addAll(anyOrganizations);
		}
	}

	/**
	 * Obtiene la entidad que define el perfil de digitalización.
	 * 
	 * @return la entidad que define el perfil de digitalización.
	 */
	public ScanProfileSpecification getSpecification() {
		return specification;
	}

	/**
	 * Establece una nueva entidad que define el perfil de digitalización.
	 * 
	 * @param aSpecification
	 *            Nueva entidad que define el perfil de digitalización.
	 */
	public void setSpecification(ScanProfileSpecification aSpecification) {
		this.specification = aSpecification;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfile other = (ScanProfile) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
