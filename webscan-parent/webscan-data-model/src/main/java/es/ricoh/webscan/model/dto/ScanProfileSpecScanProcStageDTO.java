/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA configuración de fase del proceso de digitalización para
 * definición de perfil de digitalización.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA configuración de fase del proceso de digitalización
 * para definición de perfil de digitalización.
 * <p>
 * Clase ScanProfileSpecScanProcStageDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class ScanProfileSpecScanProcStageDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Orden de ejecución de la fase para una definición de perfil de
	 * digitalización.
	 */
	private Integer order;

	/**
	 * Indicador de obligatoriedad de ejecución de la fase para una definición
	 * de proceso de digitalización.
	 */
	private Boolean mandatory = Boolean.TRUE;

	/**
	 * Indica si una fase puede ser reanudada para una definición de proceso de
	 * digitalización.
	 */
	private Boolean renewable = Boolean.FALSE;

	/**
	 * Denominación de la definición de perfil de digitalización.
	 */
	private String scanProfileSpecification;

	/**
	 * Denominación de la fase del proceso de digitalización.
	 */
	private String scanProcessStage;

	/**
	 * Identificador de la definición de perfil de digitalización.
	 */
	private Long scanProfileSpecificationId;

	/**
	 * Identificador de la fase del proceso de digitalización.
	 */
	private Long scanProcessStageId;

	/**
	 * Constructor sin argumento.
	 */
	public ScanProfileSpecScanProcStageDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el orden de ejecución de la fase para una definición de perfil de
	 * digitalización.
	 * 
	 * @return el orden de ejecución de la fase para una definición de perfil de
	 *         digitalización.
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * Establece un nuevo orden de ejecución de la fase para una definición de
	 * perfil de digitalización.
	 * 
	 * @param anOrder
	 *            nuevo orden de ejecución de la fase para una definición de
	 *            perfil de digitalización.
	 */
	public void setOrder(Integer anOrder) {
		this.order = anOrder;
	}

	/**
	 * Indica si es requerida la ejecución de la fase para una definición de
	 * proceso de digitalización.
	 * 
	 * @return Si es requerida la ejecución de la fase para una definición de
	 *         proceso de digitalización, devuelve true. En caso contrario,
	 *         retorna false.
	 */
	public Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * Estable un nuevo valor para el indicador de ejecución de la fase para una
	 * definición de proceso de digitalización.
	 * 
	 * @param mandatoryParam
	 *            nuevo valor para el indicador de ejecución de la fase para una
	 *            definición de proceso de digitalización.
	 */
	public void setMandatory(Boolean mandatoryParam) {
		this.mandatory = mandatoryParam;
	}

	/**
	 * Indica si una fase puede ser reanudada para una definición de proceso de
	 * digitalización.
	 * 
	 * @return Si una fase puede ser reanudada para una definición de proceso de
	 *         digitalización, devuelve true. En caso contrario, retorna false.
	 */
	public Boolean getRenewable() {
		return renewable;
	}

	/**
	 * Estable un nuevo valor para el indicador que informa si una fase puede
	 * ser reanudada para una definición de proceso de digitalización.
	 * 
	 * @param isRenewable
	 *            nuevo valor para el indicador que informa si una fase puede
	 *            ser reanudada para una definición de proceso de
	 *            digitalización.
	 */
	public void setRenewable(Boolean isRenewable) {
		this.renewable = isRenewable;
	}

	/**
	 * Obtiene la denominación de la definición de perfil de digitalización.
	 * 
	 * @return la denominación de la definición de perfil de digitalización.
	 */
	public String getScanProfileSpecification() {
		return scanProfileSpecification;
	}

	/**
	 * Establece la denominación de la definición de perfil de digitalización.
	 * 
	 * @param aScanProfileSpecification
	 *            nueva denominación de la definición de perfil de
	 *            digitalización.
	 */
	void setScanProfileSpecification(String aScanProfileSpecification) {
		this.scanProfileSpecification = aScanProfileSpecification;
	}

	/**
	 * Obtiene la denominación de la fase del proceso de digitalización.
	 * 
	 * @return la denominación de la fase del proceso de digitalización.
	 */
	public String getScanProcessStage() {
		return scanProcessStage;
	}

	/**
	 * Establece la denominación de la fase del proceso de digitalización.
	 * 
	 * @param aScanProcessStage
	 *            nueva denominación de la fase del proceso de digitalización.
	 */
	public void setScanProcessStage(String aScanProcessStage) {
		this.scanProcessStage = aScanProcessStage;
	}

	/**
	 * Obtiene el identificador de la definición de perfil de digitalización.
	 * 
	 * @return el identificador de la definición de perfil de digitalización.
	 */
	public Long getScanProfileSpecificationId() {
		return scanProfileSpecificationId;
	}

	/**
	 * Establece el identificador de la definición de perfil de digitalización.
	 * 
	 * @param aScanProfileSpecificationId
	 *            nuevo identificador de la definición de perfil de
	 *            digitalización.
	 */
	public void setScanProfileSpecificationId(Long aScanProfileSpecificationId) {
		this.scanProfileSpecificationId = aScanProfileSpecificationId;
	}

	/**
	 * Obtiene el identificador de la fase del proceso de digitalización.
	 * 
	 * @return el identificador de la fase del proceso de digitalización.
	 */
	public Long getScanProcessStageId() {
		return scanProcessStageId;
	}

	/**
	 * Establece el identificador de la fase del proceso de digitalización.
	 * 
	 * @param aScanProcessStageId
	 *            nuevo identificador de la fase del proceso de digitalización.
	 */
	public void setScanProcessStageId(Long aScanProcessStageId) {
		this.scanProcessStageId = aScanProcessStageId;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((scanProcessStage == null) ? 0 : scanProcessStage.hashCode());
		result = prime * result + ((scanProfileSpecification == null) ? 0 : scanProfileSpecification.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfileSpecScanProcStageDTO other = (ScanProfileSpecScanProcStageDTO) obj;
		if (scanProcessStage == null) {
			if (other.scanProcessStage != null)
				return false;
		} else if (!scanProcessStage.equals(other.scanProcessStage))
			return false;
		if (scanProfileSpecification == null) {
			if (other.scanProfileSpecification != null)
				return false;
		} else if (!scanProfileSpecification.equals(other.scanProfileSpecification))
			return false;
		return true;
	}

}
