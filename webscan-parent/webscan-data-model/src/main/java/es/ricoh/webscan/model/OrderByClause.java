/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.OrderByClause.java.</p>
 * <b>Descripción:</b><p> Tipo de ordenación sobre un campo en una clausula ORDER BY de una consulta sobre alguna entidad de BBDD.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model;

/**
 * Tipo de ordenación sobre un campo en una clausula ORDER BY de una consulta
 * sobre alguna entidad de BBDD.
 * <p>
 * Clase OrderByClause.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum OrderByClause {

	/**
	 * Tipo de ordenación ascendente.
	 */
	ASC("ASC"),
	/**
	 * Tipo de ordenación descendente.
	 */
	DESC("DESC"),
	/**
	 * Sin ordenación.
	 */
	NO_ORDER("NO ORDER");

	/**
	 * Tipo de ordenación.
	 */
	private String orderByClause;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param anOrderByClause
	 *            Tipo de ordenación.
	 */
	OrderByClause(String anOrderByClause) {
		this.orderByClause = anOrderByClause;
	}

	/**
	 * Devuelve el tipo de ordenación.
	 * 
	 * @return Tipo de ordenación.
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

}
