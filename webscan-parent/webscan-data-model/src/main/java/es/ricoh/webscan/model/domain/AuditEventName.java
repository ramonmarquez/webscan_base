/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.AuditEventName.java.</p>
 * <b>Descripción:</b><p> Clase que define el conjunto de denominaciones que pueden ser asignados a un evento de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

/**
 * Clase que define el conjunto de denominaciones que pueden ser asignados a un
 * evento de auditoría.
 * <p>
 * Clase AuditEventName.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum AuditEventName {
	/**
	 * Evento USER LOGIN.
	 */
	USER_LOGIN("USER LOGIN"),
	/**
	 * Evento USER UPDATE PASSWORD.
	 */
	USER_UPDATE_PASSWORD("USER UPDATE PASSWORD"),
	/**
	 * Evento USER RECOVERY PASSWORD REQUEST.
	 */
	USER_RECOVERY_PASSWORD_REQ("USER RECOVERY PASSWORD REQUEST"),
	/**
	 * Evento USER RECOVERY PASSWORD CONFIRM.
	 */
	USER_RECOVERY_PASSWORD_CONFIRM("USER RECOVERY PASSWORD CONFIRM"),
	/**
	 * Evento ORG UNIT CREATE.
	 */
	ORG_UNIT_CREATE("ORG UNIT CREATE"),
	/**
	 * Evento ORG UNIT UPDATE.
	 */
	ORG_UNIT_UPDATE("ORG UNIT UPDATE"),
	/**
	 * Evento ORG UNIT DELETE.
	 */
	ORG_UNIT_DELETE("ORG UNIT DELETE"),
	/**
	 * Evento ORG REMOVE USERS.
	 */
	REMOVE_USERS("REMOVE USERS"),
	/**
	 * Evento CREATE USER.
	 */
	CREATE_USER("CREATE USER"),
	/**
	 * Evento UPDATE USER.
	 */
	UPDATE_USER("UPDATE USER"),
	/**
	 * Evento PLUGIN SPEC UPDATE.
	 */
	PLUGIN_SPEC_UPDATE("PLUGIN SPEC UPDATE"),
	/**
	 * Evento PLUGIN CREATE.
	 */
	PLUGIN_CREATE("PLUGIN CREATE"),
	/**
	 * Evento PLUGIN UPDATE.
	 */
	PLUGIN_UPDATE("PLUGIN UPDATE"),
	/**
	 * Evento PLUGIN REMOVE.
	 */
	PLUGIN_REMOVE("PLUGIN REMOVE"),
	/**
	 * Evento DOCUMENT SPEC CREATE.
	 */
	DOCUMENT_SPEC_CREATE("DOCUMENT SPEC CREATE"),
	/**
	 * Evento DOCUMENT SPEC UPDATE.
	 */
	DOCUMENT_SPEC_UPDATE("DOCUMENT SPEC UPDATE"),
	/**
	 * Evento DOCUMENT SPEC REMOVE.
	 */
	DOCUMENT_SPEC_REMOVE("DOCUMENT SPEC REMOVE"),
	/**
	 * Evento SCANPROFILE CREATE.
	 */
	SCANPROFILE_CREATE("SCANPROFILE CREATE"),
	/**
	 * Evento SCANPROFILE UPDATE.
	 */
	SCANPROFILE_UPDATE("SCANPROFILE UPDATE"),
	/**
	 * Evento SCANPROFILE REMOVE.
	 */
	SCANPROFILE_REMOVE("SCANPROFILE REMOVE"),
	/**
	 * Evento SCANDOC REMOVE.
	 */
	SCAN_PROC_REMOVE_DOC("SCANNED DOC REMOVE"),
	/**
	 * Evento SCAN.
	 */
	SCAN_PROC_SCAN_DOC("SCAN"),
	/**
	 * Evento RESCAN.
	 */
	SCAN_PROC_RESCAN_DOC("RESCAN"),
	/**
	 * Evento CHECK DOC.
	 */
	SCAN_PROC_CHECK_DOC("SCAN CHECK DOC"),
	/**
	 * Evento DEPOSIT.
	 */
	SCAN_PROC_DEPOSIT_DOC("DEPOSIT"),
	/**
	 * Evento DIGITAL SIGN.
	 */
	SCAN_PROC_DIGITAL_SIGN("DIGITAL SIGN"),
	/**
	 * Evento METADATA AUTOMATIC SET.
	 */
	SCAN_PROC_METADATA_AUTOMATIC_SET("METADATA AUTOMATIC SET"),
	/**
	 * Evento METADATA MANUAL SET.
	 */
	SCAN_PROC_METADATA_MANUAL_SET("METADATA MANUAL SET"),
	/**
	 * Evento GENERATE DOCS BATCH SPACER.
	 */
	GENERATE_DOCS_BATCH_SPACER("GENERATE DOCS BATCH SPACER"),
	/**
	 * Evento COMPLETE DOCS BATCH SPACER.
	 */
	COMPLETE_DOCS_BATCH_SPACER("COMPLETE DOCS BATCH SPACER"),
	/**
	 * Evento GENERATE REUSABLE DOCS SPACERS.
	 */
	GENERATE_REUSABLE_DOCS_SPACERS("GENERATE REUSABLE DOCS SPACERS");

	/**
	 * Denominación de eventos de auditoría.
	 */
	private String eventName;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param anEventName
	 *            Denominación de eventos de auditoría.
	 */
	AuditEventName(String anEventName) {
		this.eventName = anEventName;
	}

	/**
	 * Obtiene el atributo eventName.
	 * 
	 * @return el atributo eventName.
	 */
	public String getEventName() {
		return eventName;
	}

}
