/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.impl.OrganizationUnitDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre la entidad unidad organizativa,
 * perteneciente al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dao.OrganizationUnitDAO;
import es.ricoh.webscan.model.domain.OrganizationUnit;
import es.ricoh.webscan.model.dto.MappingUtilities;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitTreeDTO;

/**
 * Implementación JPA 2.0 de la capa DAO sobre la entidad unidad organizativa,
 * perteneciente al modelo de datos base de Webscan.
 * <p>
 * Clase OrganizationUnitDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.dao.DocumentDAO
 * @version 2.0.
 */
public final class OrganizationUnitDAOJdbcImpl implements OrganizationUnitDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(OrganizationUnitDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la interacción con en el contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * Constructor sin argumentos.
	 */
	public OrganizationUnitDAOJdbcImpl() {
		super();
	}

	/**
	 * Establece un nuevo gestor de persistencia JPA.
	 * 
	 * @param aEntityManager
	 *            manager de persistencia.
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		this.entityManager = aEntityManager;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.OrganizationUnitDAO#getRootOrganizationUnits()
	 */
	@Override
	public List<OrganizationUnitDTO> getRootOrganizationUnits() throws DAOException {
		List<OrganizationUnit> entities;
		List<OrganizationUnitDTO> res = new ArrayList<OrganizationUnitDTO>();
		String excMsg;

		LOG.debug("[WEBSCAN-MODEL] Recuperando las unidades organizativas raíz registradas en el sistema ...");

		try {
			entities = getRootOrganizationUnitEntities();

			res = MappingUtilities.fillOrganizationUnitListDto(entities);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " unidades organizativas raíz recuperadas.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.OrganizationUnitDAO#getChildOrganizationUnits(java.lang.Long)
	 */
	@Override
	public List<OrganizationUnitDTO> getChildOrganizationUnits(Long orgUnitId) throws DAOException {
		OrganizationUnit entity;
		List<OrganizationUnit> orgUnits;
		List<OrganizationUnitDTO> res = new ArrayList<OrganizationUnitDTO>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las unidades organizativas hijas de la entidad " + orgUnitId + " ...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getOrganizationUnitEntity(orgUnitId, entityManager);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las unidades organizativas hijas de la entidad " + orgUnitId + " ...");
			}
			orgUnits = entity.getChildOrgs();

			res = MappingUtilities.fillOrganizationUnitListDto(orgUnits);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Unidades organizativas hijas de la entidad " + orgUnitId + " recuperadas: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas hijas de la entidad " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las unidades organizativas hijas de la entidad " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas hijas de la entidad " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas hijas de la entidad " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas hijas de la entidad " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las unidades organizativas hijas de la entidad " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas hijas de la entidad " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.OrganizationUnitDAO#getOrganizationUnit(java.lang.Long)
	 */
	@Override
	public OrganizationUnitDTO getOrganizationUnit(Long orgUnitId) throws DAOException {
		OrganizationUnitDTO res;
		OrganizationUnit entity;

		entity = CommonUtilitiesDAOJdbcImpl.getOrganizationUnitEntity(orgUnitId, entityManager);

		LOG.debug("[WEBSCAN-MODEL] Parseando la unidad organizativa obtenida al ejecutar la consulta ...");

		res = MappingUtilities.fillOrganizationUnitDto(entity);

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.OrganizationUnitDAO#getOrgUnitByExternalId(java.lang.String)
	 */
	@Override
	public OrganizationUnitDTO getOrgUnitByExternalId(String externalId) throws DAOException {
		OrganizationUnit entity;
		OrganizationUnitDTO res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la unidad organizativa con identificador externo " + externalId + " ...");
			}
			TypedQuery<OrganizationUnit> query = entityManager.createQuery("SELECT orgUnit FROM OrganizationUnit orgUnit WHERE orgUnit.externalId = :externalId", OrganizationUnit.class);
			query.setParameter("externalId", externalId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la unidad organizativa con identificador externo " + externalId + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillOrganizationUnitDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Unidad organizativa con identificador externo " + externalId + " recuperada.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la unidad organizativa con identificador externo " + externalId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la unidad organizativa con identificador externo " + externalId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la unidad organizativa con identificador externo " + externalId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la unidad organizativa con identificador externo " + externalId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la unidad organizativa con identificador externo " + externalId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la unidad organizativa con identificador externo " + externalId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la unidad organizativa con identificador externo " + externalId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la unidad organizativa con identificador externo " + externalId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la unidad organizativa con identificador externo " + externalId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.OrganizationUnitDAO#getOrganizationUnitsTree()
	 */
	@Override
	public List<OrganizationUnitTreeDTO> getOrganizationUnitsTree() throws DAOException {
		List<OrganizationUnit> rootOrgUnits;
		List<OrganizationUnitTreeDTO> res = new ArrayList<OrganizationUnitTreeDTO>();
		OrganizationUnitTreeDTO orgUnitTreeDto;

		rootOrgUnits = getRootOrganizationUnitEntities();

		for (OrganizationUnit orgUnit: rootOrgUnits) {
			orgUnitTreeDto = MappingUtilities.fillOrganizationUnitTreeDto(orgUnit, getOrganizationUnitsTreeRec(orgUnit.getChildOrgs()));

			res.add(orgUnitTreeDto);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.OrganizationUnitDAO#createOrganizationUnit(es.ricoh.webscan.model.dto.OrganizationUnitDTO)
	 */
	@Override
	public Long createOrganizationUnit(OrganizationUnitDTO orgUnit) throws DAOException {
		Long res = null;
		OrganizationUnit entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando una nueva entidad unidad organizativa, denominada " + orgUnit.getName() + " ...");
		}
		entity = MappingUtilities.fillOrganizationUnitEntity(orgUnit);

		if (orgUnit.getParentId() != null) {
			entity.setParentOrg(CommonUtilitiesDAOJdbcImpl.getOrganizationUnitEntity(orgUnit.getParentId(), entityManager));
		}

		try {
			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva unidad organizativa " + orgUnit.getName() + " creada, identificador: " + res + ".");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la unidad organizativa " + orgUnit.getName() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la unidad organizativa " + orgUnit.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la unidad organizativa " + orgUnit.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la unidad organizativa " + orgUnit.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la unidad organizativa " + orgUnit.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.OrganizationUnitDAO#updateOrganizationUnit(es.ricoh.webscan.model.dto.OrganizationUnitDTO)
	 */
	@Override
	public void updateOrganizationUnit(OrganizationUnitDTO orgUnit) throws DAOException {
		OrganizationUnit entity, parentOrgUnit;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando la entidad " + orgUnit.getId() + " ...");
		}
		parentOrgUnit = null;

		try {
			entity = CommonUtilitiesDAOJdbcImpl.getOrganizationUnitEntity(orgUnit.getId(), entityManager);

			if (entity == null) {
				excMsg = "[WEBSCAN-MODEL] Error actualizando la unidad organizativa " + orgUnit.getId() + " en BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(excMsg);
			}

			entity.setExternalId(orgUnit.getExternalId());
			entity.setName(orgUnit.getName());
			if (orgUnit.getParentId() != null) {
				parentOrgUnit = CommonUtilitiesDAOJdbcImpl.getOrganizationUnitEntity(orgUnit.getParentId(), entityManager);
			}
			entity.setParentOrg(parentOrgUnit);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando la unidad organizativa " + orgUnit.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Unidad organizativa " + orgUnit.getId() + " actualizada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la unidad organizativa " + orgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la unidad organizativa " + orgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la unidad organizativa " + orgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la unidad organizativa " + orgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.OrganizationUnitDAO#removeOrganizationUnit(java.lang.Long)
	 */
	@Override
	public void removeOrganizationUnit(Long orgUnitId) throws DAOException {
		OrganizationUnit entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Eliminando la unidad organizativa " + orgUnitId + " ...");
		}
		try {
			entity = CommonUtilitiesDAOJdbcImpl.getOrganizationUnitEntity(orgUnitId, entityManager);

			if (entity == null) {
				excMsg = "[WEBSCAN-MODEL] Error eliminando la unidad organizativa " + orgUnitId + " en BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Eliminando la unidad organizativa " + orgUnitId + " en BBDD ...");
			}
			entityManager.remove(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Unidad organizativa " + orgUnitId + " eliminada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando la unidad organizativa " + orgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando la unidad organizativa " + orgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando la unidad organizativa " + orgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando la unidad organizativa " + orgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.OrganizationUnitDAO#hasUnitOrgDocsInScanProccess(java.lang.Long)
	 */
	@Override
	public Boolean hasUnitOrgDocsInScanProccess(Long orgUnitId) throws DAOException {
		Long numberOfDocs;
		Boolean res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + " ...");
			}
			TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(sd.id) FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile osp JOIN osp.orgUnit ou WHERE ou.id = :orgUnitId", Long.class);
			query.setParameter("orgUnitId", orgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + " ...");
			}
			numberOfDocs = query.getSingleResult();

			res = numberOfDocs > 0;
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Unidad organizativa " + orgUnitId + " con " + numberOfDocs + " en proceso de digitalización.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Recupera de BBDD la relación de unidades organizativas raíz.
	 * 
	 * @return Lista de unidades organizativas raíz.
	 * @throws DAOException
	 *             Si ocurre algún error al realizar la consulta sobre la BBDD.
	 */
	private List<OrganizationUnit> getRootOrganizationUnitEntities() throws DAOException {
		int queryResultSize = 0;
		List<OrganizationUnit> res = new ArrayList<OrganizationUnit>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las unidades organizativas raíz registradas en el sistema ...");
			}
			TypedQuery<OrganizationUnit> query = entityManager.createQuery("SELECT orgUnit FROM OrganizationUnit orgUnit WHERE orgUnit.parentOrg IS NULL", OrganizationUnit.class);

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las unidades organizativas raíz registradas en el sistema ...");
			res = query.getResultList();

			if (res != null && !res.isEmpty()) {
				queryResultSize = res.size();
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Unidades organizativas raíz recuperadas: ", queryResultSize);
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las unidades organizativas raíz: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Método de ayuda que recorre de forma recursiva la jerarquía o árbol de
	 * unidades organizativas registradas en el sistema.
	 * 
	 * @param childOrgUnits
	 *            Relación de unidades organizativas.
	 * @return Listado de las jerarquías o árboles de las unidades organizativas
	 *         especificadas como parámetro de entrada.
	 */
	private List<OrganizationUnitTreeDTO> getOrganizationUnitsTreeRec(List<OrganizationUnit> childOrgUnits) {
		List<OrganizationUnitTreeDTO> res = new ArrayList<OrganizationUnitTreeDTO>();
		OrganizationUnitTreeDTO orgUnitTreeDto;

		if (childOrgUnits != null && !childOrgUnits.isEmpty()) {
			for (OrganizationUnit childOrg: childOrgUnits) {
				orgUnitTreeDto = MappingUtilities.fillOrganizationUnitTreeDto(childOrg, getOrganizationUnitsTreeRec(childOrg.getChildOrgs()));

				res.add(orgUnitTreeDto);
			}
		}

		return res;
	}

}
