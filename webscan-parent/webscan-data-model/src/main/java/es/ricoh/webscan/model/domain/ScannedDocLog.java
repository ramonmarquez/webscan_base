/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScannedDocLog.java.</p>
 * <b>Descripción:</b><p> Entidad JPA traza o fase de ejecución asociada al proceso de digitalización de un documento.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA traza o fase de ejecución asociada al proceso de digitalización
 * de un documento.
 * <p>
 * Clase ScannedDocLog.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "SCANNED_DOC_LOG", uniqueConstraints = { @UniqueConstraint(columnNames = { "DPROC_STAGE_ID", "SCANNED_DOC_ID" }) })
public class ScannedDocLog implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "SCANNED_DOC_LOG_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SCANNED_DOC_LOG_ID_SEQ", sequenceName = "SCANNED_DOC_LOG_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo DOC_LOG_STATUS de la entidad.
	 */
	@Column(name = "DOC_LOG_STATUS")
	@Enumerated(EnumType.STRING)
	private OperationStatus status;

	/**
	 * Atributo START_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START_DATE")
	private Date startDate;

	/**
	 * Atributo END_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "END_DATE")
	private Date endDate;

	/**
	 * Fase del proceso de digitalización.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProcessStage.class)
	@JoinColumn(name = "DPROC_STAGE_ID")
	private ScanProcessStage scanProcessStage;

	/**
	 * Documento.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScannedDocument.class)
	@JoinColumn(name = "SCANNED_DOC_ID")
	private ScannedDocument document;

	/**
	 * Constructor sin argumentos.
	 */
	public ScannedDocLog() {
		super();
	}

	/**
	 * Constructor method for the class ScannedDocLog.java.
	 * 
	 * @param id
	 *            Identificador de la clase.
	 */
	public ScannedDocLog(Long id) {
		super();
		this.id = id;
	}

	/**
	 * Constructor method for the class ScannedDocLog.java.
	 * 
	 * @param id
	 *            Identificador de la clase. *
	 * @param status
	 *            Situación del documento escaneado.
	 * @param startDate
	 *            Fecha de comienzo del log.
	 * @param endDate
	 *            Fecha de fin del log.
	 * @param scanProcessStage
	 *            Etapa del proceso escaneado.
	 * @param document
	 *            Objecto del documento escaneado.
	 */
	public ScannedDocLog(Long id, OperationStatus status, Date startDate, Date endDate, ScanProcessStage scanProcessStage, ScannedDocument document) {
		super();
		this.id = id;
		this.status = status;
		this.startDate = startDate;
		this.endDate = endDate;
		this.scanProcessStage = scanProcessStage;
		this.document = document;
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo DOC_LOG_STATUS de la entidad.
	 * 
	 * @return el atributo DOC_LOG_STATUS de la entidad.
	 */
	public OperationStatus getStatus() {
		return status;
	}

	/**
	 * Establece un nuevo valor para el atributo DOC_LOG_STATUS de la entidad.
	 * 
	 * @param aStatus
	 *            nuevo valor para el atributo DOC_LOG_STATUS de la entidad.
	 */
	public void setStatus(OperationStatus aStatus) {
		this.status = aStatus;
	}

	/**
	 * Obtiene el atributo START_DATE de la entidad.
	 * 
	 * @return el atributo START_DATE de la entidad.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Establece un nuevo valor para el atributo START_DATE de la entidad.
	 * 
	 * @param aStartDate
	 *            nuevo valor para el atributo START_DATE de la entidad.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}

	/**
	 * Obtiene el atributo END_DATE de la entidad.
	 * 
	 * @return el atributo END_DATE de la entidad.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Establece un nuevo valor para el atributo END_DATE de la entidad.
	 * 
	 * @param aEndDate
	 *            nuevo valor para el atributo END_DATE de la entidad.
	 */
	public void setEndDate(Date aEndDate) {
		this.endDate = aEndDate;
	}

	/**
	 * Obtiene la entidad fase del proceso de digitalización.
	 * 
	 * @return la entidad fase del proceso de digitalización.
	 */
	public ScanProcessStage getScanProcessStage() {
		return scanProcessStage;
	}

	/**
	 * Establece la entidad fase del proceso de digitalización.
	 * 
	 * @param aScanProcessStage
	 *            Nueva entidad fase del proceso de digitalización.
	 */
	public void setScanProcessStage(ScanProcessStage aScanProcessStage) {
		this.scanProcessStage = aScanProcessStage;
	}

	/**
	 * Obtiene la entidad documento.
	 * 
	 * @return la entidad documento.
	 */
	public ScannedDocument getDocument() {
		return document;
	}

	/**
	 * Establece la entidad documento.
	 * 
	 * @param aDocument
	 *            Nueva entidad documento.
	 */
	public void setDocument(ScannedDocument aDocument) {
		this.document = aDocument;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((document == null) ? 0 : document.hashCode());
		result = prime * result + ((scanProcessStage == null) ? 0 : scanProcessStage.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScannedDocLog other = (ScannedDocLog) obj;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (scanProcessStage == null) {
			if (other.scanProcessStage != null)
				return false;
		} else if (!scanProcessStage.equals(other.scanProcessStage))
			return false;
		return true;
	}

}
