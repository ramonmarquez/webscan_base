/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.MetadataSpecificationDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA especificación de metadato.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA especificación de metadato.
 * <p>
 * Clase MetadataSpecificationDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class MetadataSpecificationDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación de la entidad.
	 */
	private String name;

	/**
	 * Descripción de la entidad.
	 */
	private String description;

	/**
	 * Valor por defecto de los metadatos asociados a esta definición.
	 */
	private String defaultValue;

	/**
	 * Indicador de obligatoriedad de los metadatos asociados a esta definición.
	 */
	private Boolean mandatory;

	/**
	 * Indicador de edición de los metadatos asociados a esta definición.
	 */
	private Boolean editable;

	/**
	 * Indicador de inclusión en carátulas de lotes de documentos.
	 */
	private Boolean includeInBatchSeprator = Boolean.FALSE;

	/**
	 * Indicador que especifica si los metadatos asociados a esta definición
	 * actúan como identificador de lotes documentos.
	 */
	private Boolean isBatchId = Boolean.FALSE;

	/**
	 * Lista de valores que pueden ser asignados a los metadatos asociados a
	 * esta definición (notación JSON).
	 */
	private String sourceValues;

	/**
	 * Denominación de la especificación de documento a la que pertenece el
	 * metadato.
	 */
	private String specification;

	/**
	 * Constructor sin argumentos.
	 */
	public MetadataSpecificationDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación de la entidad.
	 * 
	 * @return la denominación de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene la descripción de la entidad.
	 * 
	 * @return la descripción de la entidad.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para la descripción de la entidad.
	 * 
	 * @param aDescription
	 *            nuevo valor para la descripción de la entidad.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene el valor por defecto de los metadatos asociados a esta
	 * definición.
	 * 
	 * @return el valor por defecto de los metadatos asociados a esta
	 *         definición.
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Establece un nuevo valor por defecto de los metadatos asociados a esta
	 * definición.
	 * 
	 * @param aDefaultValue
	 *            nuevo valor por defecto de los metadatos asociados a esta
	 *            definición.
	 */
	public void setDefaultValue(String aDefaultValue) {
		this.defaultValue = aDefaultValue;
	}

	/**
	 * Indica si es requerido el metadato.
	 * 
	 * @return Si es requerido el metadato, true. En caso contrario, false.
	 */
	public Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * Estable un nuevo valor para el indicador de obligatoriedad del metadato.
	 * 
	 * @param mandatoryParam
	 *            nuevo valor para el indicador de obligatoriedad del metadato.
	 */
	public void setMandatory(Boolean mandatoryParam) {
		this.mandatory = mandatoryParam;
	}

	/**
	 * Indica si los metadatos asociados a esta definición son editables.
	 * 
	 * @return Si son editables, true. En caso contrario, false.
	 */
	public Boolean getEditable() {
		return editable;
	}

	/**
	 * Establece un nuevo valor para el indicador de edición de los metadatos
	 * asociados a esta definición.
	 * 
	 * @param editableParam
	 *            nuevo valor para el indicador de edición de los metadatos
	 *            asociados a esta definición.
	 */
	public void setEditable(Boolean editableParam) {
		this.editable = editableParam;
	}

	/**
	 * Indica si los metadatos asociados a esta definición pueden ser incluidos
	 * en carátulas de lotes de documentos.
	 * 
	 * @return Si pueden ser incluidos, true. En caso contrario, false.
	 */
	public Boolean getIncludeInBatchSeprator() {
		return includeInBatchSeprator;
	}

	/**
	 * Establece un nuevo valor para el indicador de inclusión en carátulas de
	 * lotes de documentos.
	 * 
	 * @param includeInBatchSepratorParam
	 *            nuevo valor del indicador de inclusión en carátulas de lotes
	 *            de documentos.
	 */
	public void setIncludeInBatchSeprator(Boolean includeInBatchSepratorParam) {
		this.includeInBatchSeprator = includeInBatchSepratorParam;
	}

	/**
	 * Indica si los metadatos asociados a esta definición pueden actúan como
	 * identificador de lotes de documentos.
	 * 
	 * @return Si actúan como identificador de lotes de documentos, true. En
	 *         caso contrario, false.
	 */
	public Boolean getIsBatchId() {
		return isBatchId;
	}

	/**
	 * Establece un nuevo valor para el indicador que especifica si los
	 * metadatos asociados a esta definición actúan como identificador de lotes
	 * documentos.
	 * 
	 * @param isBatchIdParam
	 *            nuevo valor del indicador que especifica si los metadatos
	 *            asociados a esta definición actúan como identificador de lotes
	 *            documentos.
	 */
	public void setIsBatchId(Boolean isBatchIdParam) {
		this.isBatchId = isBatchIdParam;
	}

	/**
	 * Obtiene la lista de valores que pueden ser asignados a los metadatos
	 * asociados a esta definición.
	 * 
	 * @return la lista de valores que pueden ser asignados a los metadatos
	 *         asociados a esta definición.
	 */
	public String getSourceValues() {
		return sourceValues;
	}

	/**
	 * Establece una nueva lista de valores que pueden ser asignados a los
	 * metadatos asociados a esta definición.
	 * 
	 * @param anySourceValues
	 *            nueva lista de valores que pueden ser asignados a los
	 *            metadatos asociados a esta definición.
	 */
	public void setSourceValues(String anySourceValues) {
		this.sourceValues = anySourceValues;
	}

	/**
	 * Obtiene la denominación de la especificación de documento a la que
	 * pertenece el metadato.
	 * 
	 * @return la denominación de la especificación de documento a la que
	 *         pertenece el metadato.
	 */
	public String getSpecification() {
		return specification;
	}

	/**
	 * Estable un nuevo valor de la denominación de la especificación de
	 * documento a la que pertenece el metadato.
	 * 
	 * @param aSpecification
	 *            nuevo valor para la denominación de la especificación de
	 *            documento a la que pertenece el metadato.
	 */
	void setSpecification(String aSpecification) {
		this.specification = aSpecification;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((specification == null) ? 0 : specification.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MetadataSpecificationDTO other = (MetadataSpecificationDTO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (specification == null) {
			if (other.specification != null)
				return false;
		} else if (!specification.equals(other.specification))
			return false;
		return true;
	}

}
