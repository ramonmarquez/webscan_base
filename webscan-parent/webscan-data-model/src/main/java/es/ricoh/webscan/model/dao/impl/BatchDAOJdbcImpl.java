/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.impl.BatchDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre las entidades documento digitalizado, su 
	 especificación y relaciones, pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dao.BatchDAO;
import es.ricoh.webscan.model.domain.Batch;
import es.ricoh.webscan.model.domain.ScannedDocument;
import es.ricoh.webscan.model.dto.BatchDTO;
import es.ricoh.webscan.model.dto.MappingUtilities;

/**
 * Implementación JPA 2.0 de la capa DAO sobre la entidad
 * de procesos de digitalización
 * <p>
 * Clase BatchDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.dao.DocumentDAO
 * @version 2.0.
 */
public final class BatchDAOJdbcImpl implements BatchDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BatchDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la interacción con en el contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * Constructor sin argumentos.
	 */
	public BatchDAOJdbcImpl() {
		super();
	}

	/**
	 * Establece un nuevo gestor de persistencia JPA.
	 * 
	 * @param aEntityManager
	 *            manager de persistencia.
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		this.entityManager = aEntityManager;
	}

	
	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.BatchDAO#getBatchs()
	 */
	@Override
	public List<BatchDTO> getBatchs() throws DAOException {
		List<BatchDTO> res;

		String excMsg;
		String jpqlQuery;

		try {
			//jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.DocBoxScannedDocDTO(sd.id, sd.name, logs.id, stage.name, sd.batchId, sd.batchOrderNum, sd.digProcStartDate, sd.functionalOrgs, sd.username, logs.status) FROM ScannedDocument sd JOIN sd.execLogs logs JOIN logs.scanProcessStage stage JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds and logs.id = (SELECT max(auxExeLog.id) from ScannedDocLog auxExeLog JOIN auxExeLog.document auxDoc WHERE auxDoc.id = sd.id)";
			jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.BatchDTO(bt.id, bt.batchNameId, bt.batchReqId,bt.startDate, bt.username, bt.functionalOrgs) FROM Batch bt";
			
			TypedQuery<BatchDTO> query = entityManager.createQuery(jpqlQuery, BatchDTO.class);

			res = query.getResultList();

		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (IllegalArgumentException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new IllegalArgumentException(e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}
	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocumentsAndLogsByOrgUnits(java.util.List,
	 *      java.lang.String)
	 */
	@Override
	public List<BatchDTO> getBatchs(List<Long> orgUnitIds, String functionalOrgs) throws DAOException {
		List<BatchDTO> res;
		List<String> functionalOrgsList = new ArrayList<String>();
		String excMsg;
		String jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + " de BBDD ...");
			}

			if (functionalOrgs == null) {
				jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.BatchDTO(bt.id, bt.batchNameId, bt.batchReqId, bt.startDate, bt.username, bt.functionalOrgs) FROM Batch bt JOIN bt.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds";
			} else {
				// Construir lista de unidades orgánicas
				functionalOrgsList = transformArrayToList(functionalOrgs);
				jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.BatchDTO(bt.id, bt.batchNameId, bt.batchReqId, bt.startDate, bt.username, bt.functionalOrgs) FROM Batch bt JOIN bt.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds and bt.functionalOrgs IN :functionalOrgs";
			}

			TypedQuery<BatchDTO> query = entityManager.createQuery(jpqlQuery, BatchDTO.class);
			query.setParameter("orgUnitIds", orgUnitIds);

			if (functionalOrgs != null && !functionalOrgsList.isEmpty()) {
				query.setParameter("functionalOrgs", functionalOrgsList);
			}
			res = query.getResultList();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Filtrando resultados por unidades orgánicas del usuario: " + functionalOrgs);
			}

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos para las unidades organizativas " + orgUnitIds + " recuperados de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (IllegalArgumentException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new IllegalArgumentException(e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.BatchDAO#getBatchs()
	 */
	@Override
	public BatchDTO getBatch(Long aBatchId) throws DAOException {
		BatchDTO res;
		Batch entity;
		
		String excMsg;
		/* String jpqlQuery; */

		try {
			/*
			 * jpqlQuery =
			 * "SELECT new es.ricoh.webscan.model.dto.BatchDTO(bt.id, bt.batchNameId, bt.batchReqId, bt.startDate, bt.username, bt.functionalOrgs) FROM Batch bt WHERE bt.id = :batchId"
			 * ;
			 * 
			 * TypedQuery<BatchDTO> query = entityManager.createQuery(jpqlQuery,
			 * BatchDTO.class); query.setParameter("batchId", aBatchId); res =
			 * query.getSingleResult();
			 */
			
			entity = getBatchEntity(aBatchId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando el proceso de digitalización " + aBatchId + " obtenido de BBDD ...");
			}
			res = MappingUtilities.fillBatchDto(entity);

		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (IllegalArgumentException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new IllegalArgumentException(e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " +  ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}
	
	
	/**
	 * Transforma un array de String a una lista de String
	 * 
	 * @param functionalOrgs
	 * @return Lista de functionalOrgs
	 */
	private List<String> transformArrayToList(String functionalOrgs) {
		List<String> result = new ArrayList<String>();

		if (functionalOrgs != null) {
			result = Arrays.asList(functionalOrgs);
		}

		return result;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#createBatch(es.ricoh.webscan.model.dto.BatchDTO,
	 *      java.lang.Long)
	 */
	@Override
	public Long createBatch(BatchDTO batch, Long scanProfOrgUnitDocSpecId) throws DAOException {
		Long res;
		Batch entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando el documento escaneado " + batch + " ...");
		}
		try {
			entity = MappingUtilities.fillBatchEntity(batch);
			entity.setScanProfileOrgUnitDocSpec(CommonUtilitiesDAOJdbcImpl.getScanProfileOrgUnitDocSpecEntity(scanProfOrgUnitDocSpecId, entityManager));

			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Proceso de digitalización " + batch.getId() + " creado en BBDD.");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el proceso de digitalización " + batch + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el proceso de digitalización " + batch + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el proceso de digitalización " + batch + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el proceso de digitalización " + batch + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el proceso de digitalización " + batch + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#updateDocuments(java.util.List)
	 */
	@Override
	public void updateBatch(BatchDTO batch) throws DAOException {
		Batch entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Actualizando el proceso de digitalización " + batch + " ...");
			}

			entity = getBatchEntity(batch.getId());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Actualizando el proceso de digitalización " + batch.getId() + " ...");
			}
			
			// Se actualizan los datos globales
			entity.setBatchNameId(batch.getBatchNameId());
			entity.setStartDate(batch.getStartDate());
			entity.setUsername(batch.getUsername());
			entity.setFunctionalOrgs(batch.getFunctionalOrgs());
			
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando el proceso de digitalización " + entity.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Proceso de digitalización " + entity.getId() + " actualizado en BBDD ...");
			}

			entityManager.flush();
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el proceso de digitalización " + batch + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el proceso de digitalización " + batch + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el proceso de digitalización " + batch + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el proceso de digitalización " + batch + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#updateDocuments(java.util.List)
	 */
	@Override
	public void updateBatchs(List<BatchDTO> batchs) throws DAOException {
		Batch entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Actualizando la lista de procesos de digitalización " + batchs + " ...");
			}
			for (BatchDTO batch: batchs) {
				entity = getBatchEntity(batch.getId());
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Actualizando el proceso de digitalización " + batch.getId() + " ...");
				}
				// Se actualizan los datos globales
				entity.setBatchNameId(batch.getBatchNameId());
				entity.setStartDate(batch.getStartDate());
				entity.setUsername(batch.getUsername());
				entity.setFunctionalOrgs(batch.getFunctionalOrgs());
				
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Modificando el proceso de digitilazión " + entity.getId() + " en BBDD ...");
				}
				entityManager.merge(entity);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Proceso de digitalización " + entity.getId() + " actualizado en BBDD ...");
				}
			}

			entityManager.flush();
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de documento escaneados " + batchs + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de documento escaneados " + batchs + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de documento escaneados " + batchs + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de documento escaneados " + batchs + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#removeDocument(java.lang.Long)
	 */
	@Override
	public void removeBatch(Long batchId) throws DAOException {
		Batch entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Eliminando el proceso de digitalización " + batchId + " en BBDD ...");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la entidad proceso de digitalización " + batchId + " en BBDD ...");
			}
			entity = getBatchEntity(batchId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando setencia SQL para eliminar la entidad proceso de digitalización " + entity.getId() + " en BBDD ...");
			}
			entityManager.remove(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Entidad proceso de digitalización " + batchId + " eliminada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el proceso de digitalización " + batchId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el proceso de digitalización " + batchId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el proceso de digitalización " + batchId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el proceso de digitalización " + batchId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#removeDocuments(java.util.List)
	 */
	@Override
	public void removeBatchs(List<Long> batchIds) throws DAOException {
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Eliminando los documentos escaneados " + batchIds + " ...");
		}

		try {
			Query query = entityManager.createQuery("DELETE FROM Batch b WHERE b.id IN :batchIds");
			query.setParameter("batchIds", batchIds);
			int res = query.executeUpdate();

			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res + " procesos de digitalización eliminados de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los procesos de digitalización " + batchIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los procesos de digitalización " + batchIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los procesos de digitalización " + batchIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los procesos de digitalización " + batchIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los procesos de digitalización " + batchIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * Recupera de BBDD una entidad documento digitalizado a partir de su
	 * identificador.
	 * 
	 * @param docId
	 *            Identificador de documento digitalizado.
	 * @return Entidad de BBDD documento digitalizado.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private Batch getBatchEntity(Long batchId) throws DAOException {
		Batch res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el proceso de digitalización " + batchId + " de BBDD ...");
			}
			res = entityManager.find(Batch.class, batchId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando el proceso de digitalización " + batchId + " en BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Proceso de digitalización realizado " + res.getId() + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el proceso de digitalización " + batchId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el proceso de digitalización " + batchId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}
}
