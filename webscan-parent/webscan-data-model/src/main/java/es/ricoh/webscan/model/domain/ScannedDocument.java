/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScannedDocument.java.</p>
 * <b>Descripción:</b><p> Entidad JPA documento digitalizado.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad JPA documento digitalizado.
 * <p>
 * Clase ScannedDocument.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "SCANNED_DOC")
public class ScannedDocument implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "SCANNED_DOC_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SCANNED_DOC_ID_SEQ", sequenceName = "SCANNED_DOC_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	// Posible mejora en tratamiento de BLOB's como Stream (java.sql.Blob)
	/**
	 * Atributo HASH de la entidad.
	 */
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "HASH", nullable = true)
	private byte[ ] hash;

	/**
	 * Atributo TEMP_PATH de la entidad.
	 */
	@Column(name = "TEMP_PATH")
	private String storeTempPath;

	// Posible mejora en tratamiento de BLOB's como Stream (java.sql.Blob)
	/**
	 * Atributo CONTENT de la entidad.
	 */
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "CONTENT", nullable = true)
	private byte[ ] content;

	/**
	 * Atributo SIGN_TEMP_PATH de la entidad.
	 */
	@Column(name = "SIGN_TEMP_PATH")
	private String storeSignatureTempPath;

	// Posible mejora en tratamiento de BLOB's como Stream (java.sql.Blob)
	/**
	 * Atributo SIGNATURE de la entidad.
	 */
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "SIGNATURE", nullable = true)
	private byte[ ] signature;

	/**
	 * Atributo OCR_TEMP_PATH de la entidad.
	 */
	@Column(name = "OCR_TEMP_PATH")
	private String storeOcrTextTempPath;

	// Posible mejora en tratamiento de BLOB's como Stream (java.sql.Blob)
	/**
	 * Atributo OCR_CONTENT de la entidad.
	 */
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "OCR_CONTENT", nullable = true)
	private byte[ ] ocr;

	/**
	 * Atributo DIG_PROC_START_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DIG_PROC_START_DATE")
	private Date digProcStartDate;

	/**
	 * Atributo DIG_PROC_END_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DIG_PROC_END_DATE")
	private Date digProcEndDate;

	/**
	 * Atributo CHECKED de la entidad.
	 */
	@Column(name = "CHECKED")
	private Boolean checked = Boolean.FALSE;

	/**
	 * Atributo CHECK_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CHECK_DATE")
	private Date checkDate;

	/**
	 * Atributo BATCH_ID de la entidad.
	 */
	/*
	 * @Column(name = "BATCH_ID") private String batchId;
	 */
	
	
	/**
	 * Atributo BATCH_ORDER_NUM de la entidad.
	 */
	@Column(name = "BATCH_ORDER_NUM")
	private Integer batchOrderNum;

	/**
	 * Atributo BATCH_REQ_ID de la entidad.
	 */
	@Column(name = "BATCH_REQ_ID")
	private String batchReqId;

	/**
	 * Atributo MIME_TYPE de la entidad.
	 */
	@Column(name = "MIME_TYPE")
	private String mimeType;

	/**
	 * Atributo SIGNATURE_TYPE de la entidad.
	 */
	@Column(name = "SIGNATURE_TYPE")
	@Enumerated(EnumType.STRING)
	private SignatureType signatureType;

	/**
	 * Atributo USERNAME de la entidad.
	 */
	@Column(name = "USERNAME")
	private String username;

	/**
	 * Atributo USER_DPTOS de la entidad.
	 */
	@Column(name = "USER_DPTOS")
	private String functionalOrgs;

	/**
	 * Atributo AUDIT_OP_ID de la entidad.
	 */
	@Column(name = "AUDIT_OP_ID")
	private Long auditOpId;

	/**
	 * Relación de entidades de metadato del documento.
	 */
	@OneToMany(mappedBy = "document", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.Metadata.class)
	private List<Metadata> metadataCollection = new ArrayList<Metadata>();

	/**
	 * Entidad que define el conjunto de metadatos del documento.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnitDocSpec.class)
	@JoinColumn(name = "SPROF_ORG_DSPEC_ID")
	private ScanProfileOrgUnitDocSpec scanProfileOrgUnitDocSpec;
	
	/**
	 * Entidad que define el proceso del documento.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.Batch.class)
	@JoinColumn(name = "BATCH_ID")
	private Batch batch;
	
	/**
	 * Trazas de ejecución asociadas al proceso de digitalización del documento.
	 */
	@OneToMany(mappedBy = "document", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScannedDocLog.class)
	@OrderBy("startDate ASC")
	private List<ScannedDocLog> execLogs = new ArrayList<ScannedDocLog>();

	/**
	 * Constructor sin argumentos.
	 */
	public ScannedDocument() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aId
	 *            Identificador de la clase.
	 */
	public ScannedDocument(Long aId) {
		this.id = aId;
	}

	/**
	 * Constructor con argumentos.
	 * 
	 * @param anId
	 *            Identificador de la clase.
	 * @param aName
	 *            Denominación del documento.
	 * @param aHash
	 *            Hash del documento escaneado.
	 * @param aStoreTempPath
	 *            Ubicación temporal de almacenamiento.
	 * @param aContent
	 *            Contenido del documento escaneado.
	 * @param aStoreSignatureTempPath
	 *            Ubicación temporal de almacenamiento de la firma.
	 * @param aSignature
	 *            Firma del documento.
	 * @param aStoreOcrTextTempPath
	 *            Ubicación temporal de almacenamiento del texto ocr.
	 * @param anOcr
	 *            Ocr del documento.
	 * @param aDigProcStartDate
	 *            Comienzo del proceso de digitalización.
	 * @param aDigProcEndDate
	 *            Finalización del proceso de digitalización.
	 * @param checkedParam
	 *            Indica si el documento ha sido verificado y validado por el
	 *            personal de digitalización.
	 * @param aCheckDate
	 *            Fecha en la que ha sido verificado y validado el documento por
	 *            el personal de digitalización.
	 * @param aBatch
	 *            Identificador del lote.
	 * @param aBatchOrderNum
	 *            Número de orden del documento en el lote.
	 * @param aMimeType
	 *            Tipo mime del documento.
	 * @param aSignatureType
	 *            Tipo de firma del documento.
	 * @param anUsername
	 *            Identificador del nombre de usuario.
	 * @param aFunctionalOrgs
	 *            Strign con la unidades organizativas asociadas del documento.
	 * @param anAuditOpId
	 *            Identificador de la operación de auditoria.
	 * @param aMetadataCollection
	 *            Listado de metadatos.
	 * @param aScanProfileOrgUnitDocSpec
	 *            Objeto que relaciona la especificación de documento con el
	 *            perfil de escaneo y la unidad organizativa.
	 * @param someExecLogs
	 *            Listado de logs de ejecución.
	 */
	public ScannedDocument(Long anId, String aName, byte[ ] aHash, String aStoreTempPath, byte[ ] aContent, String aStoreSignatureTempPath, byte[ ] aSignature, String aStoreOcrTextTempPath, byte[ ] anOcr, Date aDigProcStartDate, Date aDigProcEndDate, Boolean checkedParam, Date aCheckDate, Batch aBatch, Integer aBatchOrderNum, String aMimeType, SignatureType aSignatureType, String anUsername, String aFunctionalOrgs, Long anAuditOpId, List<Metadata> aMetadataCollection, ScanProfileOrgUnitDocSpec aScanProfileOrgUnitDocSpec, List<ScannedDocLog> someExecLogs) {
		super();
		this.id = anId;
		this.name = aName;
		this.hash = aHash;
		this.storeTempPath = aStoreTempPath;
		this.content = aContent;
		this.storeSignatureTempPath = aStoreSignatureTempPath;
		this.signature = aSignature;
		this.storeOcrTextTempPath = aStoreOcrTextTempPath;
		this.ocr = anOcr;
		this.digProcStartDate = aDigProcStartDate;
		this.digProcEndDate = aDigProcEndDate;
		this.checked = checkedParam;
		this.checkDate = aCheckDate;
		this.batch = aBatch;
		this.batchOrderNum = aBatchOrderNum;
		this.mimeType = aMimeType;
		this.signatureType = aSignatureType;
		this.username = anUsername;
		this.functionalOrgs = aFunctionalOrgs;
		this.auditOpId = anAuditOpId;
		this.metadataCollection = aMetadataCollection;
		this.scanProfileOrgUnitDocSpec = aScanProfileOrgUnitDocSpec;
		this.execLogs = someExecLogs;
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo HASH de la entidad.
	 * 
	 * @return el atributo HASH de la entidad.
	 */
	public byte[ ] getHash() {
		return hash;
	}

	/**
	 * Establece un nuevo valor para el atributo HASH de la entidad.
	 * 
	 * @param aHash
	 *            nuevo valor para el atributo HASH de la entidad.
	 */
	public void setHash(byte[ ] aHash) {
		this.hash = aHash;
	}

	/**
	 * Obtiene el atributo TEMP_PATH de la entidad.
	 * 
	 * @return el atributo TEMP_PATH de la entidad.
	 */
	public String getStoreTempPath() {
		return storeTempPath;
	}

	/**
	 * Establece un nuevo valor para el atributo TEMP_PATH de la entidad.
	 * 
	 * @param aStoreTempPath
	 *            nuevo valor para el atributo TEMP_PATH de la entidad.
	 */
	public void setStoreTempPath(String aStoreTempPath) {
		this.storeTempPath = aStoreTempPath;
	}

	/**
	 * Obtiene el atributo CONTENT de la entidad.
	 * 
	 * @return el atributo CONTENT de la entidad.
	 */
	public byte[ ] getContent() {
		return content;
	}

	/**
	 * Establece un nuevo valor para el atributo CONTENT de la entidad.
	 * 
	 * @param aContent
	 *            nuevo valor para el atributo CONTENT de la entidad.
	 */
	public void setContent(byte[ ] aContent) {
		this.content = aContent;
	}

	/**
	 * Obtiene el atributo SIGN_TEMP_PATH de la entidad.
	 * 
	 * @return el atributo SIGN_TEMP_PATH de la entidad.
	 */
	public String getStoreSignatureTempPath() {
		return storeSignatureTempPath;
	}

	/**
	 * Establece un nuevo valor para el atributo SIGN_TEMP_PATH de la entidad.
	 * 
	 * @param aStoreSignatureTempPath
	 *            nuevo valor para el atributo SIGN_TEMP_PATH de la entidad.
	 */
	public void setStoreSignatureTempPath(String aStoreSignatureTempPath) {
		this.storeSignatureTempPath = aStoreSignatureTempPath;
	}

	/**
	 * Obtiene el atributo SIGNATURE de la entidad.
	 * 
	 * @return el atributo SIGNATURE de la entidad.
	 */
	public byte[ ] getSignature() {
		return signature;
	}

	/**
	 * Establece un nuevo valor para el atributo SIGNATURE de la entidad.
	 * 
	 * @param aSignature
	 *            nuevo valor para el atributo SIGNATURE de la entidad.
	 */
	public void setSignature(byte[ ] aSignature) {
		this.signature = aSignature;
	}

	/**
	 * Obtiene el atributo OCR_TEMP_PATH de la entidad.
	 * 
	 * @return el atributo OCR_TEMP_PATH de la entidad.
	 */
	public String getStoreOcrTextTempPath() {
		return storeOcrTextTempPath;
	}

	/**
	 * Establece un nuevo valor para el atributo OCR_TEMP_PATH de la entidad.
	 * 
	 * @param aStoreOcrTextTempPath
	 *            nuevo valor para el atributo OCR_TEMP_PATH de la entidad.
	 */
	public void setStoreOcrTextTempPath(String aStoreOcrTextTempPath) {
		this.storeOcrTextTempPath = aStoreOcrTextTempPath;
	}

	/**
	 * Obtiene el atributo OCR_CONTENT de la entidad.
	 * 
	 * @return el atributo OCR_CONTENT de la entidad.
	 */
	public byte[ ] getOcr() {
		return ocr;
	}

	/**
	 * Establece un nuevo valor para el atributo OCR_CONTENT de la entidad.
	 * 
	 * @param anOcr
	 *            nuevo valor para el atributo OCR_CONTENT de la entidad.
	 */
	public void setOcr(byte[ ] anOcr) {
		this.ocr = anOcr;
	}

	/**
	 * Obtiene el atributo DIG_PROC_START_DATE de la entidad.
	 * 
	 * @return el atributo DIG_PROC_START_DATE de la entidad.
	 */
	public Date getDigProcStartDate() {
		return digProcStartDate;
	}

	/**
	 * Establece un nuevo valor para el atributo DIG_PROC_START_DATE de la
	 * entidad.
	 * 
	 * @param aDigProcStartDate
	 *            nuevo valor para el atributo DIG_PROC_START_DATE de la
	 *            entidad.
	 */
	public void setDigProcStartDate(Date aDigProcStartDate) {
		this.digProcStartDate = aDigProcStartDate;
	}

	/**
	 * Obtiene el atributo DIG_PROC_END_DATE de la entidad.
	 * 
	 * @return el atributo DIG_PROC_END_DATE de la entidad.
	 */
	public Date getDigProcEndDate() {
		return digProcEndDate;
	}

	/**
	 * Establece un nuevo valor para el atributo DIG_PROC_END_DATE de la
	 * entidad.
	 * 
	 * @param aDigProcEndDate
	 *            nuevo valor para el atributo DIG_PROC_END_DATE de la entidad.
	 */
	public void setDigProcEndDate(Date aDigProcEndDate) {
		this.digProcEndDate = aDigProcEndDate;
	}

	/**
	 * Obtiene el atributo CHECKED de la entidad.
	 * 
	 * @return el atributo CHECKED de la entidad.
	 */
	public Boolean getChecked() {
		return checked;
	}

	/**
	 * Establece un nuevo valor para el atributo CHECKED de la entidad.
	 * 
	 * @param checkedParam
	 *            nuevo valor para el atributo CHECKED de la entidad.
	 */
	public void setChecked(Boolean checkedParam) {
		this.checked = checkedParam;
	}

	/**
	 * Obtiene el atributo CHECK_DATE de la entidad.
	 * 
	 * @return el atributo CHECK_DATE de la entidad.
	 */
	public Date getCheckDate() {
		return checkDate;
	}

	/**
	 * Establece un nuevo valor para el atributo CHECK_DATE de la entidad.
	 * 
	 * @param aCheckDate
	 *            nuevo valor para el atributo CHECK_DATE de la entidad.
	 */
	public void setCheckDate(Date aCheckDate) {
		this.checkDate = aCheckDate;
	}

	/**
	 * Obtiene el atributo BATCH_REQ_ID de la entidad.
	 * 
	 * @return el atributo BATCH_REQ_ID de la entidad.
	 */
	public String getBatchReqId() {
		return batchReqId;
	}

	/**
	 * Establece un nuevo valor para el atributo BATCH_REQ_ID de la entidad.
	 * 
	 * @param aBatchReqId
	 *            nuevo valor para el atributo BATCH_REQ_ID de la entidad.
	 */
	public void setBatchReqId(String aBatchReqId) {
		this.batchReqId = aBatchReqId;
	}

	/**
	 * Obtiene el atributo BATCH_ORDER_NUM de la entidad.
	 * 
	 * @return el atributo BATCH_ORDER_NUM de la entidad.
	 */
	public Integer getBatchOrderNum() {
		return batchOrderNum;
	}

	/**
	 * Establece un nuevo valor para el atributo BATCH_ORDER_NUM de la entidad.
	 * 
	 * @param aBatchOrderNum
	 *            nuevo valor para el atributo BATCH_ORDER_NUM de la entidad.
	 */
	public void setBatchOrderNum(Integer aBatchOrderNum) {
		this.batchOrderNum = aBatchOrderNum;
	}

	/**
	 * Obtiene el atributo BATCH_ID de la entidad.
	 * 
	 * @return el atributo BATCH_ID de la entidad.
	 */
	public Batch getBatch() {
		return batch;
	}

	/**
	 * Establece un nuevo valor para el atributo BATCH_ID de la entidad.
	 * 
	 * @param aBatch
	 *            nuevo valor para el atributo BATCH_ID de la entidad.
	 */
	public void setBatch(Batch aBatch) {
		this.batch = aBatch;
	}

	/**
	 * Obtiene el atributo MIME_TYPE de la entidad.
	 * 
	 * @return el atributo MIME_TYPE de la entidad.
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Establece un nuevo valor para el atributo MIME_TYPE de la entidad.
	 * 
	 * @param aMimeType
	 *            nuevo valor para el atributo MIME_TYPE de la entidad.
	 */
	public void setMimeType(String aMimeType) {
		this.mimeType = aMimeType;
	}

	/**
	 * Obtiene el atributo SIGNATURE_TYPE de la entidad.
	 * 
	 * @return el atributo SIGNATURE_TYPE de la entidad.
	 */
	public SignatureType getSignatureType() {
		return signatureType;
	}

	/**
	 * Establece un nuevo valor para el atributo SIGNATURE_TYPE de la entidad.
	 * 
	 * @param aSignatureType
	 *            nuevo valor para el atributo SIGNATURE_TYPE de la entidad.
	 */
	public void setSignatureType(SignatureType aSignatureType) {
		this.signatureType = aSignatureType;
	}

	/**
	 * Obtiene el atributo USERNAME de la entidad.
	 * 
	 * @return el atributo USERNAME de la entidad.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece un nuevo valor para el atributo USERNAME de la entidad.
	 * 
	 * @param anUsername
	 *            nuevo valor para el atributo USERNAME de la entidad.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene el atributo USER_DPTOS de la entidad.
	 * 
	 * @return el atributo USER_DPTOS de la entidad.
	 */
	public String getFunctionalOrgs() {
		return functionalOrgs;
	}

	/**
	 * Establece un nuevo valor para el atributo USER_DPTOS de la entidad.
	 * 
	 * @param anyFunctionalOrgs
	 *            nuevo valor para el atributo USER_DPTOS de la entidad.
	 */
	public void setFunctionalOrgs(String anyFunctionalOrgs) {
		this.functionalOrgs = anyFunctionalOrgs;
	}

	/**
	 * Obtiene el atributo AUDIT_OP_ID de la entidad.
	 * 
	 * @return el atributo AUDIT_OP_ID de la entidad.
	 */
	public Long getAuditOpId() {
		return auditOpId;
	}

	/**
	 * Establece un nuevo valor para el atributo AUDIT_OP_ID de la entidad.
	 * 
	 * @param anAuditOpId
	 *            nuevo valor para el atributo AUDIT_OP_ID de la entidad.
	 */
	public void setAuditOpId(Long anAuditOpId) {
		this.auditOpId = anAuditOpId;
	}

	/**
	 * Obtiene la relación de metadatos del documento.
	 * 
	 * @return la relación de metadatos del documento.
	 */
	public List<Metadata> getMetadataCollection() {
		return metadataCollection;
	}

	/**
	 * Establece una nueva relación de metadatos del documento.
	 * 
	 * @param aMetadataCollection
	 *            Nueva relación de metadatos del documento.
	 */
	public void setMetadataCollection(List<Metadata> aMetadataCollection) {
		this.metadataCollection.clear();

		if (aMetadataCollection != null && !aMetadataCollection.isEmpty()) {
			this.metadataCollection.addAll(aMetadataCollection);
		}
	}

	/**
	 * Obtiene la especificación que define el conjunto de metadatos del
	 * documento.
	 * 
	 * @return la especificación que define el conjunto de metadatos del
	 *         documento.
	 */
	public ScanProfileOrgUnitDocSpec getScanProfileOrgUnitDocSpec() {
		return scanProfileOrgUnitDocSpec;
	}

	/**
	 * Establece una nueva especificación que define el conjunto de metadatos
	 * del documento.
	 * 
	 * @param aScanProfileOrgUnitDocSpec
	 *            Nueva especificación que define el conjunto de metadatos del
	 *            documento.
	 */
	public void setScanProfileOrgUnitDocSpec(ScanProfileOrgUnitDocSpec aScanProfileOrgUnitDocSpec) {
		this.scanProfileOrgUnitDocSpec = aScanProfileOrgUnitDocSpec;
	}

	/**
	 * Obtiene las trazas de ejecución asociadas al proceso de digitalización
	 * del documento.
	 * 
	 * @return las trazas de ejecución asociadas al proceso de digitalización
	 *         del documento.
	 */
	public List<ScannedDocLog> getExecLogs() {
		return execLogs;
	}

	/**
	 * Establece las trazas de ejecución asociadas al proceso de digitalización
	 * del documento.
	 * 
	 * @param anyExecLogs
	 *            Nuevas trazas de ejecución asociadas al proceso de
	 *            digitalización del documento.
	 */
	public void setExecLogs(List<ScannedDocLog> anyExecLogs) {
		this.execLogs.clear();
		if (anyExecLogs != null && !anyExecLogs.isEmpty()) {
			this.execLogs.addAll(anyExecLogs);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScannedDocument other = (ScannedDocument) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
