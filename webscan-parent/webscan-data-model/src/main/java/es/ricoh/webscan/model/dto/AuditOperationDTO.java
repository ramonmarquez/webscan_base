/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.AuditOperationDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA traza de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;

/**
 * DTO para la entidad JPA traza de auditoría.
 * <p>
 * Clase AuditOperationDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class AuditOperationDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación de la entidad.
	 */
	private AuditOperationName name;

	/**
	 * Estado de la entidad.
	 */
	private AuditOperationStatus status;

	/**
	 * Fecha de creación de la entidad.
	 */
	private Date startDate;

	/**
	 * Última fecha de actualización de la entidad.
	 */
	private Date lastUpdateDate;

	/**
	 * Identificador del histórico de documento asociado a la traza de
	 * auditoría. Es opcional.
	 */
	private Long auditScannedDocId;

	/**
	 * Constructor sin argumentos.
	 */
	public AuditOperationDTO() {
		super();
	}

	/**
	 * Obtiene el identificador del evento.
	 * 
	 * @return el identificador del evento.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la traza.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la traza.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación de la traza.
	 * 
	 * @return la denominación de la traza.
	 */
	public AuditOperationName getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación de la traza.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación de la traza.
	 */
	public void setName(AuditOperationName aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el estado de la traza.
	 * 
	 * @return el estado de la traza.
	 */
	public AuditOperationStatus getStatus() {
		return status;
	}

	/**
	 * Establece un nuevo valor para el estado de la traza.
	 * 
	 * @param aStatus
	 *            nuevo valor para el estado de la traza.
	 */
	public void setStatus(AuditOperationStatus aStatus) {
		this.status = aStatus;
	}

	/**
	 * Obtiene la fecha de creación de la traza.
	 * 
	 * @return la fecha de creación de la traza.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Establece un nuevo valor para la fecha de creación de la traza.
	 * 
	 * @param aStartDate
	 *            nuevo valor para la fecha de creación de la traza.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}

	/**
	 * Obtiene la útlima fecha de actualización de la traza.
	 * 
	 * @return la útlima fecha de actualización de la traza.
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * Establece un nuevo valor para la útlima fecha de actualización de la
	 * traza.
	 * 
	 * @param aLastUpdateDate
	 *            nuevo valor para la útlima fecha de actualización de la traza.
	 */
	public void setLastUpdateDate(Date aLastUpdateDate) {
		this.lastUpdateDate = aLastUpdateDate;
	}

	/**
	 * Obtiene el identificador del histórico de documento asociado a la traza
	 * de auditoría. Es opcional.
	 * 
	 * @return el identificador del histórico de documento asociado a la traza
	 *         de auditoría. Es opcional.
	 */
	public Long getAuditScannedDocId() {
		return auditScannedDocId;
	}

	/**
	 * Establece el identificador del histórico de documento asociado a la traza
	 * de auditoría. Es opcional.
	 * 
	 * @param anAuditScannedDocId
	 *            identificador del histórico de documento asociado a la traza
	 *            de auditoría. Es opcional.
	 */
	public void setAuditScannedDocId(Long anAuditScannedDocId) {
		this.auditScannedDocId = anAuditScannedDocId;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditOperationDTO other = (AuditOperationDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
