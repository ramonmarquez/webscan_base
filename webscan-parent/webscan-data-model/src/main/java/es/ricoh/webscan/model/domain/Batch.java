/*-
 * #%L
 * Webscan 3 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScannedDocument.java.</p>
 * <b>Descripción:</b><p> Entidad JPA documento digitalizado.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 3.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad JPA documento digitalizado.
 * <p>
 * Clase Batch.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */
@Entity
@Table(name = "BATCH")
public class Batch implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "BATCH_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "BATCH_ID_SEQ", sequenceName = "BATCH_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo BATCH_NAME de la entidad.
	 */
	@Column(name = "BATCH_NAME_ID")
	private String batchNameId;

	/**
	 * Atributo BATCH_NAME de la entidad.
	 */
	@Column(name = "BATCH_REQ_ID")
	private String batchReqId;

	/**
	 * Atributo START_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START_DATE")
	private Date startDate;

	/**
	 * Atributo UPDATE_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATE_DATE")
	private Date updateDate;

	/**
	 * Atributo USERNAME de la entidad.
	 */
	@Column(name = "USERNAME")
	private String username;

	/**
	 * Atributo USER_DPTOS de la entidad.
	 */
	@Column(name = "USER_DPTOS")
	private String functionalOrgs;

	/*	*//**
			 * Relación de entidades de proceso del documento.
			 *//*
				 * @OneToMany(mappedBy = "batch", fetch = FetchType.LAZY, cascade = {
				 * CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity
				 * = es.ricoh.webscan.model.domain.ScannedDocument.class) private
				 * List<ScannedDocument> scannedDocCollection = new
				 * ArrayList<ScannedDocument>();
				 */
	/**
	 * Entidad que define el conjunto de metadatos del documento.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnitDocSpec.class)
	@JoinColumn(name = "SPROF_ORG_DSPEC_ID")
	private ScanProfileOrgUnitDocSpec scanProfileOrgUnitDocSpec;

	/**
	 * Constructor sin argumentos.
	 */
	public Batch() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aId Identificador de la clase.
	 */
	public Batch(Long aId) {
		this.id = aId;
	}

	/**
	 * Constructor con argumentos.
	 * 
	 * @param anId                       Identificador del proceso de
	 *                                   digitalización.
	 * @param aBatchNameId               Identificador normalizado del proceso de
	 *                                   digitalización.
	 * @param aStartDate                 Fecha de inicio del proceso de
	 *                                   digitalización.
	 * @param aUpdateDate                Fecha de última modificación del proceso de
	 *                                   digitalización.
	 * @param anUsername                 Usuario que ha digitalizado algún documento
	 *                                   en el proceso.
	 * @param aFunctionalOrgs            Strign con la unidades organizativas
	 *                                   asociadas del documento.
	 * @param aScanProfileOrgUnitDocSpec Objeto que relaciona la especificación de
	 *                                   documento con el perfil de escaneo y la
	 *                                   unidad organizativa.
	 */
	public Batch(Long anId, String aBatchNameId, String aBatchReqId, Date aStartDate, String anUsername,
			String aFunctionalOrgs, ScanProfileOrgUnitDocSpec aScanProfileOrgUnitDocSpec) {
		super();
		this.id = anId;
		this.batchNameId = aBatchNameId;
		this.batchReqId = aBatchReqId;
		this.startDate = aStartDate;
		this.username = anUsername;
		this.functionalOrgs = aFunctionalOrgs;
		this.scanProfileOrgUnitDocSpec = aScanProfileOrgUnitDocSpec;

	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo BATCH_NAME_ID de la entidad.
	 * 
	 * @return el atributo BATCH_NAME_ID de la entidad.
	 */
	public String getBatchNameId() {
		return batchNameId;
	}

	/**
	 * Establece un nuevo valor para el atributo BATCH_NAME_ID de la entidad.
	 * 
	 * @param aBatchNameId nuevo valor para el atributo BATCH_NAME_ID de la entidad.
	 */
	public void setBatchNameId(String aBatchNameId) {
		this.batchNameId = aBatchNameId;
	}

	/**
	 * Obtiene el atributo BATCH_REQ_ID de la entidad.
	 * 
	 * @return el atributo BATCH_REQ_ID de la entidad.
	 */
	public String getBatchReqId() {
		return batchReqId;
	}

	/**
	 * Establece un nuevo valor para el atributo BATCH_REQ_ID de la entidad.
	 * 
	 * @param aBatchNameId nuevo valor para el atributo BATCH_REQ_ID de la entidad.
	 */
	public void setBatchReqId(String aBatchReqId) {
		this.batchReqId = aBatchReqId;
	}

	/**
	 * Obtiene el atributo START_DATE de la entidad.
	 * 
	 * @return el atributo START_DATE de la entidad.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Establece un nuevo valor para el atributo START_DATE de la entidad.
	 * 
	 * @param aStartDate nuevo valor para el atributo START_DATE de la entidad.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}

	/**
	 * Obtiene el atributo UPDATE_DATE de la entidad.
	 * 
	 * @return el atributo UPDATE_DATE de la entidad.
	 */
	public Date getUpdatetDate() {
		return updateDate;
	}

	/**
	 * Establece un nuevo valor para el atributo UPDATE_DATE de la entidad.
	 * 
	 * @param aStartDate nuevo valor para el atributo UPDATE_DATE de la entidad.
	 */
	public void setUpdateDate(Date aUpdateDate) {
		this.updateDate = aUpdateDate;
	}

	/**
	 * Obtiene el atributo USERNAME de la entidad.
	 * 
	 * @return el atributo USERNAME de la entidad.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece un nuevo valor para el atributo USERNAME de la entidad.
	 * 
	 * @param anUsername nuevo valor para el atributo USERNAME de la entidad.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene el atributo USER_DPTOS de la entidad.
	 * 
	 * @return el atributo USER_DPTOS de la entidad.
	 */
	public String getFunctionalOrgs() {
		return functionalOrgs;
	}

	/**
	 * Establece un nuevo valor para el atributo USER_DPTOS de la entidad.
	 * 
	 * @param anyFunctionalOrgs nuevo valor para el atributo USER_DPTOS de la
	 *                          entidad.
	 */
	public void setFunctionalOrgs(String anyFunctionalOrgs) {
		this.functionalOrgs = anyFunctionalOrgs;
	}

	/**
	 * Obtiene la especificación que define el conjunto de metadatos del documento.
	 * 
	 * @return la especificación que define el conjunto de metadatos del documento.
	 */
	public ScanProfileOrgUnitDocSpec getScanProfileOrgUnitDocSpec() {
		return scanProfileOrgUnitDocSpec;
	}

	/**
	 * Establece una nueva especificación que define el conjunto de metadatos del
	 * documento.
	 * 
	 * @param aScanProfileOrgUnitDocSpec Nueva especificación que define el conjunto
	 *                                   de metadatos del documento.
	 */
	public void setScanProfileOrgUnitDocSpec(ScanProfileOrgUnitDocSpec aScanProfileOrgUnitDocSpec) {
		this.scanProfileOrgUnitDocSpec = aScanProfileOrgUnitDocSpec;
	}

	/*	*//**
			 * Obtiene la relación de documentos digitalizados para la definición de
			 * documentos establecida en la configuración de perfil de digitalización en
			 * unidad organizativa.
			 * 
			 * @return la relación de documentos digitalizados para la definición de
			 *         documentos establecida en la configuración de perfil de
			 *         digitalización en unidad organizativa.
			 */
	/*
	 * public List<ScannedDocument> getDocuments() { return scannedDocCollection; }
	 * 
	 *//**
		 * Establece una nueva relación de documentos digitalizados para la definición
		 * de documentos establecida en la configuración de perfil de digitalización en
		 * unidad organizativa.
		 * 
		 * @param anyDocuments Nueva relación de documentos digitalizados para la
		 *                     definición de documentos establecida en la configuración
		 *                     de perfil de digitalización en unidad organizativa.
		 *//*
			 * public void setDocuments(List<ScannedDocument> anyDocuments) {
			 * this.scannedDocCollection.clear(); if (anyDocuments != null &&
			 * !anyDocuments.isEmpty()) { this.scannedDocCollection.addAll(anyDocuments); }
			 * }
			 */
}
