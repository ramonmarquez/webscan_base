/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.ScanProfileDAO.java.</p>
 * <b>Descripción:</b><p> Interfaz que define la operaciones de la capa DAO sobre las entidades perfil de digitalización, su
	 * especificación y relaciones, pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao;

import java.util.List;
import java.util.Map;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.PluginScanProcStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecificationDTO;

/**
 * Interfaz que define la operaciones de la capa DAO sobre las entidades perfil
 * de digitalización, su especificación y relaciones, pertenecientes al modelo
 * de datos base de Webscan.
 * <p>
 * Clase ScanProfileDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface ScanProfileDAO {

	// Operaciones sobre definiciones de perfiles de digitalización
	/**
	 * Obtiene el listado de definiciones de perfiles de digitalización
	 * registradas en el sistema, permitiendo la paginación y ordenación del
	 * resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de definiciones de perfiles de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ScanProfileSpecificationDTO> listScanProfileSpecifications(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException;

	/**
	 * Obtiene una especificación de perfil de digitalización a partir de su
	 * identificador.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de especificación de perfil de digitalización.
	 * @return La definición de perfil de digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	ScanProfileSpecificationDTO getScanProfileSpecification(Long scanProfileSpecId) throws DAOException;

	/**
	 * Recupera una especificación de perfil de digitalización a partir de su
	 * denominación.
	 * 
	 * @param scanProfileSpecName
	 *            Denominación de especificación de perfil de digitalización.
	 * @return La definición de perfil de digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	ScanProfileSpecificationDTO getScanProfileSpecificationByName(String scanProfileSpecName) throws DAOException;

	/**
	 * Actualiza una especificación de perfil de digitalización registrada en el
	 * sistema. El único dato que puede ser modificado es la descripción de la
	 * especificación.
	 * 
	 * @param scanProfileSpecification
	 *            Especificación de perfil de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void updateScanProfileSpecification(ScanProfileSpecificationDTO scanProfileSpecification) throws DAOException;

	// Operaciones sobre perfiles de digitalización
	/**
	 * Obtiene el listado de perfiles de digitalización registrados en el
	 * sistema, permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de definiciones de perfiles de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ScanProfileDTO> listScanProfiles(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException;

	/**
	 * Obtiene un perfil de digitalización a partir de su identificador.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @return El perfil de digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil de digitalización, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	ScanProfileDTO getScanProfile(Long scanProfileId) throws DAOException;

	/**
	 * Obtiene un perfil de digitalización a partir de su denominación.
	 * 
	 * @param scanProfileName
	 *            Denominación de perfil de digitalización.
	 * @return El perfil de digitalización solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil de digitalización, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	ScanProfileDTO getScanProfileByName(String scanProfileName) throws DAOException;

	/**
	 * Obtiene la relación de perfiles de digitalización que implementan una
	 * especificación determinada. Este método habilita la paginación y
	 * ordenación del resultado.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de definición de perfil de digitalización.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return Relación de perfiles de digitalización que implementan una
	 *         especificación determinada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	List<ScanProfileDTO> getScanProfilesBySpec(Long scanProfileSpecId, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException;

	/**
	 * Obtiene la relación de perfiles de digitalización configurados para una
	 * unidad organizativa determinada.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return Relación de perfiles de digitalización que implementan una
	 *         especificación determinada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ScanProfileDTO> getScanProfilesByOrgUnit(Long orgUnitId) throws DAOException;

	/**
	 * Comprueba si un perfil de digitalización posee documentos en proceso de
	 * digitalización.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @return Si el perfil de digitalización posee documentos en proceso de
	 *         digitalización, retorna true. En caso contrario, false.
	 * @throws DAOException
	 *             Si no existe el perfil de digitalización, o se produce algún
	 *             error en la ejecución de la sentencia de BBDD.
	 */
	Boolean hasScanProfileDocsInScanProccess(Long scanProfileId) throws DAOException;

	/**
	 * Crea un nuevo perfil de digitalización en el sistema.
	 * 
	 * @param scanProfile
	 *            Nuevo perfil de digitalización.
	 * @param scanProfileSpecId
	 *            Identificador de la especificación que define el nuevo perfil
	 *            de digitalización.
	 * @return El perfil de digitalización creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación del nuevo perfil de digitalización, o se
	 *             produce algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createScanProfile(ScanProfileDTO scanProfile, Long scanProfileSpecId) throws DAOException;

	/**
	 * Actualiza un perfil de digitalización registrado en el sistema. Los datos
	 * que pueden ser actualizados son la denominación, descripción y el estado
	 * de activación del perfil.
	 * 
	 * @param scanProfile
	 *            perfil de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe le
	 *             perfil, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void updateScanProfile(ScanProfileDTO scanProfile) throws DAOException;

	/**
	 * Elimina un perfil de digitalización del sistema.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void removeScanProfile(Long scanProfileId) throws DAOException;

	// Operaciones sobre perfiles configurados para ser usados en unidades
	// organizativas
	/**
	 * Recupera el listado de configuraciones en unidades organizativas de un
	 * determinado perfil de digitalización.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @return Lista de configuraciones en unidades organizativas de un
	 *         determinado perfil de digitalización. Retorna lista vacía si no
	 *         existe ninguna configuración.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	List<ScanProfileOrgUnitDTO> getScanProfileOrgUnitsByScanProfile(Long scanProfileId) throws DAOException;

	/**
	 * Recupera una configuración de perfil de digitalización para unidad
	 * organizativa a partir de su identificador.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización
	 *            para unidad organizativa.
	 * @return La configuración de perfil de digitalización para unidad
	 *         organizativa solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe
	 *             configuración de perfil de digitalización para unidad
	 *             organizativa, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	ScanProfileOrgUnitDTO getScanProfileOrgUnit(Long scanProfileOrgUnitId) throws DAOException;

	/**
	 * Recupera el perfil de digitalización en una unidad organizativa asociado
	 * al proceso de digitalización del documento.
	 * 
	 * @param docId
	 *            Documento digitalizado.
	 * @return perfil de digitalización en una unidad organizativa asociado al
	 *         proceso de digitalización del documento.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	ScanProfileOrgUnitDTO getScanProfileOrgUnitByDoc(Long docId) throws DAOException;

	/**
	 * Recupera una configuración de perfil de digitalización para unidad
	 * organizativa a partir de un perfil y unidad organizativa determinados.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return La configuración de perfil de digitalización para unidad
	 *         organizativa solicitada.
	 * @throws DAOException
	 *             Si no existe configuración de perfil de digitalización para
	 *             unidad organizativa, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	ScanProfileOrgUnitDTO getScanProfileOrgUnitByScanProfAndOrgUnit(Long scanProfileId, Long orgUnitId) throws DAOException;

	/**
	 * Recupera la configuración de un perfil de digitalización en una unidad
	 * organizativa a partir de la denominación del perfil y el identificador
	 * externo de la unidad organizativa.
	 * 
	 * @param scanProfileName
	 *            Denominación del perfil de digitalización.
	 * @param orgUnitExtId
	 *            Identificador externo de unidad organizativa.
	 * @return La configuración de un perfil de digitalización en una unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si no existe perfil de digitalización configurado para la
	 *             unidad organizativa, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	ScanProfileOrgUnitDTO getScanProfileOrgUnitByScanProfileNameAndOrgUnitExtId(String scanProfileName, String orgUnitExtId) throws DAOException;

	/**
	 * Recupera el plugin configurado para una fase dada y el perfil de
	 * digitalización asociado a un documento digitalizado.
	 * 
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @param docId
	 *            identificador de documento.
	 * @return plugin configurado para una fase dada y el perfil de
	 *         digitalización asociado a un documento digitalizado.
	 * @throws DAOException
	 *             Si no existe el documento digitalizado, o se produce algún
	 *             error en la ejecución de la consulta de BBDD.
	 */
	ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPluginByStageAndDoc(Long scanProcessStageId, Long docId) throws DAOException;

	/**
	 * Recupera los plugins configurados para una fase dada y el perfil de
	 * digitalización asociado a una lista de documentos digitalizados.
	 * 
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @param docIds
	 *            Identificadores de documentos.
	 * @return relación de plugins configurados para una fase dada y el perfil
	 *         de digitalización asociado a una lista de documentos
	 *         digitalizados.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la consulta de
	 *             BBDD.
	 */
	List<ScanProfileOrgUnitPluginDTO> getScanProfileOrgUnitPluginByStageAndDocs(Long scanProcessStageId, List<Long> docIds) throws DAOException;

	/**
	 * Recupera el plugin establecido para una fase y configuración de perfil de
	 * digitalización en unidad organziativa determinados.
	 * 
	 * @param scanProfileId
	 *            identificador de perfil de digitalización.
	 * @param orgUnitId
	 *            identificador de unidad organizativa.
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @return La configuración de plugin establecida para una fase y
	 *         configuración de perfil de digitalización en unidad organziativa.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la consulta de
	 *             BBDD.
	 */
	ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(Long scanProfileId, Long orgUnitId, Long scanProcessStageId) throws DAOException;

	/**
	 * Persiste en BBDD una nueva configuración de perfil de digitalización para
	 * unidad organizativa.
	 * 
	 * @param scanProfileOrg
	 *            Nueva configuración de perfil de digitalización para unidad
	 *            organizativa.
	 * @return Identificador de la nueva configuración de perfil de
	 *         digitalización para unidad organizativa.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	Long createScanProfileOrgUnit(ScanProfileOrgUnitDTO scanProfileOrg) throws DAOException;

	/**
	 * Elimina una relación de configuraciones de perfil de digitalización para
	 * unidades organizativas del sistema.
	 * 
	 * @param scanProfileOrgs
	 *            Lista de identificadores de configuraciones de perfil de
	 *            digitalización para unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe
	 *             configuración de perfil de digitalización para unidad
	 *             organizativa, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void removeScanProfileOrgUnits(List<Long> scanProfileOrgs) throws DAOException;

	// Operaciones sobre plugins usados en perfiles de digitalización
	/**
	 * Recupera la relación de plugins utilizados en una configuración de perfil
	 * de digitalización para unidad organizativa determinada.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización
	 *            para unidad organizativa.
	 * @return Lista de plugins utilizados en la configuración de perfil de
	 *         digitalización para unidad organizativa solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no se encuentra
	 *             configuración del perfil de digitalización para la unidad
	 *             organizativa especificada como parámetro de entrada.
	 */
	List<ScanProfileOrgUnitPluginDTO> getScanProfileOrgUnitPluginsByScanProfile(Long scanProfileOrgUnitId) throws DAOException;

	/**
	 * Recupera la relación de uso de plugins en configuraciones de perfil de
	 * digitalización para unidad organizativa donde es empleado un plugin
	 * determinado.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @return Lista de plugins utilizados en la configuración de perfil de
	 *         digitalización para unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin.
	 */
	List<ScanProfileOrgUnitPluginDTO> getScanProfileOrgUnitPluginsByPlugin(Long pluginId) throws DAOException;

	/**
	 * Establece la nueva relación de plugins empleados en una configuración de
	 * un perfil de digitalización en una unidad organizativa.
	 * 
	 * @param plugins
	 *            Lista de plugins empleados en las fases del proceso de
	 *            digitalización implementado por el perfil de digitalización
	 *            configurado para una unidad organizativa.
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización
	 *            para unidad organizativa.
	 * @throws DAOException
	 *             Si se produce algún error al ejecutar la sentencia de BBDD, o
	 *             no existe la configuración de perfil de digitalización para
	 *             unidad organizativa.
	 */
	void setScanProfileOrgUnitPlugins(List<PluginScanProcStageDTO> plugins, Long scanProfileOrgUnitId) throws DAOException;

	/**
	 * Actualiza el estado de activación de una lista de plugins establecidos
	 * para un perfil de digitalización configurado en una unidad organizativa.
	 * 
	 * @param scanProfileOrgUnitPlugins
	 *            Lista de plugins empleados en las fases del proceso de
	 *            digitalización implementado por el perfil de digitalización
	 *            configurado para una unidad organizativa.
	 * @throws DAOException
	 *             Si se produce algún error al ejecutar la sentencia de BBDD.
	 */
	void updateScanProfileOrgUnitPlugins(List<ScanProfileOrgUnitPluginDTO> scanProfileOrgUnitPlugins) throws DAOException;

	// Operaciones sobre especificaciones de documentos usados en perfiles de
	// digitalización
	// y configurados para organismos
	/**
	 * Recupera la relación de especificaciones de documentos utilizadas en una
	 * configuración de perfil de digitalización para unidad organizativa
	 * determinada.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización
	 *            para unidad organizativa.
	 * @return Lista de especificaciones de documentos utilizadas en
	 *         configuraciones de perfil de digitalización para unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no se encuentra
	 *             configuración del perfil de digitalización para la unidad
	 *             organizativa especificada como parámetro de entrada.
	 */
	List<ScanProfileOrgUnitDocSpecDTO> getScanProfileOrgUnitDocSpecsByScanProfile(Long scanProfileOrgUnitId) throws DAOException;

	/**
	 * Recupera las especificaciones de documentos asociadas a una configuración
	 * de perfil de digitalización en unidad organizativa.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Relación de pares clave / valor, en la que cada entrada es
	 *         identificada por una definición de documentos establecida para la
	 *         configuración de perfil de digitalización en unidad organizativa
	 *         especificada como parámetro de entrada, y cuyo valor se
	 *         corresponde con la relación de asociación de la definición de
	 *         documentos a la configuración de perfil en unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getDocSpecificationsByScanProfileOrgUnitId(Long scanProfileOrgUnitId) throws DAOException;

	/**
	 * Recupera las especificaciones de documentos asociadas a una configuración
	 * de perfil de digitalización en unidad organizativa que esten activas
	 * (configuración en perfil y definición de documento).
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Relación de pares clave / valor, en la que cada entrada es
	 *         identificada por una definición de documentos establecida para la
	 *         configuración de perfil de digitalización en unidad organizativa
	 *         especificada como parámetro de entrada, y cuyo valor se
	 *         corresponde con la relación de asociación de la definición de
	 *         documentos a la configuración de perfil en unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getActiveDocSpecificationsByScanProfileOrgUnitId(Long scanProfileOrgUnitId) throws DAOException;

	/**
	 * Recupera la relación de uso de especificaciones de documentos en
	 * configuraciones de perfil de digitalización para unidad organizativa
	 * donde está establecida una especificación de documento determinada.
	 * 
	 * @param docSpecId
	 *            Identificador de especificación de documento.
	 * @return Lista de especificaciones de documentos utilizadas en
	 *         configuraciones de perfil de digitalización para unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin.
	 */
	List<ScanProfileOrgUnitDocSpecDTO> getScanProfileOrgUnitDocSpecsByDocSpec(Long docSpecId) throws DAOException;

	/**
	 * Comprueba si un perfil de digitalización configurado en una unidad
	 * organizativa posee documentos en proceso de digitalización.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Si el perfil de digitalización configurado en una unidad
	 *         organizativa posee documentos en proceso de digitalización,
	 *         retorna true. En caso contrario, false.
	 * @throws DAOException
	 *             Si no existe la configuración de perfil de digitalización en
	 *             unidad organizativa, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	Boolean hasScanProfileOrgUnitDocsInScanProccess(Long scanProfileOrgUnitId) throws DAOException;

	/**
	 * Comprueba si un perfil de digitalización configurado en una unidad
	 * organizativa posee documentos en proceso de digitalización.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @param docSpecId
	 *            Identificador de definición de documentos.
	 * @return Si el perfil de digitalización configurado en una unidad
	 *         organizativa posee documentos en proceso de digitalización,
	 *         retorna true. En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Boolean hasScanProfileOrgUnitDocSpecDocsInScanProccess(Long scanProfileOrgUnitId, Long docSpecId) throws DAOException;

	/**
	 * Establece la nueva relación de definiciones de documentos usadas en una
	 * configuración de un perfil de digitalización en una unidad organizativa.
	 * 
	 * @param docSpecifications
	 *            Mapa en el que cada entrada es identificada por un
	 *            identificador de especificación de documento, y cuyo valor es
	 *            un boolean que indica si la especificación está activa (true).
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización
	 *            para unidad organizativa.
	 * @throws DAOException
	 *             Si se produce algún error al ejecutar la sentencia de BBDD, o
	 *             no existe la configuración de perfil de digitalización para
	 *             unidad organizativa.
	 */
	void setScanProfileOrgUnitDocSpecs(Map<Long, Boolean> docSpecifications, Long scanProfileOrgUnitId) throws DAOException;

}
