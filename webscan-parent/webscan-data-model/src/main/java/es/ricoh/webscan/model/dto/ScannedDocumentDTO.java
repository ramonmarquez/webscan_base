/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ScannedDocumentDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA documento digitalizado.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.model.domain.Batch;
import es.ricoh.webscan.model.domain.ScanProfileOrgUnitDocSpec;
import es.ricoh.webscan.model.domain.SignatureType;
import es.ricoh.webscan.utilities.MimeType;

/**
 * DTO para la entidad JPA documento digitalizado.
 * <p>
 * Clase ScannedDocumentDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class ScannedDocumentDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación del documento.
	 */
	private String name;

	/**
	 * Hash del documento digitalizado.
	 */
	private byte[ ] hash = null;

	/**
	 * Contenido del documento digitalizado.
	 */
	private byte[ ] content = null;

	/**
	 * Firma del documento digitalizado.
	 */
	private byte[ ] signature = null;

	/**
	 * Texto obtenido del documento digitalizado.
	 */
	private byte[ ] ocr;

	/**
	 * Instante en el que se inicio el proceso de digitalización del documento.
	 */
	private Date digProcStartDate;

	/**
	 * Instante en el que finalizó el proceso de digitalización del documento.
	 */
	private Date digProcEndDate;

	/**
	 * Indica si el documento ha sido verificado y validado por el personal de
	 * digitalización.
	 */
	private Boolean checked = Boolean.FALSE;

	/**
	 * Fecha en la que ha sido verificado y validado el documento por el
	 * personal de digitalización.
	 */
	private Date checkDate;

	/**
	 * Identificador de lote de documentos.
	 */
	private Batch batchId;

	/**
	 * Número de orden del documento en el lote, comenzando por 1.
	 */
	private Integer batchOrderNum;

	/**
	 * Identificador de reserva de trabajo de digitalización o carátula asociado
	 * a la digitalización del documento.
	 */
	private String batchReqId;

	/**
	 * Tipo MIME de la imagen capturada del documento.
	 */
	private MimeType mimeType;

	/**
	 * Tipo o formato de firma electrónica del documento digitalizado.
	 */
	private SignatureType signatureType;

	/**
	 * Nombre de usuario del usuario que inicia el proceso de digitalización del
	 * documento.
	 */
	private String username;

	/**
	 * Lista de identificadores de unidades orgánicas a las que pertenece el
	 * usuario que inicia el proceso de digitalización del documento (separados
	 * por coma).
	 */
	private String functionalOrgs;

	/**
	 * Identificador de la traza de auditoria asociada al proceso de
	 * digitalización del documento.
	 */
	private Long auditOpId;

	/**
	 * Configuración de especificación de documento, perfil de digitalización y
	 * unidad organizativa empleada en el proceso de digitalización.
	 */
	private ScanProfileOrgUnitDocSpec orgScanProfileDocSpec;

	/**
	 * Constructor sin argumentos.
	 */
	public ScannedDocumentDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación del documento.
	 * 
	 * @return la denominación del documento.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación del documento.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación del documento.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el hash del documento digitalizado.
	 * 
	 * @return el hash del documento digitalizado.
	 */
	public byte[ ] getHash() {
		return hash;
	}

	/**
	 * Establece un nuevo hash del documento digitalizado.
	 * 
	 * @param aHash
	 *            nuevo hash del documento digitalizado.
	 */
	public void setHash(byte[ ] aHash) {
		this.hash = aHash;
	}

	/**
	 * Obtiene el contenido del documento digitalizado.
	 * 
	 * @return el contenido del documento digitalizado.
	 */
	public byte[ ] getContent() {
		return content;
	}

	/**
	 * Establece un nuevo contenido del documento digitalizado.
	 * 
	 * @param aContent
	 *            nuevo contenido del documento digitalizado.
	 */
	public void setContent(byte[ ] aContent) {
		this.content = aContent;
	}

	/**
	 * Obtiene el tipo MIME de la imagen capturada del documento.
	 * 
	 * @return el tipo MIME de la imagen capturada del documento.
	 */
	public MimeType getMimeType() {
		return mimeType;
	}

	/**
	 * Establece un nuevo tipo MIME de la imagen capturada del documento.
	 * 
	 * @param aMimeType
	 *            nuevo valor del tipo MIME de la imagen capturada del
	 *            documento.
	 */
	public void setMimeType(MimeType aMimeType) {
		this.mimeType = aMimeType;
	}

	/**
	 * Obtiene la firma del documento digitalizado.
	 * 
	 * @return la firma del documento digitalizado.
	 */
	public byte[ ] getSignature() {
		return signature;
	}

	/**
	 * Establece un nueva firma del documento digitalizado.
	 * 
	 * @param aSignature
	 *            nueva firma del documento digitalizado.
	 */
	public void setSignature(byte[ ] aSignature) {
		this.signature = aSignature;
	}

	/**
	 * Obtiene el texto extraído del documento digitalizado.
	 * 
	 * @return el texto extraído del documento digitalizado.
	 */
	public byte[ ] getOcr() {
		return ocr;
	}

	/**
	 * Establece un nuevo texto extraído del documento digitalizado.
	 * 
	 * @param aOcr
	 *            nuevo texto extraído del documento digitalizado.
	 */
	public void setOcr(byte[ ] aOcr) {
		this.ocr = aOcr;
	}

	/**
	 * Obtiene el instante en el que se inicio el proceso de digitalización del
	 * documento.
	 * 
	 * @return el instante en el que se inicio el proceso de digitalización del
	 *         documento.
	 */
	public Date getDigProcStartDate() {
		return digProcStartDate;
	}

	/**
	 * Establece un nuevo instante en el que se inicio el proceso de
	 * digitalización del documento.
	 * 
	 * @param aDigProcStartDate
	 *            nuevo instante en el que se inicio el proceso de
	 *            digitalización del documento.
	 */
	public void setDigProcStartDate(Date aDigProcStartDate) {
		this.digProcStartDate = aDigProcStartDate;
	}

	/**
	 * Obtiene el instante en el que se finalizó el proceso de digitalización
	 * del documento.
	 * 
	 * @return el instante en el que se finalizó el proceso de digitalización
	 *         del documento.
	 */
	public Date getDigProcEndDate() {
		return digProcEndDate;
	}

	/**
	 * Establece un nuevo instante en el que se finalizó el proceso de
	 * digitalización del documento.
	 * 
	 * @param aDigProcEndDate
	 *            nuevo instante en el que se finalizó el proceso de
	 *            digitalización del documento.
	 */
	public void setDigProcEndDate(Date aDigProcEndDate) {
		this.digProcEndDate = aDigProcEndDate;
	}

	/**
	 * Obtiene el indicador que informa si el documento ha sido verificado y
	 * validado por el personal de digitalización.
	 * 
	 * @return indicador que informa si el documento ha sido verificado y
	 *         validado por el personal de digitalización.
	 */
	public Boolean getChecked() {
		return checked;
	}

	/**
	 * Establece el indicador que informa si el documento ha sido verificado y
	 * validado por el personal de digitalización.
	 * 
	 * @param checkedParam
	 *            indicador que informa si el documento ha sido verificado y
	 *            validado por el personal de digitalización.
	 */
	public void setChecked(Boolean checkedParam) {
		this.checked = checkedParam;
	}

	/**
	 * Obtiene la fecha en la que ha sido verificado y validado el documento por
	 * el personal de digitalización.
	 * 
	 * @return la fecha en la que ha sido verificado y validado el documento por
	 *         el personal de digitalización.
	 */
	public Date getCheckDate() {
		return checkDate;
	}

	/**
	 * Establece la fecha en la que ha sido verificado y validado el documento
	 * por el personal de digitalización.
	 * 
	 * @param aCheckDate
	 *            fecha en la que ha sido verificado y validado el documento por
	 *            el personal de digitalización.
	 */
	public void setCheckDate(Date aCheckDate) {
		this.checkDate = aCheckDate;
	}

	/**
	 * Obtiene el identificador de lote de documentos al que pertenece el
	 * documento digitalizado.
	 * 
	 * @return el identificador de lote de documentos al que pertenece el
	 *         documento digitalizado.
	 */
	public Batch getBatchId() {
		return batchId;
	}

	/**
	 * Establece un nuevo identificador de lote de documentos al que pertenece
	 * el documento digitalizado.
	 * 
	 * @param aBatchId
	 *            nuevo identificador de lote de documentos al que pertenece el
	 *            documento digitalizado.
	 */
	public void setBatchId(Batch aBatchId) {
		this.batchId = aBatchId;
	}

	/**
	 * Obtiene el número de orden del documento en el lote, comenzando por 1.
	 * 
	 * @return el número de orden del documento en el lote, comenzando por 1.
	 */
	public Integer getBatchOrderNum() {
		return batchOrderNum;
	}

	/**
	 * Establece un nuevo número de orden del documento en el lote, comenzando
	 * por 1.
	 * 
	 * @param aBatchOrderNum
	 *            nuevo número de orden del documento en el lote, comenzando por
	 *            1.
	 */
	public void setBatchOrderNum(Integer aBatchOrderNum) {
		this.batchOrderNum = aBatchOrderNum;
	}

	/**
	 * Obtiene el identificador de reserva de trabajo de digitalización o
	 * carátula asociado a la digitalización del documento.
	 * 
	 * @return el identificador de reserva de trabajo de digitalización o
	 *         carátula asociado a la digitalización del documento.
	 */
	public String getBatchReqId() {
		return batchReqId;
	}

	/**
	 * Establece el identificador de reserva de trabajo de digitalización o
	 * carátula asociado a la digitalización del documento.
	 * 
	 * @param aBatchReqId
	 *            Identificador de reserva de trabajo de digitalización o
	 *            carátula asociado a la digitalización del documento.
	 */
	public void setBatchReqId(String aBatchReqId) {
		this.batchReqId = aBatchReqId;
	}

	/**
	 * Obtiene el tipo o formato de la firma electrónica del documento.
	 * 
	 * @return el tipo o formato de la firma electrónica del documento.
	 */
	public SignatureType getSignatureType() {
		return signatureType;
	}

	/**
	 * Establece un nuevo tipo o formato de la firma electrónica del documento.
	 * 
	 * @param aSignatureType
	 *            nuevo tipo o formato de la firma electrónica del documento.
	 */
	public void setSignatureType(SignatureType aSignatureType) {
		this.signatureType = aSignatureType;
	}

	/**
	 * Obtiene el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @return el nombre de usuario del usuario que inicia el proceso de
	 *         digitalización del documento.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @param anUsername
	 *            nuevo nombre de usuario del usuario que inicia el proceso de
	 *            digitalización del documento.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento.
	 * 
	 * @return lista de identificadores de unidades orgánicas a las que
	 *         pertenece el usuario que inicia el proceso de digitalización del
	 *         documento.
	 */
	public String getFunctionalOrgs() {
		return functionalOrgs;
	}

	/**
	 * Establece la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento.
	 * 
	 * @param anyFunctionalOrgs
	 *            nueva lista de identificadores de unidades orgánicas a las que
	 *            pertenece el usuario que inicia el proceso de digitalización
	 *            del documento.
	 */
	public void setFunctionalOrgs(String anyFunctionalOrgs) {
		this.functionalOrgs = anyFunctionalOrgs;
	}

	/**
	 * Obtiene el identificador de la traza de auditoria asociada al proceso de
	 * digitalización del documento.
	 * 
	 * @return el identificador de la traza de auditoria asociada al proceso de
	 *         digitalización del documento.
	 */
	public Long getAuditOpId() {
		return auditOpId;
	}

	/**
	 * Establece el identificador de la traza de auditoria asociada al proceso
	 * de digitalización del documento.
	 * 
	 * @param anAuditOpId
	 *            nuevo identificador de traza de auditoria asociada al proceso
	 *            de digitalización del documento.
	 */
	public void setAuditOpId(Long anAuditOpId) {
		this.auditOpId = anAuditOpId;
	}

	/**
	 * Obtiene la configuración de especificación de documento, perfil de
	 * digitalización y unidad organizativa empleada en el proceso de
	 * digitalización del documento.
	 * 
	 * @return la configuración de especificación de documento, perfil de
	 *         digitalización y unidad organizativa empleada en el proceso de
	 *         digitalización del documento.
	 */
	public ScanProfileOrgUnitDocSpec getOrgScanProfileDocSpec() {
		return orgScanProfileDocSpec;
	}

	/**
	 * Establece la configuración de especificación de documento, perfil de
	 * digitalización y unidad organizativa empleada en el proceso de
	 * digitalización del documento.
	 * 
	 * @param anOrgScanProfileDocSpec
	 *            nueva configuración de especificación de documento, perfil de
	 *            digitalización y unidad organizativa empleada en el proceso de
	 *            digitalización del documento.
	 */
	void setOrgScanProfileDocSpec(ScanProfileOrgUnitDocSpec anOrgScanProfileDocSpec) {
		this.orgScanProfileDocSpec = anOrgScanProfileDocSpec;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScannedDocumentDTO other = (ScannedDocumentDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
