/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ScannedDocumentDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA documento digitalizado.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO para la entidad JPA documento digitalizado.
 * <p>
 * Clase BatchDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */
public final class BatchDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Identificador normalizado del proceso de digitalización.
	 */
	private String batchNameId;

	/**
	 * Identificador de reserva de trabajo de digitalización o carátula asociado a la digitalización del documento.
	 */
	private String batchReqId;
	
	/**
	 * Fecha de inicio del proceso de digitalización..
	 */
	private Date startDate;

	/**
	 * Fecha de inicio del proceso de digitalización..
	 */
	private Date updateDate;

	/**
	 * Nombre de usuario del usuario que inicia el proceso de digitalización del
	 * documento.
	 */
	private String username;

	/**
	 * Lista de identificadores de unidades orgánicas a las que pertenece el
	 * usuario que inicia el proceso de digitalización del documento (separados
	 * por coma).
	 */
	private String functionalOrgs;

	/**
	 * Configuración de especificación de documento, perfil de digitalización y
	 * unidad organizativa empleada en el proceso de digitalización.
	 */
	private Long orgScanProfileDocSpec;
	
	/**
	 * Constructor sin argumentos.
	 */
	public BatchDTO() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aBatchId
	 *            Identificador del proceso de digitalización.
	 * @param aBatchNameId
	 *            Denominación de la clave normalizada del proceso de digitalización.
	 * @param aBatchReqId
	 *            Denominación de la clave normalizada del proceso de digitalización (carátula).
	 * @param aBatchStartDate
	 *            Instante en el que se inicio el proceso de digitalización.
	 * @param aDocUsername
	 *            Nombre de usuario del usuario que inicia el proceso de
	 *            digitalización.
	 * @param anyDocFunctionalOrgs
	 *            Lista de identificadores de unidades orgánicas a las que
	 *            pertenece el usuario que inicia el proceso de digitalización
	 *            (separados por coma).
	 */
	public BatchDTO(Long aBatchId, String aBatchNameId, String aBatchReqId,Date aBatchStartDate, String aBatchUsername, String aBatchFunctionalOrgs) {
		super();
		this.id = aBatchId;
		this.batchNameId = aBatchNameId;
		this.batchReqId = aBatchReqId;
		this.startDate = aBatchStartDate;
		this.username = aBatchUsername;
		this.functionalOrgs = aBatchFunctionalOrgs;
		
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el identificador normalizado del proceso de digitalización.
	 * 
	 * @return el identificador normalizado del proceso de digitalización.
	 */
	public String getBatchNameId() {
		return batchNameId;
	}

	/**
	 * Establece el identificador normalizado del proceso de digitalización.
	 * 
	 * @param aBatchNameId
	 *            nuevo valor para el identificador normalizado del proceso de digitalización.
	 */
	public void setBatchNameId(String aBatchNameId) {
		this.batchNameId = aBatchNameId;
	}

	/**
	 * Obtiene el identificador de carátula asociado al 
	 * proceso de digitalización de documentos.
	 * 
	 * @return batchReqId el identificador de carátula asociado al 
	 * proceso de digitalización de documentos.
	 */
	public String getBatchReqId() {
		return batchReqId;
	}

	/**
	 * Establece el identificador de carátula asociado al 
	 * proceso de digitalización de documentos.
	 * 
	 * @param batchReqId el identificador de carátula asociado al 
	 * proceso de digitalización de documentos.
	 */
	public void setBatchReqId(String batchReqId) {
		this.batchReqId = batchReqId;
	}

	/**
	 * Obtiene la fecha de inicio del proceso de digitalización.
	 * 
	 * @return la fecha de inicio del proceso de digitalización.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Establece una nueva fecha de inicio del proceso de digitalización.
	 * 
	 * @param aStartDate
	 *            nueva fecha de inicio del proceso de digitalización.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}
	/**
	 * Obtiene la fecha de inicio del proceso de digitalización.
	 * 
	 * @return la fecha de inicio del proceso de digitalización.
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Establece una nueva fecha de inicio del proceso de digitalización.
	 * 
	 * @param aStartDate
	 *            nueva fecha de inicio del proceso de digitalización.
	 */
	public void setUpdateDate(Date aStartDate) {
		this.updateDate = aStartDate;
	}

	/**
	 * Obtiene el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @return el nombre de usuario del usuario que inicia el proceso de
	 *         digitalización del documento.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @param anUsername
	 *            nuevo nombre de usuario del usuario que inicia el proceso de
	 *            digitalización del documento.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento.
	 * 
	 * @return lista de identificadores de unidades orgánicas a las que
	 *         pertenece el usuario que inicia el proceso de digitalización del
	 *         documento.
	 */
	public String getFunctionalOrgs() {
		return functionalOrgs;
	}

	/**
	 * Establece la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento.
	 * 
	 * @param anyFunctionalOrgs
	 *            nueva lista de identificadores de unidades orgánicas a las que
	 *            pertenece el usuario que inicia el proceso de digitalización
	 *            del documento.
	 */
	public void setFunctionalOrgs(String anyFunctionalOrgs) {
		this.functionalOrgs = anyFunctionalOrgs;
	}
	/**
	 * Obtiene la configuración de especificación de documento, perfil de
	 * digitalización y unidad organizativa empleada en el proceso de
	 * digitalización del documento.
	 * 
	 * @return la configuración de especificación de documento, perfil de
	 *         digitalización y unidad organizativa empleada en el proceso de
	 *         digitalización del documento.
	 */
	public Long getOrgScanProfileDocSpec() {
		return orgScanProfileDocSpec;
	}

	/**
	 * Establece la configuración de especificación de documento, perfil de
	 * digitalización y unidad organizativa empleada en el proceso de
	 * digitalización del documento.
	 * 
	 * @param anOrgScanProfileDocSpec
	 *            nueva configuración de especificación de documento, perfil de
	 *            digitalización y unidad organizativa empleada en el proceso de
	 *            digitalización del documento.
	 */
	void setOrgScanProfileDocSpec(Long anOrgScanProfileDocSpec) {
		this.orgScanProfileDocSpec = anOrgScanProfileDocSpec;
	}

}