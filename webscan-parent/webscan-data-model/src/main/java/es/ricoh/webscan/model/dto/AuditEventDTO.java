/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.AuditEventDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA evento de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;

/**
 * DTO para la entidad JPA evento de auditoría.
 * <p>
 * Clase AuditEventDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class AuditEventDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación de la entidad.
	 */
	private AuditEventName eventName;

	/**
	 * Fecha en la que se produce el evento.
	 */
	private Date eventDate;

	/**
	 * Usuario que ejecuta o es objeto del evento.
	 */
	private String eventUser;

	/**
	 * Resultado de ejecución del evento.
	 */
	private AuditOpEventResult eventResult;

	/**
	 * Información adicional del evento.
	 */
	private String additionalInfo;

	/**
	 * Identificador de la traza de auditoría en la que se produce el evento.
	 */
	private Long auditOperationId;

	/**
	 * Constructor sin argumentos.
	 */
	public AuditEventDTO() {
		super();
	}

	/**
	 * Obtiene el identificador del evento.
	 * 
	 * @return el identificador del evento.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador del evento.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador del evento.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación del evento.
	 * 
	 * @return la denominación del evento.
	 */
	public AuditEventName getEventName() {
		return eventName;
	}

	/**
	 * Establece un nuevo valor para la denominación del evento.
	 * 
	 * @param anEventName
	 *            nuevo valor para la denominación del evento.
	 */
	public void setEventName(AuditEventName anEventName) {
		this.eventName = anEventName;
	}

	/**
	 * Obtiene la fecha de ejecución del evento.
	 * 
	 * @return la fecha de ejecución del evento.
	 */
	public Date getEventDate() {
		return eventDate;
	}

	/**
	 * Establece un nuevo valor para la fecha de ejecución del evento.
	 * 
	 * @param anEventDate
	 *            nuevo valor para la fecha de ejecución del evento.
	 */
	public void setEventDate(Date anEventDate) {
		this.eventDate = anEventDate;
	}

	/**
	 * Obtiene el usuario de ejecución u objeto del evento.
	 * 
	 * @return el usuario de ejecución u objeto del evento.
	 */
	public String getEventUser() {
		return eventUser;
	}

	/**
	 * Establece un nuevo valor para el usuario de ejecución u objeto del
	 * evento.
	 * 
	 * @param anEventUser
	 *            nuevo valor para el usuario de ejecución u objeto del evento.
	 */
	public void setEventUser(String anEventUser) {
		this.eventUser = anEventUser;
	}

	/**
	 * Obtiene el resultado de ejecución del evento.
	 * 
	 * @return el resultado de ejecución del evento.
	 */
	public AuditOpEventResult getEventResult() {
		return eventResult;
	}

	/**
	 * Establece un nuevo valor para el resultado de ejecución del evento.
	 * 
	 * @param anEventResult
	 *            nuevo valor para el resultado de ejecución del evento.
	 */
	public void setEventResult(AuditOpEventResult anEventResult) {
		this.eventResult = anEventResult;
	}

	/**
	 * Obtiene la información adicional del evento.
	 * 
	 * @return la información adicional del evento.
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * Establece un nuevo valor para la información adicional del evento.
	 * 
	 * @param anyAdditionalInfo
	 *            nuevo valor para la información adicional del evento.
	 */
	public void setAdditionalInfo(String anyAdditionalInfo) {
		this.additionalInfo = anyAdditionalInfo;
	}

	/**
	 * Obtiene el identificador de la traza de auditoría en la que se produce el
	 * evento.
	 * 
	 * @return el identificador de la traza de auditoría en la que se produce el
	 *         evento.
	 */
	public Long getAuditOperationId() {
		return auditOperationId;
	}

	/**
	 * Establece un nuevo identificador traza de auditoría en la que se produce
	 * el evento.
	 * 
	 * @param anAuditOperationId
	 *            Nuevo identificador de traza de auditoría en la que se produce
	 *            el evento.
	 */
	void setAuditOperationId(Long anAuditOperationId) {
		this.auditOperationId = anAuditOperationId;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditEventDTO other = (AuditEventDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
