/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScanProfileOrgUnitPlugin.java.</p>
 * <b>Descripción:</b><p> Entidad JPA plugin empleando en fase de proceso de digitalización para configuración de perfil de digitalización en unidad organizativa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA plugin empleando en fase de proceso de digitalización para
 * configuración de perfil de digitalización en unidad organizativa.
 * <p>
 * Clase ScanProfileOrgUnitPlugin.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "SPROF_ORG_PLUGIN", uniqueConstraints = { @UniqueConstraint(columnNames = { "SPROF_ORG_ID", "PLUGIN_ID" }) })
public class ScanProfileOrgUnitPlugin implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "SPROF_ORG_PLUGIN_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SPROF_ORG_PLUGIN_ID_SEQ", sequenceName = "SPROF_ORG_PLUGIN_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo ACTIVE de la entidad.
	 */
	@Column(name = "ACTIVE")
	private Boolean active = Boolean.TRUE;

	/**
	 * plugin configurado.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.Plugin.class)
	@JoinColumn(name = "PLUGIN_ID")
	private Plugin plugin;

	/**
	 * Configuración de perfil de digitalización en unidad organizativa.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnit.class)
	@JoinColumn(name = "SPROF_ORG_ID")
	private ScanProfileOrgUnit orgScanProfile;

	/**
	 * Fase del proceso de digitalización en la que es empleado el plugin.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProcessStage.class)
	@JoinColumn(name = "DPROC_STAGE_ID")
	private ScanProcessStage scanProcessStage;

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfileOrgUnitPlugin() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo ACTIVE de la entidad.
	 * 
	 * @return el atributo ACTIVE de la entidad.
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Establece un nuevo valor para el atributo ACTIVE de la entidad.
	 * 
	 * @param activeParam
	 *            nuevo valor para el atributo ACTIVE de la entidad.
	 */
	public void setActive(Boolean activeParam) {
		this.active = activeParam;
	}

	/**
	 * Obtiene la entidad plugin configurada.
	 * 
	 * @return la entidad plugin configurada.
	 */
	public Plugin getPlugin() {
		return plugin;
	}

	/**
	 * Establece una nueva entidad plugin.
	 * 
	 * @param aPlugin
	 *            Nueva entidad plugin.
	 */
	public void setPlugin(Plugin aPlugin) {
		this.plugin = aPlugin;
	}

	/**
	 * Obtiene la entidad configuración de perfil de digitalización en unidad
	 * organizativa.
	 * 
	 * @return la entidad configuración de perfil de digitalización en unidad
	 *         organizativa.
	 */
	public ScanProfileOrgUnit getOrgScanProfile() {
		return orgScanProfile;
	}

	/**
	 * Establece una nueva entidad configuración de perfil de digitalización en
	 * unidad organizativa.
	 * 
	 * @param anOrgScanProfile
	 *            Nueva entidad configuración de perfil de digitalización en
	 *            unidad organizativa.
	 */
	public void setOrgScanProfile(ScanProfileOrgUnit anOrgScanProfile) {
		this.orgScanProfile = anOrgScanProfile;
	}

	/**
	 * Obtiene la fase del proceso de digitalización en la que es empleado el
	 * plugin.
	 * 
	 * @return la fase del proceso de digitalización en la que es empleado el
	 *         plugin.
	 */
	public ScanProcessStage getScanProcessStage() {
		return scanProcessStage;
	}

	/**
	 * Establece una nueva entidad fase del proceso de digitalización.
	 * 
	 * @param aScanProcessStage
	 *            Nueva entidad fase del proceso de digitalización.
	 */
	public void setScanProcessStage(ScanProcessStage aScanProcessStage) {
		this.scanProcessStage = aScanProcessStage;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orgScanProfile == null) ? 0 : orgScanProfile.hashCode());
		result = prime * result + ((plugin == null) ? 0 : plugin.hashCode());
		result = prime * result + ((scanProcessStage == null) ? 0 : scanProcessStage.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfileOrgUnitPlugin other = (ScanProfileOrgUnitPlugin) obj;
		if (orgScanProfile == null) {
			if (other.orgScanProfile != null)
				return false;
		} else if (!orgScanProfile.equals(other.orgScanProfile))
			return false;
		if (plugin == null) {
			if (other.plugin != null)
				return false;
		} else if (!plugin.equals(other.plugin))
			return false;
		if (scanProcessStage == null) {
			if (other.scanProcessStage != null)
				return false;
		} else if (!scanProcessStage.equals(other.scanProcessStage))
			return false;
		return true;
	}

}
