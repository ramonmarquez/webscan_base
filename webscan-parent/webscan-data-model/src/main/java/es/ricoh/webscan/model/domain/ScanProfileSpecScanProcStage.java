/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScanProfileSpecScanProcStage.java.</p>
 * <b>Descripción:</b><p> Entidad JPA configuración de fase del proceso de digitalización para definición de perfil de digitalización.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA configuración de fase del proceso de digitalización para
 * definición de perfil de digitalización.
 * <p>
 * Clase ScanProfileSpecScanProcStage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "SPROF_SPEC_DPROC_STAGE", uniqueConstraints = { @UniqueConstraint(columnNames = { "DPROC_STAGE_ID", "SPROFILE_SPEC_ID" }) })
public class ScanProfileSpecScanProcStage implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "SPROF_SPEC_DPSTG_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SPROF_SPEC_DPSTG_ID_SEQ", sequenceName = "SPROF_SPEC_DPSTG_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo MANDATORY de la entidad.
	 */
	@Column(name = "EXEC_ORDER")
	private Integer order;

	/**
	 * Atributo MANDATORY de la entidad.
	 */
	@Column(name = "MANDATORY")
	private Boolean mandatory = Boolean.FALSE;

	/**
	 * Atributo RENEWABLE de la entidad.
	 */
	@Column(name = "RENEWABLE")
	private Boolean renewable = Boolean.FALSE;

	/**
	 * Fase del proceso de digitalización.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProcessStage.class)
	@JoinColumn(name = "DPROC_STAGE_ID")
	private ScanProcessStage scanProcessStage;

	/**
	 * Definición de perfil de digitalización.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProfileSpecification.class)
	@JoinColumn(name = "SPROFILE_SPEC_ID")
	private ScanProfileSpecification scanProfileSpecification;

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfileSpecScanProcStage() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo EXEC_ORDER de la entidad.
	 * 
	 * @return el atributo EXEC_ORDER de la entidad.
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * Establece un nuevo valor para el atributo EXEC_ORDER de la entidad.
	 * 
	 * @param anOrder
	 *            nuevo valor para el atributo EXEC_ORDER de la entidad.
	 */
	public void setOrder(Integer anOrder) {
		this.order = anOrder;
	}

	/**
	 * Obtiene el atributo MANDATORY de la entidad.
	 * 
	 * @return el atributo MANDATORY de la entidad.
	 */
	public Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * Establece un nuevo valor para el atributo MANDATORY de la entidad.
	 * 
	 * @param mandatoryParam
	 *            nuevo valor para el atributo MANDATORY de la entidad.
	 */
	public void setMandatory(Boolean mandatoryParam) {
		this.mandatory = mandatoryParam;
	}

	/**
	 * Obtiene el atributo RENEWABLE de la entidad.
	 * 
	 * @return el atributo RENEWABLE de la entidad.
	 */
	public Boolean getRenewable() {
		return renewable;
	}

	/**
	 * Establece un nuevo valor para el atributo RENEWABLE de la entidad.
	 * 
	 * @param isRenewable
	 *            nuevo valor para el atributo RENEWABLE de la entidad.
	 */
	public void setRenewable(Boolean isRenewable) {
		this.renewable = isRenewable;
	}

	/**
	 * Obtiene la entidad fase del proceso de digitalización.
	 * 
	 * @return la entidad fase del proceso de digitalización.
	 */
	public ScanProcessStage getScanProcessStage() {
		return scanProcessStage;
	}

	/**
	 * Establece la entidad fase del proceso de digitalización.
	 * 
	 * @param aScanProcessStage
	 *            Nueva entidad fase del proceso de digitalización.
	 */
	public void setScanProcessStage(ScanProcessStage aScanProcessStage) {
		this.scanProcessStage = aScanProcessStage;
	}

	/**
	 * Obtiene la entidad definición de perfil de digitalización.
	 * 
	 * @return la entidad definición de perfil de digitalización.
	 */
	public ScanProfileSpecification getScanProfileSpecification() {
		return scanProfileSpecification;
	}

	/**
	 * Establece una nueva entidad definición de perfil de digitalización.
	 * 
	 * @param aScanProfileSpecification
	 *            Nueva entidad definición de perfil de digitalización.
	 */
	public void setScanProfileSpecification(ScanProfileSpecification aScanProfileSpecification) {
		this.scanProfileSpecification = aScanProfileSpecification;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((scanProcessStage == null) ? 0 : scanProcessStage.hashCode());
		result = prime * result + ((scanProfileSpecification == null) ? 0 : scanProfileSpecification.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfileSpecScanProcStage other = (ScanProfileSpecScanProcStage) obj;
		if (scanProcessStage == null) {
			if (other.scanProcessStage != null)
				return false;
		} else if (!scanProcessStage.equals(other.scanProcessStage))
			return false;
		if (scanProfileSpecification == null) {
			if (other.scanProfileSpecification != null)
				return false;
		} else if (!scanProfileSpecification.equals(other.scanProfileSpecification))
			return false;
		return true;
	}

}
