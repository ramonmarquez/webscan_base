/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.AuditDAO.java.</p>
 * <b>Descripción:</b><p> Interfaz que define la operaciones de la capa DAO sobre las entidades traza y evento de auditoría, pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao;

import java.util.Date;
import java.util.List;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.dto.AuditReportDTO;
import es.ricoh.webscan.model.dto.AuditScannedDocDTO;

/**
 * Interfaz que define la operaciones de la capa DAO sobre las entidades traza y
 * evento de auditoría, pertenecientes al modelo de datos base de Webscan.
 * <p>
 * Clase AuditDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface AuditDAO {

	// Operaciones sobre auditoria de operaciones
	/**
	 * Recupera el número total de trazas de auditoría registradas en el
	 * sistema.
	 * 
	 * @return número total de trazas de auditoría registradas en el sistema.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	Long countAuditOperations() throws DAOException;

	/**
	 * Recupera el número total de trazas de auditoría registradas en el sistema
	 * que cumplen una serie de criterios de búsqueda o filtrado.
	 * 
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @return número total de trazas de auditoría registradas en el sistema que
	 *         cumplen una serie de criterios de búsqueda o filtrado.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	Long countAuditOperations(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate) throws DAOException;

	/**
	 * Obtiene el listado de trazas de auditoría registradas en el sistema,
	 * permitiendo la paginación, la ordenación del resultado y el filtrado de
	 * los mismos mediante los siguientes datos: - Denominaciones de las trazas
	 * de auditoría. - Estado de las trazas de auditoría. - Periodo de
	 * establecimiento del estado de las trazas de auditoría. - Denominaciones
	 * de los eventos de las trazas de auditoría. - Estado de los eventos de de
	 * las trazas de auditoría. - Periodo de establecimiento del estado de los
	 * eventos de las trazas de auditoría. Todos los filtros son opcionales.
	 * 
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param auditOpOrderByColumns
	 *            clausulas de ordenación sobre trazas de auditoría.
	 * @param auditEventOrderByColumns
	 *            clausulas de ordenación sobre eventos de auditoría.
	 * @return listado de trazas de auditoría.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<AuditReportDTO> listAuditOperations(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> auditOpOrderByColumns, List<EntityOrderByClause> auditEventOrderByColumns) throws DAOException;

	/**
	 * Obtiene el listado de trazas de auditoría registradas en el sistema,
	 * permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param auditOpOrderByColumns
	 *            clausulas de ordenación sobre trazas de auditoría.
	 * @param auditEventOrderByColumns
	 *            clausulas de ordenación sobre eventos de auditoría.
	 * @return listado de trazas de auditoría.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<AuditOperationDTO> listAuditOperations(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> auditOpOrderByColumns, List<EntityOrderByClause> auditEventOrderByColumns) throws DAOException;

	/**
	 * Obtiene el listado de trazas de auditoría registradas en el sistema que
	 * poseen algún evento que incluye un texto como parte de su información
	 * adicional.
	 * 
	 * @param additionalInfo
	 *            Texto que forma parte de la información adicional de los
	 *            eventos por el que serán filtradas las trazas de auditoría.
	 * @return listado de de trazas de auditoría. Lista vacía si no existen
	 *         resultados.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<AuditOperationDTO> getAuditOpsByAdditionalInfoEvents(String additionalInfo) throws DAOException;

	/**
	 * Recupera una traza de auditoría que posea un evento ejecutado por usuario
	 * dado, cuya información adicional incluya un texto determinado.
	 * 
	 * @param user
	 *            Nombre de usuario.
	 * @param additionalInfo
	 *            Texto que forma parte de la información adicional de los
	 *            eventos por el que serán filtradas las trazas de auditoría.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	AuditOperationDTO getAuditOpByUserAndAdditionalInfoEvents(String user, String additionalInfo) throws DAOException;

	/**
	 * Recupera una traza de auditoría a partir de su identificador.
	 * 
	 * @param auditOperationId
	 *            Identificador de traza de auditoría.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	AuditOperationDTO getAuditOperation(Long auditOperationId) throws DAOException;

	/**
	 * Recupera la traza de auditoría más reciente con una determinada
	 * denominación, y que posea un evento ejecutado por usuario dado.
	 * 
	 * @param auditOperationName
	 *            Denominación de traza de auditoría.
	 * @param user
	 *            Nombre de usuario.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	AuditOperationDTO getLastAuditOperationByOpNameAndUser(AuditOperationName auditOperationName, String user) throws DAOException;

	/**
	 * Recupera la traza de auditoría más reciente con una determinada
	 * denominación y estado, y que posea un evento ejecutado por usuario dado.
	 * 
	 * @param auditOperationName
	 *            Denominación de traza de auditoría.
	 * @param auditOperationStatus
	 *            Estado d ela traza de auditoría.
	 * @param user
	 *            Nombre de usuario.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	AuditOperationDTO getLastAuditOperationByOpNameStatusAndUser(AuditOperationName auditOperationName, AuditOperationStatus auditOperationStatus, String user) throws DAOException;

	/**
	 * Crea una nueva traza de auditoría en el sistema.
	 * 
	 * @param auditOperation
	 *            Nueva traza de auditoría.
	 * @return El identificador de la traza de auditoría creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createAuditOperation(AuditOperationDTO auditOperation) throws DAOException;

	/**
	 * Actualiza una traza de auditoría en BBDD. Los datos que pueden ser
	 * actualizados son el estado de la traza y la fecha de establecimiento del
	 * estado.
	 * 
	 * @param auditOperation
	 *            traza de auditoría.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	void updateAuditOperation(AuditOperationDTO auditOperation) throws DAOException;

	// Operaciones sobre eventos de auditoria
	/**
	 * Recupera los eventos de una traza de auditoría.
	 * 
	 * @param auditOperationId
	 *            Identificador de traza de auditoría.
	 * @return Lista de eventos de la traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de auditoría, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	List<AuditEventDTO> getAuditEventsByOperation(Long auditOperationId) throws DAOException;

	/**
	 * Recupera un evento de auditoría a partir de su identificador.
	 * 
	 * @param auditEventId
	 *            Identificador de evento de auditoría.
	 * @return El evento de auditoría solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe evento
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	AuditEventDTO getAuditEvent(Long auditEventId) throws DAOException;

	/**
	 * Añade una relación de eventos a una traza de auditoría.
	 * 
	 * @param auditEvents
	 *            Lista de eventos de auditoría
	 * @param auditOperationId
	 *            Identificador de traza de auditoría.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no exista la
	 *             traza de auditoría, o se produce algún error en la ejecución
	 *             de las sentencias de BBDD.
	 */
	void addAuditEvents(List<AuditEventDTO> auditEvents, Long auditOperationId) throws DAOException;

	/**
	 * Crea el histórico de un documento digitalizado en el Sistema.
	 * 
	 * @param auditScannedDoc
	 *            Información de histórico de un documento digitalizado.
	 * @param auditOperationId
	 *            Identificador de la traza de auditoría asociada al proceso de
	 *            digitalización del documento.
	 * @return El identificador del histórico de un documento digitalizado
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createAuditScannedDoc(AuditScannedDocDTO auditScannedDoc, Long auditOperationId) throws DAOException;
}
