/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.BatchDAO.java.</p>
 * <b>Descripción:</b><p> Interfaz que define la operaciones de la capa DAO sobre las entidades  
 * documento digitalizado, su especificación y relaciones, pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao;

import java.util.List;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.BatchDTO;
import es.ricoh.webscan.model.dto.DocBoxScannedDocDTO;

/**
 * Interfaz que define la operaciones de la capa DAO sobre la entidad
 * de procesos de digitalización del modelo de datos base de Webscan.
 * <p>
 * Clase BatchDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */
public interface BatchDAO {

	// Operaciones sobre procesos de digitalización

	/**
	 * Recupera un listado de procesos de digitalización.
	 *
	 * @return listado de procesos de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<BatchDTO> getBatchs() throws DAOException;

	/**
	 * Recupera un listado de procesos de digitalización
	 *  pertenecientes a unidades organizativas.
	 *
	 * @param orgUnitIds
	 *            Relación de identificadores de unidad organizativa.
	 * @param functionalOrgs
	 *            unidades orgánicas por las que filtrar los los documentos a
	 *            recuperar.
	 * @return listado de procesos de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<BatchDTO> getBatchs(List<Long> orgUnitIds, String functionalOrgs) throws DAOException;
	
	/**
	 * Recupera un proceso de digitalización.
	 *
	 * @param batchId
	 * 		Identificador del proceso de digitalización. 
	 * @return proceso de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	BatchDTO getBatch(Long batchId) throws DAOException;
	
	/**
	 * Crea un nuevo proceso de digitalización en el sistema.
	 * 
	 * @param batch
	 *            Nuevo proceso digitalización.
	 * @return El proceso de digitalización creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createBatch(BatchDTO batch, Long scanProfOrgUnitDocSpecId) throws DAOException;

	/**
	 * Actualiza un proceso de digitalización registrado en el sistema. 
	 * Es posible actualizar la siguiente información de un documento digitalizado: 
	 * batch name id, fecha de última actualización, usuario.
	 * 
	 * @param batch
	 *            Proceso de digitalización.
	 * @throws DAOException
	 *             Si no existe algún documento, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	void updateBatch(BatchDTO batch) throws DAOException;

	/**
	 * Actualiza un listado de procesos de digitalización registrado en el sistema. 
	 * Es posible actualizar la siguiente información de un documento digitalizado: 
	 * batch name id, fecha de última actualización, usuario.
	 * 
	 * @param batchs
	 *            Relación de procesos de digitalización.
	 * @throws DAOException
	 *             Si no existe algún documento, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	void updateBatchs(List<BatchDTO> batchs) throws DAOException;

	/**
	 * Elimina un proceso de digitalización.
	 * 
	 * @param batchId
	 *            Identificador del proceso de digitalización.
	 * @throws DAOException
	 *             Si el documento, o se produce algún error en la ejecución de
	 *             la sentencia de BBDD.
	 */
	void removeBatch(Long batchId) throws DAOException;

	/**
	 * Elimina un listado de procesos de digitalización registrados en el sistema.
	 * 
	 * @param batchIds
	 *            Relación de identificadores de procesos de digitalización.
	 * @throws DAOException
	 *             Si no existe algún documento, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	void removeBatchs(List<Long> batchIds) throws DAOException;

}
