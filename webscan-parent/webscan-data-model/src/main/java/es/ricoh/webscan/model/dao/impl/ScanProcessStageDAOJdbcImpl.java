/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.impl.ScanProcessStageDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre la entidad fase de proceso de digitalización.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.model.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.ScanProfileSpecScanProcStageOrderByClause;
import es.ricoh.webscan.model.dao.ScanProcessStageDAO;
import es.ricoh.webscan.model.domain.ScanProcessStage;
import es.ricoh.webscan.model.domain.ScanProfileSpecScanProcStage;
import es.ricoh.webscan.model.dto.MappingUtilities;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;

/**
 * Implementación JPA 2.0 de la capa DAO sobre la entidad fase de proceso de
 * digitalización.
 * <p>
 * Clase ScanProcessStageDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ScanProcessStageDAOJdbcImpl implements ScanProcessStageDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AuditDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la interacción con en el contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProcessStageDAOJdbcImpl() {
		super();
	}

	/**
	 * Establece un nuevo gestor de persistencia JPA.
	 * 
	 * @param aEntityManager
	 *            manager de persistencia.
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		this.entityManager = aEntityManager;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProcessStageDAO#getScanProcessStagesByScanProfileSpecId(java.lang.Long)
	 */
	@Override
	public List<ScanProfileSpecScanProcStageDTO> getScanProcessStagesByScanProfileSpecId(Long scanProfileSpecId) throws DAOException {
		List<ScanProfileSpecScanProcStage> ddbbRes = new ArrayList<ScanProfileSpecScanProcStage>();
		List<ScanProfileSpecScanProcStageDTO> res = new ArrayList<ScanProfileSpecScanProcStageDTO>();
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las fases del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + "...");
			}
			jpqlQuery = "SELECT spssps FROM ScanProfileSpecScanProcStage spssps JOIN spssps.scanProfileSpecification sps WHERE sps.id = :scanProfileSpecId";

			TypedQuery<ScanProfileSpecScanProcStage> query = entityManager.createQuery(jpqlQuery, ScanProfileSpecScanProcStage.class);
			query.setParameter("scanProfileSpecId", scanProfileSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las fases del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + " ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillScanProfileSpecScanProcStageListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Fases del proceso de digitalización definido para la especificación de perfiles de digitalización " + scanProfileSpecId + " recuperadas: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las fases del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las fases del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProcessStageDAO#getRenewableScanProcessStagesByScanProfileSpecId(java.lang.Long)
	 */
	@Override
	public List<ScanProfileSpecScanProcStageDTO> getRenewableScanProcessStagesByScanProfileSpecId(Long scanProfileSpecId) throws DAOException {
		List<ScanProfileSpecScanProcStage> ddbbRes = new ArrayList<ScanProfileSpecScanProcStage>();
		List<ScanProfileSpecScanProcStageDTO> res = new ArrayList<ScanProfileSpecScanProcStageDTO>();
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + "...");
			}
			jpqlQuery = "SELECT spssps FROM ScanProfileSpecScanProcStage spssps JOIN spssps.scanProfileSpecification sps WHERE sps.id = :scanProfileSpecId AND spssps.renewable = TRUE ORDER BY spssps.order";

			TypedQuery<ScanProfileSpecScanProcStage> query = entityManager.createQuery(jpqlQuery, ScanProfileSpecScanProcStage.class);
			query.setParameter("scanProfileSpecId", scanProfileSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + " ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillScanProfileSpecScanProcStageListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización " + scanProfileSpecId + " recuperadas: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las fases reanudables del proceso de digitalización definido para la especificación de perfiles de digitalización: " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProcessStageDAO#getScanProcessStage(java.lang.Long)
	 */
	@Override
	public ScanProcessStageDTO getScanProcessStage(Long scanProcessStageId) throws DAOException {
		ScanProcessStage entity;
		ScanProcessStageDTO res = null;
		String excMsg;

		try {
			entity = getScanProcessStageEntity(scanProcessStageId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando la fase de proceso de digitalización " + scanProcessStageId + " recuperada de BBDD...");
			}
			res = MappingUtilities.fillScanProcessStageDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProcessStageDAO#getScanProcStageByName(java.lang.String)
	 */
	@Override
	public ScanProcessStageDTO getScanProcStageByName(String scanProcessStageName) throws DAOException {
		ScanProcessStageDTO res = null;
		ScanProcessStage entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la fase de proceso de digitalización denominada " + scanProcessStageName + " ...");
			}
			TypedQuery<ScanProcessStage> query = entityManager.createQuery("SELECT sps FROM ScanProcessStage sps WHERE sps.name = :scanProcessStageName", ScanProcessStage.class);
			query.setParameter("scanProcessStageName", scanProcessStageName);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la fase de proceso de digitalización denominada " + scanProcessStageName + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScanProcessStageDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Fase de proceso de digitalización denominada " + scanProcessStageName + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización denominada " + scanProcessStageName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la fase de proceso de digitalización denominada " + scanProcessStageName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización denominada " + scanProcessStageName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización denominada " + scanProcessStageName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización denominada " + scanProcessStageName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización denominada " + scanProcessStageName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización denominada " + scanProcessStageName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización denominada " + scanProcessStageName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización denominada " + scanProcessStageName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProcessStageDAO#getScanProcessStageByNameAndScanProfileId(java.lang.String,
	 *      java.lang.Long)
	 */
	@Override
	public ScanProfileSpecScanProcStageDTO getScanProcessStageByNameAndScanProfileSpecId(String scanProcessStageName, Long scanProfileSpecId) throws DAOException {
		List<EntityOrderByClause> orderByColumns = new ArrayList<EntityOrderByClause>();
		Map<String, List<EntityOrderByClause>> spsOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		ScanProfileSpecScanProcStage entity;
		ScanProfileSpecScanProcStageDTO res;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + "...");
			}
			orderByColumns.add(ScanProfileSpecScanProcStageOrderByClause.ORDER_ASC);
			spsOrderByColumns.put("spssps.", orderByColumns);

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT spssps FROM ScanProfileSpecScanProcStage spssps JOIN spssps.scanProcessStage stage JOIN spssps.scanProfileSpecification sps WHERE stage.name = :scanProcessStageName AND sps.id = :scanProfileSpecId", spsOrderByColumns);

			TypedQuery<ScanProfileSpecScanProcStage> query = entityManager.createQuery(jpqlQuery, ScanProfileSpecScanProcStage.class);
			query.setParameter("scanProcessStageName", scanProcessStageName);
			query.setParameter("scanProfileSpecId", scanProfileSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + " ...");
			}

			entity = query.getSingleResult();

			res = MappingUtilities.fillScanProfileSpecScanProcStageDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de la fase del proceso de digitalización " + scanProcessStageName + " para la especificación de perfiles de digitalización " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad fase de proceso de digitalización a partir
	 * de su identificador.
	 * 
	 * @param scanProcessStageId
	 *            Identificador de fase de proceso de digitalización.
	 * @return Entidad de BBDD fase de proceso de digitalización.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private ScanProcessStage getScanProcessStageEntity(Long scanProcessStageId) throws DAOException {
		ScanProcessStage res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la fase de proceso de digitalización " + scanProcessStageId + " de BBDD ...");
			}
			res = entityManager.find(ScanProcessStage.class, scanProcessStageId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización " + scanProcessStageId + " de BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Fase de proceso de digitalización " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización " + scanProcessStageId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización " + scanProcessStageId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

}
