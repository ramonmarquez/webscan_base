/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA configuración de definición de documento para perfil de digitalización y unidad organizativa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA configuración de definición de documento para perfil
 * de digitalización y unidad organizativa.
 * <p>
 * Clase ScanProfileOrgUnitDocSpecDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class ScanProfileOrgUnitDocSpecDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Indicador de activación de la entidad.
	 */
	private Boolean active = Boolean.TRUE;

	/**
	 * Identificador de la configuración de perfil de digitalización en unidad
	 * organizativa.
	 */
	private Long orgScanProfile;

	/**
	 * Identificador de definición de documento.
	 */
	private Long docSpecification;

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfileOrgUnitDocSpecDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el indicador de activación de la entidad.
	 * 
	 * @return el indicador de activación de la entidad.
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Establece un nuevo valor para el indicador de activación de la entidad.
	 * 
	 * @param activeParam
	 *            nuevo valor para el indicador de activación de la entidad.
	 */
	public void setActive(Boolean activeParam) {
		this.active = activeParam;
	}

	/**
	 * Obtiene el identificador de la configuración de perfil de digitalización
	 * en unidad organizativa.
	 * 
	 * @return el identificador de la configuración de perfil de digitalización
	 *         en unidad organizativa.
	 */
	public Long getOrgScanProfile() {
		return orgScanProfile;
	}

	/**
	 * Establece un nuevo identificador de la configuración de perfil de
	 * digitalización en unidad organizativa.
	 * 
	 * @param anOrgScanProfile
	 *            nuevo identificador de la configuración de perfil de
	 *            digitalización en unidad organizativa.
	 */
	void setOrgScanProfile(Long anOrgScanProfile) {
		this.orgScanProfile = anOrgScanProfile;
	}

	/**
	 * Obtiene el identificador de definición de documento.
	 * 
	 * @return el identificador de definición de documento.
	 */
	public Long getDocSpecification() {
		return docSpecification;
	}

	/**
	 * Establece un nuevo identificador de definición de documento.
	 * 
	 * @param aDocSpecification
	 *            nuevo identificador de definición de documento.
	 */
	void setDocSpecification(Long aDocSpecification) {
		this.docSpecification = aDocSpecification;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docSpecification == null) ? 0 : docSpecification.hashCode());
		result = prime * result + ((orgScanProfile == null) ? 0 : orgScanProfile.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfileOrgUnitDocSpecDTO other = (ScanProfileOrgUnitDocSpecDTO) obj;
		if (docSpecification == null) {
			if (other.docSpecification != null)
				return false;
		} else if (!docSpecification.equals(other.docSpecification))
			return false;
		if (orgScanProfile == null) {
			if (other.orgScanProfile != null)
				return false;
		} else if (!orgScanProfile.equals(other.orgScanProfile))
			return false;
		return true;
	}

}
