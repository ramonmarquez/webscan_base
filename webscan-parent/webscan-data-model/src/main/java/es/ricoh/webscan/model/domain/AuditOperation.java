/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.AuditOperation.java.</p>
 * <b>Descripción:</b><p> Entidad JPA traza de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad JPA traza de auditoría.
 * <p>
 * Clase AuditOperation.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "AUDIT_OPERATION")
public final class AuditOperation implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "AUDIT_OPERATION_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AUDIT_OPERATION_ID_SEQ", sequenceName = "AUDIT_OPERATION_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "OPERATION_NAME")
	@Enumerated(EnumType.STRING)
	private AuditOperationName name;

	/**
	 * Atributo OPERATION_STATUS de la entidad.
	 */
	@Column(name = "OPERATION_STATUS")
	@Enumerated(EnumType.STRING)
	private AuditOperationStatus status;

	/**
	 * Atributo OPERATION_START_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "OPERATION_START_DATE")
	private Date startDate;

	/**
	 * Atributo OPERATION_STATUS_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "OPERATION_STATUS_DATE")
	private Date lastUpdateDate;

	/**
	 * Relación de entidades evento de auditoría de la traza.
	 */
	@OneToMany(mappedBy = "auditOperation", fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.AuditEvent.class)
	private List<AuditEvent> auditEvents = new ArrayList<AuditEvent>();

	/**
	 * Documento digitalizado asociado a la traza de auditoría. Opcional.
	 */
	@OneToOne(mappedBy = "auditOperation", fetch = FetchType.EAGER, cascade = { CascadeType.REMOVE }, optional = true, targetEntity = es.ricoh.webscan.model.domain.AuditScannedDoc.class)
	private AuditScannedDoc auditScannedDoc;

	/**
	 * Constructor sin argumentos.
	 */
	public AuditOperation() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo OPERATION_NAME de la entidad.
	 * 
	 * @return el atributo OPERATION_NAME de la entidad.
	 */
	public AuditOperationName getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo OPERATION_NAME de la entidad.
	 * 
	 * @param anOperationName
	 *            nuevo valor para el atributo OPERATION_NAME de la entidad.
	 */
	public void setName(AuditOperationName anOperationName) {
		this.name = anOperationName;
	}

	/**
	 * Obtiene el atributo OPERATION_STATUS de la entidad.
	 * 
	 * @return el atributo OPERATION_STATUS de la entidad.
	 */
	public AuditOperationStatus getStatus() {
		return status;
	}

	/**
	 * Establece un nuevo valor para el atributo OPERATION_STATUS de la entidad.
	 * 
	 * @param aStatus
	 *            nuevo valor para el atributo OPERATION_STATUS de la entidad.
	 */
	public void setStatus(AuditOperationStatus aStatus) {
		this.status = aStatus;
	}

	/**
	 * Obtiene el atributo OPERATION_START_DATE de la entidad.
	 * 
	 * @return el atributo OPERATION_START_DATE de la entidad.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Establece un nuevo valor para el atributo OPERATION_START_DATE de la
	 * entidad.
	 * 
	 * @param aStartDate
	 *            nuevo valor para el atributo OPERATION_START_DATE de la
	 *            entidad.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}

	/**
	 * Obtiene el atributo OPERATION_STATUS_DATE de la entidad.
	 * 
	 * @return el atributo OPERATION_STATUS_DATE de la entidad.
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * Establece un nuevo valor para el atributo OPERATION_STATUS_DATE de la
	 * entidad.
	 * 
	 * @param aLastUpdateDate
	 *            nuevo valor para el atributo OPERATION_STATUS_DATE de la
	 *            entidad.
	 */
	public void setLastUpdateDate(Date aLastUpdateDate) {
		this.lastUpdateDate = aLastUpdateDate;
	}

	/**
	 * Obtiene la relación de entidades evento de auditoría de la traza.
	 * 
	 * @return la relación de entidades evento de auditoría de la traza.
	 */
	public List<AuditEvent> getAuditEvents() {
		return auditEvents;
	}

	/**
	 * Establece una nueva relación de entidades evento de auditoría de la
	 * traza.
	 * 
	 * @param anyAuditEvents
	 *            Nueva relación de entidades evento de auditoría de la traza.
	 */
	public void setAuditEvents(List<AuditEvent> anyAuditEvents) {
		this.auditEvents.clear();
		if (anyAuditEvents != null && !anyAuditEvents.isEmpty()) {
			this.auditEvents.addAll(anyAuditEvents);
		}
	}

	/**
	 * Obtiene el documento digitalizado asociado a la traza de auditoría. Es
	 * opcional.
	 * 
	 * @return el documento digitalizado asociado a la traza de auditoría. En
	 *         caso que no exista, retorna null.
	 */
	public AuditScannedDoc getAuditScannedDoc() {
		return auditScannedDoc;
	}

	/**
	 * Establece el documento digitalizado asociado a la traza de auditoría. Es
	 * opcional.
	 * 
	 * @param anAuditScannedDoc
	 *            el documento digitalizado asociado a la traza de auditoría.
	 */
	public void setAuditScannedDoc(AuditScannedDoc anAuditScannedDoc) {
		this.auditScannedDoc = anAuditScannedDoc;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditOperation other = (AuditOperation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
