/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA plugin empleando en fase de proceso de digitalización para
 * configuración de perfil de digitalización en unidad organizativa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA plugin empleando en fase de proceso de digitalización
 * para configuración de perfil de digitalización en unidad organizativa.
 * <p>
 * Clase ScanProfileOrgUnitPluginDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class ScanProfileOrgUnitPluginDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Indicador de activación de la entidad.
	 */
	private Boolean active = Boolean.TRUE;

	/**
	 * Identificador de la configuración de perfil de digitalización en unidad
	 * organizativa.
	 */
	private Long orgScanProfile;

	/**
	 * Identificador de plugin.
	 */
	private Long plugin;

	/**
	 * Identificador de fase de proceso de digitalización.
	 */
	private Long scanProcessStage;

	/**
	 * Constructor sin argumento.
	 */
	public ScanProfileOrgUnitPluginDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el indicador de activación de la entidad.
	 * 
	 * @return el indicador de activación de la entidad.
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Establece un nuevo valor para el indicador de activación de la entidad.
	 * 
	 * @param activeParam
	 *            nuevo valor para el indicador de activación de la entidad.
	 */
	public void setActive(Boolean activeParam) {
		this.active = activeParam;
	}

	/**
	 * Obtiene el identificador de la configuración de perfil de digitalización
	 * en unidad organizativa.
	 * 
	 * @return el identificador de la configuración de perfil de digitalización
	 *         en unidad organizativa.
	 */
	public Long getOrgScanProfile() {
		return orgScanProfile;
	}

	/**
	 * Establece un nuevo identificador de la configuración de perfil de
	 * digitalización en unidad organizativa.
	 * 
	 * @param anOrgScanProfile
	 *            nuevo identificador de la configuración de perfil de
	 *            digitalización en unidad organizativa.
	 */
	void setOrgScanProfile(Long anOrgScanProfile) {
		this.orgScanProfile = anOrgScanProfile;
	}

	/**
	 * Obtiene el identificador de plugin.
	 * 
	 * @return el identificador de plugin.
	 */
	public Long getPlugin() {
		return plugin;
	}

	/**
	 * Establece un nuevo identificador de plugin.
	 * 
	 * @param aPlugin
	 *            nuevo identificador de plugin.
	 */
	void setPlugin(Long aPlugin) {
		this.plugin = aPlugin;
	}

	/**
	 * Obtiene el identificador de fase de proceso de digitalización.
	 * 
	 * @return el identificador de fase de proceso de digitalización.
	 */
	public Long getScanProcessStage() {
		return scanProcessStage;
	}

	/**
	 * Establece el identificador de fase de proceso de digitalización.
	 * 
	 * @param aScanProcessStage
	 *            nuevo identificador de fase de proceso de digitalización.
	 */
	void setScanProcessStage(Long aScanProcessStage) {
		this.scanProcessStage = aScanProcessStage;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orgScanProfile == null) ? 0 : orgScanProfile.hashCode());
		result = prime * result + ((plugin == null) ? 0 : plugin.hashCode());
		result = prime * result + ((scanProcessStage == null) ? 0 : scanProcessStage.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfileOrgUnitPluginDTO other = (ScanProfileOrgUnitPluginDTO) obj;
		if (orgScanProfile == null) {
			if (other.orgScanProfile != null)
				return false;
		} else if (!orgScanProfile.equals(other.orgScanProfile))
			return false;
		if (plugin == null) {
			if (other.plugin != null)
				return false;
		} else if (!plugin.equals(other.plugin))
			return false;
		if (scanProcessStage == null) {
			if (other.scanProcessStage != null)
				return false;
		} else if (!scanProcessStage.equals(other.scanProcessStage))
			return false;
		return true;
	}

}
