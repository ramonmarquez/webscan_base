/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.WebscanModelConstants.java.</p>
 * <b>Descripción:</b><p> Interfaz que agrupa constantes del componente que implementa la capa de
 * acceso al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model;

/**
 * Interfaz que agrupa constantes del componente que implementa la capa de
 * acceso al modelo de datos base de Webscan.
 * <p>
 * Clase WebscanModelConstants.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface WebscanModelConstants {

	/**
	 * Identificador de lote de documentos que identifica que el valor nulo.
	 */
	String NULL_BATCH_ID = "_#NULL_BATCH_ID#_";

	/**
	 * Longitud máxima del campo información adicional de los eventos asociados
	 * a las trazas de auditoría.
	 */
	Integer AUDIT_ADDITIONAL_INFO_MAX_LENGTH = 512;
}
