/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.AuditOpEventResult.java.</p>
 * <b>Descripción:</b><p> Clase que define el conjunto de códigos de resultado de una traza de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

/**
 * Clase que define el conjunto de códigos de resultado de una traza de
 * auditoría.
 * <p>
 * Clase AuditOpEventResult.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum AuditOpEventResult {
	/**
	 * Resultado correcto.
	 */
	OK("COD_000"),
	/**
	 * Resultado erróneo.
	 */
	ERROR("COD_099");

	/**
	 * Código de resultado de trazas de auditoría.
	 */
	private String result;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aResult
	 *            Código de resultado de trazas de auditoría.
	 */
	AuditOpEventResult(String aResult) {
		this.result = aResult;
	}

	/**
	 * Obtiene el atributo result.
	 * 
	 * @return el atributo result.
	 */
	public String getResult() {
		return result;
	}

}
