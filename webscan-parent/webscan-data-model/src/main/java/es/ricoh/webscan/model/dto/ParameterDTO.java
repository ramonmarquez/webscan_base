/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ParameterDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA parámetro.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA parámetro.
 * <p>
 * Clase ParameterDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class ParameterDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Valor del parámetro, expresado como cadena de caracteres.
	 */
	private String value;

	/**
	 * Denominación de la especificación que define el parámetro.
	 */
	private String specificationName;

	/**
	 * Constructor sin argumentos.
	 */
	public ParameterDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el valor del parámetro, expresado como cadena de caracteres.
	 * 
	 * @return el valor del parámetro, expresado como cadena de caracteres.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Establece un nuevo valor del parámetro, expresado como cadena de
	 * caracteres.
	 * 
	 * @param aValue
	 *            nuevo valor del parámetro, expresado como cadena de
	 *            caracteres.
	 */
	public void setValue(String aValue) {
		this.value = aValue;
	}

	/**
	 * Obtiene la denominación de la especificación que define el parámetro.
	 * 
	 * @return la denominación de la especificación que define el parámetro.
	 */
	public String getSpecificationName() {
		return specificationName;
	}

	/**
	 * Establece un nueva definición del parámetro.
	 * 
	 * @param aSpecificationName
	 *            nueva definición del parámetro.
	 */
	public void setSpecificationName(String aSpecificationName) {
		this.specificationName = aSpecificationName;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((specificationName == null) ? 0 : specificationName.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParameterDTO other = (ParameterDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (specificationName == null) {
			if (other.specificationName != null)
				return false;
		} else if (!specificationName.equals(other.specificationName))
			return false;
		return true;
	}

}
