/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.AuditEventOrderByClause.java.</p>
 * <b>Descripción:</b><p> Clase que representa la clausula ORDER BY sobre la entidad AuditEvent.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model;

/**
 * Clase que representa la clausula ORDER BY sobre la entidad AuditEvent.
 * <p>
 * Clase AuditEventOrderByClause.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.EntityOrderByClause
 * @version 2.0.
 */
public enum AuditEventOrderByClause implements EntityOrderByClause {

	/**
	 * Ordenación ascendente sobre el atributo eventName.
	 */
	EVENT_NAME_ASC("eventName", OrderByClause.ASC),
	/**
	 * Ordenación descendente sobre el atributo eventName.
	 */
	EVENT_NAME_DESC("eventName", OrderByClause.DESC),
	/**
	 * Ordenación ascendente sobre el atributo eventUser.
	 */
	EVENT_USER_ASC("eventUser", OrderByClause.ASC),
	/**
	 * Ordenación descendente sobre el atributo eventUser.
	 */
	EVENT_USER_DESC("eventUser", OrderByClause.DESC),
	/**
	 * Ordenación ascendente sobre el atributo eventDate.
	 */
	EVENT_DATE_ASC("eventDate", OrderByClause.ASC),
	/**
	 * Ordenación descendente sobre el atributo eventDate.
	 */
	EVENT_DATE_DESC("eventDate", OrderByClause.DESC),
	/**
	 * Ordenación ascendente sobre el atributo eventResult.
	 */
	EVENT_RES_ASC("eventResult", OrderByClause.ASC),
	/**
	 * Ordenación descendente sobre el atributo eventResult.
	 */
	EVENT_RES_DESC("eventResult", OrderByClause.DESC);

	/**
	 * Nombre de un atributo de la entidad AuditOperation.
	 */
	private String attName;

	/**
	 * Tipo de ordenación.
	 */
	private OrderByClause orderByClause;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param anAttName
	 *            Nombre de un atributo de la entidad AuditOperation.
	 * @param anOrderByClause
	 *            Tipo de ordenación.
	 */
	AuditEventOrderByClause(String anAttName, OrderByClause anOrderByClause) {
		this.attName = anAttName;
		this.orderByClause = anOrderByClause;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.EntityOrderByClause#getAttName()
	 */
	public String getAttName() {
		return attName;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.EntityOrderByClause#getOrderByClause()
	 */
	public OrderByClause getOrderByClause() {
		return orderByClause;
	}

}
