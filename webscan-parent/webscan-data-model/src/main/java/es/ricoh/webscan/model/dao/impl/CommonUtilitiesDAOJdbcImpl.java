/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.impl.CommonUtilitiesDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Clase de utilidades de la implementación JPA 2.0 de la capa DAO sobre las entidades pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.domain.Batch;
import es.ricoh.webscan.model.domain.DocumentSpecification;
import es.ricoh.webscan.model.domain.OrganizationUnit;
import es.ricoh.webscan.model.domain.Plugin;
import es.ricoh.webscan.model.domain.PluginSpecification;
import es.ricoh.webscan.model.domain.ScanProcessStage;
import es.ricoh.webscan.model.domain.ScanProfile;
import es.ricoh.webscan.model.domain.ScanProfileOrgUnitDocSpec;

/**
 * Clase de utilidades de la implementación JPA 2.0 de la capa DAO sobre las
 * entidades pertenecientes al modelo de datos base de Webscan.
 * <p>
 * Clase CommonUtilitiesDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class CommonUtilitiesDAOJdbcImpl {

	/**
	 * Constructor method for the class CommonUtilitiesDAOJdbcImpl.java.
	 */
	private CommonUtilitiesDAOJdbcImpl() {
	}

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CommonUtilitiesDAOJdbcImpl.class);

	/**
	 * Recupera de BBDD una entidad especificación de plugin a partir de su
	 * denominación.
	 * 
	 * @param name
	 *            Denominación de especicación de plugin.
	 * @param entityManager
	 *            gestor del contexto de persistencia JPA.
	 * @return Entidad de BBDD especificación de plugin.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	static PluginSpecification getPluginSpecificationEntityByName(String name, EntityManager entityManager) throws DAOException {
		PluginSpecification res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de plugin denominada " + name + " ...");
			}
			TypedQuery<PluginSpecification> query = entityManager.createQuery("SELECT ps FROM PluginSpecification ps WHERE ps.name = :pluginSpecName", PluginSpecification.class);
			query.setParameter("pluginSpecName", name);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la definición de plugin denominada " + name + " ...");
			}
			res = query.getSingleResult();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de plugin denominada " + res.getName() + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin denominada " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de plugin denominada " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin denominada " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin denominada " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin denominada " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin denominada " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin denominada " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de plugin denominada " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin denominada " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad perfil de digitalización a partir de su
	 * denominación.
	 * 
	 * @param name
	 *            Denominación de perfil de digitalización.
	 * @param entityManager
	 *            gestor del contexto de persistencia JPA.
	 * @return Entidad de BBDD perfil de digitalización.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	static ScanProfile getScanProfileEntityByName(String name, EntityManager entityManager) throws DAOException {
		ScanProfile res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el perfil de digitalización denominado " + name + " ...");
			}
			TypedQuery<ScanProfile> query = entityManager.createQuery("SELECT sp FROM ScanProfile sp WHERE sp.name = :scanProfileName", ScanProfile.class);
			query.setParameter("scanProfileName", name);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el perfil de digitalización denominado " + name + " ...");
			}
			res = query.getSingleResult();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Perfil de digitalización denominado " + res.getName() + " recuperado: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización denominado " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización denominado " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + name + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad plugin a partir de su identificador.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @param entityManager
	 *            gestor del contexto de persistencia JPA.
	 * @return Entidad de BBDD plugin.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	static Plugin getPluginEntity(Long pluginId, EntityManager entityManager) throws DAOException {
		Plugin res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el plugin " + pluginId + " de BBDD ...");
			}
			res = entityManager.find(Plugin.class, pluginId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " en BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Plugin " + res.getId() + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad unidad organizativa a partir de su
	 * identificador.
	 * 
	 * @param orgId
	 *            Identificador de unidad organizativa.
	 * @param entityManager
	 *            gestor del contexto de persistencia JPA.
	 * @return Entidad de BBDD perfil de digitalización.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	static OrganizationUnit getOrganizationUnitEntity(Long orgId, EntityManager entityManager) throws DAOException {
		OrganizationUnit res;
		String excMsg;

		try {

			LOG.debug("[WEBSCAN-MODEL] Recuperando la entidad {} ...", orgId);

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la entidad {} ...", orgId);

			res = entityManager.find(OrganizationUnit.class, orgId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la entidad " + orgId + " en BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperada de BBDD la entidad " + orgId + ".");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad especificación de documento a partir de su
	 * identificador.
	 * 
	 * @param docSpecificationId
	 *            Identificador de especificación de documento.
	 * @param entityManager
	 *            gestor del contexto de persistencia JPA.
	 * @return Entidad de BBDD perfil de digitalización.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	static DocumentSpecification getDocumentSpecificationEntity(Long docSpecificationId, EntityManager entityManager) throws DAOException {
		DocumentSpecification res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de documento " + docSpecificationId + " de BBDD ...");
			}
			res = entityManager.find(DocumentSpecification.class, docSpecificationId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento " + docSpecificationId + " en BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de documento " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento " + docSpecificationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento " + docSpecificationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad configuración de especificación de documento
	 * en perfil de digitalización establecido en unidad organizativa a partir
	 * de su identificador.
	 * 
	 * @param scanProfileOrgUnitDocSpecId
	 *            Identificador de configuración de especificación de documento
	 *            en perfil de digitalización establecido en unidad
	 *            organizativa.
	 * @param entityManager
	 *            gestor del contexto de persistencia JPA.
	 * @return Entidad de BBDD perfil de digitalización.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	static ScanProfileOrgUnitDocSpec getScanProfileOrgUnitDocSpecEntity(Long scanProfileOrgUnitDocSpecId, EntityManager entityManager) throws DAOException {
		ScanProfileOrgUnitDocSpec res;
		String excMsg;

		try {

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la configuración de definción de documento para perfil de digitalización y unidad organizativa " + scanProfileOrgUnitDocSpecId + " de BBDD ...");
			}

			res = entityManager.find(ScanProfileOrgUnitDocSpec.class, scanProfileOrgUnitDocSpecId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de definción de documento para perfil de digitalización y unidad organizativa " + scanProfileOrgUnitDocSpecId + " de BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Configuración de definción de documento para perfil de digitalización y unidad organizativa " + scanProfileOrgUnitDocSpecId + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de definción de documento para perfil de digitalización y unidad organizativa " + scanProfileOrgUnitDocSpecId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de definción de documento para perfil de digitalización y unidad organizativa " + scanProfileOrgUnitDocSpecId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad procesos de digitalización.
	 * 
	 * @param batchId
	 *            Identificador de proceso de digitalización.
	 * @param entityManager
	 *            gestor del contexto de persistencia JPA.
	 * @return Entidad de BBDD proceso de digitalización.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	static Batch getBatchEntity(Long batchId, EntityManager entityManager) throws DAOException {
		Batch res;
		String excMsg;

		try {

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el proceso de digitalización " + batchId + " de BBDD ...");
			}

			res = entityManager.find(Batch.class, batchId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando el proceso de digitalización " + batchId + " de BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Configuración de proceso de digitalización " + batchId + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el proceso de digitalización " + batchId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el proceso de digitalización " + batchId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad fase de proceso de digitalización de
	 * documentos a partir de su identificador.
	 * 
	 * @param scanProcessStageId
	 *            Identificador de fase del proceso de digitalización de
	 *            documentos.
	 * @param entityManager
	 *            gestor del contexto de persistencia JPA.
	 * @return Entidad de BBDD fase del proceso de digitalización de documentos.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	static ScanProcessStage getScanProcessStageEntity(Long scanProcessStageId, EntityManager entityManager) throws DAOException {
		ScanProcessStage res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la fase de proceso de digitalización de documentos " + scanProcessStageId + " de BBDD ...");
			}
			res = entityManager.find(ScanProcessStage.class, scanProcessStageId);
			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización de documentos " + scanProcessStageId + " de BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Fase de proceso de digitalización de documentos " + scanProcessStageId + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización de documentos " + scanProcessStageId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la fase de proceso de digitalización de documentos " + scanProcessStageId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Cierra la interacción con el contexto de persistencia JPA.
	 * 
	 * @param entityManager
	 *            gestor del contexto de persistencia JPA.
	 */
	public static void closeEntityManager(EntityManager entityManager) {
		try {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Error al cerrar el objeto entityManger: " + e.getMessage(), e);
			}
		}
	}

	/**
	 * Construye una consulta que incluye clausulas de ordenación sobre algunos
	 * de sus campos.
	 * 
	 * @param baseJqplQuery
	 *            Consulta JPA base.
	 * @param orderByColumns
	 *            Clausulas de ordenación, identificadas por el alias de la
	 *            entidad sobre la que se aplican.
	 * @return Consula JPA base más clausulas de ordenación generadas.
	 */
	public static String builOrderByJpqlQuery(String baseJqplQuery, Map<String, List<EntityOrderByClause>> orderByColumns) {
		Boolean isFirst = Boolean.TRUE;
		String res;
		String prefix;
		String orderByClause = "";

		res = baseJqplQuery.toString();

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (Iterator<String> it = orderByColumns.keySet().iterator(); it.hasNext();) {
				prefix = it.next();
				orderByClause += (isFirst ? "" : ",") + buildEntityOrderByClause(prefix, orderByColumns.get(prefix));
				isFirst = Boolean.FALSE;
			}

			if (!orderByClause.isEmpty()) {
				res += " ORDER BY " + orderByClause;
			}
		}

		return res;
	}

	/**
	 * Construye una clausula de ordenación a partir del alias de la entidad y
	 * los campos sobre los que se efectuará la ordenación de los resultados.
	 * 
	 * @param entityNamePrefix
	 *            Alias de la entidad sobre la que se aplican la clausulas de
	 *            ordenación.
	 * @param entitiesOrderByClause
	 *            Clausulas de ordenación.
	 * @return Cadena que representa el listado de clausulas de ordenación.
	 */
	static String buildEntityOrderByClause(String entityNamePrefix, List<EntityOrderByClause> entitiesOrderByClause) {
		String res = null;

		if (entitiesOrderByClause != null && !entitiesOrderByClause.isEmpty()) {
			res = "";
			for (EntityOrderByClause eobc: entitiesOrderByClause) {
				// user.name ASC, donde
				// entityNamePrefix = user.
				// eobc.getAttName() = name
				// eobc.getOrderByClause().name() = ASC
				res += entityNamePrefix + eobc.getAttName() + " " + eobc.getOrderByClause().name() + ",";
			}

			res = res.substring(0, res.length() - 1);
		}

		return res;
	}
}
