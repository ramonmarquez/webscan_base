/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.WebscanModelManager.java.</p>
 * <b>Descripción:</b><p> Fachada de gestión y consulta sobre las entidades pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.ScannedDocsStoreMode;
import es.ricoh.webscan.WebscanConstants;
import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.model.dao.AuditDAO;
import es.ricoh.webscan.model.dao.BatchDAO;
import es.ricoh.webscan.model.dao.DocumentDAO;
import es.ricoh.webscan.model.dao.OrganizationUnitDAO;
import es.ricoh.webscan.model.dao.PluginDAO;
import es.ricoh.webscan.model.dao.ScanProcessStageDAO;
import es.ricoh.webscan.model.dao.ScanProfileDAO;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.domain.OperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.dto.AuditReportDTO;
import es.ricoh.webscan.model.dto.AuditScannedDocDTO;
import es.ricoh.webscan.model.dto.BatchDTO;
import es.ricoh.webscan.model.dto.DocBoxScannedDocDTO;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitTreeDTO;
import es.ricoh.webscan.model.dto.ParameterDTO;
import es.ricoh.webscan.model.dto.ParameterSpecificationDTO;
import es.ricoh.webscan.model.dto.ParameterTypeDTO;
import es.ricoh.webscan.model.dto.PluginDTO;
import es.ricoh.webscan.model.dto.PluginScanProcStageDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.utilities.CryptoUtilities;
import es.ricoh.webscan.utilities.FileSystemUtilities;
import es.ricoh.webscan.utilities.MimeType;
import es.ricoh.webscan.utilities.Utils;
import es.ricoh.webscan.utilities.WebscanUtilitiesException;

/**
 * Fachada de gestión y consulta sobre las entidades pertenecientes al modelo de
 * datos base de Webscan.
 * <p>
 * Clase WebscanModelManager.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class WebscanModelManager {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WebscanModelManager.class);

	/**
	 * DAO sobre las entidades plugin, parámetro, especificaciones de plugin y
	 * parámetro, y tipos de parámetros.
	 */
	private PluginDAO pluginDAO;

	/**
	 * DAO sobre la entidad fase de proceso de digitalización.
	 */
	private ScanProcessStageDAO scanProcessStageDAO;

	/**
	 * DAO sobre las entidades perfil de digitalización, su especificación y
	 * relaciones.
	 */
	private ScanProfileDAO scanProfileDAO;

	/**
	 * DAO sobre las entidades documento digitalizado, su especificación y
	 * relaciones.
	 */
	private DocumentDAO documentDAO;

	/**
	 * DAO sobre la entidad unidad organizativa.
	 */
	private OrganizationUnitDAO orgUnitDAO;

	/**
	 * DAO sobre las entidades traza y evento de auditoría.
	 */
	private AuditDAO auditDAO;

	/**
	 * DAO sobre los procesos de digitalización.
	 */
	private BatchDAO batchDAO;

	/**
	 * Utilidades y acceso a parámetros globales del Sistema.
	 */
	private Utils webscanUtilParams;

	/**
	 * Constructor sin argumentos.
	 */
	public WebscanModelManager() {
		super();
	}

	/*************************************************************************************/
	/*					Especificaciones de plugins, plugins y parámetros				 */
	/*************************************************************************************/

	/**
	 * Obtiene el listado de definiciones de plugins registradas en el sistema,
	 * permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de definiciones de plugins.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<PluginSpecificationDTO> listPluginSpecifications(final Long pageNumber, final Long elementsPerPage, final List<EntityOrderByClause> orderByColumns) throws DAOException {
		List<PluginSpecificationDTO> res = new ArrayList<PluginSpecificationDTO>();

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las definiciones de plugins registradas en el sistema ...");

		DataValidationUtilities.checkPluginSpecGroupByClause(orderByColumns);

		res = pluginDAO.listPluginSpecifications(pageNumber, elementsPerPage, orderByColumns);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Numero de definiciones de plugins registradas en el sistema: " + res.size() + " ...");
		}
		return res;
	}

	/**
	 * Obtiene las especificaciones de plugins que implementan cada una de las
	 * fases configuradas para una especificación de perfiles de digitalización
	 * determinada, ordenadas por orden de ejecución.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de especificación de perfiles de digitalización.
	 * @return Mapa donde cada entrada es identificada por una configuración de
	 *         fase de proceso de digitalización, y cuyo valor es la relación de
	 *         especificaciones de plugin que implementan la fase que identifica
	 *         la entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe
	 *             especificación de perfiles de digitalización, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> getScanProfileSpecScanProcStagesByScanProfSpecId(Long scanProfileSpecId) throws DAOException {
		List<ScanProfileSpecScanProcStageDTO> stages;
		List<PluginSpecificationDTO> pluginSpecs;
		Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> res = new HashMap<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las especificaciones de plugins que implementan las fases configuradas para la especificación de perfiles de digitalización " + scanProfileSpecId + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(scanProfileSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recuperan las fases configuradas para la especificación de perfiles de digitalización " + scanProfileSpecId + "...");
		}
		stages = scanProcessStageDAO.getScanProcessStagesByScanProfileSpecId(scanProfileSpecId);

		for (ScanProfileSpecScanProcStageDTO stage: stages) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Se recuperan las especificaciones de plugins que implementan la fase de proceso de digitalización " + stage.getScanProcessStageId() + "...");
			}
			pluginSpecs = pluginDAO.getPluginSpecsByScanProcStageId(stage.getScanProcessStageId());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Especificaciones de plugins que implementan la fase de proceso de digitalización " + stage.getScanProcessStageId() + " recuperadas: " + pluginSpecs.size() + ".");
			}
			res.put(stage, pluginSpecs);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se ha finalizado la recuperación de las especificaciones de plugins que implementan las fases configuradas para la especificación de perfiles de digitalización " + scanProfileSpecId + ".");
		}
		return res;
	}

	/**
	 * Obtiene las especificaciones de plugins y fases de procesos de
	 * digitalización que implementan.
	 * 
	 * @return Mapa donde cada entrada es identificada por una especificación de
	 *         plugin, y cuyo valor es la relación de fases de procesos de
	 *         digitalización que identifica la entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public Map<PluginSpecificationDTO, List<ScanProcessStageDTO>> listPluginSpecsAndScanProcessStages() throws DAOException {
		Map<PluginSpecificationDTO, List<ScanProcessStageDTO>> res = new HashMap<PluginSpecificationDTO, List<ScanProcessStageDTO>>();

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las especificaciones de plugins y fases que implementan ...");

		LOG.debug("[WEBSCAN-MODEL] Se recuperan las especificaciones de plugins y fases que implementan de BBDD ...");
		res = pluginDAO.listPluginSpecsAndScanProcessStages();

		LOG.debug("[WEBSCAN-MODEL] Se ha finalizado la recuperación de las especificaciones de plugins y las fases que implementan.");

		return res;
	}

	/**
	 * Comprueba si una especificación de plugins posee documentos en proceso de
	 * digitalización.
	 * 
	 * @param pluginSpecId
	 *            Identificador de especificación de plugins.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugins, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	public Boolean hasPluginSpecDocsInScanProccess(Long pluginSpecId) throws DAOException {
		Boolean res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la comprobación de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(pluginSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = pluginDAO.hasPluginSpecDocsInScanProccess(pluginSpecId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Finalizada la comprobación de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ". Resultado: " + res);
		}
		return res;
	}

	/**
	 * Obtiene una especificación de plugin a partir de su identificador.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de especificación de plugin.
	 * @return La definición de plugin solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public PluginSpecificationDTO getPluginSpecification(Long pluginSpecificationId) throws DAOException {
		PluginSpecificationDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la definición de plugin con identificador " + pluginSpecificationId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(pluginSpecificationId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = pluginDAO.getPluginSpecification(pluginSpecificationId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la definición de plugin con identificador " + pluginSpecificationId + ".");
		}
		return res;
	}

	/**
	 * Obtiene una especificación de plugin a partir de su denominación.
	 * 
	 * @param pluginSpecificationName
	 *            Denominación de especificación de plugin.
	 * @return La definición de plugin solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public PluginSpecificationDTO getPluginSpecificationByName(String pluginSpecificationName) throws DAOException {
		PluginSpecificationDTO res = null;

		try {
			DataValidationUtilities.checkIdentifier(pluginSpecificationName);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = pluginDAO.getPluginSpecificationByName(pluginSpecificationName);

		return res;
	}

	/**
	 * Obtiene la especificación de plugin que implementa una determinada fase
	 * para un perfil de digitalización configurado en una unidad organizativa.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización
	 *            para unidad organizativa.
	 * @param scanProcessStageName
	 *            Denominación de una fase de proceso de digitalziación.
	 * @return La definición de plugin que implementa una determinada fase para
	 *         un perfil de digitalización configurado en una unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public PluginSpecificationDTO getPluginSpecByScanProfileOrgUnitAndStage(Long scanProfileOrgUnitId, String scanProcessStageName) throws DAOException {
		PluginSpecificationDTO res = null;
		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la definición de plugin que implementa la fase {} para el perfil de digitalización configurado en una unidad organizativa {}...", scanProcessStageName, scanProfileOrgUnitId);
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkIdentifier(scanProfileOrgUnitId);
			DataValidationUtilities.checkEmptyObject(scanProcessStageName, "Fase de proceso de digitalización no informada.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-MODEL] Se ejecuta la sentencia en base de datos ...");
		res = pluginDAO.getPluginSpecByScanProfileOrgUnitAndStage(scanProfileOrgUnitId, scanProcessStageName);

		LOG.debug("[WEBSCAN-MODEL] Definición de plugin {} recuperada.", res.getId());

		return res;
	}

	/**
	 * Crea una nueva definición de plugin en el sistema, sin parámetros.
	 * 
	 * @param specification
	 *            Nueva definición de plugin.
	 * @return La definición de plugin creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public PluginSpecificationDTO createPluginSpecification(PluginSpecificationDTO specification) throws DAOException {
		PluginSpecificationDTO res = null;

		res = createPluginSpecification(specification, new ArrayList<ParameterSpecificationDTO>());

		return res;
	}

	/**
	 * Crea una nueva definición de plugin en el sistema, registrando además sus
	 * parámetros y una relación de configuraciones o parametrizaciones
	 * (plugins).
	 * 
	 * @param specification
	 *            Nueva definición de plugin.
	 * @param paramSpecs
	 *            Parámetros de la nueva definición de plugin.
	 * @return La definición de plugin creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public PluginSpecificationDTO createPluginSpecification(final PluginSpecificationDTO specification, final List<ParameterSpecificationDTO> paramSpecs) throws DAOException {
		Long psId;
		PluginSpecificationDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la creación de una nueva definición de plugin, denominada " + specification.getName() + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se validan los datos de la nueva definición de plugin, denominada " + specification.getName() + " ...");
		}
		// Validaciones de datos: especificación de plugin y parámetros
		DataValidationUtilities.checkPluginSpecification(specification);
		DataValidationUtilities.checkParameterSpecifications(paramSpecs, specification.getId());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persiste la nueva definición de plugin '" + specification.getName() + "', sus configuraciones y parámetros en BBDD ...");
		}
		psId = pluginDAO.createPluginSpecification(specification);
		if (paramSpecs != null && !paramSpecs.isEmpty()) {
			pluginDAO.setParameterSpecifications(paramSpecs, psId);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recupera la nueva definición de plugin, denominada " + specification.getName() + ", de BBDD ...");
		}
		res = pluginDAO.getPluginSpecification(psId);

		return res;
	}

	/**
	 * Actualiza una especificación de plugin registrada en el sistema.
	 * 
	 * @param specification
	 *            Especificación de plugin.
	 * @param propagateActivation
	 *            Indica si los cambios sobre la definición de plugin serán
	 *            propagados a la relación de configuraciones o
	 *            parametrizaciones de la definición (plugins).
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updatePluginSpecification(final PluginSpecificationDTO specification, final Boolean propagateActivation) throws DAOException {
		Map<PluginDTO, List<ParameterDTO>> plugins;

		// Validaciones de datos: especificación de plugin y parámetros
		DataValidationUtilities.checkPluginSpecification(specification);
		plugins = new HashMap<PluginDTO, List<ParameterDTO>>();

		List<PluginDTO> currentPlugins = pluginDAO.getPluginsBySpecification(specification.getId());

		if (currentPlugins != null && !currentPlugins.isEmpty()) {
			for (PluginDTO plugin: currentPlugins) {
				plugins.put(plugin, pluginDAO.getParametersByPlugin(plugin.getId()));
			}

		}

		updatePluginSpecification(specification, pluginDAO.getParameterSpecificationsByPluginSpec(specification.getId()), propagateActivation);
	}

	/**
	 * Actualiza una especificación de plugin registrada en el sistema,
	 * estableciéndole una nueva lista de parámetros, y una nueva una relación
	 * de configuraciones o plugins.
	 * 
	 * @param specification
	 *            Especificación de plugin.
	 * @param paramSpecs
	 *            Nueva relación de parámetros de la especificación de plugin.
	 * @param propagateActivation
	 *            Indica si los cambios sobre la definición de plugin serán
	 *            propagados a la relación de configuraciones o
	 *            parametrizaciones de la definición (plugins).
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updatePluginSpecification(PluginSpecificationDTO specification, List<ParameterSpecificationDTO> paramSpecs, Boolean propagateActivation) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización de la definición de plugin, denominada " + specification.getName() + " ...");
			LOG.debug("[WEBSCAN-MODEL] Se validan los datos actualizados de la definición de plugin, denominada " + specification.getName() + " ...");
		}
		// Validaciones de datos: especificación de plugin y parámetros
		DataValidationUtilities.checkPluginSpecification(specification);
		DataValidationUtilities.checkParameterSpecifications(paramSpecs, specification.getId());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten los cambios de la definición de plugin '" + specification.getName() + "', sus configuraciones y parámetros en BBDD ...");
		}
		pluginDAO.updatePluginSpecification(specification);

		pluginDAO.setParameterSpecifications((paramSpecs == null ? new ArrayList<ParameterSpecificationDTO>() : paramSpecs), specification.getId());

		if (propagateActivation) {
			Boolean activePlugSpec = specification.getActive();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Se ha solicitado la propagación del estado de activación a los plugins, cuya definición " + "se denominada " + specification.getName() + " (Activo: " + activePlugSpec + ")...");
			}
			List<PluginDTO> updatingPlugin = pluginDAO.getPluginsBySpecification(specification.getId());

			for (PluginDTO plugin: updatingPlugin) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Se actualiza el estado de activación del plugin " + plugin.getName() + " en BBDD ...");
				}
				plugin.setActive(activePlugSpec);
				pluginDAO.updatePlugin(plugin);
				updateScanProfOrgUnitPluignActivationStatus(plugin);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Estado de activación del plugin " + plugin.getName() + " actualizado.");
				}
			}
		}
	}

	/**
	 * Elimina una definición de plugin del sistema.
	 * 
	 * @param specificationId
	 *            Identificador de especificación de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void removePluginSpecification(Long specificationId) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia el borrado de la definición de plugin " + specificationId + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se valida los datos de la definición de plugin " + specificationId + " ...");
		}
		// Validaciones de datos: especificación de plugin y parámetros
		try {
			DataValidationUtilities.checkIdentifier(specificationId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se elimina la definición de plugin " + specificationId + ", sus configuraciones y parámetros en BBDD ...");
		}
		pluginDAO.removePluginSpecification(specificationId);
	}

	/**
	 * Obtiene un plugin a partir de su identificador.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @return El plugin solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public PluginDTO getPlugin(Long pluginId) throws DAOException {
		PluginDTO res = null;

		try {
			DataValidationUtilities.checkIdentifier(pluginId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = pluginDAO.getPlugin(pluginId);

		return res;
	}

	/**
	 * Obtiene la relación de plugins registrada en el sistema para una
	 * especificación de plugin determinada.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de especificación de plugin.
	 * @return Lista de configuraciones de una especificación de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public List<PluginDTO> getPluginsBySpecification(Long pluginSpecificationId) throws DAOException {
		List<PluginDTO> res = new ArrayList<PluginDTO>();

		try {
			DataValidationUtilities.checkIdentifier(pluginSpecificationId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = pluginDAO.getPluginsBySpecification(pluginSpecificationId);

		return res;
	}

	/**
	 * Obtiene la relación de plugins registrada en el sistema para una
	 * especificación de plugin determinada, retornando además los parámetros de
	 * cada configuración.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de especificación de plugin.
	 * @return Mapa de configuraciones y parámetros de una especificación de
	 *         plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public Map<PluginDTO, List<ParameterDTO>> getPluginsAndParamsBySpecification(Long pluginSpecificationId) throws DAOException {
		List<PluginDTO> plugins;
		Map<PluginDTO, List<ParameterDTO>> res = new HashMap<PluginDTO, List<ParameterDTO>>();

		try {
			DataValidationUtilities.checkIdentifier(pluginSpecificationId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		plugins = pluginDAO.getPluginsBySpecification(pluginSpecificationId);

		for (PluginDTO plugin: plugins) {
			res.put(plugin, pluginDAO.getParametersByPlugin(plugin.getId()));
		}

		return res;
	}

	/**
	 * Crea un nuevo plugin en el sistema, sin parámetros.
	 * 
	 * @param plugin
	 *            Nuevo plugin.
	 * @param pluginSpecId
	 *            Identificador de la especificación de plugin implementada.
	 * @return El plugin creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public PluginDTO createPlugin(PluginDTO plugin, Long pluginSpecId) throws DAOException {
		PluginDTO res = null;

		res = createPlugin(plugin, new ArrayList<ParameterDTO>(), pluginSpecId);

		return res;
	}

	/**
	 * Crea un nuevo plugin en el sistema, registrando además sus parámetros.
	 * 
	 * @param plugin
	 *            Nuevo plugin.
	 * @param parameters
	 *            Relación de parámetros de la configuración o plugin.
	 * @param pluginSpecId
	 *            Identificador de la especificación de plugin implementada.
	 * @return El plugin creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public PluginDTO createPlugin(PluginDTO plugin, List<ParameterDTO> parameters, Long pluginSpecId) throws DAOException {
		List<ParameterSpecificationDTO> parameterSpecs;
		List<ParameterTypeDTO> paramTypes;
		Long pluginId;
		Map<ParameterSpecificationDTO, ParameterDTO> parametersConf;
		PluginDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la creación de un nuevo plugin, denominado " + (plugin.getName() != null ? plugin.getName() : "No especificado") + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se validan los datos del nuevo plugin, denominado " + (plugin.getName() != null ? plugin.getName() : "No especificado") + " ...");
		}
		// Validaciones de datos: especificación de plugin y parámetros
		try {
			DataValidationUtilities.checkIdentifier(pluginSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		DataValidationUtilities.checkPlugin(plugin);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recupera la especificación de los parámetros del nuevo plugin, denominado " + plugin.getName() + " ...");
		}
		parameterSpecs = pluginDAO.getParameterSpecificationsByPluginSpec(pluginSpecId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Especificación de plugin y plugins recuperadas para el plugin " + plugin.getName() + " ...");
		}
		paramTypes = pluginDAO.listParameterTypes();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros del nuevo plugin, denominado " + (plugin.getName() != null ? plugin.getName() : "No especificado") + " ...");
		}
		parametersConf = DataValidationUtilities.checkParameters((parameters == null ? new ArrayList<ParameterDTO>() : parameters), parameterSpecs, plugin, paramTypes);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persiste el nuevo plugin, denominado " + plugin.getName() + ", en BBDD ...");
		}
		pluginId = pluginDAO.createPlugin(plugin, pluginSpecId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten los parámetros del nuevo plugin, denominado " + plugin.getName() + ", en BBDD ...");
		}
		pluginDAO.setParameters(parametersConf, pluginId);

		res = pluginDAO.getPlugin(pluginId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Nuevo plugin, denominado " + plugin.getName() + ", creado con identificador: " + res.getId() + ".");
		}
		return res;
	}

	/**
	 * Actualiza un plugin registrado en el sistema.
	 * 
	 * @param plugin
	 *            Plugin.
	 * @param propagateActivation
	 *            Indica si los cambios sobre el plugin serán propagados a los
	 *            perfiles de digitalización que los utilizan.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updatePlugin(PluginDTO plugin, Boolean propagateActivation) throws DAOException {

		updatePlugin(plugin, pluginDAO.getParametersByPlugin(plugin.getId()), propagateActivation);
	}

	/**
	 * Actualiza un plugin registrado en el sistema, estableciendo además su
	 * nueva relación de parámetros.
	 * 
	 * @param plugin
	 *            Plugin.
	 * @param parameters
	 *            Nueva relación de parámetros del plugin.
	 * @param propagateActivation
	 *            Indica si los cambios sobre el plugin serán propagados a los
	 *            perfiles de digitalización que los utilizan.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updatePlugin(PluginDTO plugin, List<ParameterDTO> parameters, Boolean propagateActivation) throws DAOException {
		List<ParameterSpecificationDTO> parameterSpecs;
		List<ParameterTypeDTO> paramTypes;
		Map<ParameterSpecificationDTO, ParameterDTO> parametersConf;
		PluginSpecificationDTO pluginSpec;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización del plugin, denominado " + plugin.getName() + " ...");
			LOG.debug("[WEBSCAN-MODEL] Se validan los datos actualizados del plugin, denominado " + plugin.getName() + " ...");
		}
		// Validaciones de datos: especificación de plugin y parámetros
		DataValidationUtilities.checkPlugin(plugin);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recupera la especificación de un nuevo plugin, denominado " + plugin.getName() + " ...");
		}
		pluginSpec = pluginDAO.getPluginSpecificationByName(plugin.getSpecification());
		parameterSpecs = pluginDAO.getParameterSpecificationsByPluginSpec(pluginSpec.getId());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Especificación de plugin y plugins recuperadas para el plugin " + plugin.getName() + " ...");
		}
		paramTypes = pluginDAO.listParameterTypes();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros del nuevo plugin, denominado " + (plugin.getName() != null ? plugin.getName() : "No especificado") + " ...");
		}
		parametersConf = DataValidationUtilities.checkParameters((parameters == null ? new ArrayList<ParameterDTO>() : parameters), parameterSpecs, plugin, paramTypes);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten los cambios del plugin '" + plugin.getName() + "', y parámetros en BBDD ...");
		}
		pluginDAO.updatePlugin(plugin);

		pluginDAO.setParameters(parametersConf, plugin.getId());

		if (propagateActivation) {
			updateScanProfOrgUnitPluignActivationStatus(plugin);
		}
	}

	/**
	 * Propaga el estado de activación de un plugin a sus configuraciones en
	 * perfil de digitalización y unidad organizativa.
	 * 
	 * @param plugin
	 *            plugin para el cual será propagado el estado de activación.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de las sentencias
	 *             de BBDD.
	 */
	private void updateScanProfOrgUnitPluignActivationStatus(PluginDTO plugin) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se ha solicitado la propagación del estado de activación de los plugins en perfiles y unidades organizativas, " + "denominada " + plugin.getName() + " (Activo: " + plugin.getActive() + ")...");
		}
		List<ScanProfileOrgUnitPluginDTO> updatingScanProfilePlugins = scanProfileDAO.getScanProfileOrgUnitPluginsByPlugin(plugin.getId());
		if (updatingScanProfilePlugins != null && !updatingScanProfilePlugins.isEmpty()) {

			for (ScanProfileOrgUnitPluginDTO spoupDto: updatingScanProfilePlugins) {
				spoupDto.setActive(plugin.getActive());
			}

			scanProfileDAO.updateScanProfileOrgUnitPlugins(updatingScanProfilePlugins);
		}
	}

	/**
	 * Obtiene la relación de parámetros registrada en el sistema para una
	 * especificación de plugin determinada.
	 * 
	 * @param pluginSpecId
	 *            Identificador de especificación de plugin.
	 * @return Lista de parámetros de una especificación de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public List<ParameterSpecificationDTO> getPluginSpecParameterSpecList(Long pluginSpecId) throws DAOException {
		List<ParameterSpecificationDTO> res = new ArrayList<ParameterSpecificationDTO>();

		try {
			DataValidationUtilities.checkIdentifier(pluginSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = pluginDAO.getParameterSpecificationsByPluginSpec(pluginSpecId);

		return res;
	}

	/**
	 * Comprueba si un plugin posee documentos en proceso de digitalización.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public Boolean hasPluginDocsInScanProccess(Long pluginId) throws DAOException {
		Boolean res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la comprobación de documentos en proceso de digitalización para el plugin " + pluginId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(pluginId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = pluginDAO.hasPluginDocsInScanProccess(pluginId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Finalizada la comprobación de documentos en proceso de digitalización para el plugin " + pluginId + ". Resultado: " + res);
		}
		return res;
	}

	/**
	 * Elimina un plugin del sistema.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void removePlugin(Long pluginId) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia el borrado del plugin " + pluginId + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se valida los datos del plugin " + pluginId + " ...");
		}
		// Validaciones de datos: especificación de plugin y parámetros
		try {
			DataValidationUtilities.checkIdentifier(pluginId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se elimina el plugin " + pluginId + ", y parámetros en BBDD ...");
		}
		pluginDAO.removePlugin(pluginId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Plugin " + pluginId + " eliminado de BBDD ...");
		}
	}

	/**
	 * Recupera la relación de tipos de parámetros registrada en el sistema.
	 * 
	 * @return Listado de tipos de parámetros registrada en el sistema.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<ParameterTypeDTO> listParameterTypes() throws DAOException {
		List<ParameterTypeDTO> res = new ArrayList<ParameterTypeDTO>();

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de tipos de parámetros ...");

		res = pluginDAO.listParameterTypes();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " tipos de parámetros recuperados.");
		}
		return res;
	}

	/**
	 * Recupera la relación de parámetros registrada en el sistema para un
	 * plugin determinado.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @return Lista de parámetros de un plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la consulta a BBDD.
	 */
	public List<ParameterDTO> getParametersByPlugin(Long pluginId) throws DAOException {
		List<ParameterDTO> res = new ArrayList<ParameterDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los parámetros para el plugin " + pluginId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkIdentifier(pluginId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando parámetros de BBDD para el plugin " + pluginId + " ...");
		}
		res = pluginDAO.getParametersByPlugin(pluginId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " parámetros recuperados para el plugin " + pluginId + ".");
		}
		return res;
	}

	/*************************************************************************************/
	/*					Especificaciones de documentos y documentos						 */
	/*************************************************************************************/

	/**
	 * Obtiene el listado de definiciones de documentos registradas en el
	 * sistema, permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de definiciones de documentos.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<DocumentSpecificationDTO> listDocSpecifications(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		List<DocumentSpecificationDTO> res = new ArrayList<DocumentSpecificationDTO>();

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las definiciones de documentos registradas en el sistema ...");

		DataValidationUtilities.checkDocumentSpecGroupByClause(orderByColumns);

		res = documentDAO.listDocumentSpecifications(pageNumber, elementsPerPage, orderByColumns);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Numero de definiciones de documentos registradas en el sistema: " + res.size() + " ...");
		}
		return res;
	}

	/**
	 * Obtiene la especificación del documento registrada en el sistema.
	 * 
	 * @param orgScanProfile
	 *            Identificador de la configuración de especificación de
	 *            documento, perfil de digitalización y unidad organizativa
	 *            empleada en el proceso de digitalización.
	 * @return Objeto de la especificación del documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public DocumentSpecificationDTO getDocSpecificationByOrgScanProfile(Long orgScanProfile) {
		DocumentSpecificationDTO result = new DocumentSpecificationDTO();

		LOG.debug("[WEBSCAN-MODEL] Inicio de la búsqueda de la especificacion del documento...");

		try {
			result = documentDAO.getDocumentSpecificationByScanProfileOrgUnitDocSpec(orgScanProfile);
		} catch (DAOException e) {
			LOG.debug("[WEBSCAN-MODEL] Error en la busqyeda de la especificacion del documento...");
		}

		return result;

	}

	/**
	 * Recupera una especificación de documento a partir de su identificador.
	 * 
	 * @param docSpecificationId
	 *            Identificador de especificación de documento.
	 * @return La definición de documento solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public DocumentSpecificationDTO getDocSpecification(Long docSpecificationId) throws DAOException {
		DocumentSpecificationDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la definición de documento con identificador " + docSpecificationId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(docSpecificationId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = documentDAO.getDocumentSpecification(docSpecificationId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperación de la definición de documento con identificador " + docSpecificationId + " finalizada.");
		}
		return res;
	}

	/**
	 * Recupera una especificación de documento a partir de su denominación.
	 * 
	 * @param docSpecificationName
	 *            Denominación de especificación de documento.
	 * @return La definición de documento solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public DocumentSpecificationDTO getDocSpecificationByName(String docSpecificationName) throws DAOException {
		DocumentSpecificationDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la definición de documento denominada " + docSpecificationName + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(docSpecificationName);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = documentDAO.getDocumentSpecificationByName(docSpecificationName);

		return res;
	}

	/**
	 * Recupera las especificaciones de documentos asociadas a una configuración
	 * de perfil de digitalización en unidad organizativa.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Relación de pares clave / valor, en la que cada entrada es
	 *         identificada por una definición de documentos establecida para la
	 *         configuración de perfil de digitalización en unidad organizativa
	 *         especificada como parámetro de entrada, y cuyo valor se
	 *         corresponde con la relación de asociación de la definición de
	 *         documentos a la configuración de perfil en unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getDocSpecificationsByScanProfileOrgUnitId(Long scanProfileOrgUnitId) throws DAOException {
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> res = new HashMap<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las definiciones de documentos configuradas para el perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkIdentifier(scanProfileOrgUnitId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = scanProfileDAO.getDocSpecificationsByScanProfileOrgUnitId(scanProfileOrgUnitId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " definiciones de documentos configuradas para el perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " recuperadas.");
		}
		return res;
	}

	/**
	 * Recupera las especificaciones de documentos asociadas a una configuración
	 * de perfil de digitalización en unidad organizativa que esten activas
	 * (configuración en perfil y definición de documento).
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Relación de pares clave / valor, en la que cada entrada es
	 *         identificada por una definición de documentos establecida para la
	 *         configuración de perfil de digitalización en unidad organizativa
	 *         especificada como parámetro de entrada, y cuyo valor se
	 *         corresponde con la relación de asociación de la definición de
	 *         documentos a la configuración de perfil en unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getActiveDocSpecificationsByScanProfileOrgUnitId(Long scanProfileOrgUnitId) throws DAOException {
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> res = new HashMap<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las definiciones de documentos configuradas para el perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkIdentifier(scanProfileOrgUnitId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = scanProfileDAO.getActiveDocSpecificationsByScanProfileOrgUnitId(scanProfileOrgUnitId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " definiciones de documentos configuradas para el perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " recuperadas.");
		}
		return res;
	}

	/**
	 * Crea una nueva definición de documento en el sistema, sin metadatos.
	 * 
	 * @param specification
	 *            Nueva definición de documento.
	 * @return La definición de documento creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public DocumentSpecificationDTO createDocSpecification(DocumentSpecificationDTO specification) throws DAOException {
		DocumentSpecificationDTO res = null;

		res = createDocSpecification(specification, new ArrayList<MetadataSpecificationDTO>(), new ArrayList<DocumentSpecificationDTO>(), new ArrayList<DocumentSpecificationDTO>());

		return res;
	}

	/**
	 * Crea una nueva definición de documento en el sistema, registrando además
	 * sus metadatos, las relaciones de especificaciones padre (de las que
	 * hereda), y especificaciones hija (las que heredan).
	 * 
	 * @param specification
	 *            Nueva definición de plugin.
	 * @param metadataSpecs
	 *            Metadatos de la nueva definición de documento.
	 * @param parentDocSpecs
	 *            Especificaciones de documentos de las que heredan la
	 *            especificación a crear.
	 * @param childDocSpecs
	 *            Especificaciones de documentos que heredan de la
	 *            especificación a crear.
	 * @return La definición de documentos creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public DocumentSpecificationDTO createDocSpecification(DocumentSpecificationDTO specification, List<MetadataSpecificationDTO> metadataSpecs, List<DocumentSpecificationDTO> parentDocSpecs, List<DocumentSpecificationDTO> childDocSpecs) throws DAOException {
		Long dsId;
		DocumentSpecificationDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la creación de una nueva definición de documento, denominada " + specification.getName() + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se validan los datos de la nueva definición de documento, denominada " + specification.getName() + " ...");
		}
		// Validaciones de datos: especificación de documento y metadatos
		DataValidationUtilities.checkDocSpecification(specification);
		DataValidationUtilities.checkMetadataSpecifications(metadataSpecs, specification.getId());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se valida la lista de especificaciones de la que hereda la nueva definición de documento, denominada " + specification.getName() + " ...");
		}
		DataValidationUtilities.checkDocSpecifications(parentDocSpecs);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se valida la lista de especificaciones que heredan de la nueva definición de documento, denominada " + specification.getName() + " ...");
		}
		DataValidationUtilities.checkDocSpecifications(childDocSpecs);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persiste la nueva definición de documento '" + specification.getName() + "', y sus parámetros en BBDD ...");
		}
		dsId = documentDAO.createDocumentSpecification(specification);
		if (metadataSpecs != null && !metadataSpecs.isEmpty()) {
			documentDAO.setMetadataSpecifications(metadataSpecs, dsId);
		}

		if (parentDocSpecs != null && !parentDocSpecs.isEmpty()) {
			documentDAO.setParentDocSpecifications(parentDocSpecs, dsId);
		}

		if (childDocSpecs != null && !childDocSpecs.isEmpty()) {
			documentDAO.setChildDocSpecifications(childDocSpecs, dsId);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recupera la nueva definición de documento, denominada " + specification.getName() + ", de BBDD ...");
		}
		res = documentDAO.getDocumentSpecification(dsId);

		return res;
	}

	/**
	 * Actualiza una especificación de documento registrada en el sistema.
	 * 
	 * @param specification
	 *            Especificación de plugin.
	 * @param propagateActivation
	 *            Indica si los cambios sobre la definición de documento serán
	 *            propagados a la relación de configuraciones o
	 *            parametrizaciones de la definición (plugins).
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe alguna
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updateDocSpecification(DocumentSpecificationDTO specification, Boolean propagateActivation) throws DAOException {
		// Validaciones de datos: especificación de documento y metadatos
		DataValidationUtilities.checkDocSpecification(specification);

		updateDocSpecification(specification, documentDAO.listMetadataSpecsByDocSpec(specification.getId()), documentDAO.getParentDocSpecifications(specification.getId()), documentDAO.getChildDocSpecifications(specification.getId()), propagateActivation);
	}

	/**
	 * Actualiza una definición de documento registrada en el sistema,
	 * estableciéndole una nueva relación de metadatos, y nuevas relaciones de
	 * especificaciones padre (de las que hereda), y especificaciones hija (las
	 * que heredan).
	 * 
	 * @param specification
	 *            Nueva definición de plugin.
	 * @param metadataSpecs
	 *            Metadatos de la nueva definición de documento.
	 * @param parentDocSpecs
	 *            Especificaciones de documentos de las que heredan la
	 *            especificación a crear.
	 * @param childDocSpecs
	 *            Especificaciones de documentos que heredan de la
	 *            especificación a crear.
	 * @param propagateActivation
	 *            Indica si los cambios sobre la definición de documento serán
	 *            propagados a la relación de configuraciones o
	 *            parametrizaciones de la definición (plugins).
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, es deshabilitada y tiene documentos en
	 *             digitalización, o se produce algún error en la ejecución de
	 *             la sentencia de BBDD.
	 */
	public void updateDocSpecification(DocumentSpecificationDTO specification, List<MetadataSpecificationDTO> metadataSpecs, List<DocumentSpecificationDTO> parentDocSpecs, List<DocumentSpecificationDTO> childDocSpecs, Boolean propagateActivation) throws DAOException {
		Boolean isDocSpecDisabled;
		DocumentSpecificationDTO bbddDocSpec;
		List<ScannedDocumentDTO> updSpecScannedDocs;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización de la definición de documento, denominada " + specification.getName() + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se validan los datos actualizados de la definición de documento, denominada " + specification.getName() + " ...");
		}
		// Validaciones de datos: especificación de documento y metadatos
		DataValidationUtilities.checkDocSpecification(specification);
		DataValidationUtilities.checkMetadataSpecifications(metadataSpecs, specification.getId());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se valida la lista de especificaciones de la que hereda la definición de documento, denominada " + specification.getName() + " ...");
		}
		DataValidationUtilities.checkDocSpecifications(parentDocSpecs);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se valida la lista de especificaciones que heredan de la definición de documento, denominada " + specification.getName() + " ...");
		}
		DataValidationUtilities.checkDocSpecifications(childDocSpecs);

		bbddDocSpec = documentDAO.getDocumentSpecification(specification.getId());
		isDocSpecDisabled = bbddDocSpec.getActive() && !specification.getActive();

		updSpecScannedDocs = documentDAO.getDocumentsNotScannedByDocSpecId(specification.getId());
		if (isDocSpecDisabled && (updSpecScannedDocs != null && !updSpecScannedDocs.isEmpty())) {
			throw new DAOException(DAOException.CODE_201, "La definición de documentos " + specification.getName() + " no puede ser deshabilitada dado que existen " + updSpecScannedDocs.size() + " documentos en proceso de digitalización que la emplean.");
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten los cambios de la definición de documento '" + specification.getName() + "', sus configuraciones y parámetros en BBDD ...");
		}
		documentDAO.updateDocumentSpecification(specification);

		documentDAO.setMetadataSpecifications((metadataSpecs == null ? new ArrayList<MetadataSpecificationDTO>() : metadataSpecs), specification.getId());

		documentDAO.setParentDocSpecifications((parentDocSpecs == null ? new ArrayList<DocumentSpecificationDTO>() : parentDocSpecs), specification.getId());

		documentDAO.setChildDocSpecifications((childDocSpecs == null ? new ArrayList<DocumentSpecificationDTO>() : childDocSpecs), specification.getId());

		if (isDocSpecDisabled) {
			// Se eliminan metadatos de documentos en los que esta definición
			// este
			// referenciada como padre de la especificación del documento
			removeScannedDocMetadataByUpdateDocSpec(specification);
		}

		if (propagateActivation) {
			// Propagación en perfiles de digitalización configurados en
			// unidades organizativas
			scanProfOrgUnitDocSpecUpadatePropagation(specification);

		}
	}

	/**
	 * Elimina los metadatos definidos por las especificaciones de metadatos
	 * agrupadas por un tipo documental.
	 * 
	 * @param specification
	 *            Definición de documentos.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	private void removeScannedDocMetadataByUpdateDocSpec(DocumentSpecificationDTO specification) throws DAOException {
		List<MetadataSpecificationDTO> docSpecMetadata;

		// Se obtienen los metadatos de la definición
		docSpecMetadata = documentDAO.listMetadataSpecsByDocSpec(specification.getId());
		if (docSpecMetadata != null && !docSpecMetadata.isEmpty()) {
			// Se eliminan de los documentos los metadatos definidos por las
			// especificaciones de metadatos
			documentDAO.removeMetadataCollectionByMetadataSpecList(docSpecMetadata);
		}
	}

	/**
	 * Actualiza el estado de activación de una definición de documentos en las
	 * configuraciones de perfil en unidad organizativa donde es usada.
	 * 
	 * @param specification
	 *            Definición de de documentos.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	private void scanProfOrgUnitDocSpecUpadatePropagation(DocumentSpecificationDTO specification) throws DAOException {
		Boolean activeDocSpec;
		Long scanProfileOrgUnitId;
		Map<Long, Map<Long, Boolean>> upadatedScanProfiles;

		activeDocSpec = specification.getActive();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se ha solicitado la propagación del estado de activación a las especificaciones de documentos en perfiles, cuya definición " + "se denominada " + specification.getName() + " (Activo: " + activeDocSpec + ")...");
		}
		List<ScanProfileOrgUnitDocSpecDTO> updatingScanProfileDocs = scanProfileDAO.getScanProfileOrgUnitDocSpecsByDocSpec(specification.getId());
		if (updatingScanProfileDocs != null && !updatingScanProfileDocs.isEmpty()) {

			upadatedScanProfiles = new HashMap<Long, Map<Long, Boolean>>();

			Map<Long, Boolean> orgDocSpecs;

			for (ScanProfileOrgUnitDocSpecDTO spdDto: updatingScanProfileDocs) {
				scanProfileOrgUnitId = spdDto.getOrgScanProfile();

				orgDocSpecs = upadatedScanProfiles.get(scanProfileOrgUnitId);
				if (orgDocSpecs == null) {
					orgDocSpecs = new HashMap<Long, Boolean>();
				}
				orgDocSpecs.put(spdDto.getDocSpecification(), activeDocSpec);
				upadatedScanProfiles.put(scanProfileOrgUnitId, orgDocSpecs);
			}

			for (Iterator<Long> it = upadatedScanProfiles.keySet().iterator(); it.hasNext();) {
				scanProfileOrgUnitId = it.next();
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Se actualiza el estado de activación de la definción de documento " + specification.getName() + " configurada para el perfil de digitalización y unidad organizativa " + scanProfileOrgUnitId + " (" + activeDocSpec + ") en BBDD ...");
				}
				scanProfileDAO.setScanProfileOrgUnitDocSpecs(upadatedScanProfiles.get(scanProfileOrgUnitId), scanProfileOrgUnitId);

			}
		}
	}

	/**
	 * Elimina una lista de definiciones de documento del sistema.
	 * 
	 * @param specificationIds
	 *            Relación de identificadores de especificación de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe alguna
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void removeDocSpecification(List<Long> specificationIds) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia el borrado de la lista de definiciones de documentos " + specificationIds + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se valida los datos la lista de definiciones de documentos " + specificationIds + " ...");
		}
		// Validaciones de datos: especificación de documentos
		try {
			DataValidationUtilities.checkCollection(specificationIds);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se elimina la lista de definiciones de documentos " + specificationIds + " en BBDD ...");
		}
		documentDAO.removeDocumentSpecifications(specificationIds);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Lista de definiciones de documentos " + specificationIds + " eliminada de BBDD ...");
		}
	}

	/**
	 * Comprueba si una especificación de documentos posee documentos en proceso
	 * de digitalización.
	 * 
	 * @param docSpecId
	 *            Identificador de especificación de documentos.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de documentos, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	public Boolean hasDocSpecificationDocsInScanProccess(Long docSpecId) throws DAOException {
		Boolean res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la comprobación de documentos en proceso de digitalización para la especificación de documentos " + docSpecId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(docSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = documentDAO.hasDocSpecificationDocsInScanProccess(docSpecId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Finalizada la comprobación de documentos en proceso de digitalización para la especificación de documentos " + docSpecId + ". Resultado: " + res);
		}
		return res;
	}

	/**
	 * Recupera las especificaciones de documentos padre de una especificación
	 * de documento determinada.
	 * 
	 * @param docSpecificationId
	 *            Identificador de especificación de documento.
	 * @return Lista de definiciones de documentos padre de la definición de
	 *         documento especificada como parámetro de entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public List<DocumentSpecificationDTO> getParentDocSpecifications(Long docSpecificationId) throws DAOException {
		List<DocumentSpecificationDTO> res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las definiciones de documentos de las que hereda la especificación de documento " + docSpecificationId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(docSpecificationId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se lanza consulta sobre BBDD para recuperar las definiciones de documentos de las que hereda la especificación de documento " + docSpecificationId + " ...");
		}
		res = documentDAO.getParentDocSpecifications(docSpecificationId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " de definiciones de documentos de las que hereda la especificación de documento " + docSpecificationId + " ...");
		}
		return res;
	}

	/**
	 * Recupera las especificaciones de documentos hija de una especificación de
	 * documento determinada.
	 * 
	 * @param docSpecificationId
	 *            Identificador de especificación de documento.
	 * @return Lista de definiciones de documentos hija de la definición de
	 *         documento especificada como parámetro de entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public List<DocumentSpecificationDTO> getChildDocSpecifications(Long docSpecificationId) throws DAOException {
		List<DocumentSpecificationDTO> res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las definiciones de documentos que heredan de la especificación de documento " + docSpecificationId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(docSpecificationId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se lanza consulta sobre BBDD para recuperar las definiciones de documentos que heredan la especificación de documento " + docSpecificationId + " ...");
		}
		res = documentDAO.getChildDocSpecifications(docSpecificationId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " de definiciones de documentos que heredan de la especificación de documento " + docSpecificationId + " ...");
		}
		return res;
	}

	/**
	 * Recupera la relación de metadatos registrada en el sistema para una
	 * especificación de documento determinada, incluyendo los metdatos
	 * pertenecientes a las especificaciones de las que hereda.
	 * 
	 * @param docSpecId
	 *            Identificador de especificación de documento.
	 * @return Mapa en el que cada entrada se encuentra identificada por la
	 *         denominación de una especificación documento, y cuyo valor se
	 *         corresponde con la lista de metadatos asociada a la
	 *         especificación que identifica la entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public Map<String, List<MetadataSpecificationDTO>> getMetadaSpecificationsByDocSpec(Long docSpecId) throws DAOException {
		int resSize = 0;
		DocumentSpecificationDTO docSpec;
		List<DocumentSpecificationDTO> parentDocSpecs;
		List<MetadataSpecificationDTO> metadataSpecs;
		Map<String, List<MetadataSpecificationDTO>> res = new HashMap<String, List<MetadataSpecificationDTO>>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las definiciones de metadatos para la especificación de documento " + docSpecId + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(docSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		docSpec = documentDAO.getDocumentSpecification(docSpecId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la definición de documento con identificador " + docSpecId + ".");
			LOG.debug("[WEBSCAN-MODEL] Se recupera la lista de definiciones de metadatos para la especificación de documento " + docSpec.getName() + ".");
		}
		metadataSpecs = documentDAO.listMetadataSpecsByDocSpec(docSpecId);

		res.put(docSpec.getName(), metadataSpecs);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recuperan las lista de definiciones de metadatos de las especificaciones de las que hereda la definción de documento " + docSpec.getName() + ".");
		}
		parentDocSpecs = documentDAO.getParentDocSpecifications(docSpec.getId());

		for (DocumentSpecificationDTO dsDto: parentDocSpecs) {
			metadataSpecs = documentDAO.listMetadataSpecsByDocSpec(dsDto.getId());
			res.put(dsDto.getName(), metadataSpecs);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Numero de definiciones de metadatos recuperadas para la definición de documento " + docSpec.getName() + ": " + resSize + " ...");
		}
		return res;
	}

	/**
	 * Recupera un documento digitalizado a partir de su identificador.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return El documento digitalizado solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la consulta a BBDD.
	 */
	public ScannedDocumentDTO getDocument(Long scannedDocId) throws DAOException {
		byte[ ] docContent;
		Map<MimeType, String> docContentStorePaths;
		MimeType mimeType;
		ScannedDocsStoreMode docStoreMode;
		ScannedDocumentDTO res = null;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación del documento con identificador " + scannedDocId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(scannedDocId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = documentDAO.getDocument(scannedDocId);

		// aqui nos viene o bien, filesystem o ddbb
		docStoreMode = ScannedDocsStoreMode.getByName(webscanUtilParams.getProperty(WebscanConstants.SCANNED_DOCS_STORE_MODE_PROP_NAME));

		if (ScannedDocsStoreMode.FILE_SYSTEM.equals(docStoreMode)) {
			// Los contenidos del documento están en el sistema de archivos
			try {
				docContentStorePaths = documentDAO.getDocContentStorePaths(res.getId());
				for (Iterator<MimeType> it = docContentStorePaths.keySet().iterator(); it.hasNext();) {
					mimeType = it.next();
					docContent = FileSystemUtilities.readFile(docContentStorePaths.get(mimeType));
					if (WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE.equals(mimeType.getContentType())) {
						res.setContent(docContent);
					} else if (WebscanConstants.SIGNATURE_MIME_TYPE_CONTENT_TYPE.equals(mimeType.getContentType())) {
						res.setSignature(docContent);
					} else if (WebscanConstants.OCR_MIME_TYPE_CONTENT_TYPE.equals(mimeType.getContentType())) {
						res.setOcr(docContent);
					}
				}
			} catch (WebscanUtilitiesException e) {
				excMsg = "Se produjo un error al recuperar del sistema de archivos los contenidos del documento " + scannedDocId + ". [" + e.getCode() + "] " + e.getMessage();
				throw new DAOException(DAOException.CODE_303, excMsg, e);
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperación del documento con identificador " + scannedDocId + ".");
		}
		return res;
	}

	/**
	 * Recupera la colección de metadatos con valor establecido de un documento
	 * digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return La colección de metadatos con valor establecido de un documento
	 *         digitalizado, incluyendo sus especificaciones.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la consulta a BBDD.
	 */
	public Map<MetadataSpecificationDTO, MetadataDTO> getDocumentMetadataCollection(Long scannedDocId) throws DAOException {
		Map<MetadataSpecificationDTO, MetadataDTO> res = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los metadatos del documento con identificador " + scannedDocId + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(scannedDocId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-MODEL] Se lanza la consulta ...");

		res = documentDAO.listMetadataCollectionAndSpecByDocument(scannedDocId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " metadatos con valor asignado para el documento con identificador " + scannedDocId + ".");
		}
		return res;
	}

	/**
	 * Recupera la colección total de metadatos de un documento digitalizado,
	 * tengan o no valor ya establecido. Las especificaciones sin valor
	 * establecido tendrán como valor asociado un objeto MetadataDTO vacío.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return La colección completa de metadatos de un documento digitalizado,
	 *         incluyendo sus especificaciones.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la consulta a BBDD.
	 */
	public Map<MetadataSpecificationDTO, MetadataDTO> getCompleteDocumentMetadataCollection(Long scannedDocId) throws DAOException {
		Map<MetadataSpecificationDTO, MetadataDTO> res = new HashMap<MetadataSpecificationDTO, MetadataDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la colección total de metadatos del documento con identificador " + scannedDocId + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(scannedDocId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		LOG.debug("[WEBSCAN-MODEL] Se lanza la consulta ...");

		res = documentDAO.listCompleteMetadataCollectionAndSpecByDocument(scannedDocId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " metadatos para el documento con identificador " + scannedDocId + ".");
		}
		return res;
	}

	/**
	 * Recupera un listado de documentos digitalizados pertenecientes a una
	 * unidad organizativa determinada, permitiendo la paginación y ordenación
	 * del resultado. Los documentos retornados no incluyen su contenido binario
	 * y firma.
	 *
	 * @param orgUnitIds
	 *            Identificadores de unidades organizativas.
	 * @param functionalOrgs
	 *            unidades orgánicas por las que filtrar los los documentos a
	 *            recuperar.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de documentos digitalizados.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<ScannedDocumentDTO> getDocumentsByOrgUnits(List<Long> orgUnitIds, String functionalOrgs, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		int queryResultSize = 0;
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los documentos digitalizados por usuarios de las unidades organizativas " + orgUnitIds + " y pertenecientes a la unidades orgánicas: " + functionalOrgs + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkCollection(orgUnitIds);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		DataValidationUtilities.checkDocumentGroupByClause(orderByColumns);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recuperan los documentos digitalizados por usuarios de las unidades organizativas " + orgUnitIds + " y pertenecientes a la unidades orgánicas: " + functionalOrgs + " ...");
		}
		res = documentDAO.getDocumentsByOrgUnits(orgUnitIds, functionalOrgs, pageNumber, elementsPerPage, orderByColumns);

		if (res != null) {
			queryResultSize = res.size();
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + queryResultSize + " documentos digitalizados por usuarios de las unidades organizativas " + orgUnitIds + " y pertenecientes a la unidades orgánicas: " + functionalOrgs + " recuperados de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera un listado de documentos digitalizados, y sus trazas de
	 * ejcución, pertenecientes a una unidad organizativa determinada,
	 * permitiendo la paginación y ordenación del resultado. Los documentos
	 * retornados no incluyen su contenido binario y firma.
	 *
	 * @param orgUnitIds
	 *            Identificadores de unidades organizativas.
	 * @param functionalOrgs
	 *            unidades orgánicas por las que filtrar los documentos a
	 *            recuperar.
	 * @return listado de documentos digitalizados.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<DocBoxScannedDocDTO> getDocumentsAndLastLogByOrgUnits(List<Long> orgUnitIds, String functionalOrgs) throws DAOException {
		int queryResultSize = 0;

		List<DocBoxScannedDocDTO> res = new ArrayList<DocBoxScannedDocDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los documentos digitalizados, y sus trazas de ejecución, por usuarios de las unidades organizativas " + orgUnitIds + " y pertenecientes a la unidades orgánicas: " + functionalOrgs + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkCollection(orgUnitIds);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recuperan los documentos digitalizados, y sus trazas de ejecución, por usuarios de las unidades organizativas " + orgUnitIds + " y pertenecientes a la unidades orgánicas: " + functionalOrgs + "...");
		}

		try {
			res = documentDAO.getDocumentsAndLogsByOrgUnits(orgUnitIds, functionalOrgs);

		} catch (IllegalArgumentException e) {
			LOG.debug("[WEBSCAN-MODEL] Argumento invalido al recuperar documentos digitalizados.");
			return res;
		}

		if (res != null) {
			queryResultSize = res.size();
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + queryResultSize + " documentos digitalizados por usuarios de las unidades organizativas " + orgUnitIds + " y pertenecientes a la unidades orgánicas: " + functionalOrgs + " recuperados de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera un listado de documentos digitalizados, y sus trazas de
	 * ejcución, pertenecientes a un proceso de digitalización determinado,
	 * permitiendo la paginación y ordenación del resultado. Los documentos
	 * retornados no incluyen su contenido binario y firma.
	 *
	 * @param batchId
	 *            Identificador de proceso de digitalización.
	 * @return listado de documentos digitalizados.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<DocBoxScannedDocDTO> getDocsByBatchId(Long batchId) throws DAOException {
		int queryResultSize = 0;

		List<DocBoxScannedDocDTO> res = new ArrayList<DocBoxScannedDocDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los documentos digitalizados, para el proceso de digitalización " + batchId + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");

		try {
			res = documentDAO.getDocsByBatchId(batchId);

		} catch (IllegalArgumentException e) {
			LOG.debug("[WEBSCAN-MODEL] Argumento invalido al recuperar documentos digitalizados.");
			return res;
		}

		if (res != null) {
			queryResultSize = res.size();
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + queryResultSize + " documentos digitalizados para el proceso de digitalización " + batchId + " recuperados de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera un listado de documentos definidos mediante una especificación
	 * de documentos, cuyo proceso de digitalización ha sido iniciado, pero no
	 * ha finalizado.
	 *
	 * @param docSpecId
	 *            Identificador de especificación de documentos.
	 * @return listado de documentos.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<ScannedDocumentDTO> getDocumentsNotScannedByDocSpecId(Long docSpecId) throws DAOException {
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(docSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = documentDAO.getDocumentsNotScannedByDocSpecId(docSpecId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + ", recuperados de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera la relación de documentos que están siendo procesados, mediante
	 * un proceso de digitalización dado, y cuyo procesamiento fue iniciado por
	 * un usuario determinado. Los documentos no incluyen su contenido y firma.
	 * 
	 * @param username
	 *            Nombre de usuario.
	 * @param scanProfileSpec
	 *            Nombre de proceso de digitalización.
	 * @return la relación de documentos que están siendo procesados, mediante
	 *         un proceso de digitalización dado, y cuyo procesamiento fue
	 *         iniciado por un usuario determinado.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public List<ScannedDocumentDTO> getActiveDocumentsByUsernameAndScanProfileSpec(String username, String scanProfileSpec) throws DAOException {
		List<EntityOrderByClause> orderByColumns = new ArrayList<EntityOrderByClause>();
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los documentos digitalizados en proceso para el usuario {} y el proceso de digitalización {} ...", username, scanProfileSpec);
		}

		try {
			DataValidationUtilities.checkIdentifier(username);
			DataValidationUtilities.checkIdentifier(scanProfileSpec);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		orderByColumns.add(DocumentOrderByClause.BATCH_ORDER_NUM_ASC);
		res = documentDAO.getActiveDocumentsByUsernameAndScanProfileSpec(username, scanProfileSpec, orderByColumns);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos digitalizados en proceso para el usuario {} y el proceso de digitalización {} recuperados.", username, scanProfileSpec);
		}

		return res;
	}

	/**
	 * Recupera la relación de documentos que están siendo procesados para una
	 * carátula o reserva dada. Los documentos no incluyen su contenido y firma.
	 * 
	 * @param batchReqId
	 *            Identificador de reserva de trabajo de digitalización o
	 *            carátula asociado a la digitalización de los documentos.
	 * @return la relación de documentos que están siendo procesados, para una
	 *         carátula o reserva dada, y cuyo procesamiento fue iniciado por un
	 *         usuario determinado.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public List<ScannedDocumentDTO> getActiveDocumentsByBatchReqId(String batchReqId) throws DAOException {
		List<EntityOrderByClause> orderByColumns = new ArrayList<EntityOrderByClause>();
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los documentos digitalizados en proceso para la caratula / reserva {} ...", batchReqId);
		}

		try {
			DataValidationUtilities.checkIdentifier(batchReqId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		orderByColumns.add(DocumentOrderByClause.BATCH_ORDER_NUM_ASC);
		res = documentDAO.getActiveDocumentsByBatchReqId(batchReqId, orderByColumns);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos digitalizados en proceso para la caratula / reserva {} ...", batchReqId);
		}

		return res;
	}
	
	/**
	 * Recupera la relación de documentos que están siendo procesados para un
	 * proceso de digitalización dado. Los documentos no incluyen su contenido y firma.
	 * 
	 * @param batchId
	 *            Identificador proceso de digitalización de los documentos.
	 * @return la relación de documentos que están siendo procesados, para una
	 *         carátula o reserva dada, y cuyo procesamiento fue iniciado por un
	 *         usuario determinado.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public List<ScannedDocumentDTO> getActiveDocumentsByBatchId(Long batchId) throws DAOException {
		List<EntityOrderByClause> orderByColumns = new ArrayList<EntityOrderByClause>();
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los documentos digitalizados en proceso para la caratula / reserva {} ...", batchId);
		}

		try {
			DataValidationUtilities.checkIdentifier(batchId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		orderByColumns.add(DocumentOrderByClause.BATCH_ORDER_NUM_ASC);
		res = documentDAO.getActiveDocumentsByBatchId(batchId, orderByColumns);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos digitalizados en proceso para la caratula / reserva {} ...", batchId);
		}

		return res;
	}
	/**
	 * Crea un nuevo documento digitalizado en el sistema, sin metadatos.
	 * 
	 * @param doc
	 *            Nuevo documento digitalizado.
	 * @param scanProfOrgUnitDocSpecId
	 *            identificador de la especificación de documentos asociada al
	 *            perfil de digitalización configurado en unidad organizativa.
	 * @return El documento creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public ScannedDocumentDTO createScannedDocument(ScannedDocumentDTO doc, Long scanProfOrgUnitDocSpecId, Long batchId) throws DAOException {
		ScannedDocumentDTO res;

		res = createScannedDocument(doc, new HashMap<MetadataSpecificationDTO, MetadataDTO>(), scanProfOrgUnitDocSpecId,batchId);

		return res;
	}

	/**
	 * Crea un nuevo documento digitalizado en el sistema, registrando además
	 * sus metadatos.
	 * 
	 * @param doc
	 *            Nuevo documento digitalizado.
	 * @param metadataCollection
	 *            Mapa de metadatos del nuevo documento digitalizado y sus
	 *            definiciones.
	 * @param scanProfOrgUnitDocSpecId
	 *            identificador de la especificación de documentos asociada al
	 *            perfil de digitalización configurado en un unidad
	 *            organizativa.
	 * @return El documento creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public ScannedDocumentDTO createScannedDocument(ScannedDocumentDTO doc, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection, Long scanProfOrgUnitDocSpecId, Long batchId) throws DAOException {
		Long docId;
		Map<MimeType, String> docContentStorePaths;
		ScannedDocsStoreMode docStoreMode;
		ScannedDocumentDTO auxDoc;
		ScannedDocumentDTO res;
		String docContentStorePath;
		String docRootPath;
		String excMsg;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la creación de un nuevo documento ...");

		LOG.debug("[WEBSCAN-MODEL] Se valida el nuevo documento y sus metadatos ...");
		// Validaciones de datos: especificación de documento y metadatos
		try {
			DataValidationUtilities.checkIdentifier(scanProfOrgUnitDocSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		DataValidationUtilities.checkDocument(doc, Boolean.TRUE);
		DataValidationUtilities.checkMetadataCollection(metadataCollection, doc.getId());

		LOG.debug("[WEBSCAN-MODEL] Se persiste el nuevo documento y sus metadatos en BBDD ...");

		auxDoc = new ScannedDocumentDTO();
		auxDoc.setAuditOpId(doc.getAuditOpId());
		auxDoc.setBatchId(doc.getBatchId());
		auxDoc.setBatchOrderNum(doc.getBatchOrderNum());
		auxDoc.setBatchReqId(doc.getBatchReqId());
		auxDoc.setDigProcEndDate(doc.getDigProcEndDate());
		auxDoc.setDigProcStartDate(doc.getDigProcStartDate());
		auxDoc.setChecked(doc.getChecked());
		auxDoc.setCheckDate(doc.getCheckDate());
		auxDoc.setFunctionalOrgs(doc.getFunctionalOrgs());
		try {
			auxDoc.setHash(CryptoUtilities.digest(webscanUtilParams.getProperty(WebscanConstants.SCANNED_DOCS_HASH_ALG_PROP_NAME, WebscanConstants.HASH_ALGORITHM_SHA256), doc.getContent()));
		} catch (WebscanUtilitiesException ex) {
			excMsg = "Se produjo un error al almacenar un documento en el modelo de datos. [" + ex.getCode() + "] " + ex.getMessage();
			LOG.error(excMsg, ex);
			throw new DAOException(DAOException.CODE_903, excMsg, ex);
		}
		auxDoc.setMimeType(doc.getMimeType());
		auxDoc.setName(doc.getName());
		auxDoc.setSignatureType(doc.getSignatureType());
		auxDoc.setUsername(doc.getUsername());

		docStoreMode = ScannedDocsStoreMode.getByName(webscanUtilParams.getProperty(WebscanConstants.SCANNED_DOCS_STORE_MODE_PROP_NAME));
		if (ScannedDocsStoreMode.DDBB.equals(docStoreMode)) {
			// Los contenidos del documento son almacenado en BBDD como LOB's
			auxDoc.setContent(doc.getContent());
			auxDoc.setOcr(doc.getOcr());
			auxDoc.setSignature(doc.getSignature());
			docId = documentDAO.createDocument(auxDoc, scanProfOrgUnitDocSpecId, batchId);
			//doc.getBatchId().getId());
		} else {
			// Los contenidos del documento son almacenado en el sistema de
			// archivos
			docId = documentDAO.createDocument(auxDoc, scanProfOrgUnitDocSpecId, batchId);
			//doc.getBatchId().getId());
			// Se guarda el contenido del documento en el sistema de archivos
			docContentStorePaths = new HashMap<MimeType, String>();
			try {
				docRootPath = webscanUtilParams.createScannedDocumentFileSystemRootPath(docId);
				docContentStorePath = webscanUtilParams.persistDocContentInFileSystem(docRootPath, docId, doc.getMimeType(), doc.getContent());
				docContentStorePaths.put(doc.getMimeType(), docContentStorePath);

				if (doc.getSignature() != null) {
					docContentStorePath = webscanUtilParams.persistDocContentInFileSystem(docRootPath, docId, MimeType.SIGNATURE, doc.getSignature());
					docContentStorePaths.put(MimeType.SIGNATURE, docContentStorePath);
				}

				if (doc.getOcr() != null) {
					docContentStorePath = webscanUtilParams.persistDocContentInFileSystem(docRootPath, docId, MimeType.TEXT_PLAIN_OCR, doc.getOcr());
					docContentStorePaths.put(MimeType.TEXT_PLAIN_OCR, docContentStorePath);
				}
			} catch (WebscanUtilitiesException e) {
				throw new DAOException(DAOException.CODE_300, e.getMessage(), e);
			}

			// Se actualiza la información en BBDD
			documentDAO.updateDocContentStorePaths(docId, docContentStorePaths);
		}

		if (metadataCollection != null && !metadataCollection.isEmpty()) {
			documentDAO.setMetadataCollection(metadataCollection, docId);
		}

		res = documentDAO.getDocument(docId);
		res.setContent(doc.getContent());
		res.setSignature(doc.getSignature());
		res.setOcr(doc.getOcr());

		return res;
	}

	/**
	 * Actualiza un listado de documentos registrados en el sistema.
	 * 
	 * @param docs
	 *            Relación de documentos digitalizados.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe algún
	 *             documento, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updateScannedDoc(List<ScannedDocumentDTO> docs) throws DAOException {
		Map<ScannedDocumentDTO, Map<MetadataSpecificationDTO, MetadataDTO>> docsMap = new HashMap<ScannedDocumentDTO, Map<MetadataSpecificationDTO, MetadataDTO>>();

		try {
			DataValidationUtilities.checkCollection(docs);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		for (Iterator<ScannedDocumentDTO> it = docs.iterator(); it.hasNext();) {
			ScannedDocumentDTO doc = it.next();
			docsMap.put(doc, documentDAO.listMetadataCollectionAndSpecByDocument(doc.getId()));
		}

		updateScannedDoc(docsMap);
	}

	/**
	 * Actualiza un listado de documentos registrados en el sistema,
	 * estableciendo además su nuevas relaciones de metadatos.
	 * 
	 * @param docs
	 *            Mapa de documentos (clave), y metadatos de cada documento
	 *            (valor).
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe algún
	 *             documento, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updateScannedDoc(Map<ScannedDocumentDTO, Map<MetadataSpecificationDTO, MetadataDTO>> docs) throws DAOException {
		List<ScannedDocumentDTO> docList;
		Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection;
		ScannedDocumentDTO doc;
		ScannedDocumentDTO auxDoc;
		ScannedDocsStoreMode docStoreMode;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización de documentos ...");

		LOG.debug("[WEBSCAN-MODEL] Se validan los datos actualizados de los documentos ...");

		// Validaciones de datos: documento y metadatos
		try {
			DataValidationUtilities.checkCollection(docs.keySet());
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		DataValidationUtilities.checkDocuments(docs.keySet(), Boolean.FALSE);

		LOG.debug("[WEBSCAN-MODEL] Se persisten los cambios de los documentos y sus metadatos en BBDD ...");
		docList = new ArrayList<ScannedDocumentDTO>();
		docStoreMode = ScannedDocsStoreMode.getByName(webscanUtilParams.getProperty(WebscanConstants.SCANNED_DOCS_STORE_MODE_PROP_NAME));

		for (Iterator<ScannedDocumentDTO> it = docs.keySet().iterator(); it.hasNext();) {
			doc = it.next();
			auxDoc = documentDAO.getDocument(doc.getId());

			// Se modifican los datos actualizables de los documentos
			auxDoc.setAuditOpId(doc.getAuditOpId());

			auxDoc.setChecked(doc.getChecked());
			auxDoc.setCheckDate(doc.getCheckDate());

			try {
				auxDoc.setHash(CryptoUtilities.digest(webscanUtilParams.getProperty(WebscanConstants.SCANNED_DOCS_HASH_ALG_PROP_NAME, WebscanConstants.HASH_ALGORITHM_SHA256), doc.getContent()));
			} catch (WebscanUtilitiesException ex) {
				throw new DAOException("Error actualizando el hash del documento " + doc.getId() + ": [" + ex.getCode() + "] " + ex.getMessage(), ex);
			}
			auxDoc.setMimeType(doc.getMimeType());

			auxDoc.setSignatureType(doc.getSignatureType());

			if (ScannedDocsStoreMode.DDBB.equals(docStoreMode)) {
				auxDoc.setContent(doc.getContent());
				auxDoc.setOcr(doc.getOcr());
				auxDoc.setSignature(doc.getSignature());
			}

			docList.add(auxDoc);

			metadataCollection = docs.get(doc);
			DataValidationUtilities.checkMetadataCollection(metadataCollection, doc.getId());
			documentDAO.setMetadataCollection(metadataCollection, doc.getId());
		}

		documentDAO.updateDocuments(docList);
		try {
			if (ScannedDocsStoreMode.FILE_SYSTEM.equals(docStoreMode)) {
				LOG.debug("[WEBSCAN-MODEL] Se persisten los contenidos de los documentos en el sistema de archivos ...");
				for (Iterator<ScannedDocumentDTO> it = docs.keySet().iterator(); it.hasNext();) {
					doc = it.next();
					updateDocFileSystemContents(doc);
				}
			}
		} catch (DAOException e) {
			throw e;
		} catch (WebscanUtilitiesException e) {
			throw new DAOException(DAOException.CODE_300, e.getMessage(), e);
		}
	}

	/**
	 * Actualiza el documento digitalizado.
	 * 
	 * @param doc
	 *            Documento digitalizado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public void updateScannedDoc(ScannedDocumentDTO doc) throws DAOException {
		ScannedDocumentDTO auxDoc;
		ScannedDocsStoreMode docStoreMode;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización del documento " + doc.getId() + "...");
		LOG.debug("[WEBSCAN-MODEL] Se persisten los cambios del documento en BBDD ...");
		docStoreMode = ScannedDocsStoreMode.getByName(webscanUtilParams.getProperty(WebscanConstants.SCANNED_DOCS_STORE_MODE_PROP_NAME));

		auxDoc = getDocument(doc.getId());
		auxDoc.setAuditOpId(doc.getAuditOpId());

		auxDoc.setChecked(doc.getChecked());
		auxDoc.setCheckDate(doc.getCheckDate());

		try {
			auxDoc.setHash(CryptoUtilities.digest(webscanUtilParams.getProperty(WebscanConstants.SCANNED_DOCS_HASH_ALG_PROP_NAME, WebscanConstants.HASH_ALGORITHM_SHA256), doc.getContent()));
		} catch (WebscanUtilitiesException ex) {
			throw new DAOException("Error actualizando el hash del documento " + doc.getId() + ": [" + ex.getCode() + "] " + ex.getMessage(), ex);
		}
		auxDoc.setMimeType(doc.getMimeType());

		auxDoc.setSignatureType(doc.getSignatureType());

		if (ScannedDocsStoreMode.DDBB.equals(docStoreMode)) {
			auxDoc.setContent(doc.getContent());
			auxDoc.setOcr(doc.getOcr());
			auxDoc.setSignature(doc.getSignature());
		}

		documentDAO.updateDocument(auxDoc);
		try {
			if (ScannedDocsStoreMode.FILE_SYSTEM.equals(docStoreMode)) {
				LOG.debug("[WEBSCAN-MODEL] Se persisten el contenido del documento en el sistema de archivos ...");
				updateDocFileSystemContents(doc);
			}
		} catch (DAOException e) {
			throw e;
		} catch (WebscanUtilitiesException e) {
			throw new DAOException(DAOException.CODE_300, e.getMessage(), e);
		}
	}

	/**
	 * Actualiza los contenidos de un documento en el sistema de archivos.
	 * 
	 * @param doc
	 *            documento digitalizado.
	 * @throws DAOException
	 *             Si se produce algún error al consultar las rutas de los
	 *             contenidos en el sistema de archivos.
	 * @throws WebscanUtilitiesException
	 *             Si se produce algún error en la escritura de los contenidos
	 *             en el sistema de archivos.
	 */
	private void updateDocFileSystemContents(ScannedDocumentDTO doc) throws DAOException, WebscanUtilitiesException {
		Map<MimeType, String> docContentStorePaths;
		String docContentStorePath;
		String docRootFileSystemPath;

		docContentStorePaths = documentDAO.getDocContentStorePaths(doc.getId());

		FileSystemUtilities.writeFile(doc.getContent(), docContentStorePaths.get(doc.getMimeType()));
		docRootFileSystemPath = webscanUtilParams.getDocRootFileSystemPath(docContentStorePaths.get(doc.getMimeType()));

		// Firma del documento
		docContentStorePath = docContentStorePaths.get(MimeType.SIGNATURE);
		if (doc.getSignature() != null && docContentStorePath != null) {
			// Ya existe el archivo
			FileSystemUtilities.writeFile(doc.getSignature(), docContentStorePath);
		} else if (doc.getSignature() != null && docContentStorePath == null) {
			// Nuevo archivo
			docContentStorePath = webscanUtilParams.persistDocContentInFileSystem(docRootFileSystemPath, doc.getId(), MimeType.SIGNATURE, doc.getSignature());
		}

		// OCR del documento
		docContentStorePath = docContentStorePaths.get(MimeType.TEXT_PLAIN_OCR);
		if (doc.getOcr() != null && docContentStorePath != null) {
			FileSystemUtilities.writeFile(doc.getOcr(), docContentStorePath);
		} else if (doc.getSignature() != null && docContentStorePath == null) {
			docContentStorePath = webscanUtilParams.persistDocContentInFileSystem(docRootFileSystemPath, doc.getId(), MimeType.TEXT_PLAIN_OCR, doc.getOcr());
		}
	}

	/**
	 * Actualiza la relación de metadatos de un documento digitalizado.
	 * 
	 * @param scannedDocumentId
	 *            Identificador del documento para el que son actualizados los
	 *            metadatos.
	 * @param metadataCollection
	 *            Mapa de definiciones de metadatos (clave), y metadatos del
	 *            documento (valor).
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updateScannedDocMetadataColl(Long scannedDocumentId, Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection) throws DAOException {
		ScannedDocumentDTO doc;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización de los metadatos del documento " + scannedDocumentId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");

		// Validaciones de datos: documento y metadatos
		try {
			DataValidationUtilities.checkIdentifier(scannedDocumentId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		DataValidationUtilities.checkMetadataCollection(metadataCollection, scannedDocumentId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recupera el documento con identificador " + scannedDocumentId + " de BBDD ...");
		}
		doc = documentDAO.getDocument(scannedDocumentId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten los cambios en los metadatos del documento " + scannedDocumentId + " en BBDD ...");
		}
		documentDAO.setMetadataCollection(metadataCollection, doc.getId());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Metadatos del documento " + scannedDocumentId + " actualizados en BBDD ...");
		}
	}

	/**
	 * Elimina una lista de documentos registrados del sistema.
	 * 
	 * @param docId
	 *            Relación de identificadores de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe algún
	 *             documento, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void removeScannedDoc(Long docId) throws DAOException {
		Map<MimeType, String> docContentStorePaths;
		ScannedDocumentDTO doc;
		ScannedDocsStoreMode docStoreMode;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia el borrado del documento " + docId + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se valida los datos del documento " + docId + " ...");
		}
		// Validaciones de datos: especificación de documentos
		try {
			DataValidationUtilities.checkIdentifier(docId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se elimina el documento " + docId + " de BBDD ...");
		}
		try {
			docStoreMode = ScannedDocsStoreMode.getByName(webscanUtilParams.getProperty(WebscanConstants.SCANNED_DOCS_STORE_MODE_PROP_NAME));
			LOG.debug("[WEBSCAN-MODEL] Se persisten los contenidos de los documentos en el sistema de archivos ...");
			doc = documentDAO.getDocument(docId);
			docContentStorePaths = documentDAO.getDocContentStorePaths(doc.getId());
			// Se crea la entrada en el histórico de documentos
			doc.setDigProcEndDate(new Date());
			createAuditScannedDoc(doc);
			documentDAO.removeDocument(docId);
			if (ScannedDocsStoreMode.FILE_SYSTEM.equals(docStoreMode)) {
				FileSystemUtilities.removeDirectory(webscanUtilParams.getDocRootFileSystemPath(docContentStorePaths.get(doc.getMimeType())));
			}
		} catch (DAOException e) {
			throw e;
		} catch (WebscanUtilitiesException e) {
			throw new DAOException(DAOException.CODE_300, e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Documento " + docId + " eliminado de BBDD ...");
		}
	}

	/**
	 * Elimina una lista de documentos registrados del sistema.
	 * 
	 * @param docIds
	 *            Relación de identificadores de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe algún
	 *             documento, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void removeScannedDocs(List<Long> docIds) throws DAOException {
		Map<MimeType, String> docContentStorePaths;
		ScannedDocumentDTO doc;
		ScannedDocsStoreMode docStoreMode;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia el borrado de la lista de documentos " + docIds + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se valida los datos la lista de documentos " + docIds + " ...");
		}
		try {
			DataValidationUtilities.checkCollection(docIds);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se elimina la lista de documentos " + docIds + " de BBDD ...");
		}
		try {
			docStoreMode = ScannedDocsStoreMode.getByName(webscanUtilParams.getProperty(WebscanConstants.SCANNED_DOCS_STORE_MODE_PROP_NAME));
			LOG.debug("[WEBSCAN-MODEL] Se persisten los contenidos de los documentos en el sistema de archivos ...");
			for (Long docId: docIds) {
				doc = documentDAO.getDocument(docId);
				docContentStorePaths = documentDAO.getDocContentStorePaths(doc.getId());
				// Se crea la entrada en el histórico de documentos
				doc.setDigProcEndDate(new Date());
				createAuditScannedDoc(doc);
				documentDAO.removeDocument(docId);
				if (ScannedDocsStoreMode.FILE_SYSTEM.equals(docStoreMode)) {
					FileSystemUtilities.removeDirectory(webscanUtilParams.getDocRootFileSystemPath(docContentStorePaths.get(doc.getMimeType())));
				}
			}
		} catch (DAOException e) {
			throw e;
		} catch (WebscanUtilitiesException e) {
			throw new DAOException(DAOException.CODE_300, e.getMessage(), e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Lista de documentos " + docIds + " eliminada de BBDD ...");
		}
	}

	/**
	 * Registra el histórico de un documento en el modelo de datos.
	 * 
	 * @param doc
	 *            documento digitalizado.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	private void createAuditScannedDoc(ScannedDocumentDTO doc) throws DAOException {
		AuditScannedDocDTO auditScannedDoc;
		BatchDTO batch;
//RMF
		
		batch = batchDAO.getBatch(doc.getBatchId().getId());
		auditScannedDoc = new AuditScannedDocDTO();
		auditScannedDoc.setBatchId(batch.getBatchNameId());
		auditScannedDoc.setBatchReqId(doc.getBatchReqId());
		auditScannedDoc.setDigProcEndDate(doc.getDigProcEndDate());
		auditScannedDoc.setDigProcStartDate(doc.getDigProcStartDate());
		DocumentSpecificationDTO docSpec = documentDAO.getDocumentSpecificationByScanProfileOrgUnitDocSpec(doc.getOrgScanProfileDocSpec().getId());
		auditScannedDoc.setDocSpec(docSpec.getName());
		auditScannedDoc.setFunctionalOrgs(doc.getFunctionalOrgs());
		auditScannedDoc.setHash(doc.getHash());
		auditScannedDoc.setMimeType(doc.getMimeType());
		auditScannedDoc.setSignatureType(doc.getSignatureType());
		auditScannedDoc.setUsername(doc.getUsername());

		auditDAO.createAuditScannedDoc(auditScannedDoc, doc.getAuditOpId());
	}

	// Trazas de ejecución de documentos
	/**
	 * Obtiene un listado de trazas o fases de ejecución asociada al proceso de
	 * digitalización de un documento a partir de su identificador.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return Relación de trazas de ejecución asociada al proceso de
	 *         digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public List<ScannedDocLogDTO> getScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		List<ScannedDocLogDTO> res = new ArrayList<ScannedDocLogDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las trazas de ejecución del documento digitalizado " + scannedDocId + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(scannedDocId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las trazas de ejecución del documento digitalizado " + scannedDocId + " en BBDD...");
		}

		res = documentDAO.getScannedDocLogsByDocId(scannedDocId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " trazas de ejecución del documento digitalizado " + scannedDocId + " en BBDD...");
		}
		return res;
	}

	/**
	 * Obtiene la última de traza de ejecución asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return Ultima traza de ejecución asociada al proceso de digitalización
	 *         del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScannedDocLogDTO getLastScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		ScannedDocLogDTO res = new ScannedDocLogDTO();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de última traza de ejecución del documento digitalizado " + scannedDocId + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(scannedDocId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la última traza de ejecución del documento digitalizado " + scannedDocId + " en BBDD...");
		}

		res = documentDAO.getLastScannedDocLogsByDocId(scannedDocId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la traza de ejecución " + res.getId() + " para el documento digitalizado " + scannedDocId + " en BBDD...");
		}

		return res;
	}

	/**
	 * Obtiene la primera traza de ejecución errónea asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return Primera traza de ejecución errónea asociada al proceso de
	 *         digitalización del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScannedDocLogDTO getFirstFailedScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		ScannedDocLogDTO res = new ScannedDocLogDTO();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la primera traza de ejecución errónea del documento digitalizado {}...", scannedDocId);
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(scannedDocId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la primera traza de ejecución errónea del documento digitalizado {} en BBDD...", scannedDocId);
		}

		res = documentDAO.getFirstFailedScannedDocLogsByDocId(scannedDocId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la traza de ejecución " + res.getId() + " para el documento digitalizado {} en BBDD...", scannedDocId);
		}

		return res;
	}

	/**
	 * Obtiene las trazas de ejecución, cuyas fases son reanudables, y han sido
	 * ejeuctadas en el proceso de digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return las trazas de ejecución, cuyas fases son reanudables, y han sido
	 *         ejeuctadas en el proceso de digitalización de un documento
	 *         digitalizado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public List<ScannedDocLogDTO> getRenewableAndPerformedScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		List<ScannedDocLogDTO> res = new ArrayList<ScannedDocLogDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las trazas de ejecución de fases reanudables del documento digitalizado {}...", scannedDocId);
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(scannedDocId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las trazas de ejecución de fases reanudables del documento digitalizado {} de BBDD...", scannedDocId);
		}

		res = documentDAO.getRenewableAndPerformedScannedDocLogsByDocId(scannedDocId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperadas " + res.size() + " trazas de ejecución de fases reanudables del documento digitalizado {} de BBDD...", scannedDocId);
		}

		return res;
	}

	/**
	 * Obtiene la traza de ejecución de una fase asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @param stageName
	 *            Nombre de la fase.
	 * @return Traza de ejecución de una fase asociada al proceso de
	 *         digitalización del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScannedDocLogDTO getScannedDocLogByStageNameAndDocId(Long scannedDocId, String stageName) throws DAOException {
		ScannedDocLogDTO res = new ScannedDocLogDTO();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la traza de ejecución de la fase {} del documento digitalizado ...", stageName, scannedDocId);
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(scannedDocId);
			DataValidationUtilities.checkIdentifier(stageName);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la traza de ejecución de la fase {} del documento digitalizado {} en BBDD...", stageName, scannedDocId);
		}

		res = documentDAO.getScannedDocLogByStageNameAndDocId(scannedDocId, stageName);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la traza de ejecución " + res.getId() + " para el documento digitalizado {} en BBDD...", scannedDocId);
		}

		return res;
	}

	/**
	 * Crea una nueva traza de ejecución asociada al proceso de digitalización
	 * de un documento digitalizado.
	 * 
	 * @param scannedDocLogDto
	 *            Nueva traza de ejecución asociada al proceso de digitalización
	 *            de un documento digitalizado.
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @param scanProcessStageId
	 *            Identificador de fase del proceso de digitalización.
	 * @return El identificador de la traza de ejecución creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, ya existe traza
	 *             para el documento y la fase del proceso de digitalización, o
	 *             se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	public Long createScannedDocLog(ScannedDocLogDTO scannedDocLogDto, Long scannedDocId, Long scanProcessStageId) throws DAOException {
		Long res;
		ScannedDocLogDTO auxScannedDocLog;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la creación de una traza de ejecución para el documento digitalizado " + scannedDocId + " en la fase " + scanProcessStageId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(scannedDocId);
			DataValidationUtilities.checkIdentifier(scanProcessStageId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		DataValidationUtilities.checkScannedDocLog(scannedDocLogDto, Boolean.TRUE);

		auxScannedDocLog = new ScannedDocLogDTO();
		auxScannedDocLog.setStartDate(scannedDocLogDto.getStartDate());
		auxScannedDocLog.setStatus(OperationStatus.IN_PROCESS);
		auxScannedDocLog.setEndDate(null);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando setencia SQL para la inserción de la nueva traza de ejecución del documento " + scannedDocId + " ...");
		}
		res = documentDAO.createScannedDocLog(scannedDocLogDto, scannedDocId, scanProcessStageId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Traza de ejecución " + res + " del documento " + scannedDocId + " creada correctamente.");
		}

		return res;
	}

	/**
	 * Actualiza el estado de una traza de ejecución asociada al proceso de
	 * digitalización de un documento digitalizado. Los cambios posibles son de
	 * estado en proceso a error, y viceversa.
	 * 
	 * @param scannedDocLogDto
	 *            Traza de ejecución asociada al proceso de digitalización de un
	 *            documento digitalizado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             para el documento y la fase del proceso de digitalización, o
	 *             se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	public void setStatusScannedDocLog(ScannedDocLogDTO scannedDocLogDto) throws DAOException {
		ScannedDocLogDTO auxScannedDocLog;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización del estado de la traza de ejecución " + scannedDocLogDto.getId() + " del documento digitalizado " + scannedDocLogDto.getScannedDocId() + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkScannedDocLog(scannedDocLogDto, Boolean.FALSE);
			DataValidationUtilities.checkInProccesScannedDocLogStatus(scannedDocLogDto.getStatus());
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Obteniendo la trza de ejecución " + scannedDocLogDto.getId() + " de BBDD ...");
		}

		auxScannedDocLog = documentDAO.getScannedDocLog(scannedDocLogDto.getId());
		auxScannedDocLog.setStatus(scannedDocLogDto.getStatus());

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando setencia SQL para la actualización del estado de la traza de ejecución del documento " + scannedDocLogDto.getId() + " ...");
		}
		documentDAO.updateScannedDocLog(auxScannedDocLog);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Estado de la traza de ejecución del documento " + scannedDocLogDto.getId() + " actualizada correctamente.");
		}
	}

	/**
	 * Cierra una traza de ejecución asociada al proceso de digitalización de un
	 * documento digitalizado y registrado en el sistema. Es posible actualizar
	 * la siguiente información de una traza de ejecución de un documento
	 * digitalizado: fecha de finalización y estado de ejecución.
	 * 
	 * @param scannedDocLogDto
	 *            Traza de ejecución asociada al proceso de digitalización de un
	 *            documento digitalizado a actualizar.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de ejecución del documento digitalizado, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public void closeScannedDocLog(ScannedDocLogDTO scannedDocLogDto) throws DAOException {
		ScannedDocLogDTO auxScannedDocLog;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización de la traza de ejecución " + scannedDocLogDto.getId() + " del documento digitalizado " + scannedDocLogDto.getScannedDocId() + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkScannedDocLog(scannedDocLogDto, Boolean.FALSE);
			DataValidationUtilities.checkClosedScannedDocLogStatus(scannedDocLogDto.getStatus());
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Obteniendo la trza de ejecución " + scannedDocLogDto.getId() + " de BBDD ...");
		}
		auxScannedDocLog = documentDAO.getScannedDocLog(scannedDocLogDto.getId());

		auxScannedDocLog.setEndDate(scannedDocLogDto.getEndDate());
		auxScannedDocLog.setStatus(scannedDocLogDto.getStatus());

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando setencia SQL para la actualización de la traza de ejecución del documento " + scannedDocLogDto.getId() + " ...");
		}
		documentDAO.updateScannedDocLog(auxScannedDocLog);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Traza de ejecución del documento " + scannedDocLogDto.getId() + " cerrada correctamente.");
		}
	}

	/**
	 * Elimina las trazas de ejecución asociadas al proceso de digitalización de
	 * un documento digitalizado y registrado en el sistema.
	 * 
	 * @param docId
	 *            Identificador de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public void removeScannedDocLogs(Long docId) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la eliminación de las traza de ejecución del documento digitalizado {}...", docId);
			LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		}

		try {
			DataValidationUtilities.checkIdentifier(docId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se lanza la sentencia para eliminar las trazas de ejecución del documento {} de BBDD ...", docId);
		}
		documentDAO.removeScannedDocLogs(docId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Trazas de ejecución del documento {} eliminadas.", docId);
		}
	}

	/*************************************************************************************/
	/*								Fases de procesos de digitalización			 		 */
	/*************************************************************************************/

	/**
	 * Obtiene una fase de proceso de digitalización a partir de su
	 * denominación.
	 * 
	 * @param scanProcessStageName
	 *            Página de resultados a devolver.
	 * @return fase de proceso de digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición de perfil de digitalización, o se produce algún
	 *             error en la consulta a BBDD.
	 */
	public ScanProcessStageDTO getScanProcStageByName(String scanProcessStageName) throws DAOException {
		ScanProcessStageDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la fase del proceso de digitalización " + scanProcessStageName + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkEmptyObject(scanProcessStageName, "[WEBSCAN-MODEL] Denominación de fase de proceso de digitalización nula o vacía.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando setencia SQL para la recuperación de la fase del proceso de digitalización " + scanProcessStageName + " ...");
		}
		res = scanProcessStageDAO.getScanProcStageByName(scanProcessStageName);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Fase del proceso de digitalización recuperada: " + res.getId() + " ...");
		}

		return res;
	}

	/**
	 * Obtiene la relación de fases de proceso de digitalización de una
	 * definición de perfil de digitalización determinada.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de una definición de perfil de digitalización.
	 * @return relación de fases de proceso de digitalización de una definición
	 *         de perfil de digitalización determinada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición de perfil de digitalización, o se produce algún
	 *             error en la consulta a BBDD.
	 */
	public List<ScanProfileSpecScanProcStageDTO> getScanProcStagesByScanProfileSpec(Long scanProfileSpecId) throws DAOException {
		int queryResultSize = 0;
		List<ScanProfileSpecScanProcStageDTO> res = new ArrayList<ScanProfileSpecScanProcStageDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las fases del proceso de digitalización de la definición de perfil de digitalización " + scanProfileSpecId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkIdentifier(scanProfileSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando setencia SQL para la recuperación de las fases del proceso de digitalización de la definición de perfil de digitalización " + scanProfileSpecId + " ...");
		}
		res = scanProcessStageDAO.getScanProcessStagesByScanProfileSpecId(scanProfileSpecId);

		if (res != null) {
			queryResultSize = res.size();
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Numero de fases del proceso de digitalización de la definición de perfil de digitalización " + scanProfileSpecId + " recuperadas: " + queryResultSize + " ...");
		}
		return res;
	}

	/**
	 * Obtiene la relación de fases reanudables de una definición de perfil de
	 * digitalización determinada.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de una definición de perfil de digitalización.
	 * @return relación de fases reanudables de una definición de perfil de
	 *         digitalización determinada ordenadas por orden de ejecución.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición de perfil de digitalización, o se produce algún
	 *             error en la consulta a BBDD.
	 */
	public List<ScanProfileSpecScanProcStageDTO> getRenewableScanProcStagesByScanProfileSpec(Long scanProfileSpecId) throws DAOException {
		int queryResultSize = 0;
		List<ScanProfileSpecScanProcStageDTO> res = new ArrayList<ScanProfileSpecScanProcStageDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las fases del proceso de digitalización de la definición de perfil de digitalización " + scanProfileSpecId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");

		try {
			DataValidationUtilities.checkIdentifier(scanProfileSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando setencia SQL para la recuperación de las fases del proceso de digitalización de la definición de perfil de digitalización " + scanProfileSpecId + " ...");
		}
		res = scanProcessStageDAO.getRenewableScanProcessStagesByScanProfileSpecId(scanProfileSpecId);

		if (res != null) {
			queryResultSize = res.size();
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Numero de fases del proceso de digitalización de la definición de perfil de digitalización " + scanProfileSpecId + " recuperadas: " + queryResultSize + " ...");
		}
		return res;
	}

	/*************************************************************************************/
	/*								Perfiles de digitalización			 				 */
	/*************************************************************************************/

	/**
	 * Obtiene el listado de definiciones de perfiles de digitalización
	 * registradas en el sistema, permitiendo la paginación y ordenación del
	 * resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de definiciones de perfiles de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<ScanProfileSpecificationDTO> listScanProfileSpecifications(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		int queryResultSize = 0;
		List<ScanProfileSpecificationDTO> res = new ArrayList<ScanProfileSpecificationDTO>();

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de definiciones de perfiles de digitalización registradas en el sistema ...");

		DataValidationUtilities.checkScanProfileGroupByClause(orderByColumns);

		res = scanProfileDAO.listScanProfileSpecifications(pageNumber, elementsPerPage, orderByColumns);

		if (res != null) {
			queryResultSize = res.size();
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Numero de perfiles de digitalización registrados en el sistema: " + queryResultSize + " ...");
		}
		return res;
	}

	/**
	 * Obtiene una especificación de perfil de digitalización a partir de su
	 * identificador.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de especificación de perfil de digitalización.
	 * @return La definición de perfil de digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public ScanProfileSpecificationDTO getScanProfileSpecification(Long scanProfileSpecId) throws DAOException {
		ScanProfileSpecificationDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la definición de perfil de digitalización con identificador " + scanProfileSpecId + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(scanProfileSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = scanProfileDAO.getScanProfileSpecification(scanProfileSpecId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperado el perfil de digitalización con identificador " + scanProfileSpecId + ".");
		}
		return res;
	}

	/**
	 * Recupera una especificación de perfil de digitalización a partir de su
	 * denominación.
	 * 
	 * @param scanProfileSpecName
	 *            Denominación de especificación de perfil de digitalización.
	 * @return La definición de perfil de digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public ScanProfileSpecificationDTO getScanProfileSpecificationByName(String scanProfileSpecName) throws DAOException {
		ScanProfileSpecificationDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la definición de perfil de digitalización denominada " + scanProfileSpecName + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(scanProfileSpecName);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = scanProfileDAO.getScanProfileSpecificationByName(scanProfileSpecName);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la definición de perfil de digitalización denominada " + scanProfileSpecName + ".");
		}
		return res;
	}

	/**
	 * Actualiza una especificación de perfil de digitalización registrada en el
	 * sistema. El único dato que puede ser modificado es la descripción de la
	 * especificación.
	 * 
	 * @param scanProfileSpecification
	 *            Especificación de perfil de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updateScanProfileSpecification(ScanProfileSpecificationDTO scanProfileSpecification) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización de la definición de perfil de digitalización " + (scanProfileSpecification != null ? scanProfileSpecification.getId() : "No identificado") + "...");
		}
		DataValidationUtilities.checkScanProfileSpecification(scanProfileSpecification);

		scanProfileDAO.updateScanProfileSpecification(scanProfileSpecification);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Definición de perfil de digitalización " + scanProfileSpecification.getId() + " actualizada correctamente.");
		}
	}

	/**
	 * Obtiene el listado de perfiles de digitalización registrados en el
	 * sistema, permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de definiciones de perfiles de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<ScanProfileDTO> listScanProfiles(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		List<ScanProfileDTO> res = new ArrayList<ScanProfileDTO>();

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de perfiles de digitalización registrados en el sistema ...");

		DataValidationUtilities.checkScanProfileGroupByClause(orderByColumns);

		res = scanProfileDAO.listScanProfiles(pageNumber, elementsPerPage, orderByColumns);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Numero de perfiles de digitalización registrados en el sistema: " + res.size() + " ...");
		}
		return res;
	}

	/**
	 * Obtiene un perfil de digitalización a partir de su identificador.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @return El perfil de digitalización solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil de digitalización, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScanProfileDTO getScanProfile(Long scanProfileId) throws DAOException {
		ScanProfileDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación del perfil de digitalización con identificador " + scanProfileId + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(scanProfileId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = scanProfileDAO.getScanProfile(scanProfileId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperado el perfil de digitalización con identificador " + scanProfileId + ".");
		}
		return res;
	}

	/**
	 * Obtiene un perfil de digitalización a partir de su denominación.
	 * 
	 * @param scanProfileName
	 *            Denominación de perfil de digitalización.
	 * @return El perfil de digitalización solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil de digitalización, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScanProfileDTO getScanProfileByName(String scanProfileName) throws DAOException {
		ScanProfileDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación del perfil de digitalización con denominación " + scanProfileName + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(scanProfileName);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = scanProfileDAO.getScanProfileByName(scanProfileName);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperado el perfil de digitalización, denominado " + scanProfileName + ", recueperado correctamente.");
		}
		return res;
	}

	/**
	 * Obtiene la relación de perfiles de digitalización que implementan una
	 * especificación determinada. Este método habilita la paginación y
	 * ordenación del resultado.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de definición de perfil de digitalización.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return Relación de perfiles de digitalización que implementan una
	 *         especificación determinada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public List<ScanProfileDTO> getScanProfilesBySpec(Long scanProfileSpecId, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		List<ScanProfileDTO> res = new ArrayList<ScanProfileDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de perfiles de digitalización definidos mediante la especificación " + scanProfileSpecId + " ...");
		}
		DataValidationUtilities.checkScanProfileGroupByClause(orderByColumns);
		try {
			DataValidationUtilities.checkIdentifier(scanProfileSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = scanProfileDAO.getScanProfilesBySpec(scanProfileSpecId, pageNumber, elementsPerPage, orderByColumns);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Numero de perfiles de digitalización recuperados para la especificación " + scanProfileSpecId + ": " + res.size() + " ...");
		}
		return res;
	}

	/**
	 * Obtiene la relación de perfiles de digitalización configurados para una
	 * unidad organizativa determinada.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return Relación de perfiles de digitalización que implementan una
	 *         especificación determinada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<ScanProfileDTO> getScanProfilesByOrgUnit(Long orgUnitId) throws DAOException {
		List<ScanProfileDTO> res = new ArrayList<ScanProfileDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de perfiles de digitalización para la unidad organizativa " + orgUnitId + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(orgUnitId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = scanProfileDAO.getScanProfilesByOrgUnit(orgUnitId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Numero de perfiles de digitalización recuperados para la unidad organizativa " + orgUnitId + ": " + res.size() + " ...");
		}
		return res;
	}

	/**
	 * Crea un nuevo perfil de digitalización en el sistema.
	 * 
	 * @param scanProfile
	 *            Nuevo perfil de digitalización.
	 * @param scanProfileSpecId
	 *            Identificador de la especificación que define el nuevo perfil
	 *            de digitalización.
	 * @return El perfil de digitalización creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación del nuevo perfil de digitalización, o se
	 *             produce algún error en la ejecución de la sentencia de BBDD.
	 */
	public ScanProfileDTO createScanProfile(ScanProfileDTO scanProfile, Long scanProfileSpecId) throws DAOException {
		ScanProfileDTO res = null;

		res = createScanProfile(scanProfile, scanProfileSpecId, new HashMap<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>>(), new HashMap<ScanProfileOrgUnitDTO, Map<Long, Boolean>>());

		return res;
	}

	/**
	 * Crea un nuevo perfil de digitalización en el sistema, estableciendo la
	 * relación de plugins y especificaciones de documentos configuradas en las
	 * diferentes unidades organizativas donde es empleado el perfil de
	 * digitalización.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de la especificación que define el nuevo perfil
	 *            de digitalización.
	 * @param scanProfile
	 *            Nuevo perfil de digitalización.
	 * @param orgUnitPlugins
	 *            Mapa en el que cada entrada es identificada mediante la unidad
	 *            organizativa donde se usa el perfil, y cuyo valor se
	 *            corresponde con una lista de plugins configurados.
	 * @param orgUnitDocs
	 *            Mapa en el que cada entrada es identificada mediante la unidad
	 *            organizativa donde se usa el perfil, y cuyo valor se
	 *            corresponde con un mapa donde se identifican las
	 *            especificaciones de documentos configuradas y un indicador de
	 *            activación.
	 * @return El perfil de digitalización creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación del nuevo perfil de digitalización, o se
	 *             produce algún error en la ejecución de la sentencia de BBDD.
	 */
	public ScanProfileDTO createScanProfile(ScanProfileDTO scanProfile, Long scanProfileSpecId, Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins, Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs) throws DAOException {
		Boolean found;
		Long scanProfileId;
		List<ScanProfileOrgUnitDTO> scanProfOrgUnits = new ArrayList<ScanProfileOrgUnitDTO>();
		Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> auxOrgUnitPlugins = new HashMap<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>>();
		Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> auxOrgUnitDocSpecs = new HashMap<ScanProfileOrgUnitDTO, Map<Long, Boolean>>();
		ScanProfileOrgUnitDTO dto;
		ScanProfileDTO res = null;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la creación de un nuevo perfil de digitalización ...");

		LOG.debug("[WEBSCAN-MODEL] Se validan los datos del nuevo perfil de digitalización ...");

		// Validaciones de datos: especificación de plugin y parámetros
		try {
			DataValidationUtilities.checkIdentifier(scanProfileSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		DataValidationUtilities.checkScanProfile(scanProfile);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persiste el nuevo perfil de digitalización, denominado " + scanProfile.getName() + " ...");
		}
		scanProfileId = scanProfileDAO.createScanProfile(scanProfile, scanProfileSpecId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se validan las unidades organizativas del nuevo perfil de digitalización, denominado " + scanProfile.getName() + " ...");
		}
		checkScanProfileOrgUnitConfig(orgUnitPlugins.keySet(), scanProfileId);
		checkScanProfileOrgUnitConfig(orgUnitDocs.keySet(), scanProfileId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten las unidades organizativas del nuevo perfil de digitalización, denominado " + scanProfile.getName() + " ...");
		}
		scanProfOrgUnits = persistScanProfileOrgUnits(orgUnitPlugins.keySet(), scanProfOrgUnits);
		scanProfOrgUnits = persistScanProfileOrgUnits(orgUnitDocs.keySet(), scanProfOrgUnits);

		for (ScanProfileOrgUnitDTO spou: scanProfOrgUnits) {
			found = Boolean.FALSE;
			for (Iterator<ScanProfileOrgUnitDTO> it = orgUnitPlugins.keySet().iterator(); !found && it.hasNext();) {
				dto = it.next();
				if (spou.equals(dto)) {
					auxOrgUnitPlugins.put(spou, orgUnitPlugins.get(dto));
					found = Boolean.TRUE;
				}
			}

			found = Boolean.FALSE;
			for (Iterator<ScanProfileOrgUnitDTO> it = orgUnitDocs.keySet().iterator(); !found && it.hasNext();) {
				dto = it.next();
				if (spou.equals(dto)) {
					auxOrgUnitDocSpecs.put(spou, orgUnitDocs.get(dto));
					found = Boolean.TRUE;
				}
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten los plugins y definiciones de documentos para las unidades organizativas configuradas en el nuevo perfil de digitalización, denominado " + scanProfile.getName() + " ...");
		}
		// Se persisten los plugins y documentos para las configuraciones
		// del perfil de digitalización por unidad organizativa
		setScanProfOrgUnitConfig(auxOrgUnitPlugins, auxOrgUnitDocSpecs, scanProfile);

		res = scanProfileDAO.getScanProfile(scanProfileId);

		return res;
	}

	/**
	 * Actualiza un perfil de digitalización registrado en el sistema. Los datos
	 * que pueden ser actualizados son la denominación, descripción y el estado
	 * de activación del perfil.
	 * 
	 * @param scanProfile
	 *            perfil de digitalización.
	 * @param propagateActivation
	 *            Indica si la activación (true), o inhabilitación (false), del
	 *            perfil de digitalización será propagada a las diferentes
	 *            configuraciones del perfil en unidades organizativas.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe le
	 *             perfil, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updateScanProfile(ScanProfileDTO scanProfile, Boolean propagateActivation) throws DAOException {
		List<ScanProfileOrgUnitDTO> currentOrgUnits;
		List<PluginScanProcStageDTO> plugins;
		Long scanProfileId;
		Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins;
		Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs;
		Map<Long, Boolean> dataConfig;
		PluginScanProcStageDTO psps;
		// Validaciones de datos: especificación de plugin y parámetros
		DataValidationUtilities.checkScanProfile(scanProfile);

		scanProfileId = scanProfile.getId();

		currentOrgUnits = scanProfileDAO.getScanProfileOrgUnitsByScanProfile(scanProfileId);
		orgUnitPlugins = new HashMap<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>>();
		orgUnitDocs = new HashMap<ScanProfileOrgUnitDTO, Map<Long, Boolean>>();
		if (currentOrgUnits != null && !currentOrgUnits.isEmpty()) {
			for (ScanProfileOrgUnitDTO spou: currentOrgUnits) {
				// Plugins
				List<ScanProfileOrgUnitPluginDTO> currentPlugins = scanProfileDAO.getScanProfileOrgUnitPluginsByScanProfile(spou.getId());
				plugins = new ArrayList<PluginScanProcStageDTO>();
				if (currentPlugins != null && !currentPlugins.isEmpty()) {
					for (ScanProfileOrgUnitPluginDTO spoup: currentPlugins) {
						psps = new PluginScanProcStageDTO();
						psps.setActive(spoup.getActive());
						psps.setPlugin(spoup.getPlugin());
						psps.setScanProcessStage(spoup.getScanProcessStage());
						plugins.add(psps);
					}
				}
				orgUnitPlugins.put(spou, plugins);

				// Documentos
				List<ScanProfileOrgUnitDocSpecDTO> currentDocs = scanProfileDAO.getScanProfileOrgUnitDocSpecsByScanProfile(spou.getId());
				dataConfig = new HashMap<Long, Boolean>();
				if (currentDocs != null && !currentDocs.isEmpty()) {
					for (ScanProfileOrgUnitDocSpecDTO spouds: currentDocs) {
						dataConfig.put(spouds.getId(), spouds.getActive());
					}
				}
				orgUnitDocs.put(spou, dataConfig);
			}
		}

		updateScanProfile(scanProfile, orgUnitPlugins, orgUnitDocs, propagateActivation);
	}

	/**
	 * Actualiza un perfil de digitalización registrado en el sistema
	 * (denominación, descripción y estado de activación), estableciendo además
	 * una nueva relación de configuraciones en unidades organizativas (plugins
	 * y definiciones de documentos), y ofreciendo la posibilidad de propagar la
	 * activación del perfil a esta nueva relación de configuraciones.
	 * 
	 * @param scanProfile
	 *            perfil de digitalización
	 * @param orgUnitPlugins
	 *            Nueva relación de plugins establecida en las diferentes
	 *            configuraciones del perfil de digitalización en unidades
	 *            organizativas.
	 * @param orgUnitDocs
	 *            nueva relación de definiciones de documentos establecida en
	 *            las diferentes configuraciones del perfil de digitalización en
	 *            unidades organizativas.
	 * @param propagateActivation
	 *            Indica si la activación (true), o inhabilitación (false), del
	 *            perfil de digitalización será propagada a las diferentes
	 *            configuraciones del perfil en unidades organizativas.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void updateScanProfile(ScanProfileDTO scanProfile, Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins, Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs, Boolean propagateActivation) throws DAOException {
		Boolean found;
		Long scanProfileId;
		List<ScanProfileOrgUnitDTO> currentOrgUnits;
		List<ScanProfileOrgUnitDTO> scanProfOrgUnits;
		Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> auxOrgUnitPlugins = new HashMap<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>>();
		Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> auxOrgUnitDocSpecs = new HashMap<ScanProfileOrgUnitDTO, Map<Long, Boolean>>();
		ScanProfileOrgUnitDTO dto;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización de un perfil de digitalización ...");

		LOG.debug("[WEBSCAN-MODEL] Se validan los datos del perfil de digitalización ...");

		// Validaciones de datos: especificación de plugin y parámetros
		DataValidationUtilities.checkScanProfile(scanProfile);
		scanProfileId = scanProfile.getId();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se validan las unidades organizativas del perfil de digitalización, denominado " + scanProfile.getName() + " ...");
		}
		checkScanProfileOrgUnitConfig(orgUnitPlugins.keySet(), scanProfileId);
		checkScanProfileOrgUnitConfig(orgUnitDocs.keySet(), scanProfileId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persiste los cambios del perfil de digitalización, denominado " + scanProfile.getName() + " ...");
		}
		scanProfileDAO.updateScanProfile(scanProfile);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten las unidades organizativas del nuevo perfil de digitalización, denominado " + scanProfile.getName() + " ...");
		}
		currentOrgUnits = scanProfileDAO.getScanProfileOrgUnitsByScanProfile(scanProfileId);

		scanProfOrgUnits = persistScanProfileOrgUnits(orgUnitPlugins.keySet(), currentOrgUnits);
		scanProfOrgUnits = persistScanProfileOrgUnits(orgUnitDocs.keySet(), scanProfOrgUnits);

		auxOrgUnitPlugins = mergeScanProfileOrgUnitPlugins(orgUnitPlugins, scanProfOrgUnits, scanProfile, propagateActivation);
		auxOrgUnitDocSpecs = mergeScanProfileOrgUnitDocSpecs(orgUnitDocs, scanProfOrgUnits, scanProfile, propagateActivation);

		for (ScanProfileOrgUnitDTO spou: scanProfOrgUnits) {
			found = Boolean.FALSE;
			for (Iterator<ScanProfileOrgUnitDTO> it = orgUnitPlugins.keySet().iterator(); !found && it.hasNext();) {
				dto = it.next();
				if (spou.equals(dto)) {
					auxOrgUnitPlugins.put(spou, orgUnitPlugins.get(dto));
					found = Boolean.TRUE;
				}
			}

			found = Boolean.FALSE;
			for (Iterator<ScanProfileOrgUnitDTO> it = orgUnitDocs.keySet().iterator(); !found && it.hasNext();) {
				dto = it.next();
				if (spou.equals(dto)) {
					auxOrgUnitDocSpecs.put(spou, orgUnitDocs.get(dto));
					found = Boolean.TRUE;
				}
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten los plugins y definiciones de documentos para las unidades organizativas configuradas en el nuevo perfil de digitalización, denominado " + scanProfile.getName() + " ...");
		}
		// Se persisten los plugins y documentos para las configuraciones
		// del perfil de digitalización por unidad organizativa
		setScanProfOrgUnitConfig(auxOrgUnitPlugins, auxOrgUnitDocSpecs, scanProfile);

		// Se eliminan las existentes y no informadas como parámetros de entrada
		removeScanProfOrgUnits(scanProfOrgUnits, currentOrgUnits);
	}

	/**
	 * Elimina un perfil de digitalización del sistema.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void removeScanProfile(Long scanProfileId) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia el borrado del perfil de digitalización " + scanProfileId + " ...");

			LOG.debug("[WEBSCAN-MODEL] Se valida los datos del perfil de digitalización " + scanProfileId + " ...");
		}
		// Validaciones de datos: especificación de plugin y parámetros
		try {
			DataValidationUtilities.checkIdentifier(scanProfileId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se elimina el perfil de digitalización " + scanProfileId + ", y sus configuraciones en BBDD ...");
		}
		scanProfileDAO.removeScanProfile(scanProfileId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Perfil de digitalización " + scanProfileId + " eliminado de BBDD ...");
		}
	}

	/**
	 * Comprueba si un perfil de digitalización posee documentos en proceso de
	 * digitalización.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @return Si el perfil de digitalización posee documentos en proceso de
	 *         digitalización, retorna true. En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil de digitalización, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	public Boolean hasScanProfileDocsInScanProccess(Long scanProfileId) throws DAOException {
		Boolean res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la comprobación de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(scanProfileId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = scanProfileDAO.hasScanProfileDocsInScanProccess(scanProfileId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Finalizada la comprobación de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ". Resultado: " + res);
		}
		return res;
	}

	/**
	 * Recupera una configuración en unidad organizativa de un perfil de
	 * digitalización a partir de su identificador.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración en unidad organizativa de un
	 *            perfil de digitalización.
	 * @return La configuración en unidad organizativa de un perfil de
	 *         digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             entidad solicitada, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	public ScanProfileOrgUnitDTO getScanProfileOrgUnit(Long scanProfileOrgUnitId) throws DAOException {
		ScanProfileOrgUnitDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando la configuración de perfil de digitalización en unidad de organizativa {} ...", scanProfileOrgUnitId);
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		// Validaciones de datos
		try {
			DataValidationUtilities.checkIdentifier(scanProfileOrgUnitId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Obteniendo la configuración de perfil de digitalización en unidad de organizativa {} de BBDD ...", scanProfileOrgUnitId);
		}
		res = scanProfileDAO.getScanProfileOrgUnit(scanProfileOrgUnitId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Configuración de perfil de digitalización en unidad de organizativa {} recuperada de BBDD.", scanProfileOrgUnitId);
		}
		return res;
	}

	/**
	 * Recupera el listado de configuraciones en unidades organizativas de un
	 * determinado perfil de digitalización.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @return Lista de configuraciones en unidades organizativas de un
	 *         determinado perfil de digitalización. Retorna lista vacía si no
	 *         existe ninguna configuración.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public List<ScanProfileOrgUnitDTO> getScanProfileOrgUnitsByScanProfile(Long scanProfileId) throws DAOException {
		List<ScanProfileOrgUnitDTO> res = new ArrayList<ScanProfileOrgUnitDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando las configuraciones del perfil de digitalización " + scanProfileId + " en unidades de organizativas ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		// Validaciones de datos
		try {
			DataValidationUtilities.checkIdentifier(scanProfileId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Obteniendo las configuraciones del perfil de digitalización " + scanProfileId + " en unidades de organizativas de BBDD ...");
		}
		res = scanProfileDAO.getScanProfileOrgUnitsByScanProfile(scanProfileId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " configuraciones del perfil de digitalización " + scanProfileId + " en unidades de organizativas recuperadas de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera la configuración de un perfil de digitalización en una unidad
	 * organizativa.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return La configuración de un perfil de digitalización en una unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe perfil
	 *             de digitalización configurado para la unidad organizativa, o
	 *             se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	public ScanProfileOrgUnitDTO getScanProfileOrgUnitByScanProfileAndOrgUnit(Long scanProfileId, Long orgUnitId) throws DAOException {
		ScanProfileOrgUnitDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando la configuración del perfil de digitalización " + scanProfileId + " en la unidad de organizativa " + orgUnitId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		// Validaciones de datos
		try {
			DataValidationUtilities.checkIdentifier(scanProfileId);
			DataValidationUtilities.checkIdentifier(orgUnitId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Obteniendo la configuración del perfil de digitalización " + scanProfileId + " en la unidad de organizativa " + orgUnitId + " de BBDD ...");
		}
		res = scanProfileDAO.getScanProfileOrgUnitByScanProfAndOrgUnit(scanProfileId, orgUnitId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Configuración del perfil de digitalización en unidad organizativa " + res.getId() + " recuperada de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera la configuración de un perfil de digitalización en una unidad
	 * organizativa a partir de la denominación del perfil y el identificador
	 * externo de la unidad organizativa.
	 * 
	 * @param scanProfileName
	 *            Denominación del perfil de digitalización.
	 * @param orgUnitExtId
	 *            Identificador externo de unidad organizativa.
	 * @return La configuración de un perfil de digitalización en una unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe perfil
	 *             de digitalización configurado para la unidad organizativa, o
	 *             se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	public ScanProfileOrgUnitDTO getScanProfileOrgUnitByScanProfileNameAndOrgUnitExtId(String scanProfileName, String orgUnitExtId) throws DAOException {
		ScanProfileOrgUnitDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando la configuración del perfil de digitalización " + scanProfileName + " en la unidad de organizativa " + orgUnitExtId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		// Validaciones de datos
		try {
			DataValidationUtilities.checkIdentifier(scanProfileName);
			DataValidationUtilities.checkIdentifier(orgUnitExtId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Obteniendo la configuración del perfil de digitalización " + scanProfileName + " en la unidad de organizativa " + orgUnitExtId + " de BBDD ...");
		}
		res = scanProfileDAO.getScanProfileOrgUnitByScanProfileNameAndOrgUnitExtId(scanProfileName, orgUnitExtId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Configuración del perfil de digitalización en unidad organizativa " + res.getId() + " recuperada de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera el plugin configurado para una espeficicación determinada en un
	 * perfil de digitalización establecido para una unidad organizativa.
	 * 
	 * @param scanProfile
	 *            Perfil de digitalización.
	 * @param orgUnit
	 *            Unidad organizativa.
	 * @param spec
	 *            Especificación de plugin.
	 * @return Plugin configurado para una espeficicación determinada en un
	 *         perfil de digitalización establecido para una unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, se produce algún
	 *             error en la consulta a BBDD, no se encuentra configuración
	 *             del perfil de digitalización para la unidad organizativa
	 *             especificada como parámetro de entrada, o no se encuentra el
	 *             plugin.
	 */
	public ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPluginConf(ScanProfileDTO scanProfile, OrganizationUnitDTO orgUnit, PluginSpecificationDTO spec) throws DAOException {
		Boolean found = Boolean.FALSE;
		List<ScanProfileOrgUnitPluginDTO> scanProfileOrgUnitPlugins;
		List<PluginDTO> plugins;
		PluginDTO plugin;
		ScanProfileOrgUnitDTO spou;
		ScanProfileOrgUnitPluginDTO res, spoup;
		String excMsg;

		res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando el plugin configurado para la definición " + spec.getName() + " en el perfil de digitalización " + scanProfile.getName() + " y la unidad organizativa " + orgUnit.getExternalId() + " ...");

			LOG.debug("[WEBSCAN-MODEL] Obteniendo el listados de plugins configurado para la definición " + spec.getName() + " de BBDD ...");
		}
		plugins = pluginDAO.getPluginsBySpecification(spec.getId());

		if (plugins != null && !plugins.isEmpty()) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Obteniendo el listado de plugins configurado para el perfil de digitalización " + scanProfile.getName() + " y la unidad organizativa " + orgUnit.getExternalId() + " de BBDD...");
			}
			spou = scanProfileDAO.getScanProfileOrgUnitByScanProfAndOrgUnit(scanProfile.getId(), orgUnit.getId());

			scanProfileOrgUnitPlugins = scanProfileDAO.getScanProfileOrgUnitPluginsByScanProfile(spou.getId());

			for (Iterator<ScanProfileOrgUnitPluginDTO> it = scanProfileOrgUnitPlugins.iterator(); !found && it.hasNext();) {
				spoup = it.next();
				for (Iterator<PluginDTO> itPlugin = plugins.iterator(); !found && itPlugin.hasNext();) {
					plugin = itPlugin.next();
					if (spoup.getPlugin().equals(plugin.getId())) {
						res = spoup;
						found = Boolean.TRUE;
					}
				}
			}

			if (!found) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando plugins configurados para el perfil de digitalización " + scanProfile.getName() + " en la unidad organizativa " + orgUnit.getExternalId() + " y la definición de plugin " + spec.getName() + " de BBDD. " + "No se encontraron configuraciones para la definción de plugin registradas en el sistema.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperado el plugin configurado para la definición " + spec.getName() + " en el perfil de digitalización " + scanProfile.getName() + " y la unidad organizativa " + orgUnit.getExternalId() + " de BBDD, identificador: " + res.getPlugin() + ".");
			}
		}

		return res;
	}

	/**
	 * Recupera la lista de parámetros del plugin configurado para llevar a cabo
	 * una determinada fase de un proceso de digitalización asociado al perfil
	 * de digitalización con figurado para una unidad organziativa.
	 * 
	 * @param scanProfileOrgUnit
	 *            Configuración de un perfil de digitalización para una unidad
	 *            organizativa.
	 * @param scanProcessStage
	 *            fase de procesos de digitalización.
	 * @return Lista de parámetros del plugin configurado para llevar a cabo una
	 *         determinada fase de un proceso de digitalización asociado al
	 *         perfil de digitalización con figurado para una unidad
	 *         organziativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, se produce algún
	 *             error en la consulta a BBDD, o no se encuentra configuración
	 *             del perfil de digitalización para la unidad organizativa
	 *             especificada como parámetro de entrada.
	 */
	public List<ParameterDTO> getPluginParametersByScanProfileOrgUnit(ScanProfileOrgUnitDTO scanProfileOrgUnit, ScanProcessStageDTO scanProcessStage) throws DAOException {
		Boolean found = Boolean.FALSE;
		List<ScanProfileOrgUnitPluginDTO> scanProfileOrgUnitPlugins;
		List<ParameterDTO> res = new ArrayList<ParameterDTO>();
		ScanProfileOrgUnitPluginDTO spoup;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando la lista de parámetros del plugin para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnit.getId() + ", y fase " + scanProcessStage.getName() + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando los parámetros de entrada ...");
		DataValidationUtilities.checkScanProfileOrgUnit(scanProfileOrgUnit, scanProfileOrgUnit.getScanProfile());
		DataValidationUtilities.checkScanProcessStage(scanProcessStage);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los plugins empleados por la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnit.getId() + " de BBDD ...");
		}
		scanProfileOrgUnitPlugins = scanProfileDAO.getScanProfileOrgUnitPluginsByScanProfile(scanProfileOrgUnit.getId());
		for (Iterator<ScanProfileOrgUnitPluginDTO> it = scanProfileOrgUnitPlugins.iterator(); !found && it.hasNext();) {
			spoup = it.next();
			if (spoup.getScanProcessStage().equals(scanProcessStage.getId())) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Obteniendo los parámetros para el plugin " + spoup.getPlugin() + " ...");
				}
				res = pluginDAO.getParametersByPlugin(spoup.getPlugin());
				found = Boolean.TRUE;
			}
		}
		// scanProcessStageDAO.get
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la lista de parámetros del plugin para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnit.getId() + ", y fase " + scanProcessStage.getName() + ". Número de parametros: " + res.size());
		}
		return res;

	}

	/**
	 * Recupera el perfil de digitalización en una unidad organizativa asociado
	 * al proceso de digitalización del documento.
	 * 
	 * @param docId
	 *            Documento digitalizado.
	 * @return perfil de digitalización en una unidad organizativa asociado al
	 *         proceso de digitalización del documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public ScanProfileOrgUnitDTO getScanProfileOrgUnitByDoc(Long docId) throws DAOException {
		ScanProfileOrgUnitDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando el perfil de digitalización asociado al proceso de digitalización del documento " + docId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		// Validaciones de datos
		try {
			DataValidationUtilities.checkIdentifier(docId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar el perfil de digitalización asociado al proceso de digitalización del documento " + docId + " de BBDD...");
		}
		res = scanProfileDAO.getScanProfileOrgUnitByDoc(docId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Perfil de digitalización en unidad organizativa " + res.getId() + " recuperado de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera el plugin configurado para una fase dada y el perfil de
	 * digitalización asociado a un documento digitalizado.
	 * 
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @param docId
	 *            identificador de documento.
	 * @return la relación de parámetros configurados para el plugin configurado
	 *         para una fase dada y el perfil de digitalización asociado a un
	 *         documento digitalizado.
	 * @throws DAOException
	 *             Si no existe el documento digitalizado, o se produce algún
	 *             error en la ejecución de la consulta de BBDD.
	 */
	public List<ParameterDTO> getScanProfileOrgUnitPluginByStageAndDoc(Long scanProcessStageId, Long docId) throws DAOException {
		List<ParameterDTO> res = new ArrayList<ParameterDTO>();
		ScanProfileOrgUnitPluginDTO spoup;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los parámetros del plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		// Validaciones de datos
		try {
			DataValidationUtilities.checkIdentifier(scanProcessStageId);
			DataValidationUtilities.checkIdentifier(docId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + " de BBDD...");
		}
		spoup = scanProfileDAO.getScanProfileOrgUnitPluginByStageAndDoc(scanProcessStageId, docId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los parámetros del plugin " + spoup.getPlugin() + " de BBDD...");
		}
		res = pluginDAO.getParametersByPlugin(spoup.getPlugin());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperados " + res.size() + " parámetros del plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + " de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera los plugins configurados y sus parámetros para una fase dada y
	 * el perfil de digitalización asociado a una lista de documentos
	 * digitalizados.
	 * 
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @param docIds
	 *            Identificadores de documentos.
	 * @return relación de plugins configurados para una fase dada y el perfil
	 *         de digitalización asociado a una lista de documentos
	 *         digitalizados, y sus parámetros.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la consulta de
	 *             BBDD.
	 */
	public Map<ScanProfileOrgUnitPluginDTO, List<ParameterDTO>> getScanProfileOrgUnitPluginByStageAndDocs(Long scanProcessStageId, List<Long> docIds) throws DAOException {
		List<ParameterDTO> params = new ArrayList<ParameterDTO>();
		Map<ScanProfileOrgUnitPluginDTO, List<ParameterDTO>> res = new HashMap<ScanProfileOrgUnitPluginDTO, List<ParameterDTO>>();
		List<ScanProfileOrgUnitPluginDTO> spoups;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los plugins y parámetros configurados para el perfil de digitalización de los documentos {} en la fase {} ...", docIds, scanProcessStageId);
			LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		}

		// Validaciones de datos
		try {
			DataValidationUtilities.checkIdentifier(scanProcessStageId);
			DataValidationUtilities.checkCollection(docIds);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los plugins configurados para el perfil de digitalización de los documentos {} en la fase de BBDD...", docIds, scanProcessStageId);
		}
		spoups = scanProfileDAO.getScanProfileOrgUnitPluginByStageAndDocs(scanProcessStageId, docIds);
		if (spoups != null) {
			for (ScanProfileOrgUnitPluginDTO spoup: spoups) {

				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Recuperando los parámetros del plugin " + spoup.getPlugin() + " de BBDD...");
				}
				params = pluginDAO.getParametersByPlugin(spoup.getPlugin());
				if (params != null) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("[WEBSCAN-MODEL] Recuperados {} parámetros del plugin {} de BBDD.", params.size(), spoup.getPlugin());
					}
					res.put(spoup, params);
				}
			}
		}

		return res;
	}

	/**
	 * Recupera el plugin establecido para una fase y configuración de perfil de
	 * digitalización en unidad organziativa determinados.
	 * 
	 * @param scanProfileId
	 *            identificador de perfil de digitalización.
	 * @param orgUnitId
	 *            identificador de unidad organizativa.
	 * @param scanProcessStageId
	 *            fase del proceso de digitalización ejecutada.
	 * @return La configuración de plugin establecida para una fase y
	 *         configuración de perfil de digitalización en unidad organziativa.
	 * @throws DAOException
	 *             Si los parámetros no son válidos, o se produce algún error en
	 *             la ejecución de la consulta de BBDD.
	 */
	public ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(Long scanProfileId, Long orgUnitId, Long scanProcessStageId) throws DAOException {
		ScanProfileOrgUnitPluginDTO res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + ", en la unidad organizativa " + orgUnitId + ", en la fase " + scanProcessStageId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		// Validaciones de datos
		try {
			DataValidationUtilities.checkIdentifier(scanProfileId);
			DataValidationUtilities.checkIdentifier(orgUnitId);
			DataValidationUtilities.checkIdentifier(scanProcessStageId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + ", en la unidad organizativa " + orgUnitId + ", en la fase " + scanProcessStageId + " de BBDD...");
		}

		res = scanProfileDAO.getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(scanProfileId, orgUnitId, scanProcessStageId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la configuración de plugin " + res.getId() + " para el perfil de digitalización " + scanProfileId + ", en la unidad organizativa " + orgUnitId + ", en la fase " + scanProcessStageId + " de BBDD.");
		}
		return res;
	}

	/**
	 * Recupera la relación de definiciones de documentos configuradas para un
	 * determinado perfil de digitalización en una unidad organizativa.
	 * 
	 * @param scanProfile
	 *            Perfil de digitalización.
	 * @param orgUnit
	 *            Unidad organizativa.
	 * @return Lista de definiciones de documentos configuradas para un
	 *         determinado perfil de digitalización en una unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no se encuentra
	 *             configuración del perfil de digitalización para la unidad
	 *             organizativa especificada como parámetro de entrada, o se
	 *             produce algún error en la consulta a BBDD.
	 */
	public List<ScanProfileOrgUnitDocSpecDTO> getScanProfileOrgUnitDocSpecConf(ScanProfileDTO scanProfile, OrganizationUnitDTO orgUnit) throws DAOException {
		List<ScanProfileOrgUnitDocSpecDTO> res;
		ScanProfileOrgUnitDTO spou;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los tipos de documentos configurados para el perfil de digitalización " + scanProfile.getName() + " y la unidad organizativa " + orgUnit.getExternalId() + " ...");

			LOG.debug("[WEBSCAN-MODEL] Obteniendo el listado de tipos de documentos configurado para el perfil de digitalización " + scanProfile.getName() + " y la unidad organizativa " + orgUnit.getExternalId() + " de BBDD...");
		}

		spou = scanProfileDAO.getScanProfileOrgUnitByScanProfAndOrgUnit(scanProfile.getId(), orgUnit.getId());

		res = scanProfileDAO.getScanProfileOrgUnitDocSpecsByScanProfile(spou.getId());

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperados " + res.size() + " tipos de documentos configurados para el perfil de digitalización " + scanProfile.getName() + " y la unidad organizativa " + orgUnit.getExternalId() + " de BBDD.");
		}

		return res;
	}

	/**
	 * Comprueba si un perfil de digitalización configurado en una unidad
	 * organizativa posee documentos en proceso de digitalización.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Si el perfil de digitalización configurado en una unidad
	 *         organizativa posee documentos en proceso de digitalización,
	 *         retorna true. En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             configuración de perfil de digitalización en unidad
	 *             organizativa, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public Boolean hasScanProfileOrgUnitDocsInScanProccess(Long scanProfileOrgUnitId) throws DAOException {
		Boolean res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la comprobación de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(scanProfileOrgUnitId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = scanProfileDAO.hasScanProfileOrgUnitDocsInScanProccess(scanProfileOrgUnitId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Finalizada la comprobación de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ". Resultado: " + res);
		}
		return res;
	}

	/**
	 * Comprueba si un perfil de digitalización configurado en una unidad
	 * organizativa posee documentos en proceso de digitalización.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @param docSpecId
	 *            Identificador de definición de documentos.
	 * @return Si el perfil de digitalización configurado en una unidad
	 *         organizativa posee documentos en proceso de digitalización,
	 *         retorna true. En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public Boolean hasScanProfileOrgUnitDocSpecDocsInScanProccess(Long scanProfileOrgUnitId, Long docSpecId) throws DAOException {
		Boolean res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la comprobación de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(scanProfileOrgUnitId);
			DataValidationUtilities.checkIdentifier(docSpecId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = scanProfileDAO.hasScanProfileOrgUnitDocSpecDocsInScanProccess(scanProfileOrgUnitId, docSpecId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Finalizada la comprobación de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ". Resultado: " + res);
		}
		return res;
	}

	/**
	 * Comprueba que una determinada relación de configuraciones de perfiles de
	 * digitalización para unidades unidades organizativas sean correctas.
	 * 
	 * @param scanProfileOrgUnits
	 *            Lista de configuraciones de perfiles de digitalización para
	 *            unidades unidades organizativas.
	 * @param scanProfileId
	 *            Identificador de perfil de digitalziación.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	private void checkScanProfileOrgUnitConfig(Collection<ScanProfileOrgUnitDTO> scanProfileOrgUnits, Long scanProfileId) throws DAOException {
		if (scanProfileOrgUnits != null && !scanProfileOrgUnits.isEmpty()) {
			DataValidationUtilities.checkScanProfileOrgUnits(scanProfileOrgUnits, scanProfileId);
		}
	}

	/**
	 * Persiste en BBDD una lista de nuevas configuraciones de perfiles de
	 * digitalización para unidades unidades organizativas.
	 * 
	 * @param scanProfOrgUnits
	 *            Lista de perfiles de digitalización para unidades
	 *            organizativas.
	 * @param currentScanProfileOrgUnits
	 *            Listado actual de configuración del perfil de digitalización
	 *            en unidades organizativas.
	 * @return Mapa en el que cada entrada es identificada mediante la
	 *         configuración de un perfil de digitalización en una unidad
	 *         organizativa, y cuyo valor se corresponde con un mapa donde se
	 *         identifican las especificaciones de documentos empleadas (entrada
	 *         "DOC_SPECS"), y plugins configurados (entrada "PLUGINS").
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	private List<ScanProfileOrgUnitDTO> persistScanProfileOrgUnits(Collection<ScanProfileOrgUnitDTO> scanProfOrgUnits, List<ScanProfileOrgUnitDTO> currentScanProfileOrgUnits) throws DAOException {
		List<ScanProfileOrgUnitDTO> res = new ArrayList<ScanProfileOrgUnitDTO>();
		ScanProfileOrgUnitDTO spou;

		for (Iterator<ScanProfileOrgUnitDTO> it = scanProfOrgUnits.iterator(); it.hasNext();) {
			spou = it.next();

			spou = getCurrentScanProfileConfig(spou, currentScanProfileOrgUnits);
			res.add(spou);
		}

		return res;
	}

	/**
	 * Comprueba si una configuración de perfil de digitalización en unidad
	 * organizativa se encuentra en la lista de configuraciones dada, y si no
	 * existe lo crea.
	 * 
	 * @param scanProfileOrgUnitConfig
	 *            perfil de digitalización en unidad organizativa.
	 * @param currentScanProfileOrgUnits
	 *            lista de configuraciones de perfil de digitalización en unidad
	 *            organizativa.
	 * @return Si se encuentra el objeto, la configuración encontrada. En caso
	 *         contrario, una nueva configuración.
	 * @throws DAOException
	 *             Si se produce algún error al ejecutar la sentencia de
	 *             creación de configuración de perfil en unidad organizativa en
	 *             BBDD.
	 */
	private ScanProfileOrgUnitDTO getCurrentScanProfileConfig(ScanProfileOrgUnitDTO scanProfileOrgUnitConfig, List<ScanProfileOrgUnitDTO> currentScanProfileOrgUnits) throws DAOException {
		Boolean found = Boolean.FALSE;
		Long scanProfOrgUnitId = null;
		ScanProfileOrgUnitDTO spou;
		ScanProfileOrgUnitDTO res = null;

		if (currentScanProfileOrgUnits != null) {
			for (Iterator<ScanProfileOrgUnitDTO> it = currentScanProfileOrgUnits.iterator(); !found && it.hasNext();) {
				spou = it.next();
				if (spou.equals(scanProfileOrgUnitConfig)) {
					res = spou;
					found = Boolean.TRUE;
				}
			}
		}

		if (!found) {
			scanProfOrgUnitId = scanProfileDAO.createScanProfileOrgUnit(scanProfileOrgUnitConfig);
			res = scanProfileDAO.getScanProfileOrgUnit(scanProfOrgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Creada la configuración de perfil en unidad organizativa " + scanProfOrgUnitId + " para el perfil " + res.getScanProfile() + " y la unidad organizativa " + res.getOrgUnit());
			}
		}

		return res;
	}

	/**
	 * Fusiona los plugins y definiciones de documentos especificados para una
	 * relación de configuraciones de perfiles de digitalización para unidades
	 * organizativas.
	 * 
	 * @param orgUnitPlugins
	 *            Listado de plugins especificados para una relación de
	 *            configuraciones de perfiles de digitalización para unidades
	 *            organizativas.
	 * @param scanProfOrgUnits
	 *            lista definitiva de configuraciones de perfil de
	 *            digitalización en unidades organizativas.
	 * @param scanProfile
	 *            Perfil de digitalización.
	 * @param propagateActivation
	 *            Indica si la activación (true), o inhabilitación (false), del
	 *            perfil de digitalización será propagada a las diferentes
	 *            configuraciones del perfil en unidades organizativas.
	 * @return Mapa en el que cada entrada es identificada mediante la
	 *         configuración de un perfil de digitalización en una unidad
	 *         organizativa, y cuyo valor se corresponde con un mapa donde se
	 *         identifican las especificaciones de documentos empleadas (entrada
	 *         "DOC_SPECS"), y plugins configurados (entrada "PLUGINS").
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	private Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> mergeScanProfileOrgUnitPlugins(Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins, List<ScanProfileOrgUnitDTO> scanProfOrgUnits, ScanProfileDTO scanProfile, Boolean propagateActivation) throws DAOException {
		int spouIndex;
		Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> res = new HashMap<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>>();
		List<PluginScanProcStageDTO> scanProfOrgUnitConfigData;
		ScanProfileOrgUnitDTO spou;

		if (orgUnitPlugins != null && !orgUnitPlugins.isEmpty()) {

			for (Iterator<ScanProfileOrgUnitDTO> it = orgUnitPlugins.keySet().iterator(); it.hasNext();) {
				spou = it.next();
				scanProfOrgUnitConfigData = setScanProfOrgUnitPluginsActivationStatus(orgUnitPlugins.get(spou), scanProfile, propagateActivation);
				spouIndex = scanProfOrgUnits.lastIndexOf(spou);
				if (spouIndex != -1) {
					spou = scanProfOrgUnits.get(spouIndex);
				}
				res.put(spou, scanProfOrgUnitConfigData);
			}
		}

		return res;
	}

	/**
	 * Fusiona las definiciones de documentos especificadas para una relación de
	 * configuraciones de perfiles de digitalización para unidades
	 * organizativas.
	 * 
	 * @param orgUnitDocs
	 *            Listado de definiciones de documentos especificados para una
	 *            relación de configuraciones de perfiles de digitalización para
	 *            unidades organizativas.
	 * @param scanProfOrgUnits
	 *            lista definitiva de configuraciones de perfil de
	 *            digitalización en unidades organizativas.
	 * @param scanProfile
	 *            Perfil de digitalización.
	 * @param propagateActivation
	 *            Indica si la activación (true), o inhabilitación (false), del
	 *            perfil de digitalización será propagada a las diferentes
	 *            configuraciones del perfil en unidades organizativas.
	 * @return Mapa en el que cada entrada es identificada mediante la
	 *         configuración de un perfil de digitalización en una unidad
	 *         organizativa, y cuyo valor se corresponde con un mapa de
	 *         especificaciones de documentos empleadas y su estado de
	 *         activación.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	private Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> mergeScanProfileOrgUnitDocSpecs(Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs, List<ScanProfileOrgUnitDTO> scanProfOrgUnits, ScanProfileDTO scanProfile, Boolean propagateActivation) throws DAOException {
		int spouIndex;
		Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> res = new HashMap<ScanProfileOrgUnitDTO, Map<Long, Boolean>>();
		Map<Long, Boolean> scanProfOrgUnitConfigData;
		ScanProfileOrgUnitDTO spou;

		if (orgUnitDocs != null && !orgUnitDocs.isEmpty()) {
			for (Iterator<ScanProfileOrgUnitDTO> itDocs = orgUnitDocs.keySet().iterator(); itDocs.hasNext();) {
				spou = itDocs.next();

				scanProfOrgUnitConfigData = setScanProfOrgUnitDocSpecsActivationStatus(orgUnitDocs.get(spou), scanProfile, propagateActivation);
				spouIndex = scanProfOrgUnits.lastIndexOf(spou);
				if (spouIndex != -1) {
					spou = scanProfOrgUnits.get(spouIndex);
				}

				res.put(spou, scanProfOrgUnitConfigData);
			}
		}

		return res;
	}

	/**
	 * Retorna una determinada relación de configuraciones de plugins o
	 * definiciones de documentos para una unidad organizativa, estableciendo el
	 * estado de activación del perfil de digitalización si así ha sido
	 * especificado.
	 * 
	 * @param plugins
	 *            Relación de configuraciones de plugins para perfil, unidad
	 *            organizativa y fase de proceso de digitalización.
	 * @param scanProfile
	 *            Perfil de digitalización.
	 * @param propagateActivation
	 *            Indica si la activación (true), o inhabilitación (false), del
	 *            perfil de digitalización será propagada a la relación de
	 *            configuraciones.
	 * @return Relación de configuraciones de plugins o definiciones de
	 *         documentos de un determinado perfil de digitalización para una
	 *         unidad organizativa.
	 */
	private List<PluginScanProcStageDTO> setScanProfOrgUnitPluginsActivationStatus(List<PluginScanProcStageDTO> plugins, ScanProfileDTO scanProfile, Boolean propagateActivation) {
		List<PluginScanProcStageDTO> res = new ArrayList<PluginScanProcStageDTO>();

		if (plugins != null && !plugins.isEmpty()) {
			if (propagateActivation) {
				for (PluginScanProcStageDTO psps: plugins) {
					psps.setActive(scanProfile.getActive());
					res.add(psps);
				}
			} else {
				res.addAll(plugins);
			}
		}

		return res;
	}

	/**
	 * Retorna una determinada relación de configuraciones de plugins o
	 * definiciones de documentos para una unidad organizativa, estableciendo el
	 * estado de activación del perfil de digitalización si así ha sido
	 * especificado.
	 * 
	 * @param dataConfig
	 *            Relación de configuraciones de definiciones de documentos para
	 *            perfil de digitalización y unidad organizativa.
	 * @param scanProfile
	 *            Perfil de digitalización.
	 * @param propagateActivation
	 *            Indica si la activación (true), o inhabilitación (false), del
	 *            perfil de digitalización será propagada a la relación de
	 *            configuraciones.
	 * @return Relación de configuraciones de plugins o definiciones de
	 *         documentos de un determinado perfil de digitalización para una
	 *         unidad organizativa.
	 */
	private Map<Long, Boolean> setScanProfOrgUnitDocSpecsActivationStatus(Map<Long, Boolean> dataConfig, ScanProfileDTO scanProfile, Boolean propagateActivation) {
		Map<Long, Boolean> res = new HashMap<Long, Boolean>();

		if (propagateActivation) {
			for (Iterator<Long> it = dataConfig.keySet().iterator(); it.hasNext();) {
				Long id = it.next();
				res.put(id, scanProfile.getActive());
			}
		} else {
			res.putAll(dataConfig);
		}

		return res;
	}

	/**
	 * Elimina de BBDD la configuración de un determinado perfil de
	 * digitalización para una relación de unidades organizativas.
	 * 
	 * @param newScanProfOrgUnitConfigs
	 *            Nueva relación de configuraciones de un perfil de
	 *            digitalización para un conjunto de unidades organizativas.
	 * @param currentOrgUnits
	 *            Relación actual de configuraciones de un perfil de
	 *            digitalización para un conjunto de unidades organizativas.
	 * @throws DAOException
	 *             Si se produce algún error al ejecutar la sentencia de BBDD.
	 */
	private void removeScanProfOrgUnits(List<ScanProfileOrgUnitDTO> newScanProfOrgUnitConfigs, List<ScanProfileOrgUnitDTO> currentOrgUnits) throws DAOException {
		Boolean found;
		List<Long> currentOrgUnitIds;
		ScanProfileOrgUnitDTO spou;

		if (currentOrgUnits != null && !currentOrgUnits.isEmpty()) {
			currentOrgUnitIds = new ArrayList<Long>();
			for (ScanProfileOrgUnitDTO auxSpou: currentOrgUnits) {
				found = Boolean.FALSE;
				if (newScanProfOrgUnitConfigs != null && !newScanProfOrgUnitConfigs.isEmpty()) {
					for (Iterator<ScanProfileOrgUnitDTO> it = newScanProfOrgUnitConfigs.iterator(); !found && it.hasNext();) {
						spou = it.next();
						found = spou.equals(auxSpou);
					}
				}

				if (!found) {
					currentOrgUnitIds.add(auxSpou.getId());
				}
			}
			if (!currentOrgUnitIds.isEmpty()) {
				scanProfileDAO.removeScanProfileOrgUnits(currentOrgUnitIds);
			}
		}
	}

	/**
	 * Establece en BBDD los plugins (y estado de activación), y definiciones de
	 * documento (y estado de activación), para las configuraciones de un
	 * determinado perfil de digitalización en un conjunto de unidades
	 * organizativas.
	 * 
	 * @param scanProfOrgUnitPlugins
	 *            Relaciones de plugins establecidas para las configuraciones de
	 *            un determinado perfil de digitalización en un conjunto de
	 *            unidades organizativas.
	 * @param scanProfOrgUnitDocSpecs
	 *            Relaciones de definiciones de documentos establecidas para las
	 *            configuraciones de un determinado perfil de digitalización en
	 *            un conjunto de unidades organizativas.
	 * @param scanProfile
	 *            Perfil de digitalización.
	 * @throws DAOException
	 *             Si se produce algún error al ejecutar la sentencia de BBDD, o
	 *             no existe alguna configuración de perfil de digitalización
	 *             para unidad organizativa.
	 */
	private void setScanProfOrgUnitConfig(Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> scanProfOrgUnitPlugins, Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> scanProfOrgUnitDocSpecs, ScanProfileDTO scanProfile) throws DAOException {
		ScanProfileOrgUnitDTO scanProfOrgUnit;

		// Se persisten los plugins y documentos para las configuraciones del
		// perfil de digitalización por unidad organizativa
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten las configuraciones de plugins en unidades organizativas para el perfil de digitalización: " + scanProfile.getName() + " ...");
		}
		for (Iterator<ScanProfileOrgUnitDTO> it = scanProfOrgUnitPlugins.keySet().iterator(); it.hasNext();) {
			scanProfOrgUnit = it.next();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Persisitiendo plugins para la unidad organizativa " + scanProfOrgUnit.getId() + " para el perfil de digitalización, denominado " + scanProfile.getName() + " ...");
			}
			scanProfileDAO.setScanProfileOrgUnitPlugins(scanProfOrgUnitPlugins.get(scanProfOrgUnit), scanProfOrgUnit.getId());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Plugins persisitidos para la unidad organizativa " + scanProfOrgUnit.getId() + " para el perfil de digitalización, denominado " + scanProfile.getName() + " ...");
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten las definiciones de documentos en unidades organizativas para el perfil de digitalización: " + scanProfile.getName() + " ...");
		}
		for (Iterator<ScanProfileOrgUnitDTO> it = scanProfOrgUnitDocSpecs.keySet().iterator(); it.hasNext();) {
			scanProfOrgUnit = it.next();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Persisitiendo definiciones de documentos para la unidad organizativa " + scanProfOrgUnit.getId() + " para el perfil de digitalización, denominado " + scanProfile.getName() + " ...");
			}
			scanProfileDAO.setScanProfileOrgUnitDocSpecs(scanProfOrgUnitDocSpecs.get(scanProfOrgUnit), scanProfOrgUnit.getId());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definiciones de documentos persisitidos para la unidad organizativa " + scanProfOrgUnit.getId() + " para el perfil de digitalización, denominado " + scanProfile.getName() + " ...");
			}
		}
	}

	/*************************************************************************************/
	/*								Unidades Organizativas					 			 */
	/*************************************************************************************/

	/**
	 * Recupera una unidad organizativa a partir de su identificador.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return La unidad organizativa solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no se encuentra
	 *             la unidad organizativa, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public OrganizationUnitDTO getOrganizationUnit(Long orgUnitId) throws DAOException {
		OrganizationUnitDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la unidad organizativa " + orgUnitId + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(orgUnitId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = orgUnitDAO.getOrganizationUnit(orgUnitId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Unidad organizativa " + orgUnitId + "recuperada.");
		}
		return res;
	}

	/**
	 * Recupera una unidad organizativa a partir de su identificador externo.
	 * 
	 * @param externalId
	 *            Identificador externo.
	 * @return La unidad organizativa solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no se encuentra
	 *             la unidad organizativa, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public OrganizationUnitDTO getOrgUnitByExternalId(String externalId) throws DAOException {
		OrganizationUnitDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la unidad organizativa denominada " + externalId + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(externalId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = orgUnitDAO.getOrgUnitByExternalId(externalId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Unidad organizativa " + externalId + "recueprada.");
		}
		return res;
	}

	/**
	 * Recupera el conjunto global de unidades organizativas registradas en el
	 * sistema, retornando para cada una de ellas además el listado de unidades
	 * organizativas hija. Pueden existir varias unidades organizativas raíz.
	 * 
	 * @return Conjunto global de unidades organizativas registradas en el
	 *         sistema.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public List<OrganizationUnitTreeDTO> getOrganizationUnitsTree() throws DAOException {
		List<OrganizationUnitTreeDTO> res = new ArrayList<OrganizationUnitTreeDTO>();

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las unidades organizativas registradas en el sistema ...");

		res = orgUnitDAO.getOrganizationUnitsTree();

		LOG.debug("[WEBSCAN-MODEL] Unidades organizativas registradas en el sistema recuepradas.");

		return res;
	}

	/**
	 * Crea un nueva unidad organizativa en el sistema.
	 * 
	 * @param orgUnit
	 *            Nueva unidad organizativa.
	 * @return El identificador de la unidad organizativa creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public Long createOrganizationUnit(OrganizationUnitDTO orgUnit) throws DAOException {
		Long res;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la creación de una nueva unidad organizativa ...");
		LOG.debug("[WEBSCAN-MODEL] Se valida la nueva unidad organizativa ...");

		// Validaciones de datos: unidad organizativa
		DataValidationUtilities.checkOrganizationUnit(orgUnit);

		LOG.debug("[WEBSCAN-MODEL] Se persiste la nueva unidad organizativa en BBDD ...");

		res = orgUnitDAO.createOrganizationUnit(orgUnit);

		return res;
	}

	/**
	 * Actualiza una unidad organizativa registrado en el sistema..
	 * 
	 * @param orgUnit
	 *            Unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             unidad organizativa, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	public void updateOrganizationUnit(OrganizationUnitDTO orgUnit) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la actualización de la unidad organizativa " + orgUnit.getId() + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se valida la unidad organizativa ...");

		// Validaciones de datos: unidad organizativa
		DataValidationUtilities.checkOrganizationUnit(orgUnit);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se persisten los cambios de la unidad organizativa " + orgUnit.getId() + " en BBDD ...");
		}

		orgUnitDAO.updateOrganizationUnit(orgUnit);
	}

	/**
	 * Elimina una unidad organizativa registrada en el sistema, borrando además
	 * todas sus unidades organizativas hija.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             unidad organizativa, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	public void removeOrganizationUnit(Long orgUnitId) throws DAOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia el borrado de la unidad organizativa " + orgUnitId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(orgUnitId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		orgUnitDAO.removeOrganizationUnit(orgUnitId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Unidad organizativa " + orgUnitId + " eliminada correctamente.");
		}
	}

	/**
	 * Comprueba si una unidad organizativa, o alguna de sus hijas poseen
	 * documentos en proceso de digitalización.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             unidad organizativa, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	public Boolean hasUnitOrgDocsInScanProccess(Long orgUnitId) throws DAOException {
		Boolean res;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la comprobación de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(orgUnitId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}

		res = hasUnitOrgDocsInScanProccessRec(orgUnitId, orgUnitDAO.getChildOrganizationUnits(orgUnitId));
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Finalizada la comprobación de documentos en proceso de digitalización para la unidad organizativa " + orgUnitId + ". Resultado: " + res);
		}
		return res;
	}

	/**
	 * Metodo auxiliar recursivo que comprueba si una unidad organizativa, o
	 * alguna de sus hijas poseen documentos en proceso de digitalización.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @param childOrganizationUnits
	 *            Relación de unidades organizativas hija.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             unidad organizativa, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	public Boolean hasUnitOrgDocsInScanProccessRec(Long orgUnitId, List<OrganizationUnitDTO> childOrganizationUnits) throws DAOException {
		Boolean res = Boolean.FALSE;
		OrganizationUnitDTO childOrganizationUnit;

		res = orgUnitDAO.hasUnitOrgDocsInScanProccess(orgUnitId);

		if (!res) {
			for (Iterator<OrganizationUnitDTO> it = childOrganizationUnits.iterator(); !res && it.hasNext();) {
				childOrganizationUnit = it.next();
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Comprobando si la unidad organizativa " + orgUnitId + " tiene documentos en proceso de digitalización ...");
				}
				res = hasUnitOrgDocsInScanProccessRec(childOrganizationUnit.getId(), orgUnitDAO.getChildOrganizationUnits(childOrganizationUnit.getId()));
			}
		}

		return res;
	}

	/*************************************************************************************/
	/*								Trazas de auditoría					 				 */
	/*************************************************************************************/

	/**
	 * Recupera el número total de trazas de auditoría registradas en el
	 * sistema.
	 * 
	 * @return número total de trazas de auditoría registradas en el sistema.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public Long countAuditOperations() throws DAOException {
		Long res;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación del número total de trazas de auditoría registradas en el sistema ...");

		res = auditDAO.countAuditOperations();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Número total de trazas de auditoría registrada en el sistema: " + res + ".");
		}
		return res;
	}

	/**
	 * Recupera el número total de trazas de auditoría registradas en el sistema
	 * que cumplen una serie de criterios de búsqueda o filtrado.
	 * 
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @return número total de trazas de auditoría registradas en el sistema que
	 *         cumplen una serie de criterios de búsqueda o filtrado.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public Long countAuditOperations(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate) throws DAOException {
		Long res;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación del número total de trazas de auditoría registradas en el sistema para una búsqueda soliictada ...");

		res = auditDAO.countAuditOperations(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Número total de trazas de auditoría registrada en el sistema para la búsqueda solicitada: " + res + ".");
		}
		return res;
	}

	/**
	 * Obtiene el listado de trazas de auditoría registradas en el sistema,
	 * permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param auditOpOrderByColumns
	 *            clausulas de ordenación sobre trazas de auditoría.
	 * @param auditEventOrderByColumns
	 *            clausulas de ordenación sobre eventos de auditoría.
	 * @return listado de de trazas de auditoría.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<AuditOperationDTO> listAuditOperations(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> auditOpOrderByColumns, List<EntityOrderByClause> auditEventOrderByColumns) throws DAOException {
		List<AuditOperationDTO> res;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de las trazas de auditoría registradas en el sistema ...");

		DataValidationUtilities.checkAuditOperationGroupByClause(auditOpOrderByColumns);
		DataValidationUtilities.checkAuditEventGroupByClause(auditEventOrderByColumns);

		res = auditDAO.listAuditOperations(pageNumber, elementsPerPage, auditOpOrderByColumns, auditEventOrderByColumns);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " trazas de auditoría recuepradas.");
		}
		return res;
	}

	/**
	 * Obtiene el listado de trazas de auditoría registradas en el sistema,
	 * permitiendo la paginación, la ordenación del resultado y el filtrado de
	 * los mismos mediante los siguientes datos: - Denominaciones de las trazas
	 * de auditoría. - Estado de las trazas de auditoría. - Periodo de
	 * establecimiento del estado de las trazas de auditoría. - Denominaciones
	 * de los eventos de las trazas de auditoría. - Estado de los eventos de de
	 * las trazas de auditoría. - Periodo de establecimiento del estado de los
	 * eventos de las trazas de auditoría. Todos los filtros son opcionales.
	 * 
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param auditOpOrderByColumns
	 *            clausulas de ordenación sobre trazas de auditoría.
	 * @param auditEventOrderByColumns
	 *            clausulas de ordenación sobre eventos de auditoría.
	 * @return listado de trazas de auditoría.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<AuditReportDTO> listAndQueryAuditOperations(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> auditOpOrderByColumns, List<EntityOrderByClause> auditEventOrderByColumns) throws DAOException {
		List<AuditReportDTO> res;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación y filtrado de las traza de auditoría registradas en el sistema ...");

		DataValidationUtilities.checkAuditOperationGroupByClause(auditOpOrderByColumns);
		DataValidationUtilities.checkAuditEventGroupByClause(auditEventOrderByColumns);

		res = auditDAO.listAuditOperations(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate, pageNumber, elementsPerPage, auditOpOrderByColumns, auditEventOrderByColumns);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " trazas de auditoría recuepradas.");
		}
		return res;
	}

	/**
	 * Recupera la relación de denominaciones de trazas de auditoría registrada
	 * en el sistema.
	 * 
	 * @return la relación de denominaciones de trazas de auditoría registrada
	 *         en el sistema.
	 * @throws DAOException
	 *             Si ocurre algún error.
	 */
	public List<AuditOperationName> listAuditOperationNames() throws DAOException {
		List<AuditOperationName> res = new ArrayList<AuditOperationName>();
		AuditOperationName[ ] auditOperationNames = AuditOperationName.values();

		for (int i = 0; i < auditOperationNames.length; i++) {
			res.add(auditOperationNames[i]);
		}

		return res;
	}

	/**
	 * Recupera la relación de estados de trazas de auditoría registrada en el
	 * sistema.
	 * 
	 * @return la relación de estados de trazas de auditoría registrada en el
	 *         sistema.
	 * @throws DAOException
	 *             Si ocurre algún error.
	 */
	public List<AuditOperationStatus> listAuditOperationStatus() throws DAOException {
		List<AuditOperationStatus> res = new ArrayList<AuditOperationStatus>();
		AuditOperationStatus[ ] auditOperationStatus = AuditOperationStatus.values();

		for (int i = 0; i < auditOperationStatus.length; i++) {
			res.add(auditOperationStatus[i]);
		}

		return res;
	}

	/**
	 * Recupera la relación de denominaciones de eventos de trazas de auditoría
	 * registrada en el sistema.
	 * 
	 * @return la relación de denominaciones de trazas de auditoría registrada
	 *         en el sistema.
	 * @throws DAOException
	 *             Si ocurre algún error.
	 */
	public List<AuditEventName> listAuditEventNames() throws DAOException {
		List<AuditEventName> res = new ArrayList<AuditEventName>();
		AuditEventName[ ] auditEventNames = AuditEventName.values();

		for (int i = 0; i < auditEventNames.length; i++) {
			res.add(auditEventNames[i]);
		}

		return res;
	}

	/**
	 * Recupera la relación de estados de eventos de trazas de auditoría
	 * registrada en el sistema.
	 * 
	 * @return la relación de estados de eventos de trazas de auditoría
	 *         registrada en el sistema.
	 * @throws DAOException
	 *             Si ocurre algún error.
	 */
	public List<AuditOpEventResult> listAuditOpEventResults() throws DAOException {
		List<AuditOpEventResult> res = new ArrayList<AuditOpEventResult>();
		AuditOpEventResult[ ] auditOpEventResults = AuditOpEventResult.values();

		for (int i = 0; i < auditOpEventResults.length; i++) {
			res.add(auditOpEventResults[i]);
		}

		return res;
	}

	/**
	 * Recupera una traza de auditoría a partir de su identificador.
	 * 
	 * @param auditOperationId
	 *            Identificador de traza de auditoría.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public AuditOperationDTO getAuditOperation(Long auditOperationId) throws DAOException {
		AuditOperationDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la traza de auditoría con identificador " + auditOperationId + " ...");
		}
		try {
			DataValidationUtilities.checkIdentifier(auditOperationId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = auditDAO.getAuditOperation(auditOperationId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la traza de auditoría con identificador " + auditOperationId + ".");
		}
		return res;
	}

	/**
	 * Recupera una traza de auditoría que posea un evento ejecutado por usuario
	 * dado y contenga como información adicional un texto determinado.
	 * 
	 * @param user
	 *            Nombre de usuario.
	 * @param additionalInfo
	 *            Texto que debe estar incluido en la información adicional del
	 *            evento.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public AuditOperationDTO getAuditOperation(String user, String additionalInfo) throws DAOException {
		AuditOperationDTO res = null;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la traza de auditoría para el usuario " + user + ", e información adicional: " + additionalInfo + "...");
		}
		try {
			DataValidationUtilities.checkIdentifier(user);
			DataValidationUtilities.checkIdentifier(additionalInfo);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		res = auditDAO.getAuditOpByUserAndAdditionalInfoEvents(user, additionalInfo);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la traza de auditoría para el usuario " + user + ", e información adicional: " + additionalInfo + ".");
		}
		return res;
	}

	/**
	 * Crea una nueva traza de auditoría en el sistema, sin eventos, y cuyo
	 * estado es AuditOperationStatus.OPEN.
	 * 
	 * @param auditOperation
	 *            Nueva traza de auditoría.
	 * @return El identificador de la traza de auditoría creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public Long openAuditOperation(AuditOperationDTO auditOperation) throws DAOException {
		Long res = null;

		res = openAuditOperation(auditOperation, new ArrayList<AuditEventDTO>());

		return res;
	}

	/**
	 * Crea una nueva traza de auditoría en el sistema, cuyo estado es
	 * AuditOperationStatus.OPEN, y su relación de eventos inicial.
	 * 
	 * @param auditOperation
	 *            Nueva traza de auditoría.
	 * @param auditEvents
	 *            Lista de eventos de la traza de auditoría.
	 * @return El identificador de la traza de auditoría creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public Long openAuditOperation(AuditOperationDTO auditOperation, List<AuditEventDTO> auditEvents) throws DAOException {
		Long res;
		AuditOperationDTO auxOp;
		res = null;

		LOG.debug("[WEBSCAN-MODEL] Se inicia la creación de una nueva traza de auditoría ...");

		LOG.debug("[WEBSCAN-MODEL] Se validan los datos de la nueva nueva traza de auditoría y sus eventos ...");

		DataValidationUtilities.checkAuditOperation(auditOperation);
		DataValidationUtilities.checkAuditEvents(auditEvents);

		LOG.debug("[WEBSCAN-MODEL] Registrando la traza de auditoría en BBDD...");
		auxOp = new AuditOperationDTO();
		auxOp.setName(auditOperation.getName());
		auxOp.setStatus(AuditOperationStatus.OPEN);
		auxOp.setStartDate(auditOperation.getStartDate());
		auxOp.setLastUpdateDate(auditOperation.getLastUpdateDate());

		res = auditDAO.createAuditOperation(auxOp);

		LOG.debug("[WEBSCAN-MODEL] Registrando los eventos de la traza de auditoría en BBDD...");

		if (auditEvents != null && !auditEvents.isEmpty()) {
			auditDAO.addAuditEvents(auditEvents, res);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Traza de auditoría con identificador " + res + " creada correctamente.");
		}
		return res;
	}

	/**
	 * Cierra una traza de auditoría, actualizando su información y estado en
	 * BBDD.
	 * 
	 * @param auditOperation
	 *            traza de auditoría.
	 * @return Identificador de la traza de auditoría cerrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public Long closeAuditOperation(AuditOperationDTO auditOperation) throws DAOException {
		return closeAuditOperation(auditOperation, new ArrayList<AuditEventDTO>());
	}

	/**
	 * Cierra una traza de auditoría, actualizando su información y estado en
	 * BBDD, e incorporando por último la lista de eventos especificada como
	 * parámetro de entrada.
	 * 
	 * @param auditOperation
	 *            traza de auditoría.
	 * @param auditEvents
	 *            Lista de eventos de la traza de auditoría.
	 * @return Identificador de la traza de auditoría cerrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public Long closeAuditOperation(AuditOperationDTO auditOperation, List<AuditEventDTO> auditEvents) throws DAOException {
		Long res;
		AuditOperationDTO auxOp;

		LOG.debug("[WEBSCAN-MODEL] Se inicia el cierre de una traza de auditoría ...");

		LOG.debug("[WEBSCAN-MODEL] Se validan los datos de la traza de auditoría y sus nuevos eventos ...");

		DataValidationUtilities.checkAuditOperation(auditOperation);
		DataValidationUtilities.checkClosedAuditOperationStatus(auditOperation.getStatus());
		DataValidationUtilities.checkAuditEvents(auditEvents);

		if (auditOperation.getId() == null) {
			// Nueva operación de auditoria
			LOG.debug("[WEBSCAN-MODEL] Creando la traza de auditoría en BBDD...");
			auxOp = new AuditOperationDTO();
			auxOp.setName(auditOperation.getName());
			auxOp.setStatus(auditOperation.getStatus());
			auxOp.setStartDate(auditOperation.getStartDate());
			auxOp.setLastUpdateDate(auditOperation.getLastUpdateDate());
			res = auditDAO.createAuditOperation(auxOp);
		} else {
			LOG.debug("[WEBSCAN-MODEL] Actualizando la traza de auditoría en BBDD...");
			auxOp = auditDAO.getAuditOperation(auditOperation.getId());
			auxOp.setStatus(auditOperation.getStatus());
			auxOp.setLastUpdateDate(auditOperation.getLastUpdateDate());
			auditDAO.updateAuditOperation(auxOp);
			res = auxOp.getId();
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Registrando los eventos de la traza de auditoría " + res + " en BBDD...");
		}

		if (auditEvents != null && !auditEvents.isEmpty()) {
			auditDAO.addAuditEvents(auditEvents, res);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Traza de auditoría con identificador " + res + " cerrada correctamente.");
		}

		return res;
	}

	/**
	 * Recupera la traza de auditoría más reciente con una determinada
	 * denominación, y que posea un evento ejecutado por usuario dado.
	 * 
	 * @param auditOperationName
	 *            Denominación de traza de auditoría.
	 * @param user
	 *            Nombre de usuario.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public AuditOperationDTO getLastAuditOperationByOpNameAndUser(AuditOperationName auditOperationName, String user) throws DAOException {
		AuditOperationDTO res = new AuditOperationDTO();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la última traza de auditoría " + auditOperationName + "registrada en el sistema y con algún evento realizado por el usuario " + user + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(auditOperationName);
			DataValidationUtilities.checkIdentifier(user);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recupera la última traza de auditoría " + auditOperationName + "registrada en el sistema y con algún evento realizado por el usuario " + user + " de BBDD ...");
		}

		res = auditDAO.getLastAuditOperationByOpNameAndUser(auditOperationName, user);

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la traza de auditoría  " + res.getId() + ".");
		}

		return res;
	}

	/**
	 * Recupera la traza de auditoría más reciente con una determinada
	 * denominación y estado, y que posea un evento ejecutado por usuario dado.
	 * 
	 * @param auditOperationName
	 *            Denominación de traza de auditoría.
	 * @param auditOperationStatus
	 *            Estado d ela traza de auditoría.
	 * @param user
	 *            Nombre de usuario.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public AuditOperationDTO getLastAuditOperationByOpNameStatusAndUser(AuditOperationName auditOperationName, AuditOperationStatus auditOperationStatus, String user) throws DAOException {
		AuditOperationDTO res = new AuditOperationDTO();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de la última traza de auditoría " + auditOperationName + "registrada en el sistema, con estado " + auditOperationStatus + ", y con algún evento realizado por el usuario " + user + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(auditOperationName);
			DataValidationUtilities.checkIdentifier(auditOperationStatus);
			DataValidationUtilities.checkIdentifier(user);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recupera la última traza de auditoría " + auditOperationName + "registrada en el sistema, con estado " + auditOperationStatus + ", y con algún evento realizado por el usuario " + user + " de BBDD ...");
		}
		res = auditDAO.getLastAuditOperationByOpNameStatusAndUser(auditOperationName, auditOperationStatus, user);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperada la traza de auditoría  " + res.getId() + ".");
		}
		return res;
	}

	/**
	 * Recupera los eventos de una traza de auditoría.
	 * 
	 * @param auditOperationId
	 *            Identificador de traza de auditoría.
	 * @return Lista de eventos de la traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de auditoría, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public List<AuditEventDTO> getAuditEventsByOperation(Long auditOperationId) throws DAOException {
		List<AuditEventDTO> res = new ArrayList<AuditEventDTO>();
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los eventos da la traza de auditoría " + auditOperationId + " ...");
		}
		LOG.debug("[WEBSCAN-MODEL] Se validan los parámetros de entrada ...");
		try {
			DataValidationUtilities.checkIdentifier(auditOperationId);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recuperan los eventos de traza de auditoría " + auditOperationId + " de BBDD ...");
		}

		res = auditDAO.getAuditEventsByOperation(auditOperationId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res.size() + " eventos recuperados para la traza de auditoría  " + auditOperationId + ".");
		}

		return res;
	}

	/**
	 * Añade una relación de eventos a una traza de auditoría.
	 * 
	 * @param auditEvents
	 *            Lista de eventos de auditoría
	 * @param auditOperationId
	 *            Identificador de traza de auditoría.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de auditoría, se encuentra cerrada, o se produce algún
	 *             error en la ejecución de las sentencias de BBDD.
	 */
	public void addAuditEvents(List<AuditEventDTO> auditEvents, Long auditOperationId) throws DAOException {
		AuditOperationDTO auditOperation;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la incorporación de nuevos eventos a la traza de auditoría " + auditOperationId + " ...");
		}

		LOG.debug("[WEBSCAN-MODEL] Se validan los datos de la traza de auditoría y sus nuevos eventos ...");
		try {
			DataValidationUtilities.checkIdentifier(auditOperationId);
			DataValidationUtilities.checkCollection(auditEvents);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		DataValidationUtilities.checkAuditEvents(auditEvents);

		LOG.debug("[WEBSCAN-MODEL] Se recupera la traza de auditoría de BBDD ...");

		auditOperation = auditDAO.getAuditOperation(auditOperationId);

		LOG.debug("[WEBSCAN-MODEL] Se comprueba si se encuentra abierta ...");

		DataValidationUtilities.checkOpenAuditOperationStatus(auditOperation.getStatus());
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Registrando los nuevos eventos de la traza de auditoría " + auditOperation.getId() + " en BBDD...");
		}
		auditDAO.addAuditEvents(auditEvents, auditOperation.getId());
		auditOperation.setLastUpdateDate(new Date());
		auditDAO.updateAuditOperation(auditOperation);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + auditEvents.size() + " eventos añadidos a la traza de auditoría  " + auditOperation.getId() + " correctamente.");
		}
	}

	/*************************************************************************************/
	/*			Metodos de acceso y establecimiento de atributos de la clase			 */
	/*************************************************************************************/

	/**
	 * Establece un nuevo DAO sobre las entidades plugin, parámetro,
	 * especificaciones de plugin y parámetro, y tipos de parámetros.
	 * 
	 * @param aPluginDAO
	 *            Nuevo DAO sobre las entidades plugin, parámetro,
	 *            especificaciones de plugin y parámetro, y tipos de parámetros.
	 */
	public void setPluginDAO(PluginDAO aPluginDAO) {
		this.pluginDAO = aPluginDAO;
	}

	/**
	 * Establece un nuevo DAO sobre la entidad fase de proceso de
	 * digitalización.
	 * 
	 * @param aScanProcessStageDAO
	 *            Nuevo DAO sobre la entidad fase de proceso de digitalización.
	 */
	public void setScanProcessStageDAO(ScanProcessStageDAO aScanProcessStageDAO) {
		this.scanProcessStageDAO = aScanProcessStageDAO;
	}

	/**
	 * Establece un nuevo DAO sobre las entidades perfil de digitalización, su
	 * especificación y relaciones.
	 * 
	 * @param aScanProfileDAO
	 *            Nuevo DAO sobre las entidades perfil de digitalización, su
	 *            especificación y relaciones.
	 */
	public void setScanProfileDAO(ScanProfileDAO aScanProfileDAO) {
		this.scanProfileDAO = aScanProfileDAO;
	}

	/**
	 * Establece un nuevo DAO sobre las entidades documento digitalizado, su
	 * especificación y relaciones.
	 * 
	 * @param aDocumentDAO
	 *            Nuevo DAO sobre las entidades documento digitalizado, su
	 *            especificación y relaciones.
	 */
	public void setDocumentDAO(DocumentDAO aDocumentDAO) {
		this.documentDAO = aDocumentDAO;
	}
	
	/**
	 * Establece un nuevo DAO sobre las entidades documento digitalizado, su
	 * especificación y relaciones.
	 * 
	 * @param aDocumentDAO
	 *            Nuevo DAO sobre las entidades documento digitalizado, su
	 *            especificación y relaciones.
	 */
	public void setBatchDAO(BatchDAO aBatchDAO) {
		this.batchDAO = aBatchDAO;
	}
	
	/**
	 * Establece un nuevo DAO sobre la entidad unidad organizativa.
	 * 
	 * @param aOrgUnitDAO
	 *            Nuevo DAO sobre la entidad unidad organizativa.
	 */
	public void setOrgUnitDAO(OrganizationUnitDAO aOrgUnitDAO) {
		this.orgUnitDAO = aOrgUnitDAO;
	}

	/**
	 * Establece un nuevo DAO sobre las entidades traza y evento de auditoría.
	 * 
	 * @param anAuditDAO
	 *            Nuevo DAO sobre las entidades traza y evento de auditoría.
	 */
	public void setAuditDAO(AuditDAO anAuditDAO) {
		this.auditDAO = anAuditDAO;
	}

	/**
	 * Establece la clase que habilita el acceso a los parámetros de
	 * configuración del Sistema.
	 * 
	 * @param aWebscanUtilParams
	 *            clase que habilita el acceso a los parámetros de configuración
	 *            del Sistema.
	 */
	public void setWebscanUtilParams(Utils aWebscanUtilParams) {
		this.webscanUtilParams = aWebscanUtilParams;
	}

	/*************************************************************************************/
	/*					Procesos de digitalización										 */
	/*************************************************************************************/
	/**
	 * Recupera la relación de procesos de digitalización.
	 * 
	 * @return la relación de procesos de digitalización sin finalizar.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public List<BatchDTO> getBatchs() throws DAOException {
		//List<EntityOrderByClause> orderByColumns = new ArrayList<EntityOrderByClause>();
		List<BatchDTO> res = new ArrayList<BatchDTO>();
		int queryResultSize = 0;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los procesos de digitalización.");
		}

		//try {
	//		DataValidationUtilities.checkIdentifier(scanProfileSpec);
	//	} catch (WebscanException e) {
	//		throw new DAOException(e.getCode(), e.getMessage(), e);
	//	}
		//orderByColumns.add(DocumentOrderByClause.BATCH_ORDER_NUM_ASC);
//		res = documentDAO.getActiveDocumentsByUsernameAndScanProfileSpec(username, scanProfileSpec, orderByColumns);
		res = batchDAO.getBatchs();

		if (res != null) {
			queryResultSize = res.size();
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] {} Procesos de digitalización recuperados.", queryResultSize);
		}
		
		return res;
	}
	
	public List<BatchDTO> getBatchs(List<Long> orgUnitIds, String functionalOrgs) throws DAOException {
		int queryResultSize = 0;

		List<BatchDTO> res = new ArrayList<BatchDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación de los documentos digitalizados, y sus trazas de ejecución, por usuarios de las unidades organizativas " + orgUnitIds + " y pertenecientes a la unidades orgánicas: " + functionalOrgs + "...");
		}
		LOG.debug("[WEBSCAN-MODEL] Validando parámetros de entrada ...");
		try {
			DataValidationUtilities.checkCollection(orgUnitIds);
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se recuperan los documentos digitalizados, y sus trazas de ejecución, por usuarios de las unidades organizativas " + orgUnitIds + " y pertenecientes a la unidades orgánicas: " + functionalOrgs + "...");
		}

		try {
			//res = documentDAO.getDocumentsAndLogsByOrgUnits(orgUnitIds, functionalOrgs);
			res = batchDAO.getBatchs(orgUnitIds, functionalOrgs);

		} catch (IllegalArgumentException e) {
			LOG.debug("[WEBSCAN-MODEL] Argumento invalido al recuperar documentos digitalizados.");
			return res;
		}

		if (res != null) {
			queryResultSize = res.size();
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + queryResultSize + " documentos digitalizados por usuarios de las unidades organizativas " + orgUnitIds + " y pertenecientes a la unidades orgánicas: " + functionalOrgs + " recuperados de BBDD.");
		}
		return res;
	}
	
	public Long getNumDocsbyBatchId(Long batchId) throws DAOException {

		Long res=null;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Se inicia la recuperación del número de documentos para el proces de digitalización " + batchId);
		}

		try {
			//res = documentDAO.getDocumentsAndLogsByOrgUnits(orgUnitIds, functionalOrgs);
			res = documentDAO.getNumDocsByBatchId(batchId);

		} catch (IllegalArgumentException e) {
			LOG.debug("[WEBSCAN-MODEL] Argumento invalido al recuperar documentos digitalizados.");
			return res;
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] " + res + " documentos digitalizados para el proceso de digitalización " + batchId);
		}
		return res;
	}
	/**
	 * Registra el histórico de un documento en el modelo de datos.
	 * 
	 * @param doc
	 *            documento digitalizado.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	public BatchDTO createBatch(BatchDTO aBatch, Long scanProfOrgUnitDocSpecId) throws DAOException {
		BatchDTO batchDTO;
		Long batchId;

		batchDTO = new BatchDTO();
		batchDTO.setBatchNameId(aBatch.getBatchNameId());
		batchDTO.setBatchReqId(aBatch.getBatchReqId());
		batchDTO.setStartDate(aBatch.getStartDate());
		batchDTO.setUsername(aBatch.getUsername());
		batchDTO.setFunctionalOrgs(aBatch.getFunctionalOrgs());
		
		batchId = batchDAO.createBatch(batchDTO, scanProfOrgUnitDocSpecId);
		
		batchDTO = batchDAO.getBatch(batchId);
		
		return batchDTO;
	}
	
	/**
	 * Registra el histórico de un documento en el modelo de datos.
	 * 
	 * @param doc
	 *            documento digitalizado.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	public BatchDTO getBatch(Long aBatchId) throws DAOException {
		BatchDTO batchDTO;
				
		batchDTO = batchDAO.getBatch(aBatchId);
		
		return batchDTO;
	}
}
