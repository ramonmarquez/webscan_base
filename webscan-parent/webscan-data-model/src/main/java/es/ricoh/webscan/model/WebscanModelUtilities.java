/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.WebscanModelUtilities.java.</p>
 * <b>Descripción:</b><p> Clase de utilidades del componente que implementa la capa de
 * acceso al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Clase de utilidades del componente que implementa la capa de acceso al modelo
 * de datos base de Webscan.
 * <p>
 * Clase WebscanModelUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class WebscanModelUtilities {

	/**
	 * Constructor method for the class WebscanModelUtilities.java.
	 */
	private WebscanModelUtilities() {
	}

	/**
	 * Constante que representa el número de milisegundos de un día.
	 */
	private static final long MILISECONDS_OF_A_DAY = 24 * 60 * 60 * 1000;

	/**
	 * Construye la cadena de caracteres que representa las clausulas de
	 * ordenación aplicadas una entidad de BBDD.
	 * 
	 * @param entityNamePrefix
	 *            alias de la entidad de BBDD sobre el que se aplican las
	 *            clausulas de ordenación.
	 * @param entitiesOrderByClause
	 *            Clausulas de ordenación sobre campos de una entidad de BBDD.
	 * @return Cadena de caracteres que representa las clausulas de ordenación
	 *         aplicadas una entidad de BBDD.
	 */
	public static String buildOrderByClause(String entityNamePrefix, List<EntityOrderByClause> entitiesOrderByClause) {
		String res = null;

		if (entitiesOrderByClause != null && !entitiesOrderByClause.isEmpty()) {
			res = "";
			for (EntityOrderByClause eobc: entitiesOrderByClause) {
				// user.name ASC, donde
				// entityNamePrefix = user.
				// eobc.getAttName() = name
				// eobc.getOrderByClause().name() = ASC
				res += entityNamePrefix + eobc.getAttName() + " " + eobc.getOrderByClause().name() + ",";
			}

			res = res.substring(0, res.length() - 1);
		}

		return res;
	}

	/**
	 * Añade un número de dias a una fecha dada. El número de días puede ser
	 * negativo.
	 * 
	 * @param date
	 *            Fecha.
	 * @param days
	 *            Número de días.
	 * @return La fecha resultante de sumar el número de días, especificado como
	 *         parámetro de entrada, a la fecha dada.
	 */
	public static Date addDays(Date date, Integer days) {
		Date res;

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);

		res = cal.getTime();

		return res;
	}

	/**
	 * Calcula la diferencia de días entre dos fechas dadas.
	 * 
	 * @param date1
	 *            Fecha 1
	 * @param date2
	 *            Fecha 2.
	 * @return Número de días entre ambas fechas.
	 */
	public static Integer differnceBetweenTwoDates(Date date1, Date date2) {
		Integer res;

		long diffDays = (date1.getTime() - date2.getTime()) / MILISECONDS_OF_A_DAY;

		if (diffDays < 0) {
			diffDays = diffDays * -1;
		}

		res = Long.valueOf(diffDays).intValue();

		return res;
	}
}
