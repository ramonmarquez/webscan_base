/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.impl.PluginDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre las entidades plugin, parámetro, especificaciones de
 * plugin, parámetro y tipos de parámetros, perteneciente al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.dao.PluginDAO;
import es.ricoh.webscan.model.domain.Parameter;
import es.ricoh.webscan.model.domain.ParameterSpecification;
import es.ricoh.webscan.model.domain.ParameterType;
import es.ricoh.webscan.model.domain.Plugin;
import es.ricoh.webscan.model.domain.PluginSpecification;
import es.ricoh.webscan.model.dto.MappingUtilities;
import es.ricoh.webscan.model.dto.ParameterDTO;
import es.ricoh.webscan.model.dto.ParameterSpecificationDTO;
import es.ricoh.webscan.model.dto.ParameterTypeDTO;
import es.ricoh.webscan.model.dto.PluginDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;

/**
 * Implementación JPA 2.0 de la capa DAO sobre las entidades plugin, parámetro,
 * especificaciones de plugin, parámetro y tipos de parámetros, perteneciente al
 * modelo de datos base de Webscan.
 * <p>
 * Clase PluginDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.dao.PluginDAO
 * @version 2.0.
 */
public final class PluginDAOJdbcImpl implements PluginDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PluginDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la manipulación de las entidades de incluidas en el
	 * contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * Constructor sin argumentos.
	 */
	public PluginDAOJdbcImpl() {
		super();
	}

	/**
	 * Establece un nuevo gestor de persistencia JPA.
	 * 
	 * @param aEntityManager
	 *            manager de persistencia.
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		this.entityManager = aEntityManager;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#listPluginSpecifications(java.lang.Long,
	 *      java.lang.Long, java.util.List)
	 */
	@Override
	public List<PluginSpecificationDTO> listPluginSpecifications(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		int first, maxResults;
		List<PluginSpecification> ddbbRes = new ArrayList<PluginSpecification>();
		List<PluginSpecificationDTO> res = new ArrayList<PluginSpecificationDTO>();
		Map<String, List<EntityOrderByClause>> pluginOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {

			LOG.debug("[WEBSCAN-MODEL] Recuperando las definiciones de plugins...");

			if (orderByColumns != null && !orderByColumns.isEmpty()) {
				pluginOrderByColumns.put("ps.", orderByColumns);
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT ps FROM PluginSpecification ps", pluginOrderByColumns);

			TypedQuery<PluginSpecification> query = entityManager.createQuery(jpqlQuery, PluginSpecification.class);

			if (elementsPerPage != null) {
				first = 0;
				maxResults = elementsPerPage.intValue();
				if (pageNumber != null) {
					first = (pageNumber.intValue() - 1) * maxResults;
				}

				query.setFirstResult(first);
				query.setMaxResults(maxResults);
			}

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las definiciones de plugins...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillPluginSpecificationListDto(ddbbRes);

			LOG.debug("[WEBSCAN-MODEL] Definiciones de plugins recuperadas: ", res.size());
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getPluginSpecification(java.lang.Long)
	 */
	@Override
	public PluginSpecificationDTO getPluginSpecification(Long pluginSpecificationId) throws DAOException {
		PluginSpecification entity;
		PluginSpecificationDTO res = null;
		String excMsg;

		try {
			entity = getPluginSpecificationEntity(pluginSpecificationId);
			res = MappingUtilities.fillPluginSpecificationDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin " + pluginSpecificationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getPluginSpecificationByName(java.lang.String)
	 */
	@Override
	public PluginSpecificationDTO getPluginSpecificationByName(String pluginSpecificationName) throws DAOException {
		PluginSpecification entity;
		PluginSpecificationDTO res = null;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de plugin con denominación " + pluginSpecificationName + " ...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getPluginSpecificationEntityByName(pluginSpecificationName, entityManager);
			res = MappingUtilities.fillPluginSpecificationDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de plugin, con denominación " + pluginSpecificationName + ", recuperada de BBDD (" + res.getId() + ").");
			}
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] No se encontro la definición de plugin con denominación " + pluginSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando, existe más de definición de plugin con denominación " + pluginSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin con denominación " + pluginSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de plugincon denominación " + pluginSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin con denominación " + pluginSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin con denominación " + pluginSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin con denominación " + pluginSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de plugincon denominación " + pluginSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin con denominación " + pluginSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getPluginSpecsByScanProcStageId(java.lang.Long)
	 */
	@Override
	public List<PluginSpecificationDTO> getPluginSpecsByScanProcStageId(Long scanProcessStageId) throws DAOException {
		List<PluginSpecification> ddbbRes = new ArrayList<PluginSpecification>();
		List<PluginSpecificationDTO> res = new ArrayList<PluginSpecificationDTO>();
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + "...");
			}
			jpqlQuery = "SELECT ps FROM PluginSpecification ps JOIN ps.scanProcessStages stages WHERE stages.id = :scanProcessStageId";

			TypedQuery<PluginSpecification> query = entityManager.createQuery(jpqlQuery, PluginSpecification.class);
			query.setParameter("scanProcessStageId", scanProcessStageId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + " ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillPluginSpecificationListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + " recuperadas: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las especificaciones de plugins que implementan la fase de proceso de digitalización: " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#listPluginSpecsAndScanProcessStages()
	 */
	@Override
	public Map<PluginSpecificationDTO, List<ScanProcessStageDTO>> listPluginSpecsAndScanProcessStages() throws DAOException {
		List<PluginSpecification> ddbbRes = new ArrayList<PluginSpecification>();
		Map<PluginSpecificationDTO, List<ScanProcessStageDTO>> res = new HashMap<PluginSpecificationDTO, List<ScanProcessStageDTO>>();
		String excMsg, jpqlQuery;

		try {

			LOG.debug("[WEBSCAN-MODEL] Recuperando las definiciones de plugins y fases que implementan ...");

			jpqlQuery = "SELECT ps FROM PluginSpecification ps";

			TypedQuery<PluginSpecification> query = entityManager.createQuery(jpqlQuery, PluginSpecification.class);

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las definiciones de plugins...");

			ddbbRes = query.getResultList();

			for (PluginSpecification ps: ddbbRes) {
				res.put(MappingUtilities.fillPluginSpecificationDto(ps), MappingUtilities.fillScanProcessStageListDto(ps.getScanProcessStages()));
			}

			LOG.debug("[WEBSCAN-MODEL] Definiciones de plugins recuperadas: ", res.size());
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins y fases que implementan: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de plugins y fases que implementan: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins y fases que implementan: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins y fases que implementan: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins y fases que implementan: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de plugins y fases que implementan: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins y fases que implementan: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#hasPluginSpecDocsInScanProccess(java.lang.Long)
	 */
	@Override
	public Boolean hasPluginSpecDocsInScanProccess(Long pluginSpecId) throws DAOException {
		Long numberOfDocs;
		Boolean res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + " ...");
			}
			TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(sd.id) FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile osp JOIN osp.orgScanProfilePlugins ospp JOIN ospp.plugin plugin JOIN plugin.specification pSpec WHERE sd.digProcEndDate IS NULL AND pSpec.id = :pluginSpecId", Long.class);
			query.setParameter("pluginSpecId", pluginSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + " ...");
			}
			numberOfDocs = query.getSingleResult();

			res = numberOfDocs > 0;
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Especificación de plugins " + pluginSpecId + " con " + numberOfDocs + " en proceso de digitalización.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la especificación de plugins " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getPluginSpecByScanProfileOrgUnitAndStage(java.lang.Long,
	 *      java.lang.String)
	 */
	@Override
	public PluginSpecificationDTO getPluginSpecByScanProfileOrgUnitAndStage(Long scanProfileOrgUnitId, String scanProcessStageName) throws DAOException {
		PluginSpecification entity;
		PluginSpecificationDTO res;
		String excMsg;

		try {

			LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de plugin establecida para ejecutar la fase {} en la configuración de perfil en unidad organizativa {} ...", scanProcessStageName, scanProfileOrgUnitId);

			TypedQuery<PluginSpecification> query = entityManager.createQuery("SELECT ps FROM PluginSpecification ps JOIN ps.plugins plugins JOIN plugins.orgScanProfilePlugins ospp JOIN ospp.orgScanProfile osp JOIN ospp.scanProcessStage sps WHERE osp.id = :scanProfileOrgUnitId AND sps.name = :scanProcessStageName", PluginSpecification.class);
			query.setParameter("scanProfileOrgUnitId", scanProfileOrgUnitId);
			query.setParameter("scanProcessStageName", scanProcessStageName);

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la definición de plugin establecida para ejecutar la fase {} en la configuración de perfil en unidad organizativa {}.", scanProcessStageName, scanProfileOrgUnitId);

			entity = query.getSingleResult();

			res = MappingUtilities.fillPluginSpecificationDto(entity);

			LOG.debug("[WEBSCAN-MODEL] Definición de plugin denominada recuperada: {}", res.getId());
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin establecida para ejecutar la fase " + scanProcessStageName + " en la configuración de perfil en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de plugin establecida para ejecutar la fase " + scanProcessStageName + " en la configuración de perfil en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin establecida para ejecutar la fase " + scanProcessStageName + " en la configuración de perfil en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin establecida para ejecutar la fase " + scanProcessStageName + " en la configuración de perfil en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin establecida para ejecutar la fase " + scanProcessStageName + " en la configuración de perfil en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin establecida para ejecutar la fase " + scanProcessStageName + " en la configuración de perfil en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin establecida para ejecutar la fase " + scanProcessStageName + " en la configuración de perfil en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de plugin establecida para ejecutar la fase " + scanProcessStageName + " en la configuración de perfil en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin establecida para ejecutar la fase " + scanProcessStageName + " en la configuración de perfil en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#createPluginSpecification(es.ricoh.webscan.model.dto.PluginSpecificationDTO)
	 */
	@Override
	public Long createPluginSpecification(PluginSpecificationDTO specification) throws DAOException {
		Long res = null;
		PluginSpecification entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando una nueva definición de plugin, denominada " + specification.getName() + " ...");
		}
		entity = MappingUtilities.fillPluginSpecificationEntity(specification);

		try {
			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva definición de plugin " + specification.getName() + " creada, identificador: " + res + ".");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de plugin " + specification.getName() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de plugin " + specification.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de plugin " + specification.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de plugin " + specification.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de plugin " + specification.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#updatePluginSpecification(es.ricoh.webscan.model.dto.PluginSpecificationDTO)
	 */
	@Override
	public void updatePluginSpecification(PluginSpecificationDTO specification) throws DAOException {
		PluginSpecification entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando la definición de plugin " + specification.getId() + " ...");
		}
		try {
			entity = getPluginSpecificationEntity(specification.getId());

			entity.setActive(specification.getActive());
			entity.setDescription(specification.getDescription());
			entity.setName(specification.getName());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando la definición de plugin " + specification.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de plugin " + specification.getId() + " actualizada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de plugin " + specification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de plugin " + specification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de plugin " + specification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de plugin " + specification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#removePluginSpecification(java.lang.Long)
	 */
	@Override
	public void removePluginSpecification(Long specificationId) throws DAOException {
		PluginSpecification entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Eliminando la definición de plugin " + specificationId + " ...");
		}
		try {
			entity = getPluginSpecificationEntity(specificationId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Eliminando la definición de plugin " + specificationId + " en BBDD ...");
			}
			entityManager.remove(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de plugin " + specificationId + " eliminada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando la definición de plugin " + specificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando la definición de plugin " + specificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando la definición de plugin " + specificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando la definición de plugin " + specificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getPlugin(java.lang.Long)
	 */
	@Override
	public PluginDTO getPlugin(Long pluginId) throws DAOException {
		Plugin entity;
		PluginDTO res = null;
		String excMsg;

		try {
			entity = CommonUtilitiesDAOJdbcImpl.getPluginEntity(pluginId, entityManager);
			res = MappingUtilities.fillPluginDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#hasPluginDocsInScanProccess(java.lang.Long)
	 */
	@Override
	public Boolean hasPluginDocsInScanProccess(Long pluginId) throws DAOException {
		Long numberOfDocs;
		Boolean res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el numero de documentos en proceso de digitalización para el plugin " + pluginId + " ...");
			}
			TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(sd.id) FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile osp JOIN osp.orgScanProfilePlugins ospp JOIN ospp.plugin plugin WHERE sd.digProcEndDate IS NULL AND plugin.id = :pluginId", Long.class);
			query.setParameter("pluginId", pluginId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el numero de documentos en proceso de digitalización para el plugin " + pluginId + " ...");
			}
			numberOfDocs = query.getSingleResult();

			res = numberOfDocs > 0;
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Plugin " + pluginId + " con " + numberOfDocs + " en proceso de digitalización.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getPluginsBySpecification(java.lang.Long)
	 */
	@Override
	public List<PluginDTO> getPluginsBySpecification(Long pluginSpecificationId) throws DAOException {
		PluginSpecification entity;
		List<Plugin> plugins;
		List<PluginDTO> res = new ArrayList<PluginDTO>();

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los plugins para la definición " + pluginSpecificationId + " ...");
			}
			entity = getPluginSpecificationEntity(pluginSpecificationId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando los plugins de la definición " + pluginSpecificationId + " ...");
			}
			plugins = entity.getPlugins();

			res = MappingUtilities.fillPluginListDto(plugins);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Plugins recuperados para la definción " + pluginSpecificationId + ": " + res.size());
			}
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#createPlugin(es.ricoh.webscan.model.dto.PluginDTO,
	 *      java.lang.Long)
	 */
	@Override
	public Long createPlugin(PluginDTO plugin, Long pluginSpecId) throws DAOException {
		Long res = null;
		Plugin entity;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando un nuevo plugin, denominado " + plugin.getName() + " ...");
		}
		try {
			entity = persistPluginEntity(plugin, pluginSpecId);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nuevo plugin " + plugin.getName() + " creado, identificador: " + res + ".");
			}
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#updatePlugin(es.ricoh.webscan.model.dto.PluginDTO)
	 */
	@Override
	public void updatePlugin(PluginDTO plugin) throws DAOException {
		Plugin entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando el plugin " + plugin.getId() + " ...");
		}
		try {
			entity = CommonUtilitiesDAOJdbcImpl.getPluginEntity(plugin.getId(), entityManager);

			entity.setActive(plugin.getActive());
			entity.setName(plugin.getName());
			entity.setDescription(plugin.getDescription());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando el plugin " + plugin.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Plugin " + plugin.getId() + " actualizado en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el plugin " + plugin.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el plugin " + plugin.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el plugin " + plugin.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el plugin " + plugin.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#removePlugin(java.lang.Long)
	 */
	@Override
	public void removePlugin(Long pluginId) throws DAOException {
		Plugin entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Eliminando el plugin " + pluginId + " ...");
		}
		try {
			entity = CommonUtilitiesDAOJdbcImpl.getPluginEntity(pluginId, entityManager);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Eliminando el plugin " + pluginId + " en BBDD ...");
			}
			entityManager.remove(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Plugin " + pluginId + " eliminado en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el plugin " + pluginId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el plugin " + pluginId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el plugin " + pluginId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el plugin " + pluginId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#setPlugins(java.util.Map,
	 *      java.lang.Long)
	 */
	@Override
	public void setPlugins(Map<PluginDTO, Map<ParameterSpecificationDTO, ParameterDTO>> plugins, Long pluginSpecificationId) throws DAOException {
		PluginSpecification entity;
		List<Plugin> entityPlugins;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Estableciendo una nueva colección de plugins para la definición " + pluginSpecificationId + " ...");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición " + pluginSpecificationId + " de BBDD ...");
			}
			entity = getPluginSpecificationEntity(pluginSpecificationId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Estableciendo la nueva colección de plugins en BBDD para la definición " + pluginSpecificationId + " ...");
			}
			entityPlugins = entity.getPlugins();
			entity.setPlugins(mergePlugins(plugins, entityPlugins, entity));

			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva colección de plugins para la definición " + pluginSpecificationId + " establecida en BBDD.");
			}
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getParameterSpecificationsByPluginSpec(java.lang.Long)
	 */
	@Override
	public List<ParameterSpecificationDTO> getParameterSpecificationsByPluginSpec(Long pluginSpecification) throws DAOException {
		List<ParameterSpecification> ddbbRes = new ArrayList<ParameterSpecification>();
		List<ParameterSpecificationDTO> res = new ArrayList<ParameterSpecificationDTO>();

		try {

			PluginSpecification entity = getPluginSpecificationEntity(pluginSpecification);

			ddbbRes = entity.getParameters();

			res = MappingUtilities.fillParameterSpecificationListDto(ddbbRes);

		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getParameterSpecification(java.lang.Long)
	 */
	@Override
	public ParameterSpecificationDTO getParameterSpecification(Long parameterSpecificationId) throws DAOException {
		ParameterSpecification entity;
		ParameterSpecificationDTO res = null;
		String excMsg;

		try {
			entity = getParameterSpecificationEntity(parameterSpecificationId);
			res = MappingUtilities.fillParameterSpecificationDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parámetro " + parameterSpecificationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#setParameterSpecifications(java.util.List,
	 *      java.lang.Long)
	 */
	@Override
	public void setParameterSpecifications(List<ParameterSpecificationDTO> parameterSpecifications, Long pluginSpecificationId) throws DAOException {
		PluginSpecification entity;
		List<ParameterSpecification> entityParamSpecs;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Estableciendo una nueva colección de parámetros para la definición " + pluginSpecificationId + " ...");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición " + pluginSpecificationId + " de BBDD ...");
			}
			entity = getPluginSpecificationEntity(pluginSpecificationId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Estableciendo la nueva colección de parámetros en BBDD para la definición " + pluginSpecificationId + " ...");
			}
			entityParamSpecs = entity.getParameters();
			entity.setParameters(mergeParameterSpecifications(parameterSpecifications, entityParamSpecs, entity));

			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva colección de parámetros para la definición " + pluginSpecificationId + " establecida en BBDD.");
			}
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getParametersByPlugin(java.lang.Long)
	 */
	@Override
	public List<ParameterDTO> getParametersByPlugin(Long pluginId) throws DAOException {
		List<Parameter> ddbbRes = new ArrayList<Parameter>();
		List<ParameterDTO> res = new ArrayList<ParameterDTO>();
		String excMsg;

		try {
			Plugin entity = CommonUtilitiesDAOJdbcImpl.getPluginEntity(pluginId, entityManager);

			ddbbRes = entity.getParameters();

			res = MappingUtilities.fillParameterListDto(ddbbRes);

		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los parámetros del plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getParameter(java.lang.Long)
	 */
	@Override
	public ParameterDTO getParameter(Long parameterId) throws DAOException {
		Parameter entity;
		ParameterDTO res = null;
		String excMsg;

		try {
			entity = getParameterEntity(parameterId);
			res = MappingUtilities.fillParameterDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el parámetro " + parameterId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getParameters(java.util.List)
	 */
	@Override
	public List<ParameterDTO> getParameters(List<Long> parameterIds) throws DAOException {
		List<Parameter> ddbbRes;
		List<ParameterDTO> res = new ArrayList<ParameterDTO>();
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los parámetros " + parameterIds + " ...");
		}
		try {

			TypedQuery<Parameter> query = entityManager.createQuery("SELECT param FROM Parameter param WHERE param.id IN :paramIds", Parameter.class);
			query.setParameter("paramIds", parameterIds);

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillParameterListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " parámetros recuperados de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los parámetros " + parameterIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los parámetros " + parameterIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los parámetros " + parameterIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los parámetros " + parameterIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los parámetros " + parameterIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#setParameters(java.util.Map,
	 *      java.lang.Long)
	 */
	@Override
	public void setParameters(Map<ParameterSpecificationDTO, ParameterDTO> parameters, Long pluginId) throws DAOException {
		Plugin entity;
		List<Parameter> entityParams;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Estableciendo una nueva colección de parámetros para el plugin " + pluginId + " ...");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el plugin " + pluginId + " de BBDD ...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getPluginEntity(pluginId, entityManager);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Estableciendo la nueva colección de parámetros en BBDD para el plugin " + pluginId + " ...");
			}
			entityParams = entity.getParameters();
			entity.setParameters(mergeParameters(parameters, entityParams, entity));
			entityManager.merge(entity);
			entityManager.flush();

		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo una nueva colección de parámetros para el plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#listParameterTypes()
	 */
	@Override
	public List<ParameterTypeDTO> listParameterTypes() throws DAOException {
		List<ParameterType> ddbbRes;
		List<ParameterTypeDTO> res = new ArrayList<ParameterTypeDTO>();
		String excMsg;

		LOG.debug("[WEBSCAN-MODEL] Recuperando los tipos de parámetros...");
		try {
			TypedQuery<ParameterType> query = entityManager.createQuery("SELECT pt FROM ParameterType pt", ParameterType.class);

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar los tipos de parámetros...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillParameterTypeListDto(ddbbRes);

			LOG.debug("[WEBSCAN-MODEL] Tipos de parámetros recueprados: ", res.size());
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando tipos de parámetros de la BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar tipos de parámetros de la BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando tipos de parámetros de la BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando tipos de parámetros de la BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando tipos de parámetros de la BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los tipos de parámetros de la BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando tipos de parámetros de la BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.PluginDAO#getParameterType(java.lang.Long)
	 */
	@Override
	public ParameterTypeDTO getParameterType(Long parameterTypeId) throws DAOException {
		ParameterTypeDTO res;
		ParameterType entity;

		entity = getParameterTypeEntity(parameterTypeId);
		res = MappingUtilities.fillParameterTypeDto(entity);

		return res;
	}

	/**
	 * Recupera de BBDD una entidad definición de plugins a partir de su
	 * identificador.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de definición de plugins.
	 * @return Entidad de BBDD definición de plugins.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private PluginSpecification getPluginSpecificationEntity(Long pluginSpecificationId) throws DAOException {
		PluginSpecification res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de plugin " + pluginSpecificationId + " de BBDD ...");
			}
			res = entityManager.find(PluginSpecification.class, pluginSpecificationId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin " + pluginSpecificationId + " en BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de plugin " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin " + pluginSpecificationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de plugin " + pluginSpecificationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Alamacena una nueva entidad plugin en BBDD.
	 * 
	 * @param plugin
	 *            Nueva entidad plugin.
	 * @param pluginSpecId
	 *            Identificador de la especificación de plugin implementada.
	 * @return La nueva entidad plugin creada.
	 * @throws DAOException
	 *             Si ocurre algún error al ejecutar la sentencia de BBDD, o no
	 *             existe la especificación que define al plugin,
	 */
	private Plugin persistPluginEntity(PluginDTO plugin, Long pluginSpecId) throws DAOException {
		Plugin res;
		String excMsg;

		try {
			PluginDTO aux = new PluginDTO();
			aux.setActive(plugin.getActive());
			aux.setName(plugin.getName());
			aux.setDescription(plugin.getDescription());

			PluginSpecification ps = getPluginSpecificationEntity(pluginSpecId);

			res = MappingUtilities.fillPluginEntity(aux);
			res.setId(null);
			res.setSpecification(ps);

			entityManager.persist(res);
		} catch (DAOException e) {
			throw e;
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el plugin " + plugin.getName() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el plugin " + plugin.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el plugin " + plugin.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el plugin " + plugin.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el plugin " + plugin.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}
		return res;
	}

	/**
	 * Efectúa la fusión en BBDD de la relación actual de plugins y la nueva
	 * relación de plugins de una especificación de plugins.
	 * 
	 * @param newPluginCollection
	 *            Nueva relación de plugins.
	 * @param currentPluginCollection
	 *            Actual relación de plugins almacenada en BBDD.
	 * @param pluginSpec
	 *            Especificación de plugins.
	 * @return Nueva lista de plugins.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o registrar los nuevos
	 *             plugins en BBDD.
	 */
	private List<Plugin> mergePlugins(Map<PluginDTO, Map<ParameterSpecificationDTO, ParameterDTO>> newPluginCollection, List<Plugin> currentPluginCollection, PluginSpecification pluginSpec) throws DAOException {
		List<Plugin> res = new ArrayList<Plugin>();
		Plugin pluginAux;
		PluginDTO pluginDto;
		String excMsg;

		try {
			if (newPluginCollection != null) {
				// Inserciones y actualizaciones
				for (Iterator<PluginDTO> itDto = newPluginCollection.keySet().iterator(); itDto.hasNext();) {
					pluginDto = itDto.next();

					pluginAux = updateOrCreatePlugin(pluginDto, newPluginCollection.get(pluginDto), currentPluginCollection, pluginSpec);
					res.add(pluginAux);
				}
			}

			// Borrados
			removeNotFoundPlugins(currentPluginCollection, newPluginCollection.keySet(), pluginSpec);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error modificando la colección de plugins para una definición: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Añade o actualiza plugins de una entidad especificación de plugin,
	 * incluyendo sus parámetros.
	 * 
	 * @param pluginDto
	 *            Plugin a actualizar o registrar.
	 * @param parameters
	 *            Parámetros del plugin.
	 * @param pluginCollection
	 *            Relación actual de plugins especificados por la definición de
	 *            plugins.
	 * @param pluginSpec
	 *            Especificación de plugins.
	 * @return La entidad plugin actualizada o registrada en BBDD.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o perisitir el plugin en
	 *             BBDD.
	 */
	private Plugin updateOrCreatePlugin(PluginDTO pluginDto, Map<ParameterSpecificationDTO, ParameterDTO> parameters, List<Plugin> pluginCollection, PluginSpecification pluginSpec) throws DAOException {
		Boolean found = Boolean.FALSE;
		Map<ParameterSpecificationDTO, ParameterDTO> params;
		Plugin res;
		String excMsg;

		res = null;

		try {

			for (Iterator<Plugin> it = pluginCollection.iterator(); !found && it.hasNext();) {
				res = it.next();

				if (res.getId().equals(pluginDto.getId())) {
					params = new HashMap<ParameterSpecificationDTO, ParameterDTO>();
					params.putAll(parameters);
					// Se actualiza entidad
					res.setName(pluginDto.getName());
					res.setActive(pluginDto.getActive());
					res.setParameters(mergeParameters(params, res.getParameters(), res));

					entityManager.merge(res);

					found = Boolean.TRUE;
				}
			}

			if (!found) {
				params = new HashMap<ParameterSpecificationDTO, ParameterDTO>();
				params.putAll(parameters);
				// Nuevo
				res = persistPluginEntity(pluginDto, pluginSpec.getId());
				res.setParameters(mergeParameters(params, res.getParameters(), res));
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo la relación de configuraciones de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo la relación de configuraciones de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo la relación de configuraciones de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}
		return res;
	}

	/**
	 * Elimina los plugins no presentes en la nueva relación de plugins de una
	 * entidad especificación de plugins.
	 * 
	 * @param pluginCollection
	 *            Relación actual de plugins de una especificación de plugins.
	 * @param newPluginCollection
	 *            Nueva relación de plugins de una especificación de plugins.
	 * @param pluginSpec
	 *            Definición de plugins.
	 * @throws DAOException
	 *             Si ocurre algún error al eliminar los plugins en BBDD.
	 */
	private void removeNotFoundPlugins(List<Plugin> pluginCollection, Collection<PluginDTO> newPluginCollection, PluginSpecification pluginSpec) throws DAOException {
		Boolean found;
		PluginDTO pluginDto;
		String excMsg;

		try {
			for (Plugin plugin: pluginCollection) {
				found = Boolean.FALSE;

				if (newPluginCollection != null) {
					for (Iterator<PluginDTO> it = newPluginCollection.iterator(); !found && it.hasNext();) {
						pluginDto = it.next();
						found = plugin.getId().equals(pluginDto.getId());
					}
				}
				if (!found) {
					entityManager.remove(plugin);
				}
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando configuraciones de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando configuraciones de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando configuraciones de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}
	}

	/**
	 * Efectúa la fusión en BBDD de la relación actual de parámetros y la nueva
	 * relación de parámetros de una especificación de plugins.
	 * 
	 * @param newSpecifications
	 *            Nueva relación de parámetros.
	 * @param currentSpecifications
	 *            Actual relación de parámetros almacenada en BBDD.
	 * @param pluginSpec
	 *            Especificación de plugins.
	 * @return Nueva lista de parámetros.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o registrar los nuevos
	 *             parámetros en BBDD.
	 */
	private List<ParameterSpecification> mergeParameterSpecifications(List<ParameterSpecificationDTO> newSpecifications, List<ParameterSpecification> currentSpecifications, PluginSpecification pluginSpec) throws DAOException {
		List<ParameterSpecification> res = new ArrayList<ParameterSpecification>();
		ParameterSpecification psAux;

		if (newSpecifications != null) {
			// Inserciones y actualizaciones
			for (ParameterSpecificationDTO psDto: newSpecifications) {
				psAux = updateOrCreateParamSpecs(psDto, currentSpecifications, pluginSpec);
				res.add(psAux);
			}
		}

		// Borrados
		removeNotFoundParamSpecs(currentSpecifications, newSpecifications, pluginSpec);

		return res;
	}

	/**
	 * Añade o actualiza parámetros de una entidad especificación de plugin.
	 * 
	 * @param psDto
	 *            Especificación de parámetro a actualizar o registrar.
	 * @param paramSpecs
	 *            Relación actual de parámetros especificados por la definición
	 *            de plugins.
	 * @param pluginSpec
	 *            Especificación de plugins.
	 * @return La entidad definición de parámetro actualizada o registrada en
	 *         BBDD.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o perisitir la entidad
	 *             definición de parámetro en BBDD.
	 */
	private ParameterSpecification updateOrCreateParamSpecs(ParameterSpecificationDTO psDto, List<ParameterSpecification> paramSpecs, PluginSpecification pluginSpec) throws DAOException {
		Boolean found = Boolean.FALSE;
		ParameterSpecification res;
		String excMsg;

		res = null;

		try {

			for (Iterator<ParameterSpecification> it = paramSpecs.iterator(); !found && it.hasNext();) {
				res = it.next();

				if (res.getId().equals(psDto.getId())) {
					// Se reflejan los cambios en BBDD
					// Se actualiza entidad
					res.setDescription(psDto.getDescription());
					res.setMandatory(psDto.getMandatory());
					res.setName(psDto.getName());

					if (!psDto.getTypeId().equals(res.getType().getId())) {
						res.setType(getParameterTypeEntity(psDto.getTypeId()));
					}

					entityManager.merge(res);
					found = Boolean.TRUE;
				}
			}

			if (!found) {
				// Nuevo
				res = MappingUtilities.fillParameterSpecificationEntity(psDto);
				res.setPluginSpecification(pluginSpec);
				res.setType(getParameterTypeEntity(psDto.getTypeId()));
				res.setId(null);
				entityManager.persist(res);
			}

		} catch (DAOException e) {
			throw e;
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de parámetros de la especificación de plugin " + pluginSpec.getId() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de parámetros de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de parámetros de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de parámetros de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de parámetros de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Elimina los parámetros no presentes en la nueva relación de parámetros de
	 * una entidad especificación de plugins.
	 * 
	 * @param paramSpecs
	 *            Relación actual de parámetros de una especificación de
	 *            plugins.
	 * @param newParamSpecs
	 *            Nueva relación de parámetros de una especificación de plugins.
	 * @param pluginSpec
	 *            Definición de plugin.
	 * @throws DAOException
	 *             Si ocurre algún error al eliminar los parámetros en BBDD.
	 */
	private void removeNotFoundParamSpecs(List<ParameterSpecification> paramSpecs, Collection<ParameterSpecificationDTO> newParamSpecs, PluginSpecification pluginSpec) throws DAOException {
		Boolean found;
		ParameterSpecificationDTO psDto;
		String excMsg;

		try {
			for (ParameterSpecification param: paramSpecs) {
				found = Boolean.FALSE;

				if (newParamSpecs != null) {
					for (Iterator<ParameterSpecificationDTO> it = newParamSpecs.iterator(); !found && it.hasNext();) {
						psDto = it.next();
						found = param.getId().equals(psDto.getId());
					}
				}
				if (!found) {
					entityManager.remove(param);
				}
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando parámetros de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando parámetros de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando parámetros de la especificación de plugin " + pluginSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}
	}

	/**
	 * Recupera de BBDD una entidad parámetro a partir de su identificador.
	 * 
	 * @param parameterId
	 *            Identificador de parámetro.
	 * @return Entidad de BBDD parámetro.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private Parameter getParameterEntity(Long parameterId) throws DAOException {
		Parameter res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el parámetro " + parameterId + " de BBDD ...");
			}
			res = entityManager.find(Parameter.class, parameterId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando el parámetro " + parameterId + " en BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parámetro " + res.getId() + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el parámetro " + parameterId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el parámetro " + parameterId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Efectúa la fusión en BBDD de la relación actual de parámetros y la nueva
	 * relación de parámetros de un plugin.
	 * 
	 * @param newParameters
	 *            Nueva relación de parámetros.
	 * @param currentParameters
	 *            Actual relación de parámetros almacenada en BBDD.
	 * @param plugin
	 *            Plugin.
	 * @return Nueva lista de parámetros.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o registrar los nuevos
	 *             parámetros en BBDD.
	 */
	private List<Parameter> mergeParameters(Map<ParameterSpecificationDTO, ParameterDTO> newParameters, List<Parameter> currentParameters, Plugin plugin) throws DAOException {
		List<Parameter> res = new ArrayList<Parameter>();
		Parameter pAux;
		ParameterSpecificationDTO pSpecDto;

		if (newParameters != null) {
			// Inserciones y actualizaciones
			for (Iterator<ParameterSpecificationDTO> it = newParameters.keySet().iterator(); it.hasNext();) {
				pSpecDto = it.next();
				pAux = updateOrCreateParams(newParameters.get(pSpecDto), pSpecDto, currentParameters, plugin);
				res.add(pAux);
			}
		}

		// Borrados
		removeNotFoundParams(currentParameters, newParameters.values(), plugin);

		return res;
	}

	/**
	 * Añade o actualiza parámetros de una entidad plugin.
	 * 
	 * @param pDto
	 *            Parámetro a actualizar o registrar.
	 * @param paramSpecDto
	 *            Especificación del parámetro a actualizar o registrar.
	 * @param params
	 *            Relación actual de parámetros de un plugin.
	 * @param plugin
	 *            Plugin.
	 * @return La entidad parámetro actualizada o registrada en BBDD.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o perisitir la entidad
	 *             parámetro en BBDD.
	 */
	private Parameter updateOrCreateParams(ParameterDTO pDto, ParameterSpecificationDTO paramSpecDto, List<Parameter> params, Plugin plugin) throws DAOException {
		Boolean found = Boolean.FALSE;
		Parameter res;
		String excMsg;

		res = null;

		try {

			for (Iterator<Parameter> it = params.iterator(); !found && it.hasNext();) {
				res = it.next();

				if (res.getId().equals(pDto.getId())) {
					// Se reflejan los cambios en BBDD
					// Se actualiza entidad
					res.setValue(pDto.getValue());

					entityManager.merge(res);
					found = Boolean.TRUE;
				}
			}

			if (!found) {
				// Nuevo
				res = MappingUtilities.fillParameterEntity(pDto);
				res.setId(null);
				res.setPlugin(plugin);
				res.setSpecification(getParameterSpecificationEntity(paramSpecDto.getId()));
				entityManager.persist(res);
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo los parametros del plugin " + plugin.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo los parametros del plugin " + plugin.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo los parametros del plugin " + plugin.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Elimina los parámetros no presentes en la nueva relación de parámetros de
	 * una entidad plugin.
	 * 
	 * @param params
	 *            Relación actual de parámetros de un plugin.
	 * @param newParams
	 *            Nueva relación de parámetros de un plugin.
	 * @param plugin
	 *            Plugin.
	 * @throws DAOException
	 *             Si ocurre algún error al eliminar los parámetros en BBDD.
	 */
	private void removeNotFoundParams(List<Parameter> params, Collection<ParameterDTO> newParams, Plugin plugin) throws DAOException {
		Boolean found;
		ParameterDTO pDto;
		String excMsg;

		try {

			for (Parameter param: params) {
				found = Boolean.FALSE;

				if (newParams != null) {
					for (Iterator<ParameterDTO> it = newParams.iterator(); !found && it.hasNext();) {
						pDto = it.next();
						found = param.getId().equals(pDto.getId());
					}
				}
				if (!found) {
					entityManager.remove(param);
				}
			}

		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando parametros del plugin " + plugin.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando parametros del plugin " + plugin.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando parametros del plugin " + plugin.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}
	}

	/**
	 * Recupera de BBDD una entidad definición de parámetros a partir de su
	 * identificador.
	 * 
	 * @param parameterSpecificationId
	 *            Identificador de definición de parámetros.
	 * @return Entidad de BBDD definición de parámetros.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private ParameterSpecification getParameterSpecificationEntity(Long parameterSpecificationId) throws DAOException {
		ParameterSpecification res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de parámetro " + parameterSpecificationId + " de BBDD ...");
			}
			res = entityManager.find(ParameterSpecification.class, parameterSpecificationId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parámetro " + parameterSpecificationId + " en BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de parámetro " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parámetro " + parameterSpecificationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parámetro " + parameterSpecificationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad definición de parámetros a partir de su
	 * denominación y el identificador de la especificación de plugin a la cual
	 * pertenece.
	 * 
	 * @param parameterSpecName
	 *            Denominación de definiicón de parámetro.
	 * @param pluginSpecId
	 *            Identificador de definición de plugin.
	 * @return Entidad de BBDD definición de parámetros.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private ParameterSpecification getParameterSpecificationByNameEntity(String parameterSpecName, Long pluginSpecId) throws DAOException {
		ParameterSpecification res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + "...");
			}
			TypedQuery<ParameterSpecification> query = entityManager.createQuery("SELECT ps FROM ParameterSpecification ps JOIN ps.pluginSpecification pluginSpec WHERE ps.name = :parameterSpecName AND pluginSpec.id = :pluginSpecId", ParameterSpecification.class);
			query.setParameter("parameterSpecName", parameterSpecName);
			query.setParameter("pluginSpecId", pluginSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + "...");
			}
			res = query.getSingleResult();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de parametro denominada " + parameterSpecName + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de parametro " + parameterSpecName + " para la definición de plugin " + pluginSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad tipo de parámetros a partir de su
	 * identificador.
	 * 
	 * @param parameterTypeId
	 *            Identificador de tipo de parámetros.
	 * @return Entidad de BBDD tipo de parámetros.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private ParameterType getParameterTypeEntity(Long parameterTypeId) throws DAOException {
		ParameterType res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el tipo de parámetro " + parameterTypeId + " de BBDD ...");
			}
			res = entityManager.find(ParameterType.class, parameterTypeId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando tipo de parámetro " + parameterTypeId + " en BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Tipo de parámetro " + res.getId() + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el tipo de parámetro " + parameterTypeId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el tipo de parámetro " + parameterTypeId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}
}
