/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.impl.AuditDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre las entidades traza y evento de auditoría, pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.dao.AuditDAO;
import es.ricoh.webscan.model.domain.AuditEvent;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperation;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.domain.AuditScannedDoc;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.dto.AuditReportDTO;
import es.ricoh.webscan.model.dto.AuditScannedDocDTO;
import es.ricoh.webscan.model.dto.MappingUtilities;

/**
 * Implementación JPA 2.0 de la capa DAO sobre las entidades traza y evento de
 * auditoría, pertenecientes al modelo de datos base de Webscan.
 * <p>
 * Clase AuditDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.dao.AuditDAO
 * @version 2.0.
 */
public final class AuditDAOJdbcImpl implements AuditDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AuditDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la interacción con en el contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * Constructor sin argumentos.
	 */
	public AuditDAOJdbcImpl() {
		super();
	}

	/**
	 * Establece un nuevo gestor de persistencia JPA.
	 * 
	 * @param aEntityManager
	 *            manager de persistencia.
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		this.entityManager = aEntityManager;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#countAuditOperations()
	 */
	@Override
	public Long countAuditOperations() throws DAOException {
		Long res = 0L;
		String excMsg;
		String jpqlQuery;

		try {
			LOG.debug("[WEBSCAN-MODEL] Recuperando el número total de trazas de auditoría registradas en el sistema ...");

			jpqlQuery = "SELECT COUNT(auditOp.id) FROM AuditOperation auditOp";

			TypedQuery<Long> query = entityManager.createQuery(jpqlQuery, Long.class);

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el número total de trazas de auditoría registradas en el sistema ...");

			res = query.getSingleResult();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res + " trazas registradas en el sisitema.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el número total de trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el número total de trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#countAuditOperations(java.util.List,
	 *      es.ricoh.webscan.model.domain.AuditOperationStatus, java.util.Date,
	 *      java.util.Date, java.util.List, java.lang.String,
	 *      es.ricoh.webscan.model.domain.AuditOpEventResult, java.util.Date,
	 *      java.util.Date)
	 */
	@Override
	public Long countAuditOperations(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate) throws DAOException {
		Long res = 0L;
		String excMsg;

		try {
			LOG.debug("[WEBSCAN-MODEL] Recuperando el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada ...");

			TypedQuery<Long> query = buildCountListAndQueryAuditOperations(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate);

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada ...");

			res = query.getSingleResult();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res + " trazas recuperadas para la consulta solicitada.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número total de trazas de auditoría registradas en el sistema para una búsqueda solicitada: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Construye la consulta JPA responsable del filtrado, ordenación y
	 * paginación de trazas de auditoría registradas en el sistema.
	 * 
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @return consulta JPA que recupera el número total un listado de trazas de
	 *         auditoría registradas en el sistema.
	 * @throws IllegalArgumentException
	 *             Si se produce algún error al construir la consulta.
	 */
	private TypedQuery<Long> buildCountListAndQueryAuditOperations(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate) throws IllegalArgumentException {
		Boolean auditOpFilters = Boolean.FALSE;
		Boolean eventFilters = Boolean.FALSE;
		Map<String, Object> queryParameters = new HashMap<String, Object>();
		Map<String, Map<String, Object>> auxQuery;
		String jpqlQuery;
		String paramKey;
		TypedQuery<Long> res = null;

		jpqlQuery = "SELECT COUNT(distinct auditOp.id) FROM AuditOperation auditOp";
		eventFilters = eventFiltersIncluded(auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate);
		if (eventFilters) {
			jpqlQuery += " JOIN auditOp.auditEvents events";
		}

		auditOpFilters = auditOpFiltersIncluded(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate);

		if (auditOpFilters || eventFilters) {
			jpqlQuery += " WHERE";

			// Filtros sobre trazas de auditoria
			auxQuery = buildAuditOpFilters(jpqlQuery, auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate);

			// Filtros sobre eventos
			if (eventFilters) {
				auxQuery = buildAuditEventFilters(auxQuery, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate);
			}

			jpqlQuery = auxQuery.keySet().iterator().next();
			queryParameters = auxQuery.get(jpqlQuery);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Búsqueda de trazas de auditoría, consulta construida: " + jpqlQuery);
		}
		res = entityManager.createQuery(jpqlQuery, Long.class);

		if (auditOpFilters || eventFilters) {
			for (Iterator<String> it = queryParameters.keySet().iterator(); it.hasNext();) {
				paramKey = it.next();
				res.setParameter(paramKey, queryParameters.get(paramKey));
			}
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#listAuditOperations(java.lang.Long,
	 *      java.lang.Long, java.util.List, java.util.List)
	 */
	@Override
	public List<AuditOperationDTO> listAuditOperations(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> auditOpOrderByColumns, List<EntityOrderByClause> auditEventOrderByColumns) throws DAOException {
		int first, maxResults;
		List<AuditOperation> ddbbRes = new ArrayList<AuditOperation>();
		List<AuditOperationDTO> res;
		Map<String, List<EntityOrderByClause>> orderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {

			LOG.debug("[WEBSCAN-MODEL] Recuperando las trazas de auditoría registradas en el sistema ...");

			jpqlQuery = "SELECT auditOp FROM AuditOperation auditOp";

			if (auditOpOrderByColumns != null && !auditOpOrderByColumns.isEmpty()) {
				orderByColumns.put("auditOp.", auditOpOrderByColumns);
			}

			if (auditEventOrderByColumns != null && !auditEventOrderByColumns.isEmpty()) {
				orderByColumns.put("events.", auditEventOrderByColumns);
				jpqlQuery += " JOIN auditOp.auditEvents events";
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery(jpqlQuery, orderByColumns);

			TypedQuery<AuditOperation> query = entityManager.createQuery(jpqlQuery, AuditOperation.class);

			if (elementsPerPage != null) {
				first = 0;
				maxResults = elementsPerPage.intValue();
				if (pageNumber != null) {
					first = (pageNumber.intValue() - 1) * maxResults;
				}

				query.setFirstResult(first);
				query.setMaxResults(maxResults);
			}

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar trazas de auditoría registradas en el sistema ...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillAuditOperationListDto(ddbbRes);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#listAuditOperations(java.util.List,
	 *      es.ricoh.webscan.model.domain.AuditOperationStatus, java.util.Date,
	 *      java.util.Date, java.util.List, java.lang.String,
	 *      es.ricoh.webscan.model.domain.AuditOpEventResult, java.util.Date,
	 *      java.util.Date, java.lang.Long, java.lang.Long, java.util.List,
	 *      java.util.List)
	 */
	@Override
	public List<AuditReportDTO> listAuditOperations(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> auditOpOrderByColumns, List<EntityOrderByClause> auditEventOrderByColumns) throws DAOException {
		List<AuditOperation> ddbbRes = new ArrayList<AuditOperation>();
		List<AuditReportDTO> res;
		String excMsg;

		try {

			LOG.debug("[WEBSCAN-MODEL] Recuperando las trazas de auditoría registradas en el sistema ...");

			TypedQuery<AuditOperation> query = buildListAndQueryAuditOperations(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate, pageNumber, elementsPerPage, auditOpOrderByColumns, auditEventOrderByColumns);

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar trazas de auditoría registradas en el sistema ...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillAuditReportListDto(ddbbRes);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Construye la consulta JPA responsable del filtrado, ordenación y
	 * paginación de trazas de auditoría registradas en el sistema.
	 * 
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param auditOpOrderByColumns
	 *            clausulas de ordenación sobre trazas de auditoría.
	 * @param auditEventOrderByColumns
	 *            clausulas de ordenación sobre eventos de auditoría.
	 * @return consulta JPA que recupera un listado de trazas de auditoría
	 *         registradas en el sistema.
	 * @throws IllegalArgumentException
	 *             Si se produce algún error al construir la consulta.
	 */
	private TypedQuery<AuditOperation> buildListAndQueryAuditOperations(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> auditOpOrderByColumns, List<EntityOrderByClause> auditEventOrderByColumns) throws IllegalArgumentException {
		Boolean auditOpFilters = Boolean.FALSE;
		Boolean eventFilters = Boolean.FALSE;
		int first, maxResults;
		Map<String, Object> queryParameters = new HashMap<String, Object>();
		Map<String, Map<String, Object>> auxQuery;
		Map<String, List<EntityOrderByClause>> orderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String jpqlQuery;
		String paramKey;
		TypedQuery<AuditOperation> res = null;

		jpqlQuery = "SELECT auditOp FROM AuditOperation auditOp INNER JOIN FETCH auditOp.auditEvents events LEFT JOIN FETCH auditOp.auditScannedDoc scannedDoc";

		eventFilters = eventFiltersIncluded(auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate);
		/*
		if (eventFilters || (auditEventOrderByColumns != null && !auditEventOrderByColumns.isEmpty())) {
			jpqlQuery += " JOIN FETCH auditOp.auditEvents events";
		}
		*/
		auditOpFilters = auditOpFiltersIncluded(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate);
		if (auditOpFilters || eventFilters) {
			jpqlQuery += " WHERE";

			// Filtros sobre trazas de auditoria
			auxQuery = buildAuditOpFilters(jpqlQuery, auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate);

			// Filtros sobre eventos
			if (eventFilters) {
				auxQuery = buildAuditEventFilters(auxQuery, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate);
			}

			jpqlQuery = auxQuery.keySet().iterator().next();
			queryParameters = auxQuery.get(jpqlQuery);
		}
		if (auditOpOrderByColumns != null && !auditOpOrderByColumns.isEmpty()) {
			orderByColumns.put("auditOp.", auditOpOrderByColumns);
		}
		if (auditEventOrderByColumns != null && !auditEventOrderByColumns.isEmpty()) {
			orderByColumns.put("events.", auditEventOrderByColumns);
		}

		jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery(jpqlQuery, orderByColumns);

		LOG.debug("[WEBSCAN-MODEL] Búsqueda de trazas de auditoría, consulta construida: {}", jpqlQuery);

		res = entityManager.createQuery(jpqlQuery, AuditOperation.class);

		if (auditOpFilters || eventFilters) {
			for (Iterator<String> it = queryParameters.keySet().iterator(); it.hasNext();) {
				paramKey = it.next();
				res.setParameter(paramKey, queryParameters.get(paramKey));
			}
		}

		if (elementsPerPage != null) {
			first = 0;
			maxResults = elementsPerPage.intValue();
			if (pageNumber != null) {
				first = (pageNumber.intValue() - 1) * maxResults;
			}

			res.setFirstResult(first);
			res.setMaxResults(maxResults);
		}

		return res;
	}

	/**
	 * Comprueba si han sido informados filtros sobre trazas de auditoria.
	 * 
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @return True, si se han informado filtros. En caso contrario, false.
	 */
	private Boolean auditOpFiltersIncluded(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate) {
		Boolean res;

		res = (auditOperationNames != null && !auditOperationNames.isEmpty()) || auditOperationStatus != null;

		res = res || auditOpStatusFromDate != null || auditOpStatusUntilDate != null;

		return res;
	}

	/**
	 * Comprueba si han sido informados filtros sobre eventos de trazas de
	 * auditoria.
	 * 
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @return True, si se han informado filtros. En caso contrario, false.
	 */
	private Boolean eventFiltersIncluded(List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate) {
		Boolean res;

		res = (auditEventNames != null && !auditEventNames.isEmpty()) || (username != null && !username.isEmpty());

		res = res || auditOpEventResult != null || auditEventStatusFromDate != null || auditEventStatusUntilDate != null;

		return res;
	}

	/**
	 * Completa la consulta añadiendo los filtros sobre las trazas de auditoría.
	 * 
	 * @param jpqlQuery
	 *            consulta construida hasta el momento.
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @return Estructura que contiene la consulta construida y los parámetros
	 *         que incluye, en formato par clave / valor.
	 */
	private Map<String, Map<String, Object>> buildAuditOpFilters(String jpqlQuery, List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate) {
		Boolean addAndClause = Boolean.FALSE;
		Map<String, Object> queryParameters = new HashMap<String, Object>();
		Map<String, Map<String, Object>> res = new HashMap<String, Map<String, Object>>();
		String auxQuery = jpqlQuery.toString();

		if (auditOperationNames != null && !auditOperationNames.isEmpty()) {
			auxQuery += " auditOp.name IN :auditOperationNames";
			queryParameters.put("auditOperationNames", auditOperationNames);
			addAndClause = Boolean.TRUE;
		}

		if (auditOperationStatus != null) {
			if (addAndClause) {
				auxQuery += " AND";
			}
			auxQuery += " auditOp.status = :auditOperationStatus";
			queryParameters.put("auditOperationStatus", auditOperationStatus);
			addAndClause = Boolean.TRUE;
		}

		if (auditOpStatusFromDate != null) {
			if (addAndClause) {
				auxQuery += " AND";
			}
			auxQuery += " auditOp.lastUpdateDate >= :auditOpStatusFromDate";
			queryParameters.put("auditOpStatusFromDate", auditOpStatusFromDate);
			addAndClause = Boolean.TRUE;
		}

		if (auditOpStatusUntilDate != null) {
			if (addAndClause) {
				auxQuery += " AND";
			}
			auxQuery += " auditOp.lastUpdateDate <= :auditOpStatusUntilDate";
			queryParameters.put("auditOpStatusUntilDate", auditOpStatusUntilDate);
			addAndClause = Boolean.TRUE;
		}

		res.put(auxQuery, queryParameters);

		return res;
	}

	/**
	 * Completa la consulta añadiendo los filtros sobre los eventos de
	 * auditoría.
	 * 
	 * @param prevQuery
	 *            Consulta y relación de parámetros construidos al procesar los
	 *            filtros sobre las trazas de auditoría.
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @return True, si se haninformado filtros. En caso contrario, false.
	 */
	private Map<String, Map<String, Object>> buildAuditEventFilters(Map<String, Map<String, Object>> prevQuery, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate) {
		Boolean addAndClause;
		Map<String, Object> queryParameters = new HashMap<String, Object>();
		Map<String, Map<String, Object>> res = new HashMap<String, Map<String, Object>>();
		String auxQuery;

		auxQuery = prevQuery.keySet().iterator().next();
		queryParameters = prevQuery.get(auxQuery);

		addAndClause = queryParameters != null && !queryParameters.isEmpty();

		// Filtros sobre eventos
		if (auditEventNames != null && !auditEventNames.isEmpty()) {
			if (addAndClause) {
				auxQuery += " AND";
			}
			auxQuery += " events.eventName IN :auditEventNames";
			queryParameters.put("auditEventNames", auditEventNames);
			addAndClause = Boolean.TRUE;
		}

		if (username != null && !username.isEmpty()) {
			if (addAndClause) {
				auxQuery += " AND";
			}
			auxQuery += " events.eventUser LIKE CONCAT('%',:username,'%')";
			queryParameters.put("username", username);
			addAndClause = Boolean.TRUE;
		}

		if (auditOpEventResult != null) {
			if (addAndClause) {
				auxQuery += " AND";
			}
			auxQuery += " events.eventResult = :auditOpEventResult";
			queryParameters.put("auditOpEventResult", auditOpEventResult);
			addAndClause = Boolean.TRUE;
		}

		if (auditEventStatusFromDate != null) {
			if (addAndClause) {
				auxQuery += " AND";
			}
			auxQuery += " events.eventDate >= :auditEventStatusFromDate";
			queryParameters.put("auditEventStatusFromDate", auditEventStatusFromDate);
			addAndClause = Boolean.TRUE;
		}

		if (auditEventStatusUntilDate != null) {
			if (addAndClause) {
				auxQuery += " AND";
			}
			auxQuery += " events.eventDate <= :auditEventStatusUntilDate";
			queryParameters.put("auditEventStatusUntilDate", auditEventStatusUntilDate);
		}

		res.put(auxQuery, queryParameters);

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#getAuditOperation(java.lang.Long)
	 */
	@Override
	public AuditOperationDTO getAuditOperation(Long auditOperationId) throws DAOException {
		AuditOperation entity;
		AuditOperationDTO res = null;
		String excMsg;

		try {
			entity = getAuditOperationEntity(auditOperationId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando la traza de auditoría " + auditOperationId + " recuperada de BBDD...");
			}
			res = MappingUtilities.fillAuditOperationDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría " + auditOperationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#getAuditEventsByAdditionalInfo(java.lang.String)
	 */
	@Override
	public List<AuditOperationDTO> getAuditOpsByAdditionalInfoEvents(String additionalInfo) throws DAOException {
		List<AuditOperation> ddbbRes = new ArrayList<AuditOperation>();
		List<AuditOperationDTO> res;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las trazas de auditoría registradas en el sistema cuyos eventos " + "incluyen como información adicional: " + additionalInfo + ".");
			}
			jpqlQuery = "SELECT auditOp FROM AuditOperation auditOp JOIN auditOp.auditEvents events WHERE events.additionalInfo LIKE CONCAT('%',:additionalInfo,'%')";

			TypedQuery<AuditOperation> query = entityManager.createQuery(jpqlQuery, AuditOperation.class);
			query.setParameter("additionalInfo", additionalInfo);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las trazas de auditoría registradas en el sistema cuyos eventos " + "incluyen como información adicional: " + additionalInfo + ".");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillAuditOperationListDto(ddbbRes);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema cuyos eventos " + "incluyen información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las trazas de auditoría registradas en el sistema cuyos eventos " + "incluyen información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema cuyos eventos " + "incluyen información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema cuyos eventos " + "incluyen información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema cuyos eventos " + "incluyen información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las trazas de auditoría registradas en el sistema cuyos eventos " + "incluyen información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de auditoría registradas en el sistema cuyos eventos " + "incluyen información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#getAuditOpByUserAndAdditionalInfoEvents(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public AuditOperationDTO getAuditOpByUserAndAdditionalInfoEvents(String user, String additionalInfo) throws DAOException {
		AuditOperation entity;
		AuditOperationDTO res;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la traza de auditoría registrada en el sistema con evento realizado por el usuario " + user + ", e incluye la siguiente información adicional: " + additionalInfo + ".");
			}
			jpqlQuery = "SELECT auditOp FROM AuditOperation auditOp JOIN auditOp.auditEvents events WHERE events.eventUser = :user AND events.additionalInfo LIKE CONCAT('%',:additionalInfo,'%')";

			TypedQuery<AuditOperation> query = entityManager.createQuery(jpqlQuery, AuditOperation.class);
			query.setParameter("user", user);
			query.setParameter("additionalInfo", additionalInfo);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e incluye la siguiente información adicional: " + additionalInfo + ".");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillAuditOperationDto(entity);

			LOG.debug("[WEBSCAN-MODEL] Traza de auditoría recuperada: {}", res.getId());
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría registrada en el sistema con evento " + "realizado por el usuario " + user + ", e información adicional (" + additionalInfo + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#getLastAuditOperationByOpNameAndUser(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public AuditOperationDTO getLastAuditOperationByOpNameAndUser(AuditOperationName auditOperationName, String user) throws DAOException {
		AuditOperation entity;
		AuditOperationDTO res;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + "...");
			}
			jpqlQuery = "SELECT auditOp FROM AuditOperation auditOp WHERE auditOp.name = :auditOperationName AND auditOp.lastUpdateDate = (SELECT MAX(auxAuditOp.lastUpdateDate) FROM AuditOperation auxAuditOp JOIN auxAuditOp.auditEvents events WHERE auxAuditOp.name = :auditOperationName AND events.eventUser = :user) ";

			TypedQuery<AuditOperation> query = entityManager.createQuery(jpqlQuery, AuditOperation.class);
			query.setParameter("auditOperationName", auditOperationName);
			query.setParameter("user", user);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + "...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillAuditOperationDto(entity);

			LOG.debug("[WEBSCAN-MODEL] Traza de auditoría recuperada: {}", res.getId());
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#getLastAuditOperationByOpNameStatusAndUser(es.ricoh.webscan.model.domain.AuditOperationName,
	 *      es.ricoh.webscan.model.domain.AuditOperationStatus,
	 *      java.lang.String)
	 */
	@Override
	public AuditOperationDTO getLastAuditOperationByOpNameStatusAndUser(AuditOperationName auditOperationName, AuditOperationStatus auditOperationStatus, String user) throws DAOException {
		AuditOperation entity;
		AuditOperationDTO res;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema, cuyo estado es " + auditOperationStatus.getStatus() + ", y con algún evento realizado por el usuario " + user + "...");
			}
			jpqlQuery = "SELECT auditOp FROM AuditOperation auditOp WHERE auditOp.name = :auditOperationName AND auditOp.status = :auditOperationStatus AND auditOp.lastUpdateDate = (SELECT MAX(auxAuditOp.lastUpdateDate) FROM AuditOperation auxAuditOp JOIN auxAuditOp.auditEvents events WHERE auxAuditOp.name = :auditOperationName AND auxAuditOp.status = :auditOperationStatus AND events.eventUser = :user) ";

			TypedQuery<AuditOperation> query = entityManager.createQuery(jpqlQuery, AuditOperation.class);
			query.setParameter("auditOperationName", auditOperationName);
			query.setParameter("auditOperationStatus", auditOperationStatus);
			query.setParameter("user", user);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + "...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillAuditOperationDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de auditoría recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de auditoría " + auditOperationName + " registrada en el sistema con estado " + auditOperationStatus.getStatus() + " y con algún evento realizado por el usuario " + user + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#createAuditOperation(es.ricoh.webscan.model.dto.AuditOperationDTO)
	 */
	@Override
	public Long createAuditOperation(AuditOperationDTO auditOperation) throws DAOException {
		Long res = null;
		AuditOperation entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando una traza de auditoría, denominada " + auditOperation.getName() + " ...");
		}
		entity = MappingUtilities.fillAuditOperationEntity(auditOperation);

		try {
			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva traza de auditoría " + auditOperation.getName() + " creada, identificador: " + res + ".");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de auditoría " + auditOperation.getName() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de auditoría " + auditOperation.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de auditoría " + auditOperation.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de auditoría " + auditOperation.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de auditoría " + auditOperation.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#updateAuditOperation(es.ricoh.webscan.model.dto.AuditOperationDTO)
	 */
	@Override
	public void updateAuditOperation(AuditOperationDTO auditOperation) throws DAOException {
		AuditOperation entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando la traza de auditoría " + auditOperation.getId() + " ...");
		}
		try {
			entity = getAuditOperationEntity(auditOperation.getId());

			if (auditOperation.getAuditScannedDocId() != null) {
				entity.setAuditScannedDoc(getAuditScannedDocEntity(auditOperation.getAuditScannedDocId()));
			}

			entity.setStatus(auditOperation.getStatus());
			entity.setLastUpdateDate(auditOperation.getLastUpdateDate());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando la traza de auditoría " + auditOperation.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de auditoría " + auditOperation.getId() + " actualizada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error la traza de auditoría " + auditOperation.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la traza de auditoría " + auditOperation.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la traza de auditoría " + auditOperation.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la traza de auditoría " + auditOperation.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#getAuditEventsByOperation(java.lang.Long)
	 */
	@Override
	public List<AuditEventDTO> getAuditEventsByOperation(Long auditOperationId) throws DAOException {
		int queryResultSize = 0;
		AuditOperation entity;
		List<AuditEvent> events;
		List<AuditEventDTO> res = new ArrayList<AuditEventDTO>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los eventos de la traza de auditoría " + auditOperationId + " ...");
			}
			entity = getAuditOperationEntity(auditOperationId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando los eventos de la traza de auditoría " + auditOperationId + " ...");
			}
			events = entity.getAuditEvents();

			if (events != null && !events.isEmpty()) {
				queryResultSize = events.size();

				for (AuditEvent ae: events) {
					res.add(MappingUtilities.fillAuditEventDto(ae));
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Eventos recuperados para la traza de auditoría " + auditOperationId + ": " + queryResultSize);
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los eventos de la traza de auditoría " + auditOperationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#getAuditEvent(java.lang.Long)
	 */
	@Override
	public AuditEventDTO getAuditEvent(Long auditEventId) throws DAOException {
		AuditEvent entity;
		AuditEventDTO res = null;
		String excMsg;

		try {
			entity = getAuditEventEntity(auditEventId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando el evento de auditoría " + auditEventId + " recuperado de BBDD...");
			}
			res = MappingUtilities.fillAuditEventDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el evento de auditoría " + auditEventId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#addAuditEvents(java.util.List,
	 *      java.lang.Long)
	 */
	@Override
	public void addAuditEvents(List<AuditEventDTO> auditEvents, Long auditOperationId) throws DAOException {
		AuditOperation entity;
		AuditEvent event;
		int numberOfNewAuditEvents = 0;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Añadiendo nuevos eventos para la traza de auditoría " + auditOperationId + " ...");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la traza de auditoría " + auditOperationId + " de BBDD ...");
			}
			entity = getAuditOperationEntity(auditOperationId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Se añaden los nuevos eventos a la traza de auditoría " + auditOperationId + " ...");
			}
			if (auditEvents != null && !auditEvents.isEmpty()) {
				numberOfNewAuditEvents = auditEvents.size();
				for (AuditEventDTO eventDto: auditEvents) {
					event = MappingUtilities.fillAuditEventEntity(eventDto);
					event.setAuditOperation(entity);
					entityManager.persist(event);
					entity.getAuditEvents().add(event);
				}
			}

			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Añadidos " + numberOfNewAuditEvents + " eventos para la traza de auditoría " + auditOperationId + " establecida en BBDD.");
			}
		} catch (DAOException e) {
			throw e;
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error añadiendo nuevos eventos a la traza de auditoría " + auditOperationId + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error añadiendo nuevos eventos a la traza de auditoría " + auditOperationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error añadiendo nuevos eventos a la traza de auditoría " + auditOperationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error añadiendo nuevos eventos a la traza de auditoría " + auditOperationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error añadiendo nuevos eventos a la traza de auditoría " + auditOperationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.AuditDAO#createAuditScannedDoc(es.ricoh.webscan.model.dto.AuditScannedDocDTO,
	 *      java.lang.Long)
	 */
	@Override
	public Long createAuditScannedDoc(AuditScannedDocDTO auditScannedDoc, Long auditOperationId) throws DAOException {
		Long res = null;
		AuditOperation auditOperation;
		AuditScannedDoc entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando el histórico para un documento con traza de auditoría " + auditOperationId + " ...");
		}
		auditOperation = getAuditOperationEntity(auditOperationId);

		try {
			entity = MappingUtilities.fillAuditScannedDocEntity(auditScannedDoc);
			entity.setAuditOperation(auditOperation);
			entityManager.persist(entity);

			auditOperation.setAuditScannedDoc(entity);
			entityManager.merge(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva histórico de documento " + entity.getId() + " creado.");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el histórico para un documento con traza de auditoría " + auditOperationId + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el histórico para un documento con traza de auditoría " + auditOperationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el histórico para un documento con traza de auditoría " + auditOperationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el histórico para un documento con traza de auditoría " + auditOperationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el histórico para un documento con traza de auditoría " + auditOperationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad traza de auditoría a partir de su
	 * identificador.
	 * 
	 * @param auditOperationId
	 *            Identificador de traza de auditoría.
	 * @return Entidad de BBDD traza de auditoría.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private AuditOperation getAuditOperationEntity(Long auditOperationId) throws DAOException {
		AuditOperation res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la traza de auditoría " + auditOperationId + " de BBDD ...");
			}
			res = entityManager.find(AuditOperation.class, auditOperationId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría " + auditOperationId + " de BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de auditoría " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría " + auditOperationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de auditoría " + auditOperationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad evento de auditoría a partir de su
	 * identificador.
	 * 
	 * @param auditEventId
	 *            Identificador de evento de auditoría.
	 * @return Entidad de BBDD evento de auditoría.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private AuditEvent getAuditEventEntity(Long auditEventId) throws DAOException {
		AuditEvent res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el evento de auditoría " + auditEventId + " de BBDD ...");
			}
			res = entityManager.find(AuditEvent.class, auditEventId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando el evento de auditoría " + auditEventId + " de BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Evento de auditoría " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el evento de auditoría " + auditEventId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el evento de auditoría " + auditEventId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad histórico de documento a partir de su
	 * identificador.
	 * 
	 * @param auditScannedDocId
	 *            Identificador de histórico de documento.
	 * @return Entidad de BBDD traza de auditoría.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private AuditScannedDoc getAuditScannedDocEntity(Long auditScannedDocId) throws DAOException {
		AuditScannedDoc res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el histórico de documento " + auditScannedDocId + " de BBDD ...");
			}
			res = entityManager.find(AuditScannedDoc.class, auditScannedDocId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando el histórico de documento " + auditScannedDocId + " de BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Histórico de documento " + res.getId() + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el histórico de documento " + auditScannedDocId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el histórico de documento " + auditScannedDocId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

}
