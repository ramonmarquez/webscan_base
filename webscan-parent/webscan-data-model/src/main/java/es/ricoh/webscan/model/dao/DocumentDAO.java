/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.DocumentDAO.java.</p>
 * <b>Descripción:</b><p> Interfaz que define la operaciones de la capa DAO sobre las entidades  
 * documento digitalizado, su especificación y relaciones, pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao;

import java.util.List;
import java.util.Map;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.dto.DocBoxScannedDocDTO;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.utilities.MimeType;

/**
 * Interfaz que define la operaciones de la capa DAO sobre las entidades
 * documento digitalizado, su especificación y relaciones, pertenecientes al
 * modelo de datos base de Webscan.
 * <p>
 * Clase DocumentDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface DocumentDAO {

	// Operaciones sobre especificaciones de documentos
	/**
	 * Obtiene el listado de definiciones de documentos registradas en el
	 * sistema, permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de definiciones de documentos.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<DocumentSpecificationDTO> listDocumentSpecifications(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException;

	/**
	 * Recupera una especificación de documento a partir de su identificador.
	 * 
	 * @param docSpecificationId
	 *            Identificador de especificación de documento.
	 * @return La definición de documento solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	DocumentSpecificationDTO getDocumentSpecification(Long docSpecificationId) throws DAOException;

	/**
	 * Recupera una especificación de documento a partir de su denominación.
	 * 
	 * @param docSpecificationName
	 *            Denominación de especificación de documento.
	 * @return La definición de documento solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	DocumentSpecificationDTO getDocumentSpecificationByName(String docSpecificationName) throws DAOException;

	/**
	 * Recupera una especificación de documento a partir de su denominación.
	 * 
	 * @param scanProfileOrgUnitDocSpecId
	 *            Identificador de una especificación de documento establecida
	 *            en un pefil configurado en una unidad organizativa.
	 * @return La definición de documento solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	DocumentSpecificationDTO getDocumentSpecificationByScanProfileOrgUnitDocSpec(Long scanProfileOrgUnitDocSpecId) throws DAOException;

	/**
	 * Recupera las especificaciones de documentos padre de una especificación
	 * de documento determinada.
	 * 
	 * @param docSpecificationId
	 *            Identificador de especificación de documento.
	 * @return Lista de definiciones de documentos padre de la definición de
	 *         documento especificada como parámetro de entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	List<DocumentSpecificationDTO> getParentDocSpecifications(Long docSpecificationId) throws DAOException;

	/**
	 * Recupera las especificaciones de documentos hija de una especificación de
	 * documento determinada.
	 * 
	 * @param docSpecificationId
	 *            Identificador de especificación de documento.
	 * @return Lista de definiciones de documentos hija de la definición de
	 *         documento especificada como parámetro de entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	List<DocumentSpecificationDTO> getChildDocSpecifications(Long docSpecificationId) throws DAOException;

	/**
	 * Crea una nueva definición de documento en el sistema.
	 * 
	 * @param specification
	 *            Nueva definición de documento.
	 * @return La definición de documento creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createDocumentSpecification(DocumentSpecificationDTO specification) throws DAOException;

	/**
	 * Actualiza una definición de documento existente en el sistema. Los datos
	 * que pueden ser actualizados son la denominación, descripción y el estado
	 * de activación de la especificación.
	 * 
	 * @param specification
	 *            Definición de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void updateDocumentSpecification(DocumentSpecificationDTO specification) throws DAOException;

	/**
	 * Elimina una lista de definiciones de documento del sistema.
	 * 
	 * @param specificationIds
	 *            Relación de identificadores de especificación de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe alguna
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void removeDocumentSpecifications(List<Long> specificationIds) throws DAOException;

	/**
	 * Comprueba si una definición de documentos posee documentos en proceso de
	 * digitalización.
	 * 
	 * @param docSpecId
	 *            Identificador de especificación de documentos.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si no existe la especificación de documentos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Boolean hasDocSpecificationDocsInScanProccess(Long docSpecId) throws DAOException;

	/**
	 * Establece una lista de definiciones de documentos padre para una
	 * definición de documento determinada.
	 * 
	 * @param docSpecs
	 *            Nueva lista de definiciones padre.
	 * @param docSpecificationId
	 *            Definición de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void setParentDocSpecifications(List<DocumentSpecificationDTO> docSpecs, Long docSpecificationId) throws DAOException;

	/**
	 * Establece una lista de definiciones de documentos hija para una
	 * definición de documento determinada.
	 * 
	 * @param docSpecs
	 *            Nueva lista de definiciones hija.
	 * @param docSpecificationId
	 *            Definición de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void setChildDocSpecifications(List<DocumentSpecificationDTO> docSpecs, Long docSpecificationId) throws DAOException;

	// Operaciones sobre especificaciones de metadatos
	/**
	 * Obtiene el listado de metadatos de una definición de documento
	 * determinada.
	 * 
	 * @param docSpecId
	 *            Identificador de definición de documento.
	 * @return listado de metadatos de una definición de documentos.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la consulta a BBDD.
	 */
	List<MetadataSpecificationDTO> listMetadataSpecsByDocSpec(Long docSpecId) throws DAOException;

	/**
	 * Obtiene una definición de metadato a partir de su identificador.
	 * 
	 * @param metadataSpecId
	 *            Identificador de definición de metadato.
	 * @return Definición de metadato solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la consulta a BBDD.
	 */
	MetadataSpecificationDTO getMetadataSpecification(Long metadataSpecId) throws DAOException;

	/**
	 * Obtiene una relación de definiciones de metadatos a partir de sus
	 * identificadores.
	 * 
	 * @param metadataSpecIds
	 *            Relación de identificadores de definiciones de metadatos.
	 * @return Lista de definiciones de metadatos solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<MetadataSpecificationDTO> getMetadataSpecifications(List<Long> metadataSpecIds) throws DAOException;

	/**
	 * Establece una nueva lista de metadatos para una definición de documento
	 * determinada.
	 * 
	 * @param metadataSpecs
	 *            Nueva lista de metadatos.
	 * @param docSpecificationId
	 *            Definición de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void setMetadataSpecifications(List<MetadataSpecificationDTO> metadataSpecs, Long docSpecificationId) throws DAOException;

	// Operaciones sobre documentos
	/**
	 * Recupera un listado de documentos digitalizados pertenecientes a unidades
	 * organizativas, permitiendo la paginación y ordenación del resultado.
	 *
	 * @param orgUnitIds
	 *            Relación de identificadores de unidad organizativa.
	 * @param functionalOrgs
	 *            unidades orgánicas por las que filtrar los los documentos a
	 *            recuperar.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de documentos digitalizados.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ScannedDocumentDTO> getDocumentsByOrgUnits(List<Long> orgUnitIds, String functionalOrgs, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException;

	/**
	 * Recupera un listado de documentos digitalizados, y su última trazas de
	 * ejcución, pertenecientes a unidades organizativas.
	 *
	 * @param orgUnitIds
	 *            Relación de identificadores de unidad organizativa.
	 * @param functionalOrgs
	 *            unidades orgánicas por las que filtrar los los documentos a
	 *            recuperar.
	 * @return listado de documentos digitalizados.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<DocBoxScannedDocDTO> getDocumentsAndLogsByOrgUnits(List<Long> orgUnitIds, String functionalOrgs) throws DAOException;
	// Map<ScannedDocumentDTO, List<ScannedDocLogDTO>>
	// getDocumentsAndLogsByOrgUnits(List<Long> orgUnitIds, String
	// functionalOrgs) throws DAOException;

	/**
	 * Recupera un listado de documentos digitalizados para un proceso de digitalización,
	 * pertenecientes a unidades organizativas.
	 *
	 * @param batchId
	 *            Proceso de digitalización.
	 * @param functionalOrgs
	 *            unidades orgánicas por las que filtrar los los documentos a
	 *            recuperar.
	 * @return listado de documentos digitalizados.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<DocBoxScannedDocDTO> getDocsByBatchId(Long batchId) throws DAOException;


	/**
	 * Recupera un listado de documentos digitalizados pertenecientes a un lote
	 * de documentos determinado, permitiendo la paginación y ordenación del
	 * resultado.
	 *
	 * @param batchId
	 *            Identificador de lote de documentos.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de documentos digitalizados.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ScannedDocumentDTO> getDocumentsByBatchId(String batchId, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException;

	/**
	 * Recupera un listado de documentos definidos mediante una especificación
	 * de documentos, cuyo proceso de digitalización ha sido iniciado, pero no
	 * ha finalizado.
	 *
	 * @param docSpecId
	 *            Identificador de especificación de documentos.
	 * @return listado de documentos.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ScannedDocumentDTO> getDocumentsNotScannedByDocSpecId(Long docSpecId) throws DAOException;

	/**
	 * Recupera la relación de documentos que están siendo procesados, mediante
	 * un proceso de digitalización dado, y cuyo procesamiento fue iniciado por
	 * un usuario determinado.
	 * 
	 * @param username
	 *            Nombre de usuario.
	 * @param scanProfileSpec
	 *            Nombre de proceso de digitalización.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return la relación de documentos que están siendo procesados, mediante
	 *         un proceso de digitalización dado, y cuyo procesamiento fue
	 *         iniciado por un usuario determinado.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	List<ScannedDocumentDTO> getActiveDocumentsByUsernameAndScanProfileSpec(String username, String scanProfileSpec, List<EntityOrderByClause> orderByColumns) throws DAOException;

	/**
	 * Recupera la relación de documentos que están siendo procesados para una
	 * carátula o reserva dada. Los documentos no incluyen su contenido y firma.
	 * 
	 * @param batchReqId
	 *            Identificador de reserva de trabajo de digitalización o
	 *            carátula asociado a la digitalización de los documentos.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return la relación de documentos que están siendo procesados para una
	 *         carátula o reserva dada.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	List<ScannedDocumentDTO> getActiveDocumentsByBatchReqId(String batchReqId, List<EntityOrderByClause> orderByColumns) throws DAOException;

	/**
	 * Recupera un documento digitalizado a partir de su identificador.
	 * 
	 * @param docId
	 *            Identificador de documento digitalizado.
	 * @return El documento digitalizado solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la consulta a BBDD.
	 */
	ScannedDocumentDTO getDocument(Long docId) throws DAOException;

	/**
	 * Recupera un listado de lotes de documentos digitalizados pertenecientes a
	 * una unidad organizativa determinada.
	 *
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return listado de identificadores de lotes de documentos digitalizados.
	 *         Lista vacía si no existe ninguno.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<String> getBatchIdsByOrgUnit(Long orgUnitId) throws DAOException;

	/**
	 * Obtiene las rutas donde son alojados uno o más de los contenidos de un
	 * documento (contenido, firma u ocr).
	 * 
	 * @param docId
	 *            identificador de documento digitalizado.
	 * @return rutas de los archivos que almacenan los contenidos de un
	 *         documento, identificados por su tipo MIME.
	 * @throws DAOException
	 *             Si no existe el documento.
	 */
	Map<MimeType, String> getDocContentStorePaths(Long docId) throws DAOException;

	/**
	 * Crea un nuevo documento digitalizado en el sistema, sin metadatos.
	 * 
	 * @param doc
	 *            Nuevo documento digitalizado.
	 * @param scanProfOrgUnitDocSpecId
	 *            identificador de la especificación de documentos asociada al
	 *            perfil de digitalización configurado en un unidad
	 *            organizativa.
	 * @return El documento creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createDocument(ScannedDocumentDTO doc, Long scanProfOrgUnitDocSpecId, Long batchId) throws DAOException;

	/**
	 * Actualiza un documento registrado en el sistema. Es posible actualizar la
	 * siguiente información de un documento digitalizado: hash, contenido,
	 * firma electrónica y texto OCR del documento, fecha de finalización del
	 * proceso de digitalización, tipo de firma, estado y fecha de validación.
	 * 
	 * @param doc
	 *            Documento digitalizado.
	 * @throws DAOException
	 *             Si no existe algún documento, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	void updateDocument(ScannedDocumentDTO doc) throws DAOException;

	/**
	 * Actualiza un listado de documentos registrados en el sistema. Es posible
	 * actualizar la siguiente información de un documento digitalizado: hash,
	 * contenido, firma electrónica y texto OCR del documento, fecha de
	 * finalización del proceso de digitalización, tipo de firma, estado y fecha
	 * de validación.
	 * 
	 * @param docs
	 *            Relación de documentos digitalizados.
	 * @throws DAOException
	 *             Si no existe algún documento, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	void updateDocuments(List<ScannedDocumentDTO> docs) throws DAOException;

	/**
	 * Actualiza las rutas donde son alojados uno o más de los contenidos de un
	 * documento (contenido, firma u ocr).
	 * 
	 * @param docId
	 *            identificador de documento digitalizado.
	 * @param docContentStorePaths
	 *            ruta de los archivos que almacenan los contenidos de un
	 *            documento, identificados por su tipo MIME.
	 * @throws DAOException
	 *             Si no existe el documento, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	void updateDocContentStorePaths(Long docId, Map<MimeType, String> docContentStorePaths) throws DAOException;

	/**
	 * Elimina un documento digitalizado del sistema.
	 * 
	 * @param docId
	 *            Relación de identificadores de documentos digitalizados.
	 * @throws DAOException
	 *             Si el documento, o se produce algún error en la ejecución de
	 *             la sentencia de BBDD.
	 */
	void removeDocument(Long docId) throws DAOException;

	/**
	 * Elimina un listado de documentos registrados en el sistema.
	 * 
	 * @param docIds
	 *            Relación de identificadores de documentos digitalizados.
	 * @throws DAOException
	 *             Si no existe algún documento, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	void removeDocuments(List<Long> docIds) throws DAOException;

	// Operaciones sobre metadatos
	/**
	 * Obtiene el listado de metadatos de un documento determinado.
	 * 
	 * @param documentId
	 *            Identificador de documento digitalizado.
	 * @return listado de metadatos de un documento digitalizado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la consulta a BBDD.
	 */
	List<MetadataDTO> listMetadataCollectionByDocument(Long documentId) throws DAOException;

	/**
	 * Obtiene el listado de metadatos establecidos para un documento
	 * determinado, y sus definiciones.
	 * 
	 * @param documentId
	 *            Identificador de documento digitalizado.
	 * @return listado de metadatos con valor de un documento digitalizado y sus
	 *         definiciones.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la consulta a BBDD.
	 */
	Map<MetadataSpecificationDTO, MetadataDTO> listMetadataCollectionAndSpecByDocument(Long documentId) throws DAOException;

	/**
	 * Obtiene el listado completo de metadatos de un documento determinado.
	 * Esto implica que retorna todas las definiciones de metadatos asociadas a
	 * su definición (incluyendo definiciones de documentos de las que hereda),
	 * y no solo las que tienen algún metadato establecido para el documento.
	 * 
	 * @param documentId
	 *            Identificador de documento digitalizado.
	 * @return listado completo de metadatos de un documento digitalizado y sus
	 *         definiciones.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la consulta a BBDD.
	 */
	Map<MetadataSpecificationDTO, MetadataDTO> listCompleteMetadataCollectionAndSpecByDocument(Long documentId) throws DAOException;

	/**
	 * Obtiene un metadato a partir de su identificador.
	 * 
	 * @param metadataId
	 *            Identificador de metadato.
	 * @return Metadato solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             metadato, o se produce algún error en la consulta a BBDD.
	 */
	MetadataDTO getMetadata(Long metadataId) throws DAOException;

	/**
	 * Obtiene un listado de metadatos a partir de sus identificadores.
	 * metadataIds
	 * 
	 * @param metadataIds
	 *            Lista de identificadores de metadato.
	 * @return Relación de metadatos solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<MetadataDTO> getMetadataCollection(List<Long> metadataIds) throws DAOException;

	/**
	 * Establece una nueva lista de metadatos para un documento determinada.
	 * 
	 * @param metadataCollection
	 *            Nueva lista de metadatos, identificados por su especificación.
	 * @param scannedDocId
	 *            Documento digitalizado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void setMetadataCollection(Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection, Long scannedDocId) throws DAOException;

	/**
	 * Elimina los metadatos de documentos en proceso de digitalización,
	 * definidos por una especificación dada.
	 * 
	 * @param metadataSpecCollection
	 *            Lista de definiciones de metadatos.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	void removeMetadataCollectionByMetadataSpecList(List<MetadataSpecificationDTO> metadataSpecCollection) throws DAOException;

	// Operaciones sobre el histórico de fases del proceso de digitalización
	// completadas para documentos
	/**
	 * Obtiene una trazas o fase de ejecución asociada al proceso de
	 * digitalización de un documento a partir de su identificador.
	 * 
	 * @param scannedDocLogId
	 *            Identificador de traza de ejecución de documento digitalizado.
	 * @return Traza de ejecución solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de documento digitalizado, o se produce algún error en
	 *             la consulta a BBDD.
	 */
	ScannedDocLogDTO getScannedDocLog(Long scannedDocLogId) throws DAOException;

	/**
	 * Obtiene un listado de trazas o fases de ejecución asociada al proceso de
	 * digitalización de un documento a partir del identificador del documento.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return Relación de trazas de ejecución asociada al proceso de
	 *         digitalización solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	List<ScannedDocLogDTO> getScannedDocLogsByDocId(Long scannedDocId) throws DAOException;

	/**
	 * Obtiene la última de traza de ejecución asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return Ultima traza de ejecución asociada al proceso de digitalización
	 *         del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	ScannedDocLogDTO getLastScannedDocLogsByDocId(Long scannedDocId) throws DAOException;

	/**
	 * Obtiene la primera traza de ejecución errónea asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return Primera traza de ejecución errónea asociada al proceso de
	 *         digitalización del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	ScannedDocLogDTO getFirstFailedScannedDocLogsByDocId(Long scannedDocId) throws DAOException;

	/**
	 * Obtiene las trazas de ejecución, cuyas fases son reanudables, y han sido
	 * ejeuctadas en el proceso de digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @return las trazas de ejecución, cuyas fases son reanudables, y han sido
	 *         ejeuctadas en el proceso de digitalización de un documento
	 *         digitalizado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	List<ScannedDocLogDTO> getRenewableAndPerformedScannedDocLogsByDocId(Long scannedDocId) throws DAOException;

	/**
	 * Obtiene la traza de ejecución de una fase asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @param stageName
	 *            Nombre de la fase.
	 * @return Traza de ejecución de una fase asociada al proceso de
	 *         digitalización del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	ScannedDocLogDTO getScannedDocLogByStageNameAndDocId(Long scannedDocId, String stageName) throws DAOException;

	/**
	 * Obtiene la traza de ejecución asociada a una fase del proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @param scanProcessStageId
	 *            Identificador de fase del proceso de digitalización.
	 * @return Ultima traza de ejecución asociada al proceso de digitalización
	 *         del documento solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza, o se produce algún error en la consulta a BBDD.
	 */
	ScannedDocLogDTO getScannedDocLogsByDocIdAndStageId(Long scannedDocId, Long scanProcessStageId) throws DAOException;

	/**
	 * Crea una nueva traza de ejecución asociada al proceso de digitalización
	 * de un documento digitalizado.
	 * 
	 * @param scannedDocLogDto
	 *            Nueva traza de ejecución asociada al proceso de digitalización
	 *            de un documento digitalizado.
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @param scanProcessStageId
	 *            Identificador de fase del proceso de digitalización.
	 * @return El identificador dela traza de ejecución creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createScannedDocLog(ScannedDocLogDTO scannedDocLogDto, Long scannedDocId, Long scanProcessStageId) throws DAOException;

	/**
	 * Actualiza una traza de ejecución asociada al proceso de digitalización de
	 * un documento digitalizado y registrado en el sistema. Es posible
	 * actualizar la siguiente información de una traza de ejecución de un
	 * documento digitalizado: fecha de finalización.
	 * 
	 * @param scannedDocLogDTO
	 *            Traza de ejecución asociada al proceso de digitalización de un
	 *            documento digitalizado a actualizar.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de ejecución del documento digitalizado, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	void updateScannedDocLog(ScannedDocLogDTO scannedDocLogDTO) throws DAOException;

	/**
	 * Elimina las trazas de ejecución asociadas al proceso de digitalización de
	 * un documento digitalizado y registrado en el sistema.
	 * 
	 * @param docId
	 *            Identificador de documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	void removeScannedDocLogs(Long docId) throws DAOException;
	
	/**
	 * Devuelve el número de documentos que pertenecen a un proceso de digitalización.
	 * 
	 * @param batchId
	 *            Identificador de proceso de digitalización.
	 * @return número de documentos de un proceso de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long getNumDocsByBatchId(Long batchId) throws DAOException;
	
	/**
	 * Recupera la relación de documentos que están siendo procesados para un
	 * proceso de digitalización. Los documentos no incluyen su contenido y firma.
	 * 
	 * @param batchId
	 *            Identificador de proceso de digitalización de los documentos.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return la relación de documentos que están siendo procesados para un
	 *         proceso de digitalización.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	List<ScannedDocumentDTO> getActiveDocumentsByBatchId(Long batchId, List<EntityOrderByClause> orderByColumns) throws DAOException;

}
