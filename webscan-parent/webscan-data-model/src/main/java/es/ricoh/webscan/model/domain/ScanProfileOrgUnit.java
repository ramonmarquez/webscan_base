/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScanProfileOrgUnit.java.</p>
 * <b>Descripción:</b><p> Entidad JPA configuración de perfil de digitalización en unidad organizativa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA configuración de perfil de digitalización en unidad organizativa.
 * <p>
 * Clase ScanProfileOrgUnit.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "SCAN_PROFILE_ORG", uniqueConstraints = { @UniqueConstraint(columnNames = { "SCAN_PROFILE_ID", "ORG_UNIT_ID" }) })
public class ScanProfileOrgUnit implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "SCAN_PROFILE_ORG_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SCAN_PROFILE_ORG_ID_SEQ", sequenceName = "SCAN_PROFILE_ORG_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Entidad perfil de digitalización.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProfile.class)
	@JoinColumn(name = "SCAN_PROFILE_ID")
	private ScanProfile scanProfile;

	/**
	 * Entidad unidad organizativa.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.OrganizationUnit.class)
	@JoinColumn(name = "ORG_UNIT_ID")
	private OrganizationUnit orgUnit;

	/**
	 * Relación de plugins configurados.
	 */
	@OneToMany(mappedBy = "orgScanProfile", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnitPlugin.class)
	private List<ScanProfileOrgUnitPlugin> orgScanProfilePlugins = new ArrayList<ScanProfileOrgUnitPlugin>();

	/**
	 * Relación de definiciones de documentos configuradas.
	 */
	@OneToMany(mappedBy = "orgScanProfile", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnitDocSpec.class)
	private List<ScanProfileOrgUnitDocSpec> orgScanProfileDocSpecs = new ArrayList<ScanProfileOrgUnitDocSpec>();

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfileOrgUnit() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el perfil de digitalización.
	 * 
	 * @return el perfil de digitalización.
	 */
	public ScanProfile getScanProfile() {
		return scanProfile;
	}

	/**
	 * Establece un nuevo perfil de digitalización.
	 * 
	 * @param aScanProfile
	 *            Nuevo perfil de digitalización.
	 */
	public void setScanProfile(ScanProfile aScanProfile) {
		this.scanProfile = aScanProfile;
	}

	/**
	 * Obtiene la unidad organizativa.
	 * 
	 * @return la unidad organizativa.
	 */
	public OrganizationUnit getOrgUnit() {
		return orgUnit;
	}

	/**
	 * Establece una nueva unidad organizativa.
	 * 
	 * @param anOrgUnit
	 *            Nueva unidad organizativa.
	 */
	public void setOrgUnit(OrganizationUnit anOrgUnit) {
		this.orgUnit = anOrgUnit;
	}

	/**
	 * Obtiene la relación de plugins configurados.
	 * 
	 * @return la relación de plugins configurados.
	 */
	public List<ScanProfileOrgUnitPlugin> getOrgScanProfilePlugins() {
		return orgScanProfilePlugins;
	}

	/**
	 * Establece una nueva relación de plugins configurados.
	 * 
	 * @param anyOrgScanProfilePlugins
	 *            Nueva relación de plugins configurados.
	 */
	public void setOrgScanProfilePlugins(List<ScanProfileOrgUnitPlugin> anyOrgScanProfilePlugins) {
		this.orgScanProfilePlugins.clear();
		if (anyOrgScanProfilePlugins != null && !anyOrgScanProfilePlugins.isEmpty()) {
			this.orgScanProfilePlugins.addAll(anyOrgScanProfilePlugins);
		}
	}

	/**
	 * Obtiene la relación de definiciones de documentos configuradas.
	 * 
	 * @return la relación de definiciones de documentos configuradas.
	 */
	public List<ScanProfileOrgUnitDocSpec> getOrgScanProfileDocSpecs() {
		return orgScanProfileDocSpecs;
	}

	/**
	 * Establece una nueva relación de definiciones de documentos configuradas.
	 * 
	 * @param anyOrgScanProfileDocSpecs
	 *            Nueva relación de definiciones de documentos configuradas.
	 */
	public void setOrgScanProfileDocSpecs(List<ScanProfileOrgUnitDocSpec> anyOrgScanProfileDocSpecs) {
		this.orgScanProfileDocSpecs.clear();

		if (anyOrgScanProfileDocSpecs != null && !anyOrgScanProfileDocSpecs.isEmpty()) {
			this.orgScanProfileDocSpecs.addAll(anyOrgScanProfileDocSpecs);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfileOrgUnit other = (ScanProfileOrgUnit) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
