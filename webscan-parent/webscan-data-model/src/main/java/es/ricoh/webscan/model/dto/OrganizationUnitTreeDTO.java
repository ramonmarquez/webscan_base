/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.OrganizationUnitTreeDTO.java.</p>
 * <b>Descripción:</b><p> DTO que representa un árbol o jerarquía de unidades organizativas.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO que representa un árbol o jerarquía de unidades organizativas.
 * <p>
 * Clase OrganizationUnitTreeDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class OrganizationUnitTreeDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Unidad organizativa padre o raíz del árbol o jerarquía de unidades
	 * organizativas.
	 */
	private OrganizationUnitDTO organizationUnit;

	/**
	 * Relación de unidades organizativas hija del árbol o jerarquía de unidades
	 * organizativas.
	 */
	private List<OrganizationUnitTreeDTO> childOrganizationUnits = new ArrayList<OrganizationUnitTreeDTO>();

	/**
	 * Constructor sin argumentos.
	 */
	public OrganizationUnitTreeDTO() {
		super();
	}

	/**
	 * Obtiene la unidad organizativa padre o raíz del árbol o jerarquía de
	 * unidades organizativas.
	 * 
	 * @return la unidad organizativa padre o raíz del árbol o jerarquía de
	 *         unidades organizativas.
	 */
	public OrganizationUnitDTO getOrganizationUnit() {
		return organizationUnit;
	}

	/**
	 * Establece un nueva unidad organizativa padre o raíz del árbol o jerarquía
	 * de unidades organizativas.
	 * 
	 * @param anOrganizationUnit
	 *            nueva unidad organizativa padre o raíz del árbol o jerarquía
	 *            de unidades organizativas.
	 */
	void setOrganizationUnit(OrganizationUnitDTO anOrganizationUnit) {
		this.organizationUnit = anOrganizationUnit;
	}

	/**
	 * Obtiene la relación de unidades organizativas hija del árbol o jerarquía
	 * de unidades organizativas.
	 * 
	 * @return la relación de unidades organizativas hija del árbol o jerarquía
	 *         de unidades organizativas.
	 */
	public List<OrganizationUnitTreeDTO> getChildOrganizationUnits() {
		return childOrganizationUnits;
	}

	/**
	 * Establece una nueva relación de unidades organizativas hija del árbol o
	 * jerarquía de unidades organizativas.
	 * 
	 * @param anyChildOrganizationUnits
	 *            nueva relación de unidades organizativas hija del árbol o
	 *            jerarquía de unidades organizativas.
	 */
	void setChildOrganizationUnits(List<OrganizationUnitTreeDTO> anyChildOrganizationUnits) {
		this.childOrganizationUnits.clear();
		if (anyChildOrganizationUnits != null && !anyChildOrganizationUnits.isEmpty()) {
			this.childOrganizationUnits.addAll(anyChildOrganizationUnits);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((organizationUnit == null) ? 0 : organizationUnit.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationUnitTreeDTO other = (OrganizationUnitTreeDTO) obj;
		if (organizationUnit == null) {
			if (other.organizationUnit != null)
				return false;
		} else if (!organizationUnit.equals(other.organizationUnit))
			return false;
		return true;
	}

}
