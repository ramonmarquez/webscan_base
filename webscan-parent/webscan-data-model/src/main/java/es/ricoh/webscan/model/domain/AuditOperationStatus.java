/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.AuditOperationStatus.java.</p>
 * <b>Descripción:</b><p> Clase que define el conjunto de estados de las trazas de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

/**
 * Clase que define el conjunto de estados de las trazas de auditoría.
 * <p>
 * Clase AuditOperationStatus.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum AuditOperationStatus {
	/**
	 * Estado INICIADA.
	 */
	OPEN("INICIADA"),
	/**
	 * Estado FINALIZADA CORRECTAMENTE.
	 */
	CLOSED_OK("FINALIZADA CORRECTAMENTE"),
	/**
	 * Estado FINALIZADA CON ERROR.
	 */
	CLOSED_ERROR("FINALIZADA CON ERROR");

	/**
	 * Estado de trazas de auditoría.
	 */
	private String status;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aStatus
	 *            Estado de trazas de auditoría.
	 */
	AuditOperationStatus(String aStatus) {
		this.status = aStatus;
	}

	/**
	 * Obtiene el atributo status.
	 * 
	 * @return el atributo status.
	 */
	public String getStatus() {
		return status;
	}

}
