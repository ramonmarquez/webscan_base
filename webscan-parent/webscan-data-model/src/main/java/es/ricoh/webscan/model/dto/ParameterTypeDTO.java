/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ParameterTypeDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA tipo de parámetro.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA tipo de parámetro.
 * <p>
 * Clase ParameterTypeDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class ParameterTypeDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación de la entidad.
	 */
	private String name;

	/**
	 * Expresión regular para obtener el valor del parámetro a partir de una
	 * cadena de caracteres.
	 */
	private String stringPattern;

	/**
	 * Constructor sin argumentos.
	 */
	public ParameterTypeDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación de la entidad.
	 * 
	 * @return la denominación de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene la expresión regular para generar el valor del parámetro a partir
	 * de una cadena de caracteres.
	 * 
	 * @return la expresión regular para generar el valor del parámetro a partir
	 *         de una cadena de caracteres.
	 */
	public String getStringPattern() {
		return stringPattern;
	}

	/**
	 * Establece un nueva expresión regular para generar el valor del parámetro
	 * a partir de una cadena de caracteres.
	 * 
	 * @param aStringPattern
	 *            nueva expresión regular para generar el valor del parámetro a
	 *            partir de una cadena de caracteres.
	 */
	void setStringPattern(String aStringPattern) {
		this.stringPattern = aStringPattern;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParameterTypeDTO other = (ParameterTypeDTO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
