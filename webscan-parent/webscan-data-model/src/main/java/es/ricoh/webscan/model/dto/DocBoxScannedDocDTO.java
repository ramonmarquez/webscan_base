/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2018 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.model.dto.DocBoxScannedDocDTO.java.</p>
* <b>Descripción:</b><p> DTO que representa la información de un documento digitalizado que será
 * presentada en la bandeja de documentos.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.model.dto;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.model.domain.OperationStatus;

/**
 * DTO que representa la información de un documento digitalizado que será
 * presentada en la bandeja de documentos.
 * <p>
 * Clase DocBoxScannedDocDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class DocBoxScannedDocDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador del documento digitalizado.
	 */
	private Long docId;

	/**
	 * Denominación del documento.
	 */
	private String docName;

	/**
	 * Identificador de la última traza de ejecución del proceso de
	 * digitalización del documento digitalizado.
	 */
	private Long lastDocExecLogId;

	/**
	 * Denominación de la fase de la última traza de ejecución del proceso de
	 * digitalización del documento digitalizado.
	 */
	private String lastDocExecLogStageName;

	/**
	 * Nombre del Identificador del lote de documentos al que pertenece el documento
	 * digitalizado.
	 */
	private String docBatchNameId;

	/**
	 * Identificador del lote de documentos al que pertenece el documento
	 * digitalizado.
	 */
	private Long docBatchId;

	/**
	 * Número de orden del documento en el lote, comenzando por 1.
	 */
	private Integer docBatchOrderNum;

	/**
	 * Instante en el que se inicio el proceso de digitalización del documento.
	 */
	private Date docStartDate;

	/**
	 * Lista de identificadores de unidades orgánicas a las que pertenece el
	 * usuario que inicia el proceso de digitalización del documento (separados
	 * por coma).
	 */
	private String docFunctionalOrgs;

	/**
	 * Nombre de usuario del usuario que inicia el proceso de digitalización del
	 * documento.
	 */
	private String docUsername;

	/**
	 * Estado de ejecución de la fase del proceso de digitalización de un
	 * documento digitalizado.
	 */
	private OperationStatus lastDocExecLogStatus;

	/**
	 * Constructor sin argumentos.
	 */
	public DocBoxScannedDocDTO() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aDocId
	 *            Identificador del documento digitalizado.
	 * @param aDocName
	 *            Denominación del documento.
	 * @param aLastDocExecLogId
	 *            Identificador de la última traza de ejecución del proceso de
	 *            digitalización del documento digitalizado.
	 * @param aLastDocExecLogStageName
	 *            Denominación de la fase de la última traza de ejecución del
	 *            proceso de digitalización del documento digitalizado.
	 * @param aDocBatchNameId
	 *            Nombre del Identificador del lote de documentos al que pertenece el
	 *            documento digitalizado.
	 * @param aDocBatchId
	 *            Identificador del lote de documentos al que pertenece el
	 *            documento digitalizado.
	 * @param aDocBatchOrderNum
	 *            Número de orden del documento en el lote, comenzando por 1.
	 * @param aDocStartDate
	 *            Instante en el que se inicio el proceso de digitalización del
	 *            documento.
	 * @param anyDocFunctionalOrgs
	 *            Lista de identificadores de unidades orgánicas a las que
	 *            pertenece el usuario que inicia el proceso de digitalización
	 *            del documento (separados por coma).
	 * @param aDocUsername
	 *            Nombre de usuario del usuario que inicia el proceso de
	 *            digitalización del documento.
	 * @param aLastDocExecLogStatus
	 *            Estado de ejecución de la fase del proceso de digitalización
	 *            de un documento digitalizado.
	 */
	public DocBoxScannedDocDTO(Long aDocId, String aDocName, Long aLastDocExecLogId, String aLastDocExecLogStageName, String aDocBatchNameId, Long aDocBatchId, Integer aDocBatchOrderNum, Date aDocStartDate, String anyDocFunctionalOrgs, String aDocUsername, OperationStatus aLastDocExecLogStatus) {
		super();
		this.docId = aDocId;
		this.docName = aDocName;
		this.lastDocExecLogId = aLastDocExecLogId;
		this.lastDocExecLogStageName = aLastDocExecLogStageName;
		this.docBatchNameId = aDocBatchNameId;
		this.docBatchId = aDocBatchId;
		this.docBatchOrderNum = aDocBatchOrderNum;
		this.docStartDate = aDocStartDate;
		this.docFunctionalOrgs = anyDocFunctionalOrgs;
		this.docUsername = aDocUsername;
		this.lastDocExecLogStatus = aLastDocExecLogStatus;
	}

	/**
	 * Obtiene el identificador del documento digitalizado.
	 * 
	 * @return el identificador del documento digitalizado.
	 */
	public Long getDocId() {
		return docId;
	}

	/**
	 * Establece el identificador del documento digitalizado.
	 * 
	 * @param aDocId
	 *            Identificador del documento digitalizado.
	 */
	public void setDocId(Long aDocId) {
		this.docId = aDocId;
	}

	/**
	 * Obtiene la denominación del documento digitalizado.
	 * 
	 * @return la denominación del documento digitalizado.
	 */
	public String getDocName() {
		return docName;
	}

	/**
	 * Establece la denominación del documento digitalizado.
	 * 
	 * @param aDocName
	 *            Denominación del documento digitalizado.
	 */
	public void setDocName(String aDocName) {
		this.docName = aDocName;
	}

	/**
	 * Obtiene el identificador de la última traza de ejecución del proceso de
	 * digitalización del documento digitalizado.
	 * 
	 * @return el identificador de la última traza de ejecución del proceso de
	 *         digitalización del documento digitalizado.
	 */
	public Long getLastDocExecLogId() {
		return lastDocExecLogId;
	}

	/**
	 * Establece el identificador de la última traza de ejecución del proceso de
	 * digitalización del documento digitalizado.
	 * 
	 * @param aLastDocExecLogId
	 *            identificador de la última traza de ejecución del proceso de
	 *            digitalización del documento digitalizado.
	 */
	public void setLastDocExecLogId(Long aLastDocExecLogId) {
		this.lastDocExecLogId = aLastDocExecLogId;
	}

	/**
	 * Obtiene la denominación de la fase de la última traza de ejecución del
	 * proceso de digitalización del documento digitalizado.
	 * 
	 * @return la denominación de la fase de la última traza de ejecución del
	 *         proceso de digitalización del documento digitalizado.
	 */
	public String getLastDocExecLogStageName() {
		return lastDocExecLogStageName;
	}

	/**
	 * Establece la denominación de la fase de la última traza de ejecución del
	 * proceso de digitalización del documento digitalizado.
	 * 
	 * @param aLastDocExecLogStageName
	 *            denominación de la fase de la última traza de ejecución del
	 *            proceso de digitalización del documento digitalizado.
	 */
	public void setLastDocExecLogStageName(String aLastDocExecLogStageName) {
		this.lastDocExecLogStageName = aLastDocExecLogStageName;
	}

	/**
	 * Obtiene el nombre del identificador del lote de documentos al que pertenece el
	 * documento digitalizado.
	 * 
	 * @return el identificador del lote de documentos al que pertenece el
	 *         documento digitalizado.
	 */
	public String getDocBatchNameId() {
		return docBatchNameId;
	}

	/**
	 * Establece el nombre del identificador del lote de documentos al que pertenece el
	 * documento digitalizado.
	 * 
	 * @param aDocBatchNameId
	 *            identificador del lote de documentos al que pertenece el
	 *            documento digitalizado.
	 */
	public void setDocBatchNameId(String aDocBatchNameId) {
		this.docBatchNameId = aDocBatchNameId;
	}


	/**
	 * Obtiene el identificador del lote de documentos al que pertenece el
	 * documento digitalizado.
	 * 
	 * @return el identificador del lote de documentos al que pertenece el
	 *         documento digitalizado.
	 */
	public Long getDocBatchId() {
		return docBatchId;
	}

	/**
	 * Establece el identificador del lote de documentos al que pertenece el
	 * documento digitalizado.
	 * 
	 * @param aDocBatchId
	 *            identificador del lote de documentos al que pertenece el
	 *            documento digitalizado.
	 */
	public void setDocBatchId(Long aDocBatchId) {
		this.docBatchId = aDocBatchId;
	}

	/**
	 * Obtiene el número de orden del documento en el lote al que pertenece.
	 * 
	 * @return el número de orden del documento en el lote al que pertenece.
	 */
	public Integer getDocBatchOrderNum() {
		return docBatchOrderNum;
	}

	/**
	 * Establece el número de orden del documento en el lote al que pertenece.
	 * 
	 * @param aDocBatchOrderNum
	 *            número de orden del documento en el lote al que pertenece.
	 */
	public void setDocBatchOrderNum(Integer aDocBatchOrderNum) {
		this.docBatchOrderNum = aDocBatchOrderNum;
	}

	/**
	 * Obtiene el instante en el que se inicio el proceso de digitalización del
	 * documento.
	 * 
	 * @return el instante en el que se inicio el proceso de digitalización del
	 *         documento.
	 */
	public Date getDocStartDate() {
		return docStartDate;
	}

	/**
	 * Establece el instante en el que se inicio el proceso de digitalización
	 * del documento.
	 * 
	 * @param aDocStartDate
	 *            instante en el que se inicio el proceso de digitalización del
	 *            documento.
	 */
	public void setDocStartDate(Date aDocStartDate) {
		this.docStartDate = aDocStartDate;
	}

	/**
	 * Obtiene la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento (separados por coma).
	 * 
	 * @return la lista de identificadores de unidades orgánicas a las que
	 *         pertenece el usuario que inicia el proceso de digitalización del
	 *         documento (separados por coma).
	 */
	public String getDocFunctionalOrgs() {
		return docFunctionalOrgs;
	}

	/**
	 * Establece la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento (separados por coma).
	 * 
	 * @param anyDocFunctionalOrgs
	 *            lista de identificadores de unidades orgánicas a las que
	 *            pertenece el usuario que inicia el proceso de digitalización
	 *            del documento (separados por coma).
	 */
	public void setDocFunctionalOrgs(String anyDocFunctionalOrgs) {
		this.docFunctionalOrgs = anyDocFunctionalOrgs;
	}

	/**
	 * Obtiene el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @return el nombre de usuario del usuario que inicia el proceso de
	 *         digitalización del documento.
	 */
	public String getDocUsername() {
		return docUsername;
	}

	/**
	 * Establece el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @param aDocUsername
	 *            nombre de usuario del usuario que inicia el proceso de
	 *            digitalización del documento.
	 */
	public void setDocUsername(String aDocUsername) {
		this.docUsername = aDocUsername;
	}

	/**
	 * Obtiene el estado de ejecución de la fase del proceso de digitalización
	 * de un documento digitalizado.
	 * 
	 * @return el estado de ejecución de la fase del proceso de digitalización
	 *         de un documento digitalizado.
	 */
	public OperationStatus getLastDocExecLogStatus() {
		return lastDocExecLogStatus;
	}

	/**
	 * Establece el estado de ejecución de la fase del proceso de digitalización
	 * de un documento digitalizado.
	 * 
	 * @param aLastDocExecLogStatus
	 *            estado de ejecución de la fase del proceso de digitalización
	 *            de un documento digitalizado.
	 */
	public void setLastDocExecLogStatus(OperationStatus aLastDocExecLogStatus) {
		this.lastDocExecLogStatus = aLastDocExecLogStatus;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docId == null) ? 0 : docId.hashCode());
		result = prime * result + ((lastDocExecLogId == null) ? 0 : lastDocExecLogId.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocBoxScannedDocDTO other = (DocBoxScannedDocDTO) obj;
		if (docId == null) {
			if (other.docId != null)
				return false;
		} else if (!docId.equals(other.docId))
			return false;
		if (lastDocExecLogId == null) {
			if (other.lastDocExecLogId != null)
				return false;
		} else if (!lastDocExecLogId.equals(other.lastDocExecLogId))
			return false;
		return true;
	}

}
