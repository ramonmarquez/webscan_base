/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScanProcessStage.java.</p>
 * <b>Descripción:</b><p> Entidad JPA fase de proceso de digitalización de documentos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA fase de proceso de digitalización de documentos.
 * <p>
 * Clase ScanProcessStage.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "DIGIT_PROC_STAGE", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME" }) })
public class ScanProcessStage implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Atributo DESCRIPTION de la entidad.
	 */
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * Relación de especificaciones de plugins que pueden ser utizados en una
	 * fase.
	 */
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@JoinTable(name = "DPROC_STAGE_PLSPEC", joinColumns = @JoinColumn(name = "DPROC_STAGE_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "PLUGIN_SPEC_ID", referencedColumnName = "ID"))
	private List<PluginSpecification> pluginSpecifications = new ArrayList<PluginSpecification>();

	/**
	 * Relación de definiciones de perfil de digitalización que incluyen una
	 * fase del proceso de digitalización.
	 */
	@OneToMany(mappedBy = "scanProcessStage", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScanProfileSpecScanProcStage.class)
	private List<ScanProfileSpecScanProcStage> scanProfileSpecifications = new ArrayList<ScanProfileSpecScanProcStage>();

	/**
	 * Relación de plugins empleados por configuraciones de perfil de
	 * digitalización en unidades organizativas.
	 */
	@OneToMany(mappedBy = "scanProcessStage", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnitPlugin.class)
	private List<ScanProfileOrgUnitPlugin> orgScanProfilePlugins = new ArrayList<ScanProfileOrgUnitPlugin>();

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProcessStage() {
		super();
	}

	/**
	 * Constructor method for the class ScanProcessStage.java.
	 * 
	 * @param id
	 *            Identificador de la entidad.
	 */
	public ScanProcessStage(Long id) {
		super();
		this.id = id;
	}

	/**
	 * Constructor method for the class ScanProcessStage.java.
	 * 
	 * @param id
	 *            Identificador de la entidad.
	 * @param name
	 *            Nombre de la etapa.
	 * @param description
	 *            Descripción de la etapa.
	 * @param pluginSpecifications
	 *            Listado de especificaciones de plugins.
	 * @param scanProfileSpecifications
	 *            Listado de especificaciones de perfiles.
	 * @param orgScanProfilePlugins
	 *            Listado de plugins relacionado con perfil de escaneo y unidad
	 *            organizativa.
	 */
	public ScanProcessStage(Long id, String name, String description, List<PluginSpecification> pluginSpecifications, List<ScanProfileSpecScanProcStage> scanProfileSpecifications, List<ScanProfileOrgUnitPlugin> orgScanProfilePlugins) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.pluginSpecifications = pluginSpecifications;
		this.scanProfileSpecifications = scanProfileSpecifications;
		this.orgScanProfilePlugins = orgScanProfilePlugins;
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo DESCRIPTION de la entidad.
	 * 
	 * @return el atributo DESCRIPTION de la entidad.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para el atributo DESCRIPTION de la entidad.
	 * 
	 * @param aDescription
	 *            nuevo valor para el atributo DESCRIPTION de la entidad.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene la relación de especificaciones de plugins que pueden ser
	 * utizados en una fase.
	 * 
	 * @return la relación de especificaciones de plugins que pueden ser
	 *         utizados en una fase.
	 */
	public List<PluginSpecification> getPluginSpecifications() {
		return pluginSpecifications;
	}

	/**
	 * Establece una nueva relación de especificaciones de plugins que pueden
	 * ser utizados en una fase.
	 * 
	 * @param anyPluginSpecifications
	 *            nueva relación de especificaciones de plugins que pueden ser
	 *            utizados en una fase.
	 */
	public void setPluginSpecifications(List<PluginSpecification> anyPluginSpecifications) {
		this.pluginSpecifications.clear();

		if (anyPluginSpecifications != null && !anyPluginSpecifications.isEmpty()) {
			this.pluginSpecifications.addAll(anyPluginSpecifications);
		}
	}

	/**
	 * Obtiene la relación de definiciones de perfil de digitalización que
	 * incluyen una fase del proceso de digitalización.
	 * 
	 * @return la relación de definiciones de perfil de digitalización que
	 *         incluyen una fase del proceso de digitalización.
	 */
	public List<ScanProfileSpecScanProcStage> getScanProfileSpecifications() {
		return scanProfileSpecifications;
	}

	/**
	 * Establece una nueva relación de definiciones de perfil de digitalización
	 * que incluyen una fase del proceso de digitalización.
	 * 
	 * @param anyScanProfileSpecifications
	 *            nueva relación de definiciones de perfil de digitalización que
	 *            incluyen una fase del proceso de digitalización.
	 */
	public void setScanProfileSpecifications(List<ScanProfileSpecScanProcStage> anyScanProfileSpecifications) {
		this.scanProfileSpecifications.clear();

		if (anyScanProfileSpecifications != null && !anyScanProfileSpecifications.isEmpty()) {
			this.scanProfileSpecifications.addAll(anyScanProfileSpecifications);
		}
	}

	/**
	 * Obtiene la relación de plugins empleados por configuraciones de perfil de
	 * digitalización en unidades organizativas.
	 * 
	 * @return la relación de plugins empleados por configuraciones de perfil de
	 *         digitalización en unidades organizativas.
	 */
	public List<ScanProfileOrgUnitPlugin> getOrgScanProfilePlugins() {
		return orgScanProfilePlugins;
	}

	/**
	 * Establece una nueva relación de plugins empleados por configuraciones de
	 * perfil de digitalización en unidades organizativas.
	 * 
	 * @param anyOrgScanProfilePlugins
	 *            nueva relación de plugins empleados por configuraciones de
	 *            perfil de digitalización en unidades organizativas.
	 */
	public void setOrgScanProfilePlugins(List<ScanProfileOrgUnitPlugin> anyOrgScanProfilePlugins) {
		this.orgScanProfilePlugins.clear();

		if (anyOrgScanProfilePlugins != null && !anyOrgScanProfilePlugins.isEmpty()) {
			this.orgScanProfilePlugins.addAll(anyOrgScanProfilePlugins);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProcessStage other = (ScanProcessStage) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
