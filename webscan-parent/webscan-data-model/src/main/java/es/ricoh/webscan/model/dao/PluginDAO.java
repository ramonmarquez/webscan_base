/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.PluginDAO.java.</p>
 * <b>Descripción:</b><p> Interfaz que define la operaciones de la capa DAO sobre las entidades plugin, parámetro, especificaciones de plugin y parámetro, y tipos de parámetros, pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao;

import java.util.List;
import java.util.Map;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.dto.ParameterDTO;
import es.ricoh.webscan.model.dto.ParameterSpecificationDTO;
import es.ricoh.webscan.model.dto.ParameterTypeDTO;
import es.ricoh.webscan.model.dto.PluginDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;

/**
 * Interfaz que define la operaciones de la capa DAO sobre las entidades plugin,
 * parámetro, especificaciones de plugin y parámetro, y tipos de parámetros,
 * pertenecientes al modelo de datos base de Webscan.
 * <p>
 * Clase PluginDAO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface PluginDAO {

	// Operaciones sobre especificaciones de plugins y parámetros
	/**
	 * Obtiene el listado de definiciones de plugins registradas en el sistema,
	 * permitiendo la paginación y ordenación del resultado.
	 * 
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return listado de definiciones de plugins.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<PluginSpecificationDTO> listPluginSpecifications(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException;

	/**
	 * Obtiene una especificación de plugin a partir de su identificador.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de especificación de plugin.
	 * @return La definición de plugin solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	PluginSpecificationDTO getPluginSpecification(Long pluginSpecificationId) throws DAOException;

	/**
	 * Obtiene una especificación de plugin a partir de su denominación.
	 * 
	 * @param pluginSpecificationName
	 *            Denominación de especificación de plugin.
	 * @return La definición de plugin solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	PluginSpecificationDTO getPluginSpecificationByName(String pluginSpecificationName) throws DAOException;

	/**
	 * Obtiene el listado de especificaciones de plugins que implementan una
	 * deteminada fase de proceso de digitalización.
	 * 
	 * @param scanProcessStageId
	 *            Identificador de fase de proceso de digitalización.
	 * @return listado de especificaciones de plugins que implementan una
	 *         deteminada fase de proceso de digitalización.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             fase, o se produce algún error en la consulta a BBDD.
	 */
	List<PluginSpecificationDTO> getPluginSpecsByScanProcStageId(Long scanProcessStageId) throws DAOException;

	/**
	 * Obtiene la especificación de plugin que implementa una determinada fase
	 * para un perfil de digitalización configurado en una unidad organizativa.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización
	 *            para unidad organizativa.
	 * @param scanProcessStageName
	 *            Denominación de una fase de proceso de digitalziación.
	 * @return La definición de plugin que implementa una determinada fase para
	 *         un perfil de digitalización configurado en una unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	PluginSpecificationDTO getPluginSpecByScanProfileOrgUnitAndStage(Long scanProfileOrgUnitId, String scanProcessStageName) throws DAOException;

	/**
	 * Obtiene las especificaciones de plugins y fases de procesos de
	 * digitalización que implementan.
	 * 
	 * @return Mapa donde cada entrada es identificada por una especificación de
	 *         plugin, y cuyo valor es la relación de fases de procesos de
	 *         digitalización que identifica la entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	Map<PluginSpecificationDTO, List<ScanProcessStageDTO>> listPluginSpecsAndScanProcessStages() throws DAOException;

	/**
	 * Comprueba si una especificación de plugins posee documentos en proceso
	 * de digitalización.
	 * 
	 * @param pluginSpecId
	 *            Identificador de especificación de plugin.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	Boolean hasPluginSpecDocsInScanProccess(Long pluginSpecId) throws DAOException;
	
	/**
	 * Crea una nueva definición de plugin en el sistema.
	 * 
	 * @param specification
	 *            Nueva definición de plugin.
	 * @return Iidentificador de la definición de plugin creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createPluginSpecification(PluginSpecificationDTO specification) throws DAOException;

	/**
	 * Actualiza una especificación de plugin registrada en el sistema. Los
	 * datos que pueden ser actualizados son la denominación, descripción y el
	 * estado de activación de la especificación.
	 * 
	 * @param specification
	 *            Especificación de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void updatePluginSpecification(PluginSpecificationDTO specification) throws DAOException;

	/**
	 * Elimina una especificación de plugin registrada en el sistema.
	 * 
	 * @param specificationId
	 *            Identificador de especificación de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             definición, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void removePluginSpecification(Long specificationId) throws DAOException;

	// Operaciones sobre plugins
	/**
	 * Obtiene la relación de plugins registrada en el sistema para una
	 * especificación de plugin determinada.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de especificación de plugin.
	 * @return Lista de configuraciones de una especificación de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	List<PluginDTO> getPluginsBySpecification(Long pluginSpecificationId) throws DAOException;

	/**
	 * Obtiene un plugin a partir de su identificador.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @return El plugin solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	PluginDTO getPlugin(Long pluginId) throws DAOException;

	/**
	 * Comprueba si un plugin posee documentos en proceso
	 * de digitalización.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	Boolean hasPluginDocsInScanProccess(Long pluginId) throws DAOException;
	
	/**
	 * Crea un nuevo plugin en el sistema.
	 * 
	 * @param plugin
	 *            Nuevo plugin.
	 * @param pluginSpecId
	 *            Identificador de la especificación de plugin implementada.
	 * @return Identificador del plugin creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	Long createPlugin(PluginDTO plugin, Long pluginSpecId) throws DAOException;

	/**
	 * Actualiza un plugin registrado en el sistema. Los datos que pueden ser
	 * actualizados son la denominación y el estado de activación del plugin.
	 * 
	 * @param plugin
	 *            Plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void updatePlugin(PluginDTO plugin) throws DAOException;

	/**
	 * Elimina un plugin del sistema.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void removePlugin(Long pluginId) throws DAOException;

	/**
	 * Establece una nueva lista de plugins para una especificación de plugin
	 * dada, registrando además nuevas listas de parámetros para los plugins.
	 * 
	 * @param plugins
	 *            Mapa donde cada entrada es identificada por un plugin, y su
	 *            valor se corresponde con la nueva lista de parámetros del
	 *            plugin que identifica la entrada (incluyen definición).
	 * @param pluginSpecificationId
	 *            Identificador de definición de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	void setPlugins(Map<PluginDTO, Map<ParameterSpecificationDTO, ParameterDTO>> plugins, Long pluginSpecificationId) throws DAOException;

	// Operaciones sobre especificaciones de parámetros
	/**
	 * Recupera la relación de parámetros registrada en el sistema para una
	 * especificación de plugin determinada.
	 * 
	 * @param pluginSpecification
	 *            Identificador de especificación de plugin.
	 * @return Lista de parámetros de una especificación de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	List<ParameterSpecificationDTO> getParameterSpecificationsByPluginSpec(Long pluginSpecification) throws DAOException;

	/**
	 * Obtiene una especificación de parámetro a partir de su identificador.
	 * 
	 * @param parameterSpecificationId
	 *            Identificador de especificación de parámetro.
	 * @return La especificación de parámetro solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de parámetro solicitada, o se produce algún
	 *             error en la consulta a BBDD.
	 */
	ParameterSpecificationDTO getParameterSpecification(Long parameterSpecificationId) throws DAOException;

	/**
	 * Establece una nueva lista de parámetros para una especificación de plugin
	 * dada.
	 * 
	 * @param parameterSpecifications
	 *            Nueva relación de parámetros.
	 * @param pluginSpecificationId
	 *            Identificador de definición de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             ejecución de la sentencia de BBDD.
	 */
	void setParameterSpecifications(List<ParameterSpecificationDTO> parameterSpecifications, Long pluginSpecificationId) throws DAOException;

	// Operaciones sobre parámetros
	/**
	 * Recupera la relación de parámetros registrada en el sistema para un
	 * plugin determinado.
	 * 
	 * @param plugin
	 *            Identificador de plugin.
	 * @return Lista de parámetros de un plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la consulta a BBDD.
	 */
	List<ParameterDTO> getParametersByPlugin(Long plugin) throws DAOException;

	/**
	 * Obtiene una relación de parámetros de plugin a partir de sus
	 * identificadores.
	 * 
	 * @param parameterIds
	 *            Relación de identificadores de parámetros.
	 * @return La lista de parámetros solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe algún
	 *             parámetro solicitado, o se produce algún error en la consulta
	 *             a BBDD.
	 */
	List<ParameterDTO> getParameters(List<Long> parameterIds) throws DAOException;

	/**
	 * Obtiene un parámetro a partir de su identificador.
	 * 
	 * @param parameterId
	 *            Identificador de parámetro.
	 * @return El parámetro solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             parámetro solicitado, o se produce algún error en la consulta
	 *             a BBDD.
	 */
	ParameterDTO getParameter(Long parameterId) throws DAOException;

	/**
	 * Establece una nueva lista de parámetros para un plugin dado.
	 * 
	 * @param parameters
	 *            Nueva relación de parámetros.
	 * @param pluginId
	 *            Identificador de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	void setParameters(Map<ParameterSpecificationDTO, ParameterDTO> parameters, Long pluginId) throws DAOException;

	// Operaciones tipos de parametros
	/**
	 * Recupera la relación de tipos de parámetros registrada en el sistema.
	 * 
	 * @return Listado de tipos de parámetros registrada en el sistema.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	List<ParameterTypeDTO> listParameterTypes() throws DAOException;

	/**
	 * Obtiene un tipo de parámetro a partir de su identificador.
	 * 
	 * @param parameterTypeId
	 *            Identificador de tipo de parámetro.
	 * @return El tipo de parámetro solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             tipo de parámetro solicitado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	ParameterTypeDTO getParameterType(Long parameterTypeId) throws DAOException;

}
