/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.OrganizationUnitDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA unidad organizativa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA unidad organizativa.
 * <p>
 * Clase OrganizationUnitDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class OrganizationUnitDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la unidad organizativa.
	 */
	private Long id;

	/**
	 * Identificador externo de la unidad organizativa.
	 */
	private String externalId;

	/**
	 * Denominación de la unidad organizativa..
	 */
	private String name;

	/**
	 * Identificador de la unidad organizativa padre.
	 */
	private Long parentId;

	/**
	 * Constructor sin argumentos.
	 */
	public OrganizationUnitDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la unidad organizativa.
	 * 
	 * @return el identificador de la unidad organizativa.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la unidad organizativa.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la unidad organizativa.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el identificador externo de la unidad organizativa.
	 * 
	 * @return el identificador externo de la unidad organizativa.
	 */
	public String getExternalId() {
		return externalId;
	}

	/**
	 * Establece un nuevo valor para el identificador externo de la unidad
	 * organizativa.
	 * 
	 * @param anExternalId
	 *            nuevo valor para el identificador externo de la unidad
	 *            organizativa.
	 */
	public void setExternalId(String anExternalId) {
		this.externalId = anExternalId;
	}

	/**
	 * Obtiene la denominación de la unidad organizativa.
	 * 
	 * @return la denominación de la unidad organizativa.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación de la unidad organizativa.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación de la unidad organizativa.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el identificador de la unidad organizativa padre. Retorna null si
	 * es unidad organizativa raíz.
	 * 
	 * @return el identificador de la unidad organizativa padre.
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * Establece un nuevo valor para el identificador de la unidad organizativa
	 * padre.
	 * 
	 * @param aParentId
	 *            nuevo valor para el identificador de la unidad organizativa
	 *            padre.
	 */
	public void setParentId(Long aParentId) {
		this.parentId = aParentId;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationUnitDTO other = (OrganizationUnitDTO) obj;
		if (externalId == null) {
			if (other.externalId != null)
				return false;
		} else if (!externalId.equals(other.externalId))
			return false;
		return true;
	}

}
