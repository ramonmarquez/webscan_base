/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.base.audit.AuditManager.java.</p>
 * <b>Descripción:</b><p> Servicio interno para el registro y consulta de trazas y eventos de
 * auditoría del módulo de unidades organizativas de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelConstants;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;

/**
 * Servicio interno para el registro y consulta de trazas y eventos de auditoría
 * del módulo de gestión de usuarios de Webscan.
 * <p>
 * Clase UsersAuditManager.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

public class AuditService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AuditService.class);

	/**
	 * Fachada de gestión y consulta sobre las entidades pertenecientes al
	 * modelo de datos base de Webscan.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Registra y cierra una traza de auditoría con un evento.
	 * 
	 * @param username
	 *            Nombre de usuario del usuario que intenta acceder al sistema.
	 * @param result
	 *            Indica si el acceso fue correcto o fallido.
	 * @param authTimestamp
	 *            Instante en el que se produce el inicio de sesión.
	 * @param exception
	 *            En caso de acceso fallido, causa del error.
	 * @param auditOperationName
	 *            Denominación de la traza de auditoría.
	 * @param auditEventName
	 *            Denominación del evento de auditoría.
	 * @throws DAOException
	 *             Si sucede algún error al registrar la traza de auditoría en
	 *             BBDD.
	 */
	public void createAuditLogs(String username, AuditOpEventResult result, Date authTimestamp, Exception exception, AuditOperationName auditOperationName, AuditEventName auditEventName) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;

		auditOperation = new AuditOperationDTO();
		auditOperation.setName(auditOperationName);
		auditOperation.setStartDate(authTimestamp);
		auditOperation.setLastUpdateDate(authTimestamp);
		auditEvent = new AuditEventDTO();
		if (AuditOpEventResult.ERROR.equals(result)) {
			auditOperation.setStatus(AuditOperationStatus.CLOSED_ERROR);
			// Tamaño máximo del campo 256 caracteres
			additionalInfo = exception.getClass().getSimpleName() + ": " + exception.getMessage();

			if (additionalInfo.length() > WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
				additionalInfo = additionalInfo.substring(0, WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
			}

			auditEvent.setAdditionalInfo(additionalInfo);
		} else {
			auditOperation.setStatus(AuditOperationStatus.CLOSED_OK);
		}
		auditEvent.setEventDate((authTimestamp != null ? authTimestamp : new Date()));
		auditEvent.setEventName(auditEventName);
		auditEvent.setEventResult(result);
		auditEvent.setEventUser(username);
		auditEvents.add(auditEvent);

		// añadimos nueva auditoria
		addNewAuditLogs(auditOperation, auditEvents, Boolean.TRUE);
	}

	/**
	 * Recupera una traza de auditoría que posea un evento ejecutado por usuario
	 * dado y contenga como información adicional un texto determinado.
	 * 
	 * @param user
	 *            Nombre de usuario.
	 * @param additionalInfo
	 *            Texto que debe estar incluido en la información adicional del
	 *            evento.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public AuditOperationDTO getAuditOperation(String user, String additionalInfo) throws DAOException {
		AuditOperationDTO res;

		res = webscanModelManager.getAuditOperation(user, additionalInfo);

		return res;
	}

	/**
	 * Recupera una traza de auditoría a partir de su identificador.
	 * 
	 * @param auditOpId
	 *            Identificador de traza de auditoría.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public AuditOperationDTO getAuditOperation(Long auditOpId) throws DAOException {
		AuditOperationDTO res;

		res = webscanModelManager.getAuditOperation(auditOpId);

		return res;
	}

	/**
	 * Recupera la traza de auditoría más reciente con una determinada
	 * denominación, que posea un evento ejecutado por usuario dado, y
	 * opcionalmente el estado que se encuentra.
	 * 
	 * @param auditOperationName
	 *            Denominación de traza de auditoría.
	 * @param auditOperationStatus
	 *            Estado de la traza de auditoría. Puede ser null.
	 * @param user
	 *            Nombre de usuario.
	 * @return La traza de auditoría solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe traza
	 *             de auditoría para los parámetros especificados, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public AuditOperationDTO getLastAuditOperation(AuditOperationName auditOperationName, AuditOperationStatus auditOperationStatus, String user) throws DAOException {
		AuditOperationDTO res = null;
		try {
			if (auditOperationStatus == null) {
				res = webscanModelManager.getLastAuditOperationByOpNameAndUser(auditOperationName, user);
			} else {
				res = webscanModelManager.getLastAuditOperationByOpNameStatusAndUser(auditOperationName, auditOperationStatus, user);
			}
		} catch (DAOException e) {
			if (DAOException.CODE_999.equals(e.getCode())) {
				throw e;
			}
		}
		return res;
	}

	/**
	 * Crea o actualiza, y cierra, una traza de auditoría en el sistema,
	 * añadiéndole un conjunto de eventos de auditoría.
	 * 
	 * @param auditOperation
	 *            Traza de auditoría a crear o actualizar.
	 * @param auditEvents
	 *            Lista de eventos de la traza de auditoría.
	 * @param closeAuditop
	 *            Indicador de actualización y cierre de traza de auditoría.
	 * @return El identificador de la traza de auditoría creada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public Long addNewAuditLogs(AuditOperationDTO auditOperation, List<AuditEventDTO> auditEvents, Boolean closeAuditop) throws DAOException {
		String excMsg;
		Long res = -1L;

		if (auditEvents == null || auditEvents.isEmpty()) {
			excMsg = "[WEBSCAN-USERS] Error al registrar trazas de auditoria, no se especificaron eventos.";
			LOG.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		if (closeAuditop) {
			res = webscanModelManager.closeAuditOperation(auditOperation, auditEvents);
		} else {
			res = webscanModelManager.openAuditOperation(auditOperation, auditEvents);
		}

		return res;
	}

	/**
	 * Añade una relación de eventos a una traza de auditoría.
	 * 
	 * @param auditOperation
	 *            Traza de auditoría.
	 * @param auditEvents
	 *            Lista de eventos de auditoría
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de auditoría, se encuentra cerrada, o se produce algún
	 *             error en la ejecución de las sentencias de BBDD.
	 */
	public void updateAuditLogs(AuditOperationDTO auditOperation, List<AuditEventDTO> auditEvents) throws DAOException {
		String excMsg;

		if (auditEvents == null || auditEvents.isEmpty()) {
			excMsg = "[WEBSCAN-USERS] Error al añadir eventos a la traza de auditoria " + (auditOperation != null ? auditOperation.getId() : "No especificada") + ", no se especificaron eventos.";
			LOG.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		webscanModelManager.addAuditEvents(auditEvents, auditOperation.getId());
	}

	/**
	 * Cierra una traza de auditoría, actualizando su información y estado, e
	 * incorporando por último la lista de eventos especificada como parámetro
	 * de entrada.
	 * 
	 * @param auditOperation
	 *            traza de auditoría.
	 * @param auditEvents
	 *            Lista de eventos de la traza de auditoría.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */
	public void closeAuditLogs(AuditOperationDTO auditOperation, List<AuditEventDTO> auditEvents) throws DAOException {
		webscanModelManager.closeAuditOperation(auditOperation, auditEvents);
	}

	/**
	 * Establece un nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}
}
