/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.EntityOrderByClause.java.</p>
 * <b>Descripción:</b><p> Interfaz que representa la clausula ORDER BY sobre una entidad de BBDD.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model;

/**
 * Interfaz que representa la clausula ORDER BY sobre una entidad de BBDD.
 * <p>
 * Interfaz EntityOrderByClause.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface EntityOrderByClause {

	/**
	 * Devuelve el nombre del atributo de la entidad de BBDD objeto de la
	 * clausula de ordenación.
	 * 
	 * @return Nombre del atributo de la entidad de BBDD objeto de la clausula
	 *         de ordenación.
	 */
	String getAttName();

	/**
	 * Devuelve el tipo de ordenación sobre un atributo de la entidad de BBDD
	 * objeto de la clausula de ordenación.
	 * 
	 * @return Tipo de ordenación sobre un atributo de la entidad de BBDD objeto
	 *         de la clausula de ordenación.
	 */
	OrderByClause getOrderByClause();

}
