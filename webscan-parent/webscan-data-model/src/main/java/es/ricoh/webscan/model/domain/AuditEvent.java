/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.AuditEvent.java.</p>
 * <b>Descripción:</b><p> Entidad JPA evento de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad JPA evento de auditoría.
 * <p>
 * Clase AuditEvent.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "AUDIT_EVENT")
public final class AuditEvent implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "AUDIT_EVENT_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AUDIT_EVENT_ID_SEQ", sequenceName = "AUDIT_EVENT_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo EVENT_NAME de la entidad.
	 */
	@Column(name = "EVENT_NAME")
	@Enumerated(EnumType.STRING)
	private AuditEventName eventName;

	/**
	 * Atributo EVENT_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EVENT_DATE")
	private Date eventDate;

	/**
	 * Atributo EVENT_USER de la entidad.
	 */
	@Column(name = "EVENT_USER")
	private String eventUser;

	/**
	 * Atributo EVENT_RESULT de la entidad.
	 */
	@Column(name = "EVENT_RESULT")
	@Enumerated(EnumType.STRING)
	private AuditOpEventResult eventResult;

	/**
	 * Atributo EVENT_DATA de la entidad.
	 */
	@Column(name = "EVENT_DATA")
	private String additionalInfo;

	/**
	 * Entidad traza de auditoría en la que se produce el evento.
	 */
	@ManyToOne(fetch = FetchType.EAGER, targetEntity = es.ricoh.webscan.model.domain.AuditOperation.class)
	@JoinColumn(name = "AUDIT_OP_ID")
	private AuditOperation auditOperation;

	/**
	 * Constructor sin argumentos.
	 */
	public AuditEvent() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo EVENT_NAME de la entidad.
	 * 
	 * @return el atributo EVENT_NAME de la entidad.
	 */
	public AuditEventName getEventName() {
		return eventName;
	}

	/**
	 * Establece un nuevo valor para el atributo EVENT_NAME de la entidad.
	 * 
	 * @param anEventName
	 *            nuevo valor para el atributo EVENT_NAME de la entidad.
	 */
	public void setEventName(AuditEventName anEventName) {
		this.eventName = anEventName;
	}

	/**
	 * Obtiene el atributo EVENT_DATE de la entidad.
	 * 
	 * @return el atributo EVENT_DATE de la entidad.
	 */
	public Date getEventDate() {
		return eventDate;
	}

	/**
	 * Establece un nuevo valor para el atributo EVENT_DATE de la entidad.
	 * 
	 * @param anEventDate
	 *            nuevo valor para el atributo EVENT_DATE de la entidad.
	 */
	public void setEventDate(Date anEventDate) {
		this.eventDate = anEventDate;
	}

	/**
	 * Obtiene el atributo EVENT_USER de la entidad.
	 * 
	 * @return el atributo EVENT_USER de la entidad.
	 */
	public String getEventUser() {
		return eventUser;
	}

	/**
	 * Establece un nuevo valor para el atributo EVENT_USER de la entidad.
	 * 
	 * @param anEventUser
	 *            nuevo valor para el atributo EVENT_USER de la entidad.
	 */
	public void setEventUser(String anEventUser) {
		this.eventUser = anEventUser;
	}

	/**
	 * Obtiene el atributo EVENT_RESULT de la entidad.
	 * 
	 * @return el atributo EVENT_RESULT de la entidad.
	 */
	public AuditOpEventResult getEventResult() {
		return eventResult;
	}

	/**
	 * Establece un nuevo valor para el atributo EVENT_RESULT de la entidad.
	 * 
	 * @param anEventResult
	 *            nuevo valor para el atributo EVENT_RESULT de la entidad.
	 */
	public void setEventResult(AuditOpEventResult anEventResult) {
		this.eventResult = anEventResult;
	}

	/**
	 * Obtiene el atributo EVENT_DATA de la entidad.
	 * 
	 * @return el atributo EVENT_DATA de la entidad.
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * Establece un nuevo valor para el atributo EVENT_DATA de la entidad.
	 * 
	 * @param anyAdditionalInfo
	 *            nuevo valor para el atributo EVENT_DATA de la entidad.
	 */
	public void setAdditionalInfo(String anyAdditionalInfo) {
		this.additionalInfo = anyAdditionalInfo;
	}

	/**
	 * Obtiene la traza de auditoría en la que se produce el evento.
	 * 
	 * @return la traza de auditoría en la que se produce el evento.
	 */
	public AuditOperation getAuditOperation() {
		return auditOperation;
	}

	/**
	 * Establece una nueva traza de auditoría en la que se produce el evento.
	 * 
	 * @param anAuditOperation
	 *            Nueva traza de auditoría en la que se produce el evento.
	 */
	public void setAuditOperation(AuditOperation anAuditOperation) {
		this.auditOperation = anAuditOperation;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditEvent other = (AuditEvent) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
