/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.impl.ScanProfileDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre las entidades perfil de digitalización, su
 *  especificación y relaciones, perteneciente al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.dao.ScanProfileDAO;
import es.ricoh.webscan.model.domain.DocumentSpecification;
import es.ricoh.webscan.model.domain.Plugin;
import es.ricoh.webscan.model.domain.ScanProfile;
import es.ricoh.webscan.model.domain.ScanProfileOrgUnit;
import es.ricoh.webscan.model.domain.ScanProfileOrgUnitDocSpec;
import es.ricoh.webscan.model.domain.ScanProfileOrgUnitPlugin;
import es.ricoh.webscan.model.domain.ScanProfileSpecification;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MappingUtilities;
import es.ricoh.webscan.model.dto.PluginScanProcStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecificationDTO;

/**
 * Implementación JPA 2.0 de la capa DAO sobre las entidades perfil de
 * digitalización, su especificación y relaciones, perteneciente al modelo de
 * datos base de Webscan.
 * <p>
 * Clase .
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.dao.ScanProfileDAO
 * @version 2.0.
 */
public final class ScanProfileDAOJdbcImpl implements ScanProfileDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScanProfileDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la manipulación de las entidades de incluidas en el
	 * contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfileDAOJdbcImpl() {
		super();
	}

	/**
	 * Establece un nuevo gestor de persistencia JPA.
	 * 
	 * @param aEntityManager
	 *            manager de persistencia.
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		this.entityManager = aEntityManager;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#listScanProfileSpecifications(java.lang.Long,
	 *      java.lang.Long, java.util.List)
	 */
	@Override
	public List<ScanProfileSpecificationDTO> listScanProfileSpecifications(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		int first, maxResults;
		List<ScanProfileSpecification> ddbbRes = new ArrayList<ScanProfileSpecification>();
		List<ScanProfileSpecificationDTO> res = new ArrayList<ScanProfileSpecificationDTO>();
		Map<String, List<EntityOrderByClause>> spsOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {

			LOG.debug("[WEBSCAN-MODEL] Recuperando las definiciones de perfil de digitalización registradas en el sistema ...");

			if (orderByColumns != null && !orderByColumns.isEmpty()) {
				spsOrderByColumns.put("sps.", orderByColumns);
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT sps FROM ScanProfileSpecification sps", spsOrderByColumns);

			TypedQuery<ScanProfileSpecification> query = entityManager.createQuery(jpqlQuery, ScanProfileSpecification.class);

			if (elementsPerPage != null) {
				first = 0;
				maxResults = elementsPerPage.intValue();
				if (pageNumber != null) {
					first = (pageNumber.intValue() - 1) * maxResults;
				}

				query.setFirstResult(first);
				query.setMaxResults(maxResults);
			}

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las definiciones de perfil de digitalización registradas en el sistema ...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillScanProfileSpecificationListDto(ddbbRes);

			LOG.debug("[WEBSCAN-MODEL] Definiciones de perfil de digitalización recuperadas:{} ", res.size());
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de perfil de digitalización registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de perfil de digitalización registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de perfil de digitalización registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de perfil de digitalización registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de perfil de digitalización registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de perfil de digitalización registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de perfil de digitalización registradas en el sistema: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileSpecification(java.lang.Long)
	 */
	@Override
	public ScanProfileSpecificationDTO getScanProfileSpecification(Long scanProfileSpecId) throws DAOException {
		ScanProfileSpecification entity;
		ScanProfileSpecificationDTO res = null;

		try {
			entity = getScanProfileSpecificationEntity(scanProfileSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando la definición de perfil de digitalización " + scanProfileSpecId + " recuperada de BBDD...");
			}
			res = MappingUtilities.fillScanProfileSpecificationDto(entity);

		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileSpecificationByName(java.lang.String)
	 */
	@Override
	public ScanProfileSpecificationDTO getScanProfileSpecificationByName(String scanProfileSpecName) throws DAOException {
		ScanProfileSpecificationDTO res;
		ScanProfileSpecification entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de perfil de digitalización denominada " + scanProfileSpecName + " de BBDD ...");
			}
			TypedQuery<ScanProfileSpecification> query = entityManager.createQuery("SELECT sps FROM ScanProfileSpecification sps WHERE sps.name = :scanProfileSpecName", ScanProfileSpecification.class);
			query.setParameter("scanProfileSpecName", scanProfileSpecName);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la definición de perfil de digitalización denominada " + scanProfileSpecName + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScanProfileSpecificationDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de perfil de digitalización denominada " + res.getName() + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización denominada " + scanProfileSpecName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de perfil de digitalización denominada " + scanProfileSpecName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización denominada " + scanProfileSpecName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización denominada " + scanProfileSpecName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización denominada " + scanProfileSpecName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización denominada " + scanProfileSpecName + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización denominada " + scanProfileSpecName + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de perfil de digitalización denominada " + scanProfileSpecName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización denominada " + scanProfileSpecName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#updateScanProfileSpecification(es.ricoh.webscan.model.dto.ScanProfileSpecificationDTO)
	 */
	@Override
	public void updateScanProfileSpecification(ScanProfileSpecificationDTO scanProfileSpecification) throws DAOException {
		ScanProfileSpecification entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando la definición de perfil de digitalización " + scanProfileSpecification.getId() + " ...");
		}
		try {
			entity = getScanProfileSpecificationEntity(scanProfileSpecification.getId());

			entity.setDescription(scanProfileSpecification.getDescription());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando la definición de perfil de digitalización " + scanProfileSpecification.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de perfil de digitalización " + scanProfileSpecification.getId() + " actualizada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de perfil de digitalización " + scanProfileSpecification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de perfil de digitalización " + scanProfileSpecification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de perfil de digitalización " + scanProfileSpecification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de perfil de digitalización " + scanProfileSpecification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#listScanProfiles(java.lang.Long,
	 *      java.lang.Long, java.util.List)
	 */
	@Override
	public List<ScanProfileDTO> listScanProfiles(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		int first, maxResults;
		List<ScanProfile> ddbbRes = new ArrayList<ScanProfile>();
		List<ScanProfileDTO> res = new ArrayList<ScanProfileDTO>();
		Map<String, List<EntityOrderByClause>> spOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {

			LOG.debug("[WEBSCAN-MODEL] Recuperando los perfiles de digitalización...");

			if (orderByColumns != null && !orderByColumns.isEmpty()) {
				spOrderByColumns.put("sp.", orderByColumns);
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT sp FROM ScanProfile sp", spOrderByColumns);

			TypedQuery<ScanProfile> query = entityManager.createQuery(jpqlQuery, ScanProfile.class);

			if (elementsPerPage != null) {
				first = 0;
				maxResults = elementsPerPage.intValue();
				if (pageNumber != null) {
					first = (pageNumber.intValue() - 1) * maxResults;
				}

				query.setFirstResult(first);
				query.setMaxResults(maxResults);
			}

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar los perfiles de digitalización...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillScanProfileListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Perfiles de digitalización recuperados: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los perfiles de digitalización: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los perfiles de digitalización: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	@Override
	public ScanProfileDTO getScanProfile(Long scanProfileId) throws DAOException {
		ScanProfile entity;
		ScanProfileDTO res = null;

		try {
			entity = getScanProfileEntity(scanProfileId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando el perfil de digitalización " + scanProfileId + " recuperado de BBDD...");
			}
			res = MappingUtilities.fillScanProfileDto(entity);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileByName(java.lang.String)
	 */
	@Override
	public ScanProfileDTO getScanProfileByName(String scanProfileName) throws DAOException {
		ScanProfileDTO res;
		ScanProfile entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el perfil de digitalización denominado " + scanProfileName + " de BBDD ...");
			}
			TypedQuery<ScanProfile> query = entityManager.createQuery("SELECT scanProfile FROM ScanProfile scanProfile WHERE scanProfile.name = :scanProfileName", ScanProfile.class);
			query.setParameter("scanProfileName", scanProfileName);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el perfil de digitalización denominado " + scanProfileName + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScanProfileDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Perfil de digitalización denominado " + res.getName() + " recuperado: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + scanProfileName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización denominado " + scanProfileName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + scanProfileName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + scanProfileName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + scanProfileName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + scanProfileName + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + scanProfileName + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización denominado " + scanProfileName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización denominado " + scanProfileName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfilesBySpec(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.util.List)
	 */
	@Override
	public List<ScanProfileDTO> getScanProfilesBySpec(Long scanProfileSpecId, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		int first, maxResults;
		List<ScanProfile> ddbbRes = new ArrayList<ScanProfile>();
		List<ScanProfileDTO> res = new ArrayList<ScanProfileDTO>();
		Map<String, List<EntityOrderByClause>> spOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los perfiles de digitalización para la definición : " + scanProfileSpecId + "...");
			}
			if (orderByColumns != null && !orderByColumns.isEmpty()) {
				spOrderByColumns.put("sp.", orderByColumns);
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT sp FROM ScanProfile sp JOIN sp.specification sps WHERE sps.id = :scanProfileSpecId", spOrderByColumns);

			TypedQuery<ScanProfile> query = entityManager.createQuery(jpqlQuery, ScanProfile.class);
			query.setParameter("scanProfileSpecId", scanProfileSpecId);

			if (elementsPerPage != null) {
				first = 0;
				maxResults = elementsPerPage.intValue();
				if (pageNumber != null) {
					first = (pageNumber.intValue() - 1) * maxResults;
				}

				query.setFirstResult(first);
				query.setMaxResults(maxResults);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar los perfiles de digitalización para la definición : " + scanProfileSpecId + " ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillScanProfileListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Perfiles de digitalización recuperados para la definición " + scanProfileSpecId + ": " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización para la definición : " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los perfiles de digitalización para la definición : " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización para la definición : " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización para la definición : " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización para la definición : " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los perfiles de digitalización para la definición : " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización para la definición : " + scanProfileSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfilesByOrgUnit(java.lang.Long)
	 */
	@Override
	public List<ScanProfileDTO> getScanProfilesByOrgUnit(Long orgUnitId) throws DAOException {
		List<ScanProfileDTO> res = new ArrayList<ScanProfileDTO>();
		List<ScanProfile> ddbbRes;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los perfiles de digitalización configurados para la unidad organizativa " + orgUnitId + " de BBDD ...");
			}
			TypedQuery<ScanProfile> query = entityManager.createQuery("SELECT sp FROM ScanProfile sp JOIN sp.organizations spOrgUnits JOIN spOrgUnits.orgUnit orgUnit WHERE orgUnit.id = :orgUnitId", ScanProfile.class);
			query.setParameter("orgUnitId", orgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar los perfiles de digitalización configurados para la " + "unidad organizativa " + orgUnitId + " ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillScanProfileListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Perfiles de digitalización configurados para unidad organizativa " + orgUnitId + " recuperados de BBDD: " + res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los perfiles de digitalización configurados para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los perfiles de digitalización configurados para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización configurados para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización configurados para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización configurados para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los perfiles de digitalización configurados para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los perfiles de digitalización configurados para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitByDoc(java.lang.Long)
	 */
	@Override
	public ScanProfileOrgUnitDTO getScanProfileOrgUnitByDoc(Long docId) throws DAOException {
		ScanProfileOrgUnitDTO res = null;
		ScanProfileOrgUnit entity;
		String excMsg;

		try {
			if (docId != null && LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el perfil de digitalización configurado en unidad organizativa del documento " + docId + " de BBDD ...");
			}
			TypedQuery<ScanProfileOrgUnit> query = entityManager.createQuery("SELECT spou FROM ScanProfileOrgUnit spou JOIN spou.orgScanProfileDocSpecs ospds JOIN ospds.documents docs WHERE docs.id = :docId", ScanProfileOrgUnit.class);
			query.setParameter("docId", docId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el perfil de digitalización configurado en unidad organizativa del documento " + docId + " de BBDD ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScanProfileOrgUnitDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperado el plugin configurado para el perfil de digitalización configurado en unidad organizativa del documento " + docId + " con identificador: " + res.getId() + ".");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización configurado en unidad organizativa del documento " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización configurado en unidad organizativa del documento " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización configurado en unidad organizativa del documento " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización configurado en unidad organizativa del documento " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado el perfil de digitalización configurado en unidad organizativa del documento " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización configurado en unidad organizativa del documento " + docId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización configurado en unidad organizativa del documento " + docId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización configurado en unidad organizativa del documento " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización configurado en unidad organizativa del documento " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitPluginByStageAndDoc(java.lang.Long,
	 *      java.lang.Long)
	 */
	@Override
	public ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPluginByStageAndDoc(Long scanProcessStageId, Long docId) throws DAOException {
		ScanProfileOrgUnitPluginDTO res = null;
		ScanProfileOrgUnitPlugin entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + " de BBDD ...");
			}
			TypedQuery<ScanProfileOrgUnitPlugin> query = entityManager.createQuery("SELECT spoup FROM ScanProfileOrgUnitPlugin spoup JOIN spoup.scanProcessStage sps JOIN spoup.orgScanProfile osp JOIN osp.orgScanProfileDocSpecs ospds JOIN ospds.documents docs WHERE sps.id = :scanProcessStageId AND docs.id = :docId", ScanProfileOrgUnitPlugin.class);
			query.setParameter("scanProcessStageId", scanProcessStageId);
			query.setParameter("docId", docId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + " de BBDD ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScanProfileOrgUnitPluginDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperado el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + " con identificador: " + res.getId() + ".");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización del documento " + docId + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitPluginByStageAndDocs(java.lang.Long,
	 *      java.util.List)
	 */
	@Override
	public List<ScanProfileOrgUnitPluginDTO> getScanProfileOrgUnitPluginByStageAndDocs(Long scanProcessStageId, List<Long> docIds) throws DAOException {
		List<ScanProfileOrgUnitPluginDTO> res = new ArrayList<ScanProfileOrgUnitPluginDTO>();
		List<ScanProfileOrgUnitPlugin> entities;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los plugins configurados para el perfil de digitalización de los documentos {} en la fase {} de BBDD ...", docIds, scanProcessStageId);
			}
			TypedQuery<ScanProfileOrgUnitPlugin> query = entityManager.createQuery("SELECT spoup FROM ScanProfileOrgUnitPlugin spoup JOIN spoup.scanProcessStage sps JOIN spoup.orgScanProfile osp JOIN osp.orgScanProfileDocSpecs ospds JOIN ospds.documents docs WHERE sps.id = :scanProcessStageId AND docs.id IN :docIds", ScanProfileOrgUnitPlugin.class);
			query.setParameter("scanProcessStageId", scanProcessStageId);
			query.setParameter("docIds", docIds);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar los plugins configurados para el perfil de digitalización de los documentos {} en la fase {} de BBDD ...", docIds, scanProcessStageId);
			}
			entities = query.getResultList();

			res = MappingUtilities.fillScanProfileOrgUnitPluginListDto(entities);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperado los plugins configurados para el perfil de digitalización de los documentos {} en la fase {}. Número de plugins recuperados: {}.", docIds, scanProcessStageId, res.size());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los plugins configurados para el perfil de digitalización de los documentos " + docIds + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los plugins configurados para el perfil de digitalización de los documentos " + docIds + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los plugins configurados para el perfil de digitalización de los documentos " + docIds + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los plugins configurados para el perfil de digitalización de los documentos " + docIds + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los plugins configurados para el perfil de digitalización de los documentos " + docIds + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los plugins configurados para el perfil de digitalización de los documentos " + docIds + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para los plugins configurados para el perfil de digitalización de los documentos " + docIds + " en la fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(java.lang.Long,
	 *      java.lang.Long, java.lang.Long)
	 */
	@Override
	public ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(Long scanProfileId, Long orgUnitId, Long scanProcessStageId) throws DAOException {
		ScanProfileOrgUnitPluginDTO res = null;
		ScanProfileOrgUnitPlugin entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + " de BBDD ...");
			}
			TypedQuery<ScanProfileOrgUnitPlugin> query = entityManager.createQuery("SELECT spoup FROM ScanProfileOrgUnitPlugin spoup JOIN spoup.scanProcessStage sps JOIN spoup.orgScanProfile osp JOIN osp.scanProfile sp JOIN osp.orgUnit orgUnit WHERE sps.id = :scanProcessStageId AND sp.id = :scanProfileId AND orgUnit.id = :orgUnitId", ScanProfileOrgUnitPlugin.class);
			query.setParameter("scanProfileId", scanProfileId);
			query.setParameter("orgUnitId", orgUnitId);
			query.setParameter("scanProcessStageId", scanProcessStageId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + " de BBDD ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScanProfileOrgUnitPluginDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperado el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + " con identificador: " + res.getId() + ".");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para el perfil de digitalización " + scanProfileId + " en unidad organizativa " + orgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#hasScanProfileDocsInScanProccess(java.lang.Long)
	 */
	@Override
	public Boolean hasScanProfileDocsInScanProccess(Long scanProfileId) throws DAOException {
		Long numberOfDocs;
		Boolean res;
		String excMsg;

		try {
			if (scanProfileId != null && LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + " ...");
			}
			TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(sd.id) FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.scanProfile sp WHERE sd.digProcEndDate IS NULL AND sp.id = :scanProfileId", Long.class);
			query.setParameter("scanProfileId", scanProfileId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + " ...");
			}
			numberOfDocs = query.getSingleResult();

			res = numberOfDocs > 0;
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Perfil de digitalización  " + scanProfileId + " con " + numberOfDocs + " en proceso de digitalización.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para el perfil de digitalización " + scanProfileId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#createScanProfile(es.ricoh.webscan.model.dto.ScanProfileDTO,
	 *      java.lang.Long)
	 */
	@Override
	public Long createScanProfile(ScanProfileDTO scanProfile, Long scanProfileSpecId) throws DAOException {
		Long res = null;
		ScanProfileSpecification specification;
		ScanProfile entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando un nuevo perfil de digitalización, denominado " + scanProfile.getName() + " ...");

			LOG.debug("[WEBSCAN-MODEL] Obteniendo la especificación (" + scanProfileSpecId + ") del nuevo perfil de digitalización, denominado " + scanProfile.getName() + " ...");
		}
		specification = getScanProfileSpecificationEntity(scanProfileSpecId);

		entity = MappingUtilities.fillScanProfileEntity(scanProfile);
		entity.setSpecification(specification);

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Persistiendo el nuevo perfil de digitalización, denominado " + scanProfile.getName() + ", en BBDD ...");
			}
			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nuevo perfil de digitalización " + scanProfile.getName() + " creado, identificador: " + res + ".");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el perfil de digitalización " + scanProfile.getName() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el perfil de digitalización " + scanProfile.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el perfil de digitalización " + scanProfile.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el perfil de digitalización " + scanProfile.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el perfil de digitalización " + scanProfile.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#updateScanProfile(es.ricoh.webscan.model.dto.ScanProfileDTO)
	 */
	@Override
	public void updateScanProfile(ScanProfileDTO scanProfile) throws DAOException {
		ScanProfile entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando el perfil de digitalización " + scanProfile.getId() + " ...");
		}
		try {
			entity = getScanProfileEntity(scanProfile.getId());

			entity.setActive(scanProfile.getActive());
			entity.setName(scanProfile.getName());
			entity.setDescription(scanProfile.getDescription());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando el perfil de digitalización " + scanProfile.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Perfil de digitalización " + scanProfile.getId() + " actualizado en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el perfil de digitalización " + scanProfile.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el perfil de digitalización " + scanProfile.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el perfil de digitalización " + scanProfile.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el perfil de digitalización " + scanProfile.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#removeScanProfile(java.lang.Long)
	 */
	@Override
	public void removeScanProfile(Long scanProfileId) throws DAOException {
		ScanProfile entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Eliminando el perfil de digitalización " + scanProfileId + " ...");
		}
		try {
			entity = getScanProfileEntity(scanProfileId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Eliminando el perfil de digitalización " + scanProfileId + " de BBDD ...");
			}
			entityManager.remove(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Perfil de digitalización " + scanProfileId + " eliminado de BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el perfil de digitalización " + scanProfileId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el perfil de digitalización " + scanProfileId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el perfil de digitalización " + scanProfileId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el perfil de digitalización " + scanProfileId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitPluginsByScanProfile(java.lang.Long)
	 */
	@Override
	public List<ScanProfileOrgUnitPluginDTO> getScanProfileOrgUnitPluginsByScanProfile(Long scanProfileOrgUnitId) throws DAOException {
		int queryResultSize = 0;
		List<ScanProfileOrgUnitPlugin> plugins;
		List<ScanProfileOrgUnitPluginDTO> res = new ArrayList<ScanProfileOrgUnitPluginDTO>();
		ScanProfileOrgUnit entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los plugins configurados para la unidad organizativa " + "y el perfil de digitalización a partir del perfil " + scanProfileOrgUnitId + " ...");
			}
			entity = getScanProfileOrgUnitEntity(scanProfileOrgUnitId);
			plugins = entity.getOrgScanProfilePlugins();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando los plugins configurados para la unidad organizativa y el perfil de digitalización " + "obtenidos a partir del perfil " + scanProfileOrgUnitId + " ...");
			}
			if (plugins != null && !plugins.isEmpty()) {
				queryResultSize = plugins.size();

				for (ScanProfileOrgUnitPlugin spoup: plugins) {
					res.add(MappingUtilities.fillScanProfileOrgUnitPluginDto(spoup));
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Plugins recuperados para el perfil de digitalización configurado para la unidad organizativa " + scanProfileOrgUnitId + ": " + queryResultSize);
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los plugins para el perfil de digitalización configurado para la unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitPluginsByPlugin(java.lang.Long)
	 */
	@Override
	public List<ScanProfileOrgUnitPluginDTO> getScanProfileOrgUnitPluginsByPlugin(Long pluginId) throws DAOException {
		int queryResultSize = 0;
		List<ScanProfileOrgUnitPlugin> plugins;
		List<ScanProfileOrgUnitPluginDTO> res = new ArrayList<ScanProfileOrgUnitPluginDTO>();
		Plugin entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los plugins configurados para la unidad organizativa " + "y el perfil de digitalización a partir del plugin " + pluginId + " ...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getPluginEntity(pluginId, entityManager);
			plugins = entity.getOrgScanProfilePlugins();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando los plugins configurados para la unidad organizativa y el perfil de digitalización " + "obtenidos a partir del plugin " + pluginId + " ...");
			}
			if (plugins != null && !plugins.isEmpty()) {
				queryResultSize = plugins.size();

				for (ScanProfileOrgUnitPlugin spoup: plugins) {
					res.add(MappingUtilities.fillScanProfileOrgUnitPluginDto(spoup));
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Plugins recuperados para la unidad organizativa y el perfil de digitalización " + "obtenidos a partir del plugin " + pluginId + ": " + queryResultSize);
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los plugins para la unidad organizativa y el perfil de digitalización " + "obtenidos a partir del plugin " + pluginId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#setScanProfileOrgUnitPlugins(java.util.List,
	 *      java.lang.Long)
	 */
	@Override
	public void setScanProfileOrgUnitPlugins(List<PluginScanProcStageDTO> plugins, Long scanProfileOrgUnitId) throws DAOException {
		List<ScanProfileOrgUnitPlugin> newScanProfOrgUnitPlugins;
		List<ScanProfileOrgUnitPluginDTO> currentScanProfOrgUnitPlugins;
		ScanProfileOrgUnitPlugin scanProfOrgUnitPlugin;
		ScanProfileOrgUnit entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Estableciendo la nueva lista de plugins para el perfil de digitalización configurado para la unidad organizativa " + scanProfileOrgUnitId + " ...");
		}
		try {
			entity = getScanProfileOrgUnitEntity(scanProfileOrgUnitId);
			currentScanProfOrgUnitPlugins = MappingUtilities.fillScanProfileOrgUnitPluginListDto(entity.getOrgScanProfilePlugins());
			newScanProfOrgUnitPlugins = new ArrayList<ScanProfileOrgUnitPlugin>();

			// Existentes e inserciones
			if (plugins != null) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Se establecen los nuevos y actualizan los plugins ya configurados para la unidad organizativa el perfil de digitalización " + scanProfileOrgUnitId + " ...");
				}
				for (PluginScanProcStageDTO psps: plugins) {
					scanProfOrgUnitPlugin = updateOrCreateScanProfileOrgUnitPlugin(psps, currentScanProfOrgUnitPlugins, entity);

					newScanProfOrgUnitPlugins.add(scanProfOrgUnitPlugin);
				}
			}

			// Borrados
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Se eliminan los plugins no configurados para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnitId + " ...");
			}
			removeNotFoundScanProfileOrgUnitPlugins(currentScanProfOrgUnitPlugins, plugins, entity);

			entity.setOrgScanProfilePlugins(newScanProfOrgUnitPlugins);
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva lista de plugins establecida para para el perfil de digitalización configurado para la unidad organizativa " + scanProfileOrgUnitId + " (Número de plugins: " + newScanProfOrgUnitPlugins.size() + ").");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo la nueva lista de plugins para el perfil de digitalización configurado para la unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo la nueva lista de plugins para el perfil de digitalización configurado para la unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo la nueva lista de plugins para el perfil de digitalización configurado para la unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#updateScanProfileOrgUnitPlugins(java.util.List)
	 */
	@Override
	public void updateScanProfileOrgUnitPlugins(List<ScanProfileOrgUnitPluginDTO> scanProfileOrgUnitPlugins) throws DAOException {
		ScanProfileOrgUnitPlugin entity;
		String excMsg;

		LOG.debug("[WEBSCAN-MODEL] Se actualiza el estado de activación de plugins configurados para implementar fases para perfiles establecidos en unidades organizativas ...");

		try {
			for (ScanProfileOrgUnitPluginDTO spoup: scanProfileOrgUnitPlugins) {
				entity = entityManager.find(ScanProfileOrgUnitPlugin.class, spoup.getId());

				if (entity == null) {
					excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin configurado para implementar fase para un perfil establecido en unidad organizativa  " + spoup.getId() + " de BBDD. No existe la entidad.";
					LOG.error(excMsg);
					throw new DAOException(DAOException.CODE_901, excMsg);
				}

				entity.setActive(spoup.getActive());
				entityManager.merge(entity);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Estado de activación del plugin " + spoup.getPlugin() + " configurado para implementar la fase " + spoup.getScanProcessStage() + " del perfil establecido en unidad organizativa " + spoup.getOrgScanProfile() + " ...");
				}
			}
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Estado de activación actualizado de " + scanProfileOrgUnitPlugins.size() + " plugins configurados para implementar fases para perfiles establecidos en unidades organizativas ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el estado de activación de plugins configurados para implementar fases para perfiles establecidos en unidades organizativas: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el estado de activación de plugins configurados para implementar fases para perfiles establecidos en unidades organizativas: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el estado de activación de plugins configurados para implementar fases para perfiles establecidos en unidades organizativas: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * Añade o actualiza un plugin empleado para una entidad configuración de
	 * perfil de digitalización en unidad organizativa.
	 * 
	 * @param plugin
	 *            plugin a actualizar o registrar.
	 * @param scanProfileOrgUnitPlugins
	 *            Relación actual de plugins empleados por una entidad
	 *            configuración de perfil de digitalización en unidad
	 *            organizativa.
	 * @param scanProfileOrgUnit
	 *            Configuración de perfil de digitalización en unidad
	 *            organizativa.
	 * @return La entidad plugin empleada para una entidad configuración de
	 *         perfil de digitalización en unidad organizativa.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o perisitir la entidad en
	 *             BBDD.
	 */
	private ScanProfileOrgUnitPlugin updateOrCreateScanProfileOrgUnitPlugin(PluginScanProcStageDTO plugin, List<ScanProfileOrgUnitPluginDTO> scanProfileOrgUnitPlugins, ScanProfileOrgUnit scanProfileOrgUnit) throws DAOException {
		Boolean found = Boolean.FALSE;
		ScanProfileOrgUnitPlugin res;
		ScanProfileOrgUnitPluginDTO dto;
		String excMsg;

		res = null;
		try {
			for (Iterator<ScanProfileOrgUnitPluginDTO> it = scanProfileOrgUnitPlugins.iterator(); !found && it.hasNext();) {
				dto = it.next();
				if (plugin.getPlugin().equals(dto.getPlugin()) && plugin.getScanProcessStage().equals(dto.getScanProcessStage())) {
					// Se actualiza la configuración
					res = getScanProfileOrgUnitPlugin(scanProfileOrgUnit.getId(), plugin.getPlugin(), plugin.getScanProcessStage());
					res.setActive(plugin.getActive());
					entityManager.merge(res);
					found = Boolean.TRUE;
				}
			}

			if (!found) {
				// Se añade una nueva configuración
				res = new ScanProfileOrgUnitPlugin();
				res.setId(null);
				res.setActive(plugin.getActive());
				res.setOrgScanProfile(scanProfileOrgUnit);
				res.setPlugin(CommonUtilitiesDAOJdbcImpl.getPluginEntity(plugin.getPlugin(), entityManager));
				res.setScanProcessStage(CommonUtilitiesDAOJdbcImpl.getScanProcessStageEntity(plugin.getScanProcessStage(), entityManager));
				entityManager.persist(res);
			}

		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error registrando el plugin " + plugin.getPlugin() + " para la configuración de perfil en unidad organizativa " + scanProfileOrgUnit.getId() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error registrando el plugin " + plugin.getPlugin() + " para la configuración de perfil en unidad organizativa " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error registrando el plugin " + plugin.getPlugin() + " para la configuración de perfil en unidad organizativa " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error registrando el plugin " + plugin.getPlugin() + " para la configuración de perfil en unidad organizativa " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error registrando el plugin " + plugin.getPlugin() + " para la configuración de perfil en unidad organizativa " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}
		return res;
	}

	/**
	 * Elimina los plugins no presentes en la nueva relación de plugins
	 * empleados en una configuración de perfil de digitalización en unidad
	 * organizativa.
	 * 
	 * @param scanProfOrgUnitPlugins
	 *            Relación actual de plugins empleados en la configuración de
	 *            perfil de digitalización en unidad organizativa.
	 * @param newPlugins
	 *            Nueva relación de plugins empleados en la configuración de
	 *            perfil de digitalización en unidad organizativa.
	 * @param scanProfileOrgUnit
	 *            Entidad configuración de perfil de digitalización en unidad
	 *            organizativa.
	 * @throws DAOException
	 *             Si ocurre algún error al eliminar los plugins empleados en la
	 *             configuración de perfil de digitalización en unidad
	 *             organizativa de BBDD.
	 */
	private void removeNotFoundScanProfileOrgUnitPlugins(List<ScanProfileOrgUnitPluginDTO> scanProfOrgUnitPlugins, List<PluginScanProcStageDTO> newPlugins, ScanProfileOrgUnit scanProfileOrgUnit) throws DAOException {
		Boolean found;
		PluginScanProcStageDTO plugin;
		String excMsg;
		try {

			for (ScanProfileOrgUnitPluginDTO spoup: scanProfOrgUnitPlugins) {
				found = Boolean.FALSE;

				if (newPlugins != null) {
					for (Iterator<PluginScanProcStageDTO> it = newPlugins.iterator(); !found && it.hasNext();) {
						plugin = it.next();
						found = spoup.getPlugin().equals(plugin.getPlugin()) && spoup.getScanProcessStage().equals(plugin.getScanProcessStage());
					}
				}
				if (!found) {
					entityManager.remove(getScanProfileOrgUnitPlugin(spoup.getOrgScanProfile(), spoup.getPlugin(), spoup.getScanProcessStage()));
				}
			}

		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los plugins de la configuración de perfil en unidad organizativa " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error registrando los plugins de la configuración de perfil en unidad organizativa " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error registrando los plugins de la configuración de perfil en unidad organizativa " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitDocSpecsByScanProfile(java.lang.Long)
	 */
	@Override
	public List<ScanProfileOrgUnitDocSpecDTO> getScanProfileOrgUnitDocSpecsByScanProfile(Long scanProfileOrgUnitId) throws DAOException {
		int queryResultSize = 0;
		ScanProfileOrgUnit entity;
		List<ScanProfileOrgUnitDocSpec> scanProfileDocs;
		List<ScanProfileOrgUnitDocSpecDTO> res = new ArrayList<ScanProfileOrgUnitDocSpecDTO>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las especificaciones de documentos configuradas para la unidad organizativa " + "y el perfil de digitalización a partir del perfil " + scanProfileOrgUnitId + " ...");
			}
			entity = getScanProfileOrgUnitEntity(scanProfileOrgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando las especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + "obtenidos a partir del perfil " + scanProfileOrgUnitId + " ...");
			}
			scanProfileDocs = entity.getOrgScanProfileDocSpecs();

			if (scanProfileDocs != null && !scanProfileDocs.isEmpty()) {
				queryResultSize += scanProfileDocs.size();

				for (ScanProfileOrgUnitDocSpec spouds: scanProfileDocs) {
					res.add(MappingUtilities.fillScanProfileOrgUnitDocSpecDto(spouds));
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnitId + " recuperadas de BBDD: " + queryResultSize);
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las especificaciones de documentos configuradas para la unidad organizativa y " + "el perfil de digitalización " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitDocSpecsByDocSpec(java.lang.Long)
	 */
	@Override
	public List<ScanProfileOrgUnitDocSpecDTO> getScanProfileOrgUnitDocSpecsByDocSpec(Long docSpecId) throws DAOException {
		int queryResultSize = 0;
		DocumentSpecification entity;
		List<ScanProfileOrgUnitDocSpec> scanProfileDocs;
		List<ScanProfileOrgUnitDocSpecDTO> res = new ArrayList<ScanProfileOrgUnitDocSpecDTO>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las especificaciones de documentos configuradas para la unidad organizativa y el perfil de " + "digitalización a partir de la especificación de documento " + docSpecId + " ...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(docSpecId, entityManager);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando las especificaciones de documentos configuradas para la unidad organizativa y el perfil de " + "digitalización a partir de la especificación de documento " + docSpecId + " ...");
			}
			scanProfileDocs = entity.getOrgScanProfiles();

			if (scanProfileDocs != null && !scanProfileDocs.isEmpty()) {
				queryResultSize += scanProfileDocs.size();

				for (ScanProfileOrgUnitDocSpec spouds: scanProfileDocs) {
					res.add(MappingUtilities.fillScanProfileOrgUnitDocSpecDto(spouds));
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Especificaciones de documentos configuradas para la unidad organizativa y el perfil de " + "digitalización a partir de la especificación de documento " + docSpecId + " recuperadas de BBDD: " + queryResultSize);
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las especificaciones de documentos configuradas para la unidad organizativa y el perfil de " + "digitalización a partir de la especificación de documento " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocSpecificationByScanProfileOrgUnitId(java.lang.Long)
	 */
	@Override
	public Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getDocSpecificationsByScanProfileOrgUnitId(Long scanProfileOrgUnitId) throws DAOException {
		List<ScanProfileOrgUnitDocSpec> ddbbRes;
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> res = new HashMap<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " de BBDD ...");
			}
			TypedQuery<ScanProfileOrgUnitDocSpec> query = entityManager.createQuery("SELECT spouds FROM ScanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile osp WHERE osp.id = :scanProfileOrgUnitId", ScanProfileOrgUnitDocSpec.class);
			query.setParameter("scanProfileOrgUnitId", scanProfileOrgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para las definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " de BBDD ...");
			}
			ddbbRes = query.getResultList();

			LOG.debug("[WEBSCAN-MODEL] Recorriendo resultados ...");

			for (ScanProfileOrgUnitDocSpec spouds: ddbbRes) {
				res.put(MappingUtilities.fillDocumentSpecificationDto(spouds.getDocSpecification()), MappingUtilities.fillScanProfileOrgUnitDocSpecDto(spouds));
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " recuperadas de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getActiveDocSpecificationsByScanProfileOrgUnitId(java.lang.Long)
	 */
	@Override
	public Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getActiveDocSpecificationsByScanProfileOrgUnitId(Long scanProfileOrgUnitId) throws DAOException {
		List<ScanProfileOrgUnitDocSpec> ddbbRes;
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> res = new HashMap<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " de BBDD ...");
			}
			TypedQuery<ScanProfileOrgUnitDocSpec> query = entityManager.createQuery("SELECT spouds FROM ScanProfileOrgUnitDocSpec spouds JOIN spouds.docSpecification docSpec JOIN spouds.orgScanProfile osp WHERE osp.id = :scanProfileOrgUnitId AND spouds.active = TRUE AND docSpec.active = TRUE", ScanProfileOrgUnitDocSpec.class);
			query.setParameter("scanProfileOrgUnitId", scanProfileOrgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar las definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " de BBDD ...");
			}
			ddbbRes = query.getResultList();

			LOG.debug("[WEBSCAN-MODEL] Recorriendo resultados ...");

			for (ScanProfileOrgUnitDocSpec spouds: ddbbRes) {
				res.put(MappingUtilities.fillDocumentSpecificationDto(spouds.getDocSpecification()), MappingUtilities.fillScanProfileOrgUnitDocSpecDto(spouds));
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " recuperadas de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos activas asociadas a configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#hasScanProfileOrgUnitDocsInScanProccess(java.lang.Long)
	 */
	@Override
	public Boolean hasScanProfileOrgUnitDocsInScanProccess(Long scanProfileOrgUnitId) throws DAOException {
		Long numberOfDocs;
		Boolean res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " ...");
			}
			TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(sd.id) FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou WHERE sd.digProcEndDate IS NULL AND spou.id = :scanProfileOrgUnitId", Long.class);
			query.setParameter("scanProfileOrgUnitId", scanProfileOrgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " ...");
			}
			numberOfDocs = query.getSingleResult();

			res = numberOfDocs > 0;
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " con " + numberOfDocs + " en proceso de digitalización.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#hasScanProfileOrgUnitDocSpecDocsInScanProccess(java.lang.Long,
	 *      java.lang.Long)
	 */
	@Override
	public Boolean hasScanProfileOrgUnitDocSpecDocsInScanProccess(Long scanProfileOrgUnitId, Long docSpecId) throws DAOException {
		Long numberOfDocs;
		Boolean res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + "...");
			}
			TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(sd.id) FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.docSpecification ds JOIN spouds.orgScanProfile spou WHERE sd.digProcEndDate IS NULL AND spou.id = :scanProfileOrgUnitId AND ds.id = :docSpecId", Long.class);
			query.setParameter("scanProfileOrgUnitId", scanProfileOrgUnitId);
			query.setParameter("docSpecId", docSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + "...");
			}
			numberOfDocs = query.getSingleResult();

			res = numberOfDocs > 0;
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Configuración de definición de documentos " + docSpecId + " establecida en el perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " con " + numberOfDocs + " en proceso de digitalización.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " y definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#setScanProfileOrgUnitDocSpecs(java.util.Map,
	 *      java.lang.Long)
	 */
	@Override
	public void setScanProfileOrgUnitDocSpecs(Map<Long, Boolean> docSpecifications, Long scanProfileOrgUnitId) throws DAOException {
		ScanProfileOrgUnit entity;
		List<ScanProfileOrgUnitDocSpec> newOrgUnitDocSpecs,
				currentOrgUnitDocSpecs;
		Long docSpecId;
		ScanProfileOrgUnitDocSpec spouds;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Estableciendo la nueva lista de especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnitId + " ...");
		}
		try {
			entity = getScanProfileOrgUnitEntity(scanProfileOrgUnitId);
			currentOrgUnitDocSpecs = entity.getOrgScanProfileDocSpecs();
			newOrgUnitDocSpecs = new ArrayList<ScanProfileOrgUnitDocSpec>();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Se establecen las nuevas y actualizan las especificaciones de documentos ya configuradas para la unidad organizativa el perfil de digitalización " + scanProfileOrgUnitId + " ...");
			}
			if (docSpecifications != null) {
				// Existentes e inserciones
				for (Iterator<Long> itDto = docSpecifications.keySet().iterator(); itDto.hasNext();) {
					docSpecId = itDto.next();
					spouds = updateOrCreateScanProfileOrgUnitDocSpec(docSpecId, docSpecifications.get(docSpecId), currentOrgUnitDocSpecs, entity);
					newOrgUnitDocSpecs.add(spouds);
				}
			}

			// Borrados
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Se eliminan las especificaciones de documentos no configuradas para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnitId + " ...");
			}
			removeNotFoundScanProfileOrgUnitDocSpecs(currentOrgUnitDocSpecs, docSpecifications.keySet(), entity);

			entity.setOrgScanProfileDocSpecs(newOrgUnitDocSpecs);
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva lista de especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnitId + " (Número de definiciones de documentos: " + newOrgUnitDocSpecs.size() + ").");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo la nueva lista de especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo la nueva lista de especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnitId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo la nueva lista de especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * Añade o actualiza una definición de documentos establecida para una
	 * entidad configuración de perfil de digitalización en unidad organizativa.
	 * 
	 * @param docSpecId
	 *            Identificador de la definición de documentos establecida a
	 *            actualizar o registrar.
	 * @param active
	 *            Indicador de estado de activo de la una definición de
	 *            documentos establecida en la configuración de perfil de
	 *            digitalización en unidad organizativa.
	 * @param scanProfileOrgUnitDocSpecs
	 *            Relación actual de definiciones de documentos establecida en
	 *            una entidad configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @param scanProfileOrgUnit
	 *            Configuración de perfil de digitalización en unidad
	 *            organizativa.
	 * @return La entidad definición de documentos establecida en una entidad
	 *         configuración de perfil de digitalización en unidad organizativa.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o perisitir la entidad en
	 *             BBDD.
	 */
	private ScanProfileOrgUnitDocSpec updateOrCreateScanProfileOrgUnitDocSpec(Long docSpecId, Boolean active, List<ScanProfileOrgUnitDocSpec> scanProfileOrgUnitDocSpecs, ScanProfileOrgUnit scanProfileOrgUnit) throws DAOException {
		Boolean found = Boolean.FALSE;
		ScanProfileOrgUnitDocSpec res;
		String excMsg;

		res = null;
		try {
			for (Iterator<ScanProfileOrgUnitDocSpec> it = scanProfileOrgUnitDocSpecs.iterator(); !found && it.hasNext();) {
				res = it.next();
				if (docSpecId.equals(res.getDocSpecification().getId())) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("[WEBSCAN-MODEL] Se actualiza la definición de documento " + res.getDocSpecification().getName() + " configuración para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnit.getId() + " ...");
					}
					res.setActive(active);
					entityManager.merge(res);

					found = Boolean.TRUE;
				}
			}

			if (!found) {
				res = new ScanProfileOrgUnitDocSpec();
				res.setActive(active);
				res.setOrgScanProfile(scanProfileOrgUnit);
				res.setDocSpecification(CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(docSpecId, entityManager));
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Se añade la definición de documento " + res.getDocSpecification().getName() + " configuración para la unidad organizativa y el perfil de digitalización " + scanProfileOrgUnit.getId() + " ...");
				}
				entityManager.persist(res);
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la definición de documentos " + docSpecId + " en la configuración de perfil de digitalización en unidad organizariva " + scanProfileOrgUnit.getId() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar las definiciones de documentos " + docSpecId + " en la configuración de perfil de digitalización en unidad organizariva " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar las definiciones de documentos " + docSpecId + " en la configuración de perfil de digitalización en unidad organizariva " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar las definiciones de documentos " + docSpecId + " en la configuración de perfil de digitalización en unidad organizariva " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar las definiciones de documentos " + docSpecId + " en la configuración de perfil de digitalización en unidad organizariva " + scanProfileOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Elimina las definición de documentos no presentes en la nueva relación de
	 * definiciones de documentos establecida en una configuración de perfil de
	 * digitalización en unidad organizativa.
	 * 
	 * @param scanProfOrgUnitDocSpecs
	 *            Relación actual de definiciones de documentos establecida en
	 *            la configuración de perfil de digitalización en unidad
	 *            organizativa.
	 * @param newScanProfOrgUnitDocSpecIds
	 *            Nueva relación de identificadores de definiciones de
	 *            documentos establecidas en la configuración de perfil de
	 *            digitalización en unidad organizativa.
	 * @param scanProfOrgUnit
	 *            configuración de perfil en unidad organizativa.
	 * @throws DAOException
	 *             Si ocurre algún error al eliminar los empleados en la
	 *             configuración de perfil de digitalización en unidad
	 *             organizativa de BBDD.
	 */
	private void removeNotFoundScanProfileOrgUnitDocSpecs(List<ScanProfileOrgUnitDocSpec> scanProfOrgUnitDocSpecs, Collection<Long> newScanProfOrgUnitDocSpecIds, ScanProfileOrgUnit scanProfOrgUnit) throws DAOException {
		Boolean found;
		Long docSpecId;
		String excMsg;

		try {
			for (ScanProfileOrgUnitDocSpec spouds: scanProfOrgUnitDocSpecs) {
				found = Boolean.FALSE;

				if (newScanProfOrgUnitDocSpecIds != null) {
					for (Iterator<Long> it = newScanProfOrgUnitDocSpecIds.iterator(); !found && it.hasNext();) {
						docSpecId = it.next();
						found = spouds.getDocSpecification().getId().equals(docSpecId);
					}
				}
				if (!found) {
					entityManager.remove(spouds);
				}
			}

		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + scanProfOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + scanProfOrgUnit.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando especificaciones de documentos configuradas para la unidad organizativa y el perfil de digitalización " + scanProfOrgUnit.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitsByScanProfile(java.lang.Long)
	 */
	@Override
	public List<ScanProfileOrgUnitDTO> getScanProfileOrgUnitsByScanProfile(Long scanProfileId) throws DAOException {
		int queryResultSize = 0;
		List<ScanProfileOrgUnitDTO> res = new ArrayList<ScanProfileOrgUnitDTO>();
		List<ScanProfileOrgUnit> entities;
		ScanProfile entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando las configuraciones del perfil de digitalización " + scanProfileId + " por unidades de organizativas ...");
		}
		try {
			entity = getScanProfileEntity(scanProfileId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recueprar las configuraciones del perfil de digitalización " + scanProfileId + " por unidades de organizativas de BBDD ...");
			}
			entities = entity.getOrganizations();

			if (entities != null && !entities.isEmpty()) {
				queryResultSize = entities.size();
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Parseando las configuraciones del perfil de digitalización " + scanProfileId + " por unidades de organizativas recuperadas de BBDD ...");
				}
				for (ScanProfileOrgUnit spou: entities) {
					res.add(MappingUtilities.fillScanProfileOrgUnitDto(spou));
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + queryResultSize + " configuraciones del perfil de digitalización " + scanProfileId + "por unidades de organizativas recuperadas de BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las configuraciones del perfil de digitalización " + scanProfileId + " por unidades de organizativas en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las configuraciones del perfil de digitalización " + scanProfileId + " por unidades de organizativas en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las configuraciones del perfil de digitalización " + scanProfileId + " por unidades de organizativas en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las configuraciones del perfil de digitalización " + scanProfileId + " por unidades de organizativas en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnit(java.lang.Long)
	 */
	@Override
	public ScanProfileOrgUnitDTO getScanProfileOrgUnit(Long scanProfileOrgUnitId) throws DAOException {
		ScanProfileOrgUnit entity;
		ScanProfileOrgUnitDTO res = null;
		String excMsg;

		try {
			entity = getScanProfileOrgUnitEntity(scanProfileOrgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando la configuración de perfil de digitalización y unidad organizativa " + scanProfileOrgUnitId + " recuperada de BBDD ...");
			}
			res = MappingUtilities.fillScanProfileOrgUnitDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitByScanProfAndOrgUnit(java.lang.Long,
	 *      java.lang.Long)
	 */
	@Override
	public ScanProfileOrgUnitDTO getScanProfileOrgUnitByScanProfAndOrgUnit(Long scanProfileId, Long orgUnitId) throws DAOException {
		ScanProfileOrgUnitDTO res = null;
		ScanProfileOrgUnit entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + " de BBDD ...");
			}
			TypedQuery<ScanProfileOrgUnit> query = entityManager.createQuery("SELECT spou FROM ScanProfileOrgUnit spou JOIN spou.orgUnit orgUnit JOIN spou.scanProfile scanProfile WHERE orgUnit.id = :orgUnitId AND scanProfile.id = :scanProfileId", ScanProfileOrgUnit.class);
			query.setParameter("scanProfileId", scanProfileId);
			query.setParameter("orgUnitId", orgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScanProfileOrgUnitDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperado el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + " con identificador: " + res.getId() + ".");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " configurado para la unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#getScanProfileOrgUnitByScanProfileNameAndOrgUnitExtId(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public ScanProfileOrgUnitDTO getScanProfileOrgUnitByScanProfileNameAndOrgUnitExtId(String scanProfileName, String orgUnitExtId) throws DAOException {
		ScanProfileOrgUnitDTO res = null;
		ScanProfileOrgUnit entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + " de BBDD ...");
			}
			TypedQuery<ScanProfileOrgUnit> query = entityManager.createQuery("SELECT spou FROM ScanProfileOrgUnit spou JOIN spou.orgUnit orgUnit JOIN spou.scanProfile scanProfile WHERE orgUnit.externalId = :orgUnitExtId AND scanProfile.name = :scanProfileName", ScanProfileOrgUnit.class);
			query.setParameter("scanProfileName", scanProfileName);
			query.setParameter("orgUnitExtId", orgUnitExtId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScanProfileOrgUnitDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperado el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + " con identificador: " + res.getId() + ".");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileName + " configurado para la unidad organizativa " + orgUnitExtId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.ScanProfileDAO#createScanProfileOrgUnit(es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO)
	 */
	@Override
	public Long createScanProfileOrgUnit(ScanProfileOrgUnitDTO scanProfileOrgUnit) throws DAOException {
		Long res = null;
		ScanProfileOrgUnit entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando una nueva configuración de perfil de digitalización en unidad organizativa (Perfil: " + scanProfileOrgUnit.getScanProfile() + ", UO: " + scanProfileOrgUnit.getOrgUnit() + ") ...");
		}
		try {
			entity = MappingUtilities.fillScanProfileOrgUnitEntity(scanProfileOrgUnit);
			entity.setScanProfile(getScanProfileEntity(scanProfileOrgUnit.getScanProfile()));
			entity.setOrgUnit(CommonUtilitiesDAOJdbcImpl.getOrganizationUnitEntity(scanProfileOrgUnit.getOrgUnit(), entityManager));

			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva configuración de perfil de digitalización en unidad organizativa creada, identificador: " + res + ".");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva configuración de perfil de digitalización en unidad organizativa (Perfil: " + scanProfileOrgUnit.getScanProfile() + ", UO: " + scanProfileOrgUnit.getOrgUnit() + "), la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva configuración de perfil de digitalización en unidad organizativa en BBDD (Perfil: " + scanProfileOrgUnit.getScanProfile() + ", UO: " + scanProfileOrgUnit.getOrgUnit() + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva configuración de perfil de digitalización en unidad organizativa en BBDD (Perfil: " + scanProfileOrgUnit.getScanProfile() + ", UO: " + scanProfileOrgUnit.getOrgUnit() + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva configuración de perfil de digitalización en unidad organizativa en BBDD (Perfil: " + scanProfileOrgUnit.getScanProfile() + ", UO: " + scanProfileOrgUnit.getOrgUnit() + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva configuración de perfil de digitalización en unidad organizativa en BBDD (Perfil: " + scanProfileOrgUnit.getScanProfile() + ", UO: " + scanProfileOrgUnit.getOrgUnit() + "): " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	@Override
	public void removeScanProfileOrgUnits(List<Long> scanProfileOrgIds) throws DAOException {
		String excMsg;

		LOG.debug("[WEBSCAN-MODEL] Eliminando configuraciones de perfil de digitalización en unidad organizativa de BBDD ...");

		try {
			Query query = entityManager.createQuery("DELETE FROM ScanProfileOrgUnit spou WHERE spou.id IN :scanProfileOrgIds");
			query.setParameter("scanProfileOrgIds", scanProfileOrgIds);
			int res = query.executeUpdate();

			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res + " configuraciones de perfil de digitalización en unidad organizativa eliminadas de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando configuraciones de perfil de digitalización en unidad eliminadas de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando configuraciones de perfil de digitalización en unidad eliminadas de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando configuraciones de perfil de digitalización en unidad eliminadas de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando configuraciones de perfil de digitalización en unidad eliminadas de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * Recupera de BBDD una entidad definición de perfil de digitalización a
	 * partir de su identificador.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de definición de perfil de digitalización.
	 * @return Entidad de BBDD definición de perfil de digitalización.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private ScanProfileSpecification getScanProfileSpecificationEntity(Long scanProfileSpecId) throws DAOException {
		ScanProfileSpecification res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de perfil de digitalización " + scanProfileSpecId + " de BBDD ...");
			}
			res = entityManager.find(ScanProfileSpecification.class, scanProfileSpecId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización " + scanProfileSpecId + " en BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de perfil de digitalización " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización " + scanProfileSpecId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de perfil de digitalización " + scanProfileSpecId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad perfil de digitalización a partir de su
	 * identificador.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @return Entidad de BBDD perfil de digitalización.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private ScanProfile getScanProfileEntity(Long scanProfileId) throws DAOException {
		ScanProfile res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el perfil de digitalización " + scanProfileId + " de BBDD ...");
			}
			res = entityManager.find(ScanProfile.class, scanProfileId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " en BBDD. No existe la entidad.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Perfil de digitalización " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el perfil de digitalización " + scanProfileId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad configuración de perfil de digitalización en
	 * unidad organizativa a partir de su identificador.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Entidad de BBDD configuración de perfil de digitalización en
	 *         unidad organizativa.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private ScanProfileOrgUnit getScanProfileOrgUnitEntity(Long scanProfileOrgUnitId) throws DAOException {
		ScanProfileOrgUnit res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " de BBDD ...");
			}
			res = entityManager.find(ScanProfileOrgUnit.class, scanProfileOrgUnitId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " de BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la configuración de perfil de digitalización en unidad organizativa " + scanProfileOrgUnitId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Obtiene una entidad plugin empleando en fase de proceso de digitalización
	 * para configuración de perfil de digitalización en unidad organizativa, a
	 * partir de los identificadores de la configuración de perfil en unidad
	 * organizativa, plugin y fase de proceso de digitalización.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificadores de la configuración de perfil de
	 *            digitalización en unidad organizativa.
	 * @param pluginId
	 *            Identificador de plugin.
	 * @param scanProcessStageId
	 *            Identificador de fase de proceso de digitalización.
	 * @return Entidad plugin empleando en fase de proceso de digitalización
	 *         para configuración de perfil de digitalización en unidad
	 *         organizativa.
	 * @throws DAOException
	 *             Si no existe la entidad en base de datos.
	 */
	private ScanProfileOrgUnitPlugin getScanProfileOrgUnitPlugin(Long scanProfileOrgUnitId, Long pluginId, Long scanProcessStageId) throws DAOException {
		ScanProfileOrgUnitPlugin res = null;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + " de BBDD ...");
			}
			TypedQuery<ScanProfileOrgUnitPlugin> query = entityManager.createQuery("SELECT spoup FROM ScanProfileOrgUnitPlugin spoup JOIN spoup.orgScanProfile osp JOIN spoup.plugin plugin JOIN spoup.scanProcessStage sps WHERE osp.id = :scanProfileOrgUnitId AND plugin.id = :pluginId AND sps.id = :scanProcessStageId", ScanProfileOrgUnitPlugin.class);
			query.setParameter("scanProfileOrgUnitId", scanProfileOrgUnitId);
			query.setParameter("pluginId", pluginId);
			query.setParameter("scanProcessStageId", scanProcessStageId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + " ...");
			}
			res = query.getSingleResult();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperado el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + res.getId() + ".");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el plugin " + pluginId + " empleado en la configuración " + scanProfileOrgUnitId + " y fase " + scanProcessStageId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

}
