/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.MetadataSpecification.java.</p>
 * <b>Descripción:</b><p> Entidad JPA definición de metadato.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA definición de metadato.
 * <p>
 * Clase MetadataSpecification.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "METADATA_SPEC", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME", "DOC_TYPE_SPEC_ID" }) })
public class MetadataSpecification implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "METADATA_SPEC_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "METADATA_SPEC_ID_SEQ", sequenceName = "METADATA_SPEC_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Atributo DESCRIPTION de la entidad.
	 */
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * Atributo DEFAULT_VALUE de la entidad.
	 */
	@Column(name = "DEFAULT_VALUE")
	private String defaultValue;

	/**
	 * Atributo MANDATORY de la entidad.
	 */
	@Column(name = "MANDATORY")
	private Boolean mandatory = Boolean.FALSE;

	/**
	 * Atributo EDITABLE de la entidad.
	 */
	@Column(name = "EDITABLE")
	private Boolean editable = Boolean.TRUE;

	/**
	 * Atributo INC_IN_BATCH_SEP de la entidad.
	 */
	@Column(name = "INC_IN_BATCH_SEP")
	private Boolean includeInBatchSeprator = Boolean.FALSE;

	/**
	 * Atributo IS_BATCH_ID de la entidad.
	 */
	@Column(name = "IS_BATCH_ID")
	private Boolean isBatchId = Boolean.FALSE;

	/**
	 * Atributo SRC_VALUES de la entidad.
	 */
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "SRC_VALUES", nullable = true)
	private byte[ ] sourceValues;

	/**
	 * Entidad especificación de documento a la que pertenece la definición de
	 * metadato.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.DocumentSpecification.class)
	@JoinColumn(name = "DOC_TYPE_SPEC_ID")
	private DocumentSpecification docSpecification;

	/**
	 * Relación de metadatos de documentos especificados mediante esta
	 * definición de metadato.
	 */
	@OneToMany(mappedBy = "specification", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.Metadata.class)
	private List<Metadata> metadataCollection = new ArrayList<Metadata>();

	/**
	 * Constructor sin argumentos.
	 */
	public MetadataSpecification() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo DESCRIPTION de la entidad.
	 * 
	 * @return el atributo DESCRIPTION de la entidad.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para el atributo DESCRIPTION de la entidad.
	 * 
	 * @param aDescription
	 *            nuevo valor para el atributo DESCRIPTION de la entidad.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene el atributo DEFAULT_VALUE de la entidad.
	 * 
	 * @return el atributo DEFAULT_VALUE de la entidad.
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Establece un nuevo valor para el atributo DEFAULT_VALUE de la entidad.
	 * 
	 * @param aDefaultValue
	 *            nuevo valor para el atributo DEFAULT_VALUE de la entidad.
	 */
	public void setDefaultValue(String aDefaultValue) {
		this.defaultValue = aDefaultValue;
	}

	/**
	 * Obtiene el atributo MANDATORY de la entidad.
	 * 
	 * @return el atributo MANDATORY de la entidad.
	 */
	public Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * Establece un nuevo valor para el atributo MANDATORY de la entidad.
	 * 
	 * @param mandatoryParam
	 *            nuevo valor para el atributo MANDATORY de la entidad.
	 */
	public void setMandatory(Boolean mandatoryParam) {
		this.mandatory = mandatoryParam;
	}

	/**
	 * Obtiene el atributo EDITABLE de la entidad.
	 * 
	 * @return el atributo EDITABLE de la entidad.
	 */
	public Boolean getEditable() {
		return editable;
	}

	/**
	 * Establece un nuevo valor para el atributo EDITABLE de la entidad.
	 * 
	 * @param editableParam
	 *            nuevo valor para el atributo EDITABLE de la entidad.
	 */
	public void setEditable(Boolean editableParam) {
		this.editable = editableParam;
	}

	/**
	 * Obtiene el atributo INC_IN_BATCH_SEP de la entidad.
	 * 
	 * @return el atributo INC_IN_BATCH_SEP de la entidad.
	 */
	public Boolean getIncludeInBatchSeprator() {
		return includeInBatchSeprator;
	}

	/**
	 * Establece un nuevo valor para el atributo INC_IN_BATCH_SEP de la entidad.
	 * 
	 * @param includeInBatchSepratorParam
	 *            nuevo valor para el atributo INC_IN_BATCH_SEP de la entidad.
	 */
	public void setIncludeInBatchSeprator(Boolean includeInBatchSepratorParam) {
		this.includeInBatchSeprator = includeInBatchSepratorParam;
	}

	/**
	 * Obtiene el atributo IS_BATCH_ID de la entidad.
	 * 
	 * @return el atributo IS_BATCH_ID de la entidad.
	 */
	public Boolean getIsBatchId() {
		return isBatchId;
	}

	/**
	 * Establece un nuevo valor para el atributo IS_BATCH_ID de la entidad.
	 * 
	 * @param isBatchIdParam
	 *            nuevo valor para el atributo IS_BATCH_ID de la entidad.
	 */
	public void setIsBatchId(Boolean isBatchIdParam) {
		this.isBatchId = isBatchIdParam;
	}

	/**
	 * Obtiene el atributo SRC_VALUES de la entidad.
	 * 
	 * @return el atributo SRC_VALUES de la entidad.
	 */
	public byte[ ] getSourceValues() {
		return sourceValues;
	}

	/**
	 * Establece un nuevo valor para el atributo SRC_VALUES de la entidad.
	 * 
	 * @param anySourceValues
	 *            nuevo valor para el atributo SRC_VALUES de la entidad.
	 */
	public void setSourceValues(byte[ ] anySourceValues) {
		this.sourceValues = anySourceValues;
	}

	/**
	 * Obtiene la especificación de documento a la que pertenece la definición
	 * de metadato.
	 * 
	 * @return la especificación de documento a la que pertenece la definición
	 *         de metadato.
	 */
	public DocumentSpecification getDocSpecification() {
		return docSpecification;
	}

	/**
	 * Establece una nueva especificación de documento a la que pertenece la
	 * definición de metadato.
	 * 
	 * @param aDocSpecification
	 *            Nueva especificación de documento a la que pertenece la
	 *            definición de metadato.
	 */
	public void setDocSpecification(DocumentSpecification aDocSpecification) {
		this.docSpecification = aDocSpecification;
	}

	/**
	 * Obtiene la relación de metadatos de documentos especificados mediante
	 * esta definición de metadato.
	 * 
	 * @return la relación de metadatos de documentos especificados mediante
	 *         esta definición de metadato.
	 */
	public List<Metadata> getMetadataCollection() {
		return metadataCollection;
	}

	/**
	 * Establece una nueva relación de metadatos de documentos especificados
	 * mediante esta definición de metadato.
	 * 
	 * @param aMetadataCollection
	 *            Nueva relación de metadatos de documentos especificados
	 *            mediante esta definición de metadato.
	 */
	public void setMetadataCollection(List<Metadata> aMetadataCollection) {
		this.metadataCollection.clear();
		if (aMetadataCollection != null && !aMetadataCollection.isEmpty()) {
			this.metadataCollection.addAll(aMetadataCollection);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docSpecification == null) ? 0 : docSpecification.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MetadataSpecification other = (MetadataSpecification) obj;
		if (docSpecification == null) {
			if (other.docSpecification != null)
				return false;
		} else if (!docSpecification.equals(other.docSpecification))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
