/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.MappingUtilities.java.</p>
 * <b>Descripción:</b><p> Clase de utilidades para la transformación de entidades JPA, pertenecientes
 * al modelos base de Webscan, en objetos DTO, y viceversa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.domain.AuditEvent;
import es.ricoh.webscan.model.domain.AuditOperation;
import es.ricoh.webscan.model.domain.AuditScannedDoc;
import es.ricoh.webscan.model.domain.Batch;
import es.ricoh.webscan.model.domain.DocumentSpecification;
import es.ricoh.webscan.model.domain.Metadata;
import es.ricoh.webscan.model.domain.MetadataSpecification;
import es.ricoh.webscan.model.domain.OrganizationUnit;
import es.ricoh.webscan.model.domain.Parameter;
import es.ricoh.webscan.model.domain.ParameterSpecification;
import es.ricoh.webscan.model.domain.ParameterType;
import es.ricoh.webscan.model.domain.Plugin;
import es.ricoh.webscan.model.domain.PluginSpecification;
import es.ricoh.webscan.model.domain.ScanProcessStage;
import es.ricoh.webscan.model.domain.ScanProfile;
import es.ricoh.webscan.model.domain.ScanProfileOrgUnit;
import es.ricoh.webscan.model.domain.ScanProfileOrgUnitDocSpec;
import es.ricoh.webscan.model.domain.ScanProfileOrgUnitPlugin;
import es.ricoh.webscan.model.domain.ScanProfileSpecScanProcStage;
import es.ricoh.webscan.model.domain.ScanProfileSpecification;
import es.ricoh.webscan.model.domain.ScannedDocLog;
import es.ricoh.webscan.model.domain.ScannedDocument;
import es.ricoh.webscan.utilities.MimeType;

/**
 * Clase de utilidades para la transformación de entidades JPA, pertenecientes
 * al modelo base de Webscan, en objetos DTO, y viceversa.
 * <p>
 * Clase MappingUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class MappingUtilities {

	/**
	 * Constructor method for the class MappingUtilities.java.
	 */
	private MappingUtilities() {
	}

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MappingUtilities.class);

	/**
	 * Juego de caracteres empleado por defecto para la conversión de cadena a
	 * array de bytes y viceversa.
	 */
	private static final String DEFAULT_CHARSET = "UTF-8";

	/**
	 * Transforma una entidad de BBDD fase de proceso de digitalización en un
	 * objeto ScanProcessStageDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD fase del proceso de digitalización.
	 * @return Objeto ScanProcessStageDTO.
	 */
	public static ScanProcessStageDTO fillScanProcessStageDto(ScanProcessStage entity) {
		ScanProcessStageDTO res = new ScanProcessStageDTO();

		if (entity != null) {
			// Datos globales
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setDescription(entity.getDescription());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ScanProcessStageDTO en una entidad de
	 * BBDD fase del proceso de digitalización.
	 * 
	 * @param dto
	 *            objeto ScanProcessStageDTO.
	 * @return Objeto entidad de BBDD fase del proceso de digitalización.
	 */
	public static ScanProcessStage fillScanProcessStageEntity(ScanProcessStageDTO dto) {
		ScanProcessStage res = new ScanProcessStage();

		if (dto != null) {
			// Datos globales
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setDescription(dto.getDescription());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD fase de proceso de
	 * digitalización en una lista de objetos ScanProcessStageDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD fase de proceso de digitalización .
	 * @return lista de objetos ScanProcessStageDTO.
	 */
	public static List<ScanProcessStageDTO> fillScanProcessStageListDto(List<ScanProcessStage> entities) {
		int queryResultSize = 0;
		List<ScanProcessStageDTO> res = new ArrayList<ScanProcessStageDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (ScanProcessStage sps: entities) {
				res.add(fillScanProcessStageDto(sps));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Fases de proceso de digitalización parseadas:{0} ", queryResultSize);

		return res;
	}

	/**
	 * Transforma una entidad de BBDD configuración de fase de proceso de
	 * digitalización en especificación de perfiles de digitalización en un
	 * objeto ScanProfileSpecScanProcStageDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD configuración de fase de proceso de
	 *            digitalización en especificación de perfiles de
	 *            digitalización.
	 * @return Objeto ScanProfileSpecScanProcStageDTO.
	 */
	public static ScanProfileSpecScanProcStageDTO fillScanProfileSpecScanProcStageDto(ScanProfileSpecScanProcStage entity) {
		ScanProfileSpecScanProcStageDTO res = new ScanProfileSpecScanProcStageDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setMandatory(entity.getMandatory());
			res.setOrder(entity.getOrder());
			res.setRenewable(entity.getRenewable());
			res.setScanProcessStage(entity.getScanProcessStage().getName());
			res.setScanProcessStageId(entity.getScanProcessStage().getId());
			res.setScanProfileSpecification(entity.getScanProfileSpecification().getName());
			res.setScanProfileSpecificationId(entity.getScanProfileSpecification().getId());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ScanProfileSpecScanProcStageDTO en una
	 * entidad de BBDD configuración de fase de proceso de digitalización en
	 * especificación de perfiles de digitalización.
	 * 
	 * @param dto
	 *            objeto ScanProfileSpecScanProcStageDTO.
	 * @return Objeto entidad de BBDD configuración de fase de proceso de
	 *         digitalización en especificación de perfiles de digitalización.
	 */
	public static ScanProfileSpecScanProcStage fillScanProfileSpecScanProcStageEntity(ScanProfileSpecScanProcStageDTO dto) {
		ScanProfileSpecScanProcStage res = new ScanProfileSpecScanProcStage();

		if (dto != null) {
			res.setId(dto.getId());
			res.setMandatory(dto.getMandatory());
			res.setOrder(dto.getOrder());
			res.setRenewable(dto.getRenewable());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD configuración de fase de
	 * proceso de digitalización en especificación de perfiles de digitalización
	 * en una lista de objetos ScanProfileSpecScanProcStageDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD configuración de fase de proceso de
	 *            digitalización en especificación de perfiles de
	 *            digitalización.
	 * @return lista de objetos ScanProfileSpecScanProcStageDTO.
	 */
	public static List<ScanProfileSpecScanProcStageDTO> fillScanProfileSpecScanProcStageListDto(List<ScanProfileSpecScanProcStage> entities) {
		int queryResultSize = 0;
		List<ScanProfileSpecScanProcStageDTO> res = new ArrayList<ScanProfileSpecScanProcStageDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (ScanProfileSpecScanProcStage spssps: entities) {
				res.add(fillScanProfileSpecScanProcStageDto(spssps));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Configuraciones de fase de proceso de digitalización en especificación de perfiles de digitalización parseadas: {0}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD especificación de perfiles de
	 * digitalización en un objeto ScanProfileSpecificationDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD especificación de perfiles de digitalización.
	 * @return Objeto ScanProfileSpecificationDTO.
	 */
	public static ScanProfileSpecificationDTO fillScanProfileSpecificationDto(ScanProfileSpecification entity) {
		ScanProfileSpecificationDTO res = new ScanProfileSpecificationDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setDescription(entity.getDescription());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ScanProfileSpecificationDTO en una
	 * entidad de BBDD especificación de perfiles de digitalización.
	 * 
	 * @param dto
	 *            objeto ScanProfileSpecificationDTO.
	 * @return Objeto entidad de BBDD especificación de perfiles de
	 *         digitalización.
	 */
	public static ScanProfileSpecification fillScanProfileSpecificationEntity(ScanProfileSpecificationDTO dto) {
		ScanProfileSpecification res = new ScanProfileSpecification();

		if (dto != null) {
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setDescription(dto.getDescription());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD especificación de perfiles de
	 * digitalización en una lista de objetos ScanProfileSpecificationDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD especificación de perfiles de
	 *            digitalización.
	 * @return lista de objetos ScanProfileSpecificationDTO.
	 */
	public static List<ScanProfileSpecificationDTO> fillScanProfileSpecificationListDto(List<ScanProfileSpecification> entities) {
		int queryResultSize = 0;
		List<ScanProfileSpecificationDTO> res = new ArrayList<ScanProfileSpecificationDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (ScanProfileSpecification sps: entities) {
				res.add(fillScanProfileSpecificationDto(sps));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Especificaciones de perfil de digitalización parseadas: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD perfil de digitalización en un objeto
	 * ScanProfileDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD perfil de digitalización.
	 * @return Objeto ScanProfileDTO.
	 */
	public static ScanProfileDTO fillScanProfileDto(ScanProfile entity) {
		ScanProfileDTO res = new ScanProfileDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setDescription(entity.getDescription());
			res.setActive(entity.getActive());
			res.setSpecification(entity.getSpecification().getId());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ScanProfileDTO en una entidad de BBDD
	 * perfil de digitalización.
	 * 
	 * @param dto
	 *            objeto ScanProfileDTO.
	 * @return Objeto entidad de BBDD perfil de digitalización.
	 */
	public static ScanProfile fillScanProfileEntity(ScanProfileDTO dto) {
		ScanProfile res = new ScanProfile();

		if (dto != null) {
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setDescription(dto.getDescription());
			res.setActive(dto.getActive());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD perfil de digitalización en una
	 * lista de objetos ScanProfileDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD perfil de digitalización.
	 * @return lista de objetos ScanProfileDTO.
	 */
	public static List<ScanProfileDTO> fillScanProfileListDto(List<ScanProfile> entities) {
		int queryResultSize = 0;
		List<ScanProfileDTO> res = new ArrayList<ScanProfileDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (ScanProfile sp: entities) {
				res.add(fillScanProfileDto(sp));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Especificaciones de perfil de digitalización parseadas: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD configuración de especificación de
	 * documento en perfil de digitalización y unidad organizativa en un objeto
	 * ScanProfileOrgUnitDocSpecDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD configuración de especificación de documento
	 *            en perfil de digitalización y unidad organizativa.
	 * @return Objeto ScanProfileOrgUnitDocSpecDTO.
	 */
	public static ScanProfileOrgUnitDocSpecDTO fillScanProfileOrgUnitDocSpecDto(ScanProfileOrgUnitDocSpec entity) {
		ScanProfileOrgUnitDocSpecDTO res = new ScanProfileOrgUnitDocSpecDTO();

		if (entity != null) {
			// Datos generales
			res.setId(entity.getId());
			res.setOrgScanProfile(entity.getOrgScanProfile().getId());
			res.setDocSpecification(entity.getDocSpecification().getId());
			res.setActive(entity.getActive());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ScanProfileOrgUnitDocSpecDTO en una
	 * entidad de BBDD configuración de especificación de documento en perfil de
	 * digitalización y unidad organizativa.
	 * 
	 * @param dto
	 *            objeto ScanProfileOrgUnitDocSpecDTO.
	 * @return Objeto entidad de BBDD configuración de especificación de
	 *         documento en perfil de digitalización y unidad organizativa.
	 */
	public static ScanProfileOrgUnitDocSpec fillScanProfileOrgUnitDocSpectEntity(ScanProfileOrgUnitDocSpecDTO dto) {
		ScanProfileOrgUnitDocSpec res = new ScanProfileOrgUnitDocSpec();

		if (dto != null) {
			res.setId(dto.getId());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto BatchDTO en una
	 * entidad de BBDD procesos de digitalización.
	 * 
	 * @param dto
	 *            objeto BatchDTO.
	 * @return Objeto entidad de BBDD procesos de digitalización.
	 */
	public static Batch fillBatchEntity(BatchDTO dto) {
		Batch res = new Batch();

		if (dto != null) {
			res.setId(dto.getId());
			res.setBatchNameId(dto.getBatchNameId());
			res.setBatchReqId(dto.getBatchReqId());
			res.setFunctionalOrgs(dto.getFunctionalOrgs());
			res.setStartDate(dto.getStartDate());
			res.setUsername(dto.getUsername());
		}

		return res;
	}
	
	/**
	 * Transforma una entidad de BBDD especificación de plugin en un objeto
	 * PluginSpecificationDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD especificación de plugin.
	 * @return Objeto PluginSpecificationDTO.
	 */
	public static PluginSpecificationDTO fillPluginSpecificationDto(PluginSpecification entity) {
		PluginSpecificationDTO res = new PluginSpecificationDTO();

		if (entity != null) {
			// Datos globales
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setDescription(entity.getDescription());
			res.setActive(entity.getActive());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto PluginSpecificationDTO en una entidad
	 * de BBDD especificación de plugin.
	 * 
	 * @param dto
	 *            objeto PluginSpecificationDTO.
	 * @return Objeto entidad de BBDD especificación de plugin.
	 */
	public static PluginSpecification fillPluginSpecificationEntity(PluginSpecificationDTO dto) {
		PluginSpecification res = new PluginSpecification();

		if (dto != null) {
			// Datos globales
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setDescription(dto.getDescription());
			res.setActive(dto.getActive());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD definición de plugins en una
	 * lista de objetos PluginSpecificationDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD definición de plugins.
	 * @return lista de objetos PluginSpecificationDTO.
	 */
	public static List<PluginSpecificationDTO> fillPluginSpecificationListDto(List<PluginSpecification> entities) {
		int queryResultSize = 0;
		List<PluginSpecificationDTO> res = new ArrayList<PluginSpecificationDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (PluginSpecification ps: entities) {
				res.add(fillPluginSpecificationDto(ps));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Definiciones de plugins parseadas: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD plugin en un objeto PluginDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD plugin.
	 * @return Objeto PluginDTO.
	 */
	public static PluginDTO fillPluginDto(Plugin entity) {
		PluginDTO res = new PluginDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setDescription(entity.getDescription());
			res.setActive(entity.getActive());
			res.setSpecification(entity.getSpecification().getName());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto PluginDTO en una entidad de BBDD
	 * plugin.
	 * 
	 * @param dto
	 *            objeto PluginDTO.
	 * @return Objeto entidad de BBDD plugin.
	 */
	public static Plugin fillPluginEntity(PluginDTO dto) {
		Plugin res = new Plugin();

		if (dto != null) {
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setDescription(dto.getDescription());
			res.setActive(dto.getActive());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD plugin en una lista de objetos
	 * PluginDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD plugin.
	 * @return lista de objetos PluginDTO.
	 */
	public static List<PluginDTO> fillPluginListDto(List<Plugin> entities) {
		int queryResultSize = 0;
		List<PluginDTO> res = new ArrayList<PluginDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (Plugin plugin: entities) {
				res.add(fillPluginDto(plugin));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Plugins parseados: {0}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD definición de parámetro en un objeto
	 * ParameterSpecificationDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD definición de parámetro.
	 * @return Objeto ParameterSpecificationDTO.
	 */
	public static ParameterSpecificationDTO fillParameterSpecificationDto(ParameterSpecification entity) {
		ParameterSpecificationDTO res = new ParameterSpecificationDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setDescription(entity.getDescription());
			res.setMandatory(entity.getMandatory());
			res.setPluginSpecificationId(entity.getPluginSpecification().getId());
			res.setTypeId(entity.getType().getId());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ParameterSpecificationDTO en una
	 * entidad de BBDD definición de parámetro.
	 * 
	 * @param dto
	 *            objeto ParameterSpecificationDTO.
	 * @return Objeto entidad de BBDD definición de parámetro.
	 */
	public static ParameterSpecification fillParameterSpecificationEntity(ParameterSpecificationDTO dto) {
		ParameterSpecification res = new ParameterSpecification();

		if (dto != null) {
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setDescription(dto.getDescription());
			res.setMandatory(dto.getMandatory());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD definición de parámetros en una
	 * lista de objetos ParameterSpecificationDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD definición de parámetros.
	 * @return lista de objetos ParameterSpecificationDTO.
	 */
	public static List<ParameterSpecificationDTO> fillParameterSpecificationListDto(List<ParameterSpecification> entities) {
		int queryResultSize = 0;
		List<ParameterSpecificationDTO> res = new ArrayList<ParameterSpecificationDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (ParameterSpecification ps: entities) {
				res.add(fillParameterSpecificationDto(ps));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Definiciones de parámetros parseadas: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD parámetro en un objeto ParameterDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD parámetro.
	 * @return Objeto ParameterDTO.
	 */
	public static ParameterDTO fillParameterDto(Parameter entity) {
		ParameterDTO res = new ParameterDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setSpecificationName(entity.getSpecification().getName());
			res.setValue(entity.getValue());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ParameterDTO en una entidad de BBDD
	 * parámetro.
	 * 
	 * @param dto
	 *            objeto ParameterDTO.
	 * @return Objeto entidad de BBDD parámetro.
	 */
	public static Parameter fillParameterEntity(ParameterDTO dto) {
		Parameter res = new Parameter();

		if (dto != null) {
			res.setId(dto.getId());
			res.setValue(dto.getValue());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD parámetro en una lista de
	 * objetos ParameterDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD parámetro.
	 * @return lista de objetos ParameterDTO.
	 */
	public static List<ParameterDTO> fillParameterListDto(List<Parameter> entities) {
		int queryResultSize = 0;
		List<ParameterDTO> res = new ArrayList<ParameterDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (Parameter param: entities) {
				res.add(fillParameterDto(param));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Parámetros parseados: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD tipo de parámetro en un objeto
	 * ParameterTypeDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD tipo de parámetro.
	 * @return Objeto ParameterTypeDTO.
	 */
	public static ParameterTypeDTO fillParameterTypeDto(ParameterType entity) {
		ParameterTypeDTO res = new ParameterTypeDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setStringPattern(entity.getStringPattern());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ParameterTypeDTO en una entidad de BBDD
	 * tipo de parámetro.
	 * 
	 * @param dto
	 *            objeto ParameterTypeDTO.
	 * @return Objeto entidad de BBDD tipo de parámetro.
	 */
	public static ParameterType fillParameterTypeEntity(ParameterTypeDTO dto) {
		ParameterType res = new ParameterType();

		if (dto != null) {
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setStringPattern(dto.getStringPattern());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD tipo de parámetros en una lista
	 * de objetos ParameterTypeDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD tipo de parámetros.
	 * @return lista de objetos ParameterTypeDTO.
	 */
	public static List<ParameterTypeDTO> fillParameterTypeListDto(List<ParameterType> entities) {
		int queryResultSize = 0;
		List<ParameterTypeDTO> res = new ArrayList<ParameterTypeDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (ParameterType pt: entities) {
				res.add(fillParameterTypeDto(pt));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Tipos de parámetros parseados: {}", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD definición de documento en un objeto
	 * DocumentSpecificationDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD definición de documento.
	 * @return Objeto DocumentSpecificationDTO.
	 */
	public static DocumentSpecificationDTO fillDocumentSpecificationDto(DocumentSpecification entity) {
		DocumentSpecificationDTO res = new DocumentSpecificationDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setName(entity.getName());
			res.setDescription(entity.getDescription());
			res.setActive(entity.getActive());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto DocumentSpecificationDTO en una entidad
	 * de BBDD definición de documento.
	 * 
	 * @param dto
	 *            objeto DocumentSpecificationDTO.
	 * @return Objeto entidad de BBDD definición de documento.
	 */
	public static DocumentSpecification fillDocumentSpecificationEntity(DocumentSpecificationDTO dto) {
		DocumentSpecification res = new DocumentSpecification();

		if (dto != null) {
			res.setId(dto.getId());
			res.setName(dto.getName());
			res.setDescription(dto.getDescription());
			res.setActive(dto.getActive());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD especificación de documento en
	 * una lista de objetos DocumentSpecificationDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD especificación de documento.
	 * @return lista de objetos DocumentSpecificationDTO.
	 */
	public static List<DocumentSpecificationDTO> fillDocumentSpecificationListDto(List<DocumentSpecification> entities) {
		int queryResultSize = 0;
		List<DocumentSpecificationDTO> res = new ArrayList<DocumentSpecificationDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando las definiciones de documento recuperadas de BBDD ...");

			for (DocumentSpecification ds: entities) {
				res.add(fillDocumentSpecificationDto(ds));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Definiciones de documentos parseadas: {}", queryResultSize);

		return res;
	}

	/**
	 * Transforma una entidad de BBDD documento en un objeto ScannedDocumentDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD documento.
	 * @return Objeto ScannedDocumentDTO.
	 */
	public static ScannedDocumentDTO fillScannedDocumentDto(ScannedDocument entity) {
		ScannedDocumentDTO res = new ScannedDocumentDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setHash(entity.getHash());
			res.setContent(entity.getContent());
			res.setSignature(entity.getSignature());
			res.setOcr(entity.getOcr());
			res.setDigProcStartDate(entity.getDigProcStartDate());
			res.setDigProcEndDate(entity.getDigProcEndDate());
			res.setChecked(entity.getChecked());
			res.setCheckDate(entity.getCheckDate());
			//res.setBatchId(entity.getBatch().getId());
			res.setBatchId(entity.getBatch());
			res.setBatchOrderNum(entity.getBatchOrderNum());
			res.setBatchReqId(entity.getBatchReqId());
			res.setMimeType(MimeType.getMimeTypeByType(entity.getMimeType()));
			res.setName(entity.getName());
			res.setSignatureType(entity.getSignatureType());
			res.setUsername(entity.getUsername());
			res.setFunctionalOrgs(entity.getFunctionalOrgs());
			//res.setOrgScanProfileDocSpec(entity.getScanProfileOrgUnitDocSpec().getId());
			res.setOrgScanProfileDocSpec(entity.getScanProfileOrgUnitDocSpec());
			res.setAuditOpId(entity.getAuditOpId());
		}

		return res;
	}

	/**
	 * Transforma una entidad de BBDD documento en un objeto ScannedDocumentDTO
	 * sin incluir sus contenidos binarios.
	 * 
	 * @param entity
	 *            Entidad de BBDD documento.
	 * @return Objeto ScannedDocumentDTO.
	 */
	public static ScannedDocumentDTO fillSummarizedScannedDocumentDto(ScannedDocument entity) {
		ScannedDocumentDTO res = new ScannedDocumentDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setHash(entity.getHash());
			res.setDigProcStartDate(entity.getDigProcStartDate());
			res.setDigProcEndDate(entity.getDigProcEndDate());
			res.setChecked(entity.getChecked());
			res.setCheckDate(entity.getCheckDate());
			//res.setBatchId(entity.getBatch().getId());
			res.setBatchId(entity.getBatch());
			res.setBatchOrderNum(entity.getBatchOrderNum());
			res.setBatchReqId(entity.getBatchReqId());
			res.setMimeType(MimeType.getMimeTypeByType(entity.getMimeType()));
			res.setName(entity.getName());
			res.setSignatureType(entity.getSignatureType());
			res.setUsername(entity.getUsername());
			res.setFunctionalOrgs(entity.getFunctionalOrgs());
			//res.setOrgScanProfileDocSpec(entity.getScanProfileOrgUnitDocSpec().getId());
			res.setOrgScanProfileDocSpec(entity.getScanProfileOrgUnitDocSpec());
			res.setAuditOpId(entity.getAuditOpId());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ScannedDocumentDTO en una entidad de
	 * BBDD documento.
	 * 
	 * @param dto
	 *            objeto ScannedDocumentDTO.
	 * @return Objeto entidad de BBDD documento.
	 */
	public static ScannedDocument fillScannedDocumentEntity(ScannedDocumentDTO dto) {
		ScannedDocument res = new ScannedDocument();

		if (dto != null) {

			res.setAuditOpId(dto.getAuditOpId());
			//res.set.setBatchId(dto.getBatchId());
			res.setBatchOrderNum(dto.getBatchOrderNum());
			res.setBatchReqId(dto.getBatchReqId());
			res.setContent(dto.getContent());
			res.setDigProcEndDate(dto.getDigProcEndDate());
			res.setDigProcStartDate(dto.getDigProcStartDate());
			res.setChecked(dto.getChecked());
			res.setCheckDate(dto.getCheckDate());
			res.setFunctionalOrgs(dto.getFunctionalOrgs());
			res.setHash(dto.getHash());
			res.setId(dto.getId());
			res.setMimeType(dto.getMimeType().getType());
			res.setName(dto.getName());
			res.setOcr(dto.getOcr());
			res.setSignature(dto.getSignature());
			res.setSignatureType(dto.getSignatureType());
			res.setUsername(dto.getUsername());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD documento digitalizado en una
	 * lista de objetos ScannedDocumentDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD documento digitalizado.
	 * @return lista de objetos ScannedDocumentDTO.
	 */
	public static List<ScannedDocumentDTO> fillScannedDocumentListDto(List<ScannedDocument> entities) {
		int queryResultSize = 0;
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando los documentos recuperados de BBDD ...");

			for (ScannedDocument sd: entities) {
				res.add(fillScannedDocumentDto(sd));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Documentos recuperados:{} ", queryResultSize);

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD documento digitalizado en una
	 * lista de objetos ScannedDocumentDTO, sin incluir los contenidos de los
	 * documentos.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD documento digitalizado.
	 * @return lista de objetos ScannedDocumentDTO.
	 */
	public static List<ScannedDocumentDTO> fillSummarizedScannedDocumentListDto(List<ScannedDocument> entities) {
		int queryResultSize = 0;
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando los documentos recuperados de BBDD ...");

			for (ScannedDocument sd: entities) {
				res.add(fillSummarizedScannedDocumentDto(sd));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Documentos recuperados: {}", queryResultSize);

		return res;
	}

	/**
	 * Transforma una entidad de BBDD traza o fase de ejecución asociada al
	 * proceso de digitalización de un documento en un objeto ScannedDocLogDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD traza o fase de ejecución asociada al proceso
	 *            de digitalización de un documento.
	 * @return Objeto ScannedDocLogDTO.
	 */
	public static ScannedDocLogDTO fillScannedDocLogDto(ScannedDocLog entity) {
		ScannedDocLogDTO res = new ScannedDocLogDTO();

		if (entity != null) {
			res.setEndDate(entity.getEndDate());
			res.setId(entity.getId());
			res.setScannedDocId(entity.getDocument().getId());
			res.setScanProcessStageId(entity.getScanProcessStage().getId());
			res.setScanProcessStageName(entity.getScanProcessStage().getName());
			res.setStartDate(entity.getStartDate());
			res.setStatus(entity.getStatus());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ScannedDocLogDTO en una entidad de BBDD
	 * traza o fase de ejecución asociada al proceso de digitalización de un
	 * documento.
	 * 
	 * @param dto
	 *            objeto ScannedDocLogDTO.
	 * @return Objeto entidad de BBDD traza o fase de ejecución asociada al
	 *         proceso de digitalización de un documento.
	 */
	public static ScannedDocLog fillScannedDocLogEntity(ScannedDocLogDTO dto) {
		ScannedDocLog res = new ScannedDocLog();

		if (dto != null) {
			res.setEndDate(dto.getEndDate());
			res.setId(dto.getId());
			res.setStartDate(dto.getStartDate());
			res.setStatus(dto.getStatus());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD traza o fase de ejecución
	 * asociadas al proceso de digitalización de un documento en una lista de
	 * objetos ScannedDocLogDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD traza o fase de ejecución asociada
	 *            al proceso de digitalización de un documento.
	 * @return lista de objetos ScannedDocLogDTO.
	 */
	public static List<ScannedDocLogDTO> fillScannedDocLogListDto(List<ScannedDocLog> entities) {
		int queryResultSize = 0;
		List<ScannedDocLogDTO> res = new ArrayList<ScannedDocLogDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando las trazas de ejecución asociadas al proceso de digitalización de un documento recuperadas de BBDD ...");

			for (ScannedDocLog sdl: entities) {
				res.add(fillScannedDocLogDto(sdl));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Trazas de ejecución asociadas al proceso de digitalización de un documento parseadas: {0}", queryResultSize);

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD documento digitalizado en una
	 * lista de objetos ScannedDocumentDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD documento digitalizado.
	 * @return lista de objetos ScannedDocumentDTO.
	 */
	public static Map<ScannedDocumentDTO, List<ScannedDocLogDTO>> fillScannedDocumentAndLogMapDto(List<ScannedDocument> entities) {
		int queryResultSize = 0;
		Map<ScannedDocumentDTO, List<ScannedDocLogDTO>> res = new HashMap<ScannedDocumentDTO, List<ScannedDocLogDTO>>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando los documentos recuperados de BBDD ...");

			for (ScannedDocument sd: entities) {
				res.put(fillScannedDocumentDto(sd), fillScannedDocLogListDto(sd.getExecLogs()));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Documentos recuperados: {}", queryResultSize);

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD documento digitalizado en una
	 * lista de objetos ScannedDocumentDTO, sin incluir el contenido de los
	 * documentos.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD documento digitalizado.
	 * @return lista de objetos ScannedDocumentDTO.
	 */
	public static Map<ScannedDocumentDTO, List<ScannedDocLogDTO>> fillSummarizedScannedDocumentAndLogMapDto(List<ScannedDocument> entities) {
		int queryResultSize = 0;
		Map<ScannedDocumentDTO, List<ScannedDocLogDTO>> res = new HashMap<ScannedDocumentDTO, List<ScannedDocLogDTO>>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando los documentos recuperados de BBDD ...");

			for (ScannedDocument sd: entities) {
				res.put(fillSummarizedScannedDocumentDto(sd), fillScannedDocLogListDto(sd.getExecLogs()));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Documentos recuperados: {}", queryResultSize);

		return res;
	}

	/**
	 * Transforma una entidad de BBDD definición de metadato en un objeto
	 * MetadataSpecificationDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD definición de metadato.
	 * @return Objeto MetadataSpecificationDTO.
	 * @throws DAOException
	 *             Si se produce un error en la conversión a cadena de
	 *             caracteres de la lista de valores asignables al metadato.
	 */
	public static MetadataSpecificationDTO fillMetadataSpecificationDto(MetadataSpecification entity) throws DAOException {
		MetadataSpecificationDTO res = new MetadataSpecificationDTO();

		try {
			if (entity != null) {
				res.setId(entity.getId());
				res.setName(entity.getName());
				res.setDescription(entity.getDescription());
				res.setDefaultValue(entity.getDefaultValue());
				res.setMandatory(entity.getMandatory());
				res.setEditable(entity.getEditable());
				res.setIncludeInBatchSeprator(entity.getIncludeInBatchSeprator());
				res.setIsBatchId(entity.getIsBatchId());
				if (entity.getSourceValues() != null) {
					res.setSourceValues(new String(entity.getSourceValues(), DEFAULT_CHARSET));
				}
				res.setSpecification(entity.getDocSpecification().getName());
			}
		} catch (UnsupportedEncodingException e) {
			throw new DAOException(DAOException.CODE_998, "Error al procesar la lista de valores asignables al metadato " + entity.getName() + " almacenada en BBDD.");
		}
		return res;
	}

	/**
	 * Transforma parcialmente un objeto MetadataSpecificationDTO en una entidad
	 * de BBDD definición de metadato.
	 * 
	 * @param dto
	 *            objeto MetadataSpecificationDTO.
	 * @return Objeto entidad de BBDD definición de metadato.
	 * @throws DAOException
	 *             Si se produce un error en la conversión a array de bytes de
	 *             la lista de valores asignables al metadato.
	 */
	public static MetadataSpecification fillMetadataSpecificationEntity(MetadataSpecificationDTO dto) throws DAOException {
		MetadataSpecification res = new MetadataSpecification();

		try {
			if (dto != null) {
				res.setId(dto.getId());
				res.setName(dto.getName());
				res.setDescription(dto.getDescription());
				res.setDefaultValue(dto.getDefaultValue());
				res.setMandatory(dto.getMandatory());
				res.setEditable(dto.getEditable());
				res.setIncludeInBatchSeprator(dto.getIncludeInBatchSeprator());
				res.setIsBatchId(dto.getIsBatchId());

				if (dto.getSourceValues() != null) {
					res.setSourceValues(dto.getSourceValues().getBytes(DEFAULT_CHARSET));
				}
			}
		} catch (UnsupportedEncodingException e) {
			throw new DAOException(DAOException.CODE_998, "Error al procesar la lista de valores asignables al metadato " + dto.getName() + ".");
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD especificación de metadatos en
	 * una lista de objetos MetadataSpecificationDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD especificación de metadatos.
	 * @return lista de objetos MetadataSpecificationDTO.
	 * @throws DAOException
	 *             Si se produce un error en la conversión a cadena de
	 *             caracteres de la lista de valores asignables a un metadato.
	 */
	public static List<MetadataSpecificationDTO> fillMetadataSpecificationListDto(List<MetadataSpecification> entities) throws DAOException {
		int queryResultSize = 0;
		List<MetadataSpecificationDTO> res = new ArrayList<MetadataSpecificationDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando las especificaciones de metadatos recuperadas de BBDD ...");

			for (MetadataSpecification ms: entities) {
				res.add(fillMetadataSpecificationDto(ms));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Especificaciones de metadatos parseadas: {}", queryResultSize);

		return res;
	}

	/**
	 * Transforma una entidad de BBDD metadato en un objeto MetadataDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD metadato.
	 * @return Objeto MetadataDTO.
	 */
	public static MetadataDTO fillMetadataDto(Metadata entity) {
		MetadataDTO res = new MetadataDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setValue(entity.getValue());
			res.setSpecification(entity.getSpecification().getId());
		}

		return res;
	}

	/**
	 * Genera un objeto MetadataDTO vacío a partir de su especificación.
	 * 
	 * @param entity
	 *            Entidad de BBDD definición de metadato.
	 * @return Objeto MetadataDTO.
	 */
	public static MetadataDTO fillEmptyMetadataDto(MetadataSpecification entity) {
		MetadataDTO res = new MetadataDTO();

		res.setSpecification(entity.getId());

		return res;
	}

	/**
	 * Transforma parcialmente un objeto MetadataDTO en una entidad de BBDD
	 * metadato.
	 * 
	 * @param dto
	 *            objeto MetadataDTO.
	 * @return Objeto entidad de BBDD metadato.
	 */
	public static Metadata fillMetadataEntity(MetadataDTO dto) {
		Metadata res = new Metadata();

		if (dto != null) {
			res.setId(dto.getId());
			res.setValue(dto.getValue());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD metadato en una lista de
	 * objetos MetadataDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD metadato.
	 * @return lista de objetos MetadataDTO.
	 */
	public static List<MetadataDTO> fillMetadataListDto(List<Metadata> entities) {
		int queryResultSize = 0;
		List<MetadataDTO> res = new ArrayList<MetadataDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando los metadatos recuperados de BBDD ...");

			for (Metadata metadata: entities) {
				res.add(fillMetadataDto(metadata));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Mmetadatos recuperados: {}", queryResultSize);

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD metadato en un mapa de
	 * especificaciones de metadatos (objeto MetadataSpecificationDTO), y
	 * objetos MetadataDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD metadato.
	 * @return lista de objetos MetadataDTO.
	 * @throws DAOException
	 *             Si se produce un error en la conversión a array de bytes de
	 *             la lista de valores asignables a un metadato.
	 */
	public static Map<MetadataSpecificationDTO, MetadataDTO> fillMetadataAndSpecMapDto(List<Metadata> entities) throws DAOException {
		int queryResultSize = 0;
		Map<MetadataSpecificationDTO, MetadataDTO> res = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		MetadataSpecificationDTO specDto;

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando los metadatos recuperados de BBDD ...");

			for (Metadata metadata: entities) {
				specDto = fillMetadataSpecificationDto(metadata.getSpecification());
				res.put(specDto, fillMetadataDto(metadata));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Mmetadatos recuperados: {}", queryResultSize);

		return res;
	}

	/**
	 * Transforma una entidad de BBDD configuración de perfil de digitalización
	 * en unidad organizativa en un objeto ScanProfileOrgUnitDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Objeto ScanProfileOrgUnitDTO.
	 */
	public static ScanProfileOrgUnitDTO fillScanProfileOrgUnitDto(ScanProfileOrgUnit entity) {
		ScanProfileOrgUnitDTO res = new ScanProfileOrgUnitDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setOrgUnit(entity.getOrgUnit().getId());
			res.setScanProfile(entity.getScanProfile().getId());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ScanProfileOrgUnitDTO en una entidad de
	 * BBDD configuración de perfil de digitalización en unidad organizativa.
	 * 
	 * @param dto
	 *            objeto ScanProfileOrgUnitDTO.
	 * @return Objeto entidad de BBDD configuración de perfil de digitalización
	 *         en unidad organizativa.
	 */
	public static ScanProfileOrgUnit fillScanProfileOrgUnitEntity(ScanProfileOrgUnitDTO dto) {
		ScanProfileOrgUnit res = new ScanProfileOrgUnit();

		if (dto != null) {
			res.setId(dto.getId());
		}

		return res;
	}

	/**
	 * Transforma una entidad de BBDD configuración de plugin para perfil de
	 * digitalización y unidad organizativa en un objeto
	 * ScanProfileOrgUnitPluginDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD configuración de plugin para perfil de
	 *            digitalización y unidad organizativa.
	 * @return Objeto ScanProfileOrgUnitPluginDTO.
	 */
	public static ScanProfileOrgUnitPluginDTO fillScanProfileOrgUnitPluginDto(ScanProfileOrgUnitPlugin entity) {
		ScanProfileOrgUnitPluginDTO res = new ScanProfileOrgUnitPluginDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setActive(entity.getActive());
			res.setOrgScanProfile(entity.getOrgScanProfile().getId());
			res.setPlugin(entity.getPlugin().getId());
			res.setScanProcessStage(entity.getScanProcessStage().getId());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto ScanProfileOrgUnitPluginDTO en una
	 * entidad de BBDD configuración de plugin para perfil de digitalización y
	 * unidad organizativa.
	 * 
	 * @param dto
	 *            objeto ScanProfileOrgUnitPluginDTO.
	 * @return Objeto entidad de BBDD configuración de plugin para perfil de
	 *         digitalización y unidad organizativa.
	 */
	public static ScanProfileOrgUnitPlugin fillScanProfileOrgUnitPluginEntity(ScanProfileOrgUnitPluginDTO dto) {
		ScanProfileOrgUnitPlugin res = new ScanProfileOrgUnitPlugin();

		if (dto != null) {
			res.setId(dto.getId());
			res.setActive(dto.getActive());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD plugin empleando en fase de
	 * proceso de digitalización para configuración de perfil de digitalización
	 * en unidad organizativa, en una lista de objetos
	 * ScanProfileOrgUnitPluginDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD plugin empleando en fase de proceso
	 *            de digitalización para configuración de perfil de
	 *            digitalización en unidad organizativa.
	 * @return lista de objetos ScanProfileOrgUnitPluginDTO.
	 */
	public static List<ScanProfileOrgUnitPluginDTO> fillScanProfileOrgUnitPluginListDto(List<ScanProfileOrgUnitPlugin> entities) {
		int queryResultSize = 0;
		List<ScanProfileOrgUnitPluginDTO> res = new ArrayList<ScanProfileOrgUnitPluginDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando los plugins en configuración de perfil, unidad organizativa y fase recuperados de BBDD ...");

			for (ScanProfileOrgUnitPlugin spoup: entities) {
				res.add(fillScanProfileOrgUnitPluginDto(spoup));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Plugins parseados:{} ", queryResultSize);

		return res;
	}

	/**
	 * Transforma una entidad de BBDD unidad organizativa en un objeto
	 * OrganizationUnitDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD unidad organizativa.
	 * @return Objeto OrganizationUnitDTO.
	 */
	public static OrganizationUnitDTO fillOrganizationUnitDto(OrganizationUnit entity) {
		OrganizationUnitDTO res = new OrganizationUnitDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setExternalId(entity.getExternalId());
			res.setName(entity.getName());
			res.setParentId((entity.getParentOrg() != null ? entity.getParentOrg().getId() : null));
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto OrganizationUnitDTO en una entidad de
	 * BBDD unidad organizativa.
	 * 
	 * @param dto
	 *            objeto OrganizationUnitDTO.
	 * @return Objeto entidad de BBDD unidad organizativa.
	 */
	public static OrganizationUnit fillOrganizationUnitEntity(OrganizationUnitDTO dto) {
		OrganizationUnit res = new OrganizationUnit();

		if (dto != null) {
			res.setId(dto.getId());
			res.setExternalId(dto.getExternalId());
			res.setName(dto.getName());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD unidad organizativa en una
	 * lista de objetos OrganizationUnitDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD unidad organizativa.
	 * @return lista de objetos OrganizationUnitDTO.
	 */
	public static List<OrganizationUnitDTO> fillOrganizationUnitListDto(List<OrganizationUnit> entities) {
		int queryResultSize = 0;
		List<OrganizationUnitDTO> res = new ArrayList<OrganizationUnitDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (OrganizationUnit orgUnit: entities) {
				res.add(fillOrganizationUnitDto(orgUnit));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Unidades organizativas parseadas: {}", queryResultSize);
		return res;
	}

	/**
	 * Construye un árbol de unidades organizativas a partir de una entidad
	 * padre y sus unidades organizativas hija.
	 * 
	 * @param entity
	 *            Entidad de BBDD unidad organizativa.
	 * @param childOrgUnitsDto
	 *            relación de objetos OrganizationUnitTreeDTO que representan
	 *            las unidades organizativas hija.
	 * @return Objeto OrganizationUnitTreeDTO que representa el árbol de
	 *         unidades organizativas generado.
	 */
	public static OrganizationUnitTreeDTO fillOrganizationUnitTreeDto(OrganizationUnit entity, List<OrganizationUnitTreeDTO> childOrgUnitsDto) {
		OrganizationUnitTreeDTO res = new OrganizationUnitTreeDTO();

		res.setOrganizationUnit(fillOrganizationUnitDto(entity));
		res.setChildOrganizationUnits(childOrgUnitsDto);

		return res;
	}

	/**
	 * Transforma una entidad de BBDD traza de auditoría en un objeto
	 * AuditOperationDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD traza de auditoría.
	 * @return Objeto AuditOperationDTO.
	 */
	public static AuditOperationDTO fillAuditOperationDto(AuditOperation entity) {
		AuditOperationDTO res = new AuditOperationDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setLastUpdateDate(entity.getLastUpdateDate());
			res.setName(entity.getName());
			res.setStatus(entity.getStatus());
			res.setStartDate(entity.getStartDate());

			if (entity.getAuditScannedDoc() != null) {
				res.setAuditScannedDocId(entity.getAuditScannedDoc().getId());
			}
		}

		return res;
	}

	/**
	 * Transforma una entidad de BBDD traza de auditoría en un objeto
	 * AuditReportDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD traza de auditoría.
	 * @return Objeto AuditReportDTO.
	 */
	public static AuditReportDTO fillAuditReportDto(AuditOperation entity) {
		AuditReportDTO res = new AuditReportDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setLastUpdateDate(entity.getLastUpdateDate());
			res.setName(entity.getName());
			res.setStatus(entity.getStatus());
			res.setStartDate(entity.getStartDate());
			if (entity.getAuditEvents() != null) {
				res.setListAuditEvent(fillAuditEventDto(entity.getAuditEvents()));
			}
			if (entity.getAuditScannedDoc() != null) {
				res.setAuditScannedDocId(entity.getAuditScannedDoc().getId());
			}
		}

		return res;
	}

	/**
	 * Transforma una lista de la entidad de BBDD de eventos de auditoría en una
	 * lista de objecto AuditEventDTO
	 * 
	 * @param listAuditEvent
	 *            Listado de entidades de BBDD de trazas de eventos de
	 *            auditorías.
	 * @return Listado de objectos de eventos de auditorías
	 */

	private static List<AuditEventDTO> fillAuditEventDto(List<AuditEvent> listAuditEvent) {
		List<AuditEventDTO> res = new ArrayList<AuditEventDTO>();
		AuditEventDTO nuevo;
		String addInfo;

		for (AuditEvent audit: listAuditEvent) {
			nuevo = new AuditEventDTO();
			nuevo.setId(audit.getId());
			nuevo.setEventUser(audit.getEventUser());
			nuevo.setEventResult(audit.getEventResult());
			nuevo.setEventName(audit.getEventName());
			nuevo.setEventDate(audit.getEventDate());
			nuevo.setAuditOperationId(audit.getAuditOperation().getId());
			if (audit.getAdditionalInfo() != null) {
				addInfo = audit.getAdditionalInfo();
				addInfo = addInfo.replaceAll("[.*+?%&/^${}()|\\\\[\\\\]<>]", " ");
				nuevo.setAdditionalInfo(addInfo);
			}
			res.add(nuevo);
		}
		return res;
	}

	/**
	 * Transforma parcialmente un objeto AuditOperationDTO en una entidad de
	 * BBDD traza de auditoría.
	 * 
	 * @param dto
	 *            objeto AuditOperationDTO.
	 * @return Objeto entidad de BBDD traza de auditoría.
	 */
	public static AuditOperation fillAuditOperationEntity(AuditOperationDTO dto) {
		AuditOperation res = new AuditOperation();

		if (dto != null) {
			res.setId(dto.getId());
			res.setLastUpdateDate(dto.getLastUpdateDate());
			res.setName(dto.getName());
			res.setStatus(dto.getStatus());
			res.setStartDate(dto.getStartDate());
		}

		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD traza de auditoría en una lista
	 * de objetos AuditOperationDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD traza de auditoría.
	 * @return lista de objetos AuditOperationDTO.
	 */
	public static List<AuditOperationDTO> fillAuditOperationListDto(List<AuditOperation> entities) {
		int queryResultSize = 0;
		List<AuditOperationDTO> res = new ArrayList<AuditOperationDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (AuditOperation auditOp: entities) {
				res.add(fillAuditOperationDto(auditOp));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Trazas de auditoría parseadas:{} ", queryResultSize);
		return res;
	}

	/**
	 * Transforma una lista de entidades de BBDD traza de auditoría en una lista
	 * de objetos AuditReportDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD traza de auditoría.
	 * @return lista de objetos AuditReportDTO.
	 */
	public static List<AuditReportDTO> fillAuditReportListDto(List<AuditOperation> entities) {
		int queryResultSize = 0;
		List<AuditReportDTO> res = new ArrayList<AuditReportDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			for (AuditOperation auditOp: entities) {
				res.add(fillAuditReportDto(auditOp));
			}
		}

		LOG.debug("[WEBSCAN-MODEL] Trazas de auditoría parseadas:{} ", queryResultSize);
		return res;
	}

	/**
	 * Transforma una entidad de BBDD evento de auditoría en un objeto
	 * AuditEventDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD evento de auditoría.
	 * @return Objeto AuditEventDTO.
	 */
	public static AuditEventDTO fillAuditEventDto(AuditEvent entity) {
		AuditEventDTO res = new AuditEventDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setEventName(entity.getEventName());
			res.setEventUser(entity.getEventUser());
			res.setEventResult(entity.getEventResult());
			res.setEventDate(entity.getEventDate());
			res.setAdditionalInfo(entity.getAdditionalInfo());
			res.setAuditOperationId((entity.getAuditOperation() != null ? entity.getAuditOperation().getId() : null));
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto AuditEventDTO en una entidad de BBDD
	 * evento de auditoría.
	 * 
	 * @param dto
	 *            objeto AuditEventDTO.
	 * @return Objeto entidad de BBDD evento de auditoría.
	 */
	public static AuditEvent fillAuditEventEntity(AuditEventDTO dto) {
		AuditEvent res = new AuditEvent();

		if (dto != null) {
			res.setId(dto.getId());
			res.setEventName(dto.getEventName());
			res.setEventUser(dto.getEventUser());
			res.setEventResult(dto.getEventResult());
			res.setEventDate(dto.getEventDate());
			res.setAdditionalInfo(dto.getAdditionalInfo());
		}

		return res;
	}

	/**
	 * Transforma una entidad de BBDD histórico de documento digitalizado en un
	 * objeto AuditScannedDocDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD histórico de documento digitalizado.
	 * @return Objeto AuditScannedDocDTO.
	 */
	public static AuditScannedDocDTO fillAuditScannedDocDto(AuditScannedDoc entity) {
		AuditScannedDocDTO res = new AuditScannedDocDTO();

		if (entity != null) {
			res.setAuditOpId(entity.getAuditOperation().getId());
			res.setBatchId(entity.getBatchId());
			res.setBatchReqId(entity.getBatchReqId());
			res.setDigProcEndDate(entity.getDigProcEndDate());
			res.setDigProcStartDate(entity.getDigProcStartDate());
			res.setDocSpec(entity.getDocSpecName());
			res.setFunctionalOrgs(entity.getFunctionalOrgs());
			res.setHash(entity.getHash());
			res.setId(entity.getId());
			res.setMimeType(MimeType.getMimeTypeByType(entity.getMimeType()));
			res.setSignatureType(entity.getSignatureType());
			res.setUsername(entity.getUsername());
		}

		return res;
	}

	/**
	 * Transforma parcialmente un objeto AuditScannedDocDTO en una entidad de
	 * BBDD histórico de documento digitalizado.
	 * 
	 * @param dto
	 *            objeto AuditScannedDocDTO.
	 * @return Objeto entidad de BBDD histórico de documento digitalizado.
	 */
	public static AuditScannedDoc fillAuditScannedDocEntity(AuditScannedDocDTO dto) {
		AuditScannedDoc res = new AuditScannedDoc();

		if (dto != null) {
			res.setBatchId(dto.getBatchId());
			res.setBatchReqId(dto.getBatchReqId());
			res.setDigProcEndDate(dto.getDigProcEndDate());
			res.setDigProcStartDate(dto.getDigProcStartDate());
			res.setDocSpecName(dto.getDocSpec());
			res.setFunctionalOrgs(dto.getFunctionalOrgs());
			res.setHash(dto.getHash());
			res.setId(dto.getId());
			res.setMimeType(dto.getMimeType().getType());
			res.setSignatureType(dto.getSignatureType());
			res.setUsername(dto.getUsername());
		}

		return res;
	}
	

	/**
	 * Transforma una lista de entidades de BBDD proceso de digitalización en una
	 * lista de objetos BatchDTO.
	 * 
	 * @param entities
	 *            lista de entidades de BBDD proceso de digitalización.
	 * @return lista de objetos BatchDTO.
	 */
	public static List<BatchDTO> fillBatchListDto(List<Batch> entities) {
		int queryResultSize = 0;
		List<BatchDTO> res = new ArrayList<BatchDTO>();

		if (entities != null && !entities.isEmpty()) {
			queryResultSize = entities.size();

			LOG.debug("[WEBSCAN-MODEL] Parseando los documentos recuperados de BBDD ...");

			for (Batch sd: entities) {
				res.add(fillBatchDto(sd));
			}
		}
		LOG.debug("[WEBSCAN-MODEL] Documentos recuperados:{} ", queryResultSize);

		return res;
	}
	
	/**
	 * Transforma una entidad de BBDD proceso de digitalización en un objeto BatchDTO.
	 * 
	 * @param entity
	 *            Entidad de BBDD proceso digitalización.
	 * @return Objeto BatchDTO.
	 */
	public static BatchDTO fillBatchDto(Batch entity) {
		BatchDTO res = new BatchDTO();

		if (entity != null) {
			res.setId(entity.getId());
			res.setBatchNameId(entity.getBatchNameId());
			res.setBatchReqId(entity.getBatchReqId());
			res.setStartDate(entity.getStartDate());
			res.setUsername(entity.getUsername());
			res.setFunctionalOrgs(entity.getFunctionalOrgs());
			res.setOrgScanProfileDocSpec(entity.getScanProfileOrgUnitDocSpec().getId());
		}

		return res;
	}
}
