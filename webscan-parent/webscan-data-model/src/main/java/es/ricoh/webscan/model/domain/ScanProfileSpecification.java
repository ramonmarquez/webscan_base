/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.ScanProfileSpecification.java.</p>
 * <b>Descripción:</b><p> Entidad JPA especificación de perfil de digitalización.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA especificación de perfil de digitalización.
 * <p>
 * Clase ScanProfileSpecification.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "SCAN_PROFILE_SPEC", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME" }) })
public class ScanProfileSpecification implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Atributo DESCRIPTION de la entidad.
	 */
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * Relación de fases de una definición de perfil de digitalización.
	 */
	@OneToMany(mappedBy = "scanProfileSpecification", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScanProfileSpecScanProcStage.class)
	@OrderBy("order ASC")
	private List<ScanProfileSpecScanProcStage> scanProcStages = new ArrayList<ScanProfileSpecScanProcStage>();

	/**
	 * Relación de perfiles de digitalización definidos por la especificación.
	 */
	@OneToMany(mappedBy = "specification", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScanProfile.class)
	private List<ScanProfile> scanProfiles = new ArrayList<ScanProfile>();

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfileSpecification() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo DESCRIPTION de la entidad.
	 * 
	 * @return el atributo DESCRIPTION de la entidad.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para el atributo DESCRIPTION de la entidad.
	 * 
	 * @param aDescription
	 *            nuevo valor para el atributo DESCRIPTION de la entidad.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene la relación de fases de una definición de perfil de
	 * digitalización.
	 * 
	 * @return la relación de fases de una definición de perfil de
	 *         digitalización.
	 */
	public List<ScanProfileSpecScanProcStage> getScanProcStages() {
		return scanProcStages;
	}

	/**
	 * Establece una nueva relación de fases de una definición de perfil de
	 * digitalización.
	 * 
	 * @param anyScanProcStages
	 *            Nueva relación de fases de una definición de perfil de
	 *            digitalización.
	 */
	public void setScanProcStages(List<ScanProfileSpecScanProcStage> anyScanProcStages) {
		this.scanProcStages.clear();

		if (anyScanProcStages != null && !anyScanProcStages.isEmpty()) {
			this.scanProcStages.addAll(anyScanProcStages);
		}
	}

	/**
	 * Obtiene la relación de perfiles de digitalización definidos por la
	 * especificación.
	 * 
	 * @return la relación de perfiles de digitalización definidos por la
	 *         especificación.
	 */
	public List<ScanProfile> getScanProfiles() {
		return scanProfiles;
	}

	/**
	 * Establece una nueva relación de perfiles de digitalización definidos por
	 * la especificación.
	 * 
	 * @param anyScanProfiles
	 *            Nueva relación de perfiles de digitalización definidos por la
	 *            especificación.
	 */
	public void setScanProfiles(List<ScanProfile> anyScanProfiles) {
		this.scanProfiles.clear();
		if (anyScanProfiles != null && !anyScanProfiles.isEmpty()) {
			this.scanProfiles.addAll(anyScanProfiles);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScanProfileSpecification other = (ScanProfileSpecification) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
