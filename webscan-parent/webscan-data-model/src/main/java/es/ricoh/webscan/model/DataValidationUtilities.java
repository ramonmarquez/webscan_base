/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.DataValidationUtilities.java.</p>
 * <b>Descripción:</b><p> Utilidad de comprobación y verificación de objetos DTO intercambiados 
 * con la capa DAO.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.ValidationUtilities;
import es.ricoh.webscan.WebscanConstants;
import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.domain.OperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.ParameterDTO;
import es.ricoh.webscan.model.dto.ParameterSpecificationDTO;
import es.ricoh.webscan.model.dto.ParameterTypeDTO;
import es.ricoh.webscan.model.dto.PluginDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;

/**
 * Utilidad de comprobación y verificación de objetos DTO intercambiados con la
 * capa DAO.
 * <p>
 * Clase DataValidationUtilities.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.ValidationUtilities
 * @version 2.0.
 */
public class DataValidationUtilities extends ValidationUtilities {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(DataValidationUtilities.class);

	/**
	 * Comprueba que una especificación de plugin no sea nula, y posea
	 * identificador y denominación.
	 * 
	 * @param specification
	 *            especificación de plugin.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkPluginSpecification(PluginSpecificationDTO specification) throws DAOException {
		Boolean isNew;
		String excMsg;

		isNew = specification != null && specification.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva definición de plugin. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar la definición de plugin " + (specification != null ? specification.getId() : "No especificado") + ". ";
		}

		try {
			checkEmptyObject(specification, excMsg + "Objeto nulo o vacío.");

			if (!isNew && specification.getId() == null) {
				excMsg += "Identificador nulo.";
				LOGGER.error(excMsg);
				throw new DAOException(DAOException.CODE_998, excMsg);
			}

			checkEmptyObject(specification.getName(), excMsg + "Denominación nula o vacía.");
		} catch (DAOException e) {
			throw e;
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba una lista de plugins, verificando los datos de cada uno de
	 * ellos y de sus parametros.
	 * 
	 * @param plugins
	 *            Relación de plugins.
	 * @param paramSpecs
	 *            Especificación de parámetros.
	 * @param paramTypes
	 *            Tipos de parámetros.
	 * @return Plugins y parámetros de estos.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static Map<PluginDTO, Map<ParameterSpecificationDTO, ParameterDTO>> checkPlugins(Map<PluginDTO, List<ParameterDTO>> plugins, List<ParameterSpecificationDTO> paramSpecs, List<ParameterTypeDTO> paramTypes) throws DAOException {
		Map<PluginDTO, Map<ParameterSpecificationDTO, ParameterDTO>> res = new HashMap<PluginDTO, Map<ParameterSpecificationDTO, ParameterDTO>>();
		Map<ParameterSpecificationDTO, ParameterDTO> parameters;
		PluginDTO plugin;

		if (plugins != null && !plugins.isEmpty()) {
			for (Iterator<PluginDTO> it = plugins.keySet().iterator(); it.hasNext();) {
				plugin = it.next();

				checkPlugin(plugin);
				parameters = checkParameters(plugins.get(plugin), paramSpecs, plugin, paramTypes);
				res.put(plugin, parameters);
			}
		}

		return res;
	}

	/**
	 * Comprueba que un plugin no sea nulo, y posea identificador, denominación
	 * y su definición.
	 * 
	 * @param plugin
	 *            Plugin.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkPlugin(PluginDTO plugin) throws DAOException {
		Boolean isNew;
		String excMsg;

		isNew = plugin != null && plugin.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear un nuevo plugin. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar un plugin " + plugin.getId() + ". ";
		}
		try {
			checkEmptyObject(plugin, excMsg + "Objeto nulo o vacío.");

			checkEmptyObject(plugin.getName(), excMsg + "Denominación nula o vacía.");
			if (!isNew) {
				checkEmptyObject(plugin.getSpecification(), excMsg + "Definición de plugin no informada.");
			}
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba una lista de definiciones de parámetros, verificando los datos
	 * de cada una de ellas.
	 * 
	 * @param parameters
	 *            Relación de definiciones de parámetros.
	 * @param specId
	 *            Especificación de plugin.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkParameterSpecifications(List<ParameterSpecificationDTO> parameters, Long specId) throws DAOException {
		if (parameters != null && !parameters.isEmpty()) {

			for (ParameterSpecificationDTO ps: parameters) {
				checkParameterSpecification(ps, specId);
			}
		}
	}

	/**
	 * Comprueba que una definición de parámetro sea correcta, verificando que
	 * no sea nula, y posea identificador, denominación.
	 * 
	 * @param parameter
	 *            Definición de parámetro.
	 * @param specId
	 *            Especificación de plugin.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkParameterSpecification(ParameterSpecificationDTO parameter, Long specId) throws DAOException {
		Boolean isNew;
		String excMsg;

		isNew = specId == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva definición de plugin. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar la definición de plugin " + specId + ". ";
		}

		try {
			checkEmptyObject(parameter, excMsg + "Especificación de parametro nula o vacía.");

			checkEmptyObject(parameter.getName(), excMsg + "Existen parámetros cuya denominación es nula o vacía.");

			checkEmptyObject(parameter.getTypeId(), excMsg + "Existen parámetros cuyo tipo de datos es nulo.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba una lista de parámetros, verificando los datos de cada uno de
	 * ellos contra sus definiciones.
	 * 
	 * @param parameters
	 *            Relación de parámetros.
	 * @param parameterSpecifications
	 *            Relación de definiciones de parámetros.
	 * @param plugin
	 *            Plugin.
	 * @param paramTypes
	 *            tipos de parámetros.
	 * @return Mapa de especificaciones y su parámetro.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static Map<ParameterSpecificationDTO, ParameterDTO> checkParameters(List<ParameterDTO> parameters, List<ParameterSpecificationDTO> parameterSpecifications, PluginDTO plugin, List<ParameterTypeDTO> paramTypes) throws DAOException {
		Boolean isNew;
		Map<ParameterSpecificationDTO, ParameterDTO> res = new HashMap<ParameterSpecificationDTO, ParameterDTO>();
		String excMsg;

		isNew = plugin != null && plugin.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear un nuevo plugin. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar el plugin " + plugin.getName() + ". ";
		}

		checkListLengths(parameters, parameterSpecifications, excMsg);

		if (!parameterSpecifications.isEmpty()) {
			res.putAll(checkParamSpecsVsParams(parameters, parameterSpecifications, paramTypes, excMsg));
			res.putAll(checkParamsVsParamSpecs(parameters, parameterSpecifications, excMsg));
		}

		return res;
	}

	/**
	 * Comprueba que la lista de parámetros sea igual o menor longitud que la
	 * lista de definciiones de parámetros.
	 * 
	 * @param parameters
	 *            Lista de parámetros.
	 * @param parameterSpecifications
	 *            Lista de definiciones de parámetros.
	 * @param excHeaderMsg
	 *            Cabecera del mensaje de error.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	private static void checkListLengths(List<ParameterDTO> parameters, List<ParameterSpecificationDTO> parameterSpecifications, String excHeaderMsg) throws DAOException {
		String excMsg;

		if ((parameterSpecifications == null || parameterSpecifications.isEmpty()) && (parameters != null && !parameters.isEmpty())) {
			excMsg = excHeaderMsg + "El plugin no admite el establecimiento de parámetros.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		if ((parameterSpecifications != null && parameters != null) && parameterSpecifications.size() < parameters.size()) {
			excMsg = excHeaderMsg + "Se han establecido más parámetros de los admitidos por la definición del plugin.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}
	}

	/**
	 * Comprueba que todas definciones de parámetros requeridas tienen informado
	 * un parámetro.
	 * 
	 * @param parameters
	 *            Lista de parámetros.
	 * @param parameterSpecifications
	 *            Lista de definiciones de parámetros.
	 * @param paramTypes
	 *            tipos de parámetros.
	 * @param excHeaderMsg
	 *            Cabecera del mensaje de error.
	 * @return Mapa de especificaciones y sus parámetro.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	private static Map<ParameterSpecificationDTO, ParameterDTO> checkParamSpecsVsParams(List<ParameterDTO> parameters, List<ParameterSpecificationDTO> parameterSpecifications, List<ParameterTypeDTO> paramTypes, String excHeaderMsg) throws DAOException {
		Boolean found;
		Map<ParameterSpecificationDTO, ParameterDTO> res = new HashMap<ParameterSpecificationDTO, ParameterDTO>();
		String excMsg;

		for (ParameterSpecificationDTO psDto: parameterSpecifications) {
			found = Boolean.FALSE;

			for (Iterator<ParameterDTO> it = parameters.iterator(); !found && it.hasNext();) {
				ParameterDTO paramDto = it.next();

				if (checkParamSpecVsParam(paramDto, psDto, paramTypes, excHeaderMsg)) {
					res.put(psDto, paramDto);
					found = Boolean.TRUE;
				}
			}

			if (psDto.getMandatory() && !found) {
				excMsg = excHeaderMsg + "No fueron configurados parámetros requeridos por la definición del plugin, como por ejemplo " + psDto.getName() + ".";
				LOGGER.error(excMsg);
				throw new DAOException(DAOException.CODE_998, excMsg);
			}

		}

		return res;
	}

	/**
	 * Comprueba que una definción de parámetro requerida tiene informado un
	 * parámetro.
	 * 
	 * @param paramDto
	 *            Parámetro.
	 * @param psDto
	 *            Definición de parámetro.
	 * @param paramTypes
	 *            tipos de parámetros.
	 * @param excHeaderMsg
	 *            Cabecera del mensaje de error.
	 * @return true, si el parámetro se corresponde con definición. En caso
	 *         contrario, false.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	private static Boolean checkParamSpecVsParam(ParameterDTO paramDto, ParameterSpecificationDTO psDto, List<ParameterTypeDTO> paramTypes, String excHeaderMsg) throws DAOException {
		Boolean res = Boolean.FALSE;
		String excMsg;

		if (psDto.getName().equals(paramDto.getSpecificationName())) {
			if (psDto.getMandatory() && (paramDto.getValue() == null || paramDto.getValue().isEmpty())) {
				excMsg = excHeaderMsg + "No fueron configurados parámetros requeridos por la definición del plugin (" + psDto.getName() + ").";
				LOGGER.error(excMsg);
				throw new DAOException(excMsg);
			} else {
				if (paramDto.getValue() != null && !paramDto.getValue().isEmpty()) {
					// Se comprueba que el valor del parámetro haya
					// sido informado
					// conforme al patrón especificado para el tipo
					// de datos
					checkParamValue(psDto.getName(), paramDto.getValue(), psDto.getTypeId(), paramTypes);
				}
			}
			res = Boolean.TRUE;
		}

		return res;

	}

	/**
	 * Comprueba que todos los parámetros informados se correspondan con una
	 * especificación de parámetro.
	 * 
	 * @param parameters
	 *            Lista de parámetros.
	 * @param parameterSpecifications
	 *            Lista de definiciones de parámetros.
	 * @param excHeaderMsg
	 *            Cabecera del mensaje de error.
	 * @return Mapa de especificaciones y sus parámetro.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	private static Map<ParameterSpecificationDTO, ParameterDTO> checkParamsVsParamSpecs(List<ParameterDTO> parameters, List<ParameterSpecificationDTO> parameterSpecifications, String excHeaderMsg) throws DAOException {
		Boolean found;
		Map<ParameterSpecificationDTO, ParameterDTO> res = new HashMap<ParameterSpecificationDTO, ParameterDTO>();
		String excMsg;

		for (ParameterDTO pDto: parameters) {
			found = Boolean.FALSE;
			for (Iterator<ParameterSpecificationDTO> it = parameterSpecifications.iterator(); !found && it.hasNext();) {
				ParameterSpecificationDTO paramSpecDto = it.next();
				if (paramSpecDto.getName().equals(pDto.getSpecificationName())) {
					res.put(paramSpecDto, pDto);
					found = Boolean.TRUE;
				}
			}

			if (!found) {
				excMsg = excHeaderMsg + "El parámetro " + pDto.getSpecificationName() + " no ha sido definido para el plugin.";
				LOGGER.error(excMsg);
				throw new DAOException(DAOException.CODE_998, excMsg);
			}
		}

		return res;
	}

	/**
	 * Comprueba una lista de especificaciones de documentos, verificando cada
	 * una de ellas.
	 * 
	 * @param docSpecifications
	 *            Relación de definiciones de documentos.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkDocSpecifications(List<DocumentSpecificationDTO> docSpecifications) throws DAOException {
		if (docSpecifications != null && !docSpecifications.isEmpty()) {
			for (DocumentSpecificationDTO ds: docSpecifications) {
				checkDocSpecification(ds);
			}
		}
	}

	/**
	 * Comprueba que una especificación de documento no sea nula, y posea
	 * identificador y denominación.
	 * 
	 * @param specification
	 *            especificación de documento.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkDocSpecification(DocumentSpecificationDTO specification) throws DAOException {
		Boolean isNew;
		String excMsg;

		isNew = specification != null && specification.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva definición de documento. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar la definición de documento " + specification.getId() + ". ";
		}

		try {
			checkEmptyObject(specification, excMsg + "Objeto nulo o vacío.");

			if (!isNew && specification.getId() == null) {
				excMsg += "Identificador nulo.";
				LOGGER.error(excMsg);
				throw new DAOException(DAOException.CODE_998, excMsg);
			}

			checkEmptyObject(specification.getName(), excMsg + "Denominación nula o vacía.");
		} catch (DAOException e) {
			throw e;
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba una lista de especificaciones de metadatos, verificando cada
	 * una de ellas.
	 * 
	 * @param metadataCollection
	 *            Relación de definiciones de metadatos de documentos.
	 * @param specId
	 *            Identificador de especificación de documento.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkMetadataSpecifications(List<MetadataSpecificationDTO> metadataCollection, Long specId) throws DAOException {
		if (metadataCollection != null && !metadataCollection.isEmpty()) {
			for (MetadataSpecificationDTO ms: metadataCollection) {
				checkMetadataSpecification(ms, specId);
			}
		}
	}

	/**
	 * Comprueba que una especificación de metadato no sea nula y posea
	 * denominación.
	 * 
	 * @param metadata
	 *            especificación de metadatos de documento.
	 * @param docSpecId
	 *            Identificador de especificación de documento.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkMetadataSpecification(MetadataSpecificationDTO metadata, Long docSpecId) throws DAOException {
		Boolean isNew;
		String excMsg;

		isNew = metadata != null && metadata.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear un nuevo metadato para la definición de documento " + docSpecId + ".";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar un metadato " + metadata.getName() + " para la definición de documento " + docSpecId + ".";
		}

		try {
			checkEmptyObject(metadata, excMsg + "Especificación de metadato nula o vacía.");

			checkEmptyObject(metadata.getName(), excMsg + "Especificación de metadatos incorrecta, denominación es nula o vacía.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba una lista de documentos digitalizados, verificando cada uno de
	 * ellos.
	 * 
	 * @param documents
	 *            Relación de documentos.
	 * @param isNew
	 *            Indica si la validación sobre un nuevo objeto, o uno ya
	 *            existente.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkDocuments(Collection<ScannedDocumentDTO> documents, Boolean isNew) throws DAOException {
		if (documents != null && !documents.isEmpty()) {
			for (ScannedDocumentDTO doc: documents) {
				checkDocument(doc, isNew);
			}
		}
	}

	/**
	 * Comprueba que un documento no sea nula, posea identificador (si no es un
	 * nuevo), contenido (ruta en su defecto), el nombre del usuario de
	 * digitalización, e identificación del perfil de digitalización.
	 * 
	 * @param doc
	 *            Documento digitalizado.
	 * @param isNew
	 *            Indica si la validación sobre un nuevo objeto, o uno ya
	 *            existente.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkDocument(ScannedDocumentDTO doc, Boolean isNew) throws DAOException {
		String excMsg;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear un nuevo documento. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar el documento " + doc.getId() + ". ";
		}

		if (doc == null) {
			excMsg += "Objeto nulo o vacío.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		if (!isNew && doc.getId() == null) {
			excMsg += "Identificador nulo.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		if (doc.getContent() == null) {
			excMsg += "Contenido del documento nulo.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		try {
			checkEmptyObject(doc.getDigProcStartDate(), excMsg + "Fecha de inicio del proceso de digitalización nula.");

			checkEmptyObject(doc.getMimeType(), excMsg + "Tipo MIME del documento nulo o vacío.");

			if (!WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE.equals(doc.getMimeType().getContentType())) {
				excMsg += "Tipo de contenido no admitido para el documento.";
				LOGGER.error(excMsg);
				throw new DAOException(DAOException.CODE_998, excMsg);
			}

			checkEmptyObject(doc.getUsername(), excMsg + "Nombre del usuario de digitalización nulo o vacío.");

			if (!isNew) {
				checkEmptyObject(doc.getOrgScanProfileDocSpec(), excMsg + "Identificador de configuración de definición de documento para perfil de digitalización y unidad organizativa nulo.");
			}
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba que un documento no sea nula, posea identificador (si no es un
	 * nuevo), contenido (ruta en su defecto), el nombre del usuario de
	 * digitalización, e identificación del perfil de digitalización.
	 * 
	 * @param scannedDocLog
	 *            Traza de ejecución de documento digitalizado.
	 * @param isNew
	 *            Indica si la validación sobre un nuevo objeto, o uno ya
	 *            existente.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkScannedDocLog(ScannedDocLogDTO scannedDocLog, Boolean isNew) throws DAOException {
		String excMsg;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva traza de ejecución de documento. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar la traza de ejecución de documento " + scannedDocLog.getId() + ". ";
		}

		if (scannedDocLog == null) {
			excMsg += "Objeto nulo o vacío.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		if (!isNew && scannedDocLog.getId() == null) {
			excMsg += "Identificador nulo.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		try {
			checkEmptyObject(scannedDocLog.getStatus(), excMsg + "Estado de ejecución nulo.");

			checkEmptyObject(scannedDocLog.getStartDate(), excMsg + "Fecha de inicio de ejecución nula.");

			if (!isNew && OperationStatus.PERFORMED.equals(scannedDocLog.getStatus())) {
				checkEmptyObject(scannedDocLog.getEndDate(), excMsg + "Fecha de finalización de ejecución nula.");
			}
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba que el estado de una traza de ejecución del proceso de
	 * digitalización de un documento es en proceso o error.
	 * 
	 * @param opStatus
	 *            Estado de una traza de ejecución del proceso de digitalización
	 *            de un documento.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkInProccesScannedDocLogStatus(OperationStatus opStatus) throws DAOException {
		String excMsg;

		if (!OperationStatus.IN_PROCESS.equals(opStatus) && !OperationStatus.FAILED.equals(opStatus)) {

			excMsg = "Estado de traza de ejecución del proceso de digitalización de un documento incorrecto (" + opStatus + ").";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}
	}

	/**
	 * Comprueba que el estado de una traza de ejecución del proceso de
	 * digitalización de un documento es cerrado.
	 * 
	 * @param opStatus
	 *            Estado de una traza de ejecución del proceso de digitalización
	 *            de un documento.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkClosedScannedDocLogStatus(OperationStatus opStatus) throws DAOException {
		String excMsg;

		if (!OperationStatus.PERFORMED.equals(opStatus)) {

			excMsg = "Estado de traza de ejecución del proceso de digitalización de un documento incorrecto.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}
	}

	/**
	 * Comprueba una lista de metadatos de un documento digitalizado,
	 * verificando cada uno de ellos.
	 * 
	 * @param metadataCollection
	 *            Mapa de metadatos de documento y sus definiciones.
	 * @param docId
	 *            Identificador de documento.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkMetadataCollection(Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection, Long docId) throws DAOException {
		MetadataSpecificationDTO spec;
		if (metadataCollection != null && !metadataCollection.isEmpty()) {
			for (Iterator<MetadataSpecificationDTO> it = metadataCollection.keySet().iterator(); it.hasNext();) {
				spec = it.next();
				checkMetadata(metadataCollection.get(spec), spec, docId);
			}
		}
	}

	/**
	 * Comprueba que un metadato no sea nulo, posea denominación, identificación
	 * de su definición, y si es requerido, su valor.
	 * 
	 * @param metadata
	 *            Metadato de un documento digitalizado.
	 * @param spec
	 *            Definición del metadato de un documento digitalizado.
	 * @param docId
	 *            Identificador de documento digitalizado.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkMetadata(MetadataDTO metadata, MetadataSpecificationDTO spec, Long docId) throws DAOException {
		Boolean isNew;
		String excMsg;

		isNew = docId == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear un nuevo metadato para un documento digitalizado. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar el metadato " + docId + ".";
		}

		try {
			checkEmptyObject(metadata, excMsg + "Metadato nulo o vacío.");
		} catch (DAOException e) {
			throw e;
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba que el valor de un parámetro de un plugin es correcto conforme
	 * al tipo que lo define.
	 * 
	 * @param paramName
	 *            Nombre del parámetro.
	 * @param paramValue
	 *            Valor del parámetro.
	 * @param paramTypeId
	 *            Identificador del tipo de datos del parámetro.
	 * @param paramTypes
	 *            Tipos de datos de parámetros.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	private static void checkParamValue(String paramName, String paramValue, Long paramTypeId, List<ParameterTypeDTO> paramTypes) throws DAOException {
		Boolean found = Boolean.FALSE;
		ParameterTypeDTO paramType;
		String excMsg, stringPattern;

		stringPattern = null;

		for (Iterator<ParameterTypeDTO> it = paramTypes.iterator(); !found && it.hasNext();) {
			paramType = it.next();

			if (paramTypeId.equals(paramType.getId())) {
				stringPattern = paramType.getStringPattern();
				found = Boolean.TRUE;
			}
		}

		if (!found) {
			excMsg = "Parámetro " + paramName + " incorrecto, se ha especificado un tipo de datos no soportado.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		// Email:
		// ^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$
		// Fecha (dd/mm/yyyy):
		// (0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)
		// Hora (24): ([01]?[0-9]|2[0-3]):[0-5][0-9]
		// Dirección
		// IP:^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$
		// Nombre de dominio: ^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$
		// Uno o más digitos numéricos: \\d+
		// etc.
		if (stringPattern != null && !paramValue.matches(stringPattern)) {
			excMsg = "Parámetro " + paramName + " incorrecto, no se ha especificado con el formato correcto (" + stringPattern + ").";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}
	}

	/**
	 * Comprueba que una definición de perfil de digitalización no sea nula, y
	 * posea denominación.
	 * 
	 * @param scanProfileSpec
	 *            Especificación de perfil de digitalización.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkScanProfileSpecification(ScanProfileSpecificationDTO scanProfileSpec) throws DAOException {
		String excMsg;

		excMsg = "[WEBSCAN-MODEL] Error al actualizar la definición de perfil de digitalización " + (scanProfileSpec != null ? scanProfileSpec.getId() : "No especificado") + ".";

		try {
			checkEmptyObject(scanProfileSpec, excMsg + "Definición de perfil de digitalización nulo o vacío.");

			checkEmptyObject(scanProfileSpec.getName(), excMsg + "Denominación es nula o vacía.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba que un perfil de digitalización no sea nulo, y posea
	 * denominación.
	 * 
	 * @param scanProfile
	 *            Perfil de digitalización.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkScanProfile(ScanProfileDTO scanProfile) throws DAOException {
		Boolean isNew;
		String excMsg;

		isNew = scanProfile != null && scanProfile.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear un nuevo Perfil de digitalización. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar el Perfil de digitalización " + (scanProfile != null ? scanProfile.getId() : "No especificado") + ".";
		}

		try {
			checkEmptyObject(scanProfile, excMsg + "Perfil de digitalización nulo o vacío.");

			checkEmptyObject(scanProfile.getName(), excMsg + "Denominación nula o vacía.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba una lista de configuraciones de perfil de digitalización para
	 * unidades organizativas, verificando cada una de ellas.
	 * 
	 * @param scanProfileOrgUnits
	 *            Relación de configuraciones de perfil de digitalización para
	 *            unidades organizativas.
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkScanProfileOrgUnits(Collection<ScanProfileOrgUnitDTO> scanProfileOrgUnits, Long scanProfileId) throws DAOException {
		if (scanProfileOrgUnits != null && !scanProfileOrgUnits.isEmpty()) {
			for (ScanProfileOrgUnitDTO spou: scanProfileOrgUnits) {
				checkScanProfileOrgUnit(spou, scanProfileId);
			}
		}
	}

	/**
	 * Comprueba que una configuración de perfil de digitalización para unidad
	 * organizativa no sea nula, posea identificador de unidad organizativa y
	 * perfil de digitalización, y que este último coincida con el identificador
	 * especificado mediante el parámetro de entrada scanProfileId.
	 * denominación.
	 * 
	 * @param scanProfileOrgUnit
	 *            Configuración de perfil de digitalización para unidad
	 *            organizativa.
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkScanProfileOrgUnit(ScanProfileOrgUnitDTO scanProfileOrgUnit, Long scanProfileId) throws DAOException {
		Boolean isNew;
		String excMsg;

		isNew = scanProfileOrgUnit != null && scanProfileOrgUnit.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva configuración para una unidad organizativa del Perfil de digitalización " + scanProfileId + ".";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar la configuración " + scanProfileOrgUnit.getId() + " para una unidad organizativa del Perfil de digitalización " + scanProfileId + ".";
		}

		try {
			checkEmptyObject(scanProfileOrgUnit, excMsg + "Configuración para una unidad organizativa de perfil de digitalización nula o vacía.");

			checkEmptyObject(scanProfileOrgUnit.getOrgUnit(), excMsg + "Unidad organizativa nula o vacía.");

			checkEmptyObject(scanProfileOrgUnit.getScanProfile(), excMsg + "Perfil de digitalización no especificado.");

			if (!scanProfileOrgUnit.getScanProfile().equals(scanProfileId)) {
				excMsg += "Configuración para una unidad organizativa de Perfil de digitalización incorrecta, Perfil de digitalización incoherente. " + "Valor informado " + scanProfileOrgUnit.getScanProfile() + ", esperado " + scanProfileId + ".";
				LOGGER.error(excMsg);
				throw new DAOException(DAOException.CODE_998, excMsg);
			}
		} catch (DAOException e) {
			throw e;
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba que una fase de proceso de digitalización no sea nula, posea
	 * identificador y denominación. denominación.
	 * 
	 * @param scanProcessStage
	 *            Fase de proceso de digitalización.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkScanProcessStage(ScanProcessStageDTO scanProcessStage) throws DAOException {
		String excMsg;

		excMsg = "[WEBSCAN-MODEL] Error al obtener una fase de proceso de  digitalización. ";

		try {
			checkEmptyObject(scanProcessStage, excMsg + "Fase de proceso de digitalización nula o vacía.");

			checkEmptyObject(scanProcessStage.getId(), excMsg + "Identificador nulo o vacío.");

			checkEmptyObject(scanProcessStage.getName(), excMsg + "Denominación nula o vacía.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba una lista de campos de ordenación de consulta sobre la entidad
	 * AuditOperation de BBDD, verificando cada una de ellas.
	 * 
	 * @param orderByColumns
	 *            Relación de de campos de ordenación de consulta la entidad
	 *            AuditOperation de BBDD.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkAuditOperationGroupByClause(List<EntityOrderByClause> orderByColumns) throws DAOException {
		String excMsg;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (EntityOrderByClause eobc: orderByColumns) {
				if (!(eobc instanceof AuditOperationOrderByClause)) {

					excMsg = "[WEBSCAN-MODEL] Error recuperando trazas de auditoría registradas en el sistema, " + "clausula ORDER BY incorrecta (tipo esperado " + AuditOperationOrderByClause.class + ", tipo informado " + orderByColumns.get(0).getClass() + ").";
					LOGGER.error(excMsg);
					throw new DAOException(DAOException.CODE_998, excMsg);
				}
			}
		}
	}

	/**
	 * Comprueba una lista de campos de ordenación de consulta sobre la entidad
	 * AuditOperation de BBDD, verificando cada una de ellas.
	 * 
	 * @param orderByColumns
	 *            Relación de de campos de ordenación de consulta la entidad
	 *            AuditOperation de BBDD.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkAuditEventGroupByClause(List<EntityOrderByClause> orderByColumns) throws DAOException {
		String excMsg;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (EntityOrderByClause eobc: orderByColumns) {
				if (!(eobc instanceof AuditEventOrderByClause)) {

					excMsg = "[WEBSCAN-MODEL] Error recuperando trazas de auditoría registradas en el sistema, " + "clausula ORDER BY incorrecta (tipo esperado " + AuditEventOrderByClause.class + ", tipo informado " + orderByColumns.get(0).getClass() + ").";
					LOGGER.error(excMsg);
					throw new DAOException(DAOException.CODE_998, excMsg);
				}
			}
		}
	}

	/**
	 * Comprueba una lista de campos de ordenación de consulta sobre la entidad
	 * ScannedDocument de BBDD, verificando cada una de ellas.
	 * 
	 * @param orderByColumns
	 *            Relación de de campos de ordenación de consulta la entidad
	 *            ScannedDocument de BBDD.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkDocumentGroupByClause(List<EntityOrderByClause> orderByColumns) throws DAOException {
		String excMsg;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (EntityOrderByClause eobc: orderByColumns) {
				if (!(eobc instanceof DocumentOrderByClause)) {

					excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados en el sistema, " + "clausula ORDER BY incorrecta (tipo esperado " + DocumentOrderByClause.class + ", tipo informado " + orderByColumns.get(0).getClass() + ").";
					LOGGER.error(excMsg);
					throw new DAOException(DAOException.CODE_998, excMsg);
				}
			}
		}
	}

	/**
	 * Comprueba una lista de campos de ordenación de consulta sobre la entidad
	 * DocumentSpecification de BBDD, verificando cada una de ellas.
	 * 
	 * @param orderByColumns
	 *            Relación de de campos de ordenación de consulta la entidad
	 *            DocumentSpecification de BBDD.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkDocumentSpecGroupByClause(List<EntityOrderByClause> orderByColumns) throws DAOException {
		String excMsg;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (EntityOrderByClause eobc: orderByColumns) {
				if (!(eobc instanceof DocumentSpecOrderByClause)) {

					excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de documentos registradas en el sistema, " + "clausula ORDER BY incorrecta (tipo esperado " + DocumentSpecOrderByClause.class + ", tipo informado " + orderByColumns.get(0).getClass() + ").";
					LOGGER.error(excMsg);
					throw new DAOException(DAOException.CODE_998, excMsg);
				}
			}
		}
	}

	/**
	 * Comprueba una lista de campos de ordenación de consulta sobre la entidad
	 * PluginSpecification de BBDD, verificando cada una de ellas.
	 * 
	 * @param orderByColumns
	 *            Relación de de campos de ordenación de consulta la entidad
	 *            PluginSpecification de BBDD.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkPluginSpecGroupByClause(List<EntityOrderByClause> orderByColumns) throws DAOException {
		String excMsg;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (EntityOrderByClause eobc: orderByColumns) {
				if (!(eobc instanceof PluginSpecOrderByClause)) {

					excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de plugins registradas en el sistema, " + "clausula ORDER BY incorrecta (tipo esperado " + PluginSpecOrderByClause.class + ", tipo informado " + orderByColumns.get(0).getClass() + ").";
					LOGGER.error(excMsg);
					throw new DAOException(DAOException.CODE_998, excMsg);
				}
			}
		}
	}

	/**
	 * Comprueba una lista de campos de ordenación de consulta sobre la entidad
	 * ScanProfile de BBDD, verificando cada una de ellas.
	 * 
	 * @param orderByColumns
	 *            Relación de de campos de ordenación de consulta la entidad
	 *            ScanProfile de BBDD.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkScanProfileGroupByClause(List<EntityOrderByClause> orderByColumns) throws DAOException {
		String excMsg;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (EntityOrderByClause eobc: orderByColumns) {
				if (!(eobc instanceof ScanProfileOrderByClause)) {

					excMsg = "[WEBSCAN-MODEL] Error recuperando perfiles de digitalización registrados en el sistema, " + "clausula ORDER BY incorrecta (tipo esperado " + ScanProfileOrderByClause.class + ", tipo informado " + orderByColumns.get(0).getClass() + ").";
					LOGGER.error(excMsg);
					throw new DAOException(DAOException.CODE_998, excMsg);
				}
			}
		}
	}

	/**
	 * Comprueba una lista de campos de ordenación de consulta sobre la entidad
	 * ScanProfileSpecification de BBDD, verificando cada una de ellas.
	 * 
	 * @param orderByColumns
	 *            Relación de de campos de ordenación de consulta la entidad
	 *            ScanProfileSpecification de BBDD.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkScanProfileSpecGroupByClause(List<EntityOrderByClause> orderByColumns) throws DAOException {
		String excMsg;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			for (EntityOrderByClause eobc: orderByColumns) {
				if (!(eobc instanceof ScanProfileSpecOrderByClause)) {

					excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de perfiles de digitalización registradas en el sistema, " + "clausula ORDER BY incorrecta (tipo esperado " + ScanProfileSpecOrderByClause.class + ", tipo informado " + orderByColumns.get(0).getClass() + ").";
					LOGGER.error(excMsg);
					throw new DAOException(DAOException.CODE_998, excMsg);
				}
			}
		}
	}

	/**
	 * Comprueba que una unidad organizativa no sea nula, posea identificador
	 * (si no es nueva), denominación e identificador externo.
	 * 
	 * @param orgUnit
	 *            Unidad organizativa.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkOrganizationUnit(OrganizationUnitDTO orgUnit) throws DAOException {
		Boolean isNew;
		String excMsg;

		isNew = orgUnit != null && orgUnit.getId() == null;

		if (isNew) {
			excMsg = "[WEBSCAN-MODEL] Error al crear una nueva unidad organizativa. ";
		} else {
			excMsg = "[WEBSCAN-MODEL] Error al actualizar la unidad organizativa " + (orgUnit != null ? orgUnit.getId() : "No especificada") + ". ";
		}

		if (orgUnit == null) {
			excMsg += "Objeto nulo o vacío.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		if (!isNew && orgUnit.getId() == null) {
			excMsg += "Identificador nulo.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		if (orgUnit.getExternalId() == null || orgUnit.getExternalId().isEmpty()) {
			excMsg += "Identificador externo nulo o vacío.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}

		try {
			checkEmptyObject(orgUnit.getExternalId(), excMsg + "Identificador externo nulo o vacío.");

			checkEmptyObject(orgUnit.getName(), excMsg + "Denominación nula o vacía.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba que una traza de auditoría no sea nula, posea denominación,
	 * estado, fecha de registro y fecha de establecimiento del estado.
	 * 
	 * @param auditOperation
	 *            Traza de auditoría.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkAuditOperation(AuditOperationDTO auditOperation) throws DAOException {
		String excMsg;

		excMsg = "[WEBSCAN-MODEL] Error al crear una nueva traza de auditoría. ";

		try {
			checkEmptyObject(auditOperation, excMsg + "Objeto nulo o vacío.");

			checkEmptyObject(auditOperation.getLastUpdateDate(), excMsg + "Fecha de establecimiento de estado nula.");

			checkEmptyObject(auditOperation.getName(), excMsg + "Denominación nula o vacía.");

			checkEmptyObject(auditOperation.getStartDate(), excMsg + "Fecha de registro de la traza de auditoría nula.");

			checkEmptyObject(auditOperation.getStatus(), excMsg + "Estado de la traza de auditoría nulo.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba que un evento de una traza de auditoría no sea nulo, posea
	 * denominación, fecha de registro, resultado y usuario de ejecución.
	 * 
	 * @param auditEvent
	 *            Evento de una traza de auditoría.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkAuditEvent(AuditEventDTO auditEvent) throws DAOException {
		String excMsg;

		excMsg = "[WEBSCAN-MODEL] Error al crear un nuevo evento de auditoría. ";

		try {
			checkEmptyObject(auditEvent, excMsg + "Objeto nulo o vacío.");

			checkEmptyObject(auditEvent.getEventName(), excMsg + "Denominación nula o vacía.");

			checkEmptyObject(auditEvent.getEventDate(), excMsg + "Fecha en la que se produce el evento nula o vacía.");

			checkEmptyObject(auditEvent.getEventResult(), excMsg + "Resultado del evento nulo o vacío.");

			checkEmptyObject(auditEvent.getEventUser(), excMsg + "Usuario asociado al evento nulo o vacío.");
		} catch (WebscanException e) {
			throw new DAOException(e.getCode(), e.getMessage(), e.getCause());
		}
	}

	/**
	 * Comprueba una lista de eventos de una traza de auditoría, verificando
	 * cada uno de ellos.
	 * 
	 * @param auditEvents
	 *            Relación de eventos de una traza de auditoría.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkAuditEvents(List<AuditEventDTO> auditEvents) throws DAOException {

		if (auditEvents != null && !auditEvents.isEmpty()) {

			for (AuditEventDTO event: auditEvents) {
				checkAuditEvent(event);
			}
		}
	}

	/**
	 * Comprueba que el estado de una traza de auditoría es cerrado.
	 * 
	 * @param auditOpStatus
	 *            Estado de una traza de auditoría.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkClosedAuditOperationStatus(AuditOperationStatus auditOpStatus) throws DAOException {
		String excMsg;

		if (!AuditOperationStatus.CLOSED_ERROR.equals(auditOpStatus) && !AuditOperationStatus.CLOSED_OK.equals(auditOpStatus)) {

			excMsg = "Estado de traza de auditoría incorrecto.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}
	}

	/**
	 * Comprueba que el estado de una traza de auditoría es abierto.
	 * 
	 * @param auditOpStatus
	 *            Estado de una traza de auditoría.
	 * @throws DAOException
	 *             Si los datos no son válidos.
	 */
	static void checkOpenAuditOperationStatus(AuditOperationStatus auditOpStatus) throws DAOException {
		String excMsg;

		if (!AuditOperationStatus.OPEN.equals(auditOpStatus)) {

			excMsg = "Estado de traza de auditoría incorrecto.";
			LOGGER.error(excMsg);
			throw new DAOException(DAOException.CODE_998, excMsg);
		}
	}
}
