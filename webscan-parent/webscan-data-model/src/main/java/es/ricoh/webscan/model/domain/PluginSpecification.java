/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.PluginSpecification.java.</p>
 * <b>Descripción:</b><p> Entidad JPA definición de plugin.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA definición de plugin.
 * <p>
 * Clase PluginSpecification.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "PLUGIN_SPEC", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME" }) })
public class PluginSpecification implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Atributo DESCRIPTION de la entidad.
	 */
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * Atributo ACTIVE de la entidad.
	 */
	@Column(name = "ACTIVE")
	private Boolean active = Boolean.TRUE;

	/**
	 * Relación de entidades definición de parámetro de la especificación.
	 */
	@OneToMany(mappedBy = "pluginSpecification", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ParameterSpecification.class)
	private List<ParameterSpecification> parameters = new ArrayList<ParameterSpecification>();

	/**
	 * Relación de entidades plugin definidos por la especificación.
	 */
	@OneToMany(mappedBy = "specification", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.Plugin.class)
	private List<Plugin> plugins = new ArrayList<Plugin>();

	/**
	 * Relación de fases de procesos de digitalización para las que pueden ser
	 * empleados plugins definidos por una definición.
	 */
	@ManyToMany(mappedBy = "pluginSpecifications", fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScanProcessStage.class)
	private List<ScanProcessStage> scanProcessStages = new ArrayList<ScanProcessStage>();

	/**
	 * Constructor sin argumentos.
	 */
	public PluginSpecification() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el atributo DESCRIPTION de la entidad.
	 * 
	 * @return el atributo DESCRIPTION de la entidad.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para el atributo DESCRIPTION de la entidad.
	 * 
	 * @param aDescription
	 *            nuevo valor para el atributo DESCRIPTION de la entidad.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene el atributo ACTIVE de la entidad.
	 * 
	 * @return el atributo ACTIVE de la entidad.
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Establece un nuevo valor para el atributo ACTIVE de la entidad.
	 * 
	 * @param activeParam
	 *            nuevo valor para el atributo ACTIVE de la entidad.
	 */
	public void setActive(Boolean activeParam) {
		this.active = activeParam;
	}

	/**
	 * Obtiene la relación de definiciones de parámetros de la especificación.
	 * 
	 * @return la relación de definiciones de parámetros de la especificación.
	 */
	public List<ParameterSpecification> getParameters() {
		return parameters;
	}

	/**
	 * Establece una nueva relación de definiciones de parámetros de la
	 * especificación.
	 * 
	 * @param anyParameters
	 *            Nueva relación de definiciones de parámetros de la
	 *            especificación.
	 */
	public void setParameters(List<ParameterSpecification> anyParameters) {
		this.parameters.clear();
		if (anyParameters != null && !anyParameters.isEmpty()) {
			this.parameters.addAll(anyParameters);
		}
	}

	/**
	 * Obtiene la relación de plugins definidos por la especificación.
	 * 
	 * @return la relación de plugins definidos por la especificación.
	 */
	public List<Plugin> getPlugins() {
		return plugins;
	}

	/**
	 * Establece una nueva relación de plugins definidos por la especificación.
	 * 
	 * @param anyPlugins
	 *            Nueva relación de plugins definidos por la especificación.
	 */
	public void setPlugins(List<Plugin> anyPlugins) {
		this.plugins.clear();
		if (anyPlugins != null && !anyPlugins.isEmpty()) {
			this.plugins.addAll(anyPlugins);
		}
	}

	/**
	 * Obtiene la relación de fases de procesos de digitalización para las que
	 * pueden ser empleados plugins definidos por una definición.
	 * 
	 * @return la relación de fases de procesos de digitalización para las que
	 *         pueden ser empleados plugins definidos por una definición.
	 */
	public List<ScanProcessStage> getScanProcessStages() {
		return scanProcessStages;
	}

	/**
	 * Establece una nueva relación de fases de procesos de digitalización para
	 * las que pueden ser empleados plugins definidos por una definición.
	 * 
	 * @param anyScanProcessStages
	 *            Nueva relación de fases de procesos de digitalización para las
	 *            que pueden ser empleados plugins definidos por una definición.
	 */
	public void setScanProcessStages(List<ScanProcessStage> anyScanProcessStages) {
		this.scanProcessStages.clear();

		if (anyScanProcessStages != null && !anyScanProcessStages.isEmpty()) {
			this.scanProcessStages.addAll(anyScanProcessStages);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PluginSpecification other = (PluginSpecification) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
