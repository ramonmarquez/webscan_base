/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.Metadata.java.</p>
 * <b>Descripción:</b><p> Entidad JPA metadato.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA metadato.
 * <p>
 * Clase Metadata.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "DOC_METADATA", uniqueConstraints = { @UniqueConstraint(columnNames = { "METADATA_SPEC_ID", "SCANNED_DOC_ID" }) })
public class Metadata implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "DOC_METADATA_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "DOC_METADATA_ID_SEQ", sequenceName = "DOC_METADATA_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo VALUE de la entidad.
	 */
	@Column(name = "VALUE")
	private String value;

	/**
	 * Entidad documento al que pertenece el metadato.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.ScannedDocument.class)
	@JoinColumn(name = "SCANNED_DOC_ID")
	private ScannedDocument document;

	/**
	 * Entidad especificación de metadato que define el metadato.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.MetadataSpecification.class)
	@JoinColumn(name = "METADATA_SPEC_ID")
	private MetadataSpecification specification;

	/**
	 * Constructor sin argumentos.
	 */
	public Metadata() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo VALUE de la entidad.
	 * 
	 * @return el atributo VALUE de la entidad.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Establece un nuevo valor para el atributo VALUE de la entidad.
	 * 
	 * @param aValue
	 *            nuevo valor para el atributo VALUE de la entidad.
	 */
	public void setValue(String aValue) {
		this.value = aValue;
	}

	/**
	 * Obtiene el documento al que pertenece el metadato.
	 * 
	 * @return el documento al que pertenece el metadato.
	 */
	public ScannedDocument getDocument() {
		return document;
	}

	/**
	 * Establece un nuevo documento al que pertenece el metadato.
	 * 
	 * @param aDocument
	 *            Nuevo documento al que pertenece el metadato.
	 */
	public void setDocument(ScannedDocument aDocument) {
		this.document = aDocument;
	}

	/**
	 * Obtiene la especificación de metadato que define el metadato.
	 * 
	 * @return la especificación de metadato que define el metadato.
	 */
	public MetadataSpecification getSpecification() {
		return specification;
	}

	/**
	 * Establece una nueva especificación de metadato que define el metadato.
	 * 
	 * @param aSpecification
	 *            Nueva especificación de metadato que define el metadato.
	 */
	public void setSpecification(MetadataSpecification aSpecification) {
		this.specification = aSpecification;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((document == null) ? 0 : document.hashCode());
		result = prime * result + ((specification == null) ? 0 : specification.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Metadata other = (Metadata) obj;
		if (document == null) {
			if (other.document != null)
				return false;
		} else if (!document.equals(other.document))
			return false;
		if (specification == null) {
			if (other.specification != null)
				return false;
		} else if (!specification.equals(other.specification))
			return false;
		return true;
	}

}
