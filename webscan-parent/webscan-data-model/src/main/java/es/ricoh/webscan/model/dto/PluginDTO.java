/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.PluginDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA plugin.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dto;

import java.io.Serializable;

/**
 * DTO para la entidad JPA plugin.
 * <p>
 * Clase PluginDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class PluginDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación de la entidad.
	 */
	private String name;

	/**
	 * Descripción de la entidad.
	 */
	private String description;

	/**
	 * Indicador de activación de la entidad.
	 */
	private Boolean active;

	/**
	 * Denominación de la especificación que define el plugin.
	 */
	private String specification;

	/**
	 * Constructor sin argumentos.
	 */
	public PluginDTO() {
		super();
	}

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación de la entidad.
	 * 
	 * @return la denominación de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene la descripción de la entidad.
	 * 
	 * @return la descripción de la entidad.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Establece un nuevo valor para la descripción de la entidad.
	 * 
	 * @param aDescription
	 *            nuevo valor para la descripción de la entidad.
	 */
	public void setDescription(String aDescription) {
		this.description = aDescription;
	}

	/**
	 * Obtiene el indicador de activación de la entidad.
	 * 
	 * @return el indicador de activación de la entidad.
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Establece un nuevo valor para el indicador de activación de la entidad.
	 * 
	 * @param activeParam
	 *            nuevo valor para el indicador de activación de la entidad.
	 */
	public void setActive(Boolean activeParam) {
		this.active = activeParam;
	}

	/**
	 * Obtiene la denominación de la especificación que define el plugin.
	 * 
	 * @return la denominación de la especificación que define el plugin.
	 */
	public String getSpecification() {
		return specification;
	}

	/**
	 * Establece una nueva definción del plugin.
	 * 
	 * @param aSpecification
	 *            nueva definción del plugin.
	 */
	void setSpecification(String aSpecification) {
		this.specification = aSpecification;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((specification == null) ? 0 : specification.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PluginDTO other = (PluginDTO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (specification == null) {
			if (other.specification != null)
				return false;
		} else if (!specification.equals(other.specification))
			return false;
		return true;
	}

}
