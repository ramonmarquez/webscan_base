/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.OrganizationUnit.java.</p>
 * <b>Descripción:</b><p> Entidad JPA unidad organizativa.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad JPA unidad organizativa.
 * <p>
 * Clase OrganizationUnit.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "ORG_UNIT", uniqueConstraints = { @UniqueConstraint(columnNames = { "EXT_ID" }) })
public class OrganizationUnit implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "ORG_UNIT_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ORG_UNIT_ID_SEQ", sequenceName = "ORG_UNIT_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo EXT_ID de la entidad.
	 */
	@Column(name = "EXT_ID")
	private String externalId;

	/**
	 * Atributo NAME de la entidad.
	 */
	@Column(name = "NAME")
	private String name;

	/**
	 * Relación de entidad unidad organizativa que son hijas.
	 */
	@OneToMany(mappedBy = "parentOrg", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.OrganizationUnit.class)
	private List<OrganizationUnit> childOrgs = new ArrayList<OrganizationUnit>();

	/**
	 * Entidad unidad organizativa padre.
	 */
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = es.ricoh.webscan.model.domain.OrganizationUnit.class)
	@JoinColumn(name = "PARENT_ORG_ID")
	private OrganizationUnit parentOrg;

	/**
	 * Relación de perfiles de digitalización configurados.
	 */
	@OneToMany(mappedBy = "orgUnit", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true, targetEntity = es.ricoh.webscan.model.domain.ScanProfileOrgUnit.class)
	private List<ScanProfileOrgUnit> scanProfiles = new ArrayList<ScanProfileOrgUnit>();

	/**
	 * Constructor sin argumentos.
	 */
	public OrganizationUnit() {
		super();
	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo EXT_ID de la entidad.
	 * 
	 * @return el atributo EXT_ID de la entidad.
	 */
	public String getExternalId() {
		return externalId;
	}

	/**
	 * Establece un nuevo valor para el atributo EXT_ID de la entidad.
	 * 
	 * @param anExternalId
	 *            nuevo valor para el atributo EXT_ID de la entidad.
	 */
	public void setExternalId(String anExternalId) {
		this.externalId = anExternalId;
	}

	/**
	 * Obtiene el atributo NAME de la entidad.
	 * 
	 * @return el atributo NAME de la entidad.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para el atributo NAME de la entidad.
	 * 
	 * @param aName
	 *            nuevo valor para el atributo NAME de la entidad.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene la relación de entidades unidad organizativa que son hijas.
	 * 
	 * @return la relación de entidades unidad organizativa que son hijas.
	 */
	public List<OrganizationUnit> getChildOrgs() {
		return childOrgs;
	}

	/**
	 * Establece una nueva relación de entidades unidad organizativa que son
	 * hijas.
	 * 
	 * @param anyChildOrgs
	 *            Nueva relación de entidades unidad organizativa que son hijas.
	 */
	public void setChildOrgs(List<OrganizationUnit> anyChildOrgs) {
		this.childOrgs.clear();
		if (anyChildOrgs != null && !anyChildOrgs.isEmpty()) {
			this.childOrgs.addAll(anyChildOrgs);
		}
	}

	/**
	 * Obtiene la unidad organizativa padre.
	 * 
	 * @return la unidad organizativa padre.
	 */
	public OrganizationUnit getParentOrg() {
		return parentOrg;
	}

	/**
	 * Establece una nueva unidad organizativa padre.
	 * 
	 * @param aParentOrg
	 *            Nueva unidad organizativa padre.
	 */
	public void setParentOrg(OrganizationUnit aParentOrg) {
		this.parentOrg = aParentOrg;
	}

	/**
	 * Obtiene la relación de perfiles de digitalización configurados.
	 * 
	 * @return la relación de perfiles de digitalización configurados.
	 */
	public List<ScanProfileOrgUnit> getScanProfiles() {
		return scanProfiles;
	}

	/**
	 * Establece una nueva relación de perfiles de digitalización configurados.
	 * 
	 * @param anyScanProfiles
	 *            Nueva relación de perfiles de digitalización configurados.
	 */
	public void setScanProfiles(List<ScanProfileOrgUnit> anyScanProfiles) {
		this.scanProfiles.clear();
		if (anyScanProfiles != null && !anyScanProfiles.isEmpty()) {
			this.scanProfiles.addAll(anyScanProfiles);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationUnit other = (OrganizationUnit) obj;
		if (externalId == null) {
			if (other.externalId != null)
				return false;
		} else if (!externalId.equals(other.externalId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
