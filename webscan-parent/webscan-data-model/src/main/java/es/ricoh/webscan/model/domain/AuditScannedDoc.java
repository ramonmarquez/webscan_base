/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.model.domain.AuditScannedDoc.java.</p>
* <b>Descripción:</b><p> Entidad histórico de documentos.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.model.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad histórico de documentos.
 * <p>
 * Clase AuditScannedDoc.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Entity
@Table(name = "AUDIT_DOC")
public class AuditScannedDoc implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Atributo ID de la entidad.
	 */
	@Id
	@GeneratedValue(generator = "AUDIT_DOC_ID_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AUDIT_DOC_ID_SEQ", sequenceName = "AUDIT_DOC_ID_SEQ", allocationSize = 2)
	@Column(name = "ID")
	private Long id;

	/**
	 * Atributo HASH de la entidad.
	 */
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "HASH", nullable = true)
	private byte[ ] hash;

	/**
	 * Atributo DIG_PROC_START_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DIG_PROC_START_DATE")
	private Date digProcStartDate;

	/**
	 * Atributo DIG_PROC_END_DATE de la entidad.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DIG_PROC_END_DATE")
	private Date digProcEndDate;

	/**
	 * Atributo BATCH_ID de la entidad.
	 */
	@Column(name = "BATCH_ID")
	private String batchId;

	/**
	 * Atributo BATCH_REQ_ID de la entidad.
	 */
	@Column(name = "BATCH_REQ_ID")
	private String batchReqId;

	/**
	 * Atributo MIME_TYPE de la entidad.
	 */
	@Column(name = "MIME_TYPE")
	private String mimeType;

	/**
	 * Atributo SIGNATURE_TYPE de la entidad.
	 */
	@Column(name = "SIGNATURE_TYPE")
	@Enumerated(EnumType.STRING)
	private SignatureType signatureType;

	/**
	 * Atributo USERNAME de la entidad.
	 */
	@Column(name = "USERNAME")
	private String username;

	/**
	 * Atributo USER_DPTOS de la entidad.
	 */
	@Column(name = "USER_DPTOS")
	private String functionalOrgs;

	/**
	 * Atributo DOC_SPEC_NAME de la entidad.
	 */
	@Column(name = "DOC_SPEC_NAME")
	private String docSpecName;

	/**
	 * Traza de auditoría asociado al proceso de digitalización del documento.
	 */
	@OneToOne(fetch = FetchType.EAGER, targetEntity = es.ricoh.webscan.model.domain.AuditOperation.class)
	@JoinColumn(name = "AUDIT_OP_ID")
	private AuditOperation auditOperation;

	/**
	 * Constructor sin argumentos.
	 */
	public AuditScannedDoc() {

	}

	/**
	 * Obtiene el atributo ID de la entidad.
	 * 
	 * @return el atributo ID de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el atributo ID de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el atributo ID de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene el atributo HASH de la entidad.
	 * 
	 * @return el atributo HASH de la entidad.
	 */
	public byte[ ] getHash() {
		return hash;
	}

	/**
	 * Establece un nuevo valor para el atributo HASH de la entidad.
	 * 
	 * @param aHash
	 *            nuevo valor para el atributo HASH de la entidad.
	 */
	public void setHash(byte[ ] aHash) {
		this.hash = aHash;
	}

	/**
	 * Obtiene el atributo DIG_PROC_START_DATE de la entidad.
	 * 
	 * @return el atributo DIG_PROC_START_DATE de la entidad.
	 */
	public Date getDigProcStartDate() {
		return digProcStartDate;
	}

	/**
	 * Establece un nuevo valor para el atributo DIG_PROC_START_DATE de la
	 * entidad.
	 * 
	 * @param aDigProcStartDate
	 *            nuevo valor para el atributo DIG_PROC_START_DATE de la
	 *            entidad.
	 */
	public void setDigProcStartDate(Date aDigProcStartDate) {
		this.digProcStartDate = aDigProcStartDate;
	}

	/**
	 * Obtiene el atributo DIG_PROC_END_DATE de la entidad.
	 * 
	 * @return el atributo DIG_PROC_END_DATE de la entidad.
	 */
	public Date getDigProcEndDate() {
		return digProcEndDate;
	}

	/**
	 * Establece un nuevo valor para el atributo DIG_PROC_END_DATE de la
	 * entidad.
	 * 
	 * @param aDigProcEndDate
	 *            nuevo valor para el atributo DIG_PROC_END_DATE de la entidad.
	 */
	public void setDigProcEndDate(Date aDigProcEndDate) {
		this.digProcEndDate = aDigProcEndDate;
	}

	/**
	 * Obtiene el atributo BATCH_ID de la entidad.
	 * 
	 * @return el atributo BATCH_ID de la entidad.
	 */
	public String getBatchId() {
		return batchId;
	}

	/**
	 * Establece un nuevo valor para el atributo BATCH_ID de la entidad.
	 * 
	 * @param aBatchId
	 *            nuevo valor para el atributo BATCH_ID de la entidad.
	 */
	public void setBatchId(String aBatchId) {
		this.batchId = aBatchId;
	}

	/**
	 * Obtiene el atributo BATCH_REQ_ID de la entidad.
	 * 
	 * @return el atributo BATCH_REQ_ID de la entidad.
	 */
	public String getBatchReqId() {
		return batchReqId;
	}

	/**
	 * Establece un nuevo valor para el atributo BATCH_REQ_ID de la entidad.
	 * 
	 * @param aBatchReqId
	 *            nuevo valor para el atributo BATCH_REQ_ID de la entidad.
	 */
	public void setBatchReqId(String aBatchReqId) {
		this.batchReqId = aBatchReqId;
	}

	/**
	 * Obtiene el atributo MIME_TYPE de la entidad.
	 * 
	 * @return el atributo MIME_TYPE de la entidad.
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Establece un nuevo valor para el atributo MIME_TYPE de la entidad.
	 * 
	 * @param aMimeType
	 *            nuevo valor para el atributo MIME_TYPE de la entidad.
	 */
	public void setMimeType(String aMimeType) {
		this.mimeType = aMimeType;
	}

	/**
	 * Obtiene el atributo SIGNATURE_TYPE de la entidad.
	 * 
	 * @return el atributo SIGNATURE_TYPE de la entidad.
	 */
	public SignatureType getSignatureType() {
		return signatureType;
	}

	/**
	 * Establece un nuevo valor para el atributo SIGNATURE_TYPE de la entidad.
	 * 
	 * @param aSignatureType
	 *            nuevo valor para el atributo SIGNATURE_TYPE de la entidad.
	 */
	public void setSignatureType(SignatureType aSignatureType) {
		this.signatureType = aSignatureType;
	}

	/**
	 * Obtiene el atributo USERNAME de la entidad.
	 * 
	 * @return el atributo USERNAME de la entidad.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece un nuevo valor para el atributo USERNAME de la entidad.
	 * 
	 * @param anUsername
	 *            nuevo valor para el atributo USERNAME de la entidad.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene el atributo USER_DPTOS de la entidad.
	 * 
	 * @return el atributo USER_DPTOS de la entidad.
	 */
	public String getFunctionalOrgs() {
		return functionalOrgs;
	}

	/**
	 * Establece un nuevo valor para el atributo USER_DPTOS de la entidad.
	 * 
	 * @param anyFunctionalOrgs
	 *            nuevo valor para el atributo USER_DPTOS de la entidad.
	 */
	public void setFunctionalOrgs(String anyFunctionalOrgs) {
		this.functionalOrgs = anyFunctionalOrgs;
	}

	/**
	 * Obtiene el atributo DOC_SPEC_NAME de la entidad.
	 * 
	 * @return el atributo DOC_SPEC_NAME de la entidad.
	 */
	public String getDocSpecName() {
		return docSpecName;
	}

	/**
	 * Establece un nuevo valor para el atributo DOC_SPEC_NAME de la entidad.
	 * 
	 * @param aDocSpecName
	 *            nuevo valor para el atributo DOC_SPEC_NAME de la entidad.
	 */
	public void setDocSpecName(String aDocSpecName) {
		this.docSpecName = aDocSpecName;
	}

	/**
	 * Obtiene la traza de auditoría asociado al proceso de digitalización del
	 * documento.
	 * 
	 * @return la traza de auditoría asociado al proceso de digitalización del
	 *         documento.
	 */
	public AuditOperation getAuditOperation() {
		return auditOperation;
	}

	/**
	 * Establece la traza de auditoría asociado al proceso de digitalización del
	 * documento.
	 * 
	 * @param anAuditOperation
	 *            traza de auditoría asociado al proceso de digitalización del
	 *            documento.
	 */
	public void setAuditOperation(AuditOperation anAuditOperation) {
		this.auditOperation = anAuditOperation;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditScannedDoc other = (AuditScannedDoc) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
