/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.domain.AuditOperationName.java.</p>
 * <b>Descripción:</b><p> Clase que define el conjunto de denominaciones que pueden ser asignados a una traza de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.domain;

/**
 * Clase que define el conjunto de denominaciones que pueden ser asignados a una
 * traza de auditoría.
 * <p>
 * Clase AuditOperationName.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public enum AuditOperationName {

	/**
	 * Traza USER LOGIN.
	 */
	USER_LOGIN("USER LOGIN"),
	/**
	 * Traza USER UPDATE PASSWORD.
	 */
	USER_UPDATE_PASSWORD("USER UPDATE PASSWORD"),
	/**
	 * Traza USER RECOVERY PASSWORD.
	 */
	USER_RECOVERY_PASSWORD("USER RECOVERY PASSWORD"),
	/**
	 * Traza ORG UNIT CREATE.
	 */
	ORG_UNIT_CREATE("ORG UNIT CREATE"),
	/**
	 * Traza ORG UNIT UPDATE.
	 */
	ORG_UNIT_UPDATE("ORG UNIT UPDATE"),
	/**
	 * Traza ORG UNIT DELETE.
	 */
	ORG_UNIT_DELETE("ORG UNIT DELETE"),
	/**
	 * Traza REMOVE USERS.
	 */
	REMOVE_USERS("REMOVE USERS"),
	/**
	 * Traza CREATE USER.
	 */
	CREATE_USER("CREATE USER"),
	/**
	 * Traza UPDATE USER.
	 */
	UPDATE_USER("UPDATE USER"),
	/**
	 * Traza PLUGIN CREATE.
	 */
	PLUGIN_CREATE("PLUGIN CREATE"),
	/**
	 * Traza PLUGIN UPDATE.
	 */
	PLUGIN_UPDATE("PLUGIN UPDATE"),
	/**
	 * Traza PLUGIN REMOVE.
	 */
	PLUGIN_REMOVE("PLUGIN REMOVE"),
	/**
	 * Traza PLUGIN SPEC CREATE.
	 */
	PLUGIN_SPEC_CREATE("PLUGIN SPEC CREATE"),
	/**
	 * Traza PLUGIN SPEC UPDATE.
	 */
	PLUGIN_SPEC_UPDATE("PLUGIN SPEC UPDATE"),
	/**
	 * Traza DOCUMENT SPEC CREATE.
	 */
	DOCUMENT_SPEC_CREATE("DOCUMENT SPEC CREATE"),
	/**
	 * Traza DOCUMENT SPEC UPDATE.
	 */
	DOCUMENT_SPEC_UPDATE("DOCUMENT SPEC UPDATE"),
	/**
	 * Traza DOCUMENT SPEC REMOVE.
	 */
	DOCUMENT_SPEC_REMOVE("DOCUMENT SPEC REMOVE"),
	/**
	 * Traza SCANPROFILE CREATE.
	 */
	SCANPROFILE_CREATE("SCANPROFILE CREATE"),
	/**
	 * Traza SCANPROFILE UPDATE.
	 */
	SCANPROFILE_UPDATE("SCANPROFILE UPDATE"),
	/**
	 * Traza SCANPROFILE REMOVE.
	 */
	SCANPROFILE_REMOVE("SCANPROFILE REMOVE"),
	/**
	 * Traza SCAN PROCCESS.
	 */
	SCAN_PROCESS("SCAN PROCESS"),
	/**
	 * Traza GENERATE DOCS BATCH SPACER.
	 */
	GENERATE_DOCS_BATCH_SPACER("GENERATE DOCS BATCH SPACER"),
	/**
	 * Traza GENERATE REUSABLE DOCS SPACERS.
	 */
	GENERATE_REUSABLE_DOCS_SPACERS("GENERATE REUSABLE DOCS SPACERS");

	/**
	 * Denominación de trazas de auditoría.
	 */
	private String operationName;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param anOperationName
	 *            Denominación de trazas de auditoría.
	 */
	AuditOperationName(String anOperationName) {
		this.operationName = anOperationName;
	}

	/**
	 * Obtiene el atributo operationName.
	 * 
	 * @return el atributo operationName.
	 */
	public String getOperationName() {
		return operationName;
	}

}
