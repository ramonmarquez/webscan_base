/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.DAOException.java.</p>
 * <b>Descripción:</b><p> Excepción que encapsula los errores producidos en el
 * componente DAO el modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model;

import es.ricoh.webscan.WebscanException;

/**
 * Excepción que encapsula los errores producidos en el componente DAO el modelo
 * de datos base de Webscan.
 * <p>
 * Clase DAOException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.WebscanException
 * @version 2.0.
 */
public class DAOException extends WebscanException {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Plugin no habilitado para su uso.
	 */
	public static final String CODE_100 = "COD_100";

	/**
	 * Definición de documento no habilitada para su uso.
	 */
	public static final String CODE_200 = "COD_200";

	/**
	 * Definición de documento con documentos en proceso de digitalización.
	 */
	public static final String CODE_201 = "COD_201";
	
	/**
	 * Metadatos requeridos no informados para un documento digitalizado.
	 */
	public static final String CODE_250 = "COD_250";

	/**
	 * Error persistiendo en el sistema de archivos el contenido de un
	 * documento.
	 */
	public static final String CODE_300 = "COD_300";

	/**
	 * Error eliminando en el sistema de archivos los contenidos de un
	 * documento.
	 */
	public static final String CODE_301 = "COD_301";

	/**
	 * Errores asociados a la lectura del archivo que aloja el almacén de
	 * certificados.
	 */
	public static final String CODE_303 = "COD_303";

	/**
	 * Entidad no encontrada.
	 */
	public static final String CODE_901 = "COD_901";

	/**
	 * Entidad duplicada o recuperación de más de un resultado.
	 */
	public static final String CODE_902 = "COD_902";

	/**
	 * Error persistiendo entidad.
	 */
	public static final String CODE_903 = "COD_903";

	/**
	 * Constructor sin argumentos.
	 */
	public DAOException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public DAOException(String aCode, String aMessage) {
		super(aCode, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public DAOException(String aMessage) {
		super(CODE_999, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public DAOException(Throwable aCause) {
		super(CODE_999, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public DAOException(final String aMessage, Throwable aCause) {
		super(CODE_999, aMessage, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public DAOException(final String aCode, final String aMessage, Throwable aCause) {
		super(aCode, aMessage, aCause);
	}

}
