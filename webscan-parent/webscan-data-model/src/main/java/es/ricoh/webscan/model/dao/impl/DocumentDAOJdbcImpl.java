/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dao.impl.DocumentDAOJdbcImpl.java.</p>
 * <b>Descripción:</b><p> Implementación JPA 2.0 de la capa DAO sobre las entidades documento digitalizado, su 
	 especificación y relaciones, pertenecientes al modelo de datos base de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.model.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.WebscanConstants;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.WebscanModelConstants;
import es.ricoh.webscan.model.dao.DocumentDAO;
import es.ricoh.webscan.model.domain.DocumentSpecification;
import es.ricoh.webscan.model.domain.Metadata;
import es.ricoh.webscan.model.domain.MetadataSpecification;
import es.ricoh.webscan.model.domain.OperationStatus;
import es.ricoh.webscan.model.domain.ScannedDocLog;
import es.ricoh.webscan.model.domain.ScannedDocument;
import es.ricoh.webscan.model.dto.DocBoxScannedDocDTO;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MappingUtilities;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.utilities.MimeType;

/**
 * Implementación JPA 2.0 de la capa DAO sobre las entidades documento
 * digitalizado, su especificación y relaciones, pertenecientes al modelo de
 * datos base de Webscan.
 * <p>
 * Clase DocumentDAOJdbcImpl.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.dao.DocumentDAO
 * @version 2.0.
 */
public final class DocumentDAOJdbcImpl implements DocumentDAO {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DocumentDAOJdbcImpl.class);

	/**
	 * Objeto empleado para la interacción con en el contexto persistencia.
	 */
	@PersistenceContext(unitName = "webscan-model-emf", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	/**
	 * Constructor sin argumentos.
	 */
	public DocumentDAOJdbcImpl() {
		super();
	}

	/**
	 * Establece un nuevo gestor de persistencia JPA.
	 * 
	 * @param aEntityManager
	 *            manager de persistencia.
	 */
	public void setEntityManager(EntityManager aEntityManager) {
		this.entityManager = aEntityManager;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#listDocumentSpecifications(java.lang.Long,
	 *      java.lang.Long, java.util.List)
	 */
	@Override
	public List<DocumentSpecificationDTO> listDocumentSpecifications(Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		int first, maxResults;
		List<DocumentSpecification> ddbbRes = new ArrayList<DocumentSpecification>();
		List<DocumentSpecificationDTO> res = new ArrayList<DocumentSpecificationDTO>();
		Map<String, List<EntityOrderByClause>> docOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String excMsg, jpqlQuery;

		try {
			LOG.debug("[WEBSCAN-MODEL] Recuperando las definiciones de documentos...");

			if (orderByColumns != null && !orderByColumns.isEmpty()) {
				docOrderByColumns.put("ds.", orderByColumns);
			}

			jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT ds FROM DocumentSpecification ds", docOrderByColumns);

			TypedQuery<DocumentSpecification> query = entityManager.createQuery(jpqlQuery, DocumentSpecification.class);

			if (elementsPerPage != null) {
				first = 0;
				maxResults = elementsPerPage.intValue();
				if (pageNumber != null) {
					first = (pageNumber.intValue() - 1) * maxResults;
				}

				query.setFirstResult(first);
				query.setMaxResults(maxResults);
			}

			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar las definiciones de documentos...");

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillDocumentSpecificationListDto(ddbbRes);

		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de documentos: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de documentos: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de documentos: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de documentos: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de documentos: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las definiciones de documentos: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando definiciones de documentos: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocumentSpecification(java.lang.Long)
	 */
	@Override
	public DocumentSpecificationDTO getDocumentSpecification(Long docSpecificationId) throws DAOException {
		DocumentSpecification entity;
		DocumentSpecificationDTO res = null;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de documento " + docSpecificationId + " ...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(docSpecificationId, entityManager);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando la definición de documento " + docSpecificationId + " recuperada de BBDD...");
			}
			res = MappingUtilities.fillDocumentSpecificationDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de documento " + docSpecificationId + " recuperada de BBDD: " + res.getName());
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento " + docSpecificationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocumentSpecificationByName(java.lang.String)
	 */
	@Override
	public DocumentSpecificationDTO getDocumentSpecificationByName(String docSpecificationName) throws DAOException {
		DocumentSpecificationDTO res = null;
		DocumentSpecification entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de documento denominada " + docSpecificationName + " ...");
			}
			TypedQuery<DocumentSpecification> query = entityManager.createQuery("SELECT ds FROM DocumentSpecification ds WHERE ds.name = :docSpecName", DocumentSpecification.class);
			query.setParameter("docSpecName", docSpecificationName);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la definición de documento denominada " + docSpecificationName + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillDocumentSpecificationDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de documento denominada " + res.getName() + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento denominada " + docSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de documento denominada " + docSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento denominada " + docSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento denominada " + docSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento denominada " + docSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento denominada " + docSpecificationName + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento denominada " + docSpecificationName + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento denominada " + docSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento denominada " + docSpecificationName + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocumentSpecificationByScanProfileOrgUnitDocSpec(java.lang.Long)
	 */
	@Override
	public DocumentSpecificationDTO getDocumentSpecificationByScanProfileOrgUnitDocSpec(Long scanProfileOrgUnitDocSpecId) throws DAOException {
		DocumentSpecificationDTO res = null;
		DocumentSpecification entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + " ...");
			}
			TypedQuery<DocumentSpecification> query = entityManager.createQuery("SELECT ds FROM DocumentSpecification ds JOIN ds.orgScanProfiles osp WHERE osp.id = :scanProfileOrgUnitDocSpecId", DocumentSpecification.class);
			query.setParameter("scanProfileOrgUnitDocSpecId", scanProfileOrgUnitDocSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillDocumentSpecificationDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de documento denominada " + res.getName() + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de documento para la configuración de definición en perfil y unidad organizativa " + scanProfileOrgUnitDocSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getParentDocSpecifications(java.lang.Long)
	 */
	@Override
	public List<DocumentSpecificationDTO> getParentDocSpecifications(Long docSpecificationId) throws DAOException {
		List<DocumentSpecificationDTO> res = new ArrayList<DocumentSpecificationDTO>();
		List<DocumentSpecification> ddbbRes;
		DocumentSpecification entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las definiciones de documentos de las que hereda la definición de documento " + docSpecificationId + "...");

				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta en BBDD para recuperar las definiciones de documentos de las que hereda la definición de documento " + docSpecificationId + "...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(docSpecificationId, entityManager);

			ddbbRes = entity.getParentDocs();

			res = MappingUtilities.fillDocumentSpecificationListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Número de definiciones de documentos de las que hereda la definición de documento " + docSpecificationId + ": " + res.size());
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos de las que hereda la definición de documento " + docSpecificationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getChildDocSpecifications(java.lang.Long)
	 */
	@Override
	public List<DocumentSpecificationDTO> getChildDocSpecifications(Long docSpecificationId) throws DAOException {
		List<DocumentSpecificationDTO> res = new ArrayList<DocumentSpecificationDTO>();
		List<DocumentSpecification> ddbbRes;
		DocumentSpecification entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las definiciones de documentos que heredan de la definición de documento " + docSpecificationId + "...");

				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta en BBDD para recuperar las definiciones de documentos que heredan de la definición de documento " + docSpecificationId + "...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(docSpecificationId, entityManager);

			ddbbRes = entity.getChildDocs();

			res = MappingUtilities.fillDocumentSpecificationListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Número de definiciones de documentos que heredan de la definición de documento " + docSpecificationId + ": " + res.size());
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de documentos que heredan de la definición de documento " + docSpecificationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#createDocumentSpecification(es.ricoh.webscan.model.dto.DocumentSpecificationDTO)
	 */
	@Override
	public Long createDocumentSpecification(DocumentSpecificationDTO specification) throws DAOException {
		Long res = null;
		DocumentSpecification entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando una nueva definición de documento, denominada " + specification.getName() + " ...");
		}
		try {
			entity = MappingUtilities.fillDocumentSpecificationEntity(specification);

			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva definición de documento " + specification.getName() + " creada, identificador: " + res + ".");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de documento " + specification.getName() + ", la entidad ya existe en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de documento " + specification.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de documento " + specification.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de documento " + specification.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la definición de documento " + specification.getName() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#updateDocumentSpecification(es.ricoh.webscan.model.dto.DocumentSpecificationDTO)
	 */
	@Override
	public void updateDocumentSpecification(DocumentSpecificationDTO specification) throws DAOException {
		DocumentSpecification entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando la definición de documento " + specification.getId() + " ...");
		}
		try {
			entity = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(specification.getId(), entityManager);

			// Se actualizan los datos globales
			entity.setActive(specification.getActive());
			entity.setDescription(specification.getDescription());
			entity.setName(specification.getName());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando la definición de documento " + specification.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de documento " + specification.getId() + " actualizada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de documento " + specification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de documento " + specification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de documento " + specification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la definición de documento " + specification.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#setParentDocSpecifications(java.util.List,
	 *      java.lang.Long)
	 */
	@Override
	public void setParentDocSpecifications(List<DocumentSpecificationDTO> docSpecs, Long docSpecificationId) throws DAOException {
		DocumentSpecification entity;
		List<DocumentSpecificationDTO> parentDocSpecs;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando la lista de especificaciones de documentos de las que hereda la definición de documento " + docSpecificationId + " ...");
		}
		try {
			entity = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(docSpecificationId, entityManager);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando la lista de especificaciones de documentos de las que hereda la definición de documento " + docSpecificationId + " en BBDD ...");
			}
			parentDocSpecs = MappingUtilities.fillDocumentSpecificationListDto(entity.getParentDocs());

			entity.setParentDocs(mergeInheritedDocumentSpecifications(docSpecs, parentDocSpecs, entity, Boolean.TRUE));

			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Lista de especificaciones de documentos de las que hereda la definición de documento " + docSpecificationId + " actualizada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de especificaciones de documentos de las que hereda la definición de documento " + docSpecificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de especificaciones de documentos de las que hereda la definición de documento " + docSpecificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de especificaciones de documentos de las que hereda la definición de documento " + docSpecificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de especificaciones de documentos de las que hereda la definición de documento " + docSpecificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#setChildDocSpecifications(java.util.List,
	 *      java.lang.Long)
	 */
	@Override
	public void setChildDocSpecifications(List<DocumentSpecificationDTO> docSpecs, Long docSpecificationId) throws DAOException {
		DocumentSpecification entity;
		List<DocumentSpecificationDTO> childDocSpecs;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando la lista de especificaciones de documentos que heredan de la definición de documento " + docSpecificationId + " ...");
		}
		try {
			entity = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(docSpecificationId, entityManager);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando la lista de especificaciones de documentos que heredan de la definición de documento " + docSpecificationId + " en BBDD ...");
			}
			childDocSpecs = MappingUtilities.fillDocumentSpecificationListDto(entity.getChildDocs());

			entity.setChildDocs(mergeInheritedDocumentSpecifications(docSpecs, childDocSpecs, entity, Boolean.FALSE));

			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Lista de especificaciones de documentos que heredan de la definición de documento " + docSpecificationId + " actualizada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de especificaciones de documentos que heredan de la definición de documento " + docSpecificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de especificaciones de documentos que heredan de la definición de documento " + docSpecificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de especificaciones de documentos que heredan de la definición de documento " + docSpecificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de especificaciones de documentos que heredan de la definición de documento " + docSpecificationId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#removeDocumentSpecifications(java.util.List)
	 */
	@Override
	public void removeDocumentSpecifications(List<Long> specificationIds) throws DAOException {
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Eliminando las definiciones de documento " + specificationIds + " ...");
		}
		try {
			Query query = entityManager.createQuery("DELETE FROM DocumentSpecification ds WHERE ds.id IN :docSpecIds");
			query.setParameter("docSpecIds", specificationIds);
			int res = query.executeUpdate();

			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res + " definiciones de documento eliminadas en BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las definiciones de documento " + specificationIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las definiciones de documento " + specificationIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las definiciones de documento " + specificationIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las definiciones de documento " + specificationIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las definiciones de documento " + specificationIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#hasDocSpecificationDocsInScanProccess(java.lang.Long)
	 */
	@Override
	public Boolean hasDocSpecificationDocsInScanProccess(Long docSpecId) throws DAOException {
		Long numberOfDocs;
		Boolean res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + " ...");
			}
			TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(sd.id) FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.docSpecification ds WHERE sd.digProcEndDate IS NULL AND ds.id = :docSpecId", Long.class);
			query.setParameter("docSpecId", docSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + " ...");
			}
			numberOfDocs = query.getSingleResult();

			res = numberOfDocs > 0;
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de documentos " + docSpecId + " con " + numberOfDocs + " en proceso de digitalización.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el numero de documentos en proceso de digitalización para la definición de documentos " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getActiveDocumentsByUsernameAndScanProfileSpec(java.lang.String,
	 *      java.lang.String, java.util.List)
	 */
	@Override
	public List<ScannedDocumentDTO> getActiveDocumentsByUsernameAndScanProfileSpec(String username, String scanProfileSpec, List<EntityOrderByClause> orderByColumns) throws DAOException {
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();
		List<ScannedDocument> ddbbRes;
		String excMsg;
		TypedQuery<ScannedDocument> query;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Construyendo la consulta para recuperar los documentos digitalizados en proceso para el usuario {} y el proceso de digitalización {} de BBDD.", username, scanProfileSpec);
			}
			query = buildGetDocumentsByUsernameAndScanProfileSpec(username, scanProfileSpec, orderByColumns);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar los documentos digitalizados en proceso para el usuario {} y el proceso de digitalización {} de BBDD.", username, scanProfileSpec);
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillSummarizedScannedDocumentListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos digitalizados en proceso para el usuario {} y el proceso de digitalización {} recuperados de BBDD.", username, scanProfileSpec);
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para el usuario " + username + " y el proceso de digitalización " + scanProfileSpec + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados en proceso para el usuario " + username + " y el proceso de digitalización " + scanProfileSpec + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para el usuario " + username + " y el proceso de digitalización " + scanProfileSpec + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para el usuario " + username + " y el proceso de digitalización " + scanProfileSpec + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para el usuario " + username + " y el proceso de digitalización " + scanProfileSpec + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados en proceso para el usuario " + username + " y el proceso de digitalización " + scanProfileSpec + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para el usuario " + username + " y el proceso de digitalización " + scanProfileSpec + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getActiveDocumentsByBatchReqId(java.lang.String,
	 *      java.util.List)
	 */
	@Override
	public List<ScannedDocumentDTO> getActiveDocumentsByBatchReqId(String batchReqId, List<EntityOrderByClause> orderByColumns) throws DAOException {
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();
		List<ScannedDocument> ddbbRes;
		String excMsg;
		TypedQuery<ScannedDocument> query;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Construyendo la consulta para recuperar los documentos digitalizados en proceso para la caratula/reserva {} de BBDD.", batchReqId);
			}
			query = buildGetDocumentsByBatchReqId(batchReqId, orderByColumns);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar los documentos digitalizados en proceso para la caratula/reserva {} de BBDD.", batchReqId);
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillSummarizedScannedDocumentListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos digitalizados en proceso para la caratula/reserva {} de BBDD.", batchReqId);
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchReqId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados en proceso para la caratula/reserva " + batchReqId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchReqId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchReqId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchReqId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados en proceso para la caratula/reserva " + batchReqId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchReqId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Método auxiliar que construye la consulta responsable de recuperar la
	 * relación de documentos que están siendo procesados por un usuario,
	 * mediante un proceso de digitalización determinado.
	 * 
	 * @param username
	 *            Identificación del usuario.
	 * @param scanProfileSpec
	 *            Denominación del proceso de digitalización.
	 * @param orderByColumns
	 *            columnas mediante las que será ordenada la relación de
	 *            documentos recuperada.
	 * @return Consulta JPQL que recupera la relación de documentos que están
	 *         siendo procesados por un usuario, mediante un proceso de
	 *         digitalización determinado.
	 */
	private TypedQuery<ScannedDocument> buildGetDocumentsByUsernameAndScanProfileSpec(String username, String scanProfileSpec, List<EntityOrderByClause> orderByColumns) {
		String jpqlQuery;
		Map<String, List<EntityOrderByClause>> docOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		TypedQuery<ScannedDocument> res;

		jpqlQuery = "SELECT sd FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spou JOIN spou.orgScanProfile osp JOIN osp.scanProfile sp JOIN sp.specification spe WHERE sd.digProcEndDate IS NULL AND sd.username = :username AND spe.name = :scanProfileSpec";

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			docOrderByColumns.put("sd.", orderByColumns);
		}

		jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery(jpqlQuery, docOrderByColumns);

		res = entityManager.createQuery(jpqlQuery, ScannedDocument.class);

		res.setParameter("username", username);
		res.setParameter("scanProfileSpec", scanProfileSpec);

		return res;
	}

	/**
	 * Método auxiliar que construye la consulta responsable de recuperar la
	 * relación de documentos que están siendo procesados para una reserva o
	 * caratula determinada.
	 * 
	 * @param batchReqId
	 *            Identificador de reserva de trabajo de digitalización o
	 *            carátula.
	 * @param orderByColumns
	 *            columnas mediante las que será ordenada la relación de
	 *            documentos recuperada.
	 * @return Consulta JPQL que recupera la relación de documentos que están
	 *         siendo procesados por un usuario y pertenecen a una reserva o
	 *         caratula determinada.
	 */
	private TypedQuery<ScannedDocument> buildGetDocumentsByBatchReqId(String batchReqId, List<EntityOrderByClause> orderByColumns) {
		String jpqlQuery;
		Map<String, List<EntityOrderByClause>> docOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		TypedQuery<ScannedDocument> res;

		jpqlQuery = "SELECT sd FROM ScannedDocument sd WHERE sd.digProcEndDate IS NULL AND sd.batchReqId = :batchReqId";

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			docOrderByColumns.put("sd.", orderByColumns);
		}

		jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery(jpqlQuery, docOrderByColumns);

		res = entityManager.createQuery(jpqlQuery, ScannedDocument.class);

		res.setParameter("batchReqId", batchReqId);

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocument(java.lang.Long)
	 */
	@Override
	public ScannedDocumentDTO getDocument(Long docId) throws DAOException {
		ScannedDocument entity;
		ScannedDocumentDTO res = null;
		String excMsg;

		try {
			entity = getScannedDocumentEntity(docId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando el documento " + docId + " obtenido de BBDD ...");
			}
			res = MappingUtilities.fillScannedDocumentDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el documento escaneado " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocument(java.lang.Long)
	 */
	@Override
	public Long getNumDocsByBatchId(Long batchId) throws DAOException {
		Long res;
		String excMsg;
		String jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el número de  documentos digitalizados del proceso de digitalización " + batchId );
			}

			jpqlQuery = "SELECT COUNT(sd.id) FROM ScannedDocument sd WHERE sd.batch = " + batchId;

			TypedQuery<Long> query = entityManager.createQuery(jpqlQuery, Long.class);
			//query.setParameter("batchId", batchId);
			LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el número total de trazas de auditoría registradas en el sistema ...");

			res = query.getSingleResult();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res + " documentos para el proceso de digitalización " + batchId);
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el número documentos digitalizados para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedió el tiempo máximo para recuperar los documentos digitalizados para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (IllegalArgumentException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new IllegalArgumentException(e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}
	

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocumentsByOrgUnits(java.util.List,
	 *      java.lang.String, java.lang.Long, java.lang.Long, java.util.List)
	 */
	@Override
	public List<ScannedDocumentDTO> getDocumentsByOrgUnits(List<Long> orgUnitIds, String functionalOrgs, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();
		List<ScannedDocument> ddbbRes;
		List<ScannedDocument> filteredDdbbRes;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los documentos digitalizados para las unidades organizativas " + orgUnitIds + " de BBDD ...");
			}
			TypedQuery<ScannedDocument> query = buildGetDocumentsByOrgUnitsQuery(orgUnitIds, pageNumber, elementsPerPage, orderByColumns);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar los documentos digitalizados para las unidades organizativas " + orgUnitIds + " de BBDD ...");
			}
			ddbbRes = query.getResultList();

			LOG.debug("[WEBSCAN-MODEL] Filtrando resultados por unidades orgánicas del usuario: ", functionalOrgs);

			filteredDdbbRes = filterDocsByUserFunctionalOrgs(ddbbRes, functionalOrgs);

			res = MappingUtilities.fillSummarizedScannedDocumentListDto(filteredDdbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos para las unidades organizativas " + orgUnitIds + " recuperados de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Construye la consulta JPA responsable de recuperar un listado de
	 * documentos digitalizados pertenecientes a una unidad organizativa
	 * determinada.
	 * 
	 * @param orgUnitIds
	 *            Relación de identificadores de unidad organizativa.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return consulta JPA a base de datos.
	 * @throws IllegalArgumentException
	 *             Si se produce un error en la construcción de la consulta JPA.
	 */
	private TypedQuery<ScannedDocument> buildGetDocumentsByOrgUnitsQuery(List<Long> orgUnitIds, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws IllegalArgumentException {
		int first, maxResults;
		Map<String, List<EntityOrderByClause>> docOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String jpqlQuery;
		TypedQuery<ScannedDocument> res = null;

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			docOrderByColumns.put("sd.", orderByColumns);
		}
		jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT sd FROM ScannedDocument sd LEFT JOIN FETCH sd.execLogs logs LEFT JOIN FETCH logs.scanProcessStage stage JOIN FETCH sd.scanProfileOrgUnitDocSpec spouds JOIN FETCH spouds.orgScanProfile spou JOIN FETCH spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds", docOrderByColumns);
		/*
		jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT sd FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds", docOrderByColumns);
		*/
		res = entityManager.createQuery(jpqlQuery, ScannedDocument.class);
		res.setParameter("orgUnitIds", orgUnitIds);

		if (elementsPerPage != null) {
			first = 0;
			maxResults = elementsPerPage.intValue();
			if (pageNumber != null) {
				first = (pageNumber.intValue() - 1) * maxResults;
			}

			res.setFirstResult(first);
			res.setMaxResults(maxResults);
		}

		return res;
	}

	/**
	 * Filtra una relación de documentos recuperados de BBDD mediante la
	 * unidades orgánicas del usuario que inicio su digitalización. El documento
	 * será incluido en el resultado si alguna de las unidades orgánicas
	 * registradas para el documento se encuentra en las unidades orgánicas
	 * pasadas como parámetro de entrada.
	 * 
	 * @param ddbbRes
	 *            Lista de documentos a filtrar
	 * @param userFunctionalOrgs
	 *            unidades orgánicas por las que filtrar los los documentos a
	 *            recuperar (sepradas por coma).
	 * @return Lista de documentos filtrados por unidades orgánicas.
	 */
	private List<ScannedDocument> filterDocsByUserFunctionalOrgs(List<ScannedDocument> ddbbRes, String userFunctionalOrgs) {
		List<ScannedDocument> res = new ArrayList<ScannedDocument>();
		String[ ] userFunctionalOrgsArray = null;

		if (userFunctionalOrgs != null && !userFunctionalOrgs.isEmpty()) {
			userFunctionalOrgsArray = userFunctionalOrgs.split(",");
		}

		for (ScannedDocument doc: ddbbRes) {
			if (filterDocByUserFunctionalOrgs(doc, userFunctionalOrgsArray)) {
				res.add(doc);
			}
		}

		return res;
	}

	/**
	 * Filtra una relación de documentos, recuperados de BBDD, para ser
	 * presentados en la bandeja de documentos, mediante las unidades orgánicas
	 * del usuario que inicio su digitalización. El documento será incluido en
	 * el resultado si alguna de las unidades orgánicas registradas para el
	 * documento se encuentra en las unidades orgánicas pasadas como parámetro
	 * de entrada.
	 * 
	 * @param ddbbRes
	 *            Lista de documentos a filtrar.
	 * @param userFunctionalOrgs
	 *            unidades orgánicas por las que filtrar los los documentos a
	 *            recuperar (sepradas por coma).
	 * @return Lista de documentos filtrados por unidades orgánicas.
	 */
	private List<DocBoxScannedDocDTO> filterDocBoxScannedDocsByUserFunctionalOrgs(List<DocBoxScannedDocDTO> ddbbRes, String userFunctionalOrgs) {
		List<DocBoxScannedDocDTO> res = new ArrayList<DocBoxScannedDocDTO>();
		String[ ] userFunctionalOrgsArray = null;

		if (userFunctionalOrgs != null && !userFunctionalOrgs.isEmpty()) {
			userFunctionalOrgsArray = userFunctionalOrgs.split(",");
		}

		for (DocBoxScannedDocDTO doc: ddbbRes) {
			if (filterDocBoxScannedDocByUserFunctionalOrgs(doc, userFunctionalOrgsArray)) {
				res.add(doc);
			}
		}

		return res;
	}

	/**
	 * Determina si un documento a presentar en la bandeja de documentos debe
	 * ser incluido al resultado, mediante las unidades orgánicas del usuario
	 * que inicio su proceso de digitalización, y las unidades orgánicas pasadas
	 * como parámetro de entrada.
	 * 
	 * @param doc
	 *            documento.
	 * @param userFunctionalOrgsArray
	 *            lista de identificadores de unidades orgánicas.
	 * @return Retorna true si alguna de las unidades del documento se
	 *         encuentran en la lista de unidades orgánicas pasadas como
	 *         parámetro de entrada, o si el documento no posee unidades
	 *         orgánicas.
	 */
	private Boolean filterDocBoxScannedDocByUserFunctionalOrgs(DocBoxScannedDocDTO doc, String[ ] userFunctionalOrgsArray) {
		Boolean res = Boolean.FALSE;
		String docFunctionalOrgs;
		String[ ] docFunctionalOrgsArray;

		docFunctionalOrgs = doc.getDocFunctionalOrgs();

		if (docFunctionalOrgs != null && !docFunctionalOrgs.isEmpty()) {
			docFunctionalOrgsArray = docFunctionalOrgs.split(",");

			if (userFunctionalOrgsArray != null) {
				for (int i = 0; !res && i < docFunctionalOrgsArray.length; i++) {
					for (int j = 0; !res && j < userFunctionalOrgsArray.length; j++) {
						res = docFunctionalOrgsArray[i].equals(userFunctionalOrgsArray[j]);
					}
				}
			}
		} else {
			res = Boolean.TRUE;
		}

		return res;
	}

	/**
	 * Determina si un documento debe ser incluido al resultado, mediante las
	 * unidades orgánicas del usuario que inicio su proceso de digitalización, y
	 * las unidades orgánicas pasadas como parámetro de entrada.
	 * 
	 * @param doc
	 *            documento.
	 * @param userFunctionalOrgsArray
	 *            lista de identificadores de unidades orgánicas.
	 * @return Retorna true si alguna de las unidades del documento se
	 *         encuentran en la lista de unidades orgánicas pasadas como
	 *         parámetro de entrada, o si el documento no posee unidades
	 *         orgánicas.
	 */
	private Boolean filterDocByUserFunctionalOrgs(ScannedDocument doc, String[ ] userFunctionalOrgsArray) {
		Boolean res = Boolean.FALSE;
		String docFunctionalOrgs;
		String[ ] docFunctionalOrgsArray;

		docFunctionalOrgs = doc.getFunctionalOrgs();

		if (docFunctionalOrgs != null && !docFunctionalOrgs.isEmpty()) {
			docFunctionalOrgsArray = docFunctionalOrgs.split(",");

			if (userFunctionalOrgsArray != null) {
				for (int i = 0; !res && i < docFunctionalOrgsArray.length; i++) {
					for (int j = 0; !res && j < userFunctionalOrgsArray.length; j++) {
						res = docFunctionalOrgsArray[i].equals(userFunctionalOrgsArray[j]);
					}
				}
			}
		} else {
			res = Boolean.TRUE;
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocumentsAndLogsByOrgUnits(java.util.List,
	 *      java.lang.String)
	 */
	@Override
	public List<DocBoxScannedDocDTO> getDocumentsAndLogsByOrgUnits(List<Long> orgUnitIds, String functionalOrgs) throws DAOException {
		List<DocBoxScannedDocDTO> res;
		List<String> functionalOrgsList = new ArrayList<String>();
		String excMsg;
		String jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + " de BBDD ...");
			}

			if (functionalOrgs == null) {
				//jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.DocBoxScannedDocDTO(sd.id, sd.name, logs.id, stage.name, sd.batchId, sd.batchOrderNum, sd.digProcStartDate, sd.functionalOrgs, sd.username, logs.status) FROM ScannedDocument sd JOIN sd.execLogs logs JOIN logs.scanProcessStage stage JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds and logs.id = (SELECT max(auxExeLog.id) from ScannedDocLog auxExeLog JOIN auxExeLog.document auxDoc WHERE auxDoc.id = sd.id)";
				jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.DocBoxScannedDocDTO(sd.id, sd.name, logs.id, stage.name, bt.batchNameId, bt.id, sd.batchOrderNum, sd.digProcStartDate, sd.functionalOrgs, sd.username, logs.status) FROM ScannedDocument sd JOIN sd.execLogs logs JOIN logs.scanProcessStage stage JOIN sd.batchId bt JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds and and bt.id = :batchId and logs.id = (SELECT max(auxExeLog.id) from ScannedDocLog auxExeLog JOIN auxExeLog.document auxDoc WHERE auxDoc.id = sd.id)";
			} else {
				// Construir lista de unidades orgánicas
				functionalOrgsList = transformArrayToList(functionalOrgs);
				//jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.DocBoxScannedDocDTO(sd.id, sd.name, logs.id, stage.name, sd.batchId, sd.batchOrderNum, sd.digProcStartDate, sd.functionalOrgs, sd.username, logs.status) FROM ScannedDocument sd JOIN sd.execLogs logs JOIN logs.scanProcessStage stage JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds and sd.functionalOrgs IN :functionalOrgs and logs.id = (SELECT max(auxExeLog.id) from ScannedDocLog auxExeLog JOIN auxExeLog.document auxDoc WHERE auxDoc.id = sd.id)";
				jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.DocBoxScannedDocDTO(sd.id, sd.name, logs.id, stage.name, bt.batchNameId, bt.id, sd.batchOrderNum, sd.digProcStartDate, sd.functionalOrgs, sd.username, logs.status) FROM ScannedDocument sd JOIN sd.execLogs logs JOIN logs.scanProcessStage stage JOIN sd.batchId bt JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds and sd.functionalOrgs IN :functionalOrgs sd.id = :batchId and logs.id = (SELECT max(auxExeLog.id) from ScannedDocLog auxExeLog JOIN auxExeLog.document auxDoc WHERE auxDoc.id = sd.id)";
			}

			TypedQuery<DocBoxScannedDocDTO> query = entityManager.createQuery(jpqlQuery, DocBoxScannedDocDTO.class);
			query.setParameter("orgUnitIds", orgUnitIds);

			if (functionalOrgs != null && !functionalOrgsList.isEmpty()) {
				query.setParameter("functionalOrgs", functionalOrgsList);
			}
			res = query.getResultList();

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Filtrando resultados por unidades orgánicas del usuario: " + functionalOrgs);
			}

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos para las unidades organizativas " + orgUnitIds + " recuperados de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (IllegalArgumentException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new IllegalArgumentException(e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para las unidades organizativas " + orgUnitIds + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocumentsAndLogsByOrgUnits(java.util.Long,
	 *      java.lang.String)
	 */
	@Override
	public List<DocBoxScannedDocDTO> getDocsByBatchId(Long batchId) throws DAOException {
		List<DocBoxScannedDocDTO> res;
		String excMsg;
		String jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los documentos digitalizados y trazas de ejecución para el proceso de digitalización " + batchId + " de BBDD ...");
			}

//			jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.DocBoxScannedDocDTO(sd.id, sd.name, logs.id, stage.name, bt.batchNameId, sd.batchOrderNum, sd.digProcStartDate, sd.functionalOrgs, sd.username, logs.status) FROM ScannedDocument sd JOIN sd.execLogs logs JOIN logs.scanProcessStage stage JOIN sd.batchId bt WHERE bt.id IN :batchId and logs.id = (SELECT max(auxExeLog.id) from ScannedDocLog auxExeLog JOIN auxExeLog.document auxDoc WHERE auxDoc.id = sd.id)";
			jpqlQuery = "SELECT new es.ricoh.webscan.model.dto.DocBoxScannedDocDTO(sd.id, sd.name, logs.id, stage.name, bt.batchNameId, bt.id, sd.batchOrderNum, sd.digProcStartDate, sd.functionalOrgs, sd.username, logs.status) FROM ScannedDocument sd JOIN sd.batch bt JOIN sd.execLogs logs JOIN logs.scanProcessStage stage WHERE bt.id = :batchId and logs.id = (SELECT max(auxExeLog.id) from ScannedDocLog auxExeLog JOIN auxExeLog.document auxDoc WHERE auxDoc.id = sd.id)";

			TypedQuery<DocBoxScannedDocDTO> query = entityManager.createQuery(jpqlQuery, DocBoxScannedDocDTO.class);
			query.setParameter("batchId", batchId);

			res = query.getResultList();

					if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos para el proceso de digitalización " + batchId + " recuperados de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados y trazas de ejecución para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (IllegalArgumentException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new IllegalArgumentException(e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados y trazas de ejecución para el proceso de digitalización " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Transforma un array de String a una lista de String
	 * 
	 * @param functionalOrgs
	 * @return Lista de functionalOrgs
	 */
	private List<String> transformArrayToList(String functionalOrgs) {
		List<String> result = new ArrayList<String>();

		if (functionalOrgs != null) {
			result = Arrays.asList(functionalOrgs);
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocumentsByBatchId(java.lang.String,
	 *      java.lang.Long, java.lang.Long, java.util.List)
	 */
	@Override
	public List<ScannedDocumentDTO> getDocumentsByBatchId(String batchId, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) throws DAOException {
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();
		List<ScannedDocument> ddbbRes;
		String excMsg;
		TypedQuery<ScannedDocument> query;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los documentos digitalizados pertenecientes al lote " + batchId + " de BBDD ...");
			}
			query = buildGetScannedDocsByBatchId(batchId, pageNumber, elementsPerPage, orderByColumns);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar los documentos digitalizados pertenecientes al lote " + batchId + " de BBDD ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillScannedDocumentListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos pertenecientes al lote " + batchId + " recuperados de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados pertenecientes al lote " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados pertenecientes al lote " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados pertenecientes al lote " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados pertenecientes al lote " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados pertenecientes al lote " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados pertenecientes al lote " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando documentos digitalizados pertenecientes al lote " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getBatchIdsByOrgUnit(java.lang.Long)
	 */
	@Override
	public List<String> getBatchIdsByOrgUnit(Long orgUnitId) throws DAOException {
		List<String> res = new ArrayList<String>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los identificadores de los lotes de documentos digitalizados para unidad organizativa " + orgUnitId + " de BBDD ...");
			}
			TypedQuery<String> query = entityManager.createQuery("SELECT DISTINCT sd.batchId FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile spou JOIN spou.orgUnit orgUnit WHERE orgUnit.id = :orgUnitId", String.class);
			query.setParameter("orgUnitId", orgUnitId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar los identificadores de los lotes de documentos digitalizados para unidad organizativa " + orgUnitId + " de BBDD ...");
			}
			res = query.getResultList();
			res.add(WebscanModelConstants.NULL_BATCH_ID);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " identificadores de los lotes de documentos digitalizados para unidad organizativa " + orgUnitId + " de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los identificadores de los lotes de documentos digitalizados para unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los identificadores de los lotes de documentos " + "digitalizados para unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los identificadores de los lotes de documentos digitalizados para unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los identificadores de los lotes de documentos digitalizados para unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los identificadores de los lotes de documentos digitalizados para unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los identificadores de los lotes de documentos " + "digitalizados para unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los identificadores de los lotes de documentos digitalizados para unidad organizativa " + orgUnitId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocContentStorePaths(java.lang.Long)
	 */
	@Override
	public Map<MimeType, String> getDocContentStorePaths(Long docId) throws DAOException {
		Map<MimeType, String> res = new HashMap<MimeType, String>();
		MimeType mimeType;
		ScannedDocument entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando las rutas de almacenamiento de los contenidos del documento " + docId + " en BBDD ...");
		}
		try {
			entity = getScannedDocumentEntity(docId);

			mimeType = MimeType.getMimeTypeByType(entity.getMimeType());
			if (entity.getStoreTempPath() != null) {
				res.put(mimeType, entity.getStoreTempPath());
			}
			if (entity.getStoreSignatureTempPath() != null) {
				res.put(MimeType.SIGNATURE, entity.getStoreSignatureTempPath());
			}
			if (entity.getStoreOcrTextTempPath() != null) {
				res.put(MimeType.TEXT_PLAIN_OCR, entity.getStoreOcrTextTempPath());
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " rutas de almacenamiento del documento escaneado " + entity.getId() + " recuperadas en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las rutas de almacenamiento de los contenidos del documento " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getDocumentsNotScannedByDocSpecId(java.lang.Long)
	 */
	@Override
	public List<ScannedDocumentDTO> getDocumentsNotScannedByDocSpecId(Long docSpecId) throws DAOException {
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();
		List<ScannedDocument> ddbbRes = new ArrayList<ScannedDocument>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + " de BBDD ...");
			}
			/*TypedQuery<ScannedDocument> query = entityManager.createQuery("SELECT sd FROM ScannedDocument sd WHERE sd.digProcEndDate IS NULL AND sd.scanProfileOrgUnitDocSpec IN (SELECT spouds FROM DocumentSpecification docSpec LEFT JOIN docSpec.orgScanProfiles spouds WHERE docSpec.id = :docSpecId)", ScannedDocument.class);*/
			TypedQuery<ScannedDocument> query = entityManager.createQuery("SELECT sd FROM ScannedDocument sd JOIN sd.scanProfileOrgUnitDocSpec spouds JOIN spouds.docSpecification docSpec WHERE sd.digProcEndDate IS NULL AND docSpec.id = :docSpecId", ScannedDocument.class);
			query.setParameter("docSpecId", docSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + " de BBDD ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillSummarizedScannedDocumentListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + " de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos, cuyo proceso de digitalización no ha finalizado, definidos mediante la especificación " + docSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#createDocument(es.ricoh.webscan.model.dto.ScannedDocumentDTO,
	 *      java.lang.Long)
	 */
	@Override
	public Long createDocument(ScannedDocumentDTO doc, Long scanProfOrgUnitDocSpecId, Long batchId) throws DAOException {
		Long res;
		ScannedDocument entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando el documento escaneado " + doc + " ...");
		}
		try {
			entity = MappingUtilities.fillScannedDocumentEntity(doc);
			entity.setScanProfileOrgUnitDocSpec(CommonUtilitiesDAOJdbcImpl.getScanProfileOrgUnitDocSpecEntity(scanProfOrgUnitDocSpecId, entityManager));
			entity.setBatch(CommonUtilitiesDAOJdbcImpl.getBatchEntity(batchId, entityManager));
			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Documento escaneado " + doc.getId() + " creado en BBDD.");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el documento escaneado " + doc + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el documento escaneado " + doc + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el documento escaneado " + doc + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el documento escaneado " + doc + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear el documento escaneado " + doc + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#updateDocuments(java.util.List)
	 */
	@Override
	public void updateDocument(ScannedDocumentDTO doc) throws DAOException {
		ScannedDocument entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Actualizando el documento escaneado " + doc + " ...");
			}

			entity = getScannedDocumentEntity(doc.getId());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Actualizando el documento " + doc.getId() + " ...");
			}
			// Se actualizan los datos globales
			entity.setAuditOpId(doc.getAuditOpId());
			entity.setContent(doc.getContent());
			entity.setDigProcEndDate(doc.getDigProcEndDate());
			entity.setChecked(doc.getChecked());
			entity.setCheckDate(doc.getCheckDate());
			entity.setHash(doc.getHash());
			entity.setOcr(doc.getOcr());
			entity.setSignature(doc.getSignature());
			entity.setSignatureType(doc.getSignatureType());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando el documento escaneado " + entity.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Documento escaneado " + entity.getId() + " actualizado en BBDD ...");
			}

			entityManager.flush();
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el documento escaneados " + doc + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el documento escaneados " + doc + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el documento escaneados " + doc + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando el documento escaneados " + doc + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#updateDocuments(java.util.List)
	 */
	@Override
	public void updateDocuments(List<ScannedDocumentDTO> docs) throws DAOException {
		ScannedDocument entity;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Actualizando la lista de documento escaneados " + docs + " ...");
			}
			for (ScannedDocumentDTO doc: docs) {
				entity = getScannedDocumentEntity(doc.getId());
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Actualizando el documento " + doc.getId() + " ...");
				}
				// Se actualizan los datos globales
				entity.setAuditOpId(doc.getAuditOpId());

				entity.setContent(doc.getContent());
				entity.setDigProcEndDate(doc.getDigProcEndDate());
				entity.setChecked(doc.getChecked());
				entity.setCheckDate(doc.getCheckDate());
				entity.setHash(doc.getHash());
				entity.setOcr(doc.getOcr());
				entity.setSignature(doc.getSignature());
				entity.setSignatureType(doc.getSignatureType());
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Modificando el documento escaneado " + entity.getId() + " en BBDD ...");
				}
				entityManager.merge(entity);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Documento escaneado " + entity.getId() + " actualizado en BBDD ...");
				}
			}

			entityManager.flush();
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de documento escaneados " + docs + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de documento escaneados " + docs + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de documento escaneados " + docs + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la lista de documento escaneados " + docs + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#updateDocContentStorePaths(java.lang.Long,
	 *      java.util.Map)
	 */
	@Override
	public void updateDocContentStorePaths(Long docId, Map<MimeType, String> docContentStorePaths) throws DAOException {
		MimeType mimeType;
		ScannedDocument entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando las rutas de almacenamiento de los contenidos del documento " + docId + " en BBDD ...");
		}
		try {
			entity = getScannedDocumentEntity(docId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando los atributos de la entidad documento " + docId + " en BBDD ...");
			}
			for (Iterator<MimeType> it = docContentStorePaths.keySet().iterator(); it.hasNext();) {
				mimeType = it.next();

				if (WebscanConstants.CONTENT_MIME_TYPE_CONTENT_TYPE.equals(mimeType.getContentType())) {
					entity.setStoreTempPath(docContentStorePaths.get(mimeType));
				} else if (WebscanConstants.SIGNATURE_MIME_TYPE_CONTENT_TYPE.equals(mimeType.getContentType())) {
					entity.setStoreSignatureTempPath(docContentStorePaths.get(mimeType));
				} else if (WebscanConstants.OCR_MIME_TYPE_CONTENT_TYPE.equals(mimeType.getContentType())) {
					entity.setStoreOcrTextTempPath(docContentStorePaths.get(mimeType));
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Actualizando el documento escaneado " + entity.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Rutas de almacenamiento del documento escaneado " + entity.getId() + " actualizadas en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando las rutas de almacenamiento de los contenidos del documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando las rutas de almacenamiento de los contenidos del documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando las rutas de almacenamiento de los contenidos del documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando las rutas de almacenamiento de los contenidos del documento " + docId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#removeDocument(java.lang.Long)
	 */
	@Override
	public void removeDocument(Long docId) throws DAOException {
		ScannedDocument entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Eliminando el documento " + docId + " en BBDD ...");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la entidad documento " + docId + " en BBDD ...");
			}
			entity = getScannedDocumentEntity(docId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando setencia SQL para eliminar la entidad documento " + entity.getId() + " en BBDD ...");
			}
			entityManager.remove(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Entidad documento " + docId + " eliminada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando el documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#removeDocuments(java.util.List)
	 */
	@Override
	public void removeDocuments(List<Long> docIds) throws DAOException {
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Eliminando los documentos escaneados " + docIds + " ...");
		}

		try {
			Query query = entityManager.createQuery("DELETE FROM ScannedDocument sd WHERE sd.id IN :docIds");
			query.setParameter("docIds", docIds);
			int res = query.executeUpdate();

			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res + " documentos escaneados eliminados de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los documentos escaneados " + docIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los documentos escaneados " + docIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los documentos escaneados " + docIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los documentos escaneados " + docIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando los documentos escaneados " + docIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	// Operaciones sobre especificaciones de metadatos
	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#listMetadataSpecsByDocSpec(java.lang.Long)
	 */
	@Override
	public List<MetadataSpecificationDTO> listMetadataSpecsByDocSpec(Long docSpecificationId) throws DAOException {
		int queryResultSize = 0;
		DocumentSpecification entity;
		List<MetadataSpecification> ddbbRes = new ArrayList<MetadataSpecification>();
		List<MetadataSpecificationDTO> res = new ArrayList<MetadataSpecificationDTO>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando metadatos para la definición de documento " + docSpecificationId + " ...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(docSpecificationId, entityManager);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Obteniendo la relación de metadatos para la definición de documento " + docSpecificationId + " ...");
			}
			ddbbRes = entity.getMetadataSpecCollection();

			if (ddbbRes != null && !ddbbRes.isEmpty()) {
				queryResultSize = ddbbRes.size();
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Parseando la relación de metadatos para la definición de documento " + docSpecificationId + " ...");
				}
				for (MetadataSpecification ms: ddbbRes) {
					res.add(MappingUtilities.fillMetadataSpecificationDto(ms));
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + queryResultSize + "metadatos obtenidos para la definición de documento " + docSpecificationId + " ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los metadatos de la definición de documento " + docSpecificationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getMetadataSpecification(java.lang.Long)
	 */
	@Override
	public MetadataSpecificationDTO getMetadataSpecification(Long metadataSpecId) throws DAOException {
		MetadataSpecification entity;
		MetadataSpecificationDTO res = null;
		String excMsg;

		try {
			entity = getMetadataSpecificationEntity(metadataSpecId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando el metadato " + metadataSpecId + " ...");
			}
			res = MappingUtilities.fillMetadataSpecificationDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de metadato " + metadataSpecId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getMetadataSpecifications(java.util.List)
	 */
	@Override
	public List<MetadataSpecificationDTO> getMetadataSpecifications(List<Long> metadataSpecIds) throws DAOException {
		int queryResultSize = 0;
		List<MetadataSpecification> ddbbRes;
		List<MetadataSpecificationDTO> res = new ArrayList<MetadataSpecificationDTO>();
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando las definiciones de metadatos " + metadataSpecIds + " ...");
		}
		try {

			TypedQuery<MetadataSpecification> query = entityManager.createQuery("SELECT ms FROM MetadataSpecification ms WHERE ms.id IN :metadataSpecIds", MetadataSpecification.class);
			query.setParameter("metadataSpecIds", metadataSpecIds);
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillMetadataSpecificationListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + queryResultSize + " definiciones de metadatos recuperadas de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de metadatos " + metadataSpecIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de metadatos " + metadataSpecIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de metadatos " + metadataSpecIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de metadatos " + metadataSpecIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las definiciones de metadatos " + metadataSpecIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#setMetadataSpecifications(java.util.List,
	 *      java.lang.Long)
	 */
	@Override
	public void setMetadataSpecifications(List<MetadataSpecificationDTO> metadataSpecs, Long docSpecificationId) throws DAOException {
		DocumentSpecification entity;
		List<MetadataSpecification> entityMetadataSpecs;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Estableciendo una nueva colección de metadatos para la definición de documento " + docSpecificationId + " ...");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de documento " + docSpecificationId + " de BBDD ...");
			}
			entity = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(docSpecificationId, entityManager);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Estableciendo la nueva colección de metadatos en BBDD para la definición de documento " + docSpecificationId + " ...");
			}
			entityMetadataSpecs = entity.getMetadataSpecCollection();
			entity.setMetadataSpecCollection(mergeMetadataSpecifications(metadataSpecs, entityMetadataSpecs, entity));

			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva colección de metadatos para la definición de documento " + docSpecificationId + " establecida en BBDD.");
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo una nueva colección de metadatos para la definición de documento " + docSpecificationId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#listMetadataCollectionByDocument(java.lang.Long)
	 */
	@Override
	public List<MetadataDTO> listMetadataCollectionByDocument(Long documentId) throws DAOException {
		List<Metadata> ddbbRes = new ArrayList<Metadata>();
		List<MetadataDTO> res = new ArrayList<MetadataDTO>();
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los metadatos del documento escaneado " + documentId + " ...");
		}
		try {
			ScannedDocument entity = getScannedDocumentEntity(documentId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar los metadatos del documento escaneado " + documentId + " ...");
			}
			ddbbRes = entity.getMetadataCollection();

			res = MappingUtilities.fillMetadataListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " metadatos recuperados para el documento escaneado " + documentId + " de BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando metadatos del documento " + documentId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#listCompleteMetadataCollectionAndSpecByDocument(java.lang.Long)
	 */
	@Override
	public Map<MetadataSpecificationDTO, MetadataDTO> listCompleteMetadataCollectionAndSpecByDocument(Long documentId) throws DAOException {
		Boolean found;
		DocumentSpecification docSpec;
		ScannedDocument doc;
		List<MetadataSpecification> mdSpecs = new ArrayList<MetadataSpecification>();
		List<Metadata> metadataColl = new ArrayList<Metadata>();
		MetadataDTO mDto;
		Metadata metadata;
		Map<MetadataSpecificationDTO, MetadataDTO> res = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando el conjunto total de especificaciones y metadatos del documento escaneado " + documentId + " ...");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar el documento escaneado " + documentId + " ...");
			}
			doc = getScannedDocumentEntity(documentId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar los metadatos y la especificación de documentos del documento escaneado " + documentId + " ...");
			}
			metadataColl = doc.getMetadataCollection();
			docSpec = doc.getScanProfileOrgUnitDocSpec().getDocSpecification();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el conjunto total de especificaciones de metadatos ...");
			}
			mdSpecs = getCompleteDocSpecMetadataSpecs(docSpec);

			for (MetadataSpecification ms: mdSpecs) {
				found = Boolean.FALSE;
				mDto = MappingUtilities.fillEmptyMetadataDto(ms);

				for (Iterator<Metadata> it = metadataColl.iterator(); !found && it.hasNext();) {
					metadata = it.next();
					if (metadata.getSpecification().equals(ms)) {
						mDto = MappingUtilities.fillMetadataDto(metadata);
						found = Boolean.TRUE;
					}
				}

				res.put(MappingUtilities.fillMetadataSpecificationDto(ms), mDto);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " especificaciones de metadatos recuperadas para el documento escaneado " + documentId + " de BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando metadatos del documento " + documentId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Obtiene la lista completa de especificaciones de metadatos de una
	 * definición de documento, incluyendo las definiciones de metadatos de las
	 * especificaciones de documentos de las que hereda.
	 * 
	 * @param docSpec
	 *            Especificación de documentos.
	 * @return la lista completa de especificaciones de metadatos de una
	 *         definición de documento, incluyendo las definiciones de metadatos
	 *         de las especificaciones de documentos de las que hereda.
	 */
	private List<MetadataSpecification> getCompleteDocSpecMetadataSpecs(DocumentSpecification docSpec) {
		List<DocumentSpecification> docSpecs = new ArrayList<DocumentSpecification>();
		List<MetadataSpecification> res = new ArrayList<MetadataSpecification>();

		docSpecs.add(docSpec);
		getParentsDocSpec(docSpec.getParentDocs(), docSpecs);

		for (DocumentSpecification ds: docSpecs) {
			res.addAll(ds.getMetadataSpecCollection());
		}

		return res;
	}

	/**
	 * Obtiene la lista completa especificaciones de documento padre de una
	 * especificación determinada, incluyenda a ella misma.
	 * 
	 * @param parentDocSpecs
	 *            especificaciones de documentos de las que hereda.
	 * @param docSpecs
	 *            Lista resultado de especificaciones de documentos de las que
	 *            hereda.
	 */
	private void getParentsDocSpec(List<DocumentSpecification> parentDocSpecs, List<DocumentSpecification> docSpecs) {

		for (DocumentSpecification ds: parentDocSpecs) {
			// Evitamos incluir ciclos en la herencia de especificaciones de
			// documentos
			if (!docSpecs.contains(ds)) {
				docSpecs.add(ds);
				getParentsDocSpec(ds.getParentDocs(), docSpecs);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#listMetadataCollectionAndSpecByDocument(java.lang.Long)
	 */
	@Override
	public Map<MetadataSpecificationDTO, MetadataDTO> listMetadataCollectionAndSpecByDocument(Long documentId) throws DAOException {
		List<Metadata> ddbbRes = new ArrayList<Metadata>();
		Map<MetadataSpecificationDTO, MetadataDTO> res = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los metadatos del documento escaneado " + documentId + " ...");
		}
		try {
			ScannedDocument entity = getScannedDocumentEntity(documentId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar los metadatos del documento escaneado " + documentId + " ...");
			}
			ddbbRes = entity.getMetadataCollection();

			res = MappingUtilities.fillMetadataAndSpecMapDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " metadatos recuperados para el documento escaneado " + documentId + " de BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando metadatos del documento " + documentId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getMetadata(java.lang.Long)
	 */
	@Override
	public MetadataDTO getMetadata(Long metadataId) throws DAOException {
		Metadata entity;
		MetadataDTO res = null;
		String excMsg;

		try {
			entity = getMetadataEntity(metadataId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando el metadato " + metadataId + " recuperado de BBDD ...");
			}
			res = MappingUtilities.fillMetadataDto(entity);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el metadato " + metadataId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getMetadataCollection(java.util.List)
	 */
	@Override
	public List<MetadataDTO> getMetadataCollection(List<Long> metadataIds) throws DAOException {
		List<Metadata> ddbbRes;
		List<MetadataDTO> res = new ArrayList<MetadataDTO>();
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Recuperando los metadatos " + metadataIds + " ...");
		}
		try {

			TypedQuery<Metadata> query = entityManager.createQuery("SELECT metadata FROM Metadata metadata WHERE metadata.id IN :metadataIds", Metadata.class);
			query.setParameter("metadataIds", metadataIds);

			ddbbRes = query.getResultList();

			res = MappingUtilities.fillMetadataListDto(ddbbRes);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los metadatos " + metadataIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los metadatos " + metadataIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los metadatos " + metadataIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los metadatos " + metadataIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los metadatos " + metadataIds + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#setMetadataCollection(java.util.Map,
	 *      java.lang.Long)
	 */
	@Override
	public void setMetadataCollection(Map<MetadataSpecificationDTO, MetadataDTO> metadataCollection, Long scannedDocId) throws DAOException {
		ScannedDocument entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Estableciendo una nueva colección de metadatos para el documento " + scannedDocId + " ...");
		}
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el documento " + scannedDocId + " de BBDD ...");
			}

			entity = getScannedDocumentEntity(scannedDocId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Estableciendo la nueva colección de metadatos en BBDD para el documento " + scannedDocId + " ...");
			}
			entity.setMetadataCollection(mergeMetadataCollection(metadataCollection, entity.getMetadataCollection(), entity));

			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Nueva colección de metadatos para el documento " + scannedDocId + " establecida en BBDD.");
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo una nueva colección de metadatos para el documento " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#removeMetadataCollectionByMetadataSpecList(java.util.List)
	 */
	@Override
	public void removeMetadataCollectionByMetadataSpecList(List<MetadataSpecificationDTO> metadataSpecCollection) throws DAOException {
		int res = 0;
		List<Long> metadataSpecIds = new ArrayList<Long>();
		String jpqlQuery;
		String excMsg;

		/*DELETE FROM DOC_METADATA 
		WHERE DOC_METADATA.METADATA_SPEC_ID IN (1,2,3) 
		AND EXISTS
		(SELECT SCANNED_DOC.ID FROM SCANNED_DOC WHERE SCANNED_DOC.DIG_PROC_END_DATE IS NOT NULL AND SCANNED_DOC.ID = DOC_METADATA.SCANNED_DOC_ID);*/

		try {

			for (MetadataSpecificationDTO dto: metadataSpecCollection) {
				metadataSpecIds.add(dto.getId());
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Se inicia el borrado de los metadatados definidos por las siguientes especificaciones " + metadataSpecIds + " ...");
			}
			jpqlQuery = "DELETE FROM Metadata md WHERE md.specification IN (SELECT docSpec FROM DocumentSpecification docSpec WHERE docSpec.id IN :metadataSpecIds) AND EXITS (SELECT doc FROM ScannedDocument doc JOIN doc.metadataCollection auxMd WHERE auxMd.id = md.id AND doc.digProcEndDate IS NULL)";

			LOG.debug("[WEBSCAN-MODEL] Ejecutando sententencia en BBDD ...");

			Query query = entityManager.createQuery(jpqlQuery);
			query.setParameter("metadataSpecIds", metadataSpecIds);
			res = query.executeUpdate();

			LOG.debug("[WEBSCAN-MODEL] Sententencia ejecutada en BBDD ...");

			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res + " metadatos eliminados en BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando metadatados definidos por las siguientes especificaciones " + metadataSpecIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando metadatados definidos por las siguientes especificaciones " + metadataSpecIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando metadatados definidos por las siguientes especificaciones " + metadataSpecIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando metadatados definidos por las siguientes especificaciones " + metadataSpecIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando metadatados definidos por las siguientes especificaciones " + metadataSpecIds + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getScannedDocLog(java.lang.Long)
	 */
	@Override
	public ScannedDocLogDTO getScannedDocLog(Long scannedDocLogId) throws DAOException {
		ScannedDocLog entity;
		ScannedDocLogDTO res = null;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la traza de ejecución de documento " + scannedDocLogId + " ...");
			}
			entity = getScannedDocLogEntity(scannedDocLogId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Parseando la traza de ejecución de documento " + scannedDocLogId + " recuperada de BBDD...");
			}
			res = MappingUtilities.fillScannedDocLogDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de ejecución de documento " + scannedDocLogId + " recuperada de BBDD correctamente.");
			}
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de documento " + scannedDocLogId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getScannedDocLogsByDocId(java.lang.Long)
	 */
	@Override
	public List<ScannedDocLogDTO> getScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		List<ScannedDocLogDTO> res = new ArrayList<ScannedDocLogDTO>();
		List<ScannedDocLog> ddbbRes;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las trazas de ejecución del documento digitalizado " + scannedDocId + " de BBDD ...");
			}
			jpqlQuery = "SELECT sdl FROM ScannedDocLog sdl JOIN sdl.document doc WHERE doc.id = :scannedDocId";

			TypedQuery<ScannedDocLog> query = entityManager.createQuery(jpqlQuery, ScannedDocLog.class);
			query.setParameter("scannedDocId", scannedDocId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar las traza de ejecución del documento digitalizado " + scannedDocId + " de BBDD ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillScannedDocLogListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " trazas de ejecución del documento digitalizado " + scannedDocId + " recuperadas de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las trazas de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las trazas de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getScannedDocLogsByDocIdAndStageId(java.lang.Long,
	 *      java.lang.Long)
	 */
	@Override
	public ScannedDocLogDTO getScannedDocLogsByDocIdAndStageId(Long scannedDocId, Long scanProcessStageId) throws DAOException {
		ScannedDocLog entity;
		ScannedDocLogDTO res;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + " ...");
			}
			jpqlQuery = "SELECT sdl FROM ScannedDocLog sdl JOIN sdl.scanProcessStage stage JOIN sdl.document doc WHERE doc.id = :scannedDocId AND stage.id = :scanProcessStageId";

			TypedQuery<ScannedDocLog> query = entityManager.createQuery(jpqlQuery, ScannedDocLog.class);
			query.setParameter("scanProcessStageId", scanProcessStageId);
			query.setParameter("scannedDocId", scannedDocId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScannedDocLogDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de ejecución del documento digitalizado " + scannedDocId + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg, e);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución para la fase " + scanProcessStageId + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getLastScannedDocLogsByDocId(java.lang.Long)
	 */
	@Override
	public ScannedDocLogDTO getLastScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		ScannedDocLog entity;
		ScannedDocLogDTO res;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la última traza de ejecución del documento digitalizado " + scannedDocId + " ...");
			}
			jpqlQuery = "SELECT sdl FROM ScannedDocLog sdl JOIN sdl.document doc WHERE doc.id = :scannedDocId AND sdl.startDate = (SELECT MAX(auxSdl.startDate) FROM ScannedDocLog auxSdl JOIN auxSdl.document auxDoc WHERE auxDoc.id = :scannedDocId) AND sdl.id = (SELECT MAX(auxIdSdl.id) FROM ScannedDocLog auxIdSdl JOIN auxIdSdl.document auxIdDoc WHERE auxIdDoc.id = :scannedDocId)";

			TypedQuery<ScannedDocLog> query = entityManager.createQuery(jpqlQuery, ScannedDocLog.class);
			query.setParameter("scannedDocId", scannedDocId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la última traza de ejecución del documento digitalizado " + scannedDocId + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScannedDocLogDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de ejecución del documento digitalizado " + scannedDocId + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la última traza de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la última traza de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la última traza de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getFirstFailedScannedDocLogsByDocId(java.lang.Long)
	 */
	@Override
	public ScannedDocLogDTO getFirstFailedScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		ScannedDocLog entity;
		ScannedDocLogDTO res;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + " ...");
			}
			jpqlQuery = "SELECT sdl FROM ScannedDocLog sdl JOIN sdl.document doc WHERE doc.id = :scannedDocId AND sdl.status = :status AND sdl.startDate = (SELECT MIN(auxSdl.startDate) FROM ScannedDocLog auxSdl JOIN auxSdl.document auxDoc WHERE auxDoc.id = :scannedDocId AND auxSdl.status = :status)";

			TypedQuery<ScannedDocLog> query = entityManager.createQuery(jpqlQuery, ScannedDocLog.class);
			query.setParameter("scannedDocId", scannedDocId);
			query.setParameter("status", OperationStatus.FAILED);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScannedDocLogDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de ejecución del documento digitalizado " + scannedDocId + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la primera traza errónea de ejecución del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getRenewableAndPerformedScannedDocLogsByDocId(java.lang.Long)
	 */
	@Override
	public List<ScannedDocLogDTO> getRenewableAndPerformedScannedDocLogsByDocId(Long scannedDocId) throws DAOException {
		List<ScannedDocLogDTO> res = new ArrayList<ScannedDocLogDTO>();
		List<ScannedDocLog> ddbbRes;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando las trazas de ejecución de fases reanudables del documento digitalizado " + scannedDocId + " de BBDD ...");
			}
			jpqlQuery = "SELECT sdl FROM ScannedDocLog sdl JOIN sdl.scanProcessStage stage JOIN sdl.document doc JOIN doc.scanProfileOrgUnitDocSpec spouds JOIN spouds.orgScanProfile osp JOIN osp.scanProfile sp JOIN sp.specification sps JOIN sps.scanProcStages spss JOIN spss.scanProcessStage scanProfStage WHERE doc.id = :scannedDocId AND spss.renewable = TRUE AND stage.id = scanProfStage.id ORDER BY spss.order";

			TypedQuery<ScannedDocLog> query = entityManager.createQuery(jpqlQuery, ScannedDocLog.class);
			query.setParameter("scannedDocId", scannedDocId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar las trazas de ejecución de fases reanudables del documento digitalizado " + scannedDocId + " de BBDD ...");
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillScannedDocLogListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " trazas de ejecución de fases reanudables del documento  digitalizado " + scannedDocId + " recuperadas de BBDD ...");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución de fases reanudables del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las trazas de ejecución de fases reanudables del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución de fases reanudables del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución de fases reanudables del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución de fases reanudables del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar las trazas de ejecución de fases reanudables del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando las trazas de ejecución de fases reanudables del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getScannedDocLogByStageNameAndDocId(java.lang.Long,
	 *      java.lang.String)
	 */
	@Override
	public ScannedDocLogDTO getScannedDocLogByStageNameAndDocId(Long scannedDocId, String stageName) throws DAOException {
		ScannedDocLog entity;
		ScannedDocLogDTO res;
		String excMsg, jpqlQuery;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + " ...");
			}
			jpqlQuery = "SELECT sdl FROM ScannedDocLog sdl JOIN sdl.document doc JOIN sdl.scanProcessStage sps WHERE doc.id = :scannedDocId AND sps.name = :stageName";

			TypedQuery<ScannedDocLog> query = entityManager.createQuery(jpqlQuery, ScannedDocLog.class);
			query.setParameter("scannedDocId", scannedDocId);
			query.setParameter("stageName", stageName);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando consulta para recuperar la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + " ...");
			}
			entity = query.getSingleResult();

			res = MappingUtilities.fillScannedDocLogDto(entity);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de ejecución del documento digitalizado " + scannedDocId + " recuperada: " + res.getId());
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (NoResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_901, excMsg);
		} catch (NonUniqueResultException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			throw new DAOException(DAOException.CODE_902, excMsg);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de la fase " + stageName + " del documento digitalizado " + scannedDocId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#createScannedDocLog(es.ricoh.webscan.model.dto.ScannedDocLogDTO,
	 *      java.lang.Long, java.lang.Long)
	 */
	@Override
	public Long createScannedDocLog(ScannedDocLogDTO scannedDocLogDto, Long scannedDocId, Long scanProcessStageId) throws DAOException {
		Long res;
		ScannedDocLog entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Creando la traza de documento digitalizado " + scannedDocLogDto + " ...");
		}
		try {
			entity = MappingUtilities.fillScannedDocLogEntity(scannedDocLogDto);
			entity.setDocument(getScannedDocumentEntity(scannedDocId));
			entity.setScanProcessStage(CommonUtilitiesDAOJdbcImpl.getScanProcessStageEntity(scanProcessStageId, entityManager));

			entityManager.persist(entity);
			entityManager.flush();

			res = entity.getId();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de documento digitalizado " + entity.getId() + " creada en BBDD.");
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de documento digitalizado " + scannedDocLogDto + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de documento digitalizado " + scannedDocLogDto + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de documento digitalizado " + scannedDocLogDto + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de documento digitalizado " + scannedDocLogDto + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al crear la traza de documento digitalizado " + scannedDocLogDto + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#updateScannedDocLog(es.ricoh.webscan.model.dto.ScannedDocLogDTO)
	 */
	@Override
	public void updateScannedDocLog(ScannedDocLogDTO scannedDocLogDto) throws DAOException {
		ScannedDocLog entity;
		String excMsg;
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-MODEL] Actualizando la traza de documento digitalizado " + scannedDocLogDto.getId() + " ...");
		}
		try {
			entity = getScannedDocLogEntity(scannedDocLogDto.getId());

			// Se actualizan los datos globales
			entity.setEndDate(scannedDocLogDto.getEndDate());
			entity.setStatus(scannedDocLogDto.getStatus());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Modificando la traza de documento digitalizado " + scannedDocLogDto.getId() + " en BBDD ...");
			}
			entityManager.merge(entity);
			entityManager.flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de documento digitalizado " + scannedDocLogDto.getId() + " actualizada en BBDD ...");
			}
		} catch (DAOException e) {
			throw e;
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la traza de documento digitalizado " + scannedDocLogDto.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la traza de documento digitalizado " + scannedDocLogDto.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la traza de documento digitalizado " + scannedDocLogDto.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error actualizando la traza de documento digitalizado " + scannedDocLogDto.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);

		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#removeScannedDocLogs(java.lang.Long)
	 */
	@Override
	public void removeScannedDocLogs(Long docId) throws DAOException {
		ScannedDocument doc;
		int res = 0;
		String excMsg;

		try {
			LOG.debug("[WEBSCAN-MODEL] Creando sententencia en BBDD para eliminar las trazas de ejecución del documento {} ...", docId);

			doc = getScannedDocumentEntity(docId);

			res = doc.getExecLogs().size();

			doc.getExecLogs().clear();
			entityManager.merge(doc);
			entityManager.flush();

			LOG.debug("[WEBSCAN-MODEL] Sententencia ejecutada en BBDD ...");
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] {} trazas eliminadas para el documento {} en BBDD ...", res, docId);
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las trazas de ejecución del documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las trazas de ejecución del documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las trazas de ejecución del documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las trazas de ejecución del documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las trazas de ejecución del documento " + docId + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}
	}

	/**
	 * Efectúa la fusión en BBDD de la relación actual de metadatos y la nueva
	 * relación de metados de una especificación de documento.
	 * 
	 * @param newSpecifications
	 *            Nueva relación de metadatos.
	 * @param currentSpecifications
	 *            Actual relación de metadatos almacenada en BBDD.
	 * @param docSpec
	 *            Especificación de documento.
	 * @return Nueva lista de metadatos.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o registrar los nuevos
	 *             metadatos en BBDD.
	 */
	private List<MetadataSpecification> mergeMetadataSpecifications(List<MetadataSpecificationDTO> newSpecifications, List<MetadataSpecification> currentSpecifications, DocumentSpecification docSpec) throws DAOException {
		List<MetadataSpecification> res = new ArrayList<MetadataSpecification>();
		MetadataSpecification msAux;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Estableciendo una nueva lista de metadatos para la definición de documento " + docSpec.getName() + " en BBDD ...");
			}
			if (newSpecifications != null) {
				// Inserciones y actualizaciones
				LOG.debug("[WEBSCAN-MODEL] Se añaden las nuevas definiciones de metadatos y actualizan las ya existentes ...");

				for (MetadataSpecificationDTO msDto: newSpecifications) {
					msAux = updateOrCreateMetadataSpecification(msDto, currentSpecifications, docSpec);
					res.add(msAux);
				}
			}

			// Borrados
			removeNotFoundMetadataSpecs(currentSpecifications, newSpecifications);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error modificando la lista de definiciones de metadatos para la definición de documento " + docSpec.getName() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Añade o actualiza metadatos para una entidad especificación de documento.
	 * 
	 * @param msDto
	 *            Metadato a actualizar o registrar.
	 * @param metadataSpecifications
	 *            Relación actual de metadatos de la definición de documentos.
	 * @param docSpec
	 *            Especificación de documento.
	 * @return La entidad especificación de metadato actualizada o registrada en
	 *         BBDD.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o perisitir la
	 *             especificación de metadato en BBDD.
	 */
	private MetadataSpecification updateOrCreateMetadataSpecification(MetadataSpecificationDTO msDto, List<MetadataSpecification> metadataSpecifications, DocumentSpecification docSpec) throws DAOException {
		Boolean found = Boolean.FALSE;
		MetadataSpecification res;
		String excMsg;
		List<Metadata> metadata;

		res = null;

		try {

			for (Iterator<MetadataSpecification> it = metadataSpecifications.iterator(); !found && it.hasNext();) {
				res = it.next();

				if (res.getId().equals(msDto.getId())) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("[WEBSCAN-MODEL] Actualizando metadato " + res.getName() + " ...");
					}
					metadata = res.getMetadataCollection();

					// Se reflejan los cambios en BBDD
					// Se actualiza entidad
					res = MappingUtilities.fillMetadataSpecificationEntity(msDto);
					res.setMetadataCollection(metadata);
					res.setDocSpecification(docSpec);
					entityManager.merge(res);

					found = Boolean.TRUE;
				}
			}

			if (!found) {
				// Nuevo
				res = MappingUtilities.fillMetadataSpecificationEntity(msDto);
				res.setDocSpecification(docSpec);
				res.setId(null);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-MODEL] Añadiendo nuevo metadato " + res.getName() + " ...");
				}
				entityManager.persist(res);
			}
		} catch (EntityExistsException e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de metadatos de la definición de documento " + docSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(DAOException.CODE_902, excMsg, e);
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de metadatos de la definición de documento " + docSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de metadatos de la definición de documento " + docSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de metadatos de la definición de documento " + docSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error al establecer la relación de metadatos de la definición de documento " + docSpec.getId() + " en BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Elimina los metadatos no presentes en la nueva relación de metados de una
	 * entidad especificación de documento.
	 * 
	 * @param metadataSpecs
	 *            Relación actual de metadatos de una especificación de
	 *            documento.
	 * @param newMetadataSpecs
	 *            Nueva relación de metadatos de una especificación de
	 *            documento.
	 * @throws DAOException
	 *             Si ocurre algún error al eliminar los metadatos en BBDD.
	 */
	private void removeNotFoundMetadataSpecs(List<MetadataSpecification> metadataSpecs, List<MetadataSpecificationDTO> newMetadataSpecs) throws DAOException {
		Boolean found;
		MetadataSpecificationDTO msDto;

		for (MetadataSpecification metadataSpec: metadataSpecs) {
			found = Boolean.FALSE;

			if (newMetadataSpecs != null) {
				for (Iterator<MetadataSpecificationDTO> it = newMetadataSpecs.iterator(); !found && it.hasNext();) {
					msDto = it.next();
					found = metadataSpec.getId().equals(msDto.getId());
				}
			}
			if (!found) {
				entityManager.remove(metadataSpec);
			}
		}
	}

	/**
	 * Fusiona las especificaciones de documentos heredadas, o de las que
	 * hereda, una determinada definición de documentos.
	 * 
	 * @param newInheritedDocuments
	 *            Nueva lista de especificaciones de documentos heredada, o de
	 *            las que hereda.
	 * @param currentInheritedDocuments
	 *            Lista actual de especificaciones de documentos heredada, o de
	 *            las que hereda.
	 * @param docSpec
	 *            Entidad definición de documentos.
	 * @param areParents
	 *            indica si se procesa lista de definiciones de documento padre
	 *            (true), o hija (false).
	 * @return Nueva lista de especificaciones de documentos heredadas, o de las
	 *         que hereda, la definición de documentos.
	 * @throws DAOException
	 *             Si ocurre algún error al recuperar definiciones de documento
	 *             de BBDD.
	 */
	private List<DocumentSpecification> mergeInheritedDocumentSpecifications(List<DocumentSpecificationDTO> newInheritedDocuments, List<DocumentSpecificationDTO> currentInheritedDocuments, DocumentSpecification docSpec, Boolean areParents) throws DAOException {
		List<DocumentSpecification> res = new ArrayList<DocumentSpecification>();
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Estableciendo una nueva relación de herencia de definciones de documentos para la definición de documento " + docSpec.getName() + " en BBDD ...");
			}
			res = addInheritedDocumentSpecifications(newInheritedDocuments, currentInheritedDocuments, docSpec, areParents);

			removeInheritedDocumentSpecifications(newInheritedDocuments, currentInheritedDocuments, docSpec, areParents);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error estableciendo una nueva relación de herencia de definciones de documentos para la definición de documento " + docSpec.getName() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Obtiene la nueva relación de especificaciones de documentos heredadas, o
	 * de las que hereda, una determinada definición de documentos al fusionar
	 * la nueva lista con la actualmente registrada en BBDD.
	 * 
	 * @param newInheritedDocuments
	 *            Nueva lista de especificaciones de documentos heredada, o de
	 *            las que hereda.
	 * @param currentInheritedDocuments
	 *            Lista actual de especificaciones de documentos heredada, o de
	 *            las que hereda.
	 * @param docSpec
	 *            Entidad definición de documentos.
	 * @param areParents
	 *            indica si se procesa lista de definiciones de documento padre
	 *            (true), o hija (false).
	 * @return Nueva lista de especificaciones de documentos heredadas, o de las
	 *         que hereda, la definición de documentos.
	 * @throws DAOException
	 *             Si ocurre algún error al ejecutar alguna de las sentecias de
	 *             BBDD.
	 */
	private List<DocumentSpecification> addInheritedDocumentSpecifications(List<DocumentSpecificationDTO> newInheritedDocuments, List<DocumentSpecificationDTO> currentInheritedDocuments, DocumentSpecification docSpec, Boolean areParents) throws DAOException {
		Boolean inheritedDocumentFound;
		List<DocumentSpecification> res = new ArrayList<DocumentSpecification>();
		DocumentSpecification dsAux;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Añadiendo documentos a la nueva relación de herencia de definciones de documentos para la definición de documento " + docSpec.getName() + " en BBDD ...");
			}
			inheritedDocumentFound = newInheritedDocuments != null && !newInheritedDocuments.isEmpty();

			if (inheritedDocumentFound) {
				// Inserciones y actualizaciones
				LOG.debug("[WEBSCAN-MODEL] Se añaden las nuevas definiciones de documentos y mantienen las ya existentes ...");

				for (DocumentSpecificationDTO dsDto: newInheritedDocuments) {
					dsAux = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(dsDto.getId(), entityManager);
					if (LOG.isDebugEnabled()) {
						LOG.debug("[WEBSCAN-MODEL] Incluyendo definición de documento " + dsAux.getName() + " ...");
					}
					res.add(dsAux);

					if (areParents && !dsAux.getChildDocs().contains(docSpec)) {
						dsAux.getChildDocs().add(docSpec);
					} else if (!areParents && !dsAux.getParentDocs().contains(docSpec)) {
						dsAux.getParentDocs().add(docSpec);
					}
					entityManager.merge(dsAux);
				}
			}
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error añadiendo documentos a la nueva relación de herencia para la definición de documento " + docSpec.getName() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Elimina especificaciones de documentos heredadas, o de las que hereda, de
	 * una determinada definición de documentos.
	 * 
	 * @param newInheritedDocuments
	 *            Nueva lista de especificaciones de documentos heredada, o de
	 *            las que hereda.
	 * @param currentInheritedDocuments
	 *            Lista actual de especificaciones de documentos heredada, o de
	 *            las que hereda.
	 * @param docSpec
	 *            Entidad definición de documentos.
	 * @param areParents
	 *            indica si se procesa lista de definiciones de documento padre
	 *            (true), o hija (false).
	 * @throws DAOException
	 *             Si ocurre algún error al recuperar definiciones de documento
	 *             de BBDD.
	 */
	private void removeInheritedDocumentSpecifications(List<DocumentSpecificationDTO> newInheritedDocuments, List<DocumentSpecificationDTO> currentInheritedDocuments, DocumentSpecification docSpec, Boolean areParents) throws DAOException {
		Boolean found;
		DocumentSpecification dsAux;
		DocumentSpecificationDTO removeDsDto;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Eliminando las definiciones existentes y no incluidas en la nueva relación de herencia para la definición de documento " + docSpec.getName() + " en BBDD ...");
			}
			if (currentInheritedDocuments != null && !currentInheritedDocuments.isEmpty()) {
				// Borrados

				for (DocumentSpecificationDTO auxDsDto: currentInheritedDocuments) {
					found = Boolean.FALSE;
					for (Iterator<DocumentSpecificationDTO> it = newInheritedDocuments.iterator(); !found && it.hasNext();) {
						removeDsDto = it.next();
						found = removeDsDto.getId().equals(auxDsDto.getId());
					}

					if (!found) {
						dsAux = CommonUtilitiesDAOJdbcImpl.getDocumentSpecificationEntity(auxDsDto.getId(), entityManager);
						// Se elimina relación
						if (areParents) {
							dsAux.getChildDocs().remove(docSpec);
						} else {
							dsAux.getParentDocs().remove(docSpec);
						}
						entityManager.merge(dsAux);
					}
				}

			}
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error eliminando las definiciones existentes y no incluidas en la nueva relación de herencia para la definición de documento " + docSpec.getName() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

	}

	/**
	 * Recupera de BBDD una entidad documento digitalizado a partir de su
	 * identificador.
	 * 
	 * @param docId
	 *            Identificador de documento digitalizado.
	 * @return Entidad de BBDD documento digitalizado.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private ScannedDocument getScannedDocumentEntity(Long docId) throws DAOException {
		ScannedDocument res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el documento escaneado " + docId + " de BBDD ...");
			}
			res = entityManager.find(ScannedDocument.class, docId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando el documento escaneado " + docId + " en BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Documento escaneado " + res.getId() + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el documento escaneado " + docId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el documento escaneado " + docId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad especificación de metadatos a partir de su
	 * identificador.
	 * 
	 * @param metadataSpecificationId
	 *            Identificador de especificación de metadatos.
	 * @return Entidad de BBDD especificación de metadatos.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private MetadataSpecification getMetadataSpecificationEntity(Long metadataSpecificationId) throws DAOException {
		MetadataSpecification res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la definición de metadato " + metadataSpecificationId + " de BBDD ...");
			}
			res = entityManager.find(MetadataSpecification.class, metadataSpecificationId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de metadato " + metadataSpecificationId + " en BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Definición de metadato " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de metadato " + metadataSpecificationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la definición de metadato " + metadataSpecificationId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad metadato a partir de su identificador.
	 * 
	 * @param metadataId
	 *            Identificador de metadato.
	 * @return Entidad de BBDD metadato.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private Metadata getMetadataEntity(Long metadataId) throws DAOException {
		Metadata res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando el metadato " + metadataId + " de BBDD ...");
			}
			res = entityManager.find(Metadata.class, metadataId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando el metadato " + metadataId + " en BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Metadato " + res.getId() + " recuperado de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el metadato " + metadataId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando el metadato " + metadataId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Recupera de BBDD una entidad traza de ejecución de documento digitalizado
	 * a partir de su identificador.
	 * 
	 * @param scannedDocLogId
	 *            Identificador de traza de ejecución de documento digitalizado.
	 * @return Entidad de BBDD traza de ejecución de documento digitalizado.
	 * @throws DAOException
	 *             Si no existe la entidad, o se pruduce algún error al realizar
	 *             sobre la BBDD.
	 */
	private ScannedDocLog getScannedDocLogEntity(Long scannedDocLogId) throws DAOException {
		ScannedDocLog res;
		String excMsg;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Recuperando la traza de ejecución de documento digitalizado " + scannedDocLogId + " de BBDD ...");
			}
			res = entityManager.find(ScannedDocLog.class, scannedDocLogId);

			if (res == null) {
				excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de documento digitalizado " + scannedDocLogId + " en BBDD. La entidad no existe.";
				LOG.error(excMsg);
				throw new DAOException(DAOException.CODE_901, excMsg);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Traza de ejecución de documento digitalizado " + res.getId() + " recuperada de BBDD.");
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de documento digitalizado " + scannedDocLogId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando la traza de ejecución de documento digitalizado " + scannedDocLogId + " de BBDD: " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Efectúa la fusión en BBDD de la relación actual de metadatos y la nueva
	 * relación de metados de un documento digitalizado.
	 * 
	 * @param newMetadataCollection
	 *            Nueva relación de metadatos.
	 * @param currentMetadataCollection
	 *            Actual relación de metadatos almacenada en BBDD.
	 * @param doc
	 *            Documento digitalizado.
	 * @return Nueva lista de metadatos.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o registrar los nuevos
	 *             metadatos en BBDD.
	 */
	private List<Metadata> mergeMetadataCollection(Map<MetadataSpecificationDTO, MetadataDTO> newMetadataCollection, List<Metadata> currentMetadataCollection, ScannedDocument doc) throws DAOException {
		List<Metadata> res = new ArrayList<Metadata>();
		MetadataDTO mDto;
		Metadata mAux;
		MetadataSpecificationDTO mSpec;
		String excMsg;

		try {
			if (newMetadataCollection != null) {
				// Inserciones y actualizaciones
				for (Iterator<MetadataSpecificationDTO> it = newMetadataCollection.keySet().iterator(); it.hasNext();) {
					mSpec = it.next();
					mDto = newMetadataCollection.get(mSpec);
					mAux = updateOrCreateMetadata(mDto, currentMetadataCollection, mSpec, doc);
					res.add(mAux);
				}
			}

			// Borrados
			removeNotFoundMetadata(currentMetadataCollection, newMetadataCollection.values());
		} catch (DAOException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error modificando la lista de metadatos para el documento " + doc.getId() + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		}

		return res;
	}

	/**
	 * Añade o actualiza metadatos para una entidad documento digitalizado.
	 * 
	 * @param mDto
	 *            Metadato a actualizar o registrar.
	 * @param metadataCollection
	 *            Relación actual de metadatos del documento digitalizado.
	 * @param doc
	 *            Documento digitalizado.
	 * @param mSpec
	 *            Especificación del metadatos a insertar o actualizar.
	 * @return La entidad metadato actualizada o registrada en BBDD.
	 * @throws DAOException
	 *             Si ocurre algún error al actualizar o perisitir el metadato
	 *             en BBDD.
	 */
	private Metadata updateOrCreateMetadata(MetadataDTO mDto, List<Metadata> metadataCollection, MetadataSpecificationDTO mSpec, ScannedDocument doc) throws DAOException {
		Boolean found = Boolean.FALSE;
		Metadata res;

		res = null;

		for (Iterator<Metadata> it = metadataCollection.iterator(); !found && it.hasNext();) {
			res = it.next();

			if (res.getId().equals(mDto.getId())) {
				// Se reflejan los cambios en BBDD
				// Se actualiza entidad
				res.setValue(mDto.getValue());

				entityManager.merge(res);
				found = Boolean.TRUE;
			}
		}

		if (!found) {
			// Nuevo
			res = MappingUtilities.fillMetadataEntity(mDto);
			res.setDocument(doc);
			res.setSpecification(getMetadataSpecificationEntity(mSpec.getId()));
			res.setId(null);
			entityManager.persist(res);
		}

		return res;
	}

	/**
	 * Elimina los metadatos no presentes en la nueva relación de metados de una
	 * entidad documento digitalizado.
	 * 
	 * @param metadataCollection
	 *            Relación actual de metadatos de un documento digitalizado.
	 * @param newMetadataCollection
	 *            Nueva relación de metadatos de un documento digitalizado.
	 * @throws DAOException
	 *             Si ocurre algún error al eliminar los metadatos en BBDD.
	 */
	private void removeNotFoundMetadata(List<Metadata> metadataCollection, Collection<MetadataDTO> newMetadataCollection) throws DAOException {
		Boolean found;
		MetadataDTO mDto;

		for (Metadata metadata: metadataCollection) {
			found = Boolean.FALSE;

			if (newMetadataCollection != null) {
				for (Iterator<MetadataDTO> it = newMetadataCollection.iterator(); !found && it.hasNext();) {
					mDto = it.next();
					found = metadata.getId().equals(mDto.getId());
				}
			}
			if (!found) {
				entityManager.remove(metadata);
			}
		}
	}

	/**
	 * Genera la consulta JPA necesaria para recuperar los documentos de un
	 * determinado lote de documentos.
	 *
	 * @param batchId
	 *            Identificador de lote de documentos. Puede ser nulo.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param orderByColumns
	 *            clausulas de ordenación.
	 * @return consulta JPA necesaria para recuperar los documentos de un
	 *         determinado lote de documentos.
	 */
	private TypedQuery<ScannedDocument> buildGetScannedDocsByBatchId(String batchId, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> orderByColumns) {
		int first, maxResults;
		Map<String, List<EntityOrderByClause>> docOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		String jpqlQuery;
		TypedQuery<ScannedDocument> res;

		if (batchId == null || (batchId != null && WebscanModelConstants.NULL_BATCH_ID.equals(batchId))) {
			jpqlQuery = "SELECT sd FROM ScannedDocument sd WHERE sd.batchId IS NULL";
		} else {
			jpqlQuery = "SELECT sd FROM ScannedDocument sd WHERE sd.batchId = :batchId";
		}

		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			docOrderByColumns.put("sd.", orderByColumns);
		}

		jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery(jpqlQuery, docOrderByColumns);

		res = entityManager.createQuery(jpqlQuery, ScannedDocument.class);

		if (elementsPerPage != null) {
			first = 0;
			maxResults = elementsPerPage.intValue();
			if (pageNumber != null) {
				first = (pageNumber.intValue() - 1) * maxResults;
			}

			res.setFirstResult(first);
			res.setMaxResults(maxResults);
		}

		if (batchId != null && !WebscanModelConstants.NULL_BATCH_ID.equals(batchId)) {
			res.setParameter("batchId", batchId);
		}

		return res;
	}
	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.model.dao.DocumentDAO#getActiveDocumentsByBatchId(java.lang.Long,
	 *      java.util.List)
	 */
	@Override
	public List<ScannedDocumentDTO> getActiveDocumentsByBatchId(Long batchId, List<EntityOrderByClause> orderByColumns) throws DAOException {
		List<ScannedDocumentDTO> res = new ArrayList<ScannedDocumentDTO>();
		List<ScannedDocument> ddbbRes;
		String excMsg;
		TypedQuery<ScannedDocument> query;

		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Construyendo la consulta para recuperar los documentos digitalizados en proceso para la caratula/reserva {} de BBDD.", batchId);
			}
			query = buildGetDocumentsByBatchId(batchId, orderByColumns);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] Ejecutando la consulta para recuperar los documentos digitalizados en proceso para la caratula/reserva {} de BBDD.", batchId);
			}
			ddbbRes = query.getResultList();

			res = MappingUtilities.fillSummarizedScannedDocumentListDto(ddbbRes);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-MODEL] " + res.size() + " documentos digitalizados en proceso para la caratula/reserva {} de BBDD.", batchId);
			}
		} catch (IllegalStateException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (QueryTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados en proceso para la caratula/reserva " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (TransactionRequiredException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PessimisticLockException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (LockTimeoutException e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (PersistenceException e) {
			excMsg = "[WEBSCAN-MODEL] Se excedio el tiempo máximo para recuperar los documentos digitalizados en proceso para la caratula/reserva " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} catch (Exception e) {
			excMsg = "[WEBSCAN-MODEL] Error recuperando los documentos digitalizados en proceso para la caratula/reserva " + batchId + ": " + e.getMessage();
			LOG.error(excMsg, e);
			throw new DAOException(excMsg, e);
		} finally {
			CommonUtilitiesDAOJdbcImpl.closeEntityManager(entityManager);
		}

		return res;
	}

	/**
	 * Método auxiliar que construye la consulta responsable de recuperar la
	 * relación de documentos que están siendo procesados para una reserva o
	 * caratula determinada.
	 * 
	 * @param batchId
	 *            Identificador de proceso de digitalización.
	 * @param orderByColumns
	 *            columnas mediante las que será ordenada la relación de
	 *            documentos recuperada.
	 * @return Consulta JPQL que recupera la relación de documentos que están
	 *         siendo procesados por un usuario y pertenecen a una reserva o
	 *         caratula determinada.
	 */
	private TypedQuery<ScannedDocument> buildGetDocumentsByBatchId(Long batchId, List<EntityOrderByClause> orderByColumns) {
		String jpqlQuery;
		Map<String, List<EntityOrderByClause>> docOrderByColumns = new HashMap<String, List<EntityOrderByClause>>();
		TypedQuery<ScannedDocument> res;
		//jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery("SELECT sd FROM ScannedDocument sd LEFT JOIN FETCH sd.execLogs logs LEFT JOIN FETCH logs.scanProcessStage stage JOIN FETCH sd.scanProfileOrgUnitDocSpec spouds JOIN FETCH spouds.orgScanProfile spou JOIN FETCH spou.orgUnit orgUnit WHERE orgUnit.id IN :orgUnitIds", docOrderByColumns);
//		jpqlQuery = "SELECT sd FROM ScannedDocument sd WHERE sd.digProcEndDate IS NULL AND sd.batchId = :batchId";
		jpqlQuery = "SELECT sd FROM ScannedDocument sd LEFT JOIN FETCH sd.batch bt LEFT JOIN FETCH sd.execLogs logs LEFT JOIN FETCH logs.scanProcessStage stage JOIN FETCH sd.scanProfileOrgUnitDocSpec spouds JOIN FETCH spouds.orgScanProfile spou JOIN FETCH spou.orgUnit orgUnit WHERE sd.digProcEndDate IS NULL AND bt.id = :batchId";
		//jpqlQuery = "SELECT sd FROM ScannedDocument sd LEFT JOIN FETCH sd.batch bt WHERE sd.digProcEndDate IS NULL AND bt.id = :batchId";
		                             
		if (orderByColumns != null && !orderByColumns.isEmpty()) {
			docOrderByColumns.put("sd.", orderByColumns);
		}

		jpqlQuery = CommonUtilitiesDAOJdbcImpl.builOrderByJpqlQuery(jpqlQuery, docOrderByColumns);

		res = entityManager.createQuery(jpqlQuery, ScannedDocument.class);

		res.setParameter("batchId", batchId);

		return res;
	}

}
