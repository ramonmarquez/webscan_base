/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.gva.webscangv.scandoc.contr.controller.vo.validateFastScanDocVO.java.</p>
 * 
 * <b>Descripción:</b><p> Clase que recoge la información de la vista de validación de un/os documento/s digitalizado/s.</p>
 * 
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox.controller.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.ricoh.webscan.docbox.service.DocBoxDocumentDTO;
//import es.ricoh.webscan.docbox.service.DocBoxDocumentDTO;

/**
 * Clase que recoge la información de la vista de validación de un documento
 * digitalizado.
 * <p>
 * Clase ValidateDocBoxVO
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ValidateDocBoxVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Trabajo de digitalización en curso.
	 */
	private ActiveScanWorkVO activeScanWork;

	/**
	 * Listado de documentos digitalizados mediante el proceso de digitalización
	 * rápida.
	 */
	private List<DocBoxDocumentDTO> listFastScannedDocument = new ArrayList<DocBoxDocumentDTO>();

	/**
	 * Constructor sin argumentos.
	 */
	public ValidateDocBoxVO() {
		super();
	}

	/**
	 * Gets the value of the attribute {@link #activeScanWork}.
	 * 
	 * @return the value of the attribute {@link #activeScanWork}.
	 */
	public ActiveScanWorkVO getActiveScanWork() {
		return activeScanWork;
	}

	/**
	 * Sets the value of the attribute {@link #activeScanWork}.
	 * 
	 * @param anActiveScanWork
	 *            The value for the attribute {@link #activeScanWork}.
	 */
	public void setActiveScanWork(ActiveScanWorkVO anActiveScanWork) {
		this.activeScanWork = anActiveScanWork;
	}

	/**
	 * Gets the value of the attribute {@link #listFastDocumentoScanned}.
	 * 
	 * @return the value of the attribute {@link #listFastDocumentoScanned}.
	 */
	public List<DocBoxDocumentDTO> getListFastScannedDocument() {
		return listFastScannedDocument;
	}

	/**
	 * Sets the value of the attribute {@link #listFastDocumentoScanned}.
	 * 
	 * @param aListFastScannedDocument
	 *            The value for the attribute {@link #listFastDocumentoScanned}.
	 */
	public void setListFastScannedDocument(List<DocBoxDocumentDTO> aListFastScannedDocument) {
		this.listFastScannedDocument.clear();

		if (aListFastScannedDocument != null && !aListFastScannedDocument.isEmpty()) {
			this.listFastScannedDocument.addAll(aListFastScannedDocument);
		}

	}

	public Boolean existActiveScanWork() {
		return !this.listFastScannedDocument.isEmpty();
	}
}
