/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.FastScannedDocumentDTO.java.</p>
 * <b>Descripción:</b><p> DTO para la entidad JPA documento digitalizado.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.docbox.service;

import java.io.Serializable;

import es.ricoh.webscan.model.dto.ScannedDocumentDTO;

/**
 * DTO para la entidad JPA documento digitalizado rápido.
 * <p>
 * Clase DocBoxDocumentDTO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public final class DocBoxDocumentDTO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Información del documento digitalizado mediante el proceso d
	 * digitalización rápida.
	 */
	private ScannedDocumentDTO doc;

	/**
	 * Estado del documento para el proceso de digitalización rápida.
	 */
	private StatusDocBox status;

	/**
	 * Constructor sin argumentos.
	 */
	public DocBoxDocumentDTO() {
		super();
	}

	/**
	 * Gets the value of the attribute {@link #doc}.
	 * 
	 * @return the value of the attribute {@link #doc}.
	 */
	public ScannedDocumentDTO getDoc() {
		return doc;
	}

	/**
	 * Sets the value of the attribute {@link #doc}.
	 * 
	 * @param aDoc
	 *            The value for the attribute {@link #doc}.
	 */
	public void setDoc(ScannedDocumentDTO aDoc) {
		this.doc = aDoc;
	}

	/**
	 * Gets the value of the attribute {@link #status}.
	 * 
	 * @return the value of the attribute {@link #status}.
	 */
	public StatusDocBox getStatus() {
		return status;
	}

	/**
	 * Sets the value of the attribute {@link #status}.
	 * 
	 * @param status
	 *            The value for the attribute {@link #status}.
	 */
	public void setStatus(StatusDocBox aStatus) {
		this.status = aStatus;
	}

}
