/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.gva.webscangv.fastscandoc.contr.service.StatusFastScanDoc.java.</p>
* <b>Descripción:</b><p> Tipos de estados que tienen los documentos de la validacion de digitalización rápida.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.docbox.service;

/**
 * Tipos de estados que tienen los documentos de la validacion de digitalización
 * rápida.
 * <p>
 * Clase StatusFastScanDoc.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum StatusDocBox {
	/**
	 * Estado OK.
	 */
	OK("0"),
	/**
	 * Estado con ERROR.
	 */
	ERROR("1"),
	/**
	 * Estado pendiente de validación del documento.
	 */
	NO_CHECKED("2"),
	/**
	 * Estado en proceso de digitalización, fases asíncronas.
	 */
	IN_PROCESS("3");

	/**
	 * Código de resultado del estado del documento.
	 */
	private String result;

	/**
	 * Constructor con argumentos.
	 * 
	 * @param aResult.
	 */
	StatusDocBox(String aResult) {
		this.result = aResult;
	}

	/**
	 * Obtiene el atributo result.
	 * 
	 * @return el atributo result.
	 */
	public String getResult() {
		return result;
	}
}
