/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.controller.vo.resultStatusContentVO.java.</p>
 * 
 * <b>Descripción:</b><p> Clase que recoge la información de la vista de estados de un documento digitalizado.</p>
 * 
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * 
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox.controller.vo;

import java.util.List;

import es.ricoh.webscan.docbox.service.ResultStatusContent;

/**
 * Clase que recoge la información de la vista de estados de un documento
 * digitalizado.
 * <p>
 * Clase ResultStatusContentVO
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ResultStatusContentVO {

	/**
	 * Identificador del la operación de auditoría.
	 */
	private Long auditOperationId;

	/**
	 * Listado de estados de un documento digitalizado
	 */
	private List<ResultStatusContent> statusContents;

	/**
	 * Booleano indicando si un estado es recuperable o no
	 */
	private Boolean actionRecovery;

	/**
	 * Indentificador del documento digitalizado
	 */
	private Long documentId;

	/**
	 * Identificador del proceso de digitalización
	 */
	private Long batchId;

	/**
	 * Gets the value of the attribute {@link #auditOperationId}.
	 * 
	 * @return the value of the attribute {@link #auditOperationId}.
	 */
	public Long getAuditOperationId() {
		return auditOperationId;
	}

	/**
	 * Sets the value of the attribute {@link #auditOperationId}.
	 * 
	 * @param auditOperationId
	 *            The value for the attribute {@link #auditOperationId}.
	 */
	public void setAuditOperationId(Long auditOperationId) {
		this.auditOperationId = auditOperationId;
	}

	/**
	 * Gets the value of the attribute {@link #statusContents}.
	 * 
	 * @return the value of the attribute {@link #statusContents}.
	 */
	public List<ResultStatusContent> getStatusContents() {
		return statusContents;
	}

	/**
	 * Sets the value of the attribute {@link #statusContents}.
	 * 
	 * @param statusContents
	 *            The value for the attribute {@link #statusContents}.
	 */
	public void setStatusContents(List<ResultStatusContent> statusContents) {
		this.statusContents = statusContents;
	}

	/**
	 * Gets the value of the attribute {@link #actionRecovery}.
	 * 
	 * @return the value of the attribute {@link #actionRecovery}.
	 */
	public Boolean getActionRecovery() {
		return actionRecovery;
	}

	/**
	 * Sets the value of the attribute {@link #actionRecovery}.
	 * 
	 * @param actionRecovery
	 *            The value for the attribute {@link #actionRecovery}.
	 */
	public void setActionRecovery(Boolean actionRecovery) {
		this.actionRecovery = actionRecovery;
	}

	/**
	 * Gets the value of the attribute {@link #documentId}.
	 * 
	 * @return the value of the attribute {@link #documentId}.
	 */
	public Long getDocumentId() {
		return documentId;
	}

	/**
	 * Sets the value of the attribute {@link #documentId}.
	 * 
	 * @param documentId
	 *            The value for the attribute {@link #documentId}.
	 */
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	/**
	 * Sets the value of the attribute {@link #documentId}.
	 * 
	 * @param documentId
	 *            The value for the attribute {@link #documentId}.
	 */
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}	/**
	 * Gets the value of the attribute {@link #documentId}.
	 * 
	 * @return the value of the attribute {@link #documentId}.
	 */
	public Long getBatchId() {
		return batchId;
	}


}
