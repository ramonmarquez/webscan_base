/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.docbox.service.AsyncScanProcessResumeManager.java.</p>
* <b>Descripción:</b><p> Clase responsable de la reanudación asíncron del proceso de digitalización de
 * un documento digitalizado.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.docbox.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import es.ricoh.webscan.ResumeStage;
import es.ricoh.webscan.base.service.AsyncScanProcessDAOManager;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.domain.OperationStatus;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.plugins.PluginException;
import es.ricoh.webscan.utilities.Utils;

/**
 * Clase responsable de la reanudación asíncrona del proceso de digitalización
 * de un documento digitalizado.
 * <p>
 * Clase AsyncScanProcessResumeManager.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class AsyncScanProcessResumeManager {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AsyncScanProcessResumeManager.class);

	/**
	 * Servicio de apoyo para las fases de ejecución asíncrona del proceso de
	 * digitalización de docmentos que agrupa la ejecución de setencias sobre el
	 * modelo de datos.
	 */
	private AsyncScanProcessDAOManager asyncScanProcessDAOManager;

	/**
	 * Utilidades y acceso a parámetros globales del Sistema.
	 */
	private Utils webscanUtilParams;

	/**
	 * Realiza la reanudación asíncrona del proceso de digitalización de un
	 * documento digitalizado.
	 * 
	 * @param scanDocId
	 *            identificador interno del documento digitalizado.
	 * @param username
	 *            identificador del nombre de usuario.
	 * @throws DAOException
	 */
	@Async("webscanTaskExecutor")
	public void resumeDocScanProcess(Long scanDocId, String username) {
		Boolean mandatoryExceution;
		Integer index;
		List<ScanProfileSpecScanProcStageDTO> renewableScanProfileStages;
		List<ScannedDocLogDTO> renewableScannedDocLog;
		Map<ScanProfileSpecScanProcStageDTO, OperationStatus> resumeScanStageInfo;
		OperationStatus operationStatus = null;
		ScanProfileSpecScanProcStageDTO execStage = null;
		ScannedDocumentDTO doc = null;
		Optional<ScannedDocLogDTO> scannedDocLog;
		ScanProfileSpecScanProcStageDTO scanProfileSpecScanProcStage = null;
		ScanProcessStageDTO stageCapture;
		ScanProfileOrgUnitPluginDTO scanProfileOrgUnitPlugin = null;

		if (LOG.isDebugEnabled()) {
			LOG.debug("[AsyncResumeDocScanProcess] Reanudamos el proceso asincrono: {}", scanDocId);
		}

		try {

			// 1. Se recupera el documento y su información de procesamiento
			doc = asyncScanProcessDAOManager.getDocument(scanDocId);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[AsyncResumeDocScanProcess] Recuperamos el documento: {}", scanDocId);
			}

			// recuperamos la relacion de perfil de digitalización y
			// unidad
			// organizativa
			ScanProfileOrgUnitDTO scanProfileOrgUnit = asyncScanProcessDAOManager.getScanProfileOrgUnitByDoc(scanDocId);

			// recuperamos la especificación del perfil de
			// digitalizacion
			ScanProfileDTO scanProfile = asyncScanProcessDAOManager.getScanProfile(scanProfileOrgUnit.getScanProfile());

			renewableScanProfileStages = asyncScanProcessDAOManager.getRenewableScanProcStagesByScanProfileSpec(scanProfile.getSpecification());
			renewableScannedDocLog = asyncScanProcessDAOManager.getRenewableAndPerformedScannedDocLogsByDocId(scanDocId);
			// 2. Se recupera la fase a partir de la se reanudará el proceso
			resumeScanStageInfo = getResumeScanStageInfo(scanDocId, renewableScanProfileStages, renewableScannedDocLog);
			execStage = resumeScanStageInfo.keySet().iterator().next();

			if (execStage != null) {
				// La fase es reanudable
				if (LOG.isDebugEnabled()) {
					LOG.debug("[AsyncResumeDocScanProcess] Se reanuda el proceso de digitalización del documento {} desde la fase {}.", scanDocId, execStage.getScanProcessStage());
				}
				index = renewableScanProfileStages.indexOf(execStage);

				// recuperamos el map con las etapas y los plugins asociados
				Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> mapStagePlugin = asyncScanProcessDAOManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfile.getSpecification());

				for (int i = index; i < renewableScanProfileStages.size(); i++) {
					// ejecutamos la fase actual
					execStage = renewableScanProfileStages.get(i);
					scanProfileSpecScanProcStage = null;
					scannedDocLog = null;
					scanProfileOrgUnitPlugin = null;
					operationStatus = OperationStatus.PERFORMED;
					final Long execStageId = execStage.getScanProcessStageId();
					/*
					*	3. Por cada fase pendiente
					*		3.1 obtenemos plugin de fase
					*		3.2 Ejecutamos el plugin (actulizamos trazas de auditorias)
					*		3.3 Registramos/Actualizamos la traza de ejecución 
					*/
					scannedDocLog = renewableScannedDocLog.stream().filter(sdl -> sdl.getScanProcessStageId().equals(execStageId)).findFirst();

					if ((scannedDocLog.isPresent() && OperationStatus.FAILED.equals(scannedDocLog.get().getStatus())) || !scannedDocLog.isPresent()) {
						if (scannedDocLog.isPresent()) {
							operationStatus = scannedDocLog.get().getStatus();
						}

						if (LOG.isDebugEnabled()) {
							LOG.debug("[AsyncResumeDocScanProcess] Se procede a ejecutar la fase {} del documento {}.", execStage.getScanProcessStage(), scanDocId);
						}

						LOG.debug("[AsyncResumeDocScanProcess] Recuperamos la informacion de base de datos que necesitamos");
						/*verificamos que tenga activo el plugin en la fase actual*/
						// recuperamos el objecto de la fase de escaneo
						stageCapture = asyncScanProcessDAOManager.getScanProcStageByName(execStage.getScanProcessStage());

						/*obtenemos la especificación del perfil por etapa para comprobar si es obligatorio*/
						scanProfileSpecScanProcStage = asyncScanProcessDAOManager.getScanProfileSpecScanProcStageByScanProfileSpecIdAndScanProcStageId(mapStagePlugin, scanProfile.getSpecification(), execStage.getScanProcessStage());
						mandatoryExceution = scanProfileSpecScanProcStage != null && scanProfileSpecScanProcStage.getMandatory();
						// verificamos que la unidad organizativa y el perfil
						// que nos informa este configurado y activo

						try {
							/*verificacion del plugin activo en fase actual*/

							scanProfileOrgUnitPlugin = asyncScanProcessDAOManager.getScanProfileOrgUnitPluginByScanProfileOrgUnitAndStage(scanProfileOrgUnit.getScanProfile(), scanProfileOrgUnit.getOrgUnit(), stageCapture.getId());

							if (Boolean.FALSE.equals(scanProfileOrgUnitPlugin.getActive()) && mandatoryExceution) {
								LOG.debug("El perfil y la unidad organizativa no tiene activo la fase actual");
								throw new PluginException(PluginException.CODE_609, "El perfil y la unidad organizativa con id " + scanProfileOrgUnit.getId() + " no tiene activo el plugin para la fase " + execStage.getScanProcessStage());
							}

							/*fin verificacion del plugin activo en fase actual*/

							// obtenemos el plugin spec activo
							PluginSpecificationDTO plugSpec = asyncScanProcessDAOManager.getPluginSpecByScanProfileOrgUnitAndStage(scanProfileOrgUnit.getId(), execStage.getScanProcessStage());
							LOG.debug("[AsyncResumeDocScanProcess] Obtenemos el  plugin: {}", plugSpec.getName());
							// obtenemos el plugin
							String namePlugin = asyncScanProcessDAOManager.getPluginCall(execStage.getScanProcessStage(), plugSpec.getName());

							if (scanProfileSpecScanProcStage.getMandatory() && (namePlugin == null || namePlugin.trim().isEmpty())) {
								throw new PluginException(PluginException.CODE_607, "No fue posible obtener del modelo de datos el plugin que ejecuta la fase " + execStage.getScanProcessStage() + " para el perfil " + scanProfileOrgUnit.getId());
							}

							operationStatus = asyncScanProcessDAOManager.performResumeStagePlugin(doc, scanProfileSpecScanProcStage, scannedDocLog.get(), plugSpec, namePlugin, username);

							// pasamos a la siguiente fase
							if (LOG.isDebugEnabled()) {
								LOG.debug("[AsyncResumeDocScanProcess] Se procede a recuperar la siguiente fase reanudable para el documento {}.", scanDocId);
							}
						} catch (DAOException e) {
							if (!e.getCode().equals(DAOException.CODE_901)) {
								throw e;
							}
							// no tenemos plugin para la fase actual
							if (scanProfileOrgUnitPlugin == null) {
								if (mandatoryExceution) {
									LOG.error("No existe plugin configurado para la fase actual");
									throw new PluginException(PluginException.CODE_608, "No existe plugin configurado para la fase " + execStage.getScanProcessStage() + " para el perfil " + scanProfileOrgUnit.getId());
								} else {
									if (LOG.isDebugEnabled()) {
										LOG.debug("[AsynScanProcessResumeManager] Etapa no obligatoria, no ha sido configurada: {}", execStage.getScanProcessStage());
									}
								}
							}
						} catch (PluginException e) {
							throw e;
						}

					}
				}
			}
		} catch (PluginException e) {
			try {
				if (operationStatus != null) {
					// está en error, se registran logs de ejecución y auditoria
					asyncScanProcessDAOManager.performResumeDocErrorLogs(doc, scanProfileSpecScanProcStage, operationStatus, e, execStage.getScanProcessStage(), username);
				}
				LOG.error("[AsyncResumeDocScanProcess] Error general al ejecutar plugin del documento: {}", doc.getId());
			} catch (DAOException ex) {
				String excMsg = "[AsyncResumeDocScanProcess] Error ejecutanto plugin: " + ex.getMessage();
				LOG.error(excMsg, ex);
			}
		} catch (DAOException e) {
			// al ser un proceso asincrono sólo queda información en el log
			LOG.error("[AsyncResumeDocScanProcess] Finalizamos la ejecución del proceso ejecución asincrono por error base de datos. Error:", e);
		} catch (Exception e) {
			LOG.error("[AsyncResumeDocScanProcess] Error no esperado. Error:", e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[AsyncResumeDocScanProcess] Fin Reanudamos el proceso asincrono del documento {}", scanDocId);
		}
	}

	/**
	 * Recupera la fase del proceso de digitalización de un documento a partir
	 * de la cual es posible su procesamiento.
	 * 
	 * @param scanDocId
	 *            Identificador del documento digitalizado.
	 * @param renewableScanProfileStages
	 *            fases reanudables del proceso de digitalización del documento.
	 * @param renewableScannedDocLog
	 *            trazas de ejecución de fases reanudables del proceso de
	 *            digitalización del documento.
	 * @return Si el proceso puede ser reanudado, retorna una relación de pares
	 *         clave/valor con una única entrada, identificada mediante la fase
	 *         a partir de la cual será reiniciado el proceso, y cuyo valor se
	 *         corresponde con el estado de dicha fase. En caso contrario,
	 *         devuelve una relación de pares clave/valor vacía.
	 * @throws DAOException
	 *             Si se produce algún error al efectuar consultas sobre el
	 *             modelo de datos del sistema.
	 */
	private Map<ScanProfileSpecScanProcStageDTO, OperationStatus> getResumeScanStageInfo(Long scanDocId, final List<ScanProfileSpecScanProcStageDTO> renewableScanProfileStages, final List<ScannedDocLogDTO> renewableScannedDocLog) throws DAOException {
		Integer stageOrder = 0;
		Map<ScanProfileSpecScanProcStageDTO, OperationStatus> res = new HashMap<ScanProfileSpecScanProcStageDTO, OperationStatus>();
		OperationStatus operationStatus = null;
		final Optional<ScanProfileSpecScanProcStageDTO> renewableStage;
		final Optional<ScanProfileSpecScanProcStageDTO> failedRenewableStage;
		ScanProfileSpecScanProcStageDTO spsps;
		Optional<ScannedDocLogDTO> scannedDocLog;
		String scanProcessStageName;

		if (renewableScannedDocLog.isEmpty()) {
			// No ha sido ejecutada ninguna fase reanudable, se retorna la
			// primera
			res.put(renewableScanProfileStages.get(0), OperationStatus.PERFORMED);
		} else {
			// Se comprueba si existe alguna fase reanudable finalizada con
			// errores
			failedRenewableStage = renewableScanProfileStages.stream().sorted((stage1, stage2) -> stage1.getOrder().compareTo(stage2.getOrder())).filter(stage -> findScannedDocLogStage(stage.getScanProcessStageId(), OperationStatus.FAILED, renewableScannedDocLog)).findFirst();

			if (failedRenewableStage.isPresent()) {
				// Se ha encontrado una traza errónea para una fase reanudable
				res.put(failedRenewableStage.get(), OperationStatus.FAILED);
			} else {
				// Se busca la primera fase reanudable no ejecutada
				renewableStage = renewableScanProfileStages.stream().sorted((stage1, stage2) -> stage1.getOrder().compareTo(stage2.getOrder())).filter(stage -> !findScannedDocLogStage(stage.getScanProcessStageId(), null, renewableScannedDocLog)).findFirst();

				if (renewableStage.isPresent()) {
					// Se ha encontrado una fase que no posee traza de ejecución
					// para el documento
					res.put(renewableStage.get(), OperationStatus.PERFORMED);
				} else {
					if (LOG.isDebugEnabled()) {
						LOG.debug("[AsyncResumeDocScanProcess] El documento con id {} no puede ser reanudado, ya que no posee fases que puedan ser reanudadas.", scanDocId);
					}
				}
			}
		}

		return res;
	}

	/**
	 * Busca una traza de ejecución con un estado determinado, de un proceso de
	 * digitalización de un documento, para una fase determinada.
	 * 
	 * @param scanProcessStageId
	 *            Identificador de fase de procesos de digitalización.
	 * @param operationStatus
	 *            Estado de la traza a recuperar. Es opcional.
	 * @param renewableScannedDocLog
	 *            Trazas de ejecución de fases reanudables ejecutadas para
	 * @return
	 */
	private Boolean findScannedDocLogStage(Long scanProcessStageId, OperationStatus operationStatus, List<ScannedDocLogDTO> renewableScannedDocLog) {
		Optional<ScannedDocLogDTO> renewableStage;

		renewableStage = renewableScannedDocLog.stream().filter(scannedDocLog -> scannedDocLog.getScanProcessStageId().equals(scanProcessStageId) && (operationStatus != null ? scannedDocLog.getStatus().equals(operationStatus) : Boolean.TRUE)).findFirst();

		return renewableStage.isPresent();
	}

	/**
	 * Establece el servicio de apoyo para las fases de ejecución asíncrona del
	 * proceso de digitalización de docmentos que agrupa la ejecución de
	 * setencias sobre el modelo de datos.
	 * 
	 * @param anAsyncScanProcessDAOManager
	 *            servicio de apoyo para las fases de ejecución asíncrona del
	 *            proceso de digitalización de docmentos que agrupa la ejecución
	 *            de setencias sobre el modelo de datos.
	 */
	public void setAsyncScanProcessDAOManager(AsyncScanProcessDAOManager anAsyncScanProcessDAOManager) {
		this.asyncScanProcessDAOManager = anAsyncScanProcessDAOManager;
	}

	/**
	 * Establece la clase de utilidades para parametros sobre el modelo de datos
	 * del Sistema.
	 * 
	 * @param awebscanUtilParams
	 *            utilidades para parametros sobre el modelo de datos del
	 *            Sistema.
	 */

	public void setWebscanUtilParams(Utils awebscanUtilParams) {
		this.webscanUtilParams = awebscanUtilParams;
	}
}
