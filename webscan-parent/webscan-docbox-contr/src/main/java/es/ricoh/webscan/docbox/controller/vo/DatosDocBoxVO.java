/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.docbox.controller.vo;

import java.io.Serializable;

/**
 * Entidad que agrupa la información de los datos de los documentos del buzon de
 * escaneados.
 * <p>
 * Class DatosDocBoxVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class DatosDocBoxVO implements Serializable {

	/**
	 * Attribute that represents el serial de la clase .
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * identificador del metadato.
	 */
	private String idMeta;
	/**
	 * valor del metadato.
	 */
	private String valor;

	/**
	 * Constructor method for the class DatosDocBoxVO.java.
	 * 
	 * @param aidMeta
	 *            identificador del metadato.
	 * @param avalor
	 *            valor del metadato.
	 */
	public DatosDocBoxVO(String aidMeta, String avalor) {
		super();
		this.idMeta = aidMeta;
		this.valor = avalor;
	}

	/**
	 * Gets the value of the attribute {@link #idMeta}.
	 * 
	 * @return the value of the attribute {@link #idMeta}.
	 */
	public String getIdMeta() {
		return idMeta;
	}

	/**
	 * Sets the value of the attribute {@link #idMeta}.
	 * 
	 * @param aidMeta
	 *            The value for the attribute {@link #idMeta}.
	 */
	public void setIdMeta(String aidMeta) {
		this.idMeta = aidMeta;
	}

	/**
	 * Gets the value of the attribute {@link #valor}.
	 * 
	 * @return the value of the attribute {@link #valor}.
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * Sets the value of the attribute {@link #valor}.
	 * 
	 * @param avalor
	 *            The value for the attribute {@link #valor}.
	 */
	public void setValor(String avalor) {
		this.valor = avalor;
	}

}
