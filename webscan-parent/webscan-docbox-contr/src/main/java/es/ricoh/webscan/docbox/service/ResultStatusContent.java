/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.docbox.service.ResultStatusContent.java.</p>
* <b>Descripción:</b><p> Objeto visualización de los estados del documento digitalizado.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.docbox.service;

import java.util.Date;

/**
 * Objeto visualización de los estados del documento digitalizado.
 * <p>
 * Clase ResultStatusContent.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ResultStatusContent {

	/**
	 * Orden de presentación
	 */
	private Integer order;

	/**
	 * Fase de la digitalización
	 */
	private String stage;

	/**
	 * Nombre de la fase.
	 */
	private String eventName;

	/**
	 * Descrición de la fase.
	 */
	private String eventData;

	/**
	 * Resultado de la fase.
	 */
	private String eventResult;

	/**
	 * Fecha del evento de auditoria
	 */
	private Date eventDate;

	/**
	 * Booleano indicando si la fase contiene errores o no
	 */
	private Boolean check;

	/**
	 * Gets the value of the attribute {@link #order}.
	 * 
	 * @return the value of the attribute {@link #order}.
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * Sets the value of the attribute {@link #order}.
	 * 
	 * @param order
	 *            The value for the attribute {@link #order}.
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}

	/**
	 * Gets the value of the attribute {@link #stage}.
	 * 
	 * @return the value of the attribute {@link #stage}.
	 */
	public String getStage() {
		return stage;
	}

	/**
	 * Sets the value of the attribute {@link #stage}.
	 * 
	 * @param stage
	 *            The value for the attribute {@link #stage}.
	 */
	public void setStage(String stage) {
		this.stage = stage;
	}

	/**
	 * Gets the value of the attribute {@link #eventName}.
	 * 
	 * @return the value of the attribute {@link #eventName}.
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * Sets the value of the attribute {@link #eventName}.
	 * 
	 * @param eventName
	 *            The value for the attribute {@link #eventName}.
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * Gets the value of the attribute {@link #eventData}.
	 * 
	 * @return the value of the attribute {@link #eventData}.
	 */
	public String getEventData() {
		return eventData;
	}

	/**
	 * Sets the value of the attribute {@link #eventData}.
	 * 
	 * @param eventData
	 *            The value for the attribute {@link #eventData}.
	 */
	public void setEventData(String eventData) {
		this.eventData = eventData;
	}

	/**
	 * Gets the value of the attribute {@link #eventResult}.
	 * 
	 * @return the value of the attribute {@link #eventResult}.
	 */
	public String getEventResult() {
		return eventResult;
	}

	/**
	 * Sets the value of the attribute {@link #eventResult}.
	 * 
	 * @param eventResult
	 *            The value for the attribute {@link #eventResult}.
	 */
	public void setEventResult(String eventResult) {
		this.eventResult = eventResult;
	}

	/**
	 * Gets the value of the attribute {@link #check}.
	 * 
	 * @return the value of the attribute {@link #check}.
	 */
	public Boolean getCheck() {
		return check;
	}

	/**
	 * Sets the value of the attribute {@link #check}.
	 * 
	 * @param check
	 *            The value for the attribute {@link #check}.
	 */
	public void setCheck(Boolean check) {
		this.check = check;
	}

	/**
	 * Gets the value of the attribute {@link #eventDate}.
	 * 
	 * @return the value of the attribute {@link #eventDate}.
	 */
	public Date getEventDate() {
		return eventDate;
	}

	/**
	 * Sets the value of the attribute {@link #eventDate}.
	 * 
	 * @param eventDate
	 *            The value for the attribute {@link #eventDate}.
	 */
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	/**
	 * Constructor method for the class ResultStatusContent.java.
	 */
	public ResultStatusContent() {
		this.stage = "";
		this.eventName = "";
		this.eventData = "";
		this.eventResult = "";
		this.check = false;
	}

}
