/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.model.WebscanDocBoxException.java.</p>
 * <b>Descripción:</b><p> Constantes de la utilidad bandeja de documentos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox;

import es.ricoh.webscan.WebscanException;

/**
 * Excepción que encapsula los errores de negocio de la bandeja de documentos.
 * <p>
 * Clase SpacersException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class WebscanDocBoxException extends WebscanException {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Error al validar el número de documentos de un lote de documentos.
	 */
	public static final String CODE_019 = "COD_019";
	
	/**
	 * Error en acceso al modelo de datos.
	 */
	public static final String CODE_025 = "COD_025";

	/**
	 * Lote de documentos con estado no válido para la operación solicitada.
	 */
	public static final String CODE_026 = "COD_026";

	/**
	 * No existe la entidad, carátula o reserva, que identifica un lote de
	 * documentos.
	 */
	public static final String CODE_027 = "COD_027";

	/**
	 * Error en la configuración o ejecución del plugin de depósito.
	 */
	public static final String CODE_028 = "COD_028";

	/**
	 * Error en depósito de documentos por no estar las fases requeridas previas
	 * del proceso ejecutadas correctamente.
	 */
	public static final String CODE_029 = "COD_029";

	/**
	 * Error en depósito de documentos por no estar informados todos los
	 * metadatos requeridos.
	 */
	public static final String CODE_030 = "COD_030";
	
	/**
	 * Error el perfil del usuario no es el mismo que el esperado de los
	 * documentos digitalizados.
	 */
	public static final String CODE_037 = "COD_037";

	/**
	 * Error al almacenar todos los documentos digitalizados.
	 */
	public static final String CODE_038 = "COD_038";
	
	/**
	 * Error No es posible depositar el documento, no ha sido validado por el
	 * usuario
	 */
	public static final String CODE_046 = "CODE_046";

	/**
	 * Error global en depósito de documentos.
	 */
	public static final String CODE_400 = "COD_400";

	/**
	 * Error en depósito de documentos por no estar las fases previas del
	 * proceso ejecutadas correctamente.
	 */
	public static final String CODE_401 = "COD_401";

	/**
	 * Error en depósito de documentos por no estar informados todos los
	 * metadatos requeridos.
	 */
	public static final String CODE_402 = "COD_402";

	/**
	 * Error en depósito de documentos, fallo al ejecutar el plugin de depósito.
	 */
	public static final String CODE_403 = "COD_403";

	/**
	 * Código del error.
	 */
	protected String code = CODE_999;

	/**
	 * Constructor sin argumentos.
	 */
	public WebscanDocBoxException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public WebscanDocBoxException(String aCode, String aMessage) {
		super(aMessage);
		this.code = aCode;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public WebscanDocBoxException(String aMessage) {
		super(aMessage);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public WebscanDocBoxException(Throwable aCause) {
		super(aCause);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public WebscanDocBoxException(final String aMessage, Throwable aCause) {
		super(aMessage, aCause);
		this.code = CODE_999;
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public WebscanDocBoxException(final String aCode, final String aMessage, Throwable aCause) {
		super(aMessage, aCause);
		this.code = CODE_999;
	}

	/**
	 * Obtiene el código de error de la excepción.
	 *
	 * @return código de error de la excepción.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Establece el código de error de la excepción.
	 *
	 * @param aCode
	 *            código de error de la excepción.
	 */
	public void setCode(final String aCode) {
		this.code = aCode;
	}

}
