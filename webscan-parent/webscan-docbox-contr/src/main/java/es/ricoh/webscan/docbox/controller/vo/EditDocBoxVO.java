/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.controller.vo.EditDocBoxVO.java.</p>
 * <b>Descripción:</b><p> .</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox.controller.vo;

import java.io.Serializable;

/**
 * <p>
 * Class .
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

public class EditDocBoxVO implements Serializable {

	/**
	 * Atributo que representa el serial de la clase.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * identificador del documento del buzon.
	 */
	private Long idDocBox;
	/**
	 * listado de datos asociados al documento del buzon.
	 */
	private ListDatosDocBoxVO datos;

	/**
	 * Gets the value of the attribute {@link #idDocBox}.
	 * 
	 * @return the value of the attribute {@link #idDocBox}.
	 */
	public Long getIdDocBox() {
		return idDocBox;
	}

	/**
	 * Sets the value of the attribute {@link #idDocBox}.
	 * 
	 * @param aidDocBox
	 *            The value for the attribute {@link #idDocBox}.
	 */
	public void setIdDocBox(Long aidDocBox) {
		this.idDocBox = aidDocBox;
	}

	/**
	 * Gets the value of the attribute {@link #datos}.
	 * 
	 * @return the value of the attribute {@link #datos}.
	 */
	public ListDatosDocBoxVO getDatos() {
		return datos;
	}

	/**
	 * Sets the value of the attribute {@link #datos}.
	 * 
	 * @param adatos
	 *            The value for the attribute {@link #datos}.
	 */
	public void setDatos(ListDatosDocBoxVO adatos) {
		this.datos = adatos;
	}

}
