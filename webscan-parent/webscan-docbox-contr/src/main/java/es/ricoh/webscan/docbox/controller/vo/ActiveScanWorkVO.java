/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.gva.webscangv.fastscandoc.contr.controller.vo.OrgUnitScanProfileVO.java.</p>
* <b>Descripción:</b><p> Objeto de la capa de presentación que agrupa la información relativa al
* trabajo de digitalización de un usuario.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
* Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.docbox.controller.vo;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.docbox.service.BatchType;
//import es.gva.webscangv.scanworkbooking.model.domain.BookingStatus;

/**
 * Objeto de la capa de presentación que agrupa la información relativa al
 * trabajo de digitalización de un usuario.
 * <p>
 * Clase ActiveScanWorkVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class ActiveScanWorkVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador del trabajo de digitalización. Será null cuando se trate de
	 * un trabajo de digitalización iniciado de forma autónoma sin carátula, ni
	 * reserva.
	 */
	private Long scanWorkId;

	/**
	 * Identificador normalizado del trabajo de digitalización. Será null cuando
	 * se trate de un trabajo de digitalización iniciado de forma autónoma sin
	 * carátula, ni reserva.
	 */
	private String scanWorkExternalId;

	/**
	 * Estado del trabajo de digitalización.
	 */
	//private BookingStatus scanWorkStatus;

	/**
	 * Tipo de trabajo de digitalización.
	 */
	private BatchType scanWorkType = BatchType.DEFAULT;

	/**
	 * Perfil de digitalización.
	 */
	private Long scanProfileId;

	/**
	 * Definición de documentos del perfil de digitalización.
	 */
	private Long docSpecId;

	/**
	 * Fecha en la que es solicitado el trabajo de digitalización.
	 */
	private Date requestDate = new Date();

	/**
	 * Fecha en la que es iniciado el trabajo de digitalización.
	 */
	private Date startDate;

	/**
	 * Identificador de la unidad orgánica a la que serán asociados los
	 * documentos del trabajo de digitalización.
	 */
	private String department;

	/**
	 * Constructor sin argumentos.
	 */
	public ActiveScanWorkVO() {
		super();
	}

	/**
	 * Gets the value of the attribute {@link #scanWorkId}.
	 * 
	 * @return the value of the attribute {@link #scanWorkId}.
	 */
	public Long getScanWorkId() {
		return scanWorkId;
	}

	/**
	 * Sets the value of the attribute {@link #scanWorkId}.
	 * 
	 * @param aScanWorkId
	 *            The value for the attribute {@link #scanWorkId}.
	 */
	public void setScanWorkId(Long aScanWorkId) {
		this.scanWorkId = aScanWorkId;
	}

	/**
	 * Gets the value of the attribute {@link #scanWorkExternalId}.
	 * 
	 * @return the value of the attribute {@link #scanWorkExternalId}.
	 */
	public String getScanWorkExternalId() {
		return scanWorkExternalId;
	}

	/**
	 * Sets the value of the attribute {@link #scanWorkExternalId}.
	 * 
	 * @param aScanWorkExternalId
	 *            The value for the attribute {@link #scanWorkExternalId}.
	 */
	public void setScanWorkExternalId(String aScanWorkExternalId) {
		this.scanWorkExternalId = aScanWorkExternalId;
	}

	/**
	 * Gets the value of the attribute {@link #scanWorkStatus}.
	 * 
	 * @return the value of the attribute {@link #scanWorkStatus}.
	 */
	//public BookingStatus getScanWorkStatus() {
	//	return scanWorkStatus;
	//}

	/**
	 * Sets the value of the attribute {@link #scanWorkStatus}.
	 * 
	 * @param aScanWorkStatus
	 *            The value for the attribute {@link #scanWorkStatus}.
	 */
	//public void setScanWorkStatus(BookingStatus aScanWorkStatus) {
	//	this.scanWorkStatus = aScanWorkStatus;
	//}

	/**
	 * Gets the value of the attribute {@link #scanWorkType}.
	 * 
	 * @return the value of the attribute {@link #scanWorkType}.
	 */
	public BatchType getScanWorkType() {
		return scanWorkType;
	}

	/**
	 * Sets the value of the attribute {@link #scanWorkType}.
	 * 
	 * @param aScanWorkType
	 *            The value for the attribute {@link #scanWorkType}.
	 */
	public void setScanWorkType(BatchType aScanWorkType) {
		this.scanWorkType = aScanWorkType;
	}

	/**
	 * Gets the value of the attribute {@link #scanProfileOrgUnitId}.
	 * 
	 * @return the value of the attribute {@link #scanProfileOrgUnitId}.
	 */
	public Long getScanProfileId() {
		return scanProfileId;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfileOrgUnitId}.
	 * 
	 * @param aScanProfileId
	 *            The value for the attribute {@link #scanProfileOrgUnitId}.
	 */
	public void setScanProfileId(Long aScanProfileId) {
		this.scanProfileId = aScanProfileId;
	}

	/**
	 * Gets the value of the attribute {@link #docSpecId}.
	 * 
	 * @return the value of the attribute {@link #docSpecId}.
	 */
	public Long getDocSpecId() {
		return docSpecId;
	}

	/**
	 * Sets the value of the attribute {@link #docSpecId}.
	 * 
	 * @param aDocSpecId
	 *            The value for the attribute {@link #docSpecId}.
	 */
	public void setDocSpecId(Long aDocSpecId) {
		this.docSpecId = aDocSpecId;
	}

	/**
	 * Gets the value of the attribute {@link #requestDate}.
	 * 
	 * @return the value of the attribute {@link #requestDate}.
	 */
	public Date getRequestDate() {
		return requestDate;
	}

	/**
	 * Sets the value of the attribute {@link #requestDate}.
	 * 
	 * @param aRequestDate
	 *            The value for the attribute {@link #requestDate}.
	 */
	public void setRequestDate(Date aRequestDate) {
		this.requestDate = aRequestDate;
	}

	/**
	 * Gets the value of the attribute {@link #startDate}.
	 * 
	 * @return the value of the attribute {@link #startDate}.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the value of the attribute {@link #startDate}.
	 * 
	 * @param aStartDate
	 *            The value for the attribute {@link #startDate}.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}

	/**
	 * Obtiene el identificador de la unidad orgánica a la que serán asociados
	 * los documentos del trabajo de digitalización.
	 * 
	 * @return el identificador de la unidad orgánica a la que serán asociados
	 *         los documentos del trabajo de digitalización.
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Establece un nuevo valor para el identificador de la unidad orgánica a la
	 * que serán asociados los documentos del trabajo de digitalización.
	 * 
	 * @param aDepartment
	 *            nuevo valor para el identificador de la unidad orgánica a la
	 *            que serán asociados los documentos del trabajo de
	 *            digitalización.
	 */
	public void setDepartment(String aDepartment) {
		this.department = aDepartment;
	}

}
