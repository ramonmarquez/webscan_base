/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.model.WebscanDocBoxConstant.java.</p>
 * <b>Descripción:</b><p> Constantes de la utilidad bandeja de documentos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox;

/**
 * Constantes de la utilidad bandeja de documentos.
 * <p>
 * Class WebscanDocBoxConstants.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public interface WebscanDocBoxConstants {

	/**
	 * Constante para la formaón del mensaje visual de resultado de deposito.
	 */

	int MESSAGE_ELEMENT_COUNT = 3;
	/**
	 * Constante que identifica la posición del id del documento dentro del
	 * mensaje.
	 */
	int MESSAGE_POS_DOC_ID = 0;
	/**
	 * Constante que identifica la posición del stage del documento dentro del
	 * mensaje.
	 */
	int MESSAGE_POS_DOC_STAGE = 1;
	/**
	 * Constante que identifica la posición del status del documento dentro del
	 * mensaje.
	 */
	int MESSAGE_POS_DOC_STATUS = 2;
	/**
	 * Constante que identifica la posición del scanprofile del documento dentro
	 * del mensaje.
	 */
	int MESSAGE_POS_DOC_SCANPROFILE = 1;
	/**
	 * Constante que identifica la posición del orgUnit del documento dentro del
	 * mensaje.
	 */
	int MESSAGE_POS_DOC_ORG_UNIT = 2;
	/**
	 * Constante que identifica la posición del docSpec del documento dentro del
	 * mensaje.
	 */
	int MESSAGE_POS_DOC_DOCSPEC = 3;
	/**
	 * Constante que identifica la posición del deposit del documento dentro del
	 * mensaje.
	 */
	int MESSAGE_POS_DOC_DEPOSIT = 1;

	/**
	 * Nombre del parámetro que identifica el mensaje de error por estado
	 * incorrecto de un documento al intentar realizar su depósito.
	 */
	String MESSAGE_RESULT_DEPOSIT_ERROR_STATE = "message.result.deposit.error.state";

	/**
	 * Nombre del parámetro que identifica el mensaje informativo tras realizar
	 * el depósito de un documento.
	 */
	String MESSAGE_RESULT_DEPOSIT_OK = "message.result.deposit.ok";

	/**
	 * Nombre del parámetro que identifica el mensaje de error genérico al
	 * intentar realizar el depósito de un documento.
	 */
	String MESSAGE_RESULT_DEPOSIT_ERROR = "message.result.deposit.error";

	/**
	 * Nombre del parámetro que identifica el mensaje de error por no estar los
	 * metadatos requeridos de un documento al intentar realizar su depósito.
	 */
	String MESSAGE_RESULT_DEPOSIT_ERROR_REQUEIRED_METADATA = "message.result.deposit.error.requiredMetadata";

	/**
	 * Nombre del parámetro que identifica el mensaje de error por estado
	 * incorrecto del tipo de documento al intentar realizar su depósito.
	 */
	String MESSAGE_RESULT_DEPOSIT_ERROR_DISABLED_TYPE_DOC = "message.result.deposit.error.disabledTypeDoc";
	
	/**
	 * respuesta documento no encontrado.
	 */
	String DOCBOX_BOOKING_DOC_NOT_FOUND = "docBox.error.bookingDocNotFound";

	/**
	 * respuesta documento no encontrado.
	 */
	String FAST_SCANDOCS_BOOKING_DOC_NOT_FOUND = "fastscandoc.error.bookingDocNotFound";
	
	/**
	 * respuesta de database error en scanDocs.
	 */
	String DOCBOX_ERROR_DATA_BASE = "docBox.error.database";


}
