/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.controller.DocBoxController.java.</p>
 * <b>Descripción:</b><p> Controlador de la utilidad bandeja de documentos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.docbox.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.UserOrgUnitScanProfiles;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.docbox.WebscanDocBoxConstants;
import es.ricoh.webscan.docbox.WebscanDocBoxException;
import es.ricoh.webscan.docbox.controller.vo.BatchVO;
import es.ricoh.webscan.docbox.controller.vo.DatosDocBoxVO;
import es.ricoh.webscan.docbox.controller.vo.DocBoxVO;
import es.ricoh.webscan.docbox.controller.vo.DocMetadataVO;
import es.ricoh.webscan.docbox.controller.vo.ListDatosDocBoxVO;
import es.ricoh.webscan.docbox.controller.vo.ListDocBoxVO;
import es.ricoh.webscan.docbox.controller.vo.ResultStatusContentVO;
import es.ricoh.webscan.docbox.service.DocBoxActionResult;
import es.ricoh.webscan.docbox.service.DocBoxService;
import es.ricoh.webscan.docbox.service.ResultStatusContent;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;

/**
 * Controlador de la utilidad bandeja de documentos.
 * <p>
 * Class DocBoxController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Controller
public class DocBoxController {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DocBoxController.class);
	/**
	 * Constante que identifica la vista de los metadatos registrados del
	 * documento escaneado.
	 */
	private static final String VIEW_CONT_META = "fragments/docBox/content_metadata";
	/**
	 * Constante que identifica la vista de los estados registrados del
	 * documento escaneado.
	 */
	private static final String VIEW_CONT_STATUS = "fragments/docBox/content_status";
	/**
	 * Constante que identifica la vista del modal de respuesta del registro de
	 * escaneo.
	 */
	private static final String VIEW_MOD_RESP_REG = "fragments/docBox/modalResponseRegistre";
	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	@Autowired
	private MessageSource messages;

	/**
	 * Objeto que representa el acceso a los servicio de DocBox.
	 */
	@Autowired
	private DocBoxService docBoxService;

	/**
	 * Controlador que carga la bandeja de documentos digitalizados.
	 * 
	 * @param userDptoId
	 *            identificador de unidad orgánica seleccionada por el usuario.
	 *            si no ha seleccionada ninguna, el valor informado es null o
	 *            "-1".
	 * @param model
	 *            Conjunto de parametros de la capa de presentación.
	 * @param request
	 *            Petición http.
	 * @return nombre de plantilla a mostrar.
	 */

	@RequestMapping(value = "/docBox/docBox", method = { RequestMethod.GET })
	public String docBox(String userDptoId, ModelMap model, HttpServletRequest request) {
		LOG.debug("Start docBox");
		String[ ] args = new String[2];
		List<Long> orgUnitIds = new ArrayList<Long>();
		String functionalOrgs = null;
		List<BatchVO> listBatchVO = new ArrayList<BatchVO>();
		List<UserOrgUnitScanProfiles> userOrgUnitScanProfiles;
		try {
			UserInSession userSession = (UserInSession) request.getSession(false).getAttribute(WebscanBaseConstants.USER_SESSION_ATT_NAME);
			LOG.debug("[DOCBOXCONTROLLER] obtenemos la información de session del usuario");

			if (userDptoId != null && !userDptoId.equals("-1")) {
				functionalOrgs = userDptoId;
			} else {
				functionalOrgs = BaseControllerUtils.getUserFunctionalOrgs(userSession);
			}

			userOrgUnitScanProfiles = userSession.getOrgUnitScanProfiles();
			if (userOrgUnitScanProfiles != null && !userOrgUnitScanProfiles.isEmpty()) {
				LOG.debug("[DOCBOXCONTROLLER] obtenemos el listado de documentos escaneados y su estado");
				for (UserOrgUnitScanProfiles orgPro: userOrgUnitScanProfiles) {
					orgUnitIds.add(orgPro.getOrganizationUnit().getId());
				}
				//listBatchVO = docBoxService.getBatchs();
				listBatchVO = docBoxService.getBatchs(orgUnitIds, functionalOrgs);
			}
		} catch (DAOException e) {
			LOG.error("[DOCBOXCONTROLLER] error al recuperar la información de base de datos sobre los documentos escaneados: {}", e.getMessage());
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		}
		// creeamos el mocka de DocBox

		model.addAttribute("listBatchVO", listBatchVO);

		LOG.debug("End docBox");
		return "docBox/docBox";
	}

	/*@RequestMapping(value = "/docBox/docBox", method = { RequestMethod.GET })
	public String docBox(String userDptoId, ModelMap model, HttpServletRequest request) {
		LOG.debug("Start docBox");
		String[ ] args = new String[2];
		List<Long> orgUnitIds = new ArrayList<Long>();
		String functionalOrgs = null;
		List<DocBoxVO> listDocBoxVO = new ArrayList<DocBoxVO>();
		List<UserOrgUnitScanProfiles> userOrgUnitScanProfiles;
		try {
			UserInSession userSession = (UserInSession) request.getSession(false).getAttribute(WebscanBaseConstants.USER_SESSION_ATT_NAME);
			LOG.debug("[DOCBOXCONTROLLER] obtenemos la información de session del usuario");

			if (userDptoId != null && !userDptoId.equals("-1")) {
				functionalOrgs = userDptoId;
			} else {
				functionalOrgs = BaseControllerUtils.getUserFunctionalOrgs(userSession);
			}

			userOrgUnitScanProfiles = userSession.getOrgUnitScanProfiles();
			if (userOrgUnitScanProfiles != null && !userOrgUnitScanProfiles.isEmpty()) {
				LOG.debug("[DOCBOXCONTROLLER] obtenemos el listado de documentos escaneados y su estado");
				for (UserOrgUnitScanProfiles orgPro: userOrgUnitScanProfiles) {
					orgUnitIds.add(orgPro.getOrganizationUnit().getId());
				}
				listDocBoxVO = docBoxService.getScannedDocumentsAndLastLog(orgUnitIds, functionalOrgs);
			}
		} catch (DAOException e) {
			LOG.error("[DOCBOXCONTROLLER] error al recuperar la información de base de datos sobre los documentos escaneados: {}", e.getMessage());
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		}
		// creeamos el mocka de DocBox

		model.addAttribute("listDocBoxVO", listDocBoxVO);

		LOG.debug("End docBox");
		return "docBox/docBox";
	}*/
	/**
	 * Recuperamos los documentos pertenecientes al proceso de digitalización.
	 * 
	 * @param model
	 *            Entidad usada para pasar información entre el controlador y la
	 *            vista.
	 * @param request
	 *            Petición http.
	 * @param batchId
	 *            Identificador del proceso de digitalización
	 * @return plantilla a mostrar
	 */
	@RequestMapping(value = "/docBox/getDocsByBatch", method = { RequestMethod.GET, RequestMethod.POST })
	public String getDocsByBatch(ModelMap model, HttpServletRequest request, Long batchId) {
		LOG.debug("Start docBox");
		String[ ] args = new String[2];
		//String functionalOrgs = null;
		List<DocBoxVO> listDocBoxVO = new ArrayList<DocBoxVO>();
		//List<UserOrgUnitScanProfiles> userOrgUnitScanProfiles;
		try {
			if (batchId != null) {
				listDocBoxVO = docBoxService.getDocsByBatchId(batchId);
			}
		} catch (DAOException e) {
			LOG.error("[DOCBOXCONTROLLER] error al recuperar la información de base de datos sobre los documentos escaneados: {}", e.getMessage());
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		}

		model.addAttribute("listDocBoxVO", listDocBoxVO);

		LOG.debug("End docBox");
		return "fragments/docBox/content_ScannedDoc :: content";
	}

	/**
	 * Recuperamos la información asociada al documento escaneado en la badeja
	 * de documento.
	 * 
	 * @param model
	 *            Entidad usada para pasar información entre el controlador y la
	 *            vista.
	 * @param request
	 *            Petición http.
	 * @param idDocBox
	 *            Identificador del documento escaneado
	 * @return plantilla a mostrar
	 */

	@RequestMapping(value = "/docBox/getViewDocBox", method = { RequestMethod.GET, RequestMethod.POST })
	public String getViewDocBox(ModelMap model, HttpServletRequest request, Long idDocBox) {
		LOG.debug("[WEBSCAN-DOCBOX] Start getViewDocBox");
		String[ ] args = new String[1];
		try {

			DocBoxVO docBox = docBoxService.getInfoScannedDocument(idDocBox);

			model.addAttribute("docBoxSelect", docBox);
			model.addAttribute("editable", false);

		} catch (DAOException e) {
			LOG.error("Error recuperando la info de metadatos del documento:{}", e.getMessage());

			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		}
		LOG.debug("[WEBSCAN-DOCBOX] End getViewDocBox");
		return "fragments/docBox/content_EditDocBox :: content";
	}

	/**
	 * Recuperamos la información asociada al documento escaneado en la badeja
	 * de documento.
	 * 
	 * @param model
	 *            Entidad usada para pasar información entre el controlador y la
	 *            vista.
	 * @param request
	 *            Petición http.
	 * @param idDocBox
	 *            Identificador del documento escaneado
	 * @return plantilla a mostrar
	 */

	@RequestMapping(value = "/docBox/getEditDocBox", method = { RequestMethod.GET, RequestMethod.POST })
	public String getEditDocBox(ModelMap model, HttpServletRequest request, Long idDocBox) {
		String[ ] args = new String[1];
		try {

			DocBoxVO docBox = docBoxService.getInfoScannedDocument(idDocBox);

			model.addAttribute("docBoxSelect", docBox);
			model.addAttribute("editable", true);

		} catch (DAOException e) {
			LOG.error("Error recuperando la info de metadatos del documento:{}", e.getMessage());
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);

		}

		return "fragments/docBox/content_EditDocBox :: content";
	}

	/**
	 * Controlador que descarga el contenido de un documento.
	 * 
	 * @param docId
	 *            Identificador de documento digitalizado.
	 * @param request
	 *            petición web.
	 * @param response
	 *            respuesta web.
	 */

	@RequestMapping(value = "/docBox/getDocContent", method = { RequestMethod.POST })
	public void getDocContent(Long docId, HttpServletRequest request, HttpServletResponse response) {
		ScannedDocumentDTO doc;
		String[ ] args = new String[1];
		try {
			if (docId == null) {
				LOG.error("[WEBSCAN-DOCBOX-CONTR] Error al descargar el contenido de un documento. Identificador de documento no especificado.");
				BaseControllerUtils.addCommonMessage("error.docBox.docIdNotSetted", null, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			} else {
				doc = docBoxService.getScannedDocument(docId);
				args[0] = Long.toString(docId);
				downloadDocContent(doc, messages.getMessage("doc.download.filename", args, request.getLocale()), response);
			}
		} catch (DAOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error recuperando el contenido del documento " + docId + ":" + e.getMessage());
			}

			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		} catch (NoSuchMessageException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error recuperando el contenido del documento " + docId + ":" + e.getMessage());
			}
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error recuperando el contenido del documento " + docId + ":" + e.getMessage());
			}
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		}
	}

	/**
	 * Establece y fuerza la descarga del contenido de un documento en la
	 * respuesta web.
	 * 
	 * @param doc
	 *            documento digitalizado.
	 * @param fileName
	 *            Nombre del archivo.
	 * @param response
	 *            respuesta web.
	 * @throws IOException
	 *             Si se produce algún error en la descarga del contenido del
	 *             documento.
	 */
	private void downloadDocContent(ScannedDocumentDTO doc, String fileName, HttpServletResponse response) throws IOException {

		// Se establecen las cabeceras de la respuesta
		response.setContentType("application/force-download");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + fileName + doc.getMimeType().getExtension() + "\""));

		// Se establecen las cabeceras de la respuesta
		response.setContentType(doc.getMimeType().getType());

		response.setContentLength(doc.getContent().length);
		// Se incorpora el informe a la respuesta
		FileCopyUtils.copy(doc.getContent(), response.getOutputStream());

		response.flushBuffer();
	}

	/**
	 * Controlador para la eliminación de los documentos seleccionados en la
	 * bandeja de documentos.
	 * 
	 * @param model
	 *            Conjunto de parametros de la capa de presentación.
	 * @param request
	 *            Petición web.
	 * @param idsDocBox
	 *            Lista de identificadores documentos seleccionados en la
	 *            bandeja.
	 * @param userDptoId
	 *            identificador de unidad orgánica seleccionada por el usuario.
	 *            si no ha seleccionada ninguna, el valor informado es null o
	 *            "-1".
	 * @return plantilla principal que actualiza los cambios en la vista.
	 */

	@RequestMapping(value = "/docBox/removeDocBox", method = { RequestMethod.POST })
	public String removeDocBox(ModelMap model, HttpServletRequest request, ListDocBoxVO idsDocBox, String userDptoId) {
		List<Long> listIdDocBox = new ArrayList<Long>();
		long t0;
		long tf;
		String[ ] args = new String[1];

		t0 = System.currentTimeMillis();

		try {
			// comprobamos que exista el parametro, lleva al final [] debido a
			// que
			// es una lista
			if (request.getParameter("idsDocBox[]") == null || request.getParameterValues("idsDocBox[]").length == 0) {
				LOG.error("No se remite el parametro de borrado idsDocBox");
				BaseControllerUtils.addCommonMessage("warning.docBox.remove.messageIdsDocBoxNotFound", args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			} else {
				// comprobamos los datos enviados a través del request
				String[ ] pIdsDocBox = request.getParameterValues("idsDocBox[]");
				// preparamos la info para poder ser enviada al service

				for (int a = 0; a < pIdsDocBox.length; a++) {
					listIdDocBox.add(Long.valueOf(pIdsDocBox[a]));
				}

				// obtenemos la info para auditoria
				UserInSession user = BaseControllerUtils.getUserInSession(request);

				docBoxService.removeDocBox(listIdDocBox, user.getUsername());
				// Se muestra el resultado en pantalla
				args[0] = Integer.toString(listIdDocBox.size());
				BaseControllerUtils.addCommonMessage("message.result.removeDocs.ok", args, ViewMessageLevel.INFO, WebScope.REQUEST, request);
			}
		} catch (DAOException e) {
			LOG.error("Error eliminando info del documento:{}", e.getMessage());
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		} catch (Exception e) {
			LOG.error("Error inesperado eliminando info del documento:{}", e.getMessage());
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-DOCBOX-CONTR] Bandeja de documentos - Borrado de {} documentos realizado en {} ms.", listIdDocBox.size(), (tf - t0));
		}

		return docBox(userDptoId, model, request);

	}

	/**
	 * Controlador para la reanudación del proceso de digitalización de los
	 * documentos seleccionados en la bandeja de documentos.
	 * 
	 * @param model
	 *            Conjunto de parametros de la capa de presentación.
	 * @param request
	 *            Petición web.
	 * @param idsDocBox
	 *            Lista de identificadores de documentos seleccionados en la
	 *            bandeja.
	 * @param userDptoId
	 *            identificador de unidad orgánica seleccionada por el usuario.
	 *            si no ha seleccionada ninguna, el valor informado es null o
	 *            "-1".
	 * @return plantilla principal que actualiza los cambios en la vista
	 */

	@RequestMapping(value = "/docBox/restartDocBox", method = { RequestMethod.POST })
	public String restartDocBox(ModelMap model, HttpServletRequest request, ListDocBoxVO idsDocBox, String userDptoId) {
		List<Long> listIdDocBox = new ArrayList<Long>();
		long t0;
		long tf;
		String[ ] args = new String[1];
		String username = "";

		t0 = System.currentTimeMillis();

		// comprobamos que exista el parametro, lleva al final [] debido a que
		// es una lista
		if (request.getParameter("idsDocBox[]") == null || request.getParameterValues("idsDocBox[]").length == 0) {
			LOG.error("No se remite el parametro de reanudación idsDocBox");
			BaseControllerUtils.addCommonMessage("warning.docBox.resume.messageIdsDocBoxNotFound", args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		} else {
			// comprobamos los datos enviados a través del request
			String[ ] pIdsDocBox = request.getParameterValues("idsDocBox[]");
			// preparamos la info para poder ser enviada al service

			for (int a = 0; a < pIdsDocBox.length; a++) {
				listIdDocBox.add(Long.valueOf(pIdsDocBox[a]));
			}

			// obtenemos la info para auditoria
			UserInSession user = BaseControllerUtils.getUserInSession(request);
			username = user.getUsername();

			docBoxService.restartDocBox(listIdDocBox, username);

			args[0] = "";
			BaseControllerUtils.addCommonMessage("message.resumeScanProc.ok", null, ViewMessageLevel.INFO, WebScope.REQUEST, request);
		}

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-DOCBOX-CONTR] Bandeja de documentos - Reanudación del proceso de digitalización de {} documentos realizado en {} ms.", listIdDocBox.size(), (tf - t0));

		return docBox(userDptoId, model, request);
	}

	/**
	 * Controlador para la reailzación del deposito de los documentos
	 * seleccionados en la bandeja de documentos.
	 * 
	 * @param model
	 *            Conjunto de parametros de la capa de presentación.
	 * @param request
	 *            Petición web.
	 * @param idsDocBox
	 *            Lista de documentos seleccionados en la bandeja.
	 * @return plantilla principal que actualiza los cambios en la vista.
	 */
	@RequestMapping(value = "/docBox/depositDocBox", method = { RequestMethod.POST })
	public String depositDocBox(ModelMap model, HttpServletRequest request, ListDocBoxVO idsDocBox) {
		List<Long> listIdDocBox = new ArrayList<Long>();
		long t0;
		long tf;
		String username;
		String respuesta = null;

		t0 = System.currentTimeMillis();

		// comprobamos que exista el parametro, lleva al final [] debido a que
		// es una lista
		if (request.getParameter("idsDocBox[]") == null || request.getParameterValues("idsDocBox[]").length == 0) {
			LOG.error("No se remite el parametro de reanudación idsDocBox");
			respuesta = messages.getMessage("warning.docBox.store.messageIdsDocBoxNotFound", null, request.getLocale());
		} else {

			// comprobamos los datos enviados a través del request
			String[ ] pIdsDocBox = request.getParameterValues("idsDocBox[]");

			// preparamos la info para poder ser enviada al service
			for (int a = 0; a < pIdsDocBox.length; a++) {
				listIdDocBox.add(Long.valueOf(pIdsDocBox[a]));
			}
			// obtenemos la info para auditoria
			UserInSession user = BaseControllerUtils.getUserInSession(request);
			username = user.getUsername();
			// realizamos el deposito y obtenemos la lista de resultado
			respuesta = depositDocBox(listIdDocBox, request, username);
		}

		model.addAttribute("respuestaDeposit", respuesta);

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-DOCBOX-CONTR] Bandeja de documentos - Depósito de {} documentos realizado en {} ms.", listIdDocBox.size(), (tf - t0));

		return "docBox/docBox:: respuestaDeposit";
	}

	/**
	 * Realiza el deposito de los documentos seleccionado de la bandeja de
	 * documentos.
	 * 
	 * @param docIds
	 *            listado de ids de documentos.
	 * @param request
	 *            Petición http.
	 * @param username
	 *            Identificador del nombre del usuario.
	 * @return String con el resultado de la ejecución del deposito.
	 */
	private String depositDocBox(List<Long> docIds, HttpServletRequest request, String username) {
		
		ScannedDocumentDTO doc;
		ScannedDocLogDTO lastScannedDocLogDto;
		String message;
		String result = null;
		String[ ] args = new String[2];
		StringBuilder res = new StringBuilder();

		LOG.debug("[WEBSCAN-DOCBOX-CONTR] Start depositDocBox");

		// verificamos la valide de los documentos
		for (Long docId: docIds) {
			doc = null;
			lastScannedDocLogDto = null;
			try {
				LOG.debug("[WEBSCAN-DOCBOX-CONTR] Se recupera la información del documento a depositar {}.", docId);
				// Se recupera el documento digitalizado y su última traza de
				// ejecución
				doc = docBoxService.getScannedDocument(docId);
				LOG.debug("[WEBSCAN-DOCBOX-CONTR] Se recupera la última traza de ejecución del documento a depositar {}.", doc.getId());
				lastScannedDocLogDto = docBoxService.getLastScannedDocLogsByDocId(doc.getId());

				docBoxService.performDocStore(doc, lastScannedDocLogDto, request, username);

				// procedemos a realizar el deposito, para ello necesitamos
				// recuperar el
				// plugin de deposito procedemos a realizar el deposito
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_ID] = Long.toString(docId);
				args[1] = "";
				result = WebscanDocBoxConstants.MESSAGE_RESULT_DEPOSIT_OK;
			} catch (DAOException e) {
				result = WebscanDocBoxConstants.MESSAGE_RESULT_DEPOSIT_ERROR;
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_ID] = Long.toString(docId);
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_DEPOSIT] = e.getMessage();
			} catch (WebscanDocBoxException e) {
				result = WebscanDocBoxConstants.MESSAGE_RESULT_DEPOSIT_ERROR;
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_ID] = Long.toString(docId);
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_DEPOSIT] = e.getMessage();

				try {
					docBoxService.performErrorDocDepositLogs(doc, username, e);
				} catch (DAOException ex) {
					LOG.error("[WEBSCAN-DOCBOX-CONTR] Error creando trazas de auditoria por error en el depósito del documento {}: {}", docId, ex.getMessage());

				}
			}
			// creamos el mensaje
			message = messages.getMessage(result, args, request.getLocale());
			res.append(message);
			res.append("<br>");
		}

		LOG.debug("[WEBSCAN-DOCBOX-CONTR] End depositDocBox");
		return res.toString();
	}

	/**
	 * Controlador que registra los cambios efectuados sobre los metadatos de un
	 * documento digitalizado en edición.
	 * 
	 * @param model
	 *            Conjunto de parametros de la capa de presentación.
	 * @param request
	 *            Petición web.
	 * @param datos
	 *            información de los metadatos asociados al documento editado en
	 *            la bandeja de documento
	 * @param idDocBox
	 *            identificador del documento digitalizado en edición.
	 * @return plantilla principal que actualiza los cambios en la vista
	 */

	@RequestMapping(value = "/docBox/saveEditDocBox", method = { RequestMethod.POST })
	public String saveEditDocBox(ModelMap model, HttpServletRequest request, ListDatosDocBoxVO datos, Long idDocBox) {
		long t0;
		long tf;
		String[ ] args = new String[1];
		String[ ] argsMet = new String[1];
		String username;
		List<DatosDocBoxVO> datosRecibidos = new ArrayList<DatosDocBoxVO>();
		Map<MetadataSpecificationDTO, MetadataDTO> docMetaDocColl;

		LOG.debug("Start saveEditDocBox");
		t0 = System.currentTimeMillis();

		try {
			for (Map<String, String> dats: datos.getDatos()) {
				// obtenemos primero elemento(idMeta)
				String idMeta = dats.get("idMeta");
				// obtenemos segundo elemento(valor)
				String valor = dats.get("valor");

				DatosDocBoxVO nuevo = new DatosDocBoxVO(idMeta, valor);
				datosRecibidos.add(nuevo);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("Datos recibidos:" + datosRecibidos.size());
			}

			UserInSession user = BaseControllerUtils.getUserInSession(request);
			username = user.getUsername();
			docMetaDocColl = docBoxService.getCompleteDocumentMetadataCollection(idDocBox);

			// si la comprobación es correcta
			if (!docBoxService.checkEditedMetadata(datosRecibidos, docMetaDocColl)) {
				argsMet[0] = Long.toString(idDocBox);
				BaseControllerUtils.addCommonMessage("warning.docBox.reqMetadataNotSet", argsMet, ViewMessageLevel.WARN, WebScope.REQUEST, request);
			}

			docBoxService.updateDocMetadataCollection(idDocBox, datosRecibidos, username, docMetaDocColl);

			args[0] = "default.message.ok";
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_RESULT_OK_VALUE, args, ViewMessageLevel.INFO, WebScope.REQUEST, request);

		} catch (DAOException e) {

			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);

			LOG.error("Error acceso a datos:{}", e.getMessage());

		} catch (Exception e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);

			LOG.error("Error save metadatos del documento:{}", e.getMessage());
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-DOCBOX-CONTR] Bandeja de documentos - Edición de metadatos realizada en {} ms.", (tf - t0));
		}

		LOG.debug("End saveEditDocBox");
		return getEditDocBox(model, request, idDocBox);
	}
	/**
	 * Devuelve la información de los metadatos del documento de digitalización.
	 * 
	 * @param docId
	 *            Identificador del documento digitalizado.
	 * @param request
	 *            Petición http.
	 * @param response
	 *            respuesta http.
	 * @return String con la información de respuesta de la vista de validacion.
	 */
	@RequestMapping(value = "/docBox/getMetadataContent", method = { RequestMethod.POST })
	public String getMetadataContent(@RequestParam(value = "docId") Long docId, @RequestParam(value = "editable") boolean editable, HttpServletRequest request, ModelMap model) {
		LOG.debug("[WEBSCAN-DOCBOX-CONTR] Start getMetadataContent");
		List<String> meRegistreDoc = new ArrayList<String>();
		Map<MetadataSpecificationDTO, MetadataDTO> mapMetadata = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		String error;
		String res = VIEW_CONT_META;

		try {
			mapMetadata = docBoxService.getCompleteDocumentMetadataCollection(docId);

			model.addAttribute("docId", docId);
			model.addAttribute("mapMetadata", mapMetadata);
			model.addAttribute("editable", editable);
			model.addAttribute("listEdit", checkMetadataEdit(mapMetadata));
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-DOCBOX-CONTR] Error recuperando la info de metadatos del documento {}: {}", docId, e.getMessage());
			res = VIEW_MOD_RESP_REG;
			String[ ] args = new String[1];
			args[0] = e.getMessage();

			if (DAOException.CODE_901.equals(e.getCode())) {
				error = messages.getMessage(WebscanDocBoxConstants.DOCBOX_BOOKING_DOC_NOT_FOUND, null, request.getLocale());
			} else {
				error = messages.getMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, request.getLocale());
			}

			meRegistreDoc.add(error);
			model.addAttribute("respWithError", Boolean.TRUE);
			model.addAttribute("resultResponse", meRegistreDoc);

			LOG.error(error);

			// BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE,
			// args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		}
		LOG.debug("[WEBSCAN-DOCBOX-CONTR] End getMetadataContent");
		return res;
	}
	
	/**
	 * Comprueba si los metadatos del documento digitalizado tiene todos los
	 * metadatos editables o no
	 * 
	 * @param mapMetadata
	 *            Map de los metadatos del documento digitalizado
	 * @return Boolean con la respuesta
	 */
	private Boolean checkMetadataEdit(Map<MetadataSpecificationDTO, MetadataDTO> mapMetadata) {
		Boolean result = Boolean.FALSE;
		for (MetadataSpecificationDTO mm: mapMetadata.keySet()) {
			if (mm.getEditable().equals(Boolean.TRUE)) {
				result = Boolean.TRUE;
			}
		}
		return result;
	}
	
	/**
	 * Método de recogida de información de los estados del documento
	 * digitalizado.
	 * 
	 * @param docId
	 *            Identificador del documento digitalizado.
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @return String con la información de respuesta de la vista de validacion.
	 */
	@RequestMapping(value = "/docBox/getStatusContent", method = { RequestMethod.POST })
	public String getStatusContent(Long docId, HttpServletRequest request, ModelMap model) {
		LOG.debug("[WEBSCAN-DOCBOX-CONTR] Start getStatusContent");
		List<String> meRegistreDoc = new ArrayList<String>();
		String error;
		String res = VIEW_MOD_RESP_REG;
		ResultStatusContentVO resultStatusContents = new ResultStatusContentVO();

		try {
			resultStatusContents = getStagesStatus(docId);

			model.addAttribute("resStatusCont", resultStatusContents);
			res = VIEW_CONT_STATUS;
			LOG.debug("[WEBSCAN-DOCBOX-CONTR] End getStatusContent");
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-DOCBOX-CONTR] Error recuperando las trazas de auditoria del documento {}: {}", docId, e.getMessage());
			String[ ] args = new String[1];
			args[0] = e.getMessage();

			if (DAOException.CODE_901.equals(e.getCode())) {
				error = messages.getMessage(WebscanDocBoxConstants.FAST_SCANDOCS_BOOKING_DOC_NOT_FOUND, null, request.getLocale());
			} else {
				error = messages.getMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, request.getLocale());
			}

			meRegistreDoc.add(error);
			model.addAttribute("respWithError", Boolean.TRUE);
			model.addAttribute("resultResponse", meRegistreDoc);

			LOG.error(error);
		}

		return res;
	}
	/**
	 * Método de eliminacion de una etapa de los estados de la vista de
	 * validación
	 * 
	 * @param docId
	 *            Identificador del documento digitalizado.
	 * @return Resultado del contenido a mostrar en la vista de estados de
	 *         validación.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */
	private ResultStatusContentVO getStagesStatus(Long docId) throws DAOException {
		List<ResultStatusContent> aux = new ArrayList<ResultStatusContent>();
		ResultStatusContentVO result = new ResultStatusContentVO();

		result = docBoxService.getStatusContent(docId);

		for (ResultStatusContent rsc: result.getStatusContents()) {
			if (rsc.getEventResult() != null && !rsc.getEventResult().isEmpty()) {
				aux.add(rsc);
			}
		}
		result.setStatusContents(aux);
		return result;
	}
	/**
	 * Método de deposito de documentos digitalizados
	 * 
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @param request
	 *            Petición http.
	 * @param listScannedDoc
	 *            Listado de documentos digitalizados
	 * @return String con la información de respuesta de la vista de validacion.
	 */
	@RequestMapping(value = "docBox/storeBatch", method = { RequestMethod.POST })
	public String storeAllDocs(ModelMap model, HttpServletRequest request, Long batchId) {
		Boolean respWithError = Boolean.TRUE;
		DocBoxActionResult result;
		List<String> meRegistreDoc = new ArrayList<String>();
		long t0;
		long tf;
		//ValidateDocBoxVO validateFastScanDoc = null;
		List<ScannedDocumentDTO> batchDocs = new ArrayList<ScannedDocumentDTO>();
		String res = VIEW_MOD_RESP_REG;

		t0 = System.currentTimeMillis();
		UserInSession user = BaseControllerUtils.getUserInSession(request);

		try {
			//validateFastScanDoc = fastScanDocsService.getValidateViewDocuments(user.getUsername(), user.getDocument(), request.getLocale());
			batchDocs = docBoxService.getScannedDocumentsByBatch(batchId);
			// comprobamos que existan documentos
			//if (!validateFastScanDoc.existActiveScanWork()) {
			//	LOG.error("Error depositando un lote de documentos, el usuario {} no posee ningún proceso de digitalización rápida activo.", user.getUsername());
			//	meRegistreDoc.add(messages.getMessage(FastScanDocsConstants.FAST_SCANDOCS_BOOKING_WORK_NOT_FOUND, null, request.getLocale()));
				// Procesamiento de errores
			//	model.addAttribute("respWithError", respWithError);
			//	model.addAttribute("resultResponse", meRegistreDoc);
			//} else {
				// obtenemos la info para auditoria
				// realizamos el deposito y obtenemos la lista de resultado
				result = docBoxService.storeAllDocuments(batchDocs, request, user.getUsername(), user.getDocument());
				
				if (result.getErrorHappen()) {
					meRegistreDoc.addAll(result.getErrorMessages());
					// Procesamiento de errores
					model.addAttribute("respWithError", respWithError);
					model.addAttribute("resultResponse", meRegistreDoc);
				} else {
					model.addAttribute("respWithError", Boolean.FALSE);
					model.addAttribute("resultResponse", meRegistreDoc);
					BaseControllerUtils.addCommonMessage("docBox.storeAllDocuments.ok", null, ViewMessageLevel.INFO, WebScope.REQUEST, request);
				}
			//}

		} catch (DAOException e) {
			meRegistreDoc.add(messages.getMessage("fastscandoc.error.database", null, request.getLocale()));
			// Procesamiento de errores
			model.addAttribute("respWithError", respWithError);
			model.addAttribute("resultResponse", meRegistreDoc);
		} catch (WebscanDocBoxException e) {
			// Procesamiento de errores
			meRegistreDoc.add(e.getMessage());
			model.addAttribute("respWithError", respWithError);
			model.addAttribute("resultResponse", meRegistreDoc);
			try {
				if (!WebscanDocBoxException.CODE_025.equals(e.getCode())) {
					// Se registran trazas de auditoría para todos los
					// documentos
					docBoxService.performErrorDocsDepositLogs(batchDocs, user.getUsername(), e);
				}
			} catch (DAOException ex) {
				// No hacer nada
			}
		} finally {
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] Depósito de {} documentos realizado en {} ms.", batchDocs.size(), (tf - t0));
		}

		return res;
	}
	/**
	 * Método de guardado de los metadatos de un documento digitalizado.
	 * 
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @param request
	 *            Petición http.
	 * @param datos
	 *            Metadatos a guardar.
	 * @param docId
	 *            Identificador del documento digitalizado.
	 * @return String con la información de respuesta de la vista de validacion.
	 */
	@RequestMapping(value = "/docBox/saveEditMetadata", method = { RequestMethod.POST })
	public String saveEditMetadata(ModelMap model, HttpServletRequest request, ListDatosDocBoxVO datos, @RequestParam(value = "docId") Long docId, @RequestParam(value = "editable") boolean editable) {
		long t0;
		long tf;
		List<String> meRegistreDoc = new ArrayList<String>();
		String error;
		String[ ] args = new String[1];
		String[ ] argsMet = new String[1];
		String res = VIEW_MOD_RESP_REG;
		String username;
		List<DocMetadataVO> recMetadataCollection = new ArrayList<DocMetadataVO>();
		Map<MetadataSpecificationDTO, MetadataDTO> docMetaDocColl;

		LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] Start saveEditMetadata");
		t0 = System.currentTimeMillis();

		try {
			for (Map<String, String> dats: datos.getDatos()) {
				// obtenemos primero elemento(idMeta)
				String idMeta = dats.get("idMeta");
				// obtenemos segundo elemento(valor)
				String valor = dats.get("valor");

				DocMetadataVO nuevo = new DocMetadataVO(idMeta, valor);
				recMetadataCollection.add(nuevo);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] Datos recibidos:" + recMetadataCollection.size());
			}

			UserInSession user = BaseControllerUtils.getUserInSession(request);
			username = user.getUsername();
			docMetaDocColl = docBoxService.getCompleteDocumentMetadataCollection(docId);

			// si la comprobación es correcta
			if (!docBoxService.checkEditedMetadataBatch(recMetadataCollection, docMetaDocColl)) {
				argsMet[0] = Long.toString(docId);
				BaseControllerUtils.addCommonMessage("warning.docBox.reqMetadataNotSet", argsMet, ViewMessageLevel.WARN, WebScope.REQUEST, request);
			}

			docBoxService.updateDocMetadataCollectionBatch(docId, recMetadataCollection, username, docMetaDocColl);

			args[0] = "default.message.ok";
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_RESULT_OK_VALUE, args, ViewMessageLevel.INFO, WebScope.REQUEST, request);
		} catch (DAOException e) {
			if (DAOException.CODE_901.equals(e.getCode())) {
				error = messages.getMessage(WebscanDocBoxConstants.FAST_SCANDOCS_BOOKING_DOC_NOT_FOUND, null, request.getLocale());
			} else {
				args[0] = e.getMessage();
				error = messages.getMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, request.getLocale());
			}

			meRegistreDoc.add(error);
			model.addAttribute("respWithError", Boolean.TRUE);
			model.addAttribute("resultResponse", meRegistreDoc);

			LOG.error("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] Error acceso a datos: {}", e.getMessage());
		} catch (Exception e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(WebscanBaseConstants.DEFAULT_ERROR_MESSAGE, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);

			LOG.error("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] Error save metadatos del documento:{}", e.getMessage());
		} finally {
			if (meRegistreDoc.isEmpty()) {
				res = getMetadataContent(docId, editable, request, model);
			}
			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] Bandeja de documentos - Edición de metadatos realizada en {} ms.", (tf - t0));
		}

		LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] End saveEditMetadata");
		return res;
	}

	/**
	 * Cancelar todos los documentos digitalizados.
	 * 
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @param request
	 *            Petición http
	 * @return String con la información de respuesta de la vista de validacion.
	 */
	@RequestMapping(value = "docBox/cancelScanWork", method = { RequestMethod.POST })
	public String cancelScanWork(Long batchId, ModelMap model, HttpServletRequest request) {
		Integer removedDocs;
		long t0;
		long tf;
		List<ScannedDocumentDTO> batchDocs = new ArrayList<ScannedDocumentDTO>();
		List<String> meRegistreDoc = new ArrayList<String>();
		String res = VIEW_MOD_RESP_REG;
		String[ ] args = new String[1];

		t0 = System.currentTimeMillis();

		UserInSession user = BaseControllerUtils.getUserInSession(request);

		try {
			batchDocs = docBoxService.getScannedDocumentsByBatch(batchId);

			removedDocs = docBoxService.cancelScanWork(batchDocs, user.getUsername(), user.getDocument(), request.getLocale());

			// Se muestra el resultado en pantalla
			args[0] = removedDocs.toString();
			BaseControllerUtils.addCommonMessage("docBox.result.cancelDocs.ok", args, ViewMessageLevel.INFO, WebScope.REQUEST, request);

		} catch (WebscanDocBoxException e) {
			LOG.error("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] Error eliminando documentos del proceso de digitalización rápida activo para el usuario {}: {}", user.getUsername(), e.getMessage());

			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage("docBox.error.cancelScanWork", args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		} catch (Exception e) {
			LOG.error("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] Error inesperado eliminando info del documento:{}", e.getMessage());
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage("docBox.error.cancelScanWork", args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		} finally {

			tf = System.currentTimeMillis();
			LOG.info("[WEBSCAN-GV-FAST-SCANDOCS-CONTR] Cancelación del trabajo de digitalización rápida iniciado por el usuario {} documentos realizado en {} ms.", user.getUsername(), (tf - t0));
		}

		return res;
	}

}
