/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.controller.vo.listDatosDocBoxVO.java.</p>
 * <b>Descripción:</b><p> .</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox.controller.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Entidad que agrupa la visualización del listado del buzon de documentos
 * escaneados.
 * <p>
 * Class ListDocBoxVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

public class ListDocBoxVO implements Serializable {

	/**
	 * Attribute that represents el serial de la clase.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Listado del buzon de documentos.
	 */
	private List<String> listDocBox;

	/**
	 * Constructor method for the class ListDocBoxVO.java.
	 */
	public ListDocBoxVO() {
		listDocBox = new ArrayList<String>();
	}

	/**
	 * Constructor method for the class ListDocBoxVO.java.
	 * 
	 * @param alistDocBox
	 *            Listado del buzon de documentos.
	 */
	public ListDocBoxVO(List<String> alistDocBox) {
		this.listDocBox = alistDocBox;
	}

	/**
	 * Obtiene un listado del buzon de documentos.
	 * 
	 * @return Listado de String de documentos.
	 */
	public List<String> getListDocBox() {
		return listDocBox;
	}

	/**
	 * Modifica el atributo listDocBox.
	 * 
	 * @param alistDocBox
	 *            Listado de String de documentos.
	 */
	public void setListDocBox(List<String> alistDocBox) {
		this.listDocBox = alistDocBox;
	}

}
