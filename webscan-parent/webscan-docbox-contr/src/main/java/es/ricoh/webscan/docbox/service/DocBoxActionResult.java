/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.gva.webscangv.fastscandoc.contr.service.FastScanDocActionResult.java.</p>
 * <b>Descripción:</b><p> Objeto que devuelve el resultado de una acción del proceso de digitalización rápida.</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * @author RICOH Spain IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.docbox.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Objeto que devuelve el resultado de una acción del proceso de digitalización
 * rápida.
 * <p>
 * Class DocBoxActionResult.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class DocBoxActionResult implements Serializable {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Mensajes de error.
	 */
	private List<String> errorMessages = new ArrayList<String>();

	/**
	 * Indica si sucedio un error al procesar el documento.
	 */
	private Boolean errorHappen = Boolean.FALSE;

	/**
	 * Constructor sin argumentos.
	 */
	public DocBoxActionResult() {
		super();
	}

	/**
	 * Gets the value of the attribute {@link #errorMessages}.
	 * 
	 * @return the value of the attribute {@link #errorMessages}.
	 */
	public List<String> getErrorMessages() {
		return errorMessages;
	}

	/**
	 * Sets the value of the attribute {@link #errorMessages}.
	 * 
	 * @param anyErrorMessages
	 *            The value for the attribute {@link #errorMessages}.
	 */
	public void setErrorMessages(List<String> anyErrorMessages) {
		this.errorMessages.clear();

		if (anyErrorMessages != null && !anyErrorMessages.isEmpty()) {
			this.errorMessages.addAll(anyErrorMessages);
		}
	}

	/**
	 * Gets the value of the attribute {@link #errorHappen}.
	 * 
	 * @return the value of the attribute {@link #errorHappen}.
	 */
	public Boolean getErrorHappen() {
		return errorHappen;
	}

	/**
	 * Sets the value of the attribute {@link #errorHappen}.
	 * 
	 * @param errorHappenValue
	 *            The value for the attribute {@link #errorHappen}.
	 */
	public void setErrorHappen(Boolean errorHappenValue) {
		this.errorHappen = errorHappenValue;
	}

}
