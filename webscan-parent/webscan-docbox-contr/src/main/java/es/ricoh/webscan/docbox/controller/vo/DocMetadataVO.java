/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.gva.webscangv.scandoc.contr.controller.vo.DocMetadataVO.java.</p>
 * 
 * <b>Descripción:</b><p> Clase que recoge la información del deposito de la vista de digitalización rápida.</p>
 * 
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.</p>
 * 
 * @author RICOH España IT Services.
 * @version 1.0.
 */
package es.ricoh.webscan.docbox.controller.vo;

import java.io.Serializable;

/**
 * Entidad que representa un metadato en edición de un documento digitalizado
 * mediante el proceso de digitalización rápida.
 * <p>
 * Class DocMetadataVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Webscan GV - Plataforma de digitalización certificada y copia
 * auténtica de la Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public class DocMetadataVO implements Serializable {

	/**
	 * Attribute that represents el serial de la clase .
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * identificador del metadato.
	 */
	private String idMeta;
	/**
	 * valor del metadato.
	 */
	private String valor;

	/**
	 * Constructor method for the class DatosDocBoxVO.java.
	 * 
	 * @param aidMeta
	 *            identificador del metadato.
	 * @param avalor
	 *            valor del metadato.
	 */
	public DocMetadataVO(String aidMeta, String avalor) {
		super();
		this.idMeta = aidMeta;
		this.valor = avalor;
	}

	/**
	 * Gets the value of the attribute {@link #idMeta}.
	 * 
	 * @return the value of the attribute {@link #idMeta}.
	 */
	public String getIdMeta() {
		return idMeta;
	}

	/**
	 * Sets the value of the attribute {@link #idMeta}.
	 * 
	 * @param aidMeta
	 *            The value for the attribute {@link #idMeta}.
	 */
	public void setIdMeta(String aidMeta) {
		this.idMeta = aidMeta;
	}

	/**
	 * Gets the value of the attribute {@link #valor}.
	 * 
	 * @return the value of the attribute {@link #valor}.
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * Sets the value of the attribute {@link #valor}.
	 * 
	 * @param avalor
	 *            The value for the attribute {@link #valor}.
	 */
	public void setValor(String avalor) {
		this.valor = avalor;
	}

}
