/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.scanProcess.service.DocBoxService.java.</p>
 * <b>Descripción:</b><p> Lógica de negocio de la utilidad bandeja de documentos.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.docbox.WebscanDocBoxConstants;
import es.ricoh.webscan.docbox.WebscanDocBoxException;
import es.ricoh.webscan.docbox.controller.vo.BatchVO;
import es.ricoh.webscan.docbox.controller.vo.DatosDocBoxVO;
import es.ricoh.webscan.docbox.controller.vo.DocBoxLogVO;
import es.ricoh.webscan.docbox.controller.vo.DocBoxVO;
import es.ricoh.webscan.docbox.controller.vo.DocMetadataVO;
import es.ricoh.webscan.docbox.controller.vo.ResultStatusContentVO;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelConstants;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.domain.OperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditOperationDTO;
import es.ricoh.webscan.model.dto.BatchDTO;
import es.ricoh.webscan.model.dto.DocBoxScannedDocDTO;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;
import es.ricoh.webscan.model.dto.ScannedDocLogDTO;
import es.ricoh.webscan.model.dto.ScannedDocumentDTO;
import es.ricoh.webscan.model.service.AuditService;
import es.ricoh.webscan.plugins.Plugin;
import es.ricoh.webscan.plugins.PluginDataModelUtilities;
import es.ricoh.webscan.plugins.PluginException;
import es.ricoh.webscan.plugins.PluginProvider;

/**
 * Lógica de negocio de la utilidad bandeja de documentos.
 * <p>
 * Class DocBoxService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class DocBoxService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DocBoxService.class);

	/**
	 * Objeto que implementa la tarea de asincrona del proceso de reanudación de
	 * digitalización.
	 */
	private AsyncScanProcessResumeManager asyncScanProcessResumeManager;

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre el modelo de datos.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Objeto que representa el acceso a los servicio de auditoria.
	 */
	private AuditService auditService;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Objeto que representa el acceso a los servicio de ejecución de plugins.
	 */
	private PluginProvider pluginProvider;

	/**
	 * Utilidades de acceso al modelo de datos del Sistema para plugins.
	 */
	private PluginDataModelUtilities pluginDataModelUtilities;

	/**
	 * Obtiene los documentos mostrados en la bandeja de documentos para una
	 * relación de unidades organizativas y colectivos determinados. Para cada
	 * documento se recupera su información general y la trazas de ejecución de
	 * la útlima fase ejecutada del proceso de digitalización.
	 * 
	 * @param orgUnitIds
	 *            Relación de unidades organizativas a las que pertenece la
	 *            persona usuaria solicitante.
	 * @param functionalOrgs
	 *            unidades orgánicas a las que pertenece o presta servicio la
	 *            persona usuaria solicitante.
	 * @return Listado de documentos digitalizados para las unidades
	 *         organizativas y orgánicas especificas como parámetros de entrada.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */
	public List<DocBoxVO> getScannedDocumentsAndLastLog(List<Long> orgUnitIds, String functionalOrgs) throws DAOException {
		LOG.debug("Start getScannedDocuments");
		List<DocBoxVO> res = new ArrayList<DocBoxVO>();
		long t0;
		long tf;
		DocBoxVO vo;

		t0 = System.currentTimeMillis();
		// recuperamos los documentos actuales por unida organizativa y
		// funcional
		List<DocBoxScannedDocDTO> mapScannedDoc = webscanModelManager.getDocumentsAndLastLogByOrgUnits(orgUnitIds, functionalOrgs);

		for (DocBoxScannedDocDTO docBoxScannedDoc: mapScannedDoc) {
			vo = fillDocBoxScannedDoc(docBoxScannedDoc);

			res.add(vo);
		}

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-DOCBOX-SERVICE] Bandeja de documentos - Recuperación de {} documentos realizada en {} ms.", res.size(), (tf - t0));

		LOG.debug("End getScannedDocuments");
		return res;

	}

	/**
	 * Obtiene los documentos pertenecientes a un colectivo determinado y a 
	 * un proceso de digitalización determinado. Para cada
	 * documento se recupera su información general y la trazas de ejecución de
	 * la útlima fase ejecutada del proceso de digitalización.
	 * 
	 * @param batchId
	 *            Proceso de digitalización para el que hay que recuperar los 
	 *            documentos
	 * @param functionalOrgs
	 *            unidades orgánicas a las que pertenece o presta servicio la
	 *            persona usuaria solicitante.
	 * @return Listado de documentos digitalizados para las unidades
	 *         organizativas y orgánicas especificas como parámetros de entrada.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */
	public List<DocBoxVO> getDocsByBatchId(Long batchId) throws DAOException {
		LOG.debug("Start getDocsByBatchReqId");
		List<DocBoxVO> res = new ArrayList<DocBoxVO>();
		long t0;
		long tf;
		DocBoxVO vo;

		t0 = System.currentTimeMillis();
		// recuperamos los documentos actuales por unida organizativa y
		// funcional
		List<DocBoxScannedDocDTO> mapScannedDoc = webscanModelManager.getDocsByBatchId(batchId);

		for (DocBoxScannedDocDTO docBoxScannedDoc: mapScannedDoc) {
			vo = fillDocBoxScannedDoc(docBoxScannedDoc);

			res.add(vo);
		}

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-DOCBOX-SERVICE] Bandeja de documentos - Recuperación de {} documentos realizada en {} ms.", res.size(), (tf - t0));

		LOG.debug("End getScannedDocuments");
		return res;

	}

	/**
	 * Obtiene la información de un documento digitalizado que es presentada en
	 * la bandeja de documentos, incluyendo sus metadatos.
	 * 
	 * @param scannedDocId
	 *            identificador de documento.
	 * @return información de un documento digitalizado presentada en la bandeja
	 *         de documentos.
	 * @throws DAOException
	 *             Si se produce un error al ejecutar la consulta sobre el
	 *             modelo de datos.
	 * @throws DAOException 
	 */
	public DocBoxVO getInfoScannedDocument(Long scannedDocId) throws DAOException {
		LOG.debug("Start getScannedDocumentsComplete");

		DocBoxVO res;
		ScannedDocumentDTO scannedDoc = webscanModelManager.getDocument(scannedDocId);
		Map<MetadataSpecificationDTO, MetadataDTO> mapMetadatos = webscanModelManager.getCompleteDocumentMetadataCollection(scannedDocId);

		res = fillDocBox(scannedDoc, mapMetadatos);

		LOG.debug("End getScannedDocumentsComplete");
		return res;
	}

	/**
	 * Obtiene la información general y contenido de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            identificador de documento.
	 * @return información general y contenido de un documento digitalizado
	 *         registrados en el sistema.
	 * @throws DAOException
	 *             Si se produce un error al ejecutar la consulta sobre el
	 *             modelo de datos.
	 */
	public ScannedDocumentDTO getScannedDocument(Long scannedDocId) throws DAOException {
		ScannedDocumentDTO res = webscanModelManager.getDocument(scannedDocId);

		return res;
	}

	/**
	 * Obtiene la última traza de ejecución de un documento digitalizado.
	 * 
	 * @param docId
	 *            identificador de documento.
	 * @return última traza de ejecución de un documento digitalizado.
	 * @throws DAOException
	 *             Si se produce un error al ejecutar la consulta sobre el
	 *             modelo de datos.
	 */
	public ScannedDocLogDTO getLastScannedDocLogsByDocId(Long docId) throws DAOException {
		ScannedDocLogDTO res = webscanModelManager.getLastScannedDocLogsByDocId(docId);

		return res;
	}

	/**
	 * Obtiene la traza de ejecución de una fase asociada al proceso de
	 * digitalización de un documento digitalizado.
	 * 
	 * @param scannedDocId
	 *            Identificador de documento digitalizado.
	 * @param stageName
	 *            Nombre de la fase.
	 * @return Traza de ejecución de una fase asociada al proceso de
	 *         digitalización del documento solicitado. En caso de no
	 *         encontrarse, devuelve null.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             documento digitalizado, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public ScannedDocLogDTO getScannedDocLogByStageNameAndDocId(Long scannedDocId, String stageName) throws DAOException {
		ScannedDocLogDTO res = null;

		try {
			webscanModelManager.getScannedDocLogByStageNameAndDocId(scannedDocId, stageName);
		} catch (DAOException e) {
			if (!e.getCode().equals(DAOException.CODE_901)) {
				throw e;
			}
		}
		return res;
	}

	/**
	 * Rellena un objeto documento digitalizado de la capa de presentación a
	 * presentar en la bandeja de documentos.
	 * 
	 * @param scannedDoc
	 *            DTO de información de documento digitalizado a presentar en
	 *            bandeja de documentos.
	 * @param mapMetadatos
	 *            Relación de metadatos.
	 * @param listScannedDocLog
	 *            Lista de trazas de ejecución del documento.
	 * @return objeto documento digitalizado de la capa de presentación.
	 */
	private DocBoxVO fillDocBoxScannedDoc(DocBoxScannedDocDTO scannedDoc) {
		DocBoxVO res;
		DocBoxLogVO lastLog;

		res = new DocBoxVO();
		lastLog = new DocBoxLogVO();
		// Datos básicos
		res.setBatchNameId(scannedDoc.getDocBatchNameId());
		res.setBatchId(scannedDoc.getDocBatchId());
		res.setDigProcStartDate(scannedDoc.getDocStartDate());
		res.setFunctionalOrgs(scannedDoc.getDocFunctionalOrgs());
		res.setId(scannedDoc.getDocId());
		res.setName(scannedDoc.getDocName());
		res.setOrderNum(scannedDoc.getDocBatchOrderNum());
		res.setUsername(scannedDoc.getDocUsername());
		// Última traza de ejecución
		lastLog.setId(scannedDoc.getLastDocExecLogId());
		lastLog.setStageName(scannedDoc.getLastDocExecLogStageName());
		lastLog.setStatus(scannedDoc.getLastDocExecLogStatus());
		res.setLastScannedDocLog(lastLog);

		return res;
	}

	/**
	 * Rellena un objeto documento digitalizado de la capa de presentación.
	 * 
	 * @param scannedDoc
	 *            DTO de documento digitalizado.
	 * @param mapMetadatos
	 *            Relación de metadatos.
	 * @param listScannedDocLog
	 *            Lista de trazas de ejecución del documento.
	 * @return objeto documento digitalizado de la capa de presentación.
	 * @throws DAOException 
	 */
	private DocBoxVO fillDocBox(ScannedDocumentDTO scannedDoc, Map<MetadataSpecificationDTO, MetadataDTO> mapMetadatos) throws DAOException {
		DocBoxVO res = null;
		BatchDTO batch;
		 
		
		// Datos básicos
		res.setAuditOpId(scannedDoc.getAuditOpId());
		batch = webscanModelManager.getBatch(scannedDoc.getBatchId().getId());
		res.setBatchNameId(batch.getBatchNameId());
		res.setDigProcEndDate(scannedDoc.getDigProcEndDate());
		res.setDigProcStartDate(scannedDoc.getDigProcStartDate());
		res.setFunctionalOrgs(scannedDoc.getFunctionalOrgs());
		res.setId(scannedDoc.getId());
		res.setMimeType(scannedDoc.getMimeType());
		res.setName(scannedDoc.getName());
		res.setOrderNum(scannedDoc.getBatchOrderNum());
		res.setOrgScanProfileDocSpec(scannedDoc.getOrgScanProfileDocSpec().getId());
		res.setSignatureType(scannedDoc.getSignatureType());
		res.setUsername(scannedDoc.getUsername());
		// Metadatos
		if (mapMetadatos != null && !mapMetadatos.isEmpty()) {
			res.setMapMetadatos(mapMetadatos);
		}
	

		return res;
	}

	/**
	 * Realiza el deposito de un documento digitalizado, verificando previamente
	 * que es posible llevarlo a cabo.
	 * 
	 * @param doc
	 *            Documento digitalizado a depositar.
	 * @param lastScannedDocLogDto
	 *            Última fase del proceso de digtialización ejecutada para un
	 *            documento.
	 * @param request
	 *            Petición web.
	 * @param username
	 *            Identificador del nombre del usuario.
	 * @throws WebscanDocBoxException
	 *             Si se produce un error al efectuar el depósito del documento.
	 */
	public void performDocStore(ScannedDocumentDTO doc, ScannedDocLogDTO lastScannedDocLogDto, HttpServletRequest request, String username) throws WebscanDocBoxException {
		LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Se inicia el depósito del documento {}.", doc.getId());
		// comprobamos que se pueda realizar el deposito
		LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Se verifica que el documento {} puede ser depositado.", doc.getId());
		checkPerformStoreDocument(doc, lastScannedDocLogDto, request);

		// procedemos a realizar el deposito, para ello necesitamos
		// recuperar el
		// plugin de deposito procedemos a realizar el deposito
		// ejecutamos el plugin de deposito de documentos
		LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Se invoca el plugin de depósito para el documento {}.", doc.getId());
		executePluginDepositDoc(doc, lastScannedDocLogDto, username);
		LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Finalizado correctamente el depósito del documento {}.", doc.getId());

	}

	/**
	 * Realiza la ejecución del plugin de deposito del docId.
	 * 
	 * @param scanDocId
	 *            idsDocBox listado de ids de documentos.
	 * @param docLogDto
	 *            Última traza de ejecución del proceso de digitalización del
	 *            documento.
	 * @param username
	 *            identificador del nombre de usuario.
	 * @throws WebscanDocBoxException
	 *             Si se produce algun error al ejecutar el plugin de depósito o
	 *             actualizar el modelo de datos.
	 */
	private void executePluginDepositDoc(ScannedDocumentDTO doc, ScannedDocLogDTO docLogDto, String username) throws WebscanDocBoxException {
		ScannedDocLogDTO lastScannedDocLogDto = null;

		try {
			// recuperamos la relacion de perfil de digitalización y unidad
			// organizativa
			ScanProfileOrgUnitDTO scanProfileOrgUnit = webscanModelManager.getScanProfileOrgUnitByDoc(doc.getId());
			// recuperamos la especificación del perfil de digitalizacion
			ScanProfileDTO scanProfile = webscanModelManager.getScanProfile(scanProfileOrgUnit.getScanProfile());
			// recuperamos el map con las etapas y los plugins asociados
			Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> mapStagePlugin = webscanModelManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfile.getSpecification());
			LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Recuperamos la informacion de base de datos que necesitamos");
			// Se obtiene la specificación del plugin configurado para el perfil
			// en esta fase
			PluginSpecificationDTO pluginSpecification = webscanModelManager.getPluginSpecByScanProfileOrgUnitAndStage(scanProfileOrgUnit.getId(), WebscanBaseConstants.STORE_STAGE_NAME);
			ScanProfileSpecScanProcStageDTO scanProfileSpecScanProcStage = pluginDataModelUtilities.getScanProfileSpecScanProcStageByScanProfileSpecIdAndScanProcStageId(mapStagePlugin, scanProfile.getSpecification(), WebscanBaseConstants.STORE_STAGE_NAME);

			LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Validamos la última traza del documento");
			if (WebscanBaseConstants.STORE_STAGE_NAME.equals(docLogDto.getScanProcessStageName())) {
				// La última fase ejecutada fue la fase de depósito
				lastScannedDocLogDto = docLogDto;
				lastScannedDocLogDto.setStatus(OperationStatus.IN_PROCESS);
				webscanModelManager.setStatusScannedDocLog(lastScannedDocLogDto);
			} else {
				// La última fase es anterior a la fase de depósito,
				// Se registra la fase de depósito
				lastScannedDocLogDto = new ScannedDocLogDTO();
				lastScannedDocLogDto.setStartDate(new Date());
				lastScannedDocLogDto.setScanProcessStageName(WebscanBaseConstants.STORE_STAGE_NAME);
				lastScannedDocLogDto.setStatus(OperationStatus.IN_PROCESS);
				// creamos nueva traza
				Long scannedDocLogDtoId = webscanModelManager.createScannedDocLog(lastScannedDocLogDto, doc.getId(), scanProfileSpecScanProcStage.getScanProcessStageId()); // IN_PROCESS
				LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Creada la traza de ejcución {} para la fase {}.", scannedDocLogDtoId, WebscanBaseConstants.STORE_STAGE_NAME);
				lastScannedDocLogDto = webscanModelManager.getLastScannedDocLogsByDocId(doc.getId());
			}

			Plugin plug = pluginProvider.getDepositPlugin(pluginSpecification.getName());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Inicializamos plugin:" + pluginSpecification.getName() + " - Fase:" + scanProfileSpecScanProcStage.getScanProcessStage());
			}
			// inicializamos el plugin
			plug.initialize(scanProfileSpecScanProcStage.getScanProcessStageId(), doc.getId());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Ejecutamos plugin:" + pluginSpecification.getName());
			}
			// ejecutamos el plugin
			plug.performAction();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Fin ejecución plugin:" + pluginSpecification.getName());
			}

			// Se actualiza la traza de ejecución del documento
			lastScannedDocLogDto.setStatus(OperationStatus.PERFORMED);
			lastScannedDocLogDto.setEndDate(new Date());
			performDocDepositLogs(doc, lastScannedDocLogDto, username);
		} catch (DAOException e) {
			// auditoria se registra error
			LOG.error("[WEBSCAN-DOCBOX-SERVICE] Error al depositar el documento {} (ejecución depósito, modelo datos): {}", doc.getId(), e.getMessage());
			throw new WebscanDocBoxException(WebscanDocBoxException.CODE_403, e.getMessage(), e);
		} catch (PluginException e) {
			LOG.error("[WEBSCAN-DOCBOX-SERVICE] Error al depositar el documento {} (ejecución depósito, plugin): {}", doc.getId(), e.getMessage());
			throw new WebscanDocBoxException(WebscanDocBoxException.CODE_403, e.getMessage(), e);
		}

	}

	/**
	 * LLeva a cabo, de forma transaccional, el registro de trazas de ejecución
	 * y auditoría tras ejecutar el plugin de depósito sobre un documento, así
	 * como su borrado del sistema.
	 * 
	 * @param doc
	 *            Documento depositado.
	 * @param lastScannedDocLogDto
	 *            última traza de ejecución del proceso de digigtalización del
	 *            documento.
	 * @param username
	 *            Nombre de usuario del usuario que solicita el depósito del
	 *            documento.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	private void performDocDepositLogs(ScannedDocumentDTO doc, ScannedDocLogDTO lastScannedDocLogDto, String username) throws DAOException {
		webscanModelManager.closeScannedDocLog(lastScannedDocLogDto);
		// auditoria
		createDepositAuditLogs(doc, username, null);
		webscanModelManager.removeScannedDoc(doc.getId());
	}

	/**
	 * Actualiza, de forma transaccional, el registro de trazas de ejecución y
	 * auditoría tras fallar la ejecución el plugin de depósito sobre un
	 * documento.
	 * 
	 * @param doc
	 *            Documento.
	 * @param username
	 *            Nombre de usuario del usuario que solicita el depósito del
	 *            documento.
	 * @param exc
	 *            Excepción.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	public void performErrorDocDepositLogs(ScannedDocumentDTO doc, String username, Exception exc) throws DAOException {
		ScannedDocLogDTO scannedDocLog = null;
		ScanProcessStageDTO storeStage;

		try {
			scannedDocLog = webscanModelManager.getScannedDocLogByStageNameAndDocId(doc.getId(), WebscanBaseConstants.STORE_STAGE_NAME);
		} catch (DAOException e) {
			if (!DAOException.CODE_901.equals(e.getCode())) {
				throw e;
			}
		}

		if (scannedDocLog == null) {
			// Se crea la traza de ejecución de la fase de depósito
			scannedDocLog = new ScannedDocLogDTO();
			scannedDocLog.setScanProcessStageName(WebscanBaseConstants.STORE_STAGE_NAME);
			scannedDocLog.setStatus(OperationStatus.IN_PROCESS);
			scannedDocLog.setStartDate(new Date());
			storeStage = webscanModelManager.getScanProcStageByName(WebscanBaseConstants.STORE_STAGE_NAME);
			Long scannedDocLogDtoId = webscanModelManager.createScannedDocLog(scannedDocLog, doc.getId(), storeStage.getId()); // IN_PROCESS
			LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Creada la traza de ejcución {} para la fase {}.", scannedDocLogDtoId, WebscanBaseConstants.STORE_STAGE_NAME);
			scannedDocLog = webscanModelManager.getScannedDocLogByStageNameAndDocId(doc.getId(), WebscanBaseConstants.STORE_STAGE_NAME);
		}

		scannedDocLog.setStatus(OperationStatus.FAILED);
		webscanModelManager.setStatusScannedDocLog(scannedDocLog);
		// auditoria
		createDepositAuditLogs(doc, username, exc);
	}

	/**
	 * Cierra la traza de auditoría de un documento tras realizar su depósito,
	 * registrando adicionalmente el evento de la fase de depósito.
	 * 
	 * @param doc
	 *            documento que es depositado.
	 * @param username
	 *            usuario que efectúa el depósito.
	 * @param exc
	 *            Excepción.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	private void createDepositAuditLogs(ScannedDocumentDTO doc, String username, Exception exc) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation = auditService.getAuditOperation(doc.getAuditOpId());
		Date now = new Date();
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;

		auditOperation.setLastUpdateDate(now);
		auditEvent = new AuditEventDTO();

		if (exc != null) {
			// Tamaño máximo del campo 256 caracteres
			additionalInfo = "Error al depositar el documento " + doc.getId() + ". Excepción " + exc.getClass().getSimpleName() + ": " + exc.getMessage();
			auditEvent.setEventResult(AuditOpEventResult.ERROR);
		} else {
			additionalInfo = "Depósito del documento " + doc.getId() + "(" + doc.getName() + ") realizado correctamente.";
			auditOperation.setStatus(AuditOperationStatus.CLOSED_OK);
			auditEvent.setEventResult(AuditOpEventResult.OK);
		}

		if (additionalInfo.length() > WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
			additionalInfo = additionalInfo.substring(0, WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
		}

		auditEvent.setAdditionalInfo(additionalInfo);
		auditEvent.setEventDate(now);
		auditEvent.setEventName(AuditEventName.SCAN_PROC_DEPOSIT_DOC);
		auditEvent.setEventUser(username);
		auditEvents.add(auditEvent);

		if (exc != null) {
			auditService.updateAuditLogs(auditOperation, auditEvents);
		} else {
			auditService.addNewAuditLogs(auditOperation, auditEvents, Boolean.TRUE);
		}
	}

	/**
	 * Cierra la traza de auditoría de un documento tras realizar su borrado.
	 * 
	 * @param doc
	 *            documento que es eliminado.
	 * @param username
	 *            usuario que solicita el borrado.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	private void createRemoveAuditLogs(ScannedDocumentDTO doc, String username) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation = auditService.getAuditOperation(doc.getAuditOpId());
		Date now = new Date();
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;

		auditOperation.setLastUpdateDate(now);
		auditEvent = new AuditEventDTO();

		additionalInfo = "Borrado del documento " + doc.getId() + " realizado correctamente.";
		auditOperation.setStatus(AuditOperationStatus.CLOSED_ERROR);
		auditEvent.setEventResult(AuditOpEventResult.OK);

		if (additionalInfo.length() > WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
			additionalInfo = additionalInfo.substring(0, WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
		}

		auditEvent.setAdditionalInfo(additionalInfo);
		auditEvent.setEventDate(now);
		auditEvent.setEventName(AuditEventName.SCAN_PROC_REMOVE_DOC);
		auditEvent.setEventUser(username);
		auditEvents.add(auditEvent);

		auditService.addNewAuditLogs(auditOperation, auditEvents, Boolean.TRUE);
	}

	/**
	 * Reanuda la ejecución de los documento de la bandeja de documentos.
	 * 
	 * @param idsDocBox
	 *            listado de ids de documentos.
	 * @param username
	 *            identificador del nombre del usuario.
	 */
	public void restartDocBox(List<Long> idsDocBox, String username) {
		LOG.debug("Start restartDocBox");

		for (int a = 0; a < idsDocBox.size(); a++) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Solicitado reanudación del proceso de de digitalizacion, scanDoc:" + idsDocBox.get(a));
			}

			asyncScanProcessResumeManager.resumeDocScanProcess(idsDocBox.get(a), username);

			if (LOG.isDebugEnabled()) {
				LOG.debug("Fin  Solicitado reanudación del proceso de de digitalizacion, scanDoc:" + idsDocBox.get(a));
			}

		}

		LOG.debug("End restartDocBox");
	}

	/**
	 * Elimina el documento de la bandeja de documentos.
	 * 
	 * @param docIds
	 *            listado de ids de documentos
	 * @param username
	 *            usuario que solicita la operación.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de auditoría, se encuentra cerrada, o se produce algún
	 *             error en la ejecución de las sentencias de BBDD.
	 */

	public void removeDocBox(List<Long> docIds, String username) throws DAOException {
		ScannedDocumentDTO doc;
		LOG.debug("Start removeDocBox");

		// registramos la auditoria
		for (Long docId: docIds) {
			doc = webscanModelManager.getDocument(docId);
			webscanModelManager.removeScannedDoc(docId);
			createRemoveAuditLogs(doc, username);
		}

		LOG.debug("End removeDocBox");
	}

	/**
	 * Comprueba si es posible llevar a cabo la fase de depósito para un
	 * documento digitalziado.
	 * 
	 * @param docId
	 *            Documento digitalizado a depositar.
	 * @param lastDocLog
	 *            útlima fase del proceso de digtialización ejecutada para un
	 *            documento.
	 * @param request
	 *            Petición web.
	 * @throws WebscanDocBoxException
	 *             Si no es posible llevar a cabo la fase de depósito del
	 *             documento.
	 */
	private void checkPerformStoreDocument(ScannedDocumentDTO doc, ScannedDocLogDTO lastDocLog, HttpServletRequest request) throws WebscanDocBoxException {
		checkScanProcessStageToStore(doc, lastDocLog, request);

		checkDocMetadataCollection(doc, request);
	}

	/**
	 * Comprueba si es posible llevar a cabo el depósito de un documento
	 * digitalizado, comprobando que todas las fases requeridas del proceso de
	 * digitalización han sido ejecutadas, exceptuando el propio depósito.
	 * 
	 * @param doc
	 *            Documento.
	 * @param lastDocLog
	 *            útlima fase del proceso de digtialización ejecutada para un
	 *            documento.
	 * @param request
	 *            Petición web.
	 * @throws WebscanDocBoxException
	 *             Si no es posible llevar a cabo la fase de depósito del
	 *             documento.
	 */
	private void checkScanProcessStageToStore(ScannedDocumentDTO doc, ScannedDocLogDTO lastDocLog, HttpServletRequest request) throws WebscanDocBoxException {
		List<ScannedDocLogDTO> docTraces;
		List<ScanProfileSpecScanProcStageDTO> scanProcStages;
		ScanProfileDTO scanProfile;
		ScanProfileOrgUnitDTO scanProfileOrgUnit;
		OrganizationUnitDTO organizationUnit;
		String message;
		String[ ] args;

		try {
			// recuperamos la relacion de perfil de digitalización y unidad
			// organizativa
			scanProfileOrgUnit = webscanModelManager.getScanProfileOrgUnitByDoc(doc.getId());
			scanProfile = webscanModelManager.getScanProfile(scanProfileOrgUnit.getScanProfile());
			scanProcStages = webscanModelManager.getScanProcStagesByScanProfileSpec(scanProfile.getSpecification());
			docTraces = webscanModelManager.getScannedDocLogsByDocId(doc.getId());
			organizationUnit = webscanModelManager.getOrganizationUnit(scanProfileOrgUnit.getOrgUnit());

			// recuperamos el tipo de documentos si está activo
			ScanProfileOrgUnitDocSpecDTO scanProfileOrgUnitDocSpec = getScanProfileOrgUnitDocSpecByScanProfileAndOrgUnitAndScannedDoc(scanProfile, organizationUnit, doc);
			DocumentSpecificationDTO docSpec = webscanModelManager.getDocSpecification(scanProfileOrgUnitDocSpec.getDocSpecification());
			if (!scanProfileOrgUnitDocSpec.getActive()) {
				args = new String[WebscanDocBoxConstants.MESSAGE_ELEMENT_COUNT + 1];

				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_ID] = Long.toString(doc.getId());
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_SCANPROFILE] = scanProfile.getName();
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_ORG_UNIT] = organizationUnit.getExternalId();
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_DOCSPEC] = docSpec.getName();
				message = messages.getMessage(WebscanDocBoxConstants.MESSAGE_RESULT_DEPOSIT_ERROR_STATE, args, request.getLocale());
				throw new WebscanDocBoxException(WebscanDocBoxException.CODE_401, message);
			}

			if (docTraces == null || docTraces.isEmpty()) {
				args = new String[WebscanDocBoxConstants.MESSAGE_ELEMENT_COUNT];

				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_ID] = Long.toString(doc.getId());
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_STAGE] = lastDocLog.getScanProcessStageName();
				args[WebscanDocBoxConstants.MESSAGE_POS_DOC_STATUS] = lastDocLog.getStatus().name();
				message = messages.getMessage(WebscanDocBoxConstants.MESSAGE_RESULT_DEPOSIT_ERROR_STATE, args, request.getLocale());
				throw new WebscanDocBoxException(WebscanDocBoxException.CODE_401, message);
			}

			for (ScanProfileSpecScanProcStageDTO spssps: scanProcStages) {
				if (spssps.getMandatory() && !checkPerformedStage(spssps, docTraces)) {
					args = new String[WebscanDocBoxConstants.MESSAGE_ELEMENT_COUNT];

					args[WebscanDocBoxConstants.MESSAGE_POS_DOC_ID] = Long.toString(doc.getId());
					args[WebscanDocBoxConstants.MESSAGE_POS_DOC_STAGE] = lastDocLog.getScanProcessStageName();
					args[WebscanDocBoxConstants.MESSAGE_POS_DOC_STATUS] = lastDocLog.getStatus().name();
					message = messages.getMessage(WebscanDocBoxConstants.MESSAGE_RESULT_DEPOSIT_ERROR_STATE, args, request.getLocale());
					throw new WebscanDocBoxException(WebscanDocBoxException.CODE_401, message);
				}
			}
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-DOCBOX-SERVICE] Error verificando la posibilidad de depositar un documento (fases proceso): {}", e.getMessage());
			throw new WebscanDocBoxException(WebscanDocBoxException.CODE_400, e.getMessage(), e);
		} catch (WebscanDocBoxException e) {
			throw e;
		}
	}

	/**
	 * Recupera la entidad ScanProfileOrgUnitDocSpecDTO relacionada con el
	 * perfil de digitalización, de la unidad organizativa y del documento
	 * escaneado.
	 * 
	 * @param scanProfile
	 *            Objecto identificativo del perfil de digitalizacion
	 * @param organizationUnit
	 *            Objecto identificativo de la unidad organizativa
	 * @param docScanned
	 *            Objecto identificativo del documento escaneado
	 * @throws DAOException
	 *             Si el documento no tiene algún metadato requerido, o se
	 *             produce un error recueprando la relación de metadatos del
	 *             documento del modelo de datos.
	 * @return ScanProfileOrgUnitDocSpecDTO Entidad asociada a la especificación
	 *         de documento del perfil de escaneo y la unidad organizativa
	 *         indicada.
	 */
	public ScanProfileOrgUnitDocSpecDTO getScanProfileOrgUnitDocSpecByScanProfileAndOrgUnitAndScannedDoc(ScanProfileDTO scanProfile, OrganizationUnitDTO organizationUnit, ScannedDocumentDTO docScanned) throws DAOException {
		List<ScanProfileOrgUnitDocSpecDTO> listScanProfileOrgUnitDocSpecDTO = webscanModelManager.getScanProfileOrgUnitDocSpecConf(scanProfile, organizationUnit);
		ScanProfileOrgUnitDocSpecDTO res = null;
		for (ScanProfileOrgUnitDocSpecDTO scanProfileOrgUnitDocSpec: listScanProfileOrgUnitDocSpecDTO) {
			if (scanProfileOrgUnitDocSpec.getId().equals(docScanned.getOrgScanProfileDocSpec().getId())) {
				res = scanProfileOrgUnitDocSpec;
			}
		}
		return res;
	}

	/**
	 * Comprueba si una fase de un proceso ha sido ejecutada correctamente, o es
	 * la fase de depósito y ha sido ejecutada con errores.
	 * 
	 * @param scanProfileSpecScanProcStage
	 *            configuración de una fase para un proceso de digitalziación.
	 * @param docTraces
	 *            trazas de ejecución de un documento.
	 * @return Si la fase de un proceso ha sido ejecutada correctamente, o es la
	 *         fase de depósito y ha sido ejecutada con errores, retorna true.
	 *         En caso contrario, false.
	 */
	private Boolean checkPerformedStage(ScanProfileSpecScanProcStageDTO scanProfileSpecScanProcStage, List<ScannedDocLogDTO> docTraces) {
		Boolean found = Boolean.FALSE;
		Boolean res = Boolean.FALSE;
		ScannedDocLogDTO sdl;

		for (Iterator<ScannedDocLogDTO> it = docTraces.iterator(); !found && it.hasNext();) {
			sdl = it.next();
			if (WebscanBaseConstants.STORE_STAGE_NAME.equals(sdl.getScanProcessStageName())) {
				res = OperationStatus.FAILED.equals(sdl.getStatus());
				found = Boolean.TRUE;
			} else {
				if (scanProfileSpecScanProcStage.getScanProcessStageId().equals(sdl.getScanProcessStageId())) {
					res = OperationStatus.PERFORMED.equals(sdl.getStatus());
					found = Boolean.TRUE;
				}
			}
		}

		if (!found && WebscanBaseConstants.STORE_STAGE_NAME.equals(scanProfileSpecScanProcStage.getScanProcessStage())) {
			res = Boolean.TRUE;
		}

		return res;
	}

	/**
	 * Comprueba que hayan sido informado todos los metadatos requeridos para un
	 * documento digitalizado.
	 * 
	 * @param doc
	 *            Documento digitalizado.
	 * @param request
	 *            Petición http.
	 * @throws WebscanDocBoxException
	 *             Si no es posible llevar a cabo la fase de depósito del
	 *             documento, al no tener informado algún metadato requerido, o
	 *             se produce un error recueprando la relación de metadatos del
	 *             documento del modelo de datos.
	 */
	private void checkDocMetadataCollection(ScannedDocumentDTO doc, HttpServletRequest request) throws WebscanDocBoxException {
		Boolean emptySrcValue;
		Boolean emptyTextValue;

		Map<MetadataSpecificationDTO, MetadataDTO> docSpecMetadataColl;
		MetadataDTO metadata;
		try {
			LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Verificando los metadatos del documento {}", doc.getId());
			docSpecMetadataColl = webscanModelManager.getCompleteDocumentMetadataCollection(doc.getId());

			for (MetadataSpecificationDTO metadataSpec: docSpecMetadataColl.keySet()) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Verificando el metadato {} del documento {}", metadataSpec.getName(), doc.getId());
				}
				if (metadataSpec.getMandatory()) {
					metadata = docSpecMetadataColl.get(metadataSpec);
					emptySrcValue = (metadataSpec.getSourceValues() != null && !metadataSpec.getSourceValues().equals("[]")) && (metadata.getValue() == null || metadata.getValue().trim().isEmpty() || metadata.getValue().equals("-1"));
					emptyTextValue = metadata.getValue() == null || metadata.getValue().trim().isEmpty();
					if (emptySrcValue || emptyTextValue) {
						LOG.debug("[WEBSCAN-DOCBOX-SERVICE] El documento {} no tiene informados todos sus metadatos requeridos.", doc.getId());
						String[ ] args = new String[1];
						args[0] = Long.toString(doc.getId());
						String message = messages.getMessage(WebscanDocBoxConstants.MESSAGE_RESULT_DEPOSIT_ERROR_REQUEIRED_METADATA, args, request.getLocale());
						throw new WebscanDocBoxException(WebscanDocBoxException.CODE_402, message);
					}
				}
				if (LOG.isDebugEnabled()) {
					LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Metadato {} del documento {} correcto.", metadataSpec.getName(), doc.getId());
				}
			}
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-DOCBOX-SERVICE] Error verificando la posibilidad de depositar un documento (metadatos requeridos): {}", e.getMessage());
			throw new WebscanDocBoxException(WebscanDocBoxException.CODE_400, e.getMessage(), e);
		} catch (WebscanDocBoxException e) {
			throw e;
		}
		LOG.debug("[WEBSCAN-DOCBOX-SERVICE] Metadatos del documento {} correctos.", doc.getId());
	}

	/**
	 * Comprueba la información de los metadatos del documento tras haber sido
	 * editados manualmente.
	 * 
	 * @param datosRecibidos
	 *            listado de información de metadatos del documento
	 *            seleccionado.
	 * @param docMetadataColl
	 *            relación completa de metadatos, identificados mediante su
	 *            definición, de un documento.
	 * @return Resultado de validación de los metadatos del documento
	 *         seleccionado de la bandeja de documento. Correcto, true.
	 */

	public Boolean checkEditedMetadata(List<DatosDocBoxVO> datosRecibidos, Map<MetadataSpecificationDTO, MetadataDTO> docMetadataColl) {
		Boolean res = Boolean.TRUE;
		MetadataSpecificationDTO metadataSpec;
		String metadataValue;

		LOG.debug("Start CheckMetadata");

		if (datosRecibidos != null && !datosRecibidos.isEmpty()) {

			for (Iterator<MetadataSpecificationDTO> it = docMetadataColl.keySet().iterator(); res && it.hasNext();) {
				metadataSpec = it.next();
				if (metadataSpec.getMandatory()) {
					metadataValue = docMetadataColl.get(metadataSpec).getValue();
					res = hasMetadataSpecNotEmptyValue(metadataSpec, datosRecibidos) || (metadataValue != null && !metadataValue.isEmpty());
				}
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("End CheckMetadata:" + res);
		}
		return res;
	}
	/**
	 * Comprueba la información de los metadatos del documento tras haber sido
	 * editados manualmente.
	 * 
	 * @param datosRecibidos
	 *            listado de información de metadatos del documento
	 *            seleccionado.
	 * @param docMetadataColl
	 *            relación completa de metadatos, identificados mediante su
	 *            definición, de un documento.
	 * @return Resultado de validación de los metadatos del documento
	 *         seleccionado de la bandeja de documento. Correcto, true.
	 */

	public Boolean checkEditedMetadataBatch(List<DocMetadataVO> datosRecibidos, Map<MetadataSpecificationDTO, MetadataDTO> docMetadataColl) {
		Boolean res = Boolean.TRUE;
		MetadataSpecificationDTO metadataSpec;
		String metadataValue;

		LOG.debug("Start CheckMetadata");

		if (datosRecibidos != null && !datosRecibidos.isEmpty()) {

			for (Iterator<MetadataSpecificationDTO> it = docMetadataColl.keySet().iterator(); res && it.hasNext();) {
				metadataSpec = it.next();
				if (metadataSpec.getMandatory()) {
					metadataValue = docMetadataColl.get(metadataSpec).getValue();
					res = hasMetadataSpecBatchNotEmptyValue(metadataSpec, datosRecibidos) || (metadataValue != null && !metadataValue.isEmpty());
				}
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("End CheckMetadata:" + res);
		}
		return res;
	}

	/**
	 * Comprueba si se ha informado un valor para una definción de metadatos.
	 * 
	 * @param metadataSpec
	 *            definición de metadatos.
	 * @param datosRecibidos
	 *            valores informados para cada definición de metadatos.
	 * @return Si la definición de metadatos tiene informado un valor no vacío,
	 *         devuelve true. En caso contrario, false.
	 */
	private Boolean hasMetadataSpecNotEmptyValue(MetadataSpecificationDTO metadataSpec, List<DatosDocBoxVO> datosRecibidos) {
		Boolean res = Boolean.FALSE;
		Boolean found = Boolean.FALSE;
		DatosDocBoxVO datoDoc;
		Long datoMetaSpecId;

		for (Iterator<DatosDocBoxVO> itDatos = datosRecibidos.iterator(); !found && itDatos.hasNext();) {
			datoDoc = itDatos.next();
			try {
				datoMetaSpecId = Long.parseLong(datoDoc.getIdMeta());
				if (metadataSpec.getId().equals(datoMetaSpecId)) {
					res = !isMetadataEmptyValue(metadataSpec, datoDoc.getValor());
					found = Boolean.TRUE;
				}
			} catch (NumberFormatException e) {

			}
		}

		return res;
	}
	/**
	 * Comprueba si se ha informado un valor para una definción de metadatos.
	 * 
	 * @param metadataSpec
	 *            definición de metadatos.
	 * @param datosRecibidos
	 *            valores informados para cada definición de metadatos.
	 * @return Si la definición de metadatos tiene informado un valor no vacío,
	 *         devuelve true. En caso contrario, false.
	 */
	private Boolean hasMetadataSpecBatchNotEmptyValue(MetadataSpecificationDTO metadataSpec, List<DocMetadataVO> datosRecibidos) {
		Boolean res = Boolean.FALSE;
		Boolean found = Boolean.FALSE;
		DocMetadataVO datoDoc;
		Long datoMetaSpecId;

		for (Iterator<DocMetadataVO> itDatos = datosRecibidos.iterator(); !found && itDatos.hasNext();) {
			datoDoc = itDatos.next();
			try {
				datoMetaSpecId = Long.parseLong(datoDoc.getIdMeta());
				if (metadataSpec.getId().equals(datoMetaSpecId)) {
					res = !isMetadataEmptyValue(metadataSpec, datoDoc.getValor());
					found = Boolean.TRUE;
				}
			} catch (NumberFormatException e) {

			}
		}
		return res;
	}

	/**
	 * Comprueba si el valor informado para un metadato es vacío.
	 * 
	 * @param metadataSpec
	 *            definición de metadatos.
	 * @param metadataValue
	 *            valor del metadato.
	 * @return Si es vacío o nulo, devuelve true. En caso contrario, false.
	 */
	private Boolean isMetadataEmptyValue(MetadataSpecificationDTO metadataSpec, String metadataValue) {
		Boolean res = Boolean.FALSE;
		if (metadataSpec.getSourceValues() != null && !metadataSpec.getSourceValues().equals("[]")) {
			// Listas de valores
			res = metadataValue == null || metadataValue.trim().isEmpty() || metadataValue.equals("-1");
		} else {
			// Texto libre
			res = metadataValue == null || metadataValue.trim().isEmpty();
		}

		return res;
	}

	/**
	 * Realiza la persistencia de la información de los datos asociados al
	 * documento indicado de la bandeja de documento.
	 * 
	 * @param docId
	 *            identificador del documento de la bandeja de documento.
	 * @param datosRecibidos
	 *            listado de información de metadatos del documento
	 *            seleccionado.
	 * @param username
	 *            Usuario que lleva a cabo la actualización de metadatos.
	 * @param docMetadataColl
	 *            relación completa de metadatos, identificados mediante su
	 *            definición, de un documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de auditoría, se encuentra cerrada, o se produce algún
	 *             error en la ejecución de las sentencias de BBDD.
	 */
	public void updateDocMetadataCollection(Long docId, List<DatosDocBoxVO> datosRecibidos, String username, Map<MetadataSpecificationDTO, MetadataDTO> docMetadataColl) throws DAOException {
		MetadataSpecificationDTO metadataSpec;
		MetadataDTO metadata;
		Map<MetadataSpecificationDTO, MetadataDTO> updatedMetadataColl = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		ScannedDocumentDTO doc = null;
		LOG.debug("Start updateDocMetadataCollection");

		try {
			doc = webscanModelManager.getDocument(docId);
			// recorremos los datos recibidos
			for (DatosDocBoxVO dat: datosRecibidos) {
				metadataSpec = getDocMetadataSpecByMetaSpecId(Long.valueOf(dat.getIdMeta()), docMetadataColl);
				if (metadataSpec != null) {
					// recuperamos la info del metadata con la especificacion
					metadata = docMetadataColl.get(metadataSpec);
					metadata.setValue(dat.getValor());
					updatedMetadataColl.put(metadataSpec, metadata);
				}
			}
			// realizamos la actualización de los metadatos
			webscanModelManager.updateScannedDocMetadataColl(docId, updatedMetadataColl);
		} catch (DAOException e) {
			// creamos la auditoria y lanzamos exception
			createSetMetadataCollectionAuditLogs(doc, username, e);
			throw e;
		}

		try {
			// creamos la auditoria
			createSetMetadataCollectionAuditLogs(doc, username, null);
		} catch (DAOException e) {
			throw e;
		}

		LOG.debug("End updateDocMetadataCollection");
	}
	/**
	 * Realiza la persistencia de la información de los datos asociados al
	 * documento indicado de la bandeja de documento.
	 * 
	 * @param docId
	 *            identificador del documento de la bandeja de documento.
	 * @param recMetadataCollection
	 *            listado de información de metadatos del documento
	 *            seleccionado.
	 * @param username
	 *            Usuario que lleva a cabo la actualización de metadatos.
	 * @param docMetadataColl
	 *            relación completa de metadatos, identificados mediante su
	 *            definición, de un documento.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             traza de auditoría, se encuentra cerrada, o se produce algún
	 *             error en la ejecución de las sentencias de BBDD.
	 */
	public void updateDocMetadataCollectionBatch(Long docId, List<DocMetadataVO> recMetadataCollection, String username, Map<MetadataSpecificationDTO, MetadataDTO> docMetadataColl) throws DAOException {
		MetadataSpecificationDTO metadataSpec;
		MetadataDTO metadata;
		Map<MetadataSpecificationDTO, MetadataDTO> updatedMetadataColl = new HashMap<MetadataSpecificationDTO, MetadataDTO>();
		ScannedDocumentDTO doc = null;
		LOG.debug("Start updateDocMetadataCollection");

		try {
			doc = webscanModelManager.getDocument(docId);
			// recorremos los datos recibidos
			for (DocMetadataVO dat: recMetadataCollection) {
				metadataSpec = getDocMetadataSpecByMetaSpecId(Long.valueOf(dat.getIdMeta()), docMetadataColl);
				if (metadataSpec != null) {
					// recuperamos la info del metadata con la especificacion
					metadata = docMetadataColl.get(metadataSpec);
					metadata.setValue(dat.getValor());
					updatedMetadataColl.put(metadataSpec, metadata);
				}
			}
			// realizamos la actualización de los metadatos
			webscanModelManager.updateScannedDocMetadataColl(docId, updatedMetadataColl);
		} catch (DAOException e) {
			// creamos la auditoria y lanzamos exception
			createSetMetadataCollectionAuditLogs(doc, username, e);
			throw e;
		}

		try {
			// creamos la auditoria
			createSetMetadataCollectionAuditLogs(doc, username, null);
		} catch (DAOException e) {
			throw e;
		}

		LOG.debug("End updateDocMetadataCollection");
	}


	/**
	 * Función auxiliar para recuperar la especificación de metadatos según el
	 * identificador del metadata especificacion dto.
	 * 
	 * @param metadataSpecId
	 *            identificador de la especificación de metadatos solicitado.
	 * @param docMetaColl
	 *            mapa de las especificaciones de metadatos y su metadatos
	 *            relacionado
	 * @return MetadataSpecificationDTO devuelve la especificación de metadatos
	 *         asociada.
	 */
	private MetadataSpecificationDTO getDocMetadataSpecByMetaSpecId(Long metadataSpecId, Map<MetadataSpecificationDTO, MetadataDTO> docMetaColl) {
		Boolean found = Boolean.FALSE;
		MetadataSpecificationDTO res = null;
		MetadataSpecificationDTO metadataSpec;

		for (Iterator<MetadataSpecificationDTO> it = docMetaColl.keySet().iterator(); !found && it.hasNext();) {
			metadataSpec = it.next();

			if (metadataSpec.getId().equals(metadataSpecId)) {
				res = metadataSpec;
				found = Boolean.TRUE;
			}
		}
		return res;
	}

	/**
	 * Obtiene la relación completa de metadatos, identificados mediante su
	 * definición, de un documento dado.
	 * 
	 * @param docId
	 *            identificador de documento.
	 * @return relación completa de metadatos, identificados mediante su
	 *         definición, del identificador de documento pasado como parámetro
	 *         de entrada.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de las sentencias
	 *             de BBDD.
	 */
	public Map<MetadataSpecificationDTO, MetadataDTO> getCompleteDocumentMetadataCollection(Long docId) throws DAOException {
		Map<MetadataSpecificationDTO, MetadataDTO> res;

		res = webscanModelManager.getCompleteDocumentMetadataCollection(docId);

		return res;
	}

	/**
	 * Obtiene los procesos de digitalización.
	 * 
	 * @return Listado de procesos de digitalización
	 *         organizativas y orgánicas especificas como parámetros de entrada.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */
	public List<BatchVO> getBatchs() throws DAOException {
		
		LOG.debug("Start getScannedDocuments");
		List<BatchVO> res = new ArrayList<BatchVO>();
		long t0;
		long tf;
		BatchVO vo;
		Long numDocs;

		t0 = System.currentTimeMillis();
		// recuperamos los procesos de digitalización 

		List<BatchDTO> mapBatch = webscanModelManager.getBatchs();

		for (BatchDTO batch: mapBatch) {
			numDocs=webscanModelManager.getNumDocsbyBatchId(batch.getId());
			vo = fillBatch(batch,numDocs);
			
			res.add(vo);
		}

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-DOCBOX-SERVICE] Bandeja de documentos - Recuperación de {} documentos realizada en {} ms.", res.size(), (tf - t0));

		LOG.debug("End getScannedDocuments");
		return res;

	}
	/**
	 * Obtiene los procesos de digitalización.
	 * 
	 * @param orgUnitIds
	 *            Relación de unidades organizativas a las que pertenece la
	 *            persona usuaria solicitante.
	 * @param functionalOrgs
	 *            unidades orgánicas a las que pertenece o presta servicio la
	 *            persona usuaria solicitante.
	 * @return Listado de procesos de digitalización
	 *         organizativas y orgánicas especificas como parámetros de entrada.
	 * @throws DAOException
	 *             Exception producida al acceder a la información de BBDD.
	 */
	public List<BatchVO> getBatchs(List<Long> orgUnitIds, String functionalOrgs) throws DAOException {
		
		LOG.debug("Start getScannedDocuments");
		List<BatchVO> res = new ArrayList<BatchVO>();
		long t0;
		long tf;
		BatchVO vo;
		Long numDocs;

		t0 = System.currentTimeMillis();
		// recuperamos los procesos de digitalizaciónactuales por unida organizativa y
		// funcional
//		List<DocBoxScannedDocDTO> mapScannedDoc = webscanModelManager.getDocumentsAndLastLogByOrgUnits(orgUnitIds, functionalOrgs);
		List<BatchDTO> mapBatch = webscanModelManager.getBatchs(orgUnitIds, functionalOrgs);

		for (BatchDTO batch: mapBatch) {
			
			numDocs=webscanModelManager.getNumDocsbyBatchId(batch.getId());
			vo = fillBatch(batch,numDocs);
			
			res.add(vo);
		}

		tf = System.currentTimeMillis();
		LOG.info("[WEBSCAN-DOCBOX-SERVICE] Bandeja de documentos - Recuperación de {} documentos realizada en {} ms.", res.size(), (tf - t0));

		LOG.debug("End getScannedDocuments");
		return res;

	}
	/**
	 * Rellena un objeto documento digitalizado de la capa de presentación a
	 * presentar en la bandeja de documentos.
	 * 
	 * @param scannedDoc
	 *            DTO de información de documento digitalizado a presentar en
	 *            bandeja de documentos.
	 * @param mapMetadatos
	 *            Relación de metadatos.
	 * @param listScannedDocLog
	 *            Lista de trazas de ejecución del documento.
	 * @return objeto documento digitalizado de la capa de presentación.
	 */
	private BatchVO fillBatch(BatchDTO batch,Long aNumDocs) {
		BatchVO res;

		res = new BatchVO();
		// Datos básicos
		
		res.setId(batch.getId());
		res.setBatchNameId(batch.getBatchNameId());
		res.setStartDate(batch.getStartDate());
		res.setUsername(batch.getUsername());
		res.setFunctionalOrgs(batch.getFunctionalOrgs());
		res.setNumDocs(aNumDocs);
		return res;
	}
	/**
	 * Registra traza de auditoría tras la modificación manual de los metadatos
	 * de un documento.
	 * 
	 * @param doc
	 *            documento modificado.
	 * @param username
	 *            usuario que efectúa la modificación.
	 * @param exc
	 *            Excepción.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	private void createSetMetadataCollectionAuditLogs(ScannedDocumentDTO doc, String username, Exception exc) throws DAOException {
		AuditEventDTO auditEvent;
		AuditOperationDTO auditOperation;
		Date now = new Date();
		List<AuditEventDTO> auditEvents = new ArrayList<AuditEventDTO>();
		String additionalInfo;

		if (doc != null) {
			auditOperation = auditService.getAuditOperation(doc.getAuditOpId());
			auditOperation.setLastUpdateDate(now);
			auditOperation.setStatus(AuditOperationStatus.OPEN);
			auditEvent = new AuditEventDTO();

			if (exc != null) {
				// Tamaño máximo del campo 512 caracteres
				additionalInfo = "Error al modificar los metadatos del documento " + doc.getId() + ". Excepción " + exc.getClass().getSimpleName() + ": " + exc.getMessage();
				auditEvent.setEventResult(AuditOpEventResult.ERROR);
			} else {
				additionalInfo = "Actualización de los metadatos del documento " + doc.getId() + " realizada correctamente.";
				auditEvent.setEventResult(AuditOpEventResult.OK);
			}

			if (additionalInfo.length() > WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH) {
				additionalInfo = additionalInfo.substring(0, WebscanModelConstants.AUDIT_ADDITIONAL_INFO_MAX_LENGTH);
			}

			auditEvent.setAdditionalInfo(additionalInfo);
			auditEvent.setEventDate(now);
			auditEvent.setEventName(AuditEventName.SCAN_PROC_METADATA_MANUAL_SET);
			auditEvent.setEventUser(username);
			auditEvents.add(auditEvent);

			auditService.updateAuditLogs(auditOperation, auditEvents);
		}

	}

	/**
	 * Establece el objeto implementa la tarea de asincrona del proceso de
	 * reanudación de digitalización.
	 * 
	 * @param anAsyncScanProcessResumeManager
	 *            objeto implementa la tarea de asincrona del proceso de
	 *            reanudación de digitalización.
	 */
	public void setAsyncScanProcessResumeManager(AsyncScanProcessResumeManager anAsyncScanProcessResumeManager) {
		this.asyncScanProcessResumeManager = anAsyncScanProcessResumeManager;
	}

	/**
	 * Establece un nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Funcion de seteo del plugin provider.
	 * 
	 * @param apluginProvider
	 *            Objeto del servicio proveedor del plugin.
	 */
	public void setPluginProvider(PluginProvider apluginProvider) {
		this.pluginProvider = apluginProvider;
	}

	/**
	 * Funcion de seteo de auditService.
	 * 
	 * @param aauditService
	 *            Objeto del servicio de auditoria.
	 */

	public void setAuditService(AuditService aauditService) {
		this.auditService = aauditService;
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Objeto que recopila los mensajes, sujetos a multidioma, que
	 *            son presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}

	/**
	 * Establece la clase de utilidades para plugins sobre el modelo de datos
	 * del Sistema.
	 * 
	 * @param aPluginDataModelUtilities
	 *            utilidades para plugins sobre el modelo de datos del Sistema.
	 */
	public void setPluginDataModelUtilities(PluginDataModelUtilities aPluginDataModelUtilities) {
		this.pluginDataModelUtilities = aPluginDataModelUtilities;
	}
	
	/**
	 * Método de obtención de los estados de un documento digitalizado
	 * 
	 * @param docId
	 *            Identificador de un documento digitalizado
	 * @return Estados de un documento digitalizado.
	 */
	public ResultStatusContentVO getStatusContent(Long docId) throws DAOException {
		Integer order = 1;
		ResultStatusContentVO result = new ResultStatusContentVO();
		List<AuditEventDTO> events = new ArrayList<AuditEventDTO>();
		ScannedDocumentDTO doc;
		List<ResultStatusContent> resStatusCont = new ArrayList<ResultStatusContent>();
		Boolean actionRecovery = Boolean.FALSE;
		ResultStatusContent rsc;
		List<ScannedDocLogDTO> scannedDocLogs;
		ScannedDocLogDTO scannedDocLog;

		doc = webscanModelManager.getDocument(docId);
		scannedDocLogs = webscanModelManager.getScannedDocLogsByDocId(docId);
		events = webscanModelManager.getAuditEventsByOperation(doc.getAuditOpId());

		result.setAuditOperationId(doc.getAuditOpId());
		result.setDocumentId(docId);
		result.setBatchId(doc.getBatchId().getId());
		
		for (AuditEventDTO event: events) {
			rsc = new ResultStatusContent();

			rsc.setStage(event.getEventName().name());
			rsc.setEventName(event.getEventName().name().toString());
			rsc.setOrder(order);
			rsc.setEventData(event.getAdditionalInfo());
			rsc.setEventResult(event.getEventResult().getResult());
			rsc.setCheck(AuditOpEventResult.OK.getResult().equals(event.getEventResult().getResult()));
			rsc.setEventDate(event.getEventDate());
			order++;
			resStatusCont.add(rsc);
		}

		for (Iterator<ScannedDocLogDTO> it = scannedDocLogs.iterator(); !actionRecovery && it.hasNext();) {
			scannedDocLog = it.next();

			actionRecovery = OperationStatus.FAILED.equals(scannedDocLog.getStatus());
		}

		result.setStatusContents(resStatusCont);
		result.setActionRecovery(actionRecovery);

		return result;
	}
	
	/**
	 * Método de obtencion de la lista de documentos digitalizados de la vista
	 * de validacion de digitalización rápida.
	 * 
	 * @param username
	 *            Nombre de usuario del usuario logado en sesión.
	 * @param userId
	 *            Documento de identificación del usuario logado en sesión.
	 * @param locale
	 *            configuración regional.
	 * @return Listado de documentos digitalizados de la vista de validacion de
	 *         digitalización rápida.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la consulta
	 *             sobre el modelo de datos.
	 * @throws WebscanDocBoxException
	 *             Si se detecta que existen documentos pertenecientes a
	 *             diferentes lotes (carátula, reserva o autónomo).
	 */
	public List<ScannedDocumentDTO> getScannedDocumentsByBatch(Long batchId) throws DAOException, WebscanDocBoxException {
		List<ScannedDocumentDTO> docs = new ArrayList<ScannedDocumentDTO>();

		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Inicio getValidateViewDocuments");
		}

		docs = webscanModelManager.getActiveDocumentsByBatchId(batchId);

		
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Fin getValidateViewDocuments");
		}

		return docs;
	}
	
	/**
	 * Actualiza, de forma transaccional, el registro de trazas de auditoría
	 * tras fallar la ejecución el plugin de depósito sobre una lista de
	 * documentos.
	 * 
	 * @param docs
	 *            Documentos digitalizados.
	 * @param lastScannedDocLogDto
	 *            última traza de ejecución del proceso de digigtalización del
	 *            documento.
	 * @param username
	 *            Nombre de usuario del usuario que solicita el depósito del
	 *            documento.
	 * @param exc
	 *            Excepción.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	public void performErrorDocsDepositLogs(List<ScannedDocumentDTO> docs, String username, Exception exc) throws DAOException {

		if (docs != null && !docs.isEmpty()) {
			for (ScannedDocumentDTO doc: docs) {
				createDepositAuditLogs(doc, username, exc);
			}
		}
	}

	/**
	 * Método de deposito de documentos digitalizados
	 * 
	 * @param batchDocs
	 *            Relación de documentos de un proceso de digitalización.
	 * @param request
	 *            Petición http.
	 * @param username
	 *            Nombre de usuario del usuario solicitante.
	 * @param userDocument
	 *            Documento de identificación del usuario solicitante.
	 * @return Resultado de la acción a mostrar en la interfaz gráfica de
	 *         usuario.
	 * @throws WebscanDocBoxException
	 *             Si se produce algún error en el depósito de los documentos.
	 */
	public DocBoxActionResult storeAllDocuments(List<ScannedDocumentDTO> batchDocs, HttpServletRequest request, String username, String userDocument) throws WebscanDocBoxException {
		
		Long docId;
		Map<ScannedDocumentDTO, ScannedDocLogDTO> docsToStore = new HashMap<ScannedDocumentDTO, ScannedDocLogDTO>();
		ScannedDocLogDTO lastScannedDocLog;
		ScanProfileOrgUnitDTO scanProfileOrgUnit;
		ScanProfileOrgUnitDTO auxScanProfileOrgUnit;
		String result = null;
		String[ ] args = new String[2];
		DocBoxActionResult res = new DocBoxActionResult();
		 
		LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Start storeAllDocuments");
		try {
			
			if (batchDocs.isEmpty()) {
				throw new WebscanDocBoxException(WebscanDocBoxException.CODE_019, messages.getMessage("docBox.error.storeAllDocuments.numDocBatch", null, request.getLocale()));
			}

			scanProfileOrgUnit = webscanModelManager.getScanProfileOrgUnitByDoc(batchDocs.get(0).getId());
			for (ScannedDocumentDTO doc: batchDocs) {
				lastScannedDocLog = null;
				docId = doc.getId();
				auxScanProfileOrgUnit = webscanModelManager.getScanProfileOrgUnitByDoc(docId);
				// verificamos la validez de los documentos
				// Se confirma que todos poseen el mismo perfil de
				// digitalización, de forma que se garantice que todos
				// emplearan
				// el mismo plugin de depósito
				if (!scanProfileOrgUnit.equals(auxScanProfileOrgUnit)) {
					throw new WebscanDocBoxException(WebscanDocBoxException.CODE_037, messages.getMessage("docBox.error.validateProfileDocs", null, request.getLocale()));
				}

				try {
					LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Se recupera la última traza de ejecución del documento a depositar {}.", docId);
					lastScannedDocLog = getLastScannedDocLogsByDocId(docId);

					// comprobamos que se pueda realizar el deposito
					LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Se verifica que el documento {} puede ser depositado.", docId);
					checkPerformStoreDocument(doc, lastScannedDocLog, request);
					docsToStore.put(doc, lastScannedDocLog);
				} catch (DAOException e) {
					result = "docBox.error.storeAllDocuments";
					args[WebscanDocBoxConstants.MESSAGE_POS_DOC_ID] = doc.getName();
					args[WebscanDocBoxConstants.MESSAGE_POS_DOC_DEPOSIT] = e.getMessage();
					res.setErrorHappen(Boolean.TRUE);
					res.getErrorMessages().add(messages.getMessage(result, args, request.getLocale()));
				} catch (WebscanDocBoxException e) {
					result = "docBox.error.storeAllDocuments";
					args[WebscanDocBoxConstants.MESSAGE_POS_DOC_ID] = doc.getName();
					args[WebscanDocBoxConstants.MESSAGE_POS_DOC_DEPOSIT] = e.getMessage();

					try {
						performErrorDocDepositLogs(doc, username, e);
					} catch (DAOException ex) {
						LOG.error("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Error creando trazas de auditoria por error en el depósito del documento {}: {}", docId, ex.getMessage());
					}

					res.setErrorHappen(Boolean.TRUE);
					res.getErrorMessages().add(messages.getMessage(result, args, request.getLocale()));
				}
			}

			if (!res.getErrorHappen()) {
				// procedemos a realizar el deposito, para ello necesitamos
				// recuperar el
				// plugin de deposito procedemos a realizar el deposito
				performStoreDocs(docsToStore, username);

				// creamos el mensaje
				result = messages.getMessage("docBox.storeAllDocuments.ok", null, request.getLocale());
				res.getErrorMessages().add(result);
			}

		} catch (DAOException e) {
			throw new WebscanDocBoxException(WebscanDocBoxException.CODE_025, messages.getMessage(WebscanDocBoxConstants.DOCBOX_ERROR_DATA_BASE, null, request.getLocale()));
		} catch (WebscanDocBoxException e) {
			switch (e.getCode()) {
				case WebscanDocBoxException.CODE_019:
				case WebscanDocBoxException.CODE_025:
				case WebscanDocBoxException.CODE_026:
				case WebscanDocBoxException.CODE_027:
				case WebscanDocBoxException.CODE_028:
				case WebscanDocBoxException.CODE_029:
				case WebscanDocBoxException.CODE_030:
				case WebscanDocBoxException.CODE_037:
				case WebscanDocBoxException.CODE_046:
					throw e;
				default:
					args = new String[2];
					args[0] = e.getMessage();
					args[1] = e.getCode();
					throw new WebscanDocBoxException(WebscanDocBoxException.CODE_038, messages.getMessage("docBox.label.error.storeDocs", args, request.getLocale()));
			}
		}

		LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] End storeAllDocuments");

		return res;
	}
	/**
	 * Cancela el proceso de digtalización eliminando suss documentos.
	 * 
	 * @param batchDocs
	 *            Nombre de usuario del usuario logado en sesión.
	 * @param username
	 *            Nombre de usuario del usuario logado en sesión.
	 * @param userId
	 *            Documento de identificación del usuario logado en sesión.
	 * @param locale
	 *            configuración regional.
	 * @throws WebscanDocBoxException
	 *             Si los parámetros de entrada no son válidos, se produce algún
	 *             error en el registro de las trazas de auditoría, o se produce
	 *             algún error en la ejecución de las sentencias de BBDD.
	 */

	public Integer cancelScanWork(List<ScannedDocumentDTO> batchDocs, String username, String userId, Locale locale) throws WebscanDocBoxException {
		Integer res = 0;
		List<Long> docIds = new ArrayList<Long>();
		LOG.debug("Start cancelScanWork");

		try {
			LOG.debug("Eliminando los documentos del proceso de digitalización rápida activo para el usuario {}", username);

			// Se forma la lista de identificadores de documentos a eliminar
			for (ScannedDocumentDTO sd: batchDocs) {
				docIds.add(sd.getId());
			}

			// Se eliminan los documentos
			webscanModelManager.removeScannedDocs(docIds);

			// Se registran trazas de auditoria
			LOG.debug("Registrando trazas de auditoría por cancelación del proceso de digitalización para el usuario {}", username);
			for (ScannedDocumentDTO sd: batchDocs) {
				createRemoveAuditLogs(sd, username);
			}

			res = batchDocs.size();
		} catch (DAOException e) {
			throw new WebscanDocBoxException(WebscanDocBoxException.CODE_025, e.getMessage(), e);
		} 

		LOG.debug("End cancelScanWork");

		return res;
	}
	
	/**
	 * Realiza el deposito de un documento digitalizado, verificando previamente
	 * que es posible llevarlo a cabo.
	 * 
	 * @param docsToStore
	 *            Relación de documentos a depositar, junto a su última traza de
	 *            ejecución.
	 * @param username
	 *            Identificador del nombre del usuario.
	 * @param batchReqId
	 *            Identificador internode carátula o reserva asociado al lote de
	 *            documentos. En caso de digitalización anónima, este parámetro
	 *            será null.
	 * @param batchType
	 *            Tipo de lo te de documentos.
	 * @throws DocBoxException
	 *             Si se produce un error al efectuar el depósito del documento.
	 */
	private void performStoreDocs(Map<ScannedDocumentDTO, ScannedDocLogDTO> docsToStore, String username) throws WebscanDocBoxException {
		List<Long> docIds = new ArrayList<Long>();
		Map<ScannedDocumentDTO, ScannedDocLogDTO> updateDocsToStore = new HashMap<ScannedDocumentDTO, ScannedDocLogDTO>();
		Plugin plug;
		ScannedDocumentDTO doc;
		ScannedDocLogDTO lastScannedDocLog;
		ScannedDocLogDTO docLog;
		LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Se inicia el depósito de los documentos ...");
		// procedemos a realizar el deposito, para ello necesitamos
		// recuperar el
		// plugin de deposito procedemos a realizar el deposito
		// ejecutamos el plugin de deposito de documentos

		try {

			// recuperamos la relacion de perfil de digitalización y unidad
			// organizativa
			ScanProfileOrgUnitDTO scanProfileOrgUnit = webscanModelManager.getScanProfileOrgUnitByDoc(docsToStore.keySet().iterator().next().getId());
			// recuperamos la especificación del perfil de digitalizacion
			ScanProfileDTO scanProfile = webscanModelManager.getScanProfile(scanProfileOrgUnit.getScanProfile());
			// recuperamos el map con las etapas y los plugins asociados
			Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> mapStagePlugin = webscanModelManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfile.getSpecification());
			LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Recuperamos la informacion de base de datos que necesitamos");
			PluginSpecificationDTO pluginSpecification = webscanModelManager.getPluginSpecByScanProfileOrgUnitAndStage(scanProfileOrgUnit.getId(), WebscanBaseConstants.STORE_STAGE_NAME);
			ScanProfileSpecScanProcStageDTO scanProfileSpecScanProcStage = pluginDataModelUtilities.getScanProfileSpecScanProcStageByScanProfileSpecIdAndScanProcStageId(mapStagePlugin, scanProfile.getSpecification(), WebscanBaseConstants.STORE_STAGE_NAME);

			// Se forma la lista de identificadores de los documentos que serán
			// depósitados, comprobándose la traza de ejecución del documento
			for (Iterator<ScannedDocumentDTO> it = docsToStore.keySet().iterator(); it.hasNext();) {
				doc = it.next();
				docLog = docsToStore.get(doc);
				LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Validamos la última traza del documento");
				// validamos si la última fase ejecutada es reanudable y si no
				// está
				// con error
				// si operation estatus es performed, significa que la ultima ha
				// sido metadata
				if (OperationStatus.PERFORMED.equals(docLog.getStatus())) {
					lastScannedDocLog = new ScannedDocLogDTO();
					lastScannedDocLog.setStartDate(new Date());
					lastScannedDocLog.setScanProcessStageName(WebscanBaseConstants.STORE_STAGE_NAME);
					lastScannedDocLog.setStatus(OperationStatus.IN_PROCESS);
					// creamos nueva traza
					Long scannedDocLogDtoId = webscanModelManager.createScannedDocLog(lastScannedDocLog, doc.getId(), scanProfileSpecScanProcStage.getScanProcessStageId()); // IN_PROCESS
					LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Creada la traza de ejcución {} para la fase {}.", scannedDocLogDtoId, WebscanBaseConstants.STORE_STAGE_NAME);
					lastScannedDocLog = webscanModelManager.getLastScannedDocLogsByDocId(doc.getId());
				} else {
					// está en error, obtengo el último
					lastScannedDocLog = docLog;
					lastScannedDocLog.setStatus(OperationStatus.IN_PROCESS);
					webscanModelManager.setStatusScannedDocLog(lastScannedDocLog);
				}
				docIds.add(doc.getId());
				updateDocsToStore.put(doc, lastScannedDocLog);
			}

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Obtenemos el  plugin:" + pluginSpecification.getName());
			}
			// obtenemos el plugin
			plug = pluginProvider.getDepositPlugin(pluginSpecification.getName());
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Inicializamos plugin:" + pluginSpecification.getName() + " - Fase:" + scanProfileSpecScanProcStage.getScanProcessStage());
			}
			// inicializamos el plugin
			plug.initialize(scanProfileSpecScanProcStage.getScanProcessStageId(), docIds);

			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Ejecutamos plugin:" + pluginSpecification.getName());
			}
			// ejecutamos el plugin
			plug.performAction();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Fin ejecución plugin:" + pluginSpecification.getName());
			}

			// Se actualiza la traza de ejecución de los documentos depositados
			performDocDepositLogs(updateDocsToStore, username);

			// Se finaliza la carátula o reserva asociada al lote de documentos.
//			completeBatchReq(batchReqId, batchType);
		} catch (DAOException e) {
			// auditoria se registra error
			LOG.error("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Error al depositar el lote de documentos: {}", e.getMessage());
			throw new WebscanDocBoxException(WebscanDocBoxException.CODE_025, e.getMessage(), e);
		} catch (PluginException e) {
			LOG.error("[WEBSCAN-GV-FAST-SCANDOCS-SERV] Error al depositar el lote de documentos: {}", e.getMessage());
			throw new WebscanDocBoxException(WebscanDocBoxException.CODE_028, e.getMessage(), e);
		}

	}

	/**
	 * LLeva a cabo, de forma transaccional, el registro de trazas de ejecución
	 * y auditoría tras ejecutar el plugin de depósito sobre un conjunto de
	 * documentos, así como su borrado del sistema.
	 * 
	 * @param docsToStore
	 *            Relación de documentos a depositar, junto a su última traza de
	 *            ejecución.
	 * @param username
	 *            Nombre de usuario del usuario que solicita el depósito del
	 *            documento.
	 * @throws DAOException
	 *             Si se produce algún error en el acceso al modelo de datos del
	 *             sistema.
	 */
	private void performDocDepositLogs(Map<ScannedDocumentDTO, ScannedDocLogDTO> docsToStore, String username) throws DAOException {
		List<Long> docIds = new ArrayList<Long>();
		Date now = new Date();
		ScannedDocumentDTO doc;
		ScannedDocLogDTO docLog;

		for (Iterator<ScannedDocumentDTO> it = docsToStore.keySet().iterator(); it.hasNext();) {
			// Se actualizan y crean trazas de ejecución y auditoría de los
			// documentos depositados.
			doc = it.next();
			// Traza de ejecución
			docLog = docsToStore.get(doc);
			docLog.setStatus(OperationStatus.PERFORMED);
			docLog.setEndDate(now);
			webscanModelManager.closeScannedDocLog(docLog);
			// Traza de auditoria
			createDepositAuditLogs(doc, username, null);
			docIds.add(doc.getId());
		}

		// Se eliminan los documentos depositados del sistema
		webscanModelManager.removeScannedDocs(docIds);
	}

}
