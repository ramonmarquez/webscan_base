/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.scanProcess.controller.vo.DocBoxVO.java.</p>
 * <b>Descripción:</b><p> Información de un documento digitalizado en la capa de presentación.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox.controller.vo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import es.ricoh.webscan.model.domain.SignatureType;
import es.ricoh.webscan.model.dto.MetadataDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.utilities.MimeType;

/**
 * Información de un documento digitalizado en la capa de presentación.
 * <p>
 * Class DocBoxVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class DocBoxVO {

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación del documento.
	 */
	private String name;

	/**
	 * Instante en el que se inicio el proceso de digitalización del documento.
	 */
	private Date digProcStartDate;

	/**
	 * Instante en el que finalizó el proceso de digitalización del documento.
	 */
	private Date digProcEndDate;

	/**
	 * Identificador de lote de documentos.
	 */
	private String batchNameId;

	/**
	 * Identificador de lote de documentos.
	 */
	private Long batchId;

	/**
	 * Número de orden del documento en el lote, comenzando por 1.
	 */
	private Integer orderNum;

	/**
	 * Tipo MIME de la imagen capturada del documento.
	 */
	private MimeType mimeType;

	/**
	 * Tipo o formato de firma electrónica del documento digitalizado.
	 */
	private SignatureType signatureType;

	/**
	 * Nombre de usuario del usuario que inicia el proceso de digitalización del
	 * documento.
	 */
	private String username;

	/**
	 * Lista de identificadores de unidades orgánicas a las que pertenece el
	 * usuario que inicia el proceso de digitalización del documento (separados
	 * por coma).
	 */
	private String functionalOrgs;

	/**
	 * Identificador de la traza de auditoria asociada al proceso de
	 * digitalización del documento.
	 */
	private Long auditOpId;

	/**
	 * Configuración de especificación de documento, perfil de digitalización y
	 * unidad organizativa empleada en el proceso de digitalización.
	 */
	private Long orgScanProfileDocSpec;

	/**
	 * Última traza de ejecución del proceso de digitalización del documento.
	 */
	private DocBoxLogVO lastScannedDocLog;

	/**
	 * Relación de metadatos del documento.
	 */
	private Map<MetadataSpecificationDTO, MetadataDTO> mapMetadatos = new HashMap<MetadataSpecificationDTO, MetadataDTO>();

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación del documento.
	 * 
	 * @return la denominación del documento.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Establece un nuevo valor para la denominación del documento.
	 * 
	 * @param aName
	 *            nuevo valor para la denominación del documento.
	 */
	public void setName(String aName) {
		this.name = aName;
	}

	/**
	 * Obtiene el tipo MIME de la imagen capturada del documento.
	 * 
	 * @return el tipo MIME de la imagen capturada del documento.
	 */
	public MimeType getMimeType() {
		return mimeType;
	}

	/**
	 * Establece un nuevo tipo MIME de la imagen capturada del documento.
	 * 
	 * @param aMimeType
	 *            nuevo valor del tipo MIME de la imagen capturada del
	 *            documento.
	 */
	public void setMimeType(MimeType aMimeType) {
		this.mimeType = aMimeType;
	}

	/**
	 * Obtiene el instante en el que se inicio el proceso de digitalización del
	 * documento.
	 * 
	 * @return el instante en el que se inicio el proceso de digitalización del
	 *         documento.
	 */
	public Date getDigProcStartDate() {
		return digProcStartDate;
	}

	/**
	 * Establece un nuevo instante en el que se inicio el proceso de
	 * digitalización del documento.
	 * 
	 * @param aDigProcStartDate
	 *            nuevo instante en el que se inicio el proceso de
	 *            digitalización del documento.
	 */
	public void setDigProcStartDate(Date aDigProcStartDate) {
		this.digProcStartDate = aDigProcStartDate;
	}

	/**
	 * Obtiene el instante en el que se finalizó el proceso de digitalización
	 * del documento.
	 * 
	 * @return el instante en el que se finalizó el proceso de digitalización
	 *         del documento.
	 */
	public Date getDigProcEndDate() {
		return digProcEndDate;
	}

	/**
	 * Establece un nuevo instante en el que se finalizó el proceso de
	 * digitalización del documento.
	 * 
	 * @param aDigProcEndDate
	 *            nuevo instante en el que se finalizó el proceso de
	 *            digitalización del documento.
	 */
	public void setDigProcEndDate(Date aDigProcEndDate) {
		this.digProcEndDate = aDigProcEndDate;
	}

	/**
	 * Obtiene el nombre del identificador de lote de documentos al que pertenece el
	 * documento digitalizado.
	 * 
	 * @return nombre del identificador de lote de documentos al que pertenece el
	 *         documento digitalizado.
	 */
	public String getBatchNameId() {
		return batchNameId;
	}

	/**
	 * Establece un nuevo nombre del identificador de lote de documentos al que pertenece
	 * el documento digitalizado.
	 * 
	 * @param aBatchNameId
	 *            nuevo nombre delidentificador de lote de documentos al que pertenece el
	 *            documento digitalizado.
	 */
	public void setBatchNameId(String aBatchNameId) {
		this.batchNameId = aBatchNameId;
	}

	/**
	 * Obtiene el identificador de lote de documentos al que pertenece el
	 * documento digitalizado.
	 * 
	 * @return el identificador de lote de documentos al que pertenece el
	 *         documento digitalizado.
	 */
	public Long getBatchId() {
		return batchId;
	}

	/**
	 * Establece un nuevo identificador de lote de documentos al que pertenece
	 * el documento digitalizado.
	 * 
	 * @param aBatchId
	 *            nuevo identificador de lote de documentos al que pertenece el
	 *            documento digitalizado.
	 */
	public void setBatchId(Long aBatchId) {
		this.batchId = aBatchId;
	}

	/**
	 * Obtiene el número de orden del documento en el lote al que pertenece.
	 * 
	 * @return el número de orden del documento en el lote al que pertenece.
	 */
	public Integer getOrderNum() {
		return orderNum;
	}

	/**
	 * Establece el número de orden del documento en el lote al que pertenece.
	 * 
	 * @param anOrderNum
	 *            número de orden del documento en el lote al que pertenece.
	 */
	public void setOrderNum(Integer anOrderNum) {
		this.orderNum = anOrderNum;
	}

	/**
	 * Obtiene el tipo o formato de la firma electrónica del documento.
	 * 
	 * @return el tipo o formato de la firma electrónica del documento.
	 */
	public SignatureType getSignatureType() {
		return signatureType;
	}

	/**
	 * Establece un nuevo tipo o formato de la firma electrónica del documento.
	 * 
	 * @param aSignatureType
	 *            nuevo tipo o formato de la firma electrónica del documento.
	 */
	public void setSignatureType(SignatureType aSignatureType) {
		this.signatureType = aSignatureType;
	}

	/**
	 * Obtiene el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @return el nombre de usuario del usuario que inicia el proceso de
	 *         digitalización del documento.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @param anUsername
	 *            nuevo nombre de usuario del usuario que inicia el proceso de
	 *            digitalización del documento.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento.
	 * 
	 * @return lista de identificadores de unidades orgánicas a las que
	 *         pertenece el usuario que inicia el proceso de digitalización del
	 *         documento.
	 */
	public String getFunctionalOrgs() {
		return functionalOrgs;
	}

	/**
	 * Establece la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento.
	 * 
	 * @param anyFunctionalOrgs
	 *            nueva lista de identificadores de unidades orgánicas a las que
	 *            pertenece el usuario que inicia el proceso de digitalización
	 *            del documento.
	 */
	public void setFunctionalOrgs(String anyFunctionalOrgs) {
		this.functionalOrgs = anyFunctionalOrgs;
	}

	/**
	 * Obtiene el identificador de la traza de auditoria asociada al proceso de
	 * digitalización del documento.
	 * 
	 * @return el identificador de la traza de auditoria asociada al proceso de
	 *         digitalización del documento.
	 */
	public Long getAuditOpId() {
		return auditOpId;
	}

	/**
	 * Establece el identificador de la traza de auditoria asociada al proceso
	 * de digitalización del documento.
	 * 
	 * @param anAuditOpId
	 *            nuevo identificador de traza de auditoria asociada al proceso
	 *            de digitalización del documento.
	 */
	public void setAuditOpId(Long anAuditOpId) {
		this.auditOpId = anAuditOpId;
	}

	/**
	 * Obtiene la configuración de especificación de documento, perfil de
	 * digitalización y unidad organizativa empleada en el proceso de
	 * digitalización del documento.
	 * 
	 * @return la configuración de especificación de documento, perfil de
	 *         digitalización y unidad organizativa empleada en el proceso de
	 *         digitalización del documento.
	 */
	public Long getOrgScanProfileDocSpec() {
		return orgScanProfileDocSpec;
	}

	/**
	 * Establece la configuración de especificación de documento, perfil de
	 * digitalización y unidad organizativa empleada en el proceso de
	 * digitalización del documento.
	 * 
	 * @param anOrgScanProfileDocSpec
	 *            nueva configuración de especificación de documento, perfil de
	 *            digitalización y unidad organizativa empleada en el proceso de
	 *            digitalización del documento.
	 */
	public void setOrgScanProfileDocSpec(Long anOrgScanProfileDocSpec) {
		this.orgScanProfileDocSpec = anOrgScanProfileDocSpec;
	}

	/**
	 * Devuelve la última traza de ejecución asociada al documento escaneado.
	 * 
	 * @return la última traza de ejecución asociada al documento escaneado..
	 */

	public DocBoxLogVO getLastScannedDocLog() {
		return lastScannedDocLog;
	}

	/**
	 * Modifica las trazas asociadas al documento escaneado.
	 * 
	 * @param aScannedDocLog
	 *            Listado de trazas del documento escaneado.
	 */
	public void setLastScannedDocLog(DocBoxLogVO aScannedDocLog) {
		this.lastScannedDocLog = aScannedDocLog;
	}

	/**
	 * Devuelve el ultimo evento asociado al documento escaneado.
	 * 
	 * @return ScannedDocLogDTO ultimo evento
	 */
	/*public ScannedDocLogDTO getLastScannedDocLog() {
		ScannedDocLogDTO res = null;
		if (listScannedDocLog != null && !listScannedDocLog.isEmpty()) {
			res = listScannedDocLog.get(listScannedDocLog.size() - 1);
		}
		return res;
	}*/

	/**
	 * Devuelve el ultimo evento asociado al documento escaneado.
	 * 
	 * @return Denominación de la última fase ejecutada para el documento
	 *         escaneado.
	 */
	/*public String getLastScannedDocLogString() {
		ScannedDocLogDTO lastScannedDocLog;
		String res = null;
		lastScannedDocLog = getLastScannedDocLog();
	
		if (lastScannedDocLog != null) {
			res = lastScannedDocLog.getScanProcessStageName();
		}
	
		return res;
	}*/

	/**
	 * Devuelve el estado del ultimo evento asociado al documento escaneado.
	 * 
	 * @return Estado de la última fase ejecutada para el documento escaneado.
	 */
	/*public String getLastScannedDocLogStatusString() {
		ScannedDocLogDTO lastScannedDocLog;
		String res = null;
		lastScannedDocLog = getLastScannedDocLog();
		if (lastScannedDocLog != null) {
			res = lastScannedDocLog.getStatus().name();
		}
	
		return res;
	}*/

	/**
	 * Gets the value of the attribute {@link #mapMetadatos}.
	 * 
	 * @return the value of the attribute {@link #mapMetadatos}.
	 */
	public Map<MetadataSpecificationDTO, MetadataDTO> getMapMetadatos() {
		return mapMetadatos;
	}

	/**
	 * Sets the value of the attribute {@link #mapMetadatos}.
	 * 
	 * @param aMapMetadatos
	 *            The value for the attribute {@link #mapMetadatos}.
	 */
	public void setMapMetadatos(Map<MetadataSpecificationDTO, MetadataDTO> aMapMetadatos) {
		this.mapMetadatos.clear();

		if (aMapMetadatos != null && !aMapMetadatos.isEmpty()) {
			this.mapMetadatos.putAll(aMapMetadatos);
		}
	}

}
