/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 - 2019 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.gva.webscangv.fastscandoc.contr.service.BatchType.java.</p>
* <b>Descripción:</b><p> Tipos de lotes de documentos admitidos por el sistema.</p>
* <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
*/
package es.ricoh.webscan.docbox.service;

/**
 * Tipos de lotes de documentos admitidos por el sistema.
 * <p>
 * Clase BatchType.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan GV - Plataforma de digitalización certificada y copia auténtica de la
 * Generalitat Valenciana.
 * </p>
 * 
 * @version 1.0.
 */
public enum BatchType {
	/**
	 * Lote de documentos por reserva.
	 */
	BOOKING,
	/**
	 * Lote de documentos por carátula.
	 */
	SPACER,
	/**
	 * Lote de documentos por defecto.
	 */
	DEFAULT;
}
