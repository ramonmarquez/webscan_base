/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.controller.vo.listDatosDocBoxVO.java.</p>
 * <b>Descripción:</b><p> .</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox.controller.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Entidad utilizada para recuperar los datos de la vista del buzon de
 * documentos.
 * <p>
 * Class ListDatosDocBoxVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ListDatosDocBoxVO implements Serializable {

	/**
	 * Attribute that represents el serial de la clase .
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * listado de map con la información asociada a los datos de los documentos
	 * del buzon.
	 */
	private List<Map<String, String>> datos;

	/**
	 * Gets the value of the attribute {@link #datos}.
	 * 
	 * @return the value of the attribute {@link #datos}.
	 */
	public List<Map<String, String>> getDatos() {
		return datos;
	}

	/**
	 * Sets the value of the attribute {@link #datos}.
	 * 
	 * @param adatos
	 *            The value for the attribute {@link #datos}.
	 */
	public void setDatos(List<Map<String, String>> adatos) {
		this.datos = adatos;
	}

}
