/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.scanProcess.controller.vo.DocBoxVO.java.</p>
 * <b>Descripción:</b><p> Información de un documento digitalizado en la capa de presentación.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox.controller.vo;

import java.util.Date;

/**
 * Información de un proceso de digitalización en la capa de presentación.
 * <p>
 * Class DocBoxVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 3.0.
 */
public class BatchVO {

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación del documento.
	 */
	private String batchNameId;

	/**
	 * Fecha de inicio del proceso de digitalización.
	 */
	private Date startDate;

	/**
	 * Fecha de última actualización del proceso de digitalización .
	 */
	private Date updateDate;

	/**
	 * Nombre de usuario del usuario que inicia el proceso de digitalización del
	 * documento.
	 */
	private String username;

	/**
	 * Lista de identificadores de unidades orgánicas a las que pertenece el
	 * usuario que inicia el proceso de digitalización del documento (separados
	 * por coma).
	 */
	private String functionalOrgs;
	
	/**
	 * Número de documentos para el proceso de digitalización.
	 */
	private Long numDocs;

	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anId
	 *            nuevo valor para el identificador de la entidad.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación del documento.
	 * 
	 * @return la denominación del documento.
	 */
	public String getBatchNameId() {
		return batchNameId;
	}

	/**
	 * Establece un nuevo valor para la denominación del documento.
	 * 
	 * @param aBatchNameId
	 *            nuevo valor para la denominación del documento.
	 */
	public void setBatchNameId(String aBatchNameId) {
		this.batchNameId = aBatchNameId;
	}

	/**
	 * Obtiene la fecha de inicio del proceso de digitalización.
	 * 
	 * @return fecha de inicio del proceso de digitalización.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Establece la fecha de inicio del proceso de digitalización.
	 * 
	 * @param aStartDate
	 *            nueva fecha de inicio del proceso de digitalización.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}

	/**
	 * Obtiene la fecha de la última actualización del proceso de digitalización.
	 * 
	 * @return la fecha de la última actualización del proceso de digitalización.
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Establece la fecha de última actualización del proceso de digitalización.
	 * 
	 * @param aUpdateDate
	 *            fecha de última actualización del proceso de digitalización.
	 */
	public void setUpdateDate(Date aUpdateDate) {
		this.updateDate = aUpdateDate;
	}

	/**
	 * Obtiene el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @return el nombre de usuario del usuario que inicia el proceso de
	 *         digitalización del documento.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Establece el nombre de usuario del usuario que inicia el proceso de
	 * digitalización del documento.
	 * 
	 * @param anUsername
	 *            nuevo nombre de usuario del usuario que inicia el proceso de
	 *            digitalización del documento.
	 */
	public void setUsername(String anUsername) {
		this.username = anUsername;
	}

	/**
	 * Obtiene la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento.
	 * 
	 * @return lista de identificadores de unidades orgánicas a las que
	 *         pertenece el usuario que inicia el proceso de digitalización del
	 *         documento.
	 */
	public String getFunctionalOrgs() {
		return functionalOrgs;
	}

	/**
	 * Establece la lista de identificadores de unidades orgánicas a las que
	 * pertenece el usuario que inicia el proceso de digitalización del
	 * documento.
	 * 
	 * @param anyFunctionalOrgs
	 *            nueva lista de identificadores de unidades orgánicas a las que
	 *            pertenece el usuario que inicia el proceso de digitalización
	 *            del documento.
	 */
	public void setFunctionalOrgs(String anyFunctionalOrgs) {
		this.functionalOrgs = anyFunctionalOrgs;
	}
	/**
	 * Obtiene el identificador de la entidad.
	 * 
	 * @return el identificador de la entidad.
	 */

	public Long getNumDocs() {
		return numDocs;
	}

	/**
	 * Establece un nuevo valor para el identificador de la entidad.
	 * 
	 * @param anNumDocs
	 *            nuevo valor para el identificador de la entidad.
	 */
	public void setNumDocs(Long aNumDocs) {
		this.numDocs = aNumDocs;
	}


}
