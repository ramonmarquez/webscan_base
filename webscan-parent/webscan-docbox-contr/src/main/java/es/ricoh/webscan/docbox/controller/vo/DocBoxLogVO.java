/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.docbox.scanProcess.controller.vo.DocBoxLogVO.java.</p>
 * <b>Descripción:</b><p> Información de una traza de ejecución del proceso de digitalización de un documento digitalizado en la capa de presentación.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.docbox.controller.vo;

import java.io.Serializable;
import java.util.Date;

import es.ricoh.webscan.model.domain.OperationStatus;

/**
 * Información de una traza de ejecución del proceso de digitalización de un
 * documento digitalizado en la capa de presentación.
 * <p>
 * Class DocBoxLogVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class DocBoxLogVO implements Serializable {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la traza de ejecución.
	 */
	private Long id;

	/**
	 * Denominación de la fase del proceso de digitalización asociada.
	 */
	private String stageName;

	/**
	 * Instante en el que se inicia la ejecución de la fase del proceso de
	 * digitalización de un documento digitalizado.
	 */
	private Date startDate;

	/**
	 * Instante en el que se finaliza la ejecución de la fase del proceso de
	 * digitalización de un documento digitalizado.
	 */
	private Date endDate;

	/**
	 * Estado de ejecución de la fase del proceso de digitalización de un
	 * documento digitalizado.
	 */
	private OperationStatus status;

	/**
	 * Constructor sin argumentos.
	 */
	public DocBoxLogVO() {
		super();
	}

	/**
	 * Obtiene el identificador de la traza de ejecución.
	 * 
	 * @return el identificador de la traza de ejecución.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Establece el identificador de la traza de ejecución.
	 * 
	 * @param anId
	 *            un identificador de la traza de ejecución.
	 */
	public void setId(Long anId) {
		this.id = anId;
	}

	/**
	 * Obtiene la denominación de la fase del proceso de digitalización
	 * asociada.
	 * 
	 * @return la denominación de la fase del proceso de digitalización
	 *         asociada.
	 */
	public String getStageName() {
		return stageName;
	}

	/**
	 * Establece la denominación de la fase del proceso de digitalización
	 * asociada.
	 * 
	 * @param anStageName
	 *            denominación de la fase del proceso de digitalización
	 *            asociada.
	 */
	public void setStageName(String anStageName) {
		this.stageName = anStageName;
	}

	/**
	 * Obtiene el instante en el que se inicia la ejecución de la fase del
	 * proceso de digitalización de un documento digitalizado.
	 * 
	 * @return el instante en el que se inicia la ejecución de la fase del
	 *         proceso de digitalización de un documento digitalizado.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Establece el instante en el que se inicia la ejecución de la fase del
	 * proceso de digitalización de un documento digitalizado.
	 * 
	 * @param aStartDate
	 *            instante en el que se inicia la ejecución de la fase del
	 *            proceso de digitalización de un documento digitalizado.
	 */
	public void setStartDate(Date aStartDate) {
		this.startDate = aStartDate;
	}

	/**
	 * Obtiene el instante en el que se finaliza la ejecución de la fase del
	 * proceso de digitalización de un documento digitalizado.
	 * 
	 * @return instante en el que se finaliza la ejecución de la fase del
	 *         proceso de digitalización de un documento digitalizado.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Establece el instante en el que se finaliza la ejecución de la fase del
	 * proceso de digitalización de un documento digitalizado.
	 * 
	 * @param anEndDate
	 *            instante en el que se finaliza la ejecución de la fase del
	 *            proceso de digitalización de un documento digitalizado.
	 */
	public void setEndDate(Date anEndDate) {
		this.endDate = anEndDate;
	}

	/**
	 * Obtiene el estado de ejecución de la fase del proceso de digitalización
	 * de un documento digitalizado.
	 * 
	 * @return el estado de ejecución de la fase del proceso de digitalización
	 *         de un documento digitalizado.
	 */
	public OperationStatus getStatus() {
		return status;
	}

	/**
	 * Establece el estado de ejecución de la fase del proceso de digitalización
	 * de un documento digitalizado.
	 * 
	 * @param aStatus
	 *            estado de ejecución de la fase del proceso de digitalización
	 *            de un documento digitalizado.
	 */
	public void setStatus(OperationStatus aStatus) {
		this.status = aStatus;
	}

}
