/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.profile.controllers.vo;

import es.ricoh.webscan.model.dto.ScanProfileSpecificationDTO;

/**
 * Entidad que usamos para extender las propiedades de la especificación del
 * perfil de escaneo.
 * <p>
 * Class ScanProfileSpecificationExtend.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

public class ScanProfileSpecificationExtend {

	/**
	 * Constructor method for the class ScanProfileSpecificationExtend.java.
	 */
	public ScanProfileSpecificationExtend() {
		super();
	}

	/**
	 * Serial de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long idSuper;

	/**
	 * Denominación de la entidad.
	 */
	private String name;

	/**
	 * Descripción de la entidad.
	 */
	private String description;

	/**
	 * Listado de Especificaciones de Plugin de la entidad.
	 */

	private String listSpecificationPlugin;

	/**
	 * Constructor method for the class ScanProfileSpecificationExtend.java.
	 * 
	 * @param nuevo
	 *            Parámetro con la especificación del perfil de escaneo
	 * @param listado
	 *            String con el listado de especificación de plugind
	 */
	public ScanProfileSpecificationExtend(ScanProfileSpecificationDTO nuevo, String listado) {
		this.name = nuevo.getName();
		this.description = nuevo.getDescription();
		this.idSuper = nuevo.getId();
		this.listSpecificationPlugin = listado;
	}

	/**
	 * Gets the value of the attribute {@link #listSpecificationPlugin}.
	 * 
	 * @return the value of the attribute {@link #listSpecificationPlugin}.
	 */
	public String getListSpecificationPlugin() {
		return listSpecificationPlugin;
	}

	/**
	 * Sets the value of the attribute {@link #listSpecificationPlugin}.
	 * 
	 * @param alistSpecificationPlugin
	 *            The value for the attribute {@link #listSpecificationPlugin}.
	 */
	public void setListSpecificationPlugin(String alistSpecificationPlugin) {
		this.listSpecificationPlugin = alistSpecificationPlugin;
	}

	/**
	 * Gets the value of the attribute {@link #idSuper}.
	 * 
	 * @return the value of the attribute {@link #idSuper}.
	 */
	public Long getIdSuper() {
		return idSuper;
	}

	/**
	 * Sets the value of the attribute {@link #idSuper}.
	 * 
	 * @param aidSuper
	 *            The value for the attribute {@link #idSuper}.
	 */

	public void setIdSuper(Long aidSuper) {
		this.idSuper = aidSuper;
	}

	/**
	 * Gets the value of the attribute {@link #name}.
	 * 
	 * @return the value of the attribute {@link #name}.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the attribute {@link #name}.
	 * 
	 * @param aname
	 *            The value for the attribute {@link #name}.
	 */
	public void setName(String aname) {
		this.name = aname;
	}

	/**
	 * Gets the value of the attribute {@link #description}.
	 * 
	 * @return the value of the attribute {@link #description}.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the attribute {@link #description}.
	 * 
	 * @param adescription
	 *            The value for the attribute {@link #description }.
	 */
	public void setDescription(String adescription) {
		this.description = adescription;
	}

	/**
	 * Gets the value of the attribute {@link #serialversionuid}.
	 * 
	 * @return the value of the attribute {@link #serialversionuid}.
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
