/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.admon.profile.controllers.scanDocsController.java.</p>
 * <b>Descripción:</b><p> .</p>
 * <b>Proyecto:</b><p>Webscan GV - Plataforma de digitalización certificada y copia auténtica de la Generalitat Valenciana.</p>
* @author RICOH Spain IT Services.
* @version 1.0.
 */
package es.ricoh.webscan.admon.profile.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.ricoh.webscan.admon.AdmonWebscanConstants;
import es.ricoh.webscan.admon.orgunit.service.OrgUnitService;
import es.ricoh.webscan.admon.profile.controllers.vo.InfoSaveVO;
import es.ricoh.webscan.admon.profile.controllers.vo.ScanProfileOrgUnitDocSpecVO;
import es.ricoh.webscan.admon.profile.controllers.vo.ScanProfileOrgUnitPluginSpecVO;
import es.ricoh.webscan.admon.profile.controllers.vo.ScanProfileSpecificationExtend;
import es.ricoh.webscan.admon.profile.service.ScanProfileException;
import es.ricoh.webscan.admon.profile.service.ScanProfileService;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.PluginScanProcStageDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;

/**
 * Controlador del perfil de escaneo.
 * <p>
 * Class ScanProfileController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

@Controller
@RequestMapping("/admin")
public class ScanProfileController {

	/**
	 * Atributo de la vista que representa el identificador de la relación de
	 * perfil de escaneo y unidad organizativa.
	 */
	private static final String ATTR_VIEW_SCAN_PROFILE_ORG_UNIT_ID = "scanProfileOrgUnitId";

	/**
	 * Atributo de la vista que representa el identificador de todos los
	 * documentos.
	 */
	private static final String ATTR_VIEW_DOCUMENTS_ALL = "documentsAll";

	/**
	 * Atributo de la vista que representa el identificador de documentos.
	 */
	private static final String ATTR_VIEW_DOCUMENTS = "documents";

	/**
	 * Atributo de la vista que representa el identificador de plugins.
	 */
	private static final String ATTR_VIEW_PLUGINS = "plugins";

	/**
	 * Atributo de la vista que representa el identificador de la especificación
	 * del perfil de escaneo.
	 */
	private static final String ATTR_VIEW_PROFILS_SPEC = "profilsSpec";

	/**
	 * Atributo de la vista que representa el identificador de perfiles.
	 */
	private static final String ATTR_VIEW_PROFILS = "profils";

	/**
	 * Atributo de la vista que representa el identificador de especificaciones.
	 */
	private static final String ATTR_VIEW_SPECIFICATIONS = "specifications";

	/**
	 * Atributo de la vista que representa el identificador del nodo
	 * seleccionado del arbol.
	 */
	private static final String ATTR_VIEW_NODO_SELECT = "nodoSelect";

	/**
	 * Atributo de la vista que repersenta el identificador del arbol.
	 */
	private static final String ATTR_VIEW_ARBOL = "arbol";

	/**
	 * conjunto de vistas usadas en las diferentes llamadas del controlador.
	 */
	private static final String FRAGMENTS_ADMIN_TABLA_MODAL_DOCUMENT_CONTENT_VIEW = "fragments/admin/tablaModalDocument :: content";

	/**
	 * Atributo que representa la vista que contiene el contenido de la
	 * configuración del fragmento de administración.
	 */
	private static final String FRAGMENTS_ADMIN_CONTENT_TABS_CONTENT_CONF_VIEW = "fragments/admin/content_tabs :: contentConf";

	/**
	 * Atributo que representa la vista que contiene el contenido de los
	 * perfiles de escaneos.
	 */
	private static final String FRAGMENTS_ADMIN_PROFILES_VIEW = "fragments/admin/profiles";

	/**
	 * Atributo que representa la vista del perfil de escaneo.
	 */
	private static final String ADMIN_PROFILES_SCAN_VIEW = "admin/profilesScan";

	/**
	 * Constante que representa la unidad organizativa.
	 */
	private static final String UNIDORG = "unidOrg";

	/**
	 * Constante que representa el identificador.
	 */
	private static final String IDENTIFICADOR = "id";

	/**
	 * Constante que representa el identificador del perfil de escaneo y unidad
	 * organizativa.
	 */
	private static final String SCANPROFILEORGUNITID = ATTR_VIEW_SCAN_PROFILE_ORG_UNIT_ID;

	/**
	 * Constante que representa el atributo delete.
	 */
	private static final String DELETE = "delete";

	/**
	 * Constante que respresenta los documentos.
	 */
	private static final String DOCUMENTS = "documentos";

	/**
	 * Constante que representa el atributo de la vista plugins.
	 */
	private static final String PLUGINS = ATTR_VIEW_PLUGINS;

	/**
	 * Constante que representa identificador de documento.
	 */
	private static final String IDDOC = "idDoc";

	/**
	 * Constante que representa identificador de documento activo.
	 */
	private static final String ACTDOC = "actDoc";

	/**
	 * Constante que representa identificador de la Fase.
	 */
	private static final String IDFASE = "idFase";

	/**
	 * Constante que representa identificador de la configuración.
	 */
	private static final String IDCONF = "idConf";

	/**
	 * Constante que representa identificador de la especificación.
	 */
	private static final String IDESPEC = "idEspec";

	/**
	 * Constante que representa identificador de plugin activo.
	 */
	private static final String ACTPLUG = "actPlug";

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScanProfileController.class);

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre las operaciones relacionadas los perfiles de escaneo.
	 */
	@Autowired
	private ScanProfileService scanProfileService;

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre las operaciones relacionadas con las Unidades Organizativas.
	 */
	@Autowired
	private OrgUnitService orgUnitService;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	@Autowired
	private MessageSource messages;

	/**
	 * Pagina inicial de Perfil de Escaneo.
	 * 
	 * @param request
	 *            Parámetro con la petición HTTP
	 * @param model
	 *            Parámetro con el map para añadir los parámetros usados en la
	 *            vista
	 * @param locale
	 *            Parámetro con el idioma de la solicitud
	 * @return vista de la plantilla a visualizar en el front
	 */

	@RequestMapping("/profilesScan")
	public String perfilsDigitals(HttpServletRequest request, ModelMap model, Locale locale) {
		Map<String, String> arbol = new HashMap<String, String>();
		LOG.debug("[ScanProfileController] Start profilesScan");
		try {
			// obtenemos el listado de definiciones de perfiles asociadas
			List<ScanProfileSpecificationExtend> listScanProfileSpe = scanProfileService.getListScanProfileSpecifications(locale);
			List<ScanProfileDTO> listScanProfile = scanProfileService.getListScanProfile();

			model.addAttribute(ATTR_VIEW_SPECIFICATIONS, listScanProfileSpe);
			model.addAttribute(ATTR_VIEW_PROFILS, listScanProfile);
			// informacion de organismos, para construir el arbol
			orgUnitService.getTreeResponse(arbol);

		} catch (DAOException e) {
			String args[] = new String[1];
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.WARN, WebScope.REQUEST, request);
			LOG.error("Error de BBDD:", e.getMessage());
		}

		model.addAttribute(ATTR_VIEW_ARBOL, arbol);
		model.addAttribute(ATTR_VIEW_NODO_SELECT, null);

		LOG.debug("[ScanProfileController] End profilesScan");
		return ADMIN_PROFILES_SCAN_VIEW;
	}

	/**
	 * Elimina un Perfil de Escaneo.
	 * 
	 * @param request
	 *            Parámetro con la petición HTTP
	 * @param model
	 *            Parámetro con el map usado para pasar parámetros a la vista
	 * @param scanProfileId
	 *            Identificador del perfil de escaneo a mostrar
	 * @param locale
	 *            Parámetro con el idioma
	 * @return vista que actualiza la eliminación del perfil
	 */

	@RequestMapping(value = "/removeScanProfile", method = RequestMethod.POST)
	public String removeScanProfile(HttpServletRequest request, ModelMap model, Long scanProfileId, Locale locale) {
		ScanProfileDTO scanProfile;
		String res = null;
		String args[] = new String[1];
		UserInSession user = (UserInSession) request.getSession(false).getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
		LOG.debug("Start removeScanProfile");

		try {
			scanProfile = scanProfileService.getScanProfile(scanProfileId);
			args[0] = scanProfile.getName();

			/* Comprobamos que no tengamos documentos en vuelos en el perfil seleccionado, 
			 * ya que no es posible eliminar en tal caso  
			 */
			if (scanProfileService.hasScanProfileDocsInScanProccess(scanProfileId)) {
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DOCUMENT_IN_FLY_PROFILE, args, ViewMessageLevel.WARN, WebScope.REQUEST, request);
				LOG.info("[ScanProfileController] Warning no se puede eliminar el perfil solicitado debido a que tiene documentos en vuelo");
			} else {
				/*
				 * eliminamos el perfil indicado por parámetro
				 * */
				scanProfileService.removeScanProfile(scanProfileId, user.getUsername());
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_PROFILE_DELETE_OK, args, ViewMessageLevel.INFO, WebScope.REQUEST, request);
				LOG.info("[ScanProfileController] Se ha eliminado correctamente el perfil {}", scanProfileId);
				res = perfilsDigitals(request, model, locale);
			}
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.error("Error de BBDD:", e.getMessage());
		}

		LOG.debug("End removeScanProfile");
		return res;
	}

	/**
	 * Obtiene el frame de las especificaciones de perfiles asociadas a una
	 * definición de perfil.
	 *
	 * @param request
	 *            Parámetro de la petición realizada HTTP
	 * @param idSpec
	 *            Identificador de la definición del perfil a mostrar
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @return nombre de plantilla a mostrar
	 */

	@RequestMapping(value = "/profilsSpec", method = RequestMethod.POST)
	public String profilsSpec(HttpServletRequest request, ModelMap model, @RequestParam Long idSpec) {
		LOG.debug("[ScanProfileController] Start perfilesEspecificaciones");
		String args[] = new String[1];
		List<ScanProfileDTO> listScanProfileSpecDto = new ArrayList<ScanProfileDTO>();

		try {
			listScanProfileSpecDto = scanProfileService.getListScanProfileByIdSpec(Long.valueOf(idSpec));
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.error("Error de BBDD:", e.getMessage());
		}

		model.addAttribute(ATTR_VIEW_PROFILS_SPEC, listScanProfileSpecDto);
		if (listScanProfileSpecDto != null && LOG.isDebugEnabled()) {
			LOG.debug("Contenido:" + listScanProfileSpecDto.toString());
		}
		LOG.debug("[ScanProfileController] End perfilesEspecificaciones");
		return FRAGMENTS_ADMIN_PROFILES_VIEW;
	}

	/**
	 * Obtiene el frame de una especificación de perfil existente.
	 * 
	 * @param request
	 *            Parámetro de la petición realizada HTTP
	 * @param scanProfileId
	 *            Id del perfil de escaneo
	 * @param orgUnitId
	 *            Id de la Unidad Organizativa
	 * @param model
	 *            Entidad usada para traspasar inforamción entre el controlador
	 *            y la vista.
	 * @param scanProfileSpecId
	 *            Identificador de la especificación del perfil de escaneo.
	 * @param locale
	 *            Entidad asociada al language de la petición
	 * @return nombre de plantilla a mostrar
	 */

	@RequestMapping(value = "/profileSpecUni", method = RequestMethod.POST)
	public String perfilEspecificacion(HttpServletRequest request, ModelMap model, String scanProfileId, String orgUnitId, String scanProfileSpecId, Locale locale) {
		LOG.debug("Start perfilesEspecificaciones");
		String args[] = new String[1];
		Long scanProfileOrgUnitId = null;
		ScanProfileOrgUnitDTO scanProfileOrgUnit = null;
		List<DocumentSpecificationDTO> docSpecAll = new ArrayList<DocumentSpecificationDTO>();
		List<ScanProfileOrgUnitDocSpecVO> docSpec = new ArrayList<ScanProfileOrgUnitDocSpecVO>();
		List<ScanProfileOrgUnitPluginSpecVO> pluginSpec = new ArrayList<ScanProfileOrgUnitPluginSpecVO>();

		Long vScanProfileId = scanProfileId == null ? -1 : Long.valueOf(scanProfileId);

		try {
			docSpecAll = scanProfileService.getListDocSpecification();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[ScanProfileController] List<DocumentSpecificationDTO> size" + docSpecAll.size());
			}
			docSpec = scanProfileService.getDocSpec(vScanProfileId, Long.valueOf(orgUnitId));
			if (LOG.isDebugEnabled()) {
				LOG.debug("[ScanProfileController] List<DocSpec> size" + docSpec.size());
			}
			pluginSpec = scanProfileService.getPlugins(vScanProfileId, Long.valueOf(orgUnitId), Long.valueOf(scanProfileSpecId), locale);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[ScanProfileController] List<PluginSpec> size" + pluginSpec.size());
			}
			if (scanProfileId != null) {
				// obtenemos el identificativo de perfil y unidad organizativa
				scanProfileOrgUnit = scanProfileService.getScanProfileOrgUnitByScanProfAndOrgUnit(Long.valueOf(scanProfileId), Long.valueOf(orgUnitId));
				scanProfileOrgUnitId = scanProfileOrgUnit != null ? scanProfileOrgUnit.getId() : null;
			}
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.error("Error de BBDD:", e.getMessage());
		}

		model.addAttribute(ATTR_VIEW_SCAN_PROFILE_ORG_UNIT_ID, scanProfileOrgUnitId);
		model.addAttribute(ATTR_VIEW_DOCUMENTS_ALL, docSpecAll);
		model.addAttribute(ATTR_VIEW_DOCUMENTS, docSpec);
		model.addAttribute(ATTR_VIEW_PLUGINS, pluginSpec);
		LOG.debug("[ScanProfileController] End perfilesEspecificaciones");

		return FRAGMENTS_ADMIN_CONTENT_TABS_CONTENT_CONF_VIEW;
	}

	/**
	 * Obtiene el frame de una especificación de perfil Nueva.
	 *
	 * @param request
	 *            Parámetro de la petición realizada HTTP.
	 * @param scanProfileSpecId
	 *            Id del perfil de escaneo.
	 * @param model
	 *            Entidad usada para traspasar la información entre el
	 *            controlador y la vista.
	 * @return nombre de plantilla a mostrar
	 */

	@RequestMapping(value = "/profileSpecUniNew", method = RequestMethod.POST)
	public String perfilEspecificacionNew(HttpServletRequest request, ModelMap model, String scanProfileSpecId) {
		LOG.debug("Start perfilEspecificacionNew");
		String args[] = new String[1];
		List<DocumentSpecificationDTO> docSpecAll = new ArrayList<DocumentSpecificationDTO>();
		List<ScanProfileOrgUnitPluginSpecVO> pluginSpec = new ArrayList<ScanProfileOrgUnitPluginSpecVO>();
		List<ScanProfileOrgUnitDocSpecVO> docSpec = null;
		try {

			docSpecAll = scanProfileService.getListDocSpecification();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[perfilEspecificacionNew] List<DocumentSpecificationDTO> size" + docSpecAll.size());
			}

			pluginSpec = scanProfileService.getPluginsBase(Long.valueOf(scanProfileSpecId));
			if (LOG.isDebugEnabled()) {
				LOG.debug("[perfilEspecificacionNew] List<PluginSpec> size:" + pluginSpec.size());
			}

		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.error("Error de BBDD:", e.getMessage());
		}

		model.addAttribute(ATTR_VIEW_DOCUMENTS_ALL, docSpecAll);
		model.addAttribute(ATTR_VIEW_DOCUMENTS, docSpec);
		model.addAttribute(ATTR_VIEW_PLUGINS, pluginSpec);
		LOG.debug("[perfilEspecificacionNew] End perfilEspecificacionNew");
		return FRAGMENTS_ADMIN_CONTENT_TABS_CONTENT_CONF_VIEW;
	}

	/**
	 * Obtiene el frame del listado de documentos disponible.
	 * 
	 * @param request
	 *            Parámetro con la petición HTTP
	 * @param model
	 *            Parámetro con el map usado para pasar parámetros a la vista
	 * @return vista con el listado de documentos disponible
	 */

	@RequestMapping(value = "/listDocuments", method = RequestMethod.GET)
	public String listadoDocumentos(HttpServletRequest request, ModelMap model) {
		LOG.debug("[ScanProfileController] Start listadoDocumentos");
		String args[] = new String[1];
		List<DocumentSpecificationDTO> docSpecAll = new ArrayList<DocumentSpecificationDTO>();
		try {
			docSpecAll = scanProfileService.getListDocSpecification();
			if (LOG.isDebugEnabled()) {
				LOG.debug("[ScanProfileController] List<DocumentSpecificationDTO> size" + docSpecAll.size());
			}

		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.error("Error de BBDD:", e.getMessage());
		}
		model.addAttribute(ATTR_VIEW_DOCUMENTS_ALL, docSpecAll);
		LOG.debug("[ScanProfileController] End listadoDocumentos");
		return FRAGMENTS_ADMIN_TABLA_MODAL_DOCUMENT_CONTENT_VIEW;
	}

	/**
	 * Guarda la información asociada a un perfil de escaneo.
	 * 
	 * @param infoSave
	 *            JSON con la información a guardar en relación al Perfil de
	 *            Escaneo.
	 * @param orgUnitIdSel
	 *            Unidad Organizativa seleccionada en la pantalla.
	 * @param locale
	 *            Entidad asociada a la configuración language de la vista.
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad usada para traspasar información del controlador a la
	 *            vista.
	 * @return nombre de plantilla a mostrar
	 */
	@RequestMapping(value = "/saveInfo", method = RequestMethod.POST)
	public String guardarInfo(HttpServletRequest request, ModelMap model, String infoSave, String orgUnitIdSel, Locale locale) {
		String viewRequest = "empty";
		String[ ] args = new String[1];
		// unidades organizativas eliminadas
		List<Long> orgUnitIdsDelete = new ArrayList<Long>();
		// unidades organizativas asociadas al perfil
		List<ScanProfileOrgUnitDTO> unitOrgCurrents = new ArrayList<ScanProfileOrgUnitDTO>();
		InfoSaveVO infoSaveVO = null;
		try {
			LOG.debug("[ScanProfileController] Start guardarInfo");
			if (LOG.isDebugEnabled()) {
				LOG.debug("[ScanProfileController] info recibida:" + infoSave);
				LOG.debug("[ScanProfileController] unidad actual:" + orgUnitIdSel);
			}

			infoSaveVO = getInfoSave(infoSave);
			/*si no es nuevo perfil, obtenemos la lista de unidades organizativas del perfil*/
			if (!infoSaveVO.getScanProfileNew()) {
				// obtenemos las unidades registradas anteriormente
				unitOrgCurrents = scanProfileService.getListScanProfileOrgUnitByScanProfileId(infoSaveVO.getScanProfile().getId());
				if (infoSaveVO.getScanProfile() != null && LOG.isDebugEnabled()) {
					LOG.debug("[ScanProfileController] Unidades recuperadas en relación al perfil(" + infoSaveVO.getScanProfile().getId() + ") :" + unitOrgCurrents.size());
				}
			}

			// obtenemos las fases segun la especificación del perfil
			List<ScanProfileSpecScanProcStageDTO> fases = scanProfileService.getScanProfileSpecScanProcStage(infoSaveVO.getScanProfileSpec());

			// recorremos las unidades organizativas que han sido cambiadas del
			// perfil
			getInfoOrgUnitwithChange(infoSave, infoSaveVO, orgUnitIdsDelete, unitOrgCurrents, fases);
			/*
			 * Fin recorremos las unidades organizativas que han sido cambiadas del perfil
			 */

			/* En este punto tenemos tanto los cambios de plugin como de tipos documentales asociados a unidades organizativas,
			 * por lo tanto, es el momento idoneo para comprobar que no hemos deshabilitado ninguno que tenga documentos en vuelo.
			 */

			/* 1.- Comprobamos que al menos en cada relación unidad organizativa exista al menos un tipo documental seleccionado y activo*/
			checkHasOneDocMinimum(infoSaveVO.getOrgUnitDocs());
			/* 2.- Comprobamos que no pueda ser desvinculado el tipo documental de cualquier relación perfil-unidad organizativa
			 * que tenga documentos en vuelo
			 */
			checkDocumentNotLink(infoSaveVO.getOrgUnitDocs(), request);
			/*
			 * 3.- Validamos los plugins requeridos sean informado
			 */
			checkHasPluginsRequiredByFases(infoSaveVO.getOrgUnitPlugins(), fases, request.getLocale());

			// eliminamos las orgunit que hayan tenido cambio, ya que trataremos
			// los cambios
			for (Long idOrg: orgUnitIdsDelete) {
				for (ScanProfileOrgUnitDTO scanUn: unitOrgCurrents) {
					if (idOrg.equals(scanUn.getOrgUnit())) {
						unitOrgCurrents.remove(scanUn);
						break;
					}
				}
			}

			// recorremos la lista de orgUnit no modificadas para obtener la
			// información que no cambia
			getInfoUnitOrgNotChange(unitOrgCurrents, infoSaveVO.getOrgUnitPlugins(), infoSaveVO.getOrgUnitDocs());

			/*Comprobamos que exista al menos una configuración para el perfil*/
			checkOneSettingMinimun(infoSaveVO.getOrgUnitPlugins(), infoSaveVO.getOrgUnitDocs());

			// actualizamos la información del perfil
			// perfil = updateScanProfileOrCreate(request,
			// infoSaveVO.getOrgUnitPlugins(), infoSaveVO.getOrgUnitDocs(),
			// perfil, scanProfileSpec, propagate);
			updateScanProfileOrCreate(request, infoSaveVO);

			// recuperamos la info para la vista y mostramos el mensaje de
			// resultado ok
			args[0] = "";
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_RESULT_OK, args, ViewMessageLevel.INFO, WebScope.REQUEST, request);
			LOG.debug("[ScanProfileController] Llamamos a las vistas para obtener el resultado de visualizar respuesta");

			/*
			 * Dependiendo si es nuevo o no el perfil, tendremos que actualizar sólo la configuración actual o toda la pantalla para recargar la tabla de
			 * configuraciones
			 */

			if (!infoSaveVO.getScanProfileNew()) {
				viewRequest = perfilEspecificacion(request, model, String.valueOf(infoSaveVO.getScanProfile().getId()), orgUnitIdSel, String.valueOf(infoSaveVO.getScanProfile().getSpecification()), locale);
			} else {
				viewRequest = perfilsDigitals(request, model, request.getLocale());
			}

		} catch (JSONException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[ScanProfileController] DEBUG:" + e.getMessage());
			}
		} catch (NumberFormatException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			if (LOG.isDebugEnabled()) {
				LOG.debug("[ScanProfileController] DEBUG:" + e.getMessage());
			}
		} catch (DAOException e) {
			args[0] = e.getMessage();
			if (ScanProfileException.CODE_001.equals(e.getCode())) {
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DOCUMENT_IN_FLY_DOCUMENT, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[ScanProfileController] ERROR: No se puede deshabilitar la especificación de documentos enviada. Existen documentos en vuelo. DocSpec:" + e.getMessage());
				}
			} else if (ScanProfileException.CODE_002.equals(e.getCode())) {
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_PLUGIN_REQUIRED, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[ScanProfileController] ERROR: No se puede deshabilitar los plugins enviados. El plugin es requerido. Plugin:" + e.getMessage());
				}
			} else if (ScanProfileException.CODE_003.equals(e.getCode())) {
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DOCUMENT_NOT_SELECT, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[ScanProfileController] ERROR: No existe al menos un tipo documental seleccionado para la unidad organizativa. Documento:" + e.getMessage());
				}
			} else if (ScanProfileException.CODE_004.equals(e.getCode())) {
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_ORGUNIT_WITH_DOC_IN_FLY, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[ScanProfileController] ERROR: La unidad organizativa indicada no puede ser eliminada debido a que tiene documentos en vuelo. Unidad Organizativa:" + e.getMessage());
				}
			} else if (ScanProfileException.CODE_005.equals(e.getCode())) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[ScanProfileController] ERROR: No se puede desvincular tipo documental con documentos en vuelo. ScanProfileOrgUnitDocSpecId: " + e.getMessage());
				}
			} else if (ScanProfileException.CODE_006.equals(e.getCode())) {
				args[0] = (infoSaveVO != null) ? infoSaveVO.getScanProfile().getName() : "";
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_SCANPROFILE_SETTING_MINIMUM, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
				LOG.debug("[ScanProfileController] ERROR: No existe una configuración mínima para el perfil");

			} else {
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
				if (LOG.isDebugEnabled()) {
					LOG.debug("[ScanProfileController] ERROR:" + e.getMessage());
				}
			}
		} catch (Exception e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);

			LOG.error("[ScanProfileController] ERROR:", e.getMessage());
		}

		LOG.debug("[ScanProfileController] End guardarInfo");
		return viewRequest;

	}

	/**
	 * Método para formar la primera parte del objecto InfoSaveVo con la
	 * información recibida de la vista.
	 * 
	 * @param infoSave
	 *            String con la información en formato JSON de la información
	 *            recibida de la vista
	 * @return Objecto con la información parseada del String de entrada
	 * @throws Exception
	 *             Exception al tratar tanto los acceso a la información de BBDD
	 *             como de tratamiento del JSON.
	 */

	private InfoSaveVO getInfoSave(String infoSave) throws Exception {
		final String IDPERFIL = "idPerfil";
		final String DENOMINACION = "denominacion";
		final String DESCRIPCION = "descripcion";
		final String ACTIVO = "activo";
		final String PROPAGACION = "propagacion";
		final String IDPERFILSPEC = "idPerfilSpec";
		InfoSaveVO infoSaveVO = new InfoSaveVO();

		// info request ejemplo
		// info a enviar: {"idPerfil":"2","denominacion":"PERFIL
		// 2","descripcion":"PERFIL
		// 2","activo":"false","unidOrg":[{"id":"1","documentos":[{"idDoc":"2"}],"plugins":[{},{}]}]}

		JSONObject obj = new JSONObject(infoSave);
		LOG.debug("[ScanProfileController] Obteniendo información de la pantalla");

		infoSaveVO.setScanProfileSpec(obj.getLong(IDPERFILSPEC));

		// primero comprobamos si existe el perfil
		if (!obj.isNull(IDPERFIL)) {

			Long scanProfileId = obj.getLong(IDPERFIL);
			LOG.debug("[ScanProfileController] Respuesta en busqueda de perfil");

			// buscamos el perfil
			infoSaveVO.setScanProfile(scanProfileService.getScanProfile(scanProfileId));
			if (LOG.isDebugEnabled()) {
				LOG.debug("[ScanProfileController] Respuesta en busqueda de perfil:" + infoSaveVO.toString());
			}

		} else
		/*si no exixte el perfil, lo creamos y el sistema le asignará el id*/
		{
			// el id es automático
			infoSaveVO.setScanProfile(new ScanProfileDTO());
			infoSaveVO.setScanProfileNew(Boolean.TRUE);
		}

		LOG.debug("[ScanProfileController] Obtenemos la informacion del parseo");
		infoSaveVO.getScanProfile().setName(obj.getString(DENOMINACION));
		infoSaveVO.getScanProfile().setDescription(obj.getString(DESCRIPCION));
		infoSaveVO.getScanProfile().setActive(obj.getBoolean(ACTIVO));
		infoSaveVO.setPropagate(obj.getBoolean(PROPAGACION));
		LOG.debug("[ScanProfileController] Preparamos el objeto ScanProfile");

		LOG.debug("[ScanProfileController] Obtenemos la info de las unidades organizativas modificadas");
		// 2","activo":"false"

		return infoSaveVO;
	}

	/**
	 * Método para actualizar el perfil o crear un nuevo perfil.
	 * 
	 * @param request
	 *            Parámetro con la petición HTTP
	 * @param infoSaveVO
	 *            Parámetro con la información recuperada de la vista de perfil
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil de escaneo , o se produce algún error en la consulta a
	 *             BBDD.
	 */
	private void updateScanProfileOrCreate(HttpServletRequest request, InfoSaveVO infoSaveVO) throws DAOException {
		// si el perfil no está creado, lo creamos antes de realizar el update
		if (infoSaveVO.getScanProfile().getId() == null) {
			scanProfileService.performCreateScanProfile(infoSaveVO, request);
		} else {
			// llamamos al manager principal de actualización de perfil
			scanProfileService.performUpdateScanProfile(request, infoSaveVO.getScanProfile(), infoSaveVO.getOrgUnitPlugins(), infoSaveVO.getOrgUnitDocs(), infoSaveVO.getPropagate(), Boolean.TRUE);
		}
	}

	/**
	 * Método para obtener toda la información de las unidades organizativas que
	 * han sido modificada en la pantalla de Perfiles de Escaneo.
	 * 
	 * @param infoSave
	 *            String con la información recibida de la vista
	 * @param infoSaveVO
	 *            Parámetro con la información recuperada del parseo de la info
	 *            de la vista
	 * @param orgUnitIdsDelete
	 *            Parámetro de entrada/salida con la unidades organizativas que
	 *            han sido eliminadas
	 * @param unitOrgCurrents
	 *            Parámetro de entrada/salida con las unidades organizativas
	 *            actuales
	 * @param fases
	 *            Listado con las fases asociadas al perfil
	 * @throws Exception
	 *             Exception no esperada en el tratamiento realizado
	 */

	private void getInfoOrgUnitwithChange(String infoSave, InfoSaveVO infoSaveVO, List<Long> orgUnitIdsDelete, List<ScanProfileOrgUnitDTO> unitOrgCurrents, List<ScanProfileSpecScanProcStageDTO> fases) throws Exception {

		// ,"unidOrg":[{"id":"1","scanProfileOrgUnitId":48,"documentos":[{"idDoc":"2"}],"plugins":[{},{}]}]}
		JSONObject obj = new JSONObject(infoSave);
		JSONArray unidades = obj.getJSONArray(UNIDORG);

		Long scanProfileId = null;
		if (!infoSaveVO.getScanProfileNew()) {
			scanProfileId = infoSaveVO.getScanProfile().getId();
		}
		for (int i = 0; i < unidades.length(); i++) {
			// obtenemos scan_prof_org a actualizar o modificar
			ScanProfileOrgUnitDTO scanOrg = null;
			Long orgUnitId = unidades.getJSONObject(i).getLong(IDENTIFICADOR);
			Long scanProfileOrgUnitId = null;
			if (!unidades.getJSONObject(i).isNull(SCANPROFILEORGUNITID)) {
				scanProfileOrgUnitId = unidades.getJSONObject(i).getLong(SCANPROFILEORGUNITID);
				// buscamos si existe la realización entre perfil y unidad
				// organizativa
				scanOrg = scanProfileService.getScanProfileOrgUnitByScanProfAndOrgUnit(Long.valueOf(scanProfileId), Long.valueOf(orgUnitId));
				if (scanOrg != null && !scanOrg.getId().equals(scanProfileOrgUnitId)) {
					LOG.error("[ScanProfileController] Existe una relación pero no coinciden con la información de la vista");
				}
			} else {
				// lo creamos porque luego se necesita para la asociación
				// con los plugins.
				scanOrg = new ScanProfileOrgUnitDTO();
				scanOrg.setOrgUnit(orgUnitId);
				scanOrg.setScanProfile(scanProfileId);
			}

			// si lo hemos eliminados antes lo quitamos
			Boolean deleteBool = unidades.getJSONObject(i).getBoolean(DELETE);
			if (deleteBool) {
				if (scanOrg.getId() != null && scanProfileService.hasScanProfileOrgUnitDocsInScanProccess(scanOrg.getId())) {
					/*lanzamos una exception para recoger que la unidad organizativa no puede ser eliminada por tener documentos en vuelo*/
					throw new ScanProfileException(ScanProfileException.CODE_004, infoSaveVO.getScanProfile().getName());
				}
				orgUnitIdsDelete.add(orgUnitId);
				continue;
			}

			// obtenemos los documentos
			JSONArray documentos = unidades.getJSONObject(i).getJSONArray(DOCUMENTS);
			// obtenemos los plugins
			JSONArray plugins = unidades.getJSONObject(i).getJSONArray(PLUGINS);

			// comprobamos que exista alguna informacion asociada al perfil
			// y la
			// entidad organizativa
			if (plugins.length() == 0 && documentos.length() == 0) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("[ScanProfileController] No tenemos información para guardar asociada al perfil:" + infoSaveVO.getScanProfile().getId() + "unidad organizativa:" + orgUnitId);
				}
				continue;
			}

			// tratamos los documentos
			Map<Long, Boolean> mapOrgUnitDocs = new HashMap<Long, Boolean>();
			for (int j = 0; j < documentos.length(); j++) {
				Long idDoc = documentos.getJSONObject(j).getLong(IDDOC);
				Boolean actDoc = documentos.getJSONObject(j).getBoolean(ACTDOC);
				mapOrgUnitDocs.put(idDoc, actDoc);
			}
			infoSaveVO.getOrgUnitDocs().put(scanOrg, mapOrgUnitDocs);

			// tratamos los plugins
			List<PluginScanProcStageDTO> listPluginScanProc = new ArrayList<PluginScanProcStageDTO>();

			for (int j = 0; j < plugins.length(); j++) {

				// solo usaremos la info de cofiguracion
				ScanProfileSpecScanProcStageDTO faseAux = null;
				// fase
				Long idFase = plugins.getJSONObject(j).getLong(IDFASE);
				// buscamos la fase
				for (ScanProfileSpecScanProcStageDTO ac: fases) {
					if (ac.getScanProcessStageId().equals(idFase)) {
						faseAux = ac;
						break;
					}
				}

				// configuracion plugin
				Long idConf = plugins.getJSONObject(j).getLong(IDCONF);
				Long idEspec = plugins.getJSONObject(j).getLong(IDESPEC);
				// activo plugin
				Boolean actPlug = plugins.getJSONObject(j).getBoolean(ACTPLUG);

				PluginSpecificationDTO plugSpec = scanProfileService.getPluginSpecification(idEspec);

				if (faseAux != null && plugSpec != null) {
					PluginScanProcStageDTO plugProc = new PluginScanProcStageDTO();
					plugProc.setPlugin(idConf);
					plugProc.setScanProcessStage(idFase);
					plugProc.setActive(actPlug);
					listPluginScanProc.add(plugProc);
				} else {
					throw new Exception("Error al tratar la especificacion plugins y su relacion de fase");

				}
			}

			// añadimos los plugins por fases
			infoSaveVO.getOrgUnitPlugins().put(scanOrg, listPluginScanProc);

			// eliminamos la unidad organizativa no pendiente de buscar.
			// Usado para luego recuperar la info que no cambia en el
			// sistema

			for (ScanProfileOrgUnitDTO scanUn: unitOrgCurrents) {
				if (scanUn.getOrgUnit().equals(orgUnitId)) {
					unitOrgCurrents.remove(scanUn);
					break;
				}
			}
		}
	}

	/**
	 * Obtiene la información de los documentos y los plugins de las unidades
	 * organizativas no modificadas en la pantalla de administración a la hora
	 * de guardar los cambios realizados.
	 * 
	 * @param unitOrgCurrents
	 *            Unidades organizativas actuales
	 * @param orgUnitPlugins
	 *            Parametro de entrada/salida que corresponde a la relación de
	 *            unidades organizativas y plugins
	 * @param orgUnitDocs
	 *            Parametro de entrada/salida que corresponde a la relación de
	 *            unidades organizativas y los documentos asociados a estas.
	 * @throws DAOException
	 *             Exception que se puede producir a la hora de acceder a la
	 *             BBDD o realizar cualquier consulta
	 */
	private void getInfoUnitOrgNotChange(List<ScanProfileOrgUnitDTO> unitOrgCurrents, Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins, Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs) throws DAOException {
		for (ScanProfileOrgUnitDTO scanProfileOrgUnit: unitOrgCurrents) {

			Map<Long, Boolean> mapOrgUnitDocs = new HashMap<Long, Boolean>();

			// obtenemos los documentos asociados a la unidad
			// organizativa y perfil
			List<ScanProfileOrgUnitDocSpecDTO> docsSpec = scanProfileService.getScanProfileOrgUnitDocsConf(scanProfileOrgUnit.getScanProfile(), scanProfileOrgUnit.getOrgUnit());

			for (ScanProfileOrgUnitDocSpecDTO doc: docsSpec) {
				mapOrgUnitDocs.put(doc.getDocSpecification(), doc.getActive());
			}
			mapOrgUnitDocs = orgUnitDocs.put(scanProfileOrgUnit, mapOrgUnitDocs);

			// obtenemos los plugins
			List<PluginScanProcStageDTO> pluginScanProcStage = new ArrayList<PluginScanProcStageDTO>();

			// función auxiliar para obtener todos los plugins relacionados
			// por el perfil y la unidad organizativa
			getPluginScanProcStageByScanProfileOrgUnit(scanProfileOrgUnit, pluginScanProcStage);

			// añadimos el listado de plugins por fase
			orgUnitPlugins.put(scanProfileOrgUnit, pluginScanProcStage);
		}
	}

	/**
	 * Método que realiza la comprobación de que exista al menos una
	 * configuración mínima de plugins y documentos un perfil.
	 * 
	 * @param orgUnitPlugins
	 *            Map con la realacion de unidades organizativas y perfil junto
	 *            con sus plugins asociados
	 * @param orgUnitDocs
	 *            Map con la realacion de unidades organizativas y perfil junto
	 *            con sus documentos asociados
	 * @throws ScanProfileException
	 *             Exception que será lanzada al comprobar que no existe ningún
	 *             plugins y tipo documental para al menos una relación de
	 *             unidad organizativa y perfil.
	 */
	private void checkOneSettingMinimun(Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins, Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs) throws ScanProfileException {
		/*posición primera si existe plugin */
		final Long POS_PLUGIN_EXIST = Long.valueOf(0);
		final Long POS_DOCS_EXIST = Long.valueOf(1);
		Boolean result = Boolean.FALSE;

		// agrupamos por perfil
		Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> mapFull = new HashMap<ScanProfileOrgUnitDTO, Map<Long, Boolean>>();

		if (orgUnitPlugins != null) {
			/*preparamos los plugins*/
			for (ScanProfileOrgUnitDTO scanProfileOrgUnit: orgUnitPlugins.keySet()) {
				List<PluginScanProcStageDTO> act = orgUnitPlugins.get(scanProfileOrgUnit);
				Boolean aux = act != null && !act.isEmpty() ? Boolean.TRUE : Boolean.FALSE;
				Map<Long, Boolean> nuevo = new HashMap<Long, Boolean>();
				nuevo.put(POS_PLUGIN_EXIST, aux);
				mapFull.put(scanProfileOrgUnit, nuevo);
			}
		}
		if (orgUnitDocs != null) {
			/*preparamos los documentos*/
			for (ScanProfileOrgUnitDTO scanProfileOrgUnit: orgUnitDocs.keySet()) {
				Map<Long, Boolean> act = orgUnitDocs.get(scanProfileOrgUnit);
				Boolean aux = act != null && !act.isEmpty() ? Boolean.TRUE : Boolean.FALSE;
				Map<Long, Boolean> nuevo;
				// comprobamos si existe ya un perfil añadido
				nuevo = mapFull.get(scanProfileOrgUnit);
				if (nuevo == null) {
					nuevo = new HashMap<Long, Boolean>();
				}
				nuevo.put(POS_DOCS_EXIST, aux);
				mapFull.put(scanProfileOrgUnit, nuevo);
			}
		}

		/*recorremos el map completo y verificamos que al menos exista un perfil con documentos y plugins*/
		for (ScanProfileOrgUnitDTO scanProfileOrgUnit: mapFull.keySet()) {
			Map<Long, Boolean> actual = mapFull.get(scanProfileOrgUnit);
			if (actual != null && Boolean.TRUE.equals(actual.get(POS_PLUGIN_EXIST).booleanValue()) && Boolean.TRUE.equals(actual.get(POS_DOCS_EXIST).booleanValue())) {
				result = Boolean.TRUE;
				break;
			}
		}

		if (!result) // no hay
		{
			throw new ScanProfileException(ScanProfileException.CODE_006, "No existe una configuración mínima");
		}
	}

	/**
	 * Método para comprobar que la configuración actual de documentos no tiene
	 * tipo documental no vinculados con documentos en vuelos. No se permite
	 * desvincular tipo documental con documentos en vuelos.
	 * 
	 * @param orgUnitDocsActuales
	 *            Map con configuración actual de tipo documental y su
	 *            activación.
	 * @param request
	 *            Petición http.
	 * @throws DAOException
	 *             Exception al tratar el acceso a BBDD.
	 * @throws ScanProfileException
	 *             Si existe algún tipo documental no vinculado con documentos
	 *             en vuelo.
	 */

	private void checkDocumentNotLink(Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocsActuales, HttpServletRequest request) throws DAOException, ScanProfileException {
		ScanProfileOrgUnitDTO scanProfileOrgUnit;
		Map<Long, Boolean> mapDocNews;
		Map<Long, ScanProfileOrgUnitDocSpecDTO> mapDocOlds = new HashMap<Long, ScanProfileOrgUnitDocSpecDTO>();

		if (orgUnitDocsActuales != null) {
			// recorremos la configuracion actual
			for (Map.Entry<ScanProfileOrgUnitDTO, Map<Long, Boolean>> actual: orgUnitDocsActuales.entrySet()) {
				scanProfileOrgUnit = actual.getKey();
				mapDocNews = actual.getValue();
				// recuperamos todos los documentos previamente vinculados, es
				// decir, existente en BBDD
				if (scanProfileOrgUnit.getId() != null) {
					mapDocOlds = getMapDocSpecOld(scanProfileService.getDocSpecificationsByScanProfileOrgUnitId(scanProfileOrgUnit.getId()));
				}
				// comparamos la antigua configuracion por perfil y unidad
				// organizativa
				compareNewsAndOldsDocsToCheckNotDocumentInFly(request, mapDocNews, mapDocOlds, scanProfileOrgUnit);
			}
		}
	}

	/**
	 * Método que realiza la comparación de la configuración existente en el
	 * sistema con la nueva enviada comprobando que la nueva configuración no
	 * tenga tipo documental desvinculado que tenga documentos en vuelos.
	 * 
	 * @param request
	 *            Parámetro de la petición
	 * @param mapDocNews
	 *            map con la configuración nueva realizada
	 * @param mapDocOlds
	 *            map con la antigua configuración existente
	 * @param scanProfileOrgUnit
	 *            Objecto que identifica la relación de perfil escaneado y
	 *            unidad organizativa
	 * @throws DAOException
	 *             Exception que se produce tanto si existe error en tratamiento
	 *             de BBDD o bien que existe un tipo documental con documentos
	 *             en vuelos que ha sido desvinculado.
	 * @throws ScanProfileException
	 *             Si existe un tipo documental con documentos en vuelos que ha
	 *             sido desvinculado.
	 */
	private void compareNewsAndOldsDocsToCheckNotDocumentInFly(HttpServletRequest request, Map<Long, Boolean> mapDocNews, Map<Long, ScanProfileOrgUnitDocSpecDTO> mapDocOlds, ScanProfileOrgUnitDTO scanProfileOrgUnit) throws DAOException, ScanProfileException {
		final int MAX_ARGS = 4;
		String[ ] args = new String[MAX_ARGS];
		final int POS_SCAN_PROFILE_NAME = 0;
		final int POS_SCAN_PROFILE_SPEC_NAME = 1;
		final int POS_SCAN_PROFILE_SPEC_EXT_ID = 2;
		final int POS_DOCUMENT_SPEC = 3;

		/*comprobación inicial */
		if (scanProfileOrgUnit == null) {
			throw new ScanProfileException(ScanProfileException.CODE_999, "[ScanProfileController] Parámetro no esperado en la función compareNewsAndDocsToCheckNotDocumentInFly");
		}

		if (mapDocOlds != null) {
			/**
			 * recorremos la antigua configuracion comprobando si no existe
			 * alguna especificación de documentos no vinculada en la actual.
			 */
			for (Long docSpecId: mapDocOlds.keySet()) {
				/**
				 * si no existe en la nueva configuracion, indica que ha sido
				 * desvinculado en la actual configuración y por lo tanto,
				 * comprobamos que dicha configuracion no tenga documentos en
				 * vuelo
				 */
				if (mapDocNews != null && !mapDocNews.containsKey(docSpecId) && scanProfileService.hasScanProfileOrgUnitDocSpecDocsInScanProccess(scanProfileOrgUnit.getId(), docSpecId)) {
					args[POS_SCAN_PROFILE_NAME] = scanProfileService.getDocSpecification(docSpecId).getName();
					args[POS_SCAN_PROFILE_SPEC_NAME] = scanProfileService.getScanProfile(scanProfileOrgUnit.getScanProfile()).getName();
					args[POS_SCAN_PROFILE_SPEC_EXT_ID] = scanProfileService.getOrganizationUnit(scanProfileOrgUnit.getOrgUnit()).getExternalId();
					args[POS_DOCUMENT_SPEC] = scanProfileService.getDocSpecification(docSpecId).getName();

					/* Mostramos el mensaje y devolvemos el control al controlador donde no tendremos que mostrar nada*/
					BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DOCSPEC_UNLINK_WITH_DOC_IN_FLY, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);

					/*lanzamos la exception comunicando que hay documentos en vuelo, pasando el id con la relación de perfil - unidad org y tipo documental*/
					throw new ScanProfileException(ScanProfileException.CODE_005, mapDocOlds.get(docSpecId).getId().toString());
				}
			}
		}
	}

	/**
	 * Método auxiliar para crear un map con la configuracion actual por
	 * identificador del tipo documental y su activacion, a partir de su
	 * configuración pasada como parámetro.
	 * 
	 * @param mapIn
	 *            Map de la configuración actual del sistema por tipo de
	 *            especificacion de documento y su configuracion.
	 * @return Map con la configuración por id del tipo documental y el objeto
	 *         que relaciona perfil escaneado, unidad organizativa y tipo
	 *         documental
	 */

	private Map<Long, ScanProfileOrgUnitDocSpecDTO> getMapDocSpecOld(Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> mapIn) {
		Map<Long, ScanProfileOrgUnitDocSpecDTO> res = new HashMap<Long, ScanProfileOrgUnitDocSpecDTO>();
		if (mapIn != null) {
			ScanProfileOrgUnitDocSpecDTO scanProfileOrgUnitDocSpec;
			for (Map.Entry<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> actual: mapIn.entrySet()) {
				scanProfileOrgUnitDocSpec = actual.getValue();
				res.put(scanProfileOrgUnitDocSpec.getDocSpecification(), scanProfileOrgUnitDocSpec);
			}
		}
		return res;
	}

	/**
	 * Método para atender las peticionese de comprobación de si existe un
	 * documentos en vuelo según los parámetros pasados en la request.
	 * 
	 * @param request
	 *            Petición HTTP realizada desde el browser.
	 * @param model
	 *            Modelo para response donde se almacena la información pasada a
	 *            la vista
	 * @param docSpecId
	 *            Identificador de la especificación de documento
	 * @param scanProfileId
	 *            Identificador del perfil de escaneo
	 * @param orgUnitId
	 *            Identificador de la unidad organizativa
	 * @return String con el resultado de la petición realizada
	 */

	@RequestMapping(value = "/checkHasDocInFly", method = RequestMethod.POST)
	@ResponseBody
	public String checkHasDocInFly(HttpServletRequest request, ModelMap model, String docSpecId, String scanProfileId, String orgUnitId) {
		final int MAX_ARGS = 3;
		Boolean res = Boolean.FALSE;
		String[ ] args = new String[MAX_ARGS];
		try {
			ScanProfileOrgUnitDTO scanProfileOrgUnit = scanProfileService.getScanProfileOrgUnitByScanProfAndOrgUnit(Long.valueOf(scanProfileId), Long.valueOf(orgUnitId));
			Boolean respuesta = scanProfileService.hasScanProfileOrgUnitDocSpecDocsInScanProccess(scanProfileOrgUnit.getId(), Long.valueOf(docSpecId));
			if (respuesta) {
				args[0] = scanProfileId;
				args[1] = orgUnitId;
				args[2] = docSpecId;
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DOCSPEC_UNLINK_WITH_DOC_IN_FLY, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
				LOG.debug("[ScanProfileController] Existen documentos en vuelo para la relacion indicada (Perfil:{} - Unidad Organizativa:{} - Doc Specification: {}).", scanProfileId, orgUnitId, docSpecId);
				res = Boolean.TRUE;
			}
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.debug("[ScanProfileController] Error Acceso a BBDD to checkHasDocInFly.");
		}
		return res.toString();
	}

	/**
	 * Método que comprueba si existe al menos un tipo documental seleccionado y
	 * activo.
	 * 
	 * @param orgUnitDocs
	 *            Map con la relación de unidades organizativas y documentos
	 *            seleccinados
	 * @throws ScanProfileException
	 *             Exception lanzada si existe alguna unidad organizativa que no
	 *             ha seleccionado al menos un tipo documental activo
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             tipo documental, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	private void checkHasOneDocMinimum(Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs) throws ScanProfileException, DAOException {
		LOG.debug("[ScanProfileController] Start checkHasOneDocMinimum");
		int numDocByScanProfileOrgUnit;
		try {
			if (orgUnitDocs != null) {
				/*Recorremos por ScanProfileOrgUnitDto*/
				for (Map.Entry<ScanProfileOrgUnitDTO, Map<Long, Boolean>> entrada: orgUnitDocs.entrySet()) {
					// inicializamos contador
					numDocByScanProfileOrgUnit = 0;
					Map<Long, Boolean> documents = entrada.getValue();
					if (documents != null) {
						/* Recorremos por unidad organizativa del perfil los documentos y aumentamos el contador con los activos*/
						for (Map.Entry<Long, Boolean> docRelation: documents.entrySet()) {
							if (Boolean.TRUE.equals(docRelation.getValue())) {
								numDocByScanProfileOrgUnit++;
							}
						}
					}
					if (numDocByScanProfileOrgUnit == 0) {
						/* 
						 * Lanzamos la excepcion indicando que para el perfil y unidad organizativa indicada por el id
						 * no tiene al menos un documento activo
						 */
						ScanProfileOrgUnitDTO scanProfileOrgUnitDTO = (ScanProfileOrgUnitDTO) entrada.getKey();
						String unidadOrganizativa = scanProfileService.getOrganizationUnit(scanProfileOrgUnitDTO.getOrgUnit()).getExternalId();
						throw new ScanProfileException(ScanProfileException.CODE_003, unidadOrganizativa);
					}
				}
			}

		} catch (DAOException e) {
			throw e;
		}
		LOG.debug("[ScanProfileController] End checkHasOneDocMinimum");
	}

	/**
	 * Método auxiliar para comprobar que existen plugins requeridos en cada
	 * fase con respecto a la relación perfil - unidad organizativa.
	 * 
	 * @param orgUnitPlugins
	 *            map de los perfiles por unidad organizativa y su relacion de
	 *            plugins por fase.
	 * @param fases
	 *            Litado de las fases de la especificacion de perfil.
	 * @param locale
	 *            configuración regional.
	 * @throws ScanProfileException
	 *             Exception indicando si hay algún documento en vuelo.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	private void checkHasPluginsRequiredByFases(Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins, List<ScanProfileSpecScanProcStageDTO> fases, Locale locale) throws ScanProfileException, DAOException {

		if (orgUnitPlugins != null && fases != null) {
			/*Recorremos por ScanProfileOrgUnitDto*/
			for (Map.Entry<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> entrada: orgUnitPlugins.entrySet()) {
				// ScanProfileOrgUnitDTO scanProfileOrgUnitDTO =
				// entrada.getKey();
				List<PluginScanProcStageDTO> listPluginScanProcStage = entrada.getValue();
				if (listPluginScanProcStage != null && !listPluginScanProcStage.isEmpty()) {
					/*recorremos por fases*/
					for (ScanProfileSpecScanProcStageDTO fase: fases) {
						/*si es requerido, identificamos el plugin y verificamos si está activo*/
						if (Boolean.TRUE.equals(fase.getMandatory())) {
							Boolean encontrado = Boolean.FALSE;
							/*recorremos los plugins configurados*/
							for (PluginScanProcStageDTO pluginScanProcStage: listPluginScanProcStage) {
								if (fase.getScanProcessStageId().equals(pluginScanProcStage.getScanProcessStage()) && Boolean.TRUE.equals(pluginScanProcStage.getActive())) {
									encontrado = Boolean.TRUE;
									break;
								}
							}
							if (!encontrado) {
								String faseStr = fase.getScanProcessStage();
								/*lanzamos la exception comunicando que hay plugins requeridos que no han sido informado*/
								throw new ScanProfileException(ScanProfileException.CODE_002, messages.getMessage("stage." + faseStr, null, locale));
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Metodo auxiliar para obtener los plugins por etapa de un perfil y unidad
	 * organizativa.
	 * 
	 * @param scanProfileOrgUnit
	 *            Entidad que relaciona el perfil y la unidad organizativa
	 * @param pluginScanProcStage
	 *            Listado de plugins por etapa del perfil y la unidad
	 *            organizativa indicada
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la consulta a BBDD.
	 */
	public void getPluginScanProcStageByScanProfileOrgUnit(ScanProfileOrgUnitDTO scanProfileOrgUnit, List<PluginScanProcStageDTO> pluginScanProcStage) throws DAOException {

		ScanProfileDTO scanProfile = scanProfileService.getScanProfile(scanProfileOrgUnit.getScanProfile());
		OrganizationUnitDTO orgUnit = scanProfileService.getOrganizationUnit(scanProfileOrgUnit.getOrgUnit());
		Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> plugs = scanProfileService.getScanProfileOrgUnitPluginConf(scanProfileOrgUnit.getScanProfile(), scanProfileOrgUnit.getOrgUnit());

		for (Entry<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> elto: plugs.entrySet()) {
			List<PluginSpecificationDTO> listPlugSpec = elto.getValue();
			for (PluginSpecificationDTO spec: listPlugSpec) {

				try {
					// buscamos si tenemos configurada ya la especificacion
					ScanProfileOrgUnitPluginDTO scanProfileOrgUnitPlugin = scanProfileService.getScanProfileOrgUnitPlugin(scanProfile, orgUnit, spec);
					// parece ser que devuelve null algunas veces
					if (scanProfileOrgUnitPlugin != null) {
						// preparamos el objeto para la lista
						PluginScanProcStageDTO nuevo = new PluginScanProcStageDTO();
						nuevo.setScanProcessStage(elto.getKey().getScanProcessStageId());
						nuevo.setPlugin(scanProfileOrgUnitPlugin.getPlugin());
						nuevo.setActive(scanProfileOrgUnitPlugin.getActive());
						pluginScanProcStage.add(nuevo);
					} else {
						if (LOG.isDebugEnabled()) {
							LOG.debug("No existe ninguna relación entre el perfil(" + scanProfile + "), la unidad organizativa(" + orgUnit + ") y especificacion plugin (" + spec + ")");
						}
					}
				} catch (DAOException e) {
					// no existe relación entre perfil y entidad
					if (e.getCode().equals(DAOException.CODE_901)) {
						LOG.debug("No existe ninguna relación entre el perfil y la unidad organizativa");

					} else {
						LOG.error("Error recuperar relacion entre perfil y unidad organizativa");
						throw e;
					}
				} // fin catch
			} // fin for spec
		} // fin for elto

	}
}
