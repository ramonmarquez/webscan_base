/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.profile.controllers.vo;

import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;

/**
 * Entidad que usamos para la visualización de la información asociada a la
 * especificación de documentos en relación con el perfil de escaneo y la unidad
 * organizativa.
 * <p>
 * Class ScanProfileOrgUnitDocSpecVO.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

public class ScanProfileOrgUnitDocSpecVO {

	/**
	 * Denominación de la entidad scanProfOrgId asociado a la especificacion si
	 * existe.
	 */

	private Long sprofOrgId;

	/**
	 * Indicador de activación de la entidad en pantalla.
	 */

	private Boolean activePantalla;
	/**
	 * id de la entidad padre.
	 */

	private Long idSuper;

	/**
	 * Denominación de la entidad.
	 */
	private String name;

	/**
	 * Descripción de la entidad.
	 */
	private String description;

	/**
	 * Indicador de activación de la entidad.
	 */
	private Boolean active = Boolean.TRUE;

	/**
	 * Constructor por defecto.
	 */
	public ScanProfileOrgUnitDocSpecVO() {
	}

	/**
	 * Constructor method for the class ScanProfileOrgUnitDocSpecVO.java.
	 * 
	 * @param aexten
	 *            Parametro DocumentSpecificationDTO con información que se
	 *            extiende del documento
	 * @param asprofOrfId
	 *            Parametro identificador de la relación entre perfil de escaneo
	 *            y unidad organizativa
	 * @param aactivo
	 *            Parametro que indica si está activo o no la especificación de
	 *            documento asociada
	 */
	public ScanProfileOrgUnitDocSpecVO(DocumentSpecificationDTO aexten, Long asprofOrfId, Boolean aactivo) {
		this.sprofOrgId = asprofOrfId;
		this.name = aexten.getName();
		this.description = aexten.getDescription();
		// this.active=exten.getActive();
		this.active = aactivo;
		this.idSuper = aexten.getId();
		// marcaba si iba estar activo en pantalla la posibilidad de modificar
		this.activePantalla = aactivo;
	}

	/**
	 * Gets the value of the attribute {@link #activePantalla}.
	 * 
	 * @return the value of the attribute {@link #activePantalla}.
	 */
	public Boolean getActivePantalla() {
		return activePantalla;
	}

	/**
	 * Sets the value of the attribute {@link #activePantalla}.
	 * 
	 * @param aactivePantalla
	 *            The value for the attribute {@link #activePantalla}.
	 */
	public void setActivePantalla(Boolean aactivePantalla) {
		this.activePantalla = aactivePantalla;
	}

	/**
	 * Gets the value of the attribute {@link #idSuper}.
	 * 
	 * @return the value of the attribute {@link #idSuper}.
	 */
	public Long getIdSuper() {
		return idSuper;
	}

	/**
	 * Sets the value of the attribute {@link #idSuper}.
	 * 
	 * @param aidSuper
	 *            The value for the attribute {@link #idSuper}.
	 */
	public void setIdSuper(Long aidSuper) {
		this.idSuper = aidSuper;
	}

	/**
	 * Gets the value of the attribute {@link #sprofOrgId}.
	 * 
	 * @return the value of the attribute {@link #sprofOrgId}.
	 */
	public Long getSprofOrgId() {
		return sprofOrgId;
	}

	/**
	 * Sets the value of the attribute {@link #sprofOrgId}.
	 * 
	 * @param asprofOrgId
	 *            The value for the attribute {@link #sprofOrgId}.
	 */
	public void setSprofOrgId(Long asprofOrgId) {
		this.sprofOrgId = asprofOrgId;
	}

	/**
	 * Gets the value of the attribute {@link #name}.
	 * 
	 * @return the value of the attribute {@link #name}.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the attribute {@link #name}.
	 * 
	 * @param aname
	 *            The value for the attribute {@link #name}.
	 */
	public void setName(String aname) {
		this.name = aname;
	}

	/**
	 * Gets the value of the attribute {@link #description}.
	 * 
	 * @return the value of the attribute {@link #description}.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the attribute {@link #description}.
	 * 
	 * @param adescription
	 *            The value for the attribute {@link #description}.
	 */
	public void setDescription(String adescription) {
		this.description = adescription;
	}

	/**
	 * Gets the value of the attribute {@link #active}.
	 * 
	 * @return the value of the attribute {@link #active}.
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * Sets the value of the attribute {@link #active}.
	 * 
	 * @param aactive
	 *            The value for the attribute {@link #active}.
	 */
	public void setActive(Boolean aactive) {
		this.active = aactive;
	}

}
