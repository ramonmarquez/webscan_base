/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.document.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.ricoh.webscan.admon.AdmonWebscanConstants;
import es.ricoh.webscan.admon.document.controllers.vo.DocSpecDefInherVO;
import es.ricoh.webscan.admon.document.controllers.vo.DocSpecVO;
import es.ricoh.webscan.admon.document.controllers.vo.MetadataValueList;
import es.ricoh.webscan.admon.document.controllers.vo.MetasDocSpecVO;
import es.ricoh.webscan.admon.document.service.DocumentException;
import es.ricoh.webscan.admon.document.service.DocumentService;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;

/**
 * <p>
 * Class DocumentController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Controller
@RequestMapping("/admin")
public class DocumentController {

	/**
	 * Atributo de la vista identificador de la ultima pagina.
	 */
	private static final String ATTR_VIEW_ULTIMA_PAGINA = "ultimaPagina";
	/**
	 * Atributo de la vista identificador del ultimo elemento seleccionado.
	 */
	private static final String ATTR_VIEW_ULTIMO_ELTO_SELECT = "ultimoEltoSelect";
	/**
	 * Atributo de la vista identificador de la lista de definición de
	 * documentso.
	 */
	private static final String ATTR_VIEW_LIST_DEF_DOC = "listDefDoc";
	/**
	 * Atributo de la vista identificador de la lista de metadatos heredados.
	 */
	private static final String ATTR_VIEW_LIST_META_INHER = "listMetaInher";
	/**
	 * Atributo de la vista identificador del listado de metadatos.
	 */
	private static final String ATTR_VIEW_LIST_META = "listMeta";
	/**
	 * Atributo de la vista identificador del documento seleccionado.
	 */
	private static final String ATTR_VIEW_DOCUMENT_SELECT = "documentSelect";
	/**
	 * Atributo de la vista identificador del listado de documentos.
	 */
	private static final String ATTR_VIEW_LIST_DOCUMENTS = "listDocuments";

	/**
	 * Vista del modal de la herencia de metadatos del documento.
	 */
	private static final String CONTENT_MODAL_DOC_META_INHER_VIEW = "fragments/admin/tablaModalDocument :: contentModalDocMetaInher";
	/**
	 * Vista del contentido de la tabla de metadatos.
	 */
	private static final String CONTENT_TABLES_METADATA_VIEW = "fragments/admin/content_tabla :: contentTablesMeta";
	/**
	 * Vista de administación de tipos de documentos.
	 */
	private static final String ADMIN_TYPES_DOCUMENTS_VIEW = "admin/typesDocuments";

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DocumentController.class);

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre las operaciones relacionadas con la especificación de documentos.
	 */
	@Autowired
	private DocumentService documentService;

	/**
	 * Pagina de tipos de documento.
	 * 
	 * @param model
	 *            Parámetro para el traspaso de información hacia la vista
	 * @param documentSelectId
	 *            Parámetro con el tipo de documento seleccionado, si existe, en
	 *            la vista
	 * @return Vista con la información asociada a los tipos de documentos
	 */
	@RequestMapping("/typesDocuments")
	public String typesDocuments(ModelMap model, @RequestParam(value = ATTR_VIEW_DOCUMENT_SELECT, required = false) String documentSelectId) {
		LOG.debug("Start typesDocuments");

		List<DocumentSpecificationDTO> documents = new ArrayList<DocumentSpecificationDTO>();

		try {
			// obtenemos el listado de los documentos disponibles en el sistema
			documents = documentService.getListDocSpecification();
		} catch (Exception e) {
			messageException(e);
		}
		model.addAttribute(ATTR_VIEW_LIST_DOCUMENTS, documents);
		// añadimos parametro de visualización para la seleccion visual
		model.addAttribute(ATTR_VIEW_DOCUMENT_SELECT, documentSelectId != null ? documentSelectId : "");

		LOG.debug("End typesDocuments");
		return ADMIN_TYPES_DOCUMENTS_VIEW;
	}

	/**
	 * Frame de la información del documento seleccionado.
	 * 
	 * @param idDocSpec
	 *            id del Documento seleccionado
	 * @param nameDocSpec
	 *            nombre del Documento seleccionado
	 * @param model
	 *            Entidad para traspasar información entre el controlador y la
	 *            vista.
	 * @return nombre de plantilla a mostrar
	 */

	@RequestMapping(value = "/infoDoc", method = RequestMethod.POST)
	public String infoDocument(ModelMap model, String idDocSpec, String nameDocSpec) {
		LOG.debug("infoDocument start");
		List<MetadataSpecificationDTO> listDocSpecSelect = null;
		List<MetadataSpecificationDTO> listDocSpecInherit = new ArrayList<MetadataSpecificationDTO>();

		// obtenemos la informacion de los metadatos asociados al documentos
		try {
			Map<String, List<MetadataSpecificationDTO>> mapListMetadataSpec = documentService.getMapListMetadataSpec(Long.valueOf(idDocSpec));
			Iterator<Entry<String, List<MetadataSpecificationDTO>>> it = mapListMetadataSpec.entrySet().iterator();

			while (it.hasNext()) {
				Map.Entry<String, List<MetadataSpecificationDTO>> pair = (Map.Entry<String, List<MetadataSpecificationDTO>>) it.next();
				if (LOG.isDebugEnabled()) {
					LOG.debug("Pareja obtenida:" + pair.getKey() + " = " + pair.getValue());
				}
				if (pair.getKey().equals(nameDocSpec)) {
					listDocSpecSelect = (List<MetadataSpecificationDTO>) pair.getValue();
				} else {
					listDocSpecInherit.addAll((List<MetadataSpecificationDTO>) pair.getValue());
				}
				it.remove(); // evitamos la concurrencia
			}
		} catch (Exception e) {
			messageException(e);
		}

		model.addAttribute(ATTR_VIEW_LIST_META, listDocSpecSelect);
		model.addAttribute(ATTR_VIEW_LIST_META_INHER, listDocSpecInherit);

		LOG.debug("infoDocument end");
		return CONTENT_TABLES_METADATA_VIEW;
	}

	/**
	 * frame con el listado de Definición de Documento existente en el sistema
	 * sin incluir el seleccionado en pantalla.
	 * 
	 * @param idTypeDefDocument
	 *            id del Documento seleccionado en pantalla.
	 * @param model
	 *            Entidad para traspasar información entre el controlador y la
	 *            vista.
	 * @return nombre de plantilla a mostrar
	 */

	@RequestMapping("/listDefDoc")
	public String listDefDoc(ModelMap model, String idTypeDefDocument) {
		LOG.debug("Start listDefDoc");
		List<DocumentSpecificationDTO> res = new ArrayList<DocumentSpecificationDTO>();

		try {
			// obtenemos el listado de los documentos disponibles en el sistema
			// menos el tipo seleccionado
			List<DocumentSpecificationDTO> documents = documentService.getListDocSpecification();
			// si no tenemos ninguna definicion previa seleccionada
			if (idTypeDefDocument != null && !idTypeDefDocument.isEmpty()) {
				for (DocumentSpecificationDTO aux: documents) {
					if (!aux.getId().equals(Long.valueOf(idTypeDefDocument)))
						res.add(aux);
				}
			} else {
				res = documents;
			}
		} catch (Exception e) {
			messageException(e);
		}

		model.addAttribute(ATTR_VIEW_LIST_DEF_DOC, res);

		LOG.debug("End listDefDoc");
		return CONTENT_MODAL_DOC_META_INHER_VIEW;
	}

	/**
	 * Método para salvaguardar la información relacionada con el Documento
	 * Seleccionado en modo edición.
	 * 
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad para traspasar información entre el controlador y la
	 *            vista.
	 * @param ultimaPagina
	 *            identificador de la ultima página seleccionada.
	 * @param jsonDocSpec
	 *            json con la información modificada del documento seleccionado
	 * @return nombre de plantilla a mostrar
	 */

	@RequestMapping(value = "/saveInfoDoc", method = RequestMethod.POST)
	public String saveInfoDoc(HttpServletRequest request, ModelMap model, String jsonDocSpec, String ultimaPagina) {
		LOG.debug("[WEBSCAN-DOCUMENT] Start saveInfoDoc");

		ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		String[ ] args = new String[1];

		String res = "";

		try {

			res = saveInfoDocAux(request, model, jsonDocSpec, ultimaPagina);

			args[0] = "";
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_RESULT_OK, args, ViewMessageLevel.INFO, WebScope.REQUEST, attrs.getRequest());

		} catch (DocumentException e) {
			LOG.error("[WEBSCAN-DOCUMENT] Se produjo un error al tratar la información recibida");
			args[0] = e.getMessage();
			if (DocumentException.CODE_902.equals(e.getCode())) {
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_METADATA_DUPLICATE_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());
			} else {
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());
			}
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-DOCUMENT] Se produjo un error al guardar la información en base de datos");

			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());
		} catch (Exception e) {
			LOG.error("[WEBSCAN-DOCUMENT] Error inesperado en DocumentController");

			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());
		}

		LOG.debug("[WEBSCAN-DOCUMENT] End saveInfoDoc");
		return res;
	}

	/**
	 * Metodo auxiliar para guardar los cambios de la información recibida de
	 * los documentos.
	 * 
	 * @param request
	 *            Petición realizada a través del navegador
	 * @param model
	 *            Parametro para devolver la información a la vista
	 * @param jsonDocSpec
	 *            Parametro de los cambios ocurridos en la vista de Documentos
	 * @param ultimaPagina
	 *            Última pagina seleccionada en el listado de documentos antes
	 *            de guardar la información
	 * @return Vista resultante tras realizar el tratamiento de la información.
	 * @throws DocumentException
	 *             Exception al tratar la información recibida JSON o
	 *             información no esperada.
	 * @throws DAOException
	 *             Exception al acceder a la información de BBDD en relación con
	 *             los documentos.
	 */
	private String saveInfoDocAux(HttpServletRequest request, ModelMap model, String jsonDocSpec, String ultimaPagina) throws DocumentException, DAOException {
		DocSpecVO infoRec = null;

		String res = null;
		Long idDocSpec = null;
		String nameDocSpec = null;
		String descDocSpec = null;
		Boolean activoDoc = null;
		Boolean propagateAct = Boolean.FALSE;
		String idMetaSelect = null;
		Boolean isBatchId = Boolean.FALSE;
		try {

			LOG.debug("[WEBSCAN-DOCUMENT] Empezamos información de parseo .....");

			JSONObject obj = new JSONObject(jsonDocSpec);
			// si existe
			if (obj.has("idDocSpec")) {
				idDocSpec = obj.getLong("idDocSpec");
			}
			nameDocSpec = obj.getString("nombreDoc");
			descDocSpec = obj.getString("descripDoc");
			activoDoc = obj.getBoolean("activoDoc");
			propagateAct = obj.getBoolean("propagateAct");
			// metadatato identificador documento
			idMetaSelect = obj.getString("idBatchDoc");

			JSONArray metasDocSpec = obj.getJSONArray("metas");
			JSONArray defHerDocSpec = obj.getJSONArray("defHer");

			LOG.debug("[WEBSCAN-DOCUMENT] Ini Informacion parseada");
			LOG.debug("[WEBSCAN-DOCUMENT] idDocSpec:", idDocSpec);
			LOG.debug("[WEBSCAN-DOCUMENT] nameDocSpec:", nameDocSpec);
			LOG.debug("[WEBSCAN-DOCUMENT] descDocSpec:", descDocSpec);
			LOG.debug("[WEBSCAN-DOCUMENT] activoDoc:", activoDoc);
			LOG.debug("[WEBSCAN-DOCUMENT] metasDocSpec:", metasDocSpec);
			LOG.debug("[WEBSCAN-DOCUMENT] defHerDocSpec", defHerDocSpec);
			LOG.debug("[WEBSCAN-DOCUMENT] propagateAct", propagateAct);

			LOG.debug("[WEBSCAN-DOCUMENT] Obtenida toda la inforamción parseada inicial....");
			// retornamos la pantalla del documento

			// creamos el objeto
			infoRec = new DocSpecVO(idDocSpec, nameDocSpec, descDocSpec, activoDoc, null, null, propagateAct);

			LOG.debug("[WEBSCAN-DOCUMENT] Obtenemos los metadatos ....");

			List<MetasDocSpecVO> metadatos = new ArrayList<MetasDocSpecVO>();
			// obtenemos los metaDatos de especificacion de documentos
			for (int i = 0; i < metasDocSpec.length(); i++) {
				JSONObject metas = metasDocSpec.getJSONObject(i);
				// Long orgUnitId = unidades.getJSONObject(i).getLong("id");
				Long id = null;
				if (!metas.isNull("id")) {
					id = metas.getLong("id");
				}
				// comprobamos si es el metadato elegido como identificador de
				// documento
				String nombre = metas.getString("nombre");
				isBatchId = Boolean.FALSE;
				if (idMetaSelect.equals(nombre)) {
					isBatchId = Boolean.TRUE;
				}
				String descripcion = metas.getString("desc");
				Boolean editable = metas.getBoolean("edit");
				Boolean requerido = metas.getBoolean("req");
				Boolean caratula = metas.getBoolean("cover");
				String valorDefecto = metas.getString("valDef");
				String listVal = metas.getString("listVal");

				validateSourceValuesList(listVal, nombre);

				MetasDocSpecVO aux = new MetasDocSpecVO(id, nombre, descripcion, editable, requerido, caratula, valorDefecto, listVal, isBatchId);
				metadatos.add(aux);
			}

			infoRec.setMetasDocSpec(metadatos);
			LOG.debug("[WEBSCAN-DOCUMENT] Fin obtención de los metadatos ....");

			LOG.debug("[WEBSCAN-DOCUMENT] Obtenemos los metadatos heredados....");

			List<DocSpecDefInherVO> inherDocSpec = new ArrayList<DocSpecDefInherVO>();
			// obtenemos los metaDatos de especificacion de documentos heredados
			for (int i = 0; i < defHerDocSpec.length(); i++) {
				JSONObject def = defHerDocSpec.getJSONObject(i);

				Long idMeta = def.getLong("idMeta");
				String specDoc = def.getString("spec");

				DocSpecDefInherVO aux = new DocSpecDefInherVO(idMeta, specDoc);
				inherDocSpec.add(aux);
			}

			infoRec.setDefHerDocSpec(inherDocSpec);

			LOG.debug("[WEBSCAN-DOCUMENT] Fin obtención de los metadatos heredados....");

			// realizamos la persistencia de los datos asociados a la definición
			// del
			// documento
			LOG.debug("[WEBSCAN-DOCUMENT] Realiazamos la persistencia de la información de Documentacion ....");
			DocumentSpecificationDTO docSpecRes = documentService.updateDocSpec(request, infoRec);
			LOG.debug("[WEBSCAN-DOCUMENT] Fin Realiazamos la persistencia de la información de Documentacion ....");

			LOG.debug("[WEBSCAN-DOCUMENT] Obtenemos la visualizacion para la respuesta ....");
			model.addAttribute(ATTR_VIEW_ULTIMO_ELTO_SELECT, docSpecRes.getId());
			model.addAttribute(ATTR_VIEW_ULTIMA_PAGINA, ultimaPagina);
			res = typesDocuments(model, null);
			LOG.debug("[WEBSCAN-DOCUMENT] Finalizada la obtención de la visualizacion para la respuesta ....");

		} catch (JSONException e) {
			LOG.error("[WEBSCAN-DOCUMENT] Error de parseo de información recibida");
			throw new DocumentException(DocumentException.CODE_900, "Error de parseo de información recibida");
		} catch (DocumentException e) {
			throw e;
		} catch (DAOException e) {
			if (DAOException.CODE_998.equals(e.getCode())) {
				throw new DocumentException(DocumentException.CODE_900, "Error de formato:" + e.getMessage());
			} else {
				throw e;
			}
		}

		return res;
	}

	/**
	 * Valida que la lista de valores de un metadato esté bien formada (JSON),
	 * en caso de ser informada.
	 * 
	 * @param sourceValuesList
	 *            Cadena de carácteres que representa la lista de valores.
	 * @param metadataName
	 *            nombre de metadato.
	 * @throws DocumentException
	 *             Si no está bien formada la lista de valores.
	 */
	private void validateSourceValuesList(String sourceValuesList, String metadataName) throws DocumentException {
		String aux;
		String excMsg;
		try {
			if (sourceValuesList != null && !sourceValuesList.isEmpty()) {
				aux = "{\"value\":" + sourceValuesList + "}";

				ObjectMapper mapper = new ObjectMapper();

				MetadataValueList metadataValueList = mapper.readValue(aux, MetadataValueList.class);

				LOG.debug("Lista de valores correctamente formada. Numero de elementos: {}", metadataValueList.getValue().size());
			}
		} catch (JsonParseException e) {
			excMsg = "Error en el parseo de la información recibida, lista de valores del metadato " + metadataName + " mal formada.";
			LOG.error("[WEBSCAN-DOCUMENT] {}", excMsg);
			throw new DocumentException(DocumentException.CODE_902, excMsg);
		} catch (JsonMappingException e) {
			excMsg = "Error en el parseo de la información recibida, lista de valores del metadato " + metadataName + " mal formada.";
			LOG.error("[WEBSCAN-DOCUMENT] {}", excMsg);
			throw new DocumentException(DocumentException.CODE_902, excMsg);
		} catch (IOException e) {
			excMsg = "Error en el parseo de la información recibida, lista de valores del metadato " + metadataName + " mal formada.";
			LOG.error("[WEBSCAN-DOCUMENT] {}", excMsg);
			throw new DocumentException(DocumentException.CODE_902, excMsg);
		}
	}

	/**
	 * Metodo auxiliar para eliminar la información relacionada con una
	 * especificación de documentos.
	 * 
	 * @param request
	 *            Parametro de la petición del navegador
	 * @param model
	 *            Parametro para devolver la información a la vista
	 * @param idDocSpec
	 *            Parametro identificador de la especificación del documento a
	 *            eliminar
	 * @return Vista resultante después de realizar la operación de eliminación.
	 */
	@RequestMapping(value = "/removeDoc", method = RequestMethod.POST)
	public String removeDoc(HttpServletRequest request, ModelMap model, String idDocSpec) {
		LOG.debug("Start removeDoc");

		ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		String[ ] args = new String[1];
		String res = null;

		try {

			removeDocAux(request, Long.valueOf(idDocSpec));
			args[0] = "";
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_RESULT_OK, args, ViewMessageLevel.INFO, WebScope.REQUEST, attrs.getRequest());

		} catch (NumberFormatException e) {
			LOG.error("[WEBSCAN-DOCUMENT] Se produjo un error al tratar la información recibida");
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());

		} catch (DocumentException e) {
			// LOG.error("[WEBSCAN-DOCUMENT] Se produjo un error al tratar la
			// información recibida");
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());

		} catch (DAOException e) {
			LOG.error("[WEBSCAN-DOCUMENT] Se produjo un error al acceder a base de datos");
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());

		}

		res = typesDocuments(model, null);

		LOG.debug("End removeDoc");
		return res;
	}

	/**
	 * Método auxiliar para eliiminar la información asociada al id de la
	 * especificación de documento pasada como parametro.
	 * 
	 * @param idDocSpec
	 *            id de la especificación de documento
	 * @param request
	 *            Petición http.
	 * @throws DocumentException
	 *             Excepción tratada por error al intentar verificar el
	 *             idDocSpec o bien al existir documentos al vuelo
	 * @throws DAOException
	 *             Excepción al realizar cualquier consulta o cambio en base de
	 *             datos
	 */
	private void removeDocAux(HttpServletRequest request, Long idDocSpec) throws DAOException, DocumentException {
		if (idDocSpec != null) {
			LOG.debug("Procedemos a eliminar la info de documento:", idDocSpec);
			// comprobamos que no exista documentos al vuelo
			Boolean checkNotDocumentFlyng = documentService.checkNotDocumentFlyng(idDocSpec);

			if (checkNotDocumentFlyng) {
				documentService.removeDocSpec(request, idDocSpec);
			} else {
				if (LOG.isErrorEnabled()) {
					LOG.error("[WEBSCAN-DOCUMENT] idDocSpec " + idDocSpec + " tiene documento escaneado al vuelo");
				}
				throw new DocumentException(DocumentException.CODE_901, "idDocSpec " + idDocSpec + " tiene documento escaneado al vuelo");

			}

		} else {

			LOG.error("[WEBSCAN-DOCUMENT] idDocSpec no válido");
			throw new DocumentException(DocumentException.CODE_900, "idDocSpec no válido");
		}
	}

	/**
	 * Método para obtener los metadatos asociados a una especificación de
	 * documento según su id de especificación.
	 * 
	 * @param idDocsSpec
	 *            listado con los identificadores de la especificación de
	 *            documentos.
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @return String con la información de los metadatos asociados.
	 */

	@RequestMapping(value = "/getMetadatosDocSpecByidDocSpec", method = RequestMethod.GET)
	public @ResponseBody String getMetadatosDocSpecByidDocSpec(ModelMap model, @RequestParam(value = "idDocsSpec") List<Long> idDocsSpec) {
		LOG.debug("Start getMetadatosDocSpecByidDocSpec");
		String res;
		List<MetadataSpecificationDTO> listMetadatos = new ArrayList<MetadataSpecificationDTO>();

		try {
			LOG.debug("idDocsSpec:", idDocsSpec);
			// obtenemos la informacion de los metadatos asociados al documentos

			for (Long idDocSpec: idDocsSpec) {
				Map<String, List<MetadataSpecificationDTO>> mapListMetadataSpec = documentService.getMapListMetadataSpec(idDocSpec);

				// prepamos el listado de todos los metadatos heredados
				// incluídos
				for (Map.Entry<String, List<MetadataSpecificationDTO>> entidad: mapListMetadataSpec.entrySet()) {
					listMetadatos.addAll(entidad.getValue());
				}
			}
		} catch (Exception e) {
			messageException(e);
		}

		res = convert(listMetadatos);

		LOG.debug("listMetadatos size:", listMetadatos.size());
		LOG.debug("End getMetadatosDocSpecByidDocSpec");
		return res;
	}

	/**
	 * Funcion auxiliar para convertir de List a Array.
	 * 
	 * @param source
	 *            Listado de un tipo T
	 * @param <T>
	 *            tipo del listado
	 * @return Array JSON del listado pasado como fuente
	 */

	public <T> String convert(List<T> source) {
		return new JSONArray(source).toString();
	}

	/**
	 * Función auxiliar para mostrar mensajes de exception en el front.
	 * 
	 * @param e
	 *            Exception recibida a mostrar
	 */

	private void messageException(Exception e) {
		String args[] = new String[0];
		ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		args[0] = e.getMessage();
		BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());
	}

}
