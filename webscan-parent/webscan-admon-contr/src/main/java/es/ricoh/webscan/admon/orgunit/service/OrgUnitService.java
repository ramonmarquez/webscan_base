/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.admon.orgunit.Service.OrgUnitService.java.</p>
 * <b>Descripción:</b><p> Servicio que agrupa operaciones de consulta, creación y modificación sobre
 * usuarios del sistema.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.admon.orgunit.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import es.ricoh.webscan.admon.orgunit.vo.FormOrganizationVO;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitTreeDTO;
import es.ricoh.webscan.model.service.AuditService;

/**
 * Implementación de la lógica de negocio de las unidades organizativas.
 * <p>
 * Class OrgUnitService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class OrgUnitService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(OrgUnitService.class);

	/**
	 * Atributo de la vista identificativa del nodo seleccionado del árbol de
	 * unidades organizativas.
	 */
	private static final String ATTR_VIEW_NODO_SELECT = "nodoSelect";
	/**
	 * Atributo de la vista identificativa del árbol de unidades organizativas.
	 */
	private static final String ATTR_VIEW_DATOS = "datos";

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre el modelo de datos.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Servicio interno para el registro y consulta de trazas y eventos de
	 * auditoría del módulo de gestión de usuarios de Webscan.
	 */
	private AuditService auditService;

	/**
	 * Obtiene un String formato JSON con la información de la unidad
	 * organizativa pasada como parámetro.
	 * 
	 * @param elto
	 *            Objeto de la Unidad Organizativa
	 * @return String con la información JSON
	 */

	private String getJsonOrgUnit(OrganizationUnitDTO elto) {
		StringBuilder cadena = new StringBuilder();
		cadena.append("{");
		cadena.append("'Id':'" + elto.getId());
		cadena.append("',");
		cadena.append("'externalId':'" + elto.getExternalId());
		cadena.append("',");
		cadena.append("'name':'" + elto.getName());
		cadena.append("',");
		cadena.append("'parentId':'" + (elto.getParentId() == null ? "null" : elto.getParentId()));
		// añadimos el id de la relacion con ScanProfile si existe sino es null
		// fin añadir id relacion
		cadena.append("'}");

		return cadena.toString();
	}

	/**
	 * Obtiene los nodos hijos del arbol de Unidades Organizativas.
	 * 
	 * @param eltos
	 *            Listado de las Unidades Organizativas
	 * @param datos
	 *            Map con la información de los hijos de las Unidades
	 *            Organizativas pasadas como parámetro
	 * @throws DAOException
	 *             Exception producida al acceder a la información de la BBDD.
	 */

	private void getChildResponse(List<OrganizationUnitTreeDTO> eltos, Map<String, String> datos) throws DAOException {

		// recoremos los hijos
		for (OrganizationUnitTreeDTO aux: eltos) {
			OrganizationUnitDTO elto = aux.getOrganizationUnit();
			String cadena = getJsonOrgUnit(elto);
			datos.put(String.valueOf(datos.size()), cadena);

			// obtenemos los hijos del nodo actual de modo recursivo
			getChildResponse(aux.getChildOrganizationUnits(), datos);

		}
	}

	/**
	 * Obtiene los nodos del arbol de Unidades Organizativas.
	 * 
	 * @param datos
	 *            Map con la información asociada a los nodos del arbol de
	 *            unidades organizativas
	 * @throws DAOException
	 *             Exception en BBDD que se produce al recuperar los elementos
	 *             de las unidades organizativas
	 */

	public void getTreeResponse(Map<String, String> datos) throws DAOException {
		LOG.debug("OrganizationService getTreeResponse start");

		// obtenemos las raices
		List<OrganizationUnitTreeDTO> unidades = new ArrayList<OrganizationUnitTreeDTO>();

		unidades = webscanModelManager.getOrganizationUnitsTree();

		for (OrganizationUnitTreeDTO aux: unidades) {

			OrganizationUnitDTO elto = aux.getOrganizationUnit();
			String cadena = getJsonOrgUnit(elto);
			datos.put(String.valueOf(datos.size()), cadena);

			// obtenemos los hijos del nodo actual de modo recursivo
			getChildResponse(aux.getChildOrganizationUnits(), datos);
		}

		LOG.debug("OrganizationService getTreeResponse end");
	}

	/**
	 * Actualiza, Crea o Elimina la información asociada a una unidad
	 * organizativa.
	 * 
	 * @param unit
	 *            Objeto con la información de la unidad organizativa
	 * @param request
	 *            Petición http.
	 * @throws DAOException
	 *             Exception que se puede producir al acceder a BBDD.
	 */

	public void setUnitOrganization(HttpServletRequest request, OrganizationUnitDTO unit) throws DAOException {
		LOG.debug("OrganizationService setUnitOrganization ini");
		Date authTimestamp = new Date();
		String username = "";
		AuditOperationName opName;
		AuditEventName evName;

		// obtenemos la info para auditoria
		UserInSession user = BaseControllerUtils.getUserInSession(request);
		username = user.getUsername();

		if (unit.getId() != null) {
			webscanModelManager.updateOrganizationUnit(unit);
			LOG.debug("OrganizationService setUnitOrganization ACTUALIZA");
			opName = AuditOperationName.ORG_UNIT_UPDATE;
			evName = AuditEventName.ORG_UNIT_UPDATE;
		} else {
			Long unidadNueva = webscanModelManager.createOrganizationUnit(unit);
			LOG.debug("OrganizationService setUnitOrganization CREADA unidad organizativa: ID -", unidadNueva);
			opName = AuditOperationName.ORG_UNIT_CREATE;
			evName = AuditEventName.ORG_UNIT_UPDATE;

		}
		LOG.debug("salvamos la información en auditoria");

		auditService.createAuditLogs(username, AuditOpEventResult.OK, authTimestamp, null, opName, evName);

		LOG.debug("OrganizationService setUnitOrganization end");

	}

	/**
	 * Eliminamos la información completa de una Unidad Organizativa.
	 * 
	 * @param id
	 *            Id de la Unidad Organizativa que vamos a eliminar.
	 * @param request
	 *            Petición http.
	 * @throws DAOException
	 *             Si no existe la Unidad Organizativa, o hay algún problema con
	 *             la BBDD.
	 */

	public void setUnitOrganizationRemove(HttpServletRequest request, Long id) throws DAOException {
		LOG.debug("OrganizationService setUnitOrganizationRemove ini");
		Date authTimestamp = new Date();
		String username = "";

		// obtenemos la info para auditoria
		UserInSession user = BaseControllerUtils.getUserInSession(request);
		username = user.getUsername();

		webscanModelManager.removeOrganizationUnit(id);

		// registramos la auditoria
		auditService.createAuditLogs(username, AuditOpEventResult.OK, authTimestamp, null, AuditOperationName.ORG_UNIT_DELETE, AuditEventName.ORG_UNIT_DELETE);

		// no necesario eliminar la relación de los usuarios con la unidad
		// organizativa eliminada ya lo hace el método
		LOG.debug("OrganizationService setUnitOrganizationRemove fin");
	}

	/**
	 * Obtenemos la información completa de una Unidad Organizativa.
	 * 
	 * @param aid
	 *            Id de una unidad organizativa.
	 * @return OrganizationUnitDTO Unidad organizativa.
	 * @throws DAOException
	 *             Si no existe la Unidad Organizativa, o hay algún problema con
	 *             la BBDD.
	 */

	public OrganizationUnitDTO getOrganizationUnitId(Long aid) throws DAOException {
		return webscanModelManager.getOrganizationUnit(aid);
	}

	/**
	 * Comprueba si una unidad organizativa, o alguna de sus hijas poseen
	 * documentos en proceso de digitalización.
	 * 
	 * @param orgUnitId
	 *            Identificador de unidad organizativa.
	 * @return Si posee documentos en proceso de digitalización, retorna true.
	 *         En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             unidad organizativa, o se produce algún error en la ejecución
	 *             de la sentencia de BBDD.
	 */
	public Boolean hasUnitOrgDocsInScanProccess(Long orgUnitId) throws DAOException {
		return webscanModelManager.hasUnitOrgDocsInScanProccess(orgUnitId);
	}

	/**
	 * Función auxiliar para enviar mensaje al front del user.
	 * 
	 * @param webScope
	 *            Ámbito del mensaje
	 * @param viewMessageLevel
	 *            Nivel de aviso o información
	 * @param admonWebscanConstanstResult
	 *            Constante asociado al mensaje a mostrar que será traducido a
	 *            lenguaje
	 * @param mensajeAux
	 *            String con la información auxiliar del mensaje.
	 */
	public void messageSystem(WebScope webScope, ViewMessageLevel viewMessageLevel, String admonWebscanConstanstResult, String mensajeAux) {
		try {
			ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			String[ ] args = new String[1];
			args[0] = mensajeAux != null ? mensajeAux : "";

			BaseControllerUtils.addCommonMessage(admonWebscanConstanstResult, args, viewMessageLevel, webScope, attrs.getRequest());
		} catch (Exception e) {
			LOG.error("Error al mostrar mensajes:", e.toString());

		}

	}

	/**
	 * Obtiene el map de las unidades organizativas.
	 * 
	 * @return map con las unidades organizativas
	 * @throws DAOException
	 *             Exception que se produce al recuperar la información de BBDD.
	 */
	public Map<String, String> getArbol() throws DAOException {
		Map<String, String> datos = new HashMap<String, String>();

		getTreeResponse(datos);

		return datos;
	}

	/**
	 * Obtiene el la entidad de Unidad organizativa pasandole los datos del form
	 * que recibimos de la vista.
	 * 
	 * @param formData
	 *            información recogida de la vista con respecto a la unidad
	 *            organizativa
	 * @return OrganizationUnitDTO con la unidades organizativa
	 * @throws Exception
	 *             Exception producida al intentar rellenar la información de la
	 *             unidad organizativa.
	 */

	public OrganizationUnitDTO fillOrgUnit(FormOrganizationVO formData) throws Exception {
		OrganizationUnitDTO res;

		// existe ya la organización
		if (!StringUtils.isEmpty(formData.getIdIdentificador())) {
			res = getOrganizationUnitId(Long.valueOf(formData.getIdIdentificador()));

		} else // es nueva
		{
			res = new OrganizationUnitDTO();
			// pendiente de ver que hacemos al
		}
		if (!StringUtils.isEmpty(formData.getParentSelect())) {
			res.setParentId(Long.valueOf(formData.getParentSelect()));
		} else {
			res.setParentId(null);
		}
		res.setExternalId(formData.getIdentificador());
		res.setName(formData.getNombre());

		return res;
	}

	/**
	 * Prepara los atributos que va recibir la vista de unidades organizativas.
	 * 
	 * @param formData
	 *            información recogida de la vista con respecto a la unidad
	 *            organizativa
	 * @param model
	 *            parametro de salida donde informamos los datos que mostraremos
	 *            en la vista de la unidad organizativa
	 * @throws DAOException
	 *             Exception producidar al acceder a la información de BBDD.
	 */

	public void refreshOrgUnitTree(FormOrganizationVO formData, ModelMap model) throws DAOException {
		// recuperamos la información de la pantalla
		Map<String, String> arbol = getArbol();
		model.addAttribute(ATTR_VIEW_DATOS, arbol);
		model.addAttribute(ATTR_VIEW_NODO_SELECT, formData.getParentSelect());
	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Establece el servicio interno para el registro y consulta de trazas y
	 * eventos de auditoría de Webscan.
	 * 
	 * @param anAuditManager
	 *            nuevo servicio interno para el registro y consulta de trazas y
	 *            eventos de auditoría de Webscan.
	 */
	public void setAuditService(AuditService anAuditManager) {
		this.auditService = anAuditManager;
	}

}
