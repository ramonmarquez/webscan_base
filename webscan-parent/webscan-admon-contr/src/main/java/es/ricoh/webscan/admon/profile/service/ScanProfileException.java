/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.ScanProfileDAOException.java.</p>
 * <b>Descripción:</b><p> Excepción que encapsula los errores producidos en el
 * componente DAO el modelo de datos de gestión de usuarios de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.admon.profile.service;

import es.ricoh.webscan.model.DAOException;

/**
 * Excepción que encapsula los errores producidos en el componente DAO el modelo
 * de datos de gestión de usuarios de Webscan.
 * <p>
 * Clase ScanProfileDAOException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ScanProfileException extends DAOException {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Error al comprobar los documentos deshabilitado o eliminado con
	 * documentos en vuelo.
	 */
	public static final String CODE_001 = "COD_001";

	/**
	 * Error al comprobar los plugins deshabilitado.
	 */
	public static final String CODE_002 = "COD_002";

	/**
	 * Error al comprobar que exista al menos un tipo documental activo en cada
	 * relacion de unidad organizativa y perfil.
	 */
	public static final String CODE_003 = "COD_003";

	/**
	 * Error al comprobar que la unidad organizativa ha querido ser eliminada y
	 * tiene documentos en vuelo.
	 */
	public static final String CODE_004 = "COD_004";

	/**
	 * Error al comprobar que la desvinculacion del documento asociado a un
	 * perfil y unidad organizativa tiene documentos en vuelo.
	 */
	public static final String CODE_005 = "COD_005";

	/**
	 * Error al comprobar que exista al menos una configuración mínima del
	 * perfil.
	 */
	public static final String CODE_006 = "COD_006";

	/**
	 * Fase no definida correctamente en el Locale de nuestra aplicación .
	 */
	public static final String CODE_1000 = "COD_1000";

	/**
	 * Constructor sin argumentos.
	 */
	public ScanProfileException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public ScanProfileException(String aCode, String aMessage) {
		super(aCode, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public ScanProfileException(String aMessage) {
		super(CODE_999, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public ScanProfileException(Throwable aCause) {
		super(CODE_999, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public ScanProfileException(final String aMessage, Throwable aCause) {
		super(CODE_999, aMessage, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public ScanProfileException(final String aCode, final String aMessage, Throwable aCause) {
		super(aCode, aMessage, aCause);
	}

}
