/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.profile.controllers.vo;

import java.util.List;

/**
 * Entidad que usamos para visualizar la información asociada a la relación
 * entre perfil de escaneo y unidades organizativas.
 * <p>
 * Class ScanProfileOrgUnitVO.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ScanProfileOrgUnitVO {

	/**
	 * identificador de la entidad que relaciona el perfil de escaneo y la
	 * unidad organizativa.
	 */
	private Long id;
	/**
	 * listado de documentos asociados al perfil de escaneo.
	 */
	private List<ScanProfileDocumentsVO> documentos;
	/**
	 * listado de plugins asociados al perfil de escaneo y la unidad
	 * organizativa.
	 */
	private List<ScanProfileOrgUnitPluginVO> plugins;

	/**
	 * Gets the value of the attribute {@link #id}.
	 * 
	 * @return the value of the attribute {@link #id}.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the value of the attribute {@link #id}.
	 * 
	 * @param aid
	 *            The value for the attribute {@link #id}.
	 */
	public void setId(Long aid) {
		this.id = aid;
	}

	/**
	 * Gets the value of the attribute {@link #documentos}.
	 * 
	 * @return the value of the attribute {@link #documentos}.
	 */
	public List<ScanProfileDocumentsVO> getDocumentos() {
		return documentos;
	}

	/**
	 * Sets the value of the attribute {@link #documentos}.
	 * 
	 * @param adocumentos
	 *            The value for the attribute {@link #documentos}.
	 */
	public void setDocumentos(List<ScanProfileDocumentsVO> adocumentos) {
		this.documentos = adocumentos;
	}

	/**
	 * Gets the value of the attribute {@link #plugins}.
	 * 
	 * @return the value of the attribute {@link #plugins}.
	 */
	public List<ScanProfileOrgUnitPluginVO> getPlugins() {
		return plugins;
	}

	/**
	 * Sets the value of the attribute {@link #plugins}.
	 * 
	 * @param aplugins
	 *            The value for the attribute {@link #plugins}.
	 */
	public void setPlugins(List<ScanProfileOrgUnitPluginVO> aplugins) {
		this.plugins = aplugins;
	}
}
