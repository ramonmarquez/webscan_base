/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.document.controllers.vo;

/**
 * Entidad que usamos para la visualización de la especificación de parámetro.
 * <p>
 * Class ParamSpecVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

public class ParamSpecVO {

	/**
	 * Identificador de la especificación de parámetro.
	 */
	private Long id;
	/**
	 * Nombre de la especificación de parámetro.
	 */
	private String name;
	/**
	 * Descripción de la especificación de parámetro.
	 */
	private String description;
	/**
	 * Obligatoriedad de la especificación de parámetro.
	 */
	private Boolean mandatory;
	/**
	 * Identificador del tipo de datos de la especificación de parámetro.
	 */
	private Long dataTypeId;
	/**
	 * Especificación del tipo de parametro. {@inheritDoc}
	 * 
	 * @see ParamDataTypeSpecVO.
	 */
	private ParamDataTypeSpecVO typeParamSpec;

	/**
	 * Constructor method for the class ParamSpecVO.java.
	 * 
	 * @param aid
	 *            Identificador.
	 * @param aname
	 *            Nombre.
	 * @param adescription
	 *            Descripción.
	 * @param amandatory
	 *            Obligatoriedad del parámetro.
	 * @param adataTypeId
	 *            Identificador del tipo de dato.
	 * @param atypeParamSpec
	 *            Especificación del tipo de parámetro.
	 */
	public ParamSpecVO(Long aid, String aname, String adescription, Boolean amandatory, Long adataTypeId, ParamDataTypeSpecVO atypeParamSpec) {
		super();
		this.id = aid;
		this.name = aname;
		this.description = adescription;
		this.mandatory = amandatory;
		this.dataTypeId = adataTypeId;
		if (atypeParamSpec != null)
			this.typeParamSpec = atypeParamSpec;
	}

	/**
	 * Obtiene el identificador de la especificación del parámetro.
	 * 
	 * @return identificador.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Modifica el identificador de la especificación del parámetro.
	 * 
	 * @param aid
	 *            identificador.
	 */
	public void setId(Long aid) {
		this.id = aid;
	}

	/**
	 * Obtiene el nombre de la especificación del parámetro.
	 * 
	 * @return String con el nombre.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Modifica el nombre de la especificación del parámetro.
	 * 
	 * @param aname
	 *            String con el nombre.
	 */
	public void setName(String aname) {
		this.name = aname;
	}

	/**
	 * Obtiene la descripción de la especificación del parámetro.
	 * 
	 * @return String con la descripción.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Modifica la descripción de la especificación del parámetro.
	 * 
	 * @param adescription
	 *            String con la descripción.
	 */
	public void setDescription(String adescription) {
		this.description = adescription;
	}

	/**
	 * Obtiene si es o no obligatorio el parámetro.
	 * 
	 * @return Boolean indicando la obligatoriedad o no.
	 */
	public Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * Modifica si es o no obligatorio el parámetro.
	 * 
	 * @param amandatory
	 *            Boolean indicando si es o no obligatorio.
	 */
	public void setMandatory(Boolean amandatory) {
		this.mandatory = amandatory;
	}

	/**
	 * Obtiene el identificador del tipo de dato del parámetro.
	 * 
	 * @return identificador del tipo de dato.
	 */
	public Long getDataTypeId() {
		return dataTypeId;
	}

	/**
	 * Modifica el identificador del tipo de dato del parámetro.
	 * 
	 * @param adataTypeId
	 *            identificador del tipo de dato.
	 */
	public void setDataTypeId(Long adataTypeId) {
		this.dataTypeId = adataTypeId;
	}

	/**
	 * Obtiene el tipo de la especificación del parámetro.
	 * 
	 * @return Entidad asociada al tipo de dato de la especificación del
	 *         parámetro.
	 */
	public ParamDataTypeSpecVO getTypeParamSpec() {
		return typeParamSpec;
	}

	/**
	 * Modifica el tipo de la especificación del parámetro.
	 * 
	 * @param atypeParamSpec
	 *            Entidad asociada al tipo de dato de la especificación de
	 *            parámetro.
	 */
	public void setTypeParamSpec(ParamDataTypeSpecVO atypeParamSpec) {
		this.typeParamSpec = atypeParamSpec;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParamSpecVO [id=" + id + ", name=" + name + ", description=" + description + ", mandatory=" + mandatory + ", dataTypeId=" + dataTypeId + "]";
	}

}
