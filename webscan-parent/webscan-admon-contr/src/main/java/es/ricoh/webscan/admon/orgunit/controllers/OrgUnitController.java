/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.admon.orgunit.controllers.OrgUnitController.java.</p>
 * <b>Descripción:</b><p> Controlador que agrupa todo lo relacionado con las unidades organizativas y su gestión.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.admon.orgunit.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.ricoh.webscan.admon.AdmonWebscanConstants;
import es.ricoh.webscan.admon.orgunit.service.OrgUnitService;
import es.ricoh.webscan.admon.orgunit.vo.FormOrganizationVO;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;

/**
 * Controlador de Unidades Organizativas.
 * <p>
 * Class OrgUnitController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

@Controller
@RequestMapping("/admin")
public class OrgUnitController {

	/**
	 * Atributo de la vista que identifica el nodo seleccionado del arbol de
	 * unidades organizativas.
	 */
	private static final String ATTR_VIEW_NODO_SELECT = "nodoSelect";
	/**
	 * Atributo de la vista que identifica el árbol de unidades organizativas.
	 */

	private static final String ATTR_VIEW_DATOS = "datos";

	/**
	 * conjunto de vistas de las diferentes llamadas.
	 */
	private static final String ADMIN_ORGANIZATION_UNIT_CONTENT_VIEW = "admin/organizationUnit :: content";

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(OrgUnitController.class);

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre las operaciones relacionadas con las Unidades Organizativas.
	 */
	@Autowired
	protected OrgUnitService orgUnitService;

	/**
	 * Página de Unidades Organizativas.
	 * 
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @return Vista resultante de la petición de la página de unidades
	 *         organizativas.
	 */
	@RequestMapping(value = "/organizationUnit", method = RequestMethod.GET)
	public String organismosParam(HttpServletRequest request, ModelMap model) {
		LOG.debug("organizationUnitController organizationUnit start");
		String args[] = new String[1];
		Map<String, String> arbol = new HashMap<String, String>();

		try {
			arbol = orgUnitService.getArbol();
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.debug("[PluginController] Error Acceso a BBDD ");
		}

		model.addAttribute(ATTR_VIEW_DATOS, arbol);
		model.addAttribute(ATTR_VIEW_NODO_SELECT, null);

		LOG.debug("organizationUnitController organizationUnit end");
		return ADMIN_ORGANIZATION_UNIT_CONTENT_VIEW;
	}

	/**
	 * Guarda la información asociada a una unidad organizativa, y retornando la
	 * pagina de Unidad Organizativa.
	 * 
	 * @param request
	 *            Petición http.
	 * @param formData
	 *            Entidad con la información recogida en pantalla respecto a una
	 *            unidad organizativa
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @return nombre de plantilla a mostrar
	 */

	@RequestMapping(value = "/organizationUnit", method = RequestMethod.POST)
	public String organismosParamSave(HttpServletRequest request, ModelMap model, @ModelAttribute FormOrganizationVO formData) {
		Boolean error = Boolean.FALSE;
		OrganizationUnitDTO orgUnit;
		String mensajeWarningDefault = AdmonWebscanConstants.ADMON_ORG_UNIT_NO_INFO;

		LOG.debug("organizationUnitController organizationUnitSave start");
		try {
			if (formData.getAction().equals("delete")) {
				// verificamos que el valor del identificador sea válido
				if (!StringUtils.isEmpty(formData.getIdIdentificador())) {
					if (!orgUnitService.hasUnitOrgDocsInScanProccess(Long.valueOf(formData.getIdIdentificador()))) {
						orgUnitService.setUnitOrganizationRemove(request, Long.valueOf(formData.getIdIdentificador()));
					} else {

						error = Boolean.TRUE;
						mensajeWarningDefault = AdmonWebscanConstants.ADMON_DOCUMENT_IN_FLY;
					}
				} else {
					// mostramos mensaje de warning al no informar el
					// identificador
					error = Boolean.TRUE;
					mensajeWarningDefault = AdmonWebscanConstants.ADMON_ORG_UNIT_ID_NO_EXIT;

				}
			} else {
				orgUnit = orgUnitService.fillOrgUnit(formData);
				if (LOG.isInfoEnabled()) {
					LOG.info("INI Enviamos dato a guardar: id-" + orgUnit.getId() + ", parentID-" + orgUnit.getParentId() + ", idExterno-" + orgUnit.getExternalId() + ", name-" + orgUnit.getName());
				}

				orgUnitService.setUnitOrganization(request, orgUnit);

				if (LOG.isInfoEnabled()) {
					LOG.info("FIN Enviamos dato a guardar: id-" + orgUnit.getId() + ", parentID-" + orgUnit.getParentId() + ", idExterno-" + orgUnit.getExternalId() + ", name-" + orgUnit.getName());
				}
			}

			// Actualización de información en pantalla
			orgUnitService.refreshOrgUnitTree(formData, model);
			if (error) {
				orgUnitService.messageSystem(WebScope.REQUEST, ViewMessageLevel.WARN, mensajeWarningDefault, "");
			} else {
				orgUnitService.messageSystem(WebScope.REQUEST, ViewMessageLevel.INFO, AdmonWebscanConstants.ADMON_RESULT_OK, "");
			}

		} catch (Exception e) {
			orgUnitService.messageSystem(WebScope.REQUEST, ViewMessageLevel.ERROR, AdmonWebscanConstants.ADMON_DEFAULT_ERROR, e.toString());
		}

		LOG.debug("organizationUnitController organizationUnitSave end");
		return ADMIN_ORGANIZATION_UNIT_CONTENT_VIEW;
	}
}
