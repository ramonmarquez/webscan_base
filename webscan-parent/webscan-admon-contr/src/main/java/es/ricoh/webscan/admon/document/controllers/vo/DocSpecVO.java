/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.document.controllers.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Entidad que usamos para trabajar con la vista de especificación de
 * documentos.
 * <p>
 * Class DocSpecVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class DocSpecVO implements Serializable {

	/**
	 * Attribute that represents serial de clase .
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de especificación de documentos.
	 */
	private Long idDocSpec;
	/**
	 * Identificador del nombre de la especificación de documentos.
	 */
	private String nameDocSpec;
	/**
	 * Identificador de la descripción de especificación de documentos.
	 */
	private String descDocSpec;
	/**
	 * Identificador de documento activo.
	 */
	private Boolean activoDoc;
	/**
	 * Identificador de propagación activa.
	 */
	private Boolean propagateAct;

	/**
	 * Listado de metadatos de la especificación de documentos.
	 */
	private List<MetasDocSpecVO> metasDocSpec;
	/**
	 * Listado de las definiciones heredadas de la especificación de documentos.
	 */
	private List<DocSpecDefInherVO> defInherDocSpec;

	/**
	 * Constructor method for the class DocSpecVO.java.
	 * 
	 * @param aidDocSpec
	 *            Identificador de la especificación de documentos.
	 * @param anameDocSpec
	 *            Nombre de la especificación de documentos.
	 * @param adescDocSpec
	 *            Descripción de la especificación de documentos.
	 * @param aactivoDoc
	 *            Indica si está activo el documento.
	 * @param ametasDocSpec
	 *            Listado de metadatos de la especificación de documento.
	 * @param adefHerDocSpec
	 *            Listado de herencia de la especificación de documento.
	 * @param apropagateAct
	 *            Indica si se propaga la activación del documento.
	 */
	public DocSpecVO(Long aidDocSpec, String anameDocSpec, String adescDocSpec, Boolean aactivoDoc, List<MetasDocSpecVO> ametasDocSpec, List<DocSpecDefInherVO> adefHerDocSpec, Boolean apropagateAct) {
		super();
		this.idDocSpec = aidDocSpec;
		this.nameDocSpec = anameDocSpec;
		this.descDocSpec = adescDocSpec;
		this.activoDoc = aactivoDoc;
		this.metasDocSpec = ametasDocSpec;
		this.defInherDocSpec = adefHerDocSpec;
		this.propagateAct = apropagateAct;
	}

	/**
	 * Obtiene el identificador de especificación de documentos.
	 * 
	 * @return identificador
	 */
	public Long getIdDocSpec() {
		return idDocSpec;
	}

	/**
	 * Modifica el identificador de especificación de documentos.
	 * 
	 * @param aidDocSpec
	 *            Identificador de la especificación del documento.
	 */
	public void setIdDocSpec(Long aidDocSpec) {
		this.idDocSpec = aidDocSpec;
	}

	/**
	 * Obtiene el nombre de especificación de documento.
	 * 
	 * @return nombre Nombre de la especificación de documento.
	 */

	public String getNameDocSpec() {
		return nameDocSpec;
	}

	/**
	 * Modific el nombre de la especificación de documentos.
	 * 
	 * @param anameDocSpec
	 *            Nombre de la especificación de documento.
	 */
	public void setNameDocSpec(String anameDocSpec) {
		this.nameDocSpec = anameDocSpec;
	}

	/**
	 * Obtiene la descripción de la especificación de documentos.
	 * 
	 * @return la descripción de la especificació de documentos
	 */
	public String getDescDocSpec() {
		return descDocSpec;
	}

	/**
	 * Modifica la descripción de la especificación de documentos.
	 * 
	 * @param adescDocSpec
	 *            Descripción de la especificación de documentos.
	 */
	public void setDescDocSpec(String adescDocSpec) {
		this.descDocSpec = adescDocSpec;
	}

	/**
	 * Obtiene si el documento es o no activo.
	 * 
	 * @return si es activo o no el documento.
	 */
	public Boolean getActivoDoc() {
		return activoDoc;
	}

	/**
	 * Modifica si el documento es o no activo.
	 * 
	 * @param aactivoDoc
	 *            Si es activo el Documento.
	 */
	public void setActivoDoc(Boolean aactivoDoc) {
		this.activoDoc = aactivoDoc;
	}

	/**
	 * Obtiene el listado de metadatos de la especificación de documento.
	 * 
	 * @return listado de metadatos de la especificación de documento.
	 */
	public List<MetasDocSpecVO> getMetasDocSpec() {
		return metasDocSpec;
	}

	/**
	 * Modifica el listado de metadatos de la especificación de documentos.
	 * 
	 * @param ametasDocSpec
	 *            listado de metadatos de la especificación de documento.
	 */
	public void setMetasDocSpec(List<MetasDocSpecVO> ametasDocSpec) {
		this.metasDocSpec = ametasDocSpec;
	}

	/**
	 * Obtiene el listado de definiciónes heredadas de la especificación de
	 * documento.
	 * 
	 * @return Listado de definiciones heredadas de la especificación de
	 *         documento.
	 */
	public List<DocSpecDefInherVO> getDefInherDocSpec() {
		return defInherDocSpec;
	}

	/**
	 * Modifica el listado de las definiciones heredadas de la especificación de
	 * documentos.
	 * 
	 * @param adefInherDocSpec
	 *            listado de las definiciones heredadas de la especificación de
	 *            documento.
	 */
	public void setDefHerDocSpec(List<DocSpecDefInherVO> adefInherDocSpec) {
		this.defInherDocSpec = adefInherDocSpec;
	}

	/**
	 * Obtiene si se propaga o no la activación.
	 * 
	 * @return si se propaga o no activación.
	 */
	public Boolean getPropagateAct() {
		return propagateAct;
	}

	/**
	 * Modifica si se propaga o no la activación.
	 * 
	 * @param apropagateAct
	 *            Boolean indicando si se activa o no la propagación.
	 */
	public void setPropagateAct(Boolean apropagateAct) {
		this.propagateAct = apropagateAct;
	}

}
