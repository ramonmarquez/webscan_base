/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.profile.controllers.vo;

import java.util.List;

/**
 * Entidad que usamos para la visualización de la información asociada al perfil
 * de escaneo.
 * <p>
 * Class ScanProfileVO.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ScanProfileVO {

	/**
	 * identificador del perfil de escaneo.
	 */
	private Long idPerfil;
	/**
	 * denominación del perfil de escaneo.
	 */
	private String denominacion;
	/**
	 * descripción del perfil de escaneo.
	 */
	private String descripcion;
	/**
	 * activo del perfil de escaneo.
	 */
	private Boolean activo;
	/**
	 * propagación de la activación del perfil de escaneo.
	 */
	private Boolean propagacion;
	/**
	 * listado de unidades organizativas asociadas al perfil de escaneo.
	 */
	private List<ScanProfileOrgUnitVO> unidOrg;

	/**
	 * Gets the value of the attribute {@link #idPerfil}.
	 * 
	 * @return the value of the attribute {@link #idPerfil}.
	 */
	public Long getIdPerfil() {
		return idPerfil;
	}

	/**
	 * Sets the value of the attribute {@link #idPerfil}.
	 * 
	 * @param aidPerfil
	 *            The value for the attribute {@link #idPerfil}.
	 */
	public void setIdPerfil(Long aidPerfil) {
		this.idPerfil = aidPerfil;
	}

	/**
	 * Gets the value of the attribute {@link #denominacion}.
	 * 
	 * @return the value of the attribute {@link #denominacion}.
	 */
	public String getDenominacion() {
		return denominacion;
	}

	/**
	 * Sets the value of the attribute {@link #denominacion}.
	 * 
	 * @param adenominacion
	 *            The value for the attribute {@link #denominacion}.
	 */
	public void setDenominacion(String adenominacion) {
		this.denominacion = adenominacion;
	}

	/**
	 * Gets the value of the attribute {@link #descripcion}.
	 * 
	 * @return the value of the attribute {@link #descripcion}.
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the value of the attribute {@link #descripcion}.
	 * 
	 * @param adescripcion
	 *            The value for the attribute {@link #descripcion}.
	 */
	public void setDescripcion(String adescripcion) {
		this.descripcion = adescripcion;
	}

	/**
	 * Gets the value of the attribute {@link #activo}.
	 * 
	 * @return the value of the attribute {@link #activo}.
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * Sets the value of the attribute {@link #activo}.
	 * 
	 * @param aactivo
	 *            The value for the attribute {@link #activo}.
	 */
	public void setActivo(Boolean aactivo) {
		this.activo = aactivo;
	}

	/**
	 * Gets the value of the attribute {@link #propagacion}.
	 * 
	 * @return the value of the attribute {@link #propagacion}.
	 */
	public Boolean getPropagacion() {
		return propagacion;
	}

	/**
	 * Sets the value of the attribute {@link #propagacion}.
	 * 
	 * @param apropagacion
	 *            The value for the attribute {@link #propagacion}.
	 */
	public void setPropagacion(Boolean apropagacion) {
		this.propagacion = apropagacion;
	}

	/**
	 * Gets the value of the attribute {@link #unidOrg}.
	 * 
	 * @return the value of the attribute {@link #unidOrg}.
	 */
	public List<ScanProfileOrgUnitVO> getUnidOrg() {
		return unidOrg;
	}

	/**
	 * Sets the value of the attribute {@link #unidOrg}.
	 * 
	 * @param aunidOrg
	 *            The value for the attribute {@link #unidOrg}.
	 */
	public void setUnidOrg(List<ScanProfileOrgUnitVO> aunidOrg) {
		this.unidOrg = aunidOrg;
	}

}
