/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.orgunit.vo;

import java.io.Serializable;

/**
 * Entidad que usamos para recuperar la información de la vista del formulario
 * de unidades organizativas
 * <p>
 * Class FormOrganizationVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

public class FormOrganizationVO implements Serializable {

	/**
	 * Attribute that represents id serial de la class.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Atributo que representa el id del campo identificador.
	 */

	private String idIdentificador;

	/**
	 * Atributo que representa el identificador de la unidad organizativa.
	 */
	private String identificador;
	/**
	 * Atributo que representa el nombre de la unidad organizativa.
	 */
	private String nombre;
	/**
	 * Atributo que representa el padre seleccionado de la unidad organizativa.
	 */
	private Long parentSelect;
	/**
	 * Atributo que respresenta la accion a realizar.
	 */
	private String action;

	/**
	 * Atributo que respresenta la relación de unidades organizativas asociadas
	 * al usuario de la aplicación.
	 */
	private String orgUnitAppUsers;
	/**
	 * Atributo que representa si ha habido algún cambio en la entidad.
	 */
	private Boolean cambioRealizado;

	/**
	 * Metodo que obtiene el valor del campo identificador.
	 * 
	 * @return Cadena con la información del campo identificador.
	 */
	public String getIdentificador() {
		return identificador;
	}

	/**
	 * Metodo que modifica el campo identificador.
	 * 
	 * @param aidentificador
	 *            Identificador de la unidad organizativa.
	 */
	public void setIdentificador(final String aidentificador) {
		this.identificador = aidentificador;
	}

	/**
	 * Metodo que obtiene el valor del campo nombre.
	 * 
	 * @return Cadena con el valor del campo nombre.
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Metodo que modifica el campo nombre.
	 * 
	 * @param anombre
	 *            Nombre de la unidad organizativa.
	 */
	public void setNombre(final String anombre) {
		this.nombre = anombre;
	}

	/**
	 * Metodo que obtiene el valor del campo parentSelect.
	 * 
	 * @return Cadena con el valor del campo parentSelect.
	 */
	public Long getParentSelect() {
		return parentSelect;
	}

	/**
	 * Metodo que modifica el campo parentSelect.
	 * 
	 * @param aparentSelect
	 *            Identificador del padre seleccionado.
	 */
	public void setParentSelect(final Long aparentSelect) {
		this.parentSelect = aparentSelect;
	}

	/**
	 * Metodo que obtiene el valor del campo action.
	 * 
	 * @return Cadena con el valor del campo action.
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Metodo que modifica el campo action.
	 * 
	 * @param aaction
	 *            Cadena con la acción.
	 */
	public void setAction(final String aaction) {
		this.action = aaction;
	}

	/**
	 * Metodo que obtiene el valor del campo cambioRealizado.
	 * 
	 * @return Cadena con el valor del campo cambioRealizado.
	 */
	public Boolean getCambioRealizado() {
		return cambioRealizado;
	}

	/**
	 * Metodo que modifica el campo cambioRealizado.
	 * 
	 * @param acambioRealizado
	 *            Indicador de si ha existido algún cambio.
	 */
	public void setCambioRealizado(final Boolean acambioRealizado) {
		this.cambioRealizado = acambioRealizado;
	}

	/**
	 * Metodo que obtiene el valor del campo idIdentificador.
	 * 
	 * @return Cadena con el valor del campo idIdentificador.
	 */
	public String getIdIdentificador() {
		return idIdentificador;
	}

	/**
	 * Metodo que modifica el campo idIdentificador.
	 * 
	 * @param aidIdentificador
	 *            Id identificador de la unidad organizativa.
	 */
	public void setIdIdentificador(final String aidIdentificador) {
		this.idIdentificador = aidIdentificador;
	}

	/**
	 * Metodo que obtiene el valor del campo orgUnitAppUsers.
	 * 
	 * @return Cadena con el valor del campo orgUnitAppUsers.
	 */
	public String getOrgUnitAppUsers() {
		return orgUnitAppUsers;
	}

	/**
	 * Metodo que modifica el campo orgUnitAppUsers.
	 * 
	 * @param aorgUnitAppUsers
	 *            Cadena con la información de las unidades organizativas
	 *            asociadas al usuario.
	 */
	public void setOrgUnitAppUsers(final String aorgUnitAppUsers) {
		this.orgUnitAppUsers = aorgUnitAppUsers;
	}

}
