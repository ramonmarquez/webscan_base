/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.admon.document.controllers.vo.Value.java.</p>
* <b>Descripción:</b><p> Representación Java de un valor de la lista de valores asignables a un
* metadato, definida en notación JSON.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.admon.document.controllers.vo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Representación Java de un valor de la lista de valores asignables a un
 * metadato, definida en notación JSON.
 * <p>
 * Clase Value.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "key", "value" })
public class Value implements Serializable {

	/**
	 * Cadena clave de un json.
	 */
	@JsonProperty("key")
	private String key;
	/**
	 * Cadena valor de un json.
	 */
	@JsonProperty("value")
	private String value;
	/**
	 * Identificador serial de la clase.
	 */
	private static final long serialVersionUID = -1101550354569518585L;

	/**
	 * Obtiene el valor de la clave de la entidad.
	 * 
	 * @return Cadena con la clave.
	 */
	@JsonProperty("key")
	public String getKey() {
		return key;
	}

	/**
	 * Modifica el valor de la clave de la entidad.
	 * 
	 * @param akey
	 *            Cadena con la clave.
	 */
	@JsonProperty("key")
	public void setKey(String akey) {
		this.key = akey;
	}

	/**
	 * Obtiene el valor de la entidad.
	 * 
	 * @return Cadena con el valor.
	 */
	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	/**
	 * Modifica el valor de la entidad.
	 * 
	 * @param avalue
	 *            Cadena con el valor.
	 */
	@JsonProperty("value")
	public void setValue(String avalue) {
		this.value = avalue;
	}

}
