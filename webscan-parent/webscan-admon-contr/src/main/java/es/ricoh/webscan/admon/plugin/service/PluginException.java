/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.users.model.PluginDAOException.java.</p>
 * <b>Descripción:</b><p> Excepción que encapsula los errores producidos en el
 * componente DAO el modelo de datos de gestión de usuarios de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.admon.plugin.service;

import es.ricoh.webscan.model.DAOException;

/**
 * Excepción que encapsula los errores producidos en el componente DAO el modelo
 * de datos de gestión de usuarios de Webscan.
 * <p>
 * Clase PluginDAOException.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class PluginException extends DAOException {

	/**
	 * Número de serie de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificacio de parametro no existente.
	 */
	public static final String CODE_800 = "COD_800";

	/**
	 * Información recibida erronea.
	 */
	public static final String CODE_900 = "COD_900";

	/**
	 * El plugin o la especificación poseen documentos en proceso de
	 * digitalización.
	 */
	public static final String CODE_901 = "COD_901";

	/**
	 * Constructor method for the class PluginException.java.
	 */
	public PluginException() {
		super();
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public PluginException(String aCode, String aMessage) {
		super(aCode, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 */
	public PluginException(String aMessage) {
		super(CODE_999, aMessage);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCause
	 *            Causa del error.
	 */
	public PluginException(Throwable aCause) {
		super(CODE_999, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public PluginException(final String aMessage, Throwable aCause) {
		super(CODE_999, aMessage, aCause);
	}

	/**
	 * Constructor con argumentos.
	 *
	 * @param aCode
	 *            Código de error.
	 * @param aMessage
	 *            Mensaje del error.
	 * @param aCause
	 *            Causa del error.
	 */
	public PluginException(final String aCode, final String aMessage, Throwable aCause) {
		super(aCode, aMessage, aCause);
	}

}
