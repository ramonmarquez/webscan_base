/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.profile.controllers.vo;

/**
 * Entidad que usamos para la visualización de la información asociada a los
 * documentos del perfil de escaneo.
 * <p>
 * Class ScanProfileDocumentsVO.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

public class ScanProfileDocumentsVO {

	/**
	 * Atributo identificador del documento.
	 */
	private Long idDoc;

	/**
	 * Gets the value of the attribute {@link #idDoc}.
	 * 
	 * @return the value of the attribute {@link #idDoc}.
	 */
	public Long getIdDoc() {
		return idDoc;
	}

	/**
	 * Sets the value of the attribute {@link #idDoc}.
	 * 
	 * @param aidDoc
	 *            The value for the attribute {@link #idDoc}.
	 */
	public void setIdDoc(Long aidDoc) {
		this.idDoc = aidDoc;
	}
}
