/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.profile.controllers.vo;

/**
 * Entidad que usamos para visualizar la información asociada a la relación
 * entre perfil de escaneo,unidades organizativas y plugin.
 * <p>
 * Class ScanProfileOrgUnitPluginVO.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ScanProfileOrgUnitPluginVO {

	/**
	 * identificador de la entidad que relaciona perfil de escaneo con unidad
	 * organizativa y plugin.
	 */
	private Long id;
	/**
	 * identificador de la configuración.
	 */
	private Long idConf;

	/**
	 * Gets the value of the attribute {@link #id}.
	 * 
	 * @return the value of the attribute {@link #id}.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the value of the attribute {@link #id}.
	 * 
	 * @param aid
	 *            The value for the attribute {@link #id}.
	 */
	public void setId(Long aid) {
		this.id = aid;
	}

	/**
	 * Gets the value of the attribute {@link #idConf}.
	 * 
	 * @return the value of the attribute {@link #idConf}.
	 */
	public Long getIdConf() {
		return idConf;
	}

	/**
	 * Sets the value of the attribute {@link #idConf}.
	 * 
	 * @param aidConf
	 *            The value for the attribute {@link #idConf}.
	 */
	public void setIdConf(Long aidConf) {
		this.idConf = aidConf;
	}
}
