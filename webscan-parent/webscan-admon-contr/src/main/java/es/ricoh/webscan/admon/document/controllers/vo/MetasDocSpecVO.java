/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.document.controllers.vo;

import java.io.Serializable;

/**
 * Entidad que usamos para la visualización de los metadatos de la
 * especificación de documentos.
 * <p>
 * Class MetasDocSpecVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */

public class MetasDocSpecVO implements Serializable {

	/**
	 * Attribute that represents el serial de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador del metadato de la especificación de documento.
	 */
	private Long id;
	/**
	 * Nombre del metadato de la especificación de documento.
	 */
	private String nombre;
	/**
	 * Descripción del metadato de la especificación de documento.
	 */
	private String descripcion;
	/**
	 * Boolean indicando si es editable o no el metadato de la especificación de
	 * documento.
	 */
	private Boolean editable;
	/**
	 * Boolean indicando si es requerido o no el metadato de la especificación
	 * de documento.
	 */
	private Boolean requerido;
	/**
	 * Boolean indicando si el metadato es usable para la caratula de la
	 * especificación de documento.
	 */
	private Boolean caratula;
	/**
	 * Cadena con el valor por defecto del metadato de la especificación de
	 * documento.
	 */
	private String valorDefecto;
	/**
	 * Cadena con la lista de valores posibles del metadato.
	 */
	private String listaValores;
	/**
	 * Boolean indicando si el metadato es un identificador de lote.
	 */
	private Boolean isBatchId;

	/**
	 * Constructor method for the class MetasDocSpecVO.java.
	 * 
	 * @param aid
	 *            Identificador del metadato.
	 * @param anombre
	 *            Nombre del metadato.
	 * @param adescripcion
	 *            Descripción del metadato.
	 * @param aeditable
	 *            Indica si es editable o no el metadato.
	 * @param arequerido
	 *            Indica si es requerido o no el metadato.
	 * @param acaratula
	 *            Indica si forma parte de la caratula.
	 * @param avalorDefecto
	 *            Valor por defecto del metadato.
	 * @param alistaValores
	 *            Lista de valores posible del metadato.
	 * @param aisBatchId
	 *            Indica si es un identificador de lote.
	 */

	public MetasDocSpecVO(Long aid, String anombre, String adescripcion, Boolean aeditable, Boolean arequerido, Boolean acaratula, String avalorDefecto, String alistaValores, Boolean aisBatchId) {
		super();
		this.id = aid;
		this.nombre = anombre;
		this.descripcion = adescripcion;
		this.editable = aeditable;
		this.requerido = arequerido;
		this.valorDefecto = avalorDefecto;
		this.listaValores = alistaValores;
		this.caratula = acaratula;
		this.isBatchId = aisBatchId;
	}

	/**
	 * Obtiene el identificador.
	 * 
	 * @return Long con el identificador.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Modifica el identificador del metadato.
	 * 
	 * @param aid
	 *            Identificador del metadato.
	 */
	public void setId(Long aid) {
		this.id = aid;
	}

	/**
	 * Obtiene el nombre del metadato.
	 * 
	 * @return Nombre del metadato.
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Modifica el nombre del metadato.
	 * 
	 * @param anombre
	 *            Cadena del metadato.
	 */
	public void setNombre(String anombre) {
		this.nombre = anombre;
	}

	/**
	 * Obtiene la descripción del metadato.
	 * 
	 * @return Cadena con la descripción del metadato.
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Modifica la descripción del metadato.
	 * 
	 * @param adescripcion
	 *            Cadena con la descripción del metadato.
	 */
	public void setDescripcion(String adescripcion) {
		this.descripcion = adescripcion;
	}

	/**
	 * Obtiene si es editable o no el metadato.
	 * 
	 * @return Boolean indicando si es editable el metadato.
	 */
	public Boolean getEditable() {
		return editable;
	}

	/**
	 * Modifica el indicador de editable.
	 * 
	 * @param aeditable
	 *            Boolean indicando si es editable.
	 */
	public void setEditable(Boolean aeditable) {
		this.editable = aeditable;
	}

	/**
	 * Obtiene si es requerido o no el metadato.
	 * 
	 * @return Boolean indicando si es requerido el metadato.
	 */
	public Boolean getRequerido() {
		return requerido;
	}

	/**
	 * Modifica si es requerido o no el metadato.
	 * 
	 * @param arequerido
	 *            Boolean indicando si es requerido.
	 */
	public void setRequerido(Boolean arequerido) {
		this.requerido = arequerido;
	}

	/**
	 * Obtiene el valor por defecto del metadato.
	 * 
	 * @return Cadena con el valor por defecto.
	 */
	public String getValorDefecto() {
		return valorDefecto;
	}

	/**
	 * Modifica el valor por defecto del metadato.
	 * 
	 * @param avalorDefecto
	 *            Cadena con el valor por defecto.
	 */
	public void setValorDefecto(String avalorDefecto) {
		this.valorDefecto = avalorDefecto;
	}

	/**
	 * Obtiene el listado de valores del metadato.
	 * 
	 * @return Cadena con el listado de valores.
	 */
	public String getListaValores() {
		return listaValores;
	}

	/**
	 * Modifica el listado de valores del metadato.
	 * 
	 * @param alistaValores
	 *            Cadena con el listado de valores.
	 */
	public void setListaValores(String alistaValores) {
		this.listaValores = alistaValores;
	}

	/**
	 * Obtiene si forma parte de la caratula.
	 * 
	 * @return Boolean indicando si forma parte de la caratula.
	 */
	public Boolean getCaratula() {
		return caratula;
	}

	/**
	 * Modifica si forma parte de la caratula.
	 * 
	 * @param acaratula
	 *            Boolean indicando si forma parte de la caratula.
	 */
	public void setCaratula(Boolean acaratula) {
		this.caratula = acaratula;
	}

	/**
	 * Obtiene indicador de si es identificador de lote.
	 * 
	 * @return Boolean indicando si es identificador de lote.
	 */
	public Boolean getIsBatchId() {
		return isBatchId;
	}

	/**
	 * Modifica el indicador de si es identificador de lote.
	 * 
	 * @param aisBatchId
	 *            Boolean indicando si es identificador de lote.
	 */
	public void setIsBatchId(Boolean aisBatchId) {
		this.isBatchId = aisBatchId;
	}

}
