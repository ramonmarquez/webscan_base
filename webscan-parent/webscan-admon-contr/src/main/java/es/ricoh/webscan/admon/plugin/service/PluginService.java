/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.plugin.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.admon.AdmonWebscanConstants;
import es.ricoh.webscan.admon.plugin.controllers.vo.ParameterSpecificationVO;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.dto.ParameterDTO;
import es.ricoh.webscan.model.dto.ParameterSpecificationDTO;
import es.ricoh.webscan.model.dto.ParameterTypeDTO;
import es.ricoh.webscan.model.dto.PluginDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;
import es.ricoh.webscan.model.service.AuditService;

/**
 * Implementación de la lógica de negocio de los plugins.
 * <p>
 * Class PluginService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class PluginService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PluginService.class);

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre el modelo de datos.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Servicio interno para el registro y consulta de trazas y eventos de
	 * auditoría del módulo de gestión de usuarios de Webscan.
	 */
	private AuditService auditService;

	/**
	 * Obtiene un listado de plugins.
	 * 
	 * @return Listado de plugins
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public List<PluginSpecificationDTO> getListPluginSpec() throws DAOException {
		List<PluginSpecificationDTO> plugins = null;
		LOG.debug("Start getListPlugins");

		plugins = webscanModelManager.listPluginSpecifications(null, null, null);

		LOG.debug("End getListPlugins");
		return plugins;
	}

	/**
	 * Obtiene la relación de especificación de plugins registrada en el
	 * sistema.
	 * 
	 * @return map de especificación de plugins y fase ordenada por plugins
	 *         especificación
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public Map<PluginSpecificationDTO, List<ScanProcessStageDTO>> listPluginSpecsAndScanProcessStages() throws DAOException {
		LOG.debug("Start listPluginSpecsAndScanProcessStages");
		Map<PluginSpecificationDTO, List<ScanProcessStageDTO>> res;

		res = webscanModelManager.listPluginSpecsAndScanProcessStages();

		LOG.debug("End listPluginSpecsAndScanProcessStages");
		return res;
	}

	/**
	 * Obtiene la relación de plugins registrada en el sistema para una
	 * especificación de plugin determinada.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de especificación de plugin.
	 * @return Lista de configuraciones de una especificación de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	public List<PluginDTO> getListPluginBySpec(Long pluginSpecificationId) throws DAOException {
		List<PluginDTO> plugins = null;
		LOG.debug("Start getListPluginBySpec");

		plugins = webscanModelManager.getPluginsBySpecification(pluginSpecificationId);

		LOG.debug("plugins size: {}", (plugins != null ? plugins.size() : 0));

		LOG.debug("End getListPluginBySpec");
		return plugins;
	}

	/**
	 * Obtiene la relación de plugins registrada en el sistema para una
	 * especificación de plugin determinada.
	 * 
	 * @param pluginSpecId
	 *            Identificador de especificación de plugin.
	 * @return Lista de configuraciones de una especificación de plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */

	public List<ParameterSpecificationDTO> getListParamSpecByPluginId(Long pluginSpecId) throws DAOException {
		List<ParameterSpecificationDTO> params;
		LOG.debug("Start getListParamSpecByPluginId");

		params = webscanModelManager.getPluginSpecParameterSpecList(pluginSpecId);

		LOG.debug("plugins size: {}", (params != null ? params.size() : 0));
		LOG.debug("Start getListParamSpecByPluginId");
		return params;
	}

	/**
	 * Obtiene una especificación de plugin a partir de su denominación.
	 * 
	 * @param pluginSpecificationName
	 *            Denominación de especificación de plugin.
	 * @return La definición de plugin solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */

	public PluginSpecificationDTO getPluginSpecificationByName(String pluginSpecificationName) throws DAOException {
		LOG.debug("Start getListParamSpecByPluginId");
		PluginSpecificationDTO pluginSpec = null;

		pluginSpec = webscanModelManager.getPluginSpecificationByName(pluginSpecificationName);

		LOG.debug("Start getListParamSpecByPluginId");
		return pluginSpec;
	}

	/**
	 * Recupera la relación de tipos de parámetros registrada en el sistema.
	 * 
	 * @return Listado de tipos de parámetros registrada en el sistema.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */

	public List<ParameterTypeDTO> listParameterTypes() throws DAOException {
		List<ParameterTypeDTO> types = null;
		LOG.debug("Start listParameterTypes");

		types = webscanModelManager.listParameterTypes();

		LOG.debug("Start listParameterTypes");
		return types;
	}

	/**
	 * Recupera la especificacion del tipo de parametro por su id.
	 * 
	 * @param idParamDataType
	 *            Identificador del tipo de dato del parámetro.
	 * @return null o la entidad ParameterTypeDTO
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */

	public ParameterTypeDTO getParameterType(Long idParamDataType) throws DAOException {
		List<ParameterTypeDTO> tipos = null;
		ParameterTypeDTO tipo = null;
		LOG.debug("Start getParameterType");

		tipos = webscanModelManager.listParameterTypes();

		for (ParameterTypeDTO aux: tipos) {
			if (aux.getId().equals(idParamDataType)) {
				tipo = aux;
				break;
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Valor de tipo:" + tipo != null ? tipo.getName() : "no encontrado ..., es null");
		}
		LOG.debug("Start getParameterType");
		return tipo;
	}

	/**
	 * Recupera la relación de los parametros asociados a una configuración de
	 * plugin.
	 * 
	 * @param pluginSpecificationId
	 *            identificador de la especificación de plugin.
	 * @param pluginConf
	 *            identificador de la configuración de plugin.
	 * @return Listado de los parametros asociados a una configuración de plugin
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */

	public List<ParameterDTO> listParameterByPluginAndConf(Long pluginSpecificationId, String pluginConf) throws DAOException {
		List<ParameterDTO> parametros = new ArrayList<ParameterDTO>();
		LOG.debug("Start listParameterByPluginAndConf");

		Map<PluginDTO, List<ParameterDTO>> pluginAndParam = webscanModelManager.getPluginsAndParamsBySpecification(pluginSpecificationId);

		Iterator<Entry<PluginDTO, List<ParameterDTO>>> it = pluginAndParam.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<PluginDTO, List<ParameterDTO>> pair = (Map.Entry<PluginDTO, List<ParameterDTO>>) it.next();
			if (LOG.isDebugEnabled()) {
				LOG.debug("Pareja obtenida:" + pair.getKey() + " = " + pair.getValue());
			}

			if (pair.getKey().getName().equals(pluginConf)) {
				parametros = (List<ParameterDTO>) pair.getValue();
				break;
			}
			it.remove();
		}

		LOG.debug("Start listParameterByPluginAndConf");
		return parametros;
	}

	/**
	 * Recupera la relación de los parametros asociados a una especificacion de
	 * plugin.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de la especificación de plugin.
	 * @return Listado de los parametros asociados a una especificacion de
	 *         plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */

	public List<ParameterSpecificationVO> listParameterByPluginSpec(Long pluginSpecificationId) throws DAOException {
		List<ParameterSpecificationVO> paramSpecVO = new ArrayList<ParameterSpecificationVO>();
		LOG.debug("Start listParameterByPlugin");

		// obtenemos todas las especificaciones por el id de la especificacion
		// de plugin
		List<ParameterSpecificationDTO> paramSpecDTO = webscanModelManager.getPluginSpecParameterSpecList(pluginSpecificationId);

		// recuperamos para nuestra entidad de visualización la información
		// asociada al nombre del tipo,
		// ya que el listado obtenido anteriormente nos viene por id y para
		// visualización necesitamos el nombre
		for (ParameterSpecificationDTO aux: paramSpecDTO) {

			ParameterSpecificationVO actual = new ParameterSpecificationVO(aux.getId(), aux.getName(), aux.getDescription(), aux.getMandatory(), getParameterType(aux.getTypeId()).getName(), getParameterType(aux.getTypeId()).getStringPattern(), aux.getPluginSpecificationId());
			paramSpecVO.add(actual);
		}

		LOG.debug("paramSpecVO size:", paramSpecVO.size());

		LOG.debug("Start listParameterByPlugin");
		return paramSpecVO;
	}

	/**
	 * Actualizamos la información del plugin y los parametros asociados.
	 * 
	 * @param request
	 *            Petición http.
	 * @param plugin
	 *            Objecto con la información del plugin que vamos actualizar o
	 *            crear
	 * @param listParam
	 *            Listado de los parametros asociados al plugin anterior
	 * @param propaga
	 *            Boolean que indica si se propaga la activacion del plugin
	 *            actualizado o creado
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */

	public void updatePlugin(HttpServletRequest request, PluginDTO plugin, List<ParameterDTO> listParam, Boolean propaga) throws DAOException {
		LOG.debug("Start updatePlugin");
		final HttpSession session;
		Exception exception = null;
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";
		session = request.getSession(false);

		try {
			// obtenemos la info para auditoria
			UserInSession user = (UserInSession) session.getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
			username = user.getUsername();

			webscanModelManager.updatePlugin(plugin, listParam, propaga);
			LOG.debug("salvamos la información en auditoria");
			auditService.createAuditLogs(username, result, authTimestamp, exception, AuditOperationName.PLUGIN_UPDATE, AuditEventName.PLUGIN_UPDATE);

		} catch (DAOException e) {
			LOG.debug("Error al realizar la actualizacion de plugin");
			result = AuditOpEventResult.ERROR;
			exception = e;
			throw e;
		}
		LOG.debug("End updatePlugin");
	}

	/**
	 * Recupera la relación de parámetros registrada en el sistema para un
	 * plugin determinado.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @return Lista de parámetros de un plugin.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin, o se produce algún error en la consulta a BBDD.
	 */

	public List<ParameterDTO> getParametersByPlugin(Long pluginId) throws DAOException {
		LOG.debug("Start updatePluginSpecification");
		List<ParameterDTO> res;

		res = webscanModelManager.getParametersByPlugin(pluginId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Parametro/s recuperado/s del plugin " + pluginId + " son:" + res.size());
		}
		LOG.debug("Start updatePluginSpecification");
		return res;
	}

	/**
	 * Actualizamos la información de la especificacion de plugin y la
	 * especificacion de parametros asociados.
	 * 
	 * @param specification
	 *            Objecto con la información de la especificacion del plugin que
	 *            vamos actualizar o crear
	 * @param paramSpecs
	 *            Listado de la especificacion de parametros asociados al plugin
	 *            anterior
	 * @param request
	 *            Petición http.
	 * @param propagateActivation
	 *            Boolean que indica si se propaga la activacion de la
	 *            especificacion del plugin actualizado o creado
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */

	public void updatePluginSpecification(HttpServletRequest request, PluginSpecificationDTO specification, List<ParameterSpecificationDTO> paramSpecs, Boolean propagateActivation) throws DAOException {
		LOG.debug("Start updatePluginSpecification");
		final HttpSession session;
		Exception exception = null;
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";
		session = request.getSession(false);

		try {
			// obtenemos la info para auditoria
			UserInSession user = (UserInSession) session.getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
			username = user.getUsername();

			webscanModelManager.updatePluginSpecification(specification, paramSpecs, propagateActivation);
			LOG.debug("salvamos la información en auditoria");
			auditService.createAuditLogs(username, result, authTimestamp, exception, AuditOperationName.PLUGIN_SPEC_UPDATE, AuditEventName.PLUGIN_SPEC_UPDATE);

		} catch (DAOException e) {
			result = AuditOpEventResult.ERROR;
			exception = e;
			throw e;
		} finally {}
		LOG.debug("End updatePluginSpecification");
	}

	/**
	 * Obtenemos la inforamción asociada al plugin a partir de su id.
	 * 
	 * @param pluginId
	 *            Identificador del plugin.
	 * @return La definición de plugin solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */

	public PluginDTO getPlugin(Long pluginId) throws DAOException {
		PluginDTO res;
		LOG.debug("Start updatePlugin");

		res = webscanModelManager.getPlugin(pluginId);

		LOG.debug("End updatePlugin");
		return res;
	}

	/**
	 * Obtiene una lista de especificación de parametros a partir del id de
	 * especificacion del plugin.
	 * 
	 * @param pluginSpecId
	 *            Identificador de especificación de plugin.
	 * @return Listado de especificacion de parametros asociados a la definición
	 *         de plugin solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */

	public List<ParameterSpecificationDTO> getParameterSpecListByPluginSpec(Long pluginSpecId) throws DAOException {
		LOG.debug("Start getParameterSpecListByPluginSpec");
		List<ParameterSpecificationDTO> res;

		res = webscanModelManager.getPluginSpecParameterSpecList(pluginSpecId);

		LOG.debug("Start getParameterSpecListByPluginSpec");
		return res;
	}

	/**
	 * Obtiene una especificación de plugin a partir de su identificador.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de especificación de plugin.
	 * @return La definición de plugin solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */

	public PluginSpecificationDTO getPluginSpecification(Long pluginSpecificationId) throws DAOException {
		LOG.debug("Start getPluginSpecification");
		PluginSpecificationDTO res = null;

		res = webscanModelManager.getPluginSpecification(pluginSpecificationId);

		LOG.debug("End getPluginSpecification");
		return res;
	}

	/**
	 * Obtiene un map con los plugins y parametros asociados según la
	 * especificación del plugin.
	 * 
	 * @param pluginSpecificationId
	 *            Identificador de la especificación del plugin
	 * @return Map con la información asociada a los plugins y parametros
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación solicitada, o se produce algún error en la
	 *             consulta a BBDD.
	 */

	public Map<PluginDTO, List<ParameterDTO>> getPluginsAndParamsBySpecification(Long pluginSpecificationId) throws DAOException {
		Map<PluginDTO, List<ParameterDTO>> res = null;
		LOG.debug("Start getPluginSpecification");

		res = webscanModelManager.getPluginsAndParamsBySpecification(pluginSpecificationId);

		LOG.debug("Start getPluginSpecification");
		return res;
	}

	/**
	 * Crea un nuevo plugin en el sistema, registrando además sus parámetros.
	 * 
	 * @param request
	 *            Petición http.
	 * @param plugin
	 *            Nuevo plugin.
	 * @param parameters
	 *            Relación de parámetros de la configuración o plugin.
	 * @param pluginSpecId
	 *            Identificador de la especificación de plugin.
	 * @return El plugin creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public PluginDTO createPlugin(HttpServletRequest request, PluginDTO plugin, List<ParameterDTO> parameters, Long pluginSpecId) throws DAOException {

		LOG.debug("Start createPlugin");
		PluginDTO res;
		final HttpSession session;
		Exception exception = null;
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";
		session = request.getSession(false);

		// obtenemos la info para auditoria
		UserInSession user = (UserInSession) session.getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
		username = user.getUsername();

		res = webscanModelManager.createPlugin(plugin, parameters, pluginSpecId);

		LOG.debug("salvamos la información en auditoria");
		auditService.createAuditLogs(username, result, authTimestamp, exception, AuditOperationName.PLUGIN_CREATE, AuditEventName.PLUGIN_CREATE);

		LOG.debug("End createPlugin");
		return res;
	}

	/**
	 * Crea un nuevo plugin en el sistema, sin parámetros.
	 * 
	 * @param request
	 *            Petición http.
	 * @param plugin
	 *            Nuevo plugin.
	 * @param pluginSpecId
	 *            Identificador de la especificación del plugin.
	 * @return El plugin creado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public PluginDTO createPlugin(HttpServletRequest request, PluginDTO plugin, Long pluginSpecId) throws DAOException {
		PluginDTO res = null;
		HttpSession session;
		Exception exception = null;
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";
		session = request.getSession(false);

		// obtenemos la info para auditoria
		UserInSession user = (UserInSession) session.getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
		username = user.getUsername();

		res = createPlugin(request, plugin, new ArrayList<ParameterDTO>(), pluginSpecId);

		LOG.debug("salvamos la información en auditoria");
		auditService.createAuditLogs(username, result, authTimestamp, exception, AuditOperationName.PLUGIN_CREATE, AuditEventName.PLUGIN_CREATE);

		return res;
	}

	/**
	 * Elimina una configuración o parámetrización de plugin y sus parámetros.
	 * 
	 * @param request
	 *            petición web.
	 * @param pluginId
	 *            Parametrización de plugin a eliminar.
	 * @throws DAOException
	 *             Si se produce algún error en la ejecución de la sentencia de
	 *             BBDD.
	 */
	public void removePluginInstance(HttpServletRequest request, Long pluginId) throws DAOException {
		LOG.debug("[WEBSCAN-PLUGIN-SERVICE] Start removePlugin");
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";

		webscanModelManager.removePlugin(pluginId);

		LOG.debug("[WEBSCAN-PLUGIN-SERVICE] Se registra la traza de auditoría de la operación...");
		// obtenemos la info para auditoria
		UserInSession user = (UserInSession) request.getSession(false).getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
		username = user.getUsername();

		auditService.createAuditLogs(username, result, authTimestamp, null, AuditOperationName.PLUGIN_REMOVE, AuditEventName.PLUGIN_REMOVE);
		if (LOG.isDebugEnabled()) {
			LOG.debug("[WEBSCAN-PLUGIN-SERVICE] Traza de auditoría " + AuditOperationName.PLUGIN_REMOVE + " registrada.");
		}
		LOG.debug("[WEBSCAN-PLUGIN-SERVICE] End removePlugin");
	}

	/**
	 * Comprueba que no exista ningún documento asociado a la especificación de
	 * documento pasada en vuelo.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @return Devuelve true si no existen documentos en vuelos para el
	 *         identificador de plugin pasado como parámetro. En caso contrario,
	 *         retorna false.
	 * @throws DAOException
	 *             Si se produce algún error ejecutando la consulta en BBDD.
	 */

	public Boolean checkPluginDoesNotHaveDocsInScanProccess(Long pluginId) throws DAOException {
		Boolean res;
		// comprueba que no exista documentos al vuelo.
		res = !webscanModelManager.hasPluginDocsInScanProccess(pluginId);

		return res;
	}

	/**
	 * Comprueba que no exista ningún documento asociado a la especificación de
	 * documento pasada en vuelo.
	 * 
	 * @param pluginSpecId
	 *            Identificador de plugin.
	 * @return Devuelve true si no existen documentos en vuelos para el
	 *         identificador de plugin pasado como parámetro. En caso contrario,
	 *         retorna false.
	 * @throws DAOException
	 *             Si se produce algún error ejecutando la consulta en BBDD.
	 */

	public Boolean checkPluginSpecDoesNotHaveDocsInScanProccess(Long pluginSpecId) throws DAOException {
		Boolean res;
		// comprueba que no exista documentos al vuelo.
		res = !webscanModelManager.hasPluginSpecDocsInScanProccess(pluginSpecId);

		return res;
	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Establece el servicio interno para el registro y consulta de trazas y
	 * eventos de auditoría de Webscan.
	 * 
	 * @param anAuditManager
	 *            nuevo servicio interno para el registro y consulta de trazas y
	 *            eventos de auditoría de Webscan.
	 */
	public void setAuditService(AuditService anAuditManager) {
		this.auditService = anAuditManager;
	}
}
