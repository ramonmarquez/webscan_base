/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/**
 * <b>Archivo:</b><p>es.ricoh.webscan.model.dto.ParameterSpecificationDTO.java.</p>
 * <b>Descripción:</b><p> VO para la entidad JPA definición de parámetro.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH Spain IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.admon.plugin.controllers.vo;

/**
 * VO para la entidad JPA definición de parámetro.
 * <p>
 * Clase ParameterSpecificationVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ParameterSpecificationVO {

	/**
	 * Número de versión de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación de la entidad.
	 */
	private String name;

	/**
	 * Descripción de la entidad.
	 */
	private String description;

	/**
	 * Indicador de obligatoriedad del parámetro.
	 */
	private Boolean mandatory;

	/**
	 * identificador del tipo de datos del parámetro.
	 */
	private String typeName;

	/**
	 * Definición de plugin a la que pertenece la especificación de parámetro.
	 */
	private Long pluginSpecificationId;

	/**
	 * Definición del patron del parametro.
	 */

	private String stringPattern;

	/**
	 * Constructor sin argumentos.
	 */
	public ParameterSpecificationVO() {
		super();
	}

	/**
	 * Constructor method for the class ParameterSpecificationVO.java.
	 * 
	 * @param aid
	 *            Identificador de la entidad
	 * @param aname
	 *            Nombre de la especificación de parámetro
	 * @param adescription
	 *            Descripción de la especificación de parámetro
	 * @param amandatory
	 *            Boolean que indica si la especificación de parámetro es
	 *            obligatorio
	 * @param atypeName
	 *            Tipo de Nombre de la especificación de parámetro
	 * @param astringPattern
	 *            Cadena patron de la especificación de parámetro
	 * @param apluginSpecId
	 *            Identificador asociado a la especificación del plugin que está
	 *            relacionado con esta especificación de parámetro
	 */
	public ParameterSpecificationVO(final Long aid, final String aname, final String adescription, final Boolean amandatory, final String atypeName, final String astringPattern, final Long apluginSpecId) {
		super();
		this.id = aid;
		this.name = aname;
		this.description = adescription;
		this.mandatory = amandatory;
		this.typeName = atypeName;
		this.pluginSpecificationId = apluginSpecId;
		this.stringPattern = astringPattern;
	}

	/**
	 * Metodo que obtiene el campo id.
	 * 
	 * @return Long con el valor del campo id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Metodo que modifica el valor del campo id.
	 * 
	 * @param aid
	 *            Identificador de la especificación del parámetro.
	 */
	public void setId(final Long aid) {
		this.id = aid;
	}

	/**
	 * Metodo que obtiene el valor del campo nombre.
	 * 
	 * @return Cadena con el valor del campo nombre
	 */
	public String getName() {
		return name;
	}

	/**
	 * Metodo que modifica el valor del campo nombre.
	 * 
	 * @param aname
	 *            Nombre de la especificación del plugin.
	 */
	public void setName(final String aname) {
		this.name = aname;
	}

	/**
	 * Metodo que obtiene el valor descripción.
	 * 
	 * @return Cadena con el valor del campo descripción
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Metodo que modifica el valor del campo descripción.
	 * 
	 * @param adescription
	 *            Descripción de la especificación del plugin.
	 */
	public void setDescription(final String adescription) {
		this.description = adescription;
	}

	/**
	 * Metodo que obtiene el valor del campo mandatorio - obligatorio.
	 * 
	 * @return Boolean que indica si el parametro actual es obligatorio.
	 */
	public Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * Metodo que modifica el valor del campo mandatorio - obligatorio.
	 * 
	 * @param amandatory
	 *            boolean indicando si es obligatorio el plugin.
	 */
	public void setMandatory(final Boolean amandatory) {
		this.mandatory = amandatory;
	}

	/**
	 * Metodo para obtener el valor del campo nombre tipo - tipo del parametro.
	 * 
	 * @return Nombre del tipo de plugin.
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * Metodo que modifica el valor del campo typeName.
	 * 
	 * @param atypeName
	 *            Nombre del tipo de plugin.
	 */
	public void setTypeName(final String atypeName) {
		this.typeName = atypeName;
	}

	/**
	 * Metodo que obtiene el valor del campo id de la especificación de plugin
	 * asociada al parámetro.
	 * 
	 * @return Long con el identificador de la especificación de plugin asociada
	 */
	public Long getPluginSpecificationId() {
		return pluginSpecificationId;
	}

	/**
	 * Metodo que modifica el valor del campo pluginSpecificationId.
	 * 
	 * @param apluginSpecId
	 *            Identificador de la especificación del plugin asociado
	 */
	public void setPluginSpecificationId(final Long apluginSpecId) {
		this.pluginSpecificationId = apluginSpecId;
	}

	/**
	 * Metodo que obtiene el valor del campo StringPattern.
	 * 
	 * @return Cadena con el patron del parámetro
	 */
	public String getStringPattern() {
		return stringPattern;
	}

	/**
	 * Metodo que modifica el campo stringPattern - patron del parámetro.
	 * 
	 * @param astringPattern
	 *            String con el patron del parámetro.
	 */
	public void setStringPattern(final String astringPattern) {
		this.stringPattern = astringPattern;
	}

	/**
	 * Gets the value of the attribute {@link #serialversionuid}.
	 * 
	 * @return the value of the attribute {@link #serialversionuid}.
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
