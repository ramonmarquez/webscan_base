/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.document.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.admon.AdmonWebscanConstants;
import es.ricoh.webscan.admon.document.controllers.vo.DocSpecDefInherVO;
import es.ricoh.webscan.admon.document.controllers.vo.DocSpecVO;
import es.ricoh.webscan.admon.document.controllers.vo.MetasDocSpecVO;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.MetadataSpecificationDTO;
import es.ricoh.webscan.model.service.AuditService;

/**
 * Implementación de la lógica de negocio de los documentos.
 * <p>
 * Class DocumentService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class DocumentService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DocumentService.class);

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre el modelo de datos.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Servicio interno para el registro y consulta de trazas y eventos de
	 * auditoría del módulo de gestión de usuarios de Webscan.
	 */
	private AuditService auditService;

	/**
	 * Obtiene un listado de las especificaciones de documentos.
	 * 
	 * @return Listado de especificaciones de documentos.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public List<DocumentSpecificationDTO> getListDocSpecification() throws DAOException {

		LOG.debug("Start getListDocSpecification");

		List<DocumentSpecificationDTO> spe = new ArrayList<DocumentSpecificationDTO>();

		spe = webscanModelManager.listDocSpecifications(null, null, null);

		LOG.debug("End getListDocSpecification");
		return spe;

	}

	/**
	 * Obtiene un listado de los metadatos asociados a una especificación de
	 * documentos.
	 * 
	 * @param docSpecId
	 *            id de la especificación de documentos.
	 * @return Mapa con la información de todos los metadatos asociados al id de
	 *         la especificación solicitada, incluido los metadatos de padres
	 *         heredados.
	 * @throws DAOException
	 *             Si el parámetro de entrada no es válido, no existe el
	 *             idDocSpec solicitado, o se produce algún error en la consulta
	 *             a BBDD.
	 */

	public Map<String, List<MetadataSpecificationDTO>> getMapListMetadataSpec(Long docSpecId) throws DAOException {
		LOG.debug("Start getListMetadataSpec");

		Map<String, List<MetadataSpecificationDTO>> res = webscanModelManager.getMetadaSpecificationsByDocSpec(docSpecId);

		LOG.debug("End getListMetadataSpec");
		return res;
	}

	/**
	 * Actualiza la información asociada a la especificación del documento
	 * incorporada, incluida la información de metadatos propios y heredados.
	 * 
	 * @param request
	 *            Petición http.
	 * @param docSpecVO
	 *            Objeto con toda la información asociada a la especificación de
	 *            Documentos.
	 * @throws DAOException
	 *             Si el parámetro de entrada no es válido, no existe el
	 *             DocSpecVO incorporado, o se produce algún error en las
	 *             consultas, modificaciones a BBDD.
	 * @return DocumentSpecificationDTO Entidad con la información modificada.
	 */
	public DocumentSpecificationDTO updateDocSpec(HttpServletRequest request, DocSpecVO docSpecVO) throws DAOException {
		LOG.debug("Start actualizarDocSpec");
		DocumentSpecificationDTO res = null;
		final HttpSession session;
		Exception exception = null;
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";
		session = request.getSession(false);
		AuditOperationName opName;
		AuditEventName evName;

		try {
			// obtenemos la info para auditoria
			UserInSession user = (UserInSession) session.getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
			username = user.getUsername();

			String validate = validateMetadata(docSpecVO);
			if (validate == "")
				res = actualizarDocSpec(docSpecVO);
			else
				throw new DocumentException(DocumentException.CODE_902, validate);

			// es una actualizacion
			if (docSpecVO.getIdDocSpec() != null) {
				opName = AuditOperationName.DOCUMENT_SPEC_UPDATE;
				evName = AuditEventName.DOCUMENT_SPEC_UPDATE;
			} else // es nuevo
			{
				opName = AuditOperationName.DOCUMENT_SPEC_CREATE;
				evName = AuditEventName.DOCUMENT_SPEC_CREATE;
			}

			LOG.debug("salvamos la información en auditoria");
			auditService.createAuditLogs(username, result, authTimestamp, exception, opName, evName);

		} catch (DAOException e) {
			result = AuditOpEventResult.ERROR;
			exception = e;
			throw e;
		}

		LOG.debug("End actualizarDocSpec");
		return res;
	}

	/**
	 * Comprueba si en los metadatos a guardar no hay nombres repetidos
	 * 
	 * @param docSpecVO
	 * @return boolean de validación
	 */
	private String validateMetadata(DocSpecVO docSpecVO) {
		String result = "";
		List<MetasDocSpecVO> metadatas = docSpecVO.getMetasDocSpec();
		Set<String> metadataNames = new HashSet<String>();

		for (MetasDocSpecVO m: metadatas) {
			if (!metadataNames.add(m.getNombre())) {
				result = m.getNombre();
			}
		}
		return result;
	}

	/**
	 * Método para actualizar la especificación de Documentos.
	 * 
	 * @param docSpecVO
	 *            Parámetro con la información nueva de la especificación de
	 *            documento.
	 * @throws DAOException
	 *             Exception al tratar de actualizar o crear dicha
	 *             especificación en BBDD.
	 * @return DocumentSpecificationDTO Entidad asociada a la especificación de
	 *         documentos.
	 */
	private DocumentSpecificationDTO actualizarDocSpec(DocSpecVO docSpecVO) throws DAOException {
		// preparamos la info a enviar al manager
		Boolean propagateActivation = docSpecVO.getPropagateAct();
		DocumentSpecificationDTO doc = new DocumentSpecificationDTO();
		DocumentSpecificationDTO oldDocSpec;
		Map<String, List<MetadataSpecificationDTO>> mapMetaSpec = null;
		List<MetadataSpecificationDTO> metasOld = null;

		// comprobamos si es nuevo o no, revisando el id del docSpec
		if (docSpecVO.getIdDocSpec() != null) {
			doc = webscanModelManager.getDocSpecification(docSpecVO.getIdDocSpec());
			// obtenemos los metadatos anteriores asociados al id del Doc Spec
			mapMetaSpec = webscanModelManager.getMetadaSpecificationsByDocSpec(docSpecVO.getIdDocSpec());
			// obtenemos la espeficicacion antigua para buscar la información
			// mapeada
			oldDocSpec = webscanModelManager.getDocSpecification(docSpecVO.getIdDocSpec());
			// obtenemos los metas asociados a la especificacion antigua
			metasOld = mapMetaSpec.get(oldDocSpec.getName());
		}

		// actualizamos la info de la especificacion de documentos
		doc.setActive(docSpecVO.getActivoDoc());
		doc.setName(docSpecVO.getNameDocSpec());
		doc.setDescription(docSpecVO.getDescDocSpec());

		// preparamos los metadatos
		List<MetadataSpecificationDTO> metadataSpecs = new ArrayList<MetadataSpecificationDTO>();
		for (MetasDocSpecVO meta: docSpecVO.getMetasDocSpec()) {
			MetadataSpecificationDTO actual = new MetadataSpecificationDTO();

			// comprobamos si tenemos el id del meta y no es nuevo
			if (meta.getId() != null) {
				// buscamos el metadato que debe existir previamente
				for (MetadataSpecificationDTO metaAux: metasOld) {
					if (metaAux.getId().equals(meta.getId())) {
						actual = metaAux;
						break;
					}
				}
			}
			// actualizamos o preparamos la nueva info recogida
			actual.setName(meta.getNombre());
			actual.setDescription(meta.getDescripcion());
			actual.setEditable(meta.getEditable());
			actual.setMandatory(meta.getRequerido());
			actual.setDefaultValue(meta.getValorDefecto());
			actual.setSourceValues(meta.getListaValores());
			actual.setIncludeInBatchSeprator(meta.getCaratula());
			actual.setIsBatchId(meta.getIsBatchId());
			// anyadimos la especificacion
			metadataSpecs.add(actual);
		}

		// creamos un map con las especificaciones y metadocumentos asociados
		Map<String, List<Long>> mapParentSpec = new HashMap<String, List<Long>>();

		for (DocSpecDefInherVO docSpecInher: docSpecVO.getDefInherDocSpec()) {
			// obtenemos la spec del metadato heredado
			String specInher = docSpecInher.getDefDoc();
			// obtenemos el id del metadato heredado
			Long idMetaInher = docSpecInher.getIdMetaSpec();

			List<Long> listMetas;

			if (mapParentSpec.containsKey(specInher)) {
				listMetas = mapParentSpec.get(specInher);
				listMetas.add(idMetaInher);
				mapParentSpec.remove(specInher);
			} else { // lo crea
				listMetas = new ArrayList<Long>();
				listMetas.add(idMetaInher);
				// mapParentSpec.put(specInher, listMetas);
			}
			mapParentSpec.put(specInher, listMetas);
		}

		List<DocumentSpecificationDTO> parentDocSpecs = new ArrayList<DocumentSpecificationDTO>();
		List<DocumentSpecificationDTO> childDocSpecs = new ArrayList<DocumentSpecificationDTO>();

		// prepamos el listado de los padres
		for (Map.Entry<String, List<Long>> entidad: mapParentSpec.entrySet()) {
			DocumentSpecificationDTO padre = webscanModelManager.getDocSpecificationByName(entidad.getKey());
			parentDocSpecs.add(padre);
		}

		// preparamos listado de los hijos
		if (docSpecVO.getIdDocSpec() != null) {
			childDocSpecs = webscanModelManager.getChildDocSpecifications(docSpecVO.getIdDocSpec());
			// otra implementacion
		}

		// no es nuevo
		if (doc.getId() != null) {
			// enviamos al manager para actualizar la especificacion de
			// documento
			webscanModelManager.updateDocSpecification(doc, metadataSpecs, parentDocSpecs, childDocSpecs, propagateActivation);
		} else {
			// enviamos al manager para crearlo
			doc = webscanModelManager.createDocSpecification(doc, metadataSpecs, parentDocSpecs, childDocSpecs);
		}
		return doc;
	}

	/**
	 * Elimina la información asociada a la especificaciones de documentos
	 * incorporado como argumento, incluida la información de metadatos propios
	 * y heredados de cada uno.
	 * 
	 * @param request
	 *            Petición http.
	 * @param specificationIds
	 *            Listado con los ids de las especificaciones de documentos a
	 *            eliminar.
	 * @throws DAOException
	 *             Si el parámetro de entrada no es válido, no existe algún
	 *             idDocSpec del listado incorporado como argumento, o se
	 *             produce algún error en las consultas, modificaciones a BBDD.
	 */

	public void removeDocsSpec(HttpServletRequest request, List<Long> specificationIds) throws DAOException {
		LOG.debug("Start removeDocSpec");
		final HttpSession session;
		Exception exception = null;
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";
		session = request.getSession(false);
		try {
			// obtenemos la info para auditoria
			UserInSession user = (UserInSession) session.getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
			username = user.getUsername();

			webscanModelManager.removeDocSpecification(specificationIds);

			LOG.debug("salvamos la información en auditoria");
			auditService.createAuditLogs(username, result, authTimestamp, exception, AuditOperationName.DOCUMENT_SPEC_REMOVE, AuditEventName.DOCUMENT_SPEC_REMOVE);

		} catch (DAOException e) {
			result = AuditOpEventResult.ERROR;
			exception = e;
			throw e;
		}

		LOG.debug("End removeDocSpec");
	}

	/**
	 * Elimina la información asociada a la especificacion de documento
	 * incorporado como argumento, incluida la información de metadatos propios
	 * y heredados de cada uno.
	 * 
	 * @param request
	 *            Petición http.
	 * @param specificationId
	 *            specificationIds Listado con los ids de las especificaciones
	 *            de documentos a eliminar.
	 * @throws DAOException
	 *             Si el parámetro de entrada no es válido, no existe algún
	 *             idDocSpec del listado incorporado como argumento, o se
	 *             produce algún error en las consultas, modificaciones a BBDD.
	 */

	public void removeDocSpec(HttpServletRequest request, Long specificationId) throws DAOException {
		LOG.debug("Start removeDocSpec");
		final HttpSession session;
		Exception exception = null;
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";
		session = request.getSession(false);
		try {
			// obtenemos la info para auditoria
			UserInSession user = (UserInSession) session.getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
			username = user.getUsername();

			List<Long> specificationsId = new ArrayList<Long>();
			specificationsId.add(specificationId);
			webscanModelManager.removeDocSpecification(specificationsId);

			LOG.debug("salvamos la información en auditoria");
			auditService.createAuditLogs(username, result, authTimestamp, exception, AuditOperationName.DOCUMENT_SPEC_REMOVE, AuditEventName.DOCUMENT_SPEC_REMOVE);

		} catch (DAOException e) {
			result = AuditOpEventResult.ERROR;
			exception = e;
			throw e;
		}

		LOG.debug("End removeDocSpec");
	}

	/**
	 * Comprueba que no exista ningún documento asociado a la especificación de
	 * documento pasada en vuelo.
	 * 
	 * @param idDocSpec
	 *            Identificador de la especificacion de documentos que tenemos
	 *            que comprobar que no exista ningún documento escaneado en
	 *            vuelo.
	 * @return Devuelve true si no existen documentos en vuelos para la
	 *         especificación de documento pasada como parámetro. En caso
	 *         contrario, retorna false.
	 * @throws DAOException
	 *             Si se produce algún error ejecutando la consulta en BBDD.
	 */

	public Boolean checkNotDocumentFlyng(Long idDocSpec) throws DAOException {
		Boolean res = Boolean.TRUE;
		// comprueba que no exista documentos al vuelo.
		res = !webscanModelManager.hasDocSpecificationDocsInScanProccess(idDocSpec);

		return res;
	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Establece el servicio interno para el registro y consulta de trazas y
	 * eventos de auditoría de Webscan.
	 * 
	 * @param anAuditManager
	 *            nuevo servicio interno para el registro y consulta de trazas y
	 *            eventos de auditoría de Webscan.
	 */
	public void setAuditService(AuditService anAuditManager) {
		this.auditService = anAuditManager;
	}

}
