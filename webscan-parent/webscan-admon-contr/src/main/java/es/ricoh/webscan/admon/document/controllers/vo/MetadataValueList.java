/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.admon.document.controllers.vo.MetadataValueList.java.</p>
* <b>Descripción:</b><p> Representación Java de la lista de valores asignables a un metadato, definida
* en notación JSON.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.admon.document.controllers.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Representación Java de la lista de valores asignables a un metadato, definida
 * en notación JSON.
 * <p>
 * Clase MetadataValueList.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "value" })
public class MetadataValueList implements Serializable {

	/**
	 * Serial de la clase.
	 */
	private static final long serialVersionUID = -3362610660269585499L;

	/**
	 * Listado de valores.
	 */
	@JsonProperty("value")
	private List<Value> value = new ArrayList<Value>();

	/**
	 * Metodo para obtener el listado de valores de los metadatos.
	 * 
	 * @return Listado de valores de los metadatos.
	 */

	@JsonProperty("value")
	public List<Value> getValue() {
		return value;
	}

	/**
	 * Metodo para modificar el listado de valores de los metadatos.
	 * 
	 * @param avalue
	 *            Listado de valores de metadatos.
	 */

	@JsonProperty("value")
	public void setValue(List<Value> avalue) {
		this.value = avalue;
	}

}
