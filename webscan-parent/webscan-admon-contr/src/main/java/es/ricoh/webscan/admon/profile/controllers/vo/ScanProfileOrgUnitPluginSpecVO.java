/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.profile.controllers.vo;

import java.util.List;

import es.ricoh.webscan.model.dto.PluginDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;

/**
 * Entidad que usamos para visualizar la información asociada a la relación
 * entre perfil de escaneo,unidades organizativas y especificación de plugin.
 * <p>
 * Class ScanProfileOrgUnitPluginSpecVO.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ScanProfileOrgUnitPluginSpecVO {

	/**
	 * Fase del plugin.
	 */

	private ScanProfileSpecScanProcStageDTO fase;

	/**
	 * definicion de plugins de la fase.
	 */
	private List<PluginSpecificationDTO> defPlugins;

	/**
	 * configuracion de plugins correspondiente a las definiciones .
	 */
	private List<PluginDTO> confPlugins;

	/**
	 * Indicador del id de definicion del plugin.
	 */

	private Long activoIdPlugDef;

	/**
	 * Indicador del id de configuracion del plugin.
	 */

	private Long activoIdPlugConf;

	/**
	 * Indicador de activo del plugin configurado.
	 */

	private Boolean activoPlugConf;

	/**
	 * Constructor method for the class ScanProfileOrgUnitPluginSpecVO.java.
	 * 
	 * @param afase
	 *            Entidad con la información asociada a la fase.
	 * @param adefPlugins
	 *            listado con las definiciones de plugins.
	 * @param aconfPlugins
	 *            listado con las configuraciones de plugins.
	 * @param aactivoIdPlugDef
	 *            identificador de la definición de plugin activa.
	 * @param aactivoIdPlugConf
	 *            identificador de la configuración de plugin activa.
	 * @param aactivoPlugConf
	 *            boolean indicando si la configuración de plugin está activa.
	 */
	public ScanProfileOrgUnitPluginSpecVO(ScanProfileSpecScanProcStageDTO afase, List<PluginSpecificationDTO> adefPlugins, List<PluginDTO> aconfPlugins, Long aactivoIdPlugDef, Long aactivoIdPlugConf, Boolean aactivoPlugConf) {

		this.fase = afase;
		this.activoIdPlugDef = aactivoIdPlugDef;
		this.activoIdPlugConf = aactivoIdPlugConf;
		this.defPlugins = adefPlugins;
		this.confPlugins = aconfPlugins;
		this.activoPlugConf = aactivoPlugConf;

	}

	/**
	 * Gets the value of the attribute {@link #fase}.
	 * 
	 * @return the value of the attribute {@link #fase}.
	 */
	public ScanProfileSpecScanProcStageDTO getFase() {
		return fase;
	}

	/**
	 * Sets the value of the attribute {@link #fase}.
	 * 
	 * @param afase
	 *            The value for the attribute {@link #fase}.
	 */
	public void setFase(ScanProfileSpecScanProcStageDTO afase) {
		this.fase = afase;
	}

	/**
	 * Gets the value of the attribute {@link #defPlugins}.
	 * 
	 * @return the value of the attribute {@link #defPlugins}.
	 */
	public List<PluginSpecificationDTO> getDefPlugins() {
		return defPlugins;
	}

	/**
	 * Sets the value of the attribute {@link #defPlugins}.
	 * 
	 * @param adefPlugins
	 *            The value for the attribute {@link #defPlugins}.
	 */
	public void setDefPlugins(List<PluginSpecificationDTO> adefPlugins) {
		this.defPlugins = adefPlugins;
	}

	/**
	 * Gets the value of the attribute {@link #confPlugins}.
	 * 
	 * @return the value of the attribute {@link #confPlugins}.
	 */
	public List<PluginDTO> getConfPlugins() {
		return confPlugins;
	}

	/**
	 * Sets the value of the attribute {@link #confPlugins}.
	 * 
	 * @param aconfPlugins
	 *            The value for the attribute {@link #confPlugins}.
	 */
	public void setConfPlugins(List<PluginDTO> aconfPlugins) {
		this.confPlugins = aconfPlugins;
	}

	/**
	 * Gets the value of the attribute {@link #activoIdPlugDef}.
	 * 
	 * @return the value of the attribute {@link #activoIdPlugDef}.
	 */
	public Long getActivoIdPlugDef() {
		return activoIdPlugDef;
	}

	/**
	 * Sets the value of the attribute {@link #activoIdPlugDef}.
	 * 
	 * @param aactivoIdPlugDef
	 *            The value for the attribute {@link #activoIdPlugDef}.
	 */
	public void setActivoIdPlugDef(Long aactivoIdPlugDef) {
		this.activoIdPlugDef = aactivoIdPlugDef;
	}

	/**
	 * Gets the value of the attribute {@link #activoIdPlugConf}.
	 * 
	 * @return the value of the attribute {@link #activoIdPlugConf}.
	 */
	public Long getActivoIdPlugConf() {
		return activoIdPlugConf;
	}

	/**
	 * Sets the value of the attribute {@link #activoIdPlugConf}.
	 * 
	 * @param aactivoIdPlugConf
	 *            The value for the attribute {@link #activoIdPlugConf}.
	 */
	public void setActivoIdPlugConf(Long aactivoIdPlugConf) {
		this.activoIdPlugConf = aactivoIdPlugConf;
	}

	/**
	 * Gets the value of the attribute {@link #activoPlugConf}.
	 * 
	 * @return the value of the attribute {@link #activoPlugConf}.
	 */
	public Boolean getActivoPlugConf() {
		return activoPlugConf;
	}

	/**
	 * Sets the value of the attribute {@link #activoPlugConf}.
	 * 
	 * @param aactivoPlugConf
	 *            The value for the attribute {@link #activoPlugConf}.
	 */
	public void setActivoPlugConf(Boolean aactivoPlugConf) {
		this.activoPlugConf = aactivoPlugConf;
	}

}
