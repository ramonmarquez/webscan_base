/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.profile.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

import es.ricoh.webscan.admon.AdmonWebscanConstants;
import es.ricoh.webscan.admon.profile.controllers.vo.InfoSaveVO;
import es.ricoh.webscan.admon.profile.controllers.vo.ScanProfileOrgUnitDocSpecVO;
import es.ricoh.webscan.admon.profile.controllers.vo.ScanProfileOrgUnitPluginSpecVO;
import es.ricoh.webscan.admon.profile.controllers.vo.ScanProfileSpecificationExtend;
import es.ricoh.webscan.base.controller.vo.UserInSession;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.dto.DocumentSpecificationDTO;
import es.ricoh.webscan.model.dto.OrganizationUnitDTO;
import es.ricoh.webscan.model.dto.PluginDTO;
import es.ricoh.webscan.model.dto.PluginScanProcStageDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDocSpecDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitPluginDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecScanProcStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileSpecificationDTO;
import es.ricoh.webscan.model.service.AuditService;

/**
 * Implementación de la lógica de negocio de los perfiles de escaneos.
 * <p>
 * Class ScanProfileService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ScanProfileService {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScanProfileService.class);

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre el modelo de datos.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Servicio interno para el registro y consulta de trazas y eventos de
	 * auditoría del módulo de gestión de usuarios de Webscan.
	 */
	private AuditService auditService;

	/**
	 * Obtiene un listado de las definiciones de los perfiles de escaneo.
	 * 
	 * @param locale
	 *            Identificador del language de la petición realizada.
	 * @return Listado de las definiciones o especificaciones de los perfiles de
	 *         escaneo.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public List<ScanProfileSpecificationExtend> getListScanProfileSpecifications(Locale locale) throws DAOException {
		LOG.debug("Start getScanProfileSpecifications");

		List<ScanProfileSpecificationExtend> resultado = new ArrayList<ScanProfileSpecificationExtend>();

		// obtenemos todas las especificaciones
		List<ScanProfileSpecificationDTO> listProfSpec = webscanModelManager.listScanProfileSpecifications(null, null, null);
		StringBuilder pluginsString;

		for (ScanProfileSpecificationDTO scanProfileSpecification: listProfSpec) {
			pluginsString = new StringBuilder();

			// obtenemos toda la información asociada a la especificacion del
			// perfil de escaneo

			Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> mapFasePlugins = webscanModelManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfileSpecification.getId());

			int contFase = mapFasePlugins.size();
			int actual = 0;

			/*Recorremos el mapa por fase y a continuación indicamos los plugins definidos para dicha fase*/
			for (Entry<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> elto: mapFasePlugins.entrySet()) {

				// obtenemos la información base
				ScanProfileSpecScanProcStageDTO stage = elto.getKey();

				String faseLocale = recoveryFaseLocale(locale, stage);

				// asignamos la fase o etapa
				pluginsString.append(faseLocale);
				actual++;
				if (contFase != actual) {
					pluginsString.append(" - ");
				}
			}

			ScanProfileSpecificationExtend nuevoSpec = new ScanProfileSpecificationExtend(scanProfileSpecification, pluginsString.toString());
			resultado.add(nuevoSpec);
		}

		LOG.debug("End getScanProfileSpecifications");
		return resultado;
	}

	/**
	 * Método para recuperar el String que identifica la fase.
	 * 
	 * @param locale
	 *            Parámetro con la configuración actual del locale de al
	 *            petición
	 * @param stage
	 *            Parámetro con la entidad que relaciona perfil de escaneo con
	 *            la fase del proceso de escaneo
	 * @return String con la identificación de la fase
	 * @throws NoSuchMessageException
	 *             Exception producida al intentar obtener el string de los
	 *             properties según el locale pasado como parámetro
	 * @throws ScanProfileException
	 *             Exception producida al intentar recuperar la fase
	 */
	public String recoveryFaseLocale(Locale locale, ScanProfileSpecScanProcStageDTO stage) throws NoSuchMessageException, ScanProfileException {
		// recuperamos la fase
		String faseBD = stage.getScanProcessStage();
		StringBuilder faseFind = new StringBuilder();
		// añadimos la parte inicial de la busqueda en locale
		faseFind.append("stage.");
		faseFind.append(faseBD);

		String faseLocale = messages.getMessage(faseFind.toString(), null, locale);
		if (faseLocale == null) {
			throw new ScanProfileException(ScanProfileException.CODE_1000, "Fase No definida");
		}
		return faseLocale;
	}

	/**
	 * Obtiene un listado de los perfiles de escaneo.
	 * 
	 * @return Listado de los perfiles de escaneo
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */

	public List<ScanProfileDTO> getListScanProfile() throws DAOException {
		LOG.debug("Start getListScanProfile");

		List<ScanProfileDTO> resultado = webscanModelManager.listScanProfiles(null, null, null);

		LOG.debug("End getListScanProfile");

		return resultado;
	}

	/**
	 * Obtiene el perfil de escaneo solicitado por id.
	 * 
	 * @param scanProfileId
	 *            Id del perfil de escaneo
	 * @return El perfil de escaneo solicitado según id *
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */

	public ScanProfileDTO getScanProfile(Long scanProfileId) throws DAOException {
		LOG.debug("Start getScanProfile");

		ScanProfileDTO resultado = webscanModelManager.getScanProfile(scanProfileId);

		LOG.debug("End getScanProfile");

		return resultado;
	}

	/**
	 * Obtiene la unidad organizativa referenciada por su id.
	 * 
	 * @param organizationUnitId
	 *            Id de la unidad organizativa
	 * @return La entidad de la unidad organizativa identificada
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */

	public OrganizationUnitDTO getOrganizationUnit(Long organizationUnitId) throws DAOException {
		LOG.debug("Start getScanProfile");

		OrganizationUnitDTO resultado = webscanModelManager.getOrganizationUnit(organizationUnitId);

		LOG.debug("End getScanProfile");

		return resultado;
	}

	/**
	 * Obtiene los perfiles de escaneo asociados a una especificación.
	 * 
	 * @param scanProfileSpecId
	 *            Id de la especificación del perfil de escaneo
	 * @return Listado con los perfiles de escaneo asociados a la especificacion
	 *         solicitada
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */

	public List<ScanProfileDTO> getListScanProfileByIdSpec(Long scanProfileSpecId) throws DAOException {
		LOG.debug("Start getListScanProfileByIdSpec");

		List<ScanProfileDTO> resultado = webscanModelManager.getScanProfilesBySpec(scanProfileSpecId, null, null, null);

		LOG.debug("End getListScanProfileByIdSpec");

		return resultado;
	}

	/**
	 * Obtiene el listado de todas las especificaciones de documentos.
	 * 
	 * @return Listado con las especificaciones de documentos existentes
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */

	public List<DocumentSpecificationDTO> getListDocSpecification() throws DAOException {

		LOG.debug("Start getListDocSpecification");

		List<DocumentSpecificationDTO> spe = new ArrayList<DocumentSpecificationDTO>();

		spe = webscanModelManager.listDocSpecifications(null, null, null);

		LOG.debug("End getListDocSpecification");
		return spe;

	}

	/**
	 * Actualiza el Perfil de Escaneo.
	 * 
	 * @param scanProfile
	 *            Objeto con la información del Perfil de Escaneo
	 * @param orgUnitPlugins
	 *            Nueva relación de plugins establecida en las diferentes
	 *            configuraciones del perfil de digitalización en unidades
	 *            organizativas
	 * @param orgUnitDocs
	 *            Nueva relación de documentos establecida en las diferentes
	 *            configuraciones del perfil de digitalización en unidades
	 *            organizativas
	 * @param propagateActivation
	 *            Boolean indicado si se propaga la activación del perfil de
	 *            escaneo
	 * @param update
	 *            Boolean indicando si es una actualización o creación
	 * @param request
	 *            Peticion http.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public void performUpdateScanProfile(HttpServletRequest request, ScanProfileDTO scanProfile, Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins, Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs, Boolean propagateActivation, Boolean update) throws DAOException {
		updateScanProfile(request, scanProfile, orgUnitPlugins, orgUnitDocs, propagateActivation, update);
	}

	/**
	 * Actualiza el Perfil de Escaneo.
	 * 
	 * @param scanProfile
	 *            Objeto con la información del Perfil de Escaneo
	 * @param orgUnitPlugins
	 *            Nueva relación de plugins establecida en las diferentes
	 *            configuraciones del perfil de digitalización en unidades
	 *            organizativas
	 * @param orgUnitDocs
	 *            Nueva relación de documentos establecida en las diferentes
	 *            configuraciones del perfil de digitalización en unidades
	 *            organizativas
	 * @param propagateActivation
	 *            Boolean indicado si se propaga la activación del perfil de
	 *            escaneo
	 * @param update
	 *            Boolean indicando si es una actualización o creación
	 * @param request
	 *            Peticion http.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	private void updateScanProfile(HttpServletRequest request, ScanProfileDTO scanProfile, Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins, Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs, Boolean propagateActivation, Boolean update) throws DAOException {

		LOG.debug("Start updateScanProfile");
		final HttpSession session;
		Exception exception = null;
		Date authTimestamp = new Date();
		AuditOpEventResult result = AuditOpEventResult.OK;
		String username = "";
		session = request.getSession(false);
		AuditOperationName opName;
		AuditEventName evName;

		// obtenemos la info para auditoria
		UserInSession user = (UserInSession) session.getAttribute(AdmonWebscanConstants.USER_SESSION_ATT_NAME);
		username = user.getUsername();

		// si es actualizacion
		if (update) {
			opName = AuditOperationName.SCANPROFILE_UPDATE;
			evName = AuditEventName.SCANPROFILE_UPDATE;
		} else // es nuevo
		{
			opName = AuditOperationName.SCANPROFILE_CREATE;
			evName = AuditEventName.SCANPROFILE_CREATE;

		}

		webscanModelManager.updateScanProfile(scanProfile, orgUnitPlugins, orgUnitDocs, propagateActivation);

		LOG.debug("salvamos la información en auditoria");
		auditService.createAuditLogs(username, result, authTimestamp, exception, opName, evName);

		LOG.debug("End updateScanProfile");
	}

	/**
	 * Obtiene el listado de configuraciones en unidades organizativas de un
	 * determinado perfil de escaneo.
	 * 
	 * @param scanProfileId
	 *            Identificador de perfil de digitalización.scanProfile
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return listado de perfiles de escaneos y unidades organizativas
	 *         relacionadas.
	 */

	public List<ScanProfileOrgUnitDTO> getListScanProfileOrgUnitByScanProfileId(Long scanProfileId) throws DAOException {
		LOG.debug("End getListScanProfileOrgUnitByScanProfileId");

		List<ScanProfileOrgUnitDTO> currentOrgUnits = webscanModelManager.getScanProfileOrgUnitsByScanProfile(scanProfileId);

		LOG.debug("End getListScanProfileOrgUnitByScanProfileId");
		return currentOrgUnits;
	}

	/**
	 * /* Obtiene la relación de fases de proceso de digitalización de una
	 * definición de perfil de digitalización determinada.
	 * 
	 * @param scanProfileSpecId
	 *            Identificador de una definición de perfil de digitalización.
	 * @return relación de fases de proceso de digitalización de una definición
	 *         de perfil de digitalización determinada.
	 * @throws DAOException
	 *             - Si los parámetros de entrada no son válidos, no existe la
	 *             definición de perfil de digitalización, o se produce algún
	 *             error en la consulta a BBDD.
	 */

	public List<ScanProfileSpecScanProcStageDTO> getScanProfileSpecScanProcStage(Long scanProfileSpecId) throws DAOException {
		LOG.debug("Start getScanProfileSpecScanProcStage");
		List<ScanProfileSpecScanProcStageDTO> res = new ArrayList<ScanProfileSpecScanProcStageDTO>();

		res = webscanModelManager.getScanProcStagesByScanProfileSpec(scanProfileSpecId);

		LOG.debug("End getScanProfileSpecScanProcStage");
		return res;

	}

	/**
	 * Método para obtener la especificación del plugin según su identificador.
	 * 
	 * @param pluginSpecificationId
	 *            Parámetro con el identificador de la especificación del plugin
	 * @return Objecto con la especificación del plugin
	 * @throws DAOException
	 *             Exception que puede ser provocada al acceder a la información
	 *             de BBDD.
	 */

	public PluginSpecificationDTO getPluginSpecification(Long pluginSpecificationId) throws DAOException {
		LOG.debug("Start getPluginSpecification");
		PluginSpecificationDTO res = null;

		res = webscanModelManager.getPluginSpecification(pluginSpecificationId);

		LOG.debug("End getPluginSpecification");
		return res;

	}

	/**
	 * Método para obtener el plugin relacionado con una especificación de
	 * plugin según el perfil y la unidad organizativa pasada como parámetros.
	 * 
	 * @param scanProfile
	 *            Parámetro con el perfil solicitado
	 * @param orgUnit
	 *            Parámetro con la unidad organizativa
	 * @param spec
	 *            Parámetro con la especificación del plugin a recuperar
	 * @return Objecto con la información asociada al plugin según el perfil, la
	 *         unidad organizativa y la especificación del perfil
	 * @throws DAOException
	 *             Exception que puede ser provocada al acceder a la información
	 *             de BBDD.
	 */
	public ScanProfileOrgUnitPluginDTO getScanProfileOrgUnitPlugin(ScanProfileDTO scanProfile, OrganizationUnitDTO orgUnit, PluginSpecificationDTO spec) throws DAOException {
		LOG.debug("Start getScanProfileOrgUnitPlugin");
		ScanProfileOrgUnitPluginDTO res = null;

		// buscamos por perfil, unidad organizativa y perfil la especificacion
		res = webscanModelManager.getScanProfileOrgUnitPluginConf(scanProfile, orgUnit, spec);

		LOG.debug("Start getScanProfileOrgUnitPlugin");
		return res;
	}

	/**
	 * Obtiene el listado de configuraciones de plugins de unidades
	 * organizativas y perfil de escaneo.
	 * 
	 * @param scanProfile
	 *            Objeto con la información del Perfil de Escaneo
	 * @param orgUnit
	 *            Objeto con la información de la Unidad Organizativa
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return map la relación de las fases del perfil de escaneo y por cada
	 *         fase la especificación de plugins asociados.
	 */

	public Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> getScanProfileOrgUnitPluginConf(ScanProfileDTO scanProfile, OrganizationUnitDTO orgUnit) throws DAOException {
		LOG.debug("Start getScanProfileOrgUnitPluginConf");

		// recuperamos todas las especificaciones de plugin por fase
		Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> mapStagePlugSpec = webscanModelManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfile.getSpecification());
		if (LOG.isDebugEnabled()) {
			LOG.debug("End getScanProfileOrgUnitPluginConf, res:" + mapStagePlugSpec.size());
		}
		return mapStagePlugSpec;
	}

	/**
	 * Obtiene el listado de configuraciones de plugins de unidades
	 * organizativas y perfil de escaneo.
	 * 
	 * @param scanProfileId
	 *            Id del Perfil de Escaneo
	 * @param orgUnitId
	 *            Id de la Unidad Organizativa
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return map la relación de las fases del perfil de escaneo y por cada
	 *         fase la especificación de plugins asociados.
	 */

	public Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> getScanProfileOrgUnitPluginConf(Long scanProfileId, Long orgUnitId) throws DAOException {
		LOG.debug("Start getScanProfileOrgUnitPluginConf");
		Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> res;
		// List<ScanProfileOrgUnitPluginDTO> res;

		ScanProfileDTO scanProfile = webscanModelManager.getScanProfile(scanProfileId);

		OrganizationUnitDTO orgUnit = webscanModelManager.getOrganizationUnit(orgUnitId);

		res = getScanProfileOrgUnitPluginConf(scanProfile, orgUnit);
		if (LOG.isDebugEnabled()) {
			LOG.debug("End getScanProfileOrgUnitPluginConf, res:" + res.size());
		}
		return res;
	}

	/**
	 * Comprueba si un perfil de digitalización configurado en una unidad
	 * organizativa posee documentos en proceso de digitalización.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Si el perfil de digitalización configurado en una unidad
	 *         organizativa posee documentos en proceso de digitalización,
	 *         retorna true. En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             configuración de perfil de digitalización en unidad
	 *             organizativa, o se produce algún error en la ejecución de la
	 *             sentencia de BBDD.
	 */

	public Boolean hasScanProfileOrgUnitDocsInScanProccess(Long scanProfileOrgUnitId) throws DAOException {
		Boolean res = Boolean.FALSE;
		LOG.debug("Start hasScanProfileOrgUnitDocsInScanProccess");

		res = webscanModelManager.hasScanProfileOrgUnitDocsInScanProccess(scanProfileOrgUnitId);
		if (LOG.isDebugEnabled()) {
			LOG.debug("End hasScanProfileOrgUnitDocsInScanProccess. res" + res);
		}
		return res;
	}

	/**
	 * Obtiene la información de la relación entre la unidad organizativa y
	 * perfil de escaneo.
	 * 
	 * @param scanProfileId
	 *            Id del Perfil de Escaneo
	 * @param orgUnitId
	 *            Id de la Unidad Organizativa
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return Entidad con la relación entre el perfil de escaneo y la unidad
	 *         organizativa.
	 */

	public ScanProfileOrgUnitDTO getScanProfileOrgUnitByScanProfAndOrgUnit(Long scanProfileId, Long orgUnitId) throws DAOException {
		ScanProfileOrgUnitDTO res = null;

		LOG.debug("Start getScanProfileOrgUnitByScanProfAndOrgUnit");
		try {
			res = webscanModelManager.getScanProfileOrgUnitByScanProfileAndOrgUnit(scanProfileId, orgUnitId);
		} catch (DAOException e) {
			// no existe relación entre perfil y entidad
			if (e.getCode().equals(DAOException.CODE_901)) {
				res = null;
				LOG.debug("No existe ninguna relación entre el perfil y la unidad organizativa");
			} else {
				LOG.error("Error recuperar relacion entre perfil y unidad organizativa");
				throw e;
			}
		}
		LOG.debug("End getScanProfileOrgUnitByScanProfAndOrgUnit");

		return res;
	}

	/**
	 * Obtiene la configuración de los documentos asociados al perfil del
	 * escaneo y la unidad organizativa.
	 * 
	 * @param scanProfile
	 *            Objeto con la información del Perfil de Escaneo
	 * @param orgUnit
	 *            Objeto con la información de la Unidad Organizativa
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return listado de especificación de documentos asociados a la relación
	 *         del perfil de escaneo y unidad organizativa pasada como
	 *         parámetros.
	 */

	public List<ScanProfileOrgUnitDocSpecDTO> getScanProfileOrgUnitDocsConf(ScanProfileDTO scanProfile, OrganizationUnitDTO orgUnit) throws DAOException {
		LOG.debug("Start getScanProfileOrgUnitDocsConf");

		List<ScanProfileOrgUnitDocSpecDTO> res = webscanModelManager.getScanProfileOrgUnitDocSpecConf(scanProfile, orgUnit);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Start getScanProfileOrgUnitDocsConf, res" + res.size());
		}
		return res;

	}

	/**
	 * Obtiene la configuración de los documentos asociados al perfil del
	 * escaneo y la unidad organizativa.
	 * 
	 * @param scanProfileId
	 *            Id del Perfil de Escaneo
	 * @param orgUnitId
	 *            Id de la Unidad Organizativa
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return listado de especificación de documentos asociados a la relación
	 *         del perfil de escaneo y unidad organizativa pasada como
	 *         parámetros.
	 */
	public List<ScanProfileOrgUnitDocSpecDTO> getScanProfileOrgUnitDocsConf(Long scanProfileId, Long orgUnitId) throws DAOException {
		LOG.debug("Start getScanProfileOrgUnitDocsConf");

		ScanProfileDTO scanProfile = webscanModelManager.getScanProfile(scanProfileId);

		OrganizationUnitDTO orgUnit = webscanModelManager.getOrganizationUnit(orgUnitId);

		List<ScanProfileOrgUnitDocSpecDTO> res = webscanModelManager.getScanProfileOrgUnitDocSpecConf(scanProfile, orgUnit);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Start getScanProfileOrgUnitDocsConf, res" + res.size());
		}
		return res;

	}

	/**
	 * Obtiene listado de la definición de documentos por perfil de escaneo y
	 * unidad organizativa.
	 * 
	 * @param scanProfileId
	 *            Id del Perfil de Escaneo
	 * @param orgUnitId
	 *            Id de la Unidad Organizativa
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return listado de especificación de documentos asociados a la relación
	 *         del perfil de escaneo y unidad organizativa pasada como
	 *         parámetros.
	 */
	public List<ScanProfileOrgUnitDocSpecVO> getDocSpec(Long scanProfileId, Long orgUnitId) throws DAOException {
		LOG.debug("End getDocSpec");

		List<ScanProfileOrgUnitDocSpecVO> resultado = new ArrayList<ScanProfileOrgUnitDocSpecVO>();
		// comprobamos que no venga informado y el valor sea -1
		if (scanProfileId != -1) {
			// obtenemos el ScanProfileDTO
			ScanProfileDTO scanProfileDTO = webscanModelManager.getScanProfile(scanProfileId);

			OrganizationUnitDTO organizationUnitDTO = webscanModelManager.getOrganizationUnit(orgUnitId);

			// obtenemos los seleccionados (vinculados)
			try {
				List<ScanProfileOrgUnitDocSpecDTO> intermedio = webscanModelManager.getScanProfileOrgUnitDocSpecConf(scanProfileDTO, organizationUnitDTO);

				for (ScanProfileOrgUnitDocSpecDTO aux: intermedio) {
					DocumentSpecificationDTO nuevo = webscanModelManager.getDocSpecification(aux.getDocSpecification());

					ScanProfileOrgUnitDocSpecVO anyadir = new ScanProfileOrgUnitDocSpecVO(nuevo, aux.getOrgScanProfile(), aux.getActive());

					// inicialmente
					resultado.add(anyadir);
				}
			} catch (DAOException e) {
				if (e.getCode().equals(DAOException.CODE_901)) {
					LOG.info("No info relacionada");
				} else
					throw e;
			}

		}
		LOG.debug("End getDocSpec");

		return resultado;
	}

	/**
	 * Obtiene listado de la definición de plugins por perfil de escaneo y
	 * unidad organizativa.
	 * 
	 * @param scanProfileId
	 *            Id del Perfil de Escaneo
	 * @param orgUnitId
	 *            Id de la Unidad Organizativa
	 * @param scanProfileSpecId
	 *            Id de la especificación del perfil de escaneo.
	 * @param locale
	 *            Entidad con la información del language de la petición http.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return listado de especificación de documentos asociados a la relación
	 *         del perfil de escaneo y unidad organizativa pasada como
	 *         parámetros.
	 */

	public List<ScanProfileOrgUnitPluginSpecVO> getPlugins(Long scanProfileId, Long orgUnitId, Long scanProfileSpecId, Locale locale) throws DAOException {
		LOG.debug("Start getPlugins");
		List<ScanProfileOrgUnitPluginSpecVO> res = new ArrayList<ScanProfileOrgUnitPluginSpecVO>();
		ScanProfileDTO scanProfile = null;
		OrganizationUnitDTO orgUnit = null;
		Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> scanProfilePluginSpecDTO = null;

		// obtenemos unitOrg
		orgUnit = webscanModelManager.getOrganizationUnit(orgUnitId);

		// obtenemos scanProfile
		if (scanProfileId != -1) {
			scanProfile = webscanModelManager.getScanProfile(scanProfileId);
			// obtenemos la configuracion de los plugins por fase
			scanProfilePluginSpecDTO = webscanModelManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfile.getSpecification());
		} else {
			// obtenemos la configuracion de los plugins por fase, usamos para
			// la ello directamente la especificación
			scanProfilePluginSpecDTO = webscanModelManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfileSpecId);
		}

		for (Entry<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> elto: scanProfilePluginSpecDTO.entrySet()) {
			// obtenemos la fase
			ScanProfileSpecScanProcStageDTO fase = elto.getKey();

			// preparamos obtenemos la fase según el locale establecido
			String faseLocale = recoveryFaseLocale(locale, fase);
			fase.setScanProcessStage(faseLocale);

			// obtenemo las definiciones o especificaciones
			List<PluginSpecificationDTO> definiciones = elto.getValue();

			// para mostrar las definiciones
			List<PluginSpecificationDTO> defPlugins = new ArrayList<PluginSpecificationDTO>();
			// para mostrar las configuraciones
			List<PluginDTO> confPlugins = new ArrayList<PluginDTO>();

			// variable para registrar el plugin de configuracion seleccionado
			// en la fase en caso de que exista
			Long selectIdPlugConf = null;
			Long selectIdPlugDef = null;
			Boolean activoPlugConf = Boolean.FALSE;

			// obtenemos las configuraciones disponibles según los plugins
			// especificados
			for (PluginSpecificationDTO conf: definiciones) {

				List<PluginDTO> plugins = webscanModelManager.getPluginsBySpecification(conf.getId());

				// añadimos la definicion y configuraciones disponible en la
				// fase
				defPlugins.add(conf);

				confPlugins.addAll(plugins);
				// no es nuevo
				if (scanProfile != null) {
					try {
						ScanProfileOrgUnitPluginDTO selectConf = webscanModelManager.getScanProfileOrgUnitPluginConf(scanProfile, orgUnit, conf);
						if (selectConf != null) {
							activoPlugConf = selectConf.getActive();
							// recorremos el list pluginDTO para seleccionar el
							// elegido
							// de la configuracion
							for (PluginDTO rec: plugins) {
								if (selectConf.getPlugin().equals(rec.getId())) {
									selectIdPlugConf = rec.getId();
									selectIdPlugDef = conf.getId();
									break;
								}
							}
						}
					} catch (DAOException e) {
						LOG.debug("No existe ninguna informacion de configuracion");
					}
				}

			}
			// preparamos el objeto a mostrar
			ScanProfileOrgUnitPluginSpecVO nuevo = new ScanProfileOrgUnitPluginSpecVO(fase, defPlugins, confPlugins, selectIdPlugDef, selectIdPlugConf, activoPlugConf);
			res.add(nuevo);
		}

		LOG.debug("End getPlugins");
		return res;

	}

	/**
	 * Obtiene el listado de especificaciones de plugin configuradas para una
	 * espeficicación de perfil de digitalización determinada.
	 * 
	 * @param scanProfileSpecId
	 *            Id de la Especificación del Perfil de Escaneo
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return listado de especificaciones de plugins asociados a los perfiles
	 *         de escaneos y unidades organizativas.
	 */

	public List<ScanProfileOrgUnitPluginSpecVO> getPluginsBase(Long scanProfileSpecId) throws DAOException {
		LOG.debug("Start getPluginsBase");
		List<ScanProfileOrgUnitPluginSpecVO> res = new ArrayList<ScanProfileOrgUnitPluginSpecVO>();

		// obtenemos la configuracion de los plugins por fase
		Map<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> scanProfilePluginSpecDTO = webscanModelManager.getScanProfileSpecScanProcStagesByScanProfSpecId(scanProfileSpecId);

		for (Entry<ScanProfileSpecScanProcStageDTO, List<PluginSpecificationDTO>> elto: scanProfilePluginSpecDTO.entrySet()) {
			// obtenemos las fase
			ScanProfileSpecScanProcStageDTO fase = elto.getKey();

			// obtenemo las definiciones o especificaciones
			List<PluginSpecificationDTO> definiciones = elto.getValue();

			// para mostrar las configuraciones
			List<PluginDTO> confPlugins = new ArrayList<PluginDTO>();

			// configuraciones correspondiente a las definiciones
			for (PluginSpecificationDTO conf: definiciones) {

				confPlugins.addAll(webscanModelManager.getPluginsBySpecification(conf.getId()));
			}

			// preparamos el objeto a mostrar
			ScanProfileOrgUnitPluginSpecVO nuevo = new ScanProfileOrgUnitPluginSpecVO(fase, definiciones, confPlugins, null, null, Boolean.FALSE);
			res.add(nuevo);
		}
		LOG.debug("End getPluginsBase");
		return res;
	}

	/**
	 * Elimina un perfil de digitalización del sistema.
	 * 
	 * @param scanProfileId
	 *            Id del perfil de digitalización.
	 * @param username
	 *            Nombre de usuario del usuario que solicita el borrado del
	 *            perfil de digitalización.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */

	public void removeScanProfile(Long scanProfileId, String username) throws DAOException {
		LOG.debug("Start removeScanProfile");
		webscanModelManager.removeScanProfile(scanProfileId);
		auditService.createAuditLogs(username, AuditOpEventResult.OK, new Date(), null, AuditOperationName.SCANPROFILE_REMOVE, AuditEventName.SCANPROFILE_REMOVE);
		LOG.debug("End removeScanProfile");
	}

	/**
	 * Registra un nuevo perfil de digitalización en el sistema.
	 * 
	 * @param infoSaveVO
	 *            Parámetro con la información recuperada de la vista de perfil.
	 * @param request
	 *            Parámetro con la petición HTTP.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             perfil de escaneo , o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public void performCreateScanProfile(InfoSaveVO infoSaveVO, HttpServletRequest request) throws DAOException {
		Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPluginsEnd = new HashMap<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>>();
		Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocsEnd = new HashMap<ScanProfileOrgUnitDTO, Map<Long, Boolean>>();

		infoSaveVO.setScanProfile(newScanProfile(infoSaveVO.getScanProfile(), infoSaveVO.getScanProfileSpec()));

		// recoremos los orgUnitPlugins y orgUnitDocs y creamos las
		// nuevas informando el perfil nuevo
		for (ScanProfileOrgUnitDTO orgUnitP: infoSaveVO.getOrgUnitPlugins().keySet()) {
			// creamos auxiliar
			ScanProfileOrgUnitDTO nuevo = new ScanProfileOrgUnitDTO();
			nuevo.setOrgUnit(orgUnitP.getOrgUnit());
			nuevo.setScanProfile(infoSaveVO.getScanProfile().getId());
			List<PluginScanProcStageDTO> listP = infoSaveVO.getOrgUnitPlugins().get(orgUnitP);
			orgUnitPluginsEnd.put(nuevo, listP);
		}

		for (ScanProfileOrgUnitDTO orgUnitD: infoSaveVO.getOrgUnitDocs().keySet()) {
			ScanProfileOrgUnitDTO nuevo = new ScanProfileOrgUnitDTO();
			nuevo.setOrgUnit(orgUnitD.getOrgUnit());
			nuevo.setScanProfile(infoSaveVO.getScanProfile().getId());
			Map<Long, Boolean> map = infoSaveVO.getOrgUnitDocs().get(orgUnitD);
			orgUnitDocsEnd.put(nuevo, map);
		}

		// llamamos al manager principal de actualización de perfil
		updateScanProfile(request, infoSaveVO.getScanProfile(), orgUnitPluginsEnd, orgUnitDocsEnd, infoSaveVO.getPropagate(), Boolean.FALSE);

	}

	/**
	 * Crea un nuevo perfil de escaneo del sistema.
	 * 
	 * @param scanProfile
	 *            Objeto del nuevo perfil de escaneo
	 * @param scanProfileSpecId
	 *            Id de la especificación del perfil de escaneo
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 * @return Entidad del perfil de escaneo.
	 */

	private ScanProfileDTO newScanProfile(ScanProfileDTO scanProfile, Long scanProfileSpecId) throws DAOException {
		LOG.debug("Start newScanProfile");
		ScanProfileDTO res = webscanModelManager.createScanProfile(scanProfile, scanProfileSpecId);
		LOG.debug("End newScanProfile");
		return res;
	}

	/**
	 * Metodo para comprobar si existen documentos en vuelo para el perfil
	 * indicado.
	 * 
	 * @param scanProfileId
	 *            Identificativo del perfil
	 * @return Boolean Indica si existe o no documentos en vuelo.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */

	public Boolean hasScanProfileDocsInScanProccess(Long scanProfileId) throws DAOException {
		Boolean res = Boolean.FALSE;
		LOG.debug("Start hasScanProfileDocsInScanProccess");
		res = webscanModelManager.hasScanProfileDocsInScanProccess(scanProfileId);
		LOG.debug("End hasScanProfileDocsInScanProccess");
		return res;
	}

	/**
	 * Método para comprobar si existen documentos en vuelo a partir de la
	 * especificación de documentos.
	 * 
	 * @param docSpecId
	 *            Identificador de la especificación del documento
	 * @return Boolean Indica si existe o no documentos en vuelo.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public Boolean hasDocSpecificationDocsInScanProccess(Long docSpecId) throws DAOException {
		Boolean res = Boolean.FALSE;
		LOG.debug("Start hasDocSpecificationDocsInScanProccess");
		res = webscanModelManager.hasDocSpecificationDocsInScanProccess(docSpecId);
		LOG.debug("End hasDocSpecificationDocsInScanProccess");
		return res;
	}

	/**
	 * Método para comprobar si existen documentos en vuelo a partir del
	 * identificador del plugin.
	 * 
	 * @param pluginId
	 *            Identificador del plugin
	 * @return Boolean Indica si existe o no documentos en vuelo.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public Boolean hasPluginDocsInScanProccess(Long pluginId) throws DAOException {
		Boolean res = Boolean.FALSE;
		LOG.debug("Start hasPluginDocsInScanProccess");
		res = webscanModelManager.hasPluginDocsInScanProccess(pluginId);
		LOG.debug("End hasPluginDocsInScanProccess");
		return res;
	}

	/**
	 * Recupera una especificación de documento a partir de su identificador.
	 * 
	 * @param docSpecificationId
	 *            Identificador de especificación de documento.
	 * @return La definición de documento solicitada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public DocumentSpecificationDTO getDocSpecification(Long docSpecificationId) throws DAOException {
		LOG.debug("Start getDocSpecification");
		DocumentSpecificationDTO res;
		res = webscanModelManager.getDocSpecification(docSpecificationId);
		LOG.debug("End getDocSpecification");
		return res;
	}

	/**
	 * Obtiene un plugin a partir de su identificador.
	 * 
	 * @param pluginId
	 *            Identificador de plugin.
	 * @return El plugin solicitado.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe el
	 *             plugin solicitado, o se produce algún error en la consulta a
	 *             BBDD.
	 */
	public PluginDTO getPlugin(Long pluginId) throws DAOException {
		PluginDTO res;
		LOG.debug("Start getPlugin");
		res = webscanModelManager.getPlugin(pluginId);
		LOG.debug("End getPlugin");
		return res;
	}

	/**
	 * Comprueba si un perfil de digitalización configurado en una unidad
	 * organizativa posee documentos en proceso de digitalización.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @param docSpecId
	 *            Identificador de definición de documentos.
	 * @return Si el perfil de digitalización configurado en una unidad
	 *         organizativa posee documentos en proceso de digitalización,
	 *         retorna true. En caso contrario, false.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la ejecución de la sentencia de BBDD.
	 */
	public Boolean hasScanProfileOrgUnitDocSpecDocsInScanProccess(Long scanProfileOrgUnitId, Long docSpecId) throws DAOException {
		Boolean res = Boolean.FALSE;
		LOG.debug("Start hasScanProfileOrgUnitDocSpecDocsInScanProccess");
		res = webscanModelManager.hasScanProfileOrgUnitDocSpecDocsInScanProccess(scanProfileOrgUnitId, docSpecId);
		LOG.debug("End hasScanProfileOrgUnitDocSpecDocsInScanProccess");
		return res;
	}

	/**
	 * Recupera las especificaciones de documentos asociadas a una configuración
	 * de perfil de digitalización en unidad organizativa.
	 * 
	 * @param scanProfileOrgUnitId
	 *            Identificador de configuración de perfil de digitalización en
	 *            unidad organizativa.
	 * @return Relación de pares clave / valor, en la que cada entrada es
	 *         identificada por una definición de documentos establecida para la
	 *         configuración de perfil de digitalización en unidad organizativa
	 *         especificada como parámetro de entrada, y cuyo valor se
	 *         corresponde con la relación de asociación de la definición de
	 *         documentos a la configuración de perfil en unidad organizativa.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> getDocSpecificationsByScanProfileOrgUnitId(Long scanProfileOrgUnitId) throws DAOException {
		Map<DocumentSpecificationDTO, ScanProfileOrgUnitDocSpecDTO> res;
		LOG.debug("Start getDocSpecificationsByScanProfileOrgUnitId");
		res = webscanModelManager.getDocSpecificationsByScanProfileOrgUnitId(scanProfileOrgUnitId);
		LOG.debug("End getDocSpecificationsByScanProfileOrgUnitId");
		return res;
	}

	/**
	 * Establece un nuevo objeto de mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Nuevo objeto de mensajes, sujetos a multidioma, que son
	 *            presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Establece el servicio interno para el registro y consulta de trazas y
	 * eventos de auditoría de Webscan.
	 * 
	 * @param anAuditManager
	 *            nuevo servicio interno para el registro y consulta de trazas y
	 *            eventos de auditoría de Webscan.
	 */
	public void setAuditService(AuditService anAuditManager) {
		this.auditService = anAuditManager;
	}

}