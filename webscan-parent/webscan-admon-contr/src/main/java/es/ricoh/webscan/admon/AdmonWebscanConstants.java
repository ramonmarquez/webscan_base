/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p> es.ricoh.webscan.admon.AdmonWebscanConstants.java.</p>
 * <b>Descripción:</b><p> Interfaz que agrupa constantes del componente que gestiona toda la información 
 * relacionada con la gestión de Webscan.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */

package es.ricoh.webscan.admon;

import es.ricoh.webscan.base.WebscanBaseConstants;

/**
 * Interfaz que agrupa constantes del componente que implementa la capa de
 * acceso a datos al modelo de datos de gestión de usuarios de Webscan.
 * <p>
 * Clase UsersWebscanConstants.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.model.WebscanModelConstants
 * @version 2.0.
 */
public interface AdmonWebscanConstants extends WebscanBaseConstants {

	/**
	 * Parámetro que representa el texto de resultado de una operación realizada
	 * correctamente.
	 */

	String ADMON_RESULT_OK = "admon.resultOk";

	/**
	 * Parámetro que representa el texto de resultado de una operación warning.
	 */

	String ADMON_RESULT_WARNING = "admon.warning";

	/**
	 * Parámetro que representa el texto de resultado de una operación realizada
	 * correctamente.
	 */

	String ADMON_ORG_UNIT_NO_INFO = "admon.orgUnit.NoInfo";

	/**
	 * Parámetro que representa el texto de resultado de una operación realizada
	 * correctamente.
	 */
	String ADMON_DEFAULT_ERROR = "default.error.message";

	/**
	 * Parámetro que representa el texto de resultado de una no terminada por
	 * motivo de documentos en vuelo.
	 */

	String ADMON_DOCUMENT_IN_FLY = "warning.doc.infly";
	/**
	 * Parámetro que representa el texto de resultado de una operacion no
	 * terminada por no informar el identificador.
	 */

	String ADMON_ORG_UNIT_ID_NO_EXIT = "warning.orgUnit.notExits";

	/**
	 * Parámetro que representa el texto de resultado de no terminada por motivo
	 * de documentos en vuelo, para el caso concreto de perfil.
	 */
	String ADMON_DOCUMENT_IN_FLY_PROFILE = "warning.doc.infly.profile";

	/**
	 * Parámetro que indica que se ha realizado correctamente la eliminación del
	 * perfil.
	 */

	String ADMON_PROFILE_DELETE_OK = "message.profile.delete";

	/**
	 * Parámetro que representa el texto de resultado de no terminada por motivo
	 * de documentos en vuelo, para el caso concreto de especificacion
	 * documental.
	 */
	String ADMON_DOCUMENT_IN_FLY_DOCUMENT = "warning.doc.infly.document";

	/**
	 * Parámetro que representa el texto de resultado de no seleccionado al
	 * menos un documento para el perfil y unidad organizativa.
	 */
	String ADMON_DOCUMENT_NOT_SELECT = "warning.doc.notSelect";

	/**
	 * Parámetro que representa el texto de resultado de que el plugin requerido
	 * ha sido deshabilitado.
	 */
	String ADMON_PLUGIN_REQUIRED = "warning.plugin.required";

	/**
	 * Parámetro que representa el texto de resultado de que la unidad
	 * organizativa tiene documentos en vuelo.
	 */
	String ADMON_ORGUNIT_WITH_DOC_IN_FLY = "warning.orgunit.withDocInFly";

	/**
	 * Parámetro que representa el texto de resultado de que no puede ser
	 * desvinculado por tener documentos en vuelo.
	 */
	String ADMON_DOCSPEC_UNLINK_WITH_DOC_IN_FLY = "message.result.docUnLink.infly";

	/**
	 * Parámetro que representa el texto de resultado para informar que no hay
	 * al menos una configuración mínima de unidad organizativa y perfil, en
	 * relación a los tipos de documentales y plugins.
	 */
	String ADMON_SCANPROFILE_SETTING_MINIMUM = "message.result.setting.minimum";

	/**
	 * Parámetro que representa el texto de resultado para informar que hay un
	 * metadato duplicado en el guardado del tipo de documento.
	 */
	String ADMON_METADATA_DUPLICATE_ERROR = "error.metadata.duplicate.message";

}
