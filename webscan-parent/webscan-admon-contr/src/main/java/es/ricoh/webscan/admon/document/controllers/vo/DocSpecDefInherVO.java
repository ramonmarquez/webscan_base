/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.document.controllers.vo;

import java.io.Serializable;

/**
 * <p>
 * Class DocSpecDefInherVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class DocSpecDefInherVO implements Serializable {

	/**
	 * Attribute that represents .
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id de identificacion de la especificacion del metadatos.
	 */
	private Long idMetaSpec;

	/**
	 * definicion del documento padre que hereda.
	 */

	private String defDoc;

	/**
	 * Constructor de la entidad.
	 * 
	 * @param aidMetaSpec
	 *            identificador de la especificación de metadatos.
	 * @param adefDoc
	 *            identificador de la definición de documento.
	 */

	public DocSpecDefInherVO(Long aidMetaSpec, String adefDoc) {
		super();
		this.idMetaSpec = aidMetaSpec;
		this.defDoc = adefDoc;
	}

	/**
	 * Obtiene el Identificador de la especificacion de metadato.
	 * 
	 * @return identificador de la especificación del metadato.
	 */
	public Long getIdMetaSpec() {
		return idMetaSpec;
	}

	/**
	 * Modifica el valor deel identificador de la especificación de metadato.
	 * 
	 * @param aidMetaSpec
	 *            identifidador de la especificación de metadato.
	 */
	public void setIdMetaSpec(Long aidMetaSpec) {
		this.idMetaSpec = aidMetaSpec;
	}

	/**
	 * Obtiene el valor del identificador de definición de documentos.
	 * 
	 * @return identificador de definición de documentos
	 */
	public String getDefDoc() {
		return defDoc;
	}

	/**
	 * Modifica el valor de la definición de documentos.
	 * 
	 * @param adefDoc
	 *            identificador de la definición de documentos.
	 */
	public void setDefDoc(String adefDoc) {
		this.defDoc = adefDoc;
	}

}
