/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.plugin.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import es.ricoh.webscan.admon.AdmonWebscanConstants;
import es.ricoh.webscan.admon.document.controllers.vo.ParamDataTypeSpecVO;
import es.ricoh.webscan.admon.document.controllers.vo.ParamSpecVO;
import es.ricoh.webscan.admon.plugin.controllers.vo.ParameterSpecificationVO;
import es.ricoh.webscan.admon.plugin.service.PluginException;
import es.ricoh.webscan.admon.plugin.service.PluginService;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.dto.ParameterDTO;
import es.ricoh.webscan.model.dto.ParameterSpecificationDTO;
import es.ricoh.webscan.model.dto.ParameterTypeDTO;
import es.ricoh.webscan.model.dto.PluginDTO;
import es.ricoh.webscan.model.dto.PluginSpecificationDTO;
import es.ricoh.webscan.model.dto.ScanProcessStageDTO;

/**
 * Controlador de plugins.
 * <p>
 * Class PluginController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Controller
@RequestMapping("/admin")
public class PluginController {

	/**
	 * Constante identificativa de la denominación de la especificación del
	 * plugin.
	 */
	private static final String DEN_PLUGIN_SPEC = "denPluginSpec";
	/**
	 * Constante identificativa de la descripción de la especificación del
	 * plugin.
	 */
	private static final String DESC_PLUGIN_SPEC = "descPluginSpec";
	/**
	 * Constante identificativa de si está activo o no la especificación del
	 * plugin.
	 */
	private static final String ACTIVE_PLUGIN_SPEC = "activePluginSpec";
	/**
	 * Constante identificativa de la propagación de la especificación del
	 * plugin.
	 */
	private static final String PROPAGACION = "propagacion";

	/**
	 * Constante que identifica el valueParam que incorpora la información de la
	 * petición json.
	 */
	private static final String VALUE_PARAM = "valueParam";

	/**
	 * Constante que identifica el nameParam que incorpora la información de la
	 * petición json.
	 */
	private static final String NAME_PARAM = "nameParam";

	/**
	 * Constante que identifica el propagacionConf que incorpora la información
	 * de la petición json.
	 */
	private static final String PROPAGACION_CONF = "propagacionConf";

	/**
	 * Constante que identifica el activePlugin que incorpora la información de
	 * la petición json.
	 */
	private static final String ACTIVE_PLUGIN = "activePlugin";

	/**
	 * Constante que identifica el descPlugin que incorpora la información de la
	 * petición json.
	 */
	private static final String DESC_PLUGIN = "descPlugin";

	/**
	 * Constante que identifica el denPlugin que incorpora la información de la
	 * petición json.
	 */
	private static final String DEN_PLUGIN = "denPlugin";

	/**
	 * Constante que identifica el idPlugin que incorpora la información de la
	 * petición json.
	 */
	private static final String ID_PLUGIN = "idPlugin";

	/**
	 * Constante que identifica el idPluginSpec que incorpora la información de
	 * la petición json.
	 */
	private static final String ID_PLUGIN_SPEC = "idPluginSpec";

	/**
	 * Constante texto que identifica error dentro del controlador en relación
	 * acceso a BBDD.
	 */
	private static final String PLUGIN_CONTROLLER_ERROR_ACCESO_A_BBDD = "[PluginController] Error Acceso a BBDD ";

	/**
	 * Atributo de la vista identificativa de la lista de especificación de
	 * parámetro.
	 */
	private static final String ATTR_VIEW_LIST_PARAM_SPEC = "listParamSpec";

	/**
	 * Atributo de la vista identificativa de la configuración de plugins.
	 */
	private static final String ATTR_VIEW_PLUGINS_PARAM_CONF = "pluginsParamConf";

	/**
	 * Atributo de la vista identificativa de los parámetros del plugin.
	 */
	private static final String ATTR_VIEW_PLUGINS_PARAM = "pluginsParam";

	/**
	 * Atributo de la vista identificativa de plugins.
	 */
	private static final String ATTR_VIEW_PLUGINS = "plugins";

	/**
	 * Atributo de la vista identificativa de la configuración seleccionada del
	 * plugin.
	 */
	private static final String ATTR_VIEW_PLUGIN_CONF_SELECT = "pluginConfSelect";

	/**
	 * Atributo de la vista identificativa de la especificación seleccionada del
	 * plugin.
	 */
	private static final String ATTR_VIEW_PLUGIN_SPEC_SELECT = "pluginSpecSelect";

	/**
	 * Atributo de la vista identificativa de la especificación del tipo de
	 * parámetro.
	 */
	private static final String ATTR_VIEW_TYPE_PARAM_SPEC = "typeParamSpec";

	/**
	 * Atributo de la vista identificativa de la especificación del plugin.
	 */
	private static final String ATTR_VIEW_PLUGINS_SPEC = "pluginsSpec";

	/**
	 * vista modal de configuración de parámetro.
	 */
	private static final String MODAL_PARAM_CONFIG_VIEW = "fragments/admin/parametros :: modalParamConfig";

	/**
	 * vista de listado de configuración de parámetros.
	 */
	private static final String LIST_PARAM_CONFIG_VIEW = "fragments/admin/parametros :: listParamConfig";

	/**
	 * vista modal del listado de tipo de parametros.
	 */
	private static final String LIST_MODAL_TYPE_PARAM_VIEW = "fragments/admin/parametros :: listModalParam";

	/**
	 * vista principal de listado de parámetros por especificación de plugin.
	 */
	private static final String LISTADO_PARAM_BY_PLUGIN_SPEC_VIEW = "fragments/admin/parametros :: listadoParam";

	/**
	 * vista principal de configuración de plugin.
	 */
	private static final String PLUGIN_CONF_VIEW = "fragments/admin/listPlugins :: listConfig";

	/**
	 * vista principal de plugins.
	 */
	private static final String ADMIN_PLUGINS_VIEW = "admin/plugins";

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PluginController.class);

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre las operaciones relacionadas los perfiles de escaneo.
	 */
	@Autowired
	private PluginService pluginService;

	/**
	 * Pagina incial de Plugins.
	 * 
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad para traspasar información entre el controlador y la
	 *            vista.
	 * @param pluginSpec
	 *            String con la especificación del plugin.
	 * @param pluginConf
	 *            String con la configuración del plugin.
	 * @return nombre de plantilla a mostrar
	 */

	@RequestMapping("/plugins")
	public String plugins(HttpServletRequest request, ModelMap model, @RequestParam(value = "pluginSpec", required = false) String pluginSpec, @RequestParam(value = "pluginConf", required = false) String pluginConf) {
		LOG.debug("[WEBSCAN-PLUGIN] Start plugins");
		String args[] = new String[1];
		Map<PluginSpecificationDTO, List<ScanProcessStageDTO>> listPluginSpec = new HashMap<PluginSpecificationDTO, List<ScanProcessStageDTO>>();
		List<ParameterTypeDTO> typeParamSpec = new ArrayList<ParameterTypeDTO>();
		try {
			listPluginSpec = pluginService.listPluginSpecsAndScanProcessStages();
			LOG.debug("[WEBSCAN-PLUGIN] List<PluginSpecificationDTO> size:", listPluginSpec.size());
			typeParamSpec = pluginService.listParameterTypes();
			LOG.debug("[WEBSCAN-PLUGIN] typeParamSpec size:", typeParamSpec.size());
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.debug(PLUGIN_CONTROLLER_ERROR_ACCESO_A_BBDD);

		}

		model.addAttribute(ATTR_VIEW_PLUGINS_SPEC, listPluginSpec);
		model.addAttribute(ATTR_VIEW_TYPE_PARAM_SPEC, typeParamSpec);
		// auxiliar de vista
		model.addAttribute(ATTR_VIEW_PLUGIN_SPEC_SELECT, pluginSpec != null ? pluginSpec : "");
		model.addAttribute(ATTR_VIEW_PLUGIN_CONF_SELECT, pluginConf != null ? pluginConf : "");

		LOG.debug("[WEBSCAN-PLUGIN] End plugins");
		return ADMIN_PLUGINS_VIEW;
	}

	/**
	 * Obtiene la relación de plugins registrada en el sistema para una
	 * especificación de plugin determinada.
	 * 
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad para traspasar la información entre el controlador y
	 *            la vista.
	 * @param idPluginSpec
	 *            Identificador de especificación de plugin.
	 * @return Lista de configuraciones de una especificación de plugin.
	 */
	@RequestMapping("/pluginConf")
	public String pluginConf(HttpServletRequest request, ModelMap model, Long idPluginSpec) {
		LOG.debug("[WEBSCAN-PLUGIN]  Start plugins");
		String args[] = new String[1];
		List<PluginDTO> listPlugin = new ArrayList<PluginDTO>();
		try {
			listPlugin = pluginService.getListPluginBySpec(idPluginSpec);
			LOG.debug("[WEBSCAN-PLUGIN]  List<PluginDTO> size:", listPlugin.size());
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.debug(PLUGIN_CONTROLLER_ERROR_ACCESO_A_BBDD);
		}

		model.addAttribute(ATTR_VIEW_PLUGINS, listPlugin);
		LOG.debug("[WEBSCAN-PLUGIN] End plugins");
		return PLUGIN_CONF_VIEW;
	}

	/**
	 * Obtiene la relación de parámetros registrada en el sistema para una
	 * especificación de plugin determinada.
	 * 
	 * @param model
	 *            Entidad para traspasar información entre la vista y el
	 *            controlador
	 * @param request
	 *            Petición http.
	 * @param namePluginSpec
	 *            Nombre de la especificación de plugin.
	 * @return Lista de parametros de una especificación de plugin.
	 */
	@RequestMapping(value = "/listParamByPluginSpec", method = RequestMethod.POST)
	public String getParamSpecByPluginId(HttpServletRequest request, ModelMap model, String namePluginSpec) {
		LOG.debug("[WEBSCAN-PLUGIN]  Start getParamSpecByPluginId");
		String args[] = new String[1];
		List<ParamSpecVO> listParamSpecVO = new ArrayList<ParamSpecVO>();
		try {

			// obtenemos la definición del plugin
			PluginSpecificationDTO pluginSpec = pluginService.getPluginSpecificationByName(namePluginSpec);
			// obtenemos la especificacion de parametros por id del plugin
			// especificacion
			List<ParameterSpecificationDTO> listParamSpec = pluginService.getListParamSpecByPluginId(pluginSpec.getId());
			// obtenemos el listado de los tipos de parametros
			List<ParameterTypeDTO> typeParamSpec = pluginService.listParameterTypes();
			// creamos el objeto que vamos a usar visualmente
			ParamSpecVO parametro = null;
			ParamDataTypeSpecVO typeSpec = null;
			for (ParameterSpecificationDTO parSpec: listParamSpec) {
				// obtenemos la informacion del tipo de parametro
				Long dataTypeId = parSpec.getTypeId();
				typeSpec = null;
				for (ParameterTypeDTO parType: typeParamSpec) {
					if (parType.getId().equals(dataTypeId)) {
						typeSpec = new ParamDataTypeSpecVO(parType.getId(), parType.getName(), parType.getStringPattern());
						break;
					}
				} // for parType
				parametro = new ParamSpecVO(parSpec.getId(), parSpec.getName(), parSpec.getDescription(), parSpec.getMandatory(), parSpec.getTypeId(), typeSpec);
				listParamSpecVO.add(parametro);
			} // for parSpec
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.debug(PLUGIN_CONTROLLER_ERROR_ACCESO_A_BBDD);
		}
		model.addAttribute(ATTR_VIEW_LIST_PARAM_SPEC, listParamSpecVO);
		LOG.debug("[WEBSCAN-PLUGIN] listParamSpecVO size:", listParamSpecVO.size());
		LOG.debug("[WEBSCAN-PLUGIN] End getParamSpecByPluginId");
		return LISTADO_PARAM_BY_PLUGIN_SPEC_VIEW;
	}

	/**
	 * Obtiene la relación de los typos de parámetros registrada en el sistema.
	 * 
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad para traspasar información entre la vista y el
	 *            controlador.
	 * @return la vista que mostrara la información.
	 */
	@RequestMapping(value = "/listTypeParamModal", method = RequestMethod.GET)
	public String getListParam(HttpServletRequest request, ModelMap model) {
		LOG.debug("[WEBSCAN-PLUGIN] Start getListParam");
		String args[] = new String[1];
		List<ParameterTypeDTO> typeParamSpec = new ArrayList<ParameterTypeDTO>();
		try {
			typeParamSpec = pluginService.listParameterTypes();
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.debug(PLUGIN_CONTROLLER_ERROR_ACCESO_A_BBDD);
		}

		model.addAttribute(ATTR_VIEW_TYPE_PARAM_SPEC, typeParamSpec);
		LOG.debug("[WEBSCAN-PLUGIN] End getListParam");
		return LIST_MODAL_TYPE_PARAM_VIEW;
	}

	/**
	 * Obtiene la relación de los parámetros asociados a la configuracion de un
	 * plugin.
	 * 
	 * @param denomPluginConf
	 *            la denominación de la configuración del plugin.
	 * @param pluginSpecificationId
	 *            id de la especificación de plugin solicitada.
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad usada para traspasar información entre el controlador
	 *            y la vista.
	 * @return la vista que mostrará la información
	 */
	@RequestMapping(value = "/listParamConf", method = RequestMethod.POST)
	public String getListParamConf(HttpServletRequest request, ModelMap model, String denomPluginConf, Long pluginSpecificationId) {
		LOG.debug("[WEBSCAN-PLUGIN]  Start getListParamConf");
		String args[] = new String[1];
		List<ParameterSpecificationVO> paramConf = new ArrayList<ParameterSpecificationVO>();
		List<ParameterDTO> param = new ArrayList<ParameterDTO>();

		try {
			if (denomPluginConf != null && pluginSpecificationId != null && pluginSpecificationId > 0) {
				param = pluginService.listParameterByPluginAndConf(pluginSpecificationId, denomPluginConf);
			}
			// lo añadimos para tener actualizado en caso de cambio en la
			// especificacion
			if (pluginSpecificationId != null && pluginSpecificationId > 0) {

				paramConf = pluginService.listParameterByPluginSpec(pluginSpecificationId);
			}
		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.debug(PLUGIN_CONTROLLER_ERROR_ACCESO_A_BBDD);
		}

		model.addAttribute(ATTR_VIEW_PLUGINS_PARAM, param);
		model.addAttribute(ATTR_VIEW_PLUGINS_PARAM_CONF, paramConf);
		LOG.debug("[WEBSCAN-PLUGIN] pluginsParam size:", param.size());
		LOG.debug("[WEBSCAN-PLUGIN] pluginsParamConf size:", paramConf.size());
		LOG.debug("[WEBSCAN-PLUGIN] End getListParamConf");
		return LIST_PARAM_CONFIG_VIEW;
	}

	/**
	 * Obtiene la relación de los parámetros asociados a la configuracion de un
	 * plugin.
	 * 
	 * @param pluginSpecificationId
	 *            id de la especificación de plugin solicitada.
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad usada para traspasar inforamción entre la vista y el
	 *            controlador.
	 * @return la vista que mostrará la información.
	 */
	@RequestMapping(value = "/loadModalParamConf", method = RequestMethod.POST)
	public String getModalParamConf(HttpServletRequest request, ModelMap model, Long pluginSpecificationId) {
		LOG.debug("Start getModalParamConf");
		String args[] = new String[1];
		List<ParameterSpecificationVO> param = new ArrayList<ParameterSpecificationVO>();
		try {
			if (pluginSpecificationId != null && pluginSpecificationId > 0) {
				param = pluginService.listParameterByPluginSpec(pluginSpecificationId);
			}

		} catch (DAOException e) {
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			LOG.debug(PLUGIN_CONTROLLER_ERROR_ACCESO_A_BBDD);
		}

		model.addAttribute(ATTR_VIEW_PLUGINS_PARAM_CONF, param);
		LOG.debug("pluginsParamConf size:", param.size());
		LOG.debug("End getModalParamConf");
		return MODAL_PARAM_CONFIG_VIEW;
	}

	/**
	 * Guarda la información asociada a la especificación de plugin.
	 * 
	 * @param datosEspecificacion
	 *            String JSON con la informacion asociada a la especificacion a
	 *            guardar.
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad usada para pasar información del controlador a la
	 *            vista.
	 * @return la vista que mostrará la información actualizada
	 */
	@RequestMapping(value = "/saveEspecPlugin", method = RequestMethod.POST)
	public String saveEspecPlugin(HttpServletRequest request, ModelMap model, String datosEspecificacion) {
		LOG.debug("[WEBSCAN-PLUGIN] Start saveEspecPlugin");
		String[ ] args = new String[1];

		try {
			actualizarInfoPluginSpec(request, datosEspecificacion);
			args[0] = "";
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_RESULT_OK, args, ViewMessageLevel.INFO, WebScope.REQUEST, request);

		} catch (PluginException e) {
			LOG.error("[WEBSCAN-PLUGIN] Se produjo un error al tratar la información recibida");
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			// BaseControllerUtils.setDefaultExceptionInRequest(e, request,
			// messages);
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-PLUGIN] Se produjo un error al guardar la información en base de datos");

			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
		}

		LOG.debug("[WEBSCAN-PLUGIN] End saveEspecPlugin");
		// redireccionamos para cargar la pantalla de plugin
		return plugins(request, model, null, null);
		// return "Success";

	}

	/**
	 * @param datosEspecificacion
	 *            JSON con la información recogida del front respecto a la
	 *            información de cambios de especificación de plugin
	 * @param request
	 *            Entidad que recoge la petición realizada
	 * @throws PluginException
	 *             Exception lanzada en caso de problemas al tratar la
	 *             información recibida
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	private void actualizarInfoPluginSpec(HttpServletRequest request, String datosEspecificacion) throws PluginException, DAOException {

		LOG.debug("[WEBSCAN-PLUGIN] Start actualizarInfoPluginSpec");
		try {
			LOG.debug("[WEBSCAN-PLUGIN] Obtenemos el objeto de la especificacion de plugin ");
			// comprobamos la informacion que recibimos
			JSONObject espec = new JSONObject(datosEspecificacion);
			LOG.debug("[WEBSCAN-PLUGIN] recogemos la información del objeto");
			Boolean activePluginSpec = espec.getBoolean(ACTIVE_PLUGIN_SPEC);
			Long idPluginSpec = espec.getLong(ID_PLUGIN_SPEC);
			// realizamos la comprobacion si ha sido desactivado y en tal caso,
			// comprobamos si tiene documentos en vuelo.
			if (activePluginSpec.equals(Boolean.FALSE)) {
				if (!pluginService.checkPluginSpecDoesNotHaveDocsInScanProccess(idPluginSpec)) {
					throw new PluginException(PluginException.CODE_901, "Hay documentos en vuelo, y por tanto no se puede desactivar la especificacion plugin.");
				}
			}
			Boolean propagacion = espec.getBoolean(PROPAGACION);
			String denPluginSpec = espec.getString(DEN_PLUGIN_SPEC);
			String descPluginSpec = espec.getString(DESC_PLUGIN_SPEC);

			LOG.debug("[WEBSCAN-PLUGIN] Obtenemos el listado de la especificacion de parametros");
			JSONArray listParam = espec.getJSONArray(ATTR_VIEW_LIST_PARAM_SPEC);

			// preparamos para la nueva lista
			LOG.debug("[WEBSCAN-PLUGIN] Recorremos el listado de parametros para obtener la información actualizar ");
			List<ParameterSpecificationDTO> listParamEspec = getUpdateListParamEspec(listParam, idPluginSpec);

			LOG.debug("[WEBSCAN-PLUGIN] Obtenemos la especificacion anterior del plugin ");
			PluginSpecificationDTO pluginSpec = pluginService.getPluginSpecification(idPluginSpec);

			pluginSpec.setName(denPluginSpec);
			pluginSpec.setDescription(descPluginSpec);
			pluginSpec.setActive(activePluginSpec);

			LOG.debug("[WEBSCAN-PLUGIN] Actualizamos la información del plugin ");
			// guardamos la info en el modelo
			pluginService.updatePluginSpecification(request, pluginSpec, listParamEspec, propagacion);

		} catch (JSONException e) {
			LOG.error("[WEBSCAN-PLUGIN] Error de parseo de información recibida");
			throw new PluginException(PluginException.CODE_900, "Error de parseo de información recibida");
		}
		LOG.debug("[WEBSCAN-PLUGIN] End actualizarInfoPluginSpec");
	}

	/**
	 * Obtiene la información actualizada de los parametros recuperado del JSON
	 * de entrada.
	 * 
	 * @param listParam
	 *            Lista de Parametros en formato JSON.
	 * @param idPluginSpec
	 *            Identificador de la especificación de plugin.
	 * @return Listado con la especificación de parametros actualizada.
	 * @throws JSONException
	 *             Exception al tratar el JSON de entrada.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	private List<ParameterSpecificationDTO> getUpdateListParamEspec(JSONArray listParam, Long idPluginSpec) throws JSONException, DAOException {
		final String ID = "id";
		final String NAME_PARAM_SPEC = "nameParamSpec";
		final String DESC_PARAM_SPEC = "descParamSpec";
		final String REQ_PARAM_SPEC = "reqParamSpec";
		final String TYPE_PARAM_SPEC_ID = "typeParamSpecId";

		// resultado
		List<ParameterSpecificationDTO> listParamEspec = new ArrayList<ParameterSpecificationDTO>();
		// obtenemos el listado anterior
		List<ParameterSpecificationDTO> listParamEspecOld = pluginService.getParameterSpecListByPluginSpec(idPluginSpec);
		// recorre el JSON para recuperar la información de cada parametro
		// configurado
		for (int i = 0; i < listParam.length(); i++) {
			ParameterSpecificationDTO paramSpec = new ParameterSpecificationDTO();
			JSONObject parametros = listParam.getJSONObject(i);
			Long id = null;
			if (!parametros.isNull(ID)) {
				id = parametros.getLong(ID);
			}
			String nameParamSpec = parametros.getString(NAME_PARAM_SPEC);
			String descParamSpec = parametros.getString(DESC_PARAM_SPEC);
			Long typeParamSpecId = parametros.getLong(TYPE_PARAM_SPEC_ID);
			Boolean reqParamSpec = parametros.getBoolean(REQ_PARAM_SPEC);

			/*comprobamos si existe ya el parametro en el sistema*/
			if (id != null) // no es nuevo
			{
				for (ParameterSpecificationDTO aux: listParamEspecOld) {
					if (aux.getId().equals(id)) {
						paramSpec = aux;
						break;
					}
				}
			}
			// el id no se rellena ya que puede ser nuevo o existente, en
			// nuestro modelo se asigna dinamicamente el id
			paramSpec.setName(nameParamSpec);
			paramSpec.setDescription(descParamSpec);
			paramSpec.setMandatory(reqParamSpec);
			paramSpec.setTypeId(typeParamSpecId);

			// anyadimos a la lista la info
			listParamEspec.add(paramSpec);
		}
		return listParamEspec;
	}

	/**
	 * Guarda la información asociada a la configuracion del plugin
	 * seleccionado.
	 * 
	 * @param datosConfiguracion
	 *            String JSON con la informacion asociada a la especificacion a
	 *            guardar
	 * @param request
	 *            Petición http.
	 * @param model
	 *            Entidad para pasar información del contralador a la vista.
	 * @return la vista que mostrará la información actualizada
	 */

	@RequestMapping(value = "/saveConfPlugin", method = RequestMethod.POST)
	public @ResponseBody String saveConfPlugin(HttpServletRequest request, ModelMap model, String datosConfiguracion) {
		LOG.debug("[WEBSCAN-PLUGIN] Start saveConfPlugin");
		String[ ] args = new String[1];
		try {

			actualizarInfoPlugin(request, datosConfiguracion);

			args[0] = "";
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_RESULT_OK, args, ViewMessageLevel.INFO, WebScope.REQUEST, request);

		} catch (PluginException e) {
			if (PluginException.CODE_900.equals(e.getCode())) {

				LOG.debug("[WEBSCAN-PLUGIN] Se produjo un warning al tratar la información recibida", e.getMessage());
				args[0] = e.getMessage();
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_RESULT_WARNING, args, ViewMessageLevel.WARN, WebScope.REQUEST, request);

			} else {
				LOG.error("[WEBSCAN-PLUGIN] Se produjo un error al tratar la información recibida");
				args[0] = e.getMessage();
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			}
			return "error";
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-PLUGIN] Se produjo un error al guardar la información en base de datos");
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
			return "error";
		}

		LOG.debug("[WEBSCAN-PLUGIN] End saveConfPlugin");
		return "Success";

	}

	/**
	 * Metodo auxiliar para obtener y guardar la informacion del plugin.
	 * 
	 * @param request
	 *            Petición http.
	 * @param datosConfiguracion
	 *            String con la información de configuración realizada en
	 *            formato JSON
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 * @throws PluginException
	 *             Exception al tratar los datos de configuración o bien al
	 *             recuperar la información JSON.
	 */
	private void actualizarInfoPlugin(HttpServletRequest request, String datosConfiguracion) throws PluginException, DAOException {

		LOG.debug("[WEBSCAN-PLUGIN] Start actualizarInfoPlugin");
		try {
			// comprobamos la informacion que recibimos
			LOG.debug("[WEBSCAN-PLUGIN] Recuperamos los datos de configuracion");
			JSONObject conf = new JSONObject(datosConfiguracion);

			LOG.debug("[WEBSCAN-PLUGIN] Obtenemos la información del plugin");
			Boolean activePlugin = conf.getBoolean(ACTIVE_PLUGIN);
			Long idPlugin = conf.get(ID_PLUGIN).equals("") ? null : conf.getLong(ID_PLUGIN);
			// realizamos la comprobacion si ha sido desactivado y en tal caso,
			// comprobamos si tiene documentos en vuelo.
			if (idPlugin != null && activePlugin.equals(Boolean.FALSE) && !pluginService.checkPluginDoesNotHaveDocsInScanProccess(idPlugin)) {
				throw new PluginException(PluginException.CODE_901, "Hay documentos en vuelo, y por tanto no se puede desactivar el plugin.");
			}
			Long idPlugSpec = conf.getLong(ID_PLUGIN_SPEC);
			String denPlugin = conf.getString(DEN_PLUGIN);
			String descPlugin = conf.getString(DESC_PLUGIN);
			Boolean propagacion = conf.getBoolean(PROPAGACION_CONF);

			LOG.debug("[WEBSCAN-PLUGIN] Recuperamos los datos listado de parametros");
			JSONArray listParam = conf.getJSONArray("listParam");

			// obtenemos la relacion de parametros por plugin
			List<ParameterSpecificationVO> listParamSpec = pluginService.listParameterByPluginSpec(idPlugSpec);

			List<ParameterDTO> paramConf = new ArrayList<ParameterDTO>();
			Boolean encontrado = Boolean.TRUE;
			Boolean noEncontradoIn;
			List<ParameterDTO> paramCurrents = null;
			if (idPlugin != null) {
				paramCurrents = pluginService.getParametersByPlugin(idPlugin);
			}

			LOG.debug("[WEBSCAN-PLUGIN] Preparamos la información de los parametros recibidos");
			for (int i = 0; i < listParam.length(); i++) {
				JSONObject parametros = listParam.getJSONObject(i);
				String name = parametros.getString(NAME_PARAM);
				String value = parametros.getString(VALUE_PARAM);

				noEncontradoIn = Boolean.FALSE;
				// buqueda del parametro en la espeficicación
				for (ParameterSpecificationVO parSpec: listParamSpec) {
					if (parSpec.getName().equals(name)) {

						ParameterDTO newOrExist = null;
						// comprobamos si existe ya en nuestra base de datos
						if (paramCurrents != null) {

							for (ParameterDTO current: paramCurrents) {
								if (current.getSpecificationName().equals(name)) {
									// obtenemos la actual configuración del
									// parametro
									newOrExist = current;
									LOG.debug("[WEBSCAN-PLUGIN] Existia ya el parametro..");
									break;
								}
							}
						}

						if (newOrExist == null) {
							newOrExist = new ParameterDTO();
							LOG.debug("[WEBSCAN-PLUGIN] Nuevo parametro..");
						}
						newOrExist.setSpecificationName(name);
						newOrExist.setValue(value);
						paramConf.add(newOrExist);
						noEncontradoIn = Boolean.TRUE;
						break;
					}
				}
				encontrado &= noEncontradoIn;
			}

			if (!encontrado) {
				LOG.error("[WEBSCAN-PLUGIN] Configuración de parametros no esperado");
				throw new PluginException(PluginException.CODE_800, "Error en configuracion de parametros recibidos");
			}

			LOG.debug("[WEBSCAN-PLUGIN] Obtenemos la informacion guardada del plugin para actualizarla en caso de que exista");

			// creamos o actualizamos el plugin
			setPluginInfoData(request, activePlugin, idPlugin, idPlugSpec, denPlugin, descPlugin, propagacion, paramConf);

		} catch (JSONException e) {
			LOG.error("[WEBSCAN-PLUGIN] Error de parseo de información recibida");
			throw new PluginException(PluginException.CODE_900, "Error de parseo de información recibida:" + e.getMessage());
		} catch (DAOException e) {
			if (e.getCode().equals(DAOException.CODE_998)) {
				throw new PluginException(PluginException.CODE_900, "Error de formato:" + e.getMessage());
			} else {
				throw e;
			}

		}

		LOG.debug("[WEBSCAN-PLUGIN] End actualizarInfoPlugin");

	}

	/**
	 * Metodo que realiza la actualización o creación del Plugin.
	 * 
	 * @param request
	 *            Parámetro que identifica la petición
	 * @param activePlugin
	 *            Parámetro con el valor el activePlugin
	 * @param idPlugin
	 *            Parámetro con el valor del idPlugin
	 * @param idPlugSpec
	 *            Parámetro con el valor del idPlugSpec
	 * @param denPlugin
	 *            Parámetro con el valor del denPlugin
	 * @param descPlugin
	 *            Parámetro con el valor del descPlugin
	 * @param propagacion
	 *            Parámetro con el valor del propagacion
	 * @param paramConf
	 *            Parámetro con el valor del paramConf
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos, no existe la
	 *             especificación de plugin, o se produce algún error en la
	 *             consulta a BBDD.
	 */
	private void setPluginInfoData(HttpServletRequest request, Boolean activePlugin, Long idPlugin, Long idPlugSpec, String denPlugin, String descPlugin, Boolean propagacion, List<ParameterDTO> paramConf) throws DAOException {
		PluginDTO plugin;
		if (idPlugin != null) {
			plugin = pluginService.getPlugin(idPlugin);

			// obtenemos la información actual del plugin
			plugin.setName(denPlugin);
			plugin.setDescription(descPlugin);
			plugin.setActive(activePlugin);

			LOG.debug("[WEBSCAN-PLUGIN] Actualizamos la inforamción del plugin y los parametros asociados");
			// actualizamos la informacion llamando al metodo del modelo
			pluginService.updatePlugin(request, plugin, paramConf, propagacion);

		} else {
			// obtenemos la información actual del plugin
			plugin = new PluginDTO();
			plugin.setName(denPlugin);
			plugin.setDescription(descPlugin);
			plugin.setActive(activePlugin);
			pluginService.createPlugin(request, plugin, paramConf, idPlugSpec);

		}
	}

	/**
	 * Controlador que elimina la información relacionada con una configuración
	 * o parametrización de plugin.
	 * 
	 * @param request
	 *            Petición web.
	 * @param model
	 *            Parametro para devolver la información a la vista.
	 * @param pluginIdReqParam
	 *            Parametro identificador del plugin a eliminar.
	 * @return String con la vista resultante tras la ejecución.
	 */
	@RequestMapping(value = "/removePluginInstance", method = RequestMethod.POST)
	public String removePluginInstance(HttpServletRequest request, ModelMap model, String pluginIdReqParam) {
		LOG.debug("Start removePluginInstance");
		Long pluginId;
		PluginDTO plugin = null;
		PluginSpecificationDTO pluginSpec = null;
		ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		String[ ] args = new String[1];
		String res = null;

		try {
			pluginId = Long.valueOf(pluginIdReqParam);
			plugin = pluginService.getPlugin(pluginId);
			pluginSpec = pluginService.getPluginSpecificationByName(plugin.getSpecification());

			if (!pluginService.checkPluginDoesNotHaveDocsInScanProccess(pluginId)) {
				if (LOG.isErrorEnabled()) {
					LOG.error("[WEBSCAN-PLUGIN] El plugin " + pluginIdReqParam + " está condigurado en perfiles de digitalización con documentos en proceso de digitalización.");
				}
				throw new PluginException(PluginException.CODE_901, "");
			} else {

				pluginService.removePluginInstance(request, pluginId);
				args[0] = "";
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_RESULT_OK, args, ViewMessageLevel.INFO, WebScope.REQUEST, attrs.getRequest());
			}
		} catch (NumberFormatException e) {
			LOG.error("[WEBSCAN-PLUGIN] Se produjo un error al tratar la información recibida");
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());
		} catch (PluginException e) {
			args[0] = e.getMessage();
			if (PluginException.CODE_901.equals(e.getCode())) {
				BaseControllerUtils.addCommonMessage("error.plugin.docsInScanProccess", null, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());
			} else {
				BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());
			}
		} catch (DAOException e) {
			LOG.error("[WEBSCAN-PLUGIN] Se produjo un error al acceder a base de datos");
			args[0] = e.getMessage();
			BaseControllerUtils.addCommonMessage(AdmonWebscanConstants.ADMON_DEFAULT_ERROR, args, ViewMessageLevel.ERROR, WebScope.REQUEST, attrs.getRequest());

		}

		res = pluginConf(request, model, pluginSpec.getId());

		LOG.debug("End removePluginInstance");
		return res;
	}

}
