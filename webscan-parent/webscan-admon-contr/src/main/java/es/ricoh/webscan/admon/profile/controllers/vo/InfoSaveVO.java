/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>File:</b><p>es.ricoh.webscan.admon.profile.controllers.vo.InfoSaveVO.java.</p>
 * <b>Description:</b><p> .</p>
 * <b>Project:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.admon.profile.controllers.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.ricoh.webscan.model.dto.PluginScanProcStageDTO;
import es.ricoh.webscan.model.dto.ScanProfileDTO;
import es.ricoh.webscan.model.dto.ScanProfileOrgUnitDTO;

/**
 * Entidad que usamos para la visualización de la información asociada al perfil
 * de escaneo.
 * <p>
 * Class InfoSaveVO.
 * </p>
 * <b>Project:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class InfoSaveVO {

	/**
	 * Attribute that represents si el perfil es nuevo.
	 */
	private Boolean scanProfileNew = Boolean.FALSE;

	/**
	 * Attribute that represents la especificación del perfil.
	 */
	private Long scanProfileSpec;

	/**
	 * Attribute that represents el perfil de escaneo.
	 */
	private ScanProfileDTO scanProfile;

	/**
	 * Attribute that represents si el perfil se propaga o no la activación.
	 */
	private Boolean propagate;

	/**
	 * Attribute that represents la relación de perfil de escaneo y unidad
	 * organizativa con los plugins de cada fase.
	 */
	private Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> orgUnitPlugins = new HashMap<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>>();

	/**
	 * Attribute that represents la relación de perfil de escaneo y unidad
	 * organizativa con los documentos y su activación.
	 */

	private Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> orgUnitDocs = new HashMap<ScanProfileOrgUnitDTO, Map<Long, Boolean>>();

	/**
	 * Constructor method for the class InfoSaveVO.java.
	 */
	public InfoSaveVO() {
		this.scanProfileSpec = null;
		this.scanProfile = null;
		this.propagate = null;
	}

	/**
	 * Gets the value of the attribute {@link #scanProfileSpec}.
	 * 
	 * @return the value of the attribute {@link #scanProfileSpec}.
	 */
	public Long getScanProfileSpec() {
		return scanProfileSpec;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfileSpec}.
	 * 
	 * @param ascanProfileSpec
	 *            The value for the attribute {@link #scanProfileSpec}.
	 */
	public void setScanProfileSpec(Long ascanProfileSpec) {
		this.scanProfileSpec = ascanProfileSpec;
	}

	/**
	 * Gets the value of the attribute {@link #scanProfile}.
	 * 
	 * @return the value of the attribute {@link #scanProfile}.
	 */
	public ScanProfileDTO getScanProfile() {
		return scanProfile;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfile}.
	 * 
	 * @param perfil
	 *            The value for the attribute {@link #scanProfile}.
	 */
	public void setScanProfile(ScanProfileDTO perfil) {
		this.scanProfile = perfil;
	}

	/**
	 * Gets the value of the attribute {@link #propagate}.
	 * 
	 * @return the value of the attribute {@link #propagate}.
	 */
	public Boolean getPropagate() {
		return propagate;
	}

	/**
	 * Sets the value of the attribute {@link #propagate}.
	 * 
	 * @param apropagate
	 *            The value for the attribute {@link #propagate}.
	 */
	public void setPropagate(Boolean apropagate) {
		this.propagate = apropagate;
	}

	/**
	 * Gets the value of the attribute {@link #orgUnitPlugins}.
	 * 
	 * @return the value of the attribute {@link #orgUnitPlugins}.
	 */
	public Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> getOrgUnitPlugins() {
		return orgUnitPlugins;
	}

	/**
	 * Sets the value of the attribute {@link #orgUnitPlugins}.
	 * 
	 * @param aorgUnitPlugins
	 *            The value for the attribute {@link #orgUnitPlugins}.
	 */
	public void setOrgUnitPlugins(Map<ScanProfileOrgUnitDTO, List<PluginScanProcStageDTO>> aorgUnitPlugins) {
		this.orgUnitPlugins = aorgUnitPlugins;
	}

	/**
	 * Gets the value of the attribute {@link #orgUnitDocs}.
	 * 
	 * @return the value of the attribute {@link #orgUnitDocs}.
	 */
	public Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> getOrgUnitDocs() {
		return orgUnitDocs;
	}

	/**
	 * Sets the value of the attribute {@link #orgUnitDocs}.
	 * 
	 * @param aorgUnitDocs
	 *            The value for the attribute {@link #orgUnitDocs}.
	 */
	public void setOrgUnitDocs(Map<ScanProfileOrgUnitDTO, Map<Long, Boolean>> aorgUnitDocs) {
		this.orgUnitDocs = aorgUnitDocs;
	}

	/**
	 * Gets the value of the attribute {@link #scanProfileNew}.
	 * 
	 * @return the value of the attribute {@link #scanProfileNew}.
	 */
	public Boolean getScanProfileNew() {
		return scanProfileNew;
	}

	/**
	 * Sets the value of the attribute {@link #scanProfileNew}.
	 * 
	 * @param ascanProfileNew
	 *            The value for the attribute {@link #scanProfileNew}.
	 */
	public void setScanProfileNew(Boolean ascanProfileNew) {
		this.scanProfileNew = ascanProfileNew;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InfoSaveVO [scanProfileNew=");
		builder.append(scanProfileNew);
		builder.append(", scanProfileSpec=");
		builder.append(scanProfileSpec);
		builder.append(", scanProfile=");
		builder.append(scanProfile);
		builder.append(", propagate=");
		builder.append(propagate);
		builder.append(", orgUnitPlugins=");
		builder.append(orgUnitPlugins);
		builder.append(", orgUnitDocs=");
		builder.append(orgUnitDocs);
		builder.append("]");
		return builder.toString();
	}

}
