/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

package es.ricoh.webscan.admon.document.controllers.vo;

/**
 * Entidad que usamos para trabajar con la vista de especificación de tipo de
 * datos de parámetro.
 * <p>
 * Class ParamDataTypeSpecVO.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class ParamDataTypeSpecVO {

	/**
	 * identificador de la especificación del tipo de dato del parametro.
	 */
	private Long id;
	/**
	 * Nombre de la especificación del tipo de dato del parametro.
	 */
	private String name;
	/**
	 * Patron de la especificación del tipo de dato del parametro.
	 */
	private String stringPattern;

	/**
	 * Constructor method for the class ParamDataTypeSpecVO.java.
	 * 
	 * @param aid
	 *            identificador.
	 * @param aname
	 *            nombre.
	 * @param astringPattern
	 *            patron.
	 */
	public ParamDataTypeSpecVO(Long aid, String aname, String astringPattern) {
		super();
		this.id = aid;
		this.name = aname;
		this.stringPattern = astringPattern;
	}

	/**
	 * Obtiene el identificador de la especificación.
	 * 
	 * @return identificador.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Modifica el identificador de la especificación.
	 * 
	 * @param aid
	 *            identificador.
	 */
	public void setId(Long aid) {
		this.id = aid;
	}

	/**
	 * Obtiene el nombre de la especificación.
	 * 
	 * @return Cadena con el nombre de la especificación.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Modifica el nombre de la especificación.
	 * 
	 * @param aname
	 *            Cadena con el nombre de la especificación.
	 */
	public void setName(String aname) {
		this.name = aname;
	}

	/**
	 * Obtiene el patron de la especificación.
	 * 
	 * @return Cadena con el patro de la especificación.
	 */
	public String getStringPattern() {
		return stringPattern;
	}

	/**
	 * Modifica el patron de la especificación.
	 * 
	 * @param astringPattern
	 *            Cadena con el patron de la especificación.
	 */
	public void setStringPattern(String astringPattern) {
		this.stringPattern = astringPattern;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParamDataTypeSpecVO [id=" + id + ", name=" + name + ", stringPattern=" + stringPattern + "]";
	}

}
