/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.audit.console.service.AuditConsoleService.java.</p>
 * <b>Descripción:</b><p> Lógica de negocio del componente consola de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.audit.console.service;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.audit.console.controller.vo.AuditOperationVO;
import es.ricoh.webscan.auditconsole.reports.CSVAuditReport;
import es.ricoh.webscan.auditconsole.reports.PDFAuditReport;
import es.ricoh.webscan.auditconsole.reports.XLSXAuditReport;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.WebscanModelManager;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditReportDTO;

/**
 * Lógica de negocio del componente consola de auditoría.
 * <p>
 * Class AuditConsoleService.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class AuditConsoleService {

	/**
	 * Constante que identifica las peticiones de archivo csv para los reports
	 * de auditoria.
	 */
	private static final String TYPE_CSV = "csv";

	/**
	 * Constante que identifica las peticiones de archivo excel para los reports
	 * de auditoria.
	 */
	private static final String TYPE_EXCEL = "excel";

	/**
	 * Constante que identifica las peticiones de archivo de tipo pdf para los
	 * reports de auditoria.
	 */
	private static final String TYPE_PDF = "pdf";

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AuditConsoleService.class);

	/**
	 * Clase generadora de informes y listados de trazas de auditoría en formato
	 * CSV.
	 */
	private CSVAuditReport csvAuditReport;

	/**
	 * Clase generadora de informes y listados de trazas de auditoría en formato
	 * PDF.
	 */
	private PDFAuditReport pdfAuditReport;

	/**
	 * Clase generadora de informes y listados de trazas de auditoría en formato
	 * XLSX.
	 */
	private XLSXAuditReport xslxAuditReport;

	/**
	 * Servicio que agrupa operaciones de consulta, creación y modificación
	 * sobre el modelo de datos.
	 */
	private WebscanModelManager webscanModelManager;

	/**
	 * Recupera la relación de denominaciones de trazas de auditoría registrada
	 * en el sistema.
	 * 
	 * @return la relación de denominaciones de trazas de auditoría registrada
	 *         en el sistema.
	 * @throws DAOException
	 *             Si ocurre algún error.
	 */
	public List<AuditOperationName> listAuditOperationNames() throws DAOException {
		LOG.debug("[AUDIT_CONSOLE_SERVICE] listAuditOperationNames");
		return webscanModelManager.listAuditOperationNames();
	}

	/**
	 * Recupera la relación de estados de trazas de auditoría registrada en el
	 * sistema.
	 * 
	 * @return la relación de estados de trazas de auditoría registrada en el
	 *         sistema.
	 * @throws DAOException
	 *             Si ocurre algún error.
	 */
	public List<AuditOperationStatus> listAuditOperationStatus() throws DAOException {
		LOG.debug("[AUDIT_CONSOLE_SERVICE] listAuditOperationStatus");
		return webscanModelManager.listAuditOperationStatus();
	}

	/**
	 * Recupera la relación de denominaciones de eventos de trazas de auditoría
	 * registrada en el sistema.
	 * 
	 * @return la relación de denominaciones de trazas de auditoría registrada
	 *         en el sistema.
	 * @throws DAOException
	 *             Si ocurre algún error.
	 */
	public List<AuditEventName> listAuditEventNames() throws DAOException {
		LOG.debug("[AUDIT_CONSOLE_SERVICE] listAuditEventNames");
		return webscanModelManager.listAuditEventNames();
	}

	/**
	 * Recupera la relación de estados de eventos de trazas de auditoría
	 * registrada en el sistema.
	 * 
	 * @return la relación de estados de eventos de trazas de auditoría
	 *         registrada en el sistema.
	 * @throws DAOException
	 *             Si ocurre algún error.
	 */
	public List<AuditOpEventResult> listAuditOpEventResults() throws DAOException {
		LOG.debug("[AUDIT_CONSOLE_SERVICE] listAuditOpEventResults");
		return webscanModelManager.listAuditOpEventResults();
	}

	/**
	 * Recupera el número total de trazas de auditoría registradas en el sistema
	 * que cumplen una serie de criterios de búsqueda o filtrado.
	 * 
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @return número total de trazas de auditoría registradas en el sistema que
	 *         cumplen una serie de criterios de búsqueda o filtrado.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public Long countAuditOperations(final List<AuditOperationName> auditOperationNames, final AuditOperationStatus auditOperationStatus, final Date auditOpStatusFromDate, final Date auditOpStatusUntilDate, final List<AuditEventName> auditEventNames, final String username, final AuditOpEventResult auditOpEventResult, final Date auditEventStatusFromDate, final Date auditEventStatusUntilDate) throws DAOException {
		LOG.debug("[AUDIT_CONSOLE_SERVICE] countAuditOperations");
		return webscanModelManager.countAuditOperations(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate);
	}

	/**
	 * Recupera el número total de trazas de auditoría registradas en el
	 * sistema.
	 * 
	 * @return número total de trazas de auditoría registradas en el sistema.
	 * @throws DAOException
	 *             Si se produce algún error en la consulta a BBDD.
	 */
	public Long countAuditOperations() throws DAOException {
		LOG.debug("[AUDIT_CONSOLE_SERVICE] countAuditOperations");
		return webscanModelManager.countAuditOperations();
	}

	/**
	 * Obtiene el listado de trazas y eventos de auditoría registradas en el
	 * sistema, permitiendo la paginación, la ordenación del resultado y el
	 * filtrado de los mismos mediante los siguientes datos: - Denominaciones de
	 * las trazas de auditoría. - Estado de las trazas de auditoría. - Periodo
	 * de establecimiento del estado de las trazas de auditoría. -
	 * Denominaciones de los eventos de las trazas de auditoría. - Estado de los
	 * eventos de de las trazas de auditoría. - Periodo de establecimiento del
	 * estado de los eventos de las trazas de auditoría. Todos los filtros son
	 * opcionales.
	 * 
	 * @param auditOperationNames
	 *            Relación de denominaciones de las trazas de auditoría.
	 * @param auditOperationStatus
	 *            Estado de las trazas de auditoría.
	 * @param auditOpStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditOpStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de las
	 *            trazas de auditoría.
	 * @param auditEventNames
	 *            Relación de denominaciones de los eventos de las trazas de
	 *            auditoría.
	 * @param username
	 *            Nombre de usuario que ejecuta eventos de las trazas de
	 *            auditoría.
	 * @param auditOpEventResult
	 *            Estado de los eventos de las trazas de auditoría.
	 * @param auditEventStatusFromDate
	 *            Fecha inicial del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param auditEventStatusUntilDate
	 *            Fecha final del periodo de establecimiento del estado de los
	 *            eventos de las trazas de auditoría.
	 * @param pageNumber
	 *            Página de resultados a devolver.
	 * @param elementsPerPage
	 *            Número de elementos por página.
	 * @param auditOpOrderByColumns
	 *            clausulas de ordenación sobre trazas de auditoría.
	 * @param auditEventOrderByColumns
	 *            clausulas de ordenación sobre eventos de auditoría.
	 * @return listado de trazas de auditoría.
	 * @throws DAOException
	 *             Si los parámetros de entrada no son válidos o se produce
	 *             algún error en la consulta a BBDD.
	 */
	public List<AuditOperationVO> listAndQueryAuditOperations(final List<AuditOperationName> auditOperationNames, final AuditOperationStatus auditOperationStatus, final Date auditOpStatusFromDate, final Date auditOpStatusUntilDate, final List<AuditEventName> auditEventNames, final String username, AuditOpEventResult auditOpEventResult, final Date auditEventStatusFromDate, final Date auditEventStatusUntilDate, final Long pageNumber, final Long elementsPerPage, final List<EntityOrderByClause> auditOpOrderByColumns, final List<EntityOrderByClause> auditEventOrderByColumns) throws DAOException {
		LOG.debug("[AUDIT_CONSOLE_SERVICE] listAndQueryAuditOperations Start");
		AuditReportDTO element;

		List<AuditOperationVO> res = new ArrayList<AuditOperationVO>();
		List<AuditReportDTO> result = webscanModelManager.listAndQueryAuditOperations(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate, pageNumber, elementsPerPage, auditOpOrderByColumns, auditEventOrderByColumns);
		/*preparamos el objecto auditOperationVO para la vista*/
		for (Iterator<AuditReportDTO> itr = result.iterator(); itr.hasNext();) {
			element = (AuditReportDTO) itr.next();
			res.add(new AuditOperationVO(element));
		}

		LOG.debug("[AUDIT_CONSOLE_SERVICE] listAndQueryAuditOperations End");
		return res;
	}

	/**
	 * Método para poder filtrar los eventos de las operaciones.
	 * 
	 * @param auditEventNames
	 *            Parámetro de nombre de eventos de auditorias
	 * @param username
	 *            Parámetro de nombre de usuario
	 * @param auditOpEventResult
	 *            Parámetro de resultado de eventos de auditoria
	 * @param auditEventStatusFromDate
	 *            Parámetro de fecha de evento (desde)
	 * @param auditEventStatusUntilDate
	 *            Parámetro de fecha de evento (hasta)
	 * @param eventos
	 *            Parámetro con los eventos de operaciones
	 */
	public void filterEventosOp(final List<AuditEventName> auditEventNames, final String username, final AuditOpEventResult auditOpEventResult, final Date auditEventStatusFromDate, final Date auditEventStatusUntilDate, final List<AuditEventDTO> eventos) {
		LOG.debug("[AUDIT_CONSOLE_SERVICE] filterEventosOp Start");
		/* 
		 * Recorremos los eventos y filtramos, ya que no estamos realizando previamente el filtrado de los eventos y lo recuperamos por la operacion.
		 * Por lo tanto, hay que eliminar aquellos eventos que no esten dentro del filtro realizado. 
		 * 
		 * */
		Boolean eliminar = Boolean.FALSE;
		List<AuditEventDTO> evtDelete = new ArrayList<AuditEventDTO>();
		for (AuditEventDTO evt: eventos) {
			if (!auditEventNames.isEmpty() && !auditEventNames.contains(evt.getEventName())) {
				eliminar = Boolean.TRUE;
			} else if (!username.isEmpty() && !evt.getEventUser().contains(username)) {
				eliminar = Boolean.TRUE;
			} else if (auditOpEventResult != null && !evt.getEventResult().equals(auditOpEventResult)) {
				eliminar = Boolean.TRUE;
			} else if (auditEventStatusFromDate != null && auditEventStatusFromDate.compareTo(evt.getEventDate()) > 0) {
				eliminar = Boolean.TRUE;
			} else if (auditEventStatusUntilDate != null && auditEventStatusUntilDate.compareTo(evt.getEventDate()) < 0) {
				eliminar = Boolean.TRUE;
			}
			// tratamiento de cola
			if (eliminar) {
				evtDelete.add(evt);
			}
			eliminar = Boolean.FALSE;
		}
		// borramos los que no debe mostrarse
		eventos.removeAll(evtDelete);
		LOG.debug("[AUDIT_CONSOLE_SERVICE] filterEventosOp End");
	}

	/**
	 * Genera un documento en un formato dato que presenta el informe de trazas
	 * de auditoría solicitado.
	 * 
	 * @param auditLogs
	 *            trazas de auditoría a incluir en el informe.
	 * @param fileType
	 *            formato del informe.
	 * @param locale
	 *            Idioma empleado en la generación del informe.
	 * @return Stream que representa el documento que contiene el informe
	 *         generado.
	 * @throws WebscanException
	 *             Si no es admitido el formato de informe, o se produce algún
	 *             error en la generación del documento que contiene el informe.
	 */
	public OutputStream generateAuditLogReport(final List<AuditOperationVO> auditLogs, final String fileType, final Locale locale) throws WebscanException {
		LOG.debug("[AUDIT_CONSOLE_SERVICE] generateAuditLogReport Start");

		OutputStream res;

		if (TYPE_PDF.equals(fileType)) {
			res = pdfAuditReport.createReport(auditLogs, locale);
		} else if (TYPE_EXCEL.equals(fileType)) {
			res = xslxAuditReport.createReport(auditLogs, locale);
		} else if (TYPE_CSV.equals(fileType)) {
			res = csvAuditReport.createReport(auditLogs, locale);
		} else {
			throw new WebscanException(WebscanException.CODE_995, "El formato de informe " + fileType + " no es admitido por el Sistema.");
		}

		LOG.debug("[AUDIT_CONSOLE_SERVICE] generateAuditLogReport End");
		return res;

	}

	/**
	 * Establece una nueva fachada de gestión y consulta sobre las entidades
	 * pertenecientes al modelo de datos base de Webscan.
	 * 
	 * @param aWebscanModelManager
	 *            nueva fachada de gestión y consulta sobre las entidades
	 *            pertenecientes al modelo de datos base de Webscan.
	 */
	public void setWebscanModelManager(WebscanModelManager aWebscanModelManager) {
		this.webscanModelManager = aWebscanModelManager;
	}

	/**
	 * Establece la clase generadora de informes y listados de trazas de
	 * auditoría en formato CSV.
	 * 
	 * @param aCsvAuditReport
	 *            clase generadora de informes y listados de trazas de auditoría
	 *            en formato CSV.
	 */
	public void setCsvAuditReport(CSVAuditReport aCsvAuditReport) {
		this.csvAuditReport = aCsvAuditReport;
	}

	/**
	 * Establece la clase generadora de informes y listados de trazas de
	 * auditoría en formato CSV.
	 * 
	 * @param aPdfAuditReport
	 *            clase generadora de informes y listados de trazas de auditoría
	 *            en formato PDF.
	 */
	public void setPdfAuditReport(PDFAuditReport aPdfAuditReport) {
		this.pdfAuditReport = aPdfAuditReport;
	}

	/**
	 * Establece la clase generadora de informes y listados de trazas de
	 * auditoría en formato XSLX.
	 * 
	 * @param aXslxAuditReport
	 *            clase generadora de informes y listados de trazas de auditoría
	 *            en formato XSLX.
	 */
	public void setXslxAuditReport(XLSXAuditReport aXslxAuditReport) {
		this.xslxAuditReport = aXslxAuditReport;
	}

}
