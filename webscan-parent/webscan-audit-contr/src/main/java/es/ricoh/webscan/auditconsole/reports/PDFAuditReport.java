/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.auditconsole.reports.PDFAuditReport.java.</p>
* <b>Descripción:</b><p> Clase que imlementa la generación de informes de la utilidad de auditoría en
* formato PDF.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.auditconsole.reports;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.audit.console.controller.vo.AuditOperationVO;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.utilities.Utils;

/**
 * Clase que imlementa la generación de informes de la utilidad de auditoría en
 * formato PDF.
 * <p>
 * Clase PDFAuditReport.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.auditconsole.reports.AuditReports
 * @version 2.0.
 */
public class PDFAuditReport extends AuditReports {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PDFAuditReport.class);

	/**
	 * Nombre del parámetro que representa el texto del asunto del informe.
	 */
	private static final String PDF_INFO_SUBJECT = "audit.report.subject";

	/**
	 * Nombre del parámetro que representa el texto del título del informe.
	 */
	private static final String PDF_INFO_TITLE = "audit.report.title";

	/**
	 * Nombre del parámetro que representa el autor del informe.
	 */
	private static final String PDF_INFO_AUTHOR = "audit.report.author";

	/**
	 * Nombre del parámetro que representa el creador del informe.
	 */
	private static final String PDF_INFO_CREATOR = "audit.report.creator";

	/**
	 * Clase de utilidades que habilita el acceso a los parámetros de
	 * configuración del sistema.
	 */
	private Utils webscanUtilParams;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Constructor sin argumentos.
	 */
	public PDFAuditReport() {
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.auditconsole.reports.AuditReports#createReport(java.util.Map,
	 *      java.util.Locale)
	 */
	@Override
	public OutputStream createReport(List<AuditOperationVO> operations, Locale locale) throws WebscanException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Document pdf = new Document();

		Properties reportProps;
		String excMsg;

		try {
			PdfWriter.getInstance(pdf, baos);
			reportProps = loadStyleSheet(webscanUtilParams);

			// Parámetros generales del PDF
			// A4 apaisado
			pdf.setPageSize(PageSize.A4.rotate());
			pdf.addSubject(messages.getMessage(PDF_INFO_SUBJECT, null, locale));
			pdf.addAuthor(messages.getMessage(PDF_INFO_AUTHOR, null, locale));
			pdf.addCreator(messages.getMessage(PDF_INFO_CREATOR, null, locale));
			pdf.addTitle(messages.getMessage(PDF_INFO_TITLE, null, locale));
			pdf.addCreationDate();
			pdf.open();
			// Creación de la tabla
			PdfPTable table = new PdfPTable(7);
			// Sin dividir filas
			table.setSplitRows(false);
			// 90% del ancho de una página
			table.setWidthPercentage(90f);

			Font fontHeader = new Font(Font.getFamilyIndex(reportProps.getProperty(PROP_STYLE_TITLE_FONT)), Float.parseFloat(reportProps.getProperty(PROP_STYLE_TITLE_FONTSIZE)), getFontStyle(reportProps.getProperty(PROP_STYLE_TITLE_FONTFACE)));
			Font fontBody = new Font(Font.getFamilyIndex(reportProps.getProperty(PROP_STYLE_ROWS_FONT)), Float.parseFloat(reportProps.getProperty(PROP_STYLE_ROWS_FONTSIZE)), getFontStyle(reportProps.getProperty(PROP_STYLE_ROWS_FONTFACE)));

			// Cabecera de 1 fila. Se repetirá en cada página
			table.setHeaderRows(1);
			table.addCell(new PdfPCell(new Phrase(messages.getMessage(TITLE_OPERATION_NAME, null, locale), fontHeader)));
			table.addCell(new PdfPCell(new Phrase(messages.getMessage(TITLE_OPERATION_STARTDATE, null, locale), fontHeader)));
			table.addCell(new PdfPCell(new Phrase(messages.getMessage(TITLE_OPERATION_STATUS, null, locale), fontHeader)));
			table.addCell(new PdfPCell(new Phrase(messages.getMessage(TITLE_EVENT_NAME, null, locale), fontHeader)));
			table.addCell(new PdfPCell(new Phrase(messages.getMessage(TITLE_EVENT_USER, null, locale), fontHeader)));
			table.addCell(new PdfPCell(new Phrase(messages.getMessage(TITLE_EVENT_STARTDATE, null, locale), fontHeader)));
			table.addCell(new PdfPCell(new Phrase(messages.getMessage(TITLE_EVENT_RESULT, null, locale), fontHeader)));

			for (AuditOperationVO op: operations) {

				PdfPCell cell = new PdfPCell(new Phrase(messages.getMessage(op.getName().toString(), null, locale), fontBody));
				cell.setRowspan(op.getListAuditEvent().size());
				table.addCell(cell);
				cell = new PdfPCell(new Phrase(webscanUtilParams.formatDateTime(op.getStartDate()), fontBody));
				cell.setRowspan(op.getListAuditEvent().size());
				table.addCell(cell);
				cell = new PdfPCell(new Phrase(messages.getMessage(op.getStatus().toString(), null, locale), fontBody));
				cell.setRowspan(op.getListAuditEvent().size());
				table.addCell(cell);

				for (AuditEventDTO event: op.getListAuditEvent()) {
					table.addCell(new Phrase(messages.getMessage(event.getEventName().toString(), null, locale), fontBody));
					table.addCell(new Phrase(event.getEventUser(), fontBody));
					table.addCell(new Phrase(webscanUtilParams.formatDateTime(event.getEventDate()), fontBody));
					table.addCell(new Phrase(messages.getMessage(event.getEventResult().name(), null, locale), fontBody));
				}
			}

			pdf.add(table);
			pdf.close();

		} catch (DocumentException e) {
			excMsg = "Se produjo un error al generar un informe de auditoría en formato PDF: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] DocumentException:", excMsg);
			throw new WebscanException(WebscanException.CODE_996, excMsg, e);
		} catch (NoSuchMessageException e) {
			excMsg = "Se produjo un error al generar un informe de auditoría en formato PDF: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] NoSuchMessageException:", excMsg);
			throw new WebscanException(WebscanException.CODE_996, excMsg, e);
		} catch (Exception e) {
			excMsg = "Se produjo un error al generar un informe de auditoría en formato PDF: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] Exception:", excMsg);
			throw new WebscanException(WebscanException.CODE_996, excMsg, e);
		}
		return baos;
	}

	/**
	 * Obtiene el valor numérico que identifica el tipo de estilo aplicado sobre
	 * una fuente.
	 * 
	 * @param style
	 *            cadena de caracteres que representa el tipo de estilo aplicado
	 *            sobre una fuente.
	 * @return valor numérico que identifica el tipo de estilo aplicado sobre
	 *         una fuente.
	 */
	private int getFontStyle(String style) {
		int res = Font.NORMAL;

		if (style != null && !style.isEmpty()) {
			String[ ] styles = style.split("|");
			for (int i = 0; i < styles[i].length(); i++) {
				res += Font.getStyleValue(style);
			}
		}

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.auditconsole.reports.AuditReports#getMimeType()
	 */
	@Override
	public String getMimeType() {
		return "application/pdf";
	}

	/**
	 * Establece la clase de utilidades que habilita el acceso a los parámetros
	 * de configuración del sistema.
	 * 
	 * @param aWebscanUtilParams
	 *            clase de utilidades que habilita el acceso a los parámetros de
	 *            configuración del sistema.
	 */
	public void setWebscanUtilParams(Utils aWebscanUtilParams) {
		this.webscanUtilParams = aWebscanUtilParams;
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Objeto que recopila los mensajes, sujetos a multidioma, que
	 *            son presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}
}
