/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.auditconsole.reports.XLSXAuditReport.java.</p>
* <b>Descripción:</b><p> Clase que imlementa la generación de informes de la utilidad de auditoría en
* formato Excel.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.auditconsole.reports;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.audit.console.controller.vo.AuditOperationVO;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.utilities.Utils;

/**
 * Clase que imlementa la generación de informes de la utilidad de auditoría en
 * formato Excel.
 * <p>
 * Clase XLSXAuditReport.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.auditconsole.reports.AuditReports
 * @version 2.0.
 */
public class XLSXAuditReport extends AuditReports {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(XLSXAuditReport.class);

	/**
	 * Clase de utilidades que habilita el acceso a los parámetros de
	 * configuración del sistema.
	 */
	private Utils webscanUtilParams;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * Constructor sin argumentos.
	 */
	public XLSXAuditReport() {
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.auditconsole.reports.AuditReports#createReport(java.util.Map,
	 *      java.util.Locale)
	 */
	@Override
	public OutputStream createReport(List<AuditOperationVO> operations, Locale locale) throws WebscanException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Properties style = loadStyleSheet(webscanUtilParams);
		String excMsg;
		try {
			/**
			 * TITLE_OPERATION_NAME,TITLE_OPERATION_STARTDATE,TITLE_OPERATION_STATUS,TITLE_EVENT_NAME,TITLE_EVENT_STARTDATE,TITLE_EVENT_RESULT
			 */
			XSSFWorkbook wb = new XSSFWorkbook(); // CreationHelper createHelper
												  // = wb.getCreationHelper();
			Sheet sheet = wb.createSheet(messages.getMessage("label.Audit", null, locale));

			// Cabecera
			Row header = sheet.createRow(0);
			header.setRowStyle(createHeaderStyle(wb, style));
			header.createCell(0).setCellValue(encodeToCodePage(messages.getMessage(TITLE_OPERATION_NAME, null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
			header.createCell(1).setCellValue(encodeToCodePage(messages.getMessage(TITLE_OPERATION_STARTDATE, null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
			header.createCell(2).setCellValue(encodeToCodePage(messages.getMessage(TITLE_OPERATION_STATUS, null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
			header.createCell(3).setCellValue(encodeToCodePage(messages.getMessage(TITLE_EVENT_NAME, null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
			header.createCell(4).setCellValue(encodeToCodePage(messages.getMessage(TITLE_EVENT_USER, null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
			header.createCell(5).setCellValue(encodeToCodePage(messages.getMessage(TITLE_EVENT_STARTDATE, null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
			header.createCell(6).setCellValue(encodeToCodePage(messages.getMessage(TITLE_EVENT_RESULT, null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));

			XSSFCellStyle rowStyle = createRowsStyle(wb, style);
			int rows = 0;
			for (AuditOperationVO op: operations) {
				rows++;
				int initialRow = rows;
				List<AuditEventDTO> events = op.getListAuditEvent();
				for (Iterator<AuditEventDTO> eventIt = events.iterator(); eventIt.hasNext();) {
					AuditEventDTO event = eventIt.next();
					Row current = sheet.createRow(rows++);
					current.setRowStyle(rowStyle);

					current.createCell(0).setCellValue(encodeToCodePage(messages.getMessage(op.getName().toString(), null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
					current.createCell(1).setCellValue(encodeToCodePage(webscanUtilParams.formatDateTime(op.getStartDate()), style.getProperty(PROP_REPORT_CODE_PAGE)));
					current.createCell(2).setCellValue(encodeToCodePage(messages.getMessage(op.getStatus().toString(), null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
					current.createCell(3).setCellValue(encodeToCodePage(messages.getMessage(event.getEventName().toString(), null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
					current.createCell(4).setCellValue(encodeToCodePage(event.getEventUser(), style.getProperty(PROP_REPORT_CODE_PAGE)));
					current.createCell(5).setCellValue(encodeToCodePage(webscanUtilParams.formatDateTime(event.getEventDate()), style.getProperty(PROP_REPORT_CODE_PAGE)));
					current.createCell(6).setCellValue(encodeToCodePage(messages.getMessage(event.getEventResult().name(), null, locale), style.getProperty(PROP_REPORT_CODE_PAGE)));
				}
				rows--;
				int endRow = rows;
				if (LOG.isDebugEnabled()) {
					LOG.debug("Merge from " + initialRow + " to " + endRow);
				}
				sheet.addMergedRegion(new CellRangeAddress(initialRow, endRow, 0, 0));
				sheet.addMergedRegion(new CellRangeAddress(initialRow, endRow, 1, 1));
				sheet.addMergedRegion(new CellRangeAddress(initialRow, endRow, 2, 2));
			}

			wb.write(baos);

		} catch (IOException e) {
			excMsg = "Se produjo un error al generar un informe de auditoría en formato Excel: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] IOException:", excMsg);
			throw new WebscanException(WebscanException.CODE_996, excMsg, e);
		} catch (NoSuchMessageException e) {
			excMsg = "Se produjo un error al generar un informe de auditoría en formato Excel: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] NoSuchMessageException:", excMsg);
			throw new WebscanException(WebscanException.CODE_996, excMsg, e);
		} catch (Exception e) {
			excMsg = "Se produjo un error al generar un informe de auditoría en formato Excel: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] Exception:", excMsg);
			throw new WebscanException(WebscanException.CODE_996, excMsg, e);
		}
		return baos;
	}

	/**
	 * Encode to indicado como parametro.
	 * 
	 * @param in
	 *            Cadena de entrada.
	 * @param codePage
	 *            nombre del enconde a convertir.
	 * @return Cadena resultante
	 */

	private String encodeToCodePage(String in, String codePage) {
		String res;
		Charset outputCharset = Charset.forName(codePage);
		/*
		Charset utf8charset = Charset.forName("UTF-8");
		ByteBuffer aux = ByteBuffer.wrap(in.getBytes(utf8charset));
		
		// decode 
		CharBuffer data = utf8charset.decode(aux);
		
		// encode 
		 ByteBuffer outputBuffer = isoCharset.encode(data);
		*/
		ByteBuffer outputBuffer = outputCharset.encode(in);

		try {
			res = new String(outputBuffer.array(), codePage);
		} catch (UnsupportedEncodingException e) {
			LOG.debug("Error al convertir texto al encode referido");
			res = "";
		}
		return res;
	}

	/**
	 * Establece el estilo de las filas de la tabla incluida en el cuerpo del
	 * informe, a partir de la configuración especificada a nivel de Sistema.
	 * 
	 * @param wb
	 *            libro excel.
	 * @param style
	 *            configuración especificada a nivel de Sistema.
	 * @return Estilo de las celdas de la tabla incluida en el cuerpo del
	 *         informe.
	 */
	private XSSFCellStyle createRowsStyle(XSSFWorkbook wb, Properties style) {

		XSSFCellStyle st = wb.createCellStyle();
		/*
		st.setFillForegroundColor(new XSSFColor(hex2Rgb(style.getProperty(PROP_STYLE_ROWS_BACKGROUND1))));
		st.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		*/
		List<String> fontStyle = Arrays.asList(style.getProperty(PROP_STYLE_ROWS_FONTFACE).split("|"));
		XSSFFont font = wb.createFont();
		font.setBold(fontStyle.contains("bold"));
		font.setItalic(fontStyle.contains("italic"));
		font.setFontName(style.getProperty(PROP_STYLE_ROWS_FONT));
		font.setColor(new XSSFColor(hex2Rgb(style.getProperty(PROP_STYLE_ROWS_FOREGROUND1))));
		font.setFontHeightInPoints(Short.parseShort(style.getProperty(PROP_STYLE_ROWS_FONTSIZE)));
		st.setFont(font);

		st.setVerticalAlignment(VerticalAlignment.TOP);
		switch (style.getProperty(PROP_STYLE_ROWS_ALIGNMENT)) {
			case "right":
				st.setAlignment(HorizontalAlignment.RIGHT);
				break;
			case "left":
				st.setAlignment(HorizontalAlignment.LEFT);
				break;
			case "justify":
				st.setAlignment(HorizontalAlignment.JUSTIFY);
			default:
				st.setAlignment(HorizontalAlignment.CENTER);
				break;
		}

		return st;
	}

	/**
	 * Establece el estilo de la cabecera de la tabla incluida en el cuerpo del
	 * informe, a partir de la configuración especificada a nivel de Sistema.
	 * 
	 * @param wb
	 *            libro excel.
	 * @param style
	 *            configuración especificada a nivel de Sistema.
	 * @return Estilo de las celdas de la cabecera de la tabla incluida en el
	 *         cuerpo del informe.
	 */
	private XSSFCellStyle createHeaderStyle(XSSFWorkbook wb, Properties style) {
		XSSFCellStyle st = wb.createCellStyle();
		/*
		st.setFillForegroundColor(new XSSFColor(hex2Rgb(style.getProperty(PROP_STYLE_TITLE_BACKGROUND))));
		st.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		*/
		List<String> fontStyle = Arrays.asList(style.getProperty(PROP_STYLE_TITLE_FONTFACE).split("|"));

		XSSFFont font = wb.createFont();
		font.setBold(fontStyle.contains("bold"));
		font.setItalic(fontStyle.contains("italic"));
		font.setFontName(style.getProperty(PROP_STYLE_TITLE_FONT));
		font.setColor(new XSSFColor(hex2Rgb(style.getProperty(PROP_STYLE_TITLE_FOREGROUND))));
		font.setFontHeightInPoints(Short.parseShort(style.getProperty(PROP_STYLE_TITLE_FONTSIZE)));
		st.setFont(font);

		st.setVerticalAlignment(VerticalAlignment.TOP);
		switch (style.getProperty(PROP_STYLE_TITLE_ALIGNMENT)) {
			case "right":
				st.setAlignment(HorizontalAlignment.RIGHT);
				break;
			case "left":
				st.setAlignment(HorizontalAlignment.LEFT);
				break;
			case "justify":
				st.setAlignment(HorizontalAlignment.JUSTIFY);
			default:
				st.setAlignment(HorizontalAlignment.CENTER);
				break;
		}

		return st;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.auditconsole.reports.AuditReports#getMimeType()
	 */
	@Override
	public String getMimeType() {
		return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	}

	/**
	 * Establece la clase de utilidades que habilita el acceso a los parámetros
	 * de configuración del sistema.
	 * 
	 * @param aWebscanUtilParams
	 *            clase de utilidades que habilita el acceso a los parámetros de
	 *            configuración del sistema.
	 */
	public void setWebscanUtilParams(Utils aWebscanUtilParams) {
		this.webscanUtilParams = aWebscanUtilParams;
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Objeto que recopila los mensajes, sujetos a multidioma, que
	 *            son presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}
}
