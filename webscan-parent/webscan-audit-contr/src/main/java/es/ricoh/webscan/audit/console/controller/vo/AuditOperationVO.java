/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.base.controller.vo.AuditOperationVO.java.</p>
 * <b>Descripción:</b><p> .</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.audit.console.controller.vo;

import java.util.Date;
import java.util.List;

import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.model.dto.AuditReportDTO;

/**
 * <p>
 * Class .
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public class AuditOperationVO {

	/**
	 * Identificador de la entidad.
	 */
	private Long id;

	/**
	 * Denominación de la entidad.
	 */
	private AuditOperationName name;

	/**
	 * Estado de la entidad.
	 */
	private AuditOperationStatus status;

	/**
	 * Fecha de creación de la entidad.
	 */
	private Date startDate;

	/**
	 * Última fecha de actualización de la entidad.
	 */
	private Date lastUpdateDate;

	/**
	 * Identificador del histórico de documento asociado a la traza de
	 * auditoría. Es opcional.
	 */
	private Long auditScannedDocId;

	/**
	 * Listado de eventos de auditorias asociados a la traza de auditoría.
	 */

	private List<AuditEventDTO> listAuditEvent;

	/**
	 * Constructor method for the class AuditOperationVO.java.
	 * 
	 * @param id
	 * @param name
	 * @param status
	 * @param startDate
	 * @param lastUpdateDate
	 * @param auditScannedDocId
	 * @param listAuditEvent
	 */
	public AuditOperationVO(AuditReportDTO op) {
		super();
		this.id = op.getId();
		this.name = op.getName();
		this.status = op.getStatus();
		this.startDate = op.getStartDate();
		this.lastUpdateDate = op.getLastUpdateDate();
		this.auditScannedDocId = op.getAuditScannedDocId();
		this.listAuditEvent = op.getListAuditEvent();
	}

	/**
	 * Gets the value of the attribute {@link #id}.
	 * 
	 * @return the value of the attribute {@link #id}.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the value of the attribute {@link #id}.
	 * 
	 * @param id
	 *            The value for the attribute {@link #id}.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the value of the attribute {@link #name}.
	 * 
	 * @return the value of the attribute {@link #name}.
	 */
	public AuditOperationName getName() {
		return name;
	}

	/**
	 * Sets the value of the attribute {@link #name}.
	 * 
	 * @param name
	 *            The value for the attribute {@link #name}.
	 */
	public void setName(AuditOperationName name) {
		this.name = name;
	}

	/**
	 * Gets the value of the attribute {@link #status}.
	 * 
	 * @return the value of the attribute {@link #status}.
	 */
	public AuditOperationStatus getStatus() {
		return status;
	}

	/**
	 * Sets the value of the attribute {@link #status}.
	 * 
	 * @param status
	 *            The value for the attribute {@link #status}.
	 */
	public void setStatus(AuditOperationStatus status) {
		this.status = status;
	}

	/**
	 * Gets the value of the attribute {@link #startDate}.
	 * 
	 * @return the value of the attribute {@link #startDate}.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the value of the attribute {@link #startDate}.
	 * 
	 * @param anstartDate
	 *            The value for the attribute {@link #startDate}.
	 */
	public void setStartDate(Date anstartDate) {
		this.startDate = anstartDate;
	}

	/**
	 * Gets the value of the attribute {@link #lastUpdateDate}.
	 * 
	 * @return the value of the attribute {@link #lastUpdateDate}.
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * Sets the value of the attribute {@link #lastUpdateDate}.
	 * 
	 * @param anlastUpdateDate
	 *            The value for the attribute {@link #lastUpdateDate}.
	 */
	public void setLastUpdateDate(Date anlastUpdateDate) {
		this.lastUpdateDate = anlastUpdateDate;
	}

	/**
	 * Gets the value of the attribute {@link #auditScannedDocId}.
	 * 
	 * @return the value of the attribute {@link #auditScannedDocId}.
	 */
	public Long getAuditScannedDocId() {
		return auditScannedDocId;
	}

	/**
	 * Sets the value of the attribute {@link #auditScannedDocId}.
	 * 
	 * @param anauditScannedDocId
	 *            The value for the attribute {@link #auditScannedDocId}.
	 */
	public void setAuditScannedDocId(Long anauditScannedDocId) {
		this.auditScannedDocId = anauditScannedDocId;
	}

	/**
	 * Gets the value of the attribute {@link #listAuditEvent}.
	 * 
	 * @return the value of the attribute {@link #listAuditEvent}.
	 */
	public List<AuditEventDTO> getListAuditEvent() {
		return listAuditEvent;
	}

	/**
	 * Sets the value of the attribute {@link #listAuditEvent}.
	 * 
	 * @param anlistAuditEvent
	 *            The value for the attribute {@link #listAuditEvent}.
	 */
	public void setListAuditEvent(List<AuditEventDTO> anlistAuditEvent) {
		this.listAuditEvent = anlistAuditEvent;
	}

}
