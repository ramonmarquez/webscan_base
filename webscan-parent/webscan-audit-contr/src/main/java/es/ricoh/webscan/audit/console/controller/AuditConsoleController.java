/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
 * <b>Archivo:</b><p>es.ricoh.webscan.audit.console.controller.auditController.java.</p>
 * <b>Descripción:</b><p> Controlador del componente consola de auditoría.</p>
 * <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
 * @author RICOH España IT Services.
 * @version 2.0.
 */
package es.ricoh.webscan.audit.console.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.audit.console.controller.vo.AuditOperationVO;
import es.ricoh.webscan.audit.console.service.AuditConsoleService;
import es.ricoh.webscan.base.WebscanBaseConstants;
import es.ricoh.webscan.base.controller.BaseControllerUtils;
import es.ricoh.webscan.base.controller.vo.ViewMessageLevel;
import es.ricoh.webscan.base.controller.vo.WebScope;
import es.ricoh.webscan.model.AuditEventOrderByClause;
import es.ricoh.webscan.model.AuditOperationOrderByClause;
import es.ricoh.webscan.model.DAOException;
import es.ricoh.webscan.model.EntityOrderByClause;
import es.ricoh.webscan.model.domain.AuditEventName;
import es.ricoh.webscan.model.domain.AuditOpEventResult;
import es.ricoh.webscan.model.domain.AuditOperationName;
import es.ricoh.webscan.model.domain.AuditOperationStatus;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.utilities.MimeType;

/**
 * Controlador del componente consola de auditoría.
 * <p>
 * Class AuditConsoleController.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
@Controller
public class AuditConsoleController {

	/**
	 * Lista que contiene los nombres de las columnas de la tabla de resultados
	 * en el orden en el que son presentadas en el interfaz de usuario.
	 */
	public static final String[ ] AUDIT_COLUMN_ORDER = { "NAME", "STATUS", "LAST_UPDATE_DATE", "EVENT_NAME", "EVENT_USER", "EVENT_DATE", "EVENT_RES", "EVENT_INFO_ADIC" };

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AuditConsoleController.class);

	/**
	 * Servicio que implementa la lógica de negocio del componente consola de
	 * auditoría.
	 */
	@Autowired
	private AuditConsoleService auditConsoleService;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	@Autowired
	private MessageSource messages;

	/**
	 * Pagina principal de la consola de auditoría.
	 * 
	 * @param model
	 *            objeto que agrupa los atributos del modelo presentes en la
	 *            vista.
	 * @param request
	 *            petición web.
	 * @return nombre de la vista a mostrar.
	 */
	@RequestMapping(value = "/audit/auditConsole", method = { RequestMethod.GET })
	public String auditoria(final Model model, HttpServletRequest request) {
		HttpSession session;
		List<AuditOperationName> listAuditOperationName = new ArrayList<AuditOperationName>();
		List<AuditOperationStatus> listAuditOperationStatus = new ArrayList<AuditOperationStatus>();
		List<AuditEventName> listAuditEventName = new ArrayList<AuditEventName>();
		List<AuditOpEventResult> listAuditOpEventResult = new ArrayList<AuditOpEventResult>();

		LOG.debug("[WEBSCAN-AUDIT] Presentando vista principal de auditoria ...");

		session = request.getSession(false);
		// Inicializamos el atributo de session
		session.setAttribute(WebscanBaseConstants.LIST_OP_EVENT_SESSION_ATT_NAME, new ArrayList<AuditOperationVO>());

		try {
			listAuditOperationName = auditConsoleService.listAuditOperationNames();
			listAuditOperationStatus = auditConsoleService.listAuditOperationStatus();
			listAuditEventName = auditConsoleService.listAuditEventNames();
			listAuditOpEventResult = auditConsoleService.listAuditOpEventResults();

		} catch (DAOException e) {
			setDAOExceptionInRequest(e, request, messages);
		}

		model.addAttribute("listDenomTraza", listAuditOperationName);
		model.addAttribute("listEstadoTraza", listAuditOperationStatus);
		model.addAttribute("listDenomEvento", listAuditEventName);
		model.addAttribute("listEstadoEvento", listAuditOpEventResult);

		return "audit/auditConsole";
	}

	/**
	 * Obtiene un valor de un tipo enumerado.
	 * 
	 * @param clazz
	 *            clase que implementa el tipo enumerado.
	 * @param name
	 *            valor del tipo enumerado a recuperar.
	 * @param <E>
	 *            identificador del tipo
	 * @return un valor de un tipo enumerado.
	 */
	private static <E extends Enum<E>> E getEnumerator(Class<E> clazz, String name) {
		E value = null;
		try {
			value = Enum.valueOf(clazz, name);
		} catch (Exception e) {
			LOG.debug("tipo enumerado no existe");
		}
		return value;
	}

	/**
	 * Obtiene una lista de valores de un tipo enumerado.
	 * 
	 * @param clazz
	 *            clase que implementa el tipo enumerado.
	 * @param valor
	 *            lista de valores del tipo enumerado a recuperar.
	 * @param <E>
	 *            identificador del tipo
	 * @return lista de valores de un tipo enumerado.
	 */
	private static <E extends Enum<E>> List<E> getEnumeratores(Class<E> clazz, String[ ] valor) {
		List<E> list = new ArrayList<E>();
		for (int i = 0; i < valor.length; i++) {
			if (!valor[i].equals("-1") && !valor[i].equals("")) {
				E nuevo = Enum.valueOf(clazz, valor[i]);
				list.add(nuevo);
			}
		}
		return list;
	}

	/**
	 * Controlador que implementa la operación de filtrado, paginación y
	 * ordenación sobre las trazade auditoría registrada en el modelo de datos
	 * del Sistema.
	 * 
	 * @param model
	 *            objeto que agrupa los atributos del modelo presentes en la
	 *            vista.
	 * @param request
	 *            petición web.
	 * @return cadena de caracteres en notación JSON que representa el contenido
	 *         de la respuesta.
	 */
	@RequestMapping(value = "/audit/auditOperations", method = { RequestMethod.POST })
	public @ResponseBody String auditoriaJSON(final Model model, HttpServletRequest request) {
		String res = "";

		Map<String, String[ ]> parametros = request.getParameterMap();
		int nextDraw = 0;
		// parametos de funcion de busqueda
		List<AuditOperationName> auditOperationNames = new ArrayList<AuditOperationName>();
		AuditOperationStatus auditOperationStatus = null;
		Date auditOpStatusFromDate = null;
		Date auditOpStatusUntilDate = null;

		List<AuditEventName> auditEventNames = null;
		String username = null;
		AuditOpEventResult auditOpEventResult = null;
		Date auditEventStatusFromDate = null;
		Date auditEventStatusUntilDate = null;

		Long pageNumber = null;
		Long elementsPerPage = null;

		List<EntityOrderByClause> auditOpOrderByColumns = new ArrayList<EntityOrderByClause>();
		List<EntityOrderByClause> auditEventOrderByColumns = new ArrayList<EntityOrderByClause>();

		Long startV = Long.valueOf(0);
		int columnOrder = -1;

		try {
			// obtenemos el formato de los dates
			String dateFormat = messages.getMessage("date.format", null, request.getLocale());
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

			// Imprimimos el Map con un Iterador

			for (Iterator<String> it = parametros.keySet().iterator(); it.hasNext();) {
				String key = it.next();
				String[ ] valor = parametros.get(key);
				if (LOG.isDebugEnabled()) {
					LOG.debug("Clave: " + key + " -> Valor: " + valor[0]);
				}

				// para los casos de multiseleccion
				if (key.contains("denTrace")) {
					auditOperationNames = getEnumeratores(AuditOperationName.class, valor);
				} else if (key.contains("denEvent")) {
					auditEventNames = getEnumeratores(AuditEventName.class, valor);
				}

				switch (key) {
					case "draw":
						nextDraw = Integer.valueOf(valor[0]) + 1;
						break;
					case "stateTrace":
						auditOperationStatus = getEnumerator(AuditOperationStatus.class, valor[0]);
						break;
					case "dateFromTrace":
						auditOpStatusFromDate = parseDateRequest(valor[0]);
						break;
					case "dateToTrace":
						auditOpStatusUntilDate = parseDateRequest(valor[0]);
						break;
					case "stateEvent":
						auditOpEventResult = getEnumerator(AuditOpEventResult.class, valor[0]);
						break;
					case "userEvent":
						username = valor[0];
						break;
					case "dateFromEvent":
						auditEventStatusFromDate = parseDateRequest(valor[0]);
						break;
					case "dateToEvent":
						auditEventStatusUntilDate = parseDateRequest(valor[0]);
						break;
					case "start":
						// representa el primer elemento, empezando en 0
						startV = Long.valueOf(valor[0]);
						break;
					case "length":
						// elementos por página
						elementsPerPage = Long.valueOf(valor[0]);
						break;
				}
				// queda el tratamiento de la ordenacion
				if (key.contains("order[") && key.contains("][column]")) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("key:" + key + "order:" + valor[0]);
					}
					// obtenemos la columna cuyo valor será de 0 a 7
					columnOrder = Integer.valueOf(valor[0]);
				}

				if (key.contains("order[") && key.contains("][dir]") && columnOrder != -1) {
					LOG.debug("key:{} order:{}", key, valor[0]);

					// AUDIT_COLUMN_ORDER

					String orden = AUDIT_COLUMN_ORDER[columnOrder] + "_" + valor[0].toUpperCase();

					// tres primeras columnas corresponden a operaciones, el
					// resto a
					// eventos
					if (columnOrder <= 2) {

						AuditOperationOrderByClause claus;

						claus = getEnumerator(AuditOperationOrderByClause.class, orden);

						auditOpOrderByColumns.add(claus);

					} else if (columnOrder < 7) { // INFORMACION ADICIONAL NO
												  // LLEVA
												  // ORDENACION

						AuditEventOrderByClause claus;

						claus = getEnumerator(AuditEventOrderByClause.class, orden);

						auditEventOrderByColumns.add(claus);
					}
				}

			}

			// tenemos resolver los calculos de elementos por paginas y pagina
			pageNumber = (long) Math.round((startV + 1) / elementsPerPage) + 1;

			res = getResponseData(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate, pageNumber, elementsPerPage, auditOpOrderByColumns, auditEventOrderByColumns, nextDraw, formatter, request);

		} catch (WebscanException e) {
			setWebscanExceptionInRequest(e, request, messages);
		} catch (Exception e) {

		}

		return res;

	}

	/**
	 * Parsea una cadena de caracteres que representa el número de milisegundos
	 * transcurridos desde el 01/01/1970, a partir de la cual puede ser obtenida
	 * una fecha.
	 * 
	 * @param date
	 *            cadena de caracteres que representa el número de milisegundos
	 *            transcurridos desde el 01/01/1970.
	 * @return fecha correspondiente al número de milisegundos transcurridos
	 *         desde el 01/01/1970. Si se produce algún error de parseo, o la
	 *         cadena es null, retorna null.
	 */
	private static Date parseDateRequest(String date) {
		Date res = null;

		try {
			if (date != null && !date.isEmpty()) {
				res = new Date(Long.parseLong(date));
			}
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Error parseando la cadena " + date + ", para obtener una fecha.");
			}
		}
		return res;
	}

	/**
	 * Ejecuta la consulta demandada sobre el modelo de datos de auditoría,
	 * parsea el resultado y lo retorna como una cadena en notación JSON.
	 * 
	 * @param auditOperationNames
	 *            filtro sobre las trazas. Nombres de trazas.
	 * @param auditOperationStatus
	 *            filtro sobre el estado de los trazas. Estado de la traza.
	 * @param auditOpStatusFromDate
	 *            filtro sobre la fecha de estado de las trazas. Fecha inferior
	 *            del rango.
	 * @param auditOpStatusUntilDate
	 *            filtro sobre la fecha de estado de las trazas. Fecha superior
	 *            del rango.
	 * @param auditEventNames
	 *            filtro sobre los eventos. Nombres de eventos.
	 * @param username
	 *            filtro sobre los eventos. Usuario de ejecución.
	 * @param auditOpEventResult
	 *            filtro sobre el estado de los eventos. Resultado de la
	 *            ejecución del evento.
	 * @param auditEventStatusFromDate
	 *            filtro sobre la fecha de estado de los eventos. Fecha inferior
	 *            del rango.
	 * @param auditEventStatusUntilDate
	 *            filtro sobre la fecha de estado de los eventos. Fecha superior
	 *            del rango.
	 * @param pageNumber
	 *            número de página de resultados solicitada.
	 * @param elementsPerPage
	 *            número de resultados a mostrar por página.
	 * @param auditOpOrderByColumns
	 *            criterios de ordenación sobre la entidad traza de auditoría.
	 * @param auditEventOrderByColumns
	 *            criterios de ordenación sobre la entidad evento de auditoría.
	 * @param nextDraw
	 *            próxima página de resultados.
	 * @param formatter
	 *            formateador de fechas.
	 * @param request
	 *            petición web.
	 * @return cadena de caracteres en notación JSON que representa el contenido
	 *         de la respuesta.
	 * @throws WebscanException
	 *             Si se produce algun error al ejecutar la consulta sobre BBDD,
	 *             o al parsear los resultados.
	 */
	private String getResponseData(List<AuditOperationName> auditOperationNames, AuditOperationStatus auditOperationStatus, Date auditOpStatusFromDate, Date auditOpStatusUntilDate, List<AuditEventName> auditEventNames, String username, AuditOpEventResult auditOpEventResult, Date auditEventStatusFromDate, Date auditEventStatusUntilDate, Long pageNumber, Long elementsPerPage, List<EntityOrderByClause> auditOpOrderByColumns, List<EntityOrderByClause> auditEventOrderByColumns, int nextDraw, SimpleDateFormat formatter, HttpServletRequest request) throws WebscanException {
		List<AuditOperationVO> listResponse = new ArrayList<AuditOperationVO>();
		List<AuditOperationVO> listResponseAux = new ArrayList<AuditOperationVO>();
		HttpSession session;
		Long recordsTotal = Long.valueOf(0);
		Long recordsFiltered = Long.valueOf(0);
		String excMsg;
		String res = "";

		try {
			listResponse = auditConsoleService.listAndQueryAuditOperations(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate, pageNumber, elementsPerPage, auditOpOrderByColumns, auditEventOrderByColumns);
			// para el filtro actual
			recordsFiltered = auditConsoleService.countAuditOperations(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate);
			// totales
			recordsTotal = auditConsoleService.countAuditOperations();

			res = transformAuditResultToString(listResponse, recordsTotal, recordsFiltered, nextDraw, formatter, request);

			listResponseAux = auditConsoleService.listAndQueryAuditOperations(auditOperationNames, auditOperationStatus, auditOpStatusFromDate, auditOpStatusUntilDate, auditEventNames, username, auditOpEventResult, auditEventStatusFromDate, auditEventStatusUntilDate, null, null, auditOpOrderByColumns, auditEventOrderByColumns);

			// obtenemos user para guardar la informacion en session
			session = request.getSession(false);

			// Actualizar ultima informacion solicitada de auditoria
			session.setAttribute(WebscanBaseConstants.LIST_OP_EVENT_SESSION_ATT_NAME, listResponseAux);

		} catch (DAOException e) {
			excMsg = "Se produjo un error al obtener las trazas de auditoría de base de datos. Mensaje de error: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] DAOError:", excMsg);
			throw new WebscanException(WebscanException.CODE_994, excMsg, e);
		} catch (WebscanException e) {
			throw e;
		} catch (Exception e) {
			excMsg = "Se produjo un error al filtrar y parsear las trazas de auditoría de base de datos. Mensaje de error: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] DAOException: ", excMsg);
			throw new WebscanException(WebscanException.CODE_999, excMsg);

		}

		return res;
	}

	/**
	 * Transforma el resultado de una consulta de trazas de auditoría en una
	 * cadena de caracteres en notación JSON.
	 * 
	 * @param listResponse
	 *            relación de trazas de auditoría incluidas en la respuesta.
	 * @param recordsTotal
	 *            número total de trazas de auditoría registradas en el sistema.
	 * @param recordsFiltered
	 *            número de trazas de auditoría recuperadas de la consulta.
	 * @param nextDraw
	 *            próxima página.
	 * @param formatter
	 *            utilidad de parseo de fechas.
	 * @param request
	 *            petición web.
	 * @return cadena de caracteres en notación JSON que representa el contenido
	 *         de la respuesta.
	 * @throws WebscanException
	 *             si se produce algún error.
	 */
	private String transformAuditResultToString(List<AuditOperationVO> listResponse, Long recordsTotal, Long recordsFiltered, int nextDraw, SimpleDateFormat formatter, HttpServletRequest request) throws WebscanException {
		String res = "";
		int registros = 0;
		int op = 0;
		List<AuditEventDTO> eventos;
		Locale locale = request.getLocale();
		StringBuilder datos = new StringBuilder();
		StringBuilder response = new StringBuilder();
		String excMsg;
		String additionalInfo;
		StringBuilder aux = new StringBuilder();

		try {

			// preparamos los datos
			for (AuditOperationVO operacion: listResponse) {
				op++;
				aux = new StringBuilder();
				aux.append("['");
				aux.append(messages.getMessage(operacion.getName().name(), null, locale));
				aux.append("','");
				aux.append(messages.getMessage(operacion.getStatus().name(), null, locale));
				aux.append("','");
				aux.append(formatter.format(operacion.getLastUpdateDate()));
				aux.append("','");
				aux.append(operacion.getId());
				aux.append("','");
				// obtenemos los eventos
				eventos = operacion.getListAuditEvent();
				for (AuditEventDTO evento: eventos) {

					datos.append(aux.toString());
					// datos.append(evento.getAuditOperationId());
					// datos.append("','");
					datos.append(messages.getMessage(evento.getEventName().name(), null, locale));
					datos.append("','");
					datos.append(evento.getEventUser());
					datos.append("','");
					datos.append(formatter.format(evento.getEventDate()));
					datos.append("','");
					datos.append(messages.getMessage(evento.getEventResult().name(), null, locale));
					datos.append("','");
					additionalInfo = evento.getAdditionalInfo();
					if (additionalInfo == null || additionalInfo.isEmpty()) {
						additionalInfo = messages.getMessage("audit.fields.additonalInfo.empty", null, locale);
					}
					datos.append(prepareAdditionalInfo(additionalInfo));

					datos.append("']");
					registros++;
					if (eventos.size() > registros) {
						datos.append(",");
					}

				}

				registros = 0;
				if (listResponse.size() != op) {
					datos.append(",");
				}
			}
			LOG.debug("Total registros informados: ", registros);
			// preparamos la respuesta
			response.append("{");
			response.append("'draw':" + nextDraw + ",");
			response.append("'recordsTotal':" + recordsTotal + ",");
			response.append("'recordsFiltered':" + recordsFiltered + ",");
			response.append("'data':");
			response.append("["); // inicio registro
			response.append(datos.toString());
			response.append("]"); // fin registros
			response.append("}");

			res = response.toString().replaceAll("\'", "\"");

		} catch (Exception e) {
			excMsg = "Se produjo un error al parsear a JSON las trazas de auditoría recuperadas de base de datos. Mensaje de error: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] ", excMsg);
			throw new WebscanException(WebscanException.CODE_999, excMsg);
		}
		return res;
	}

	/**
	 * Prepara la información para ser mostrada en la vista, ya que puede
	 * contener elementos que dan error al ser tratado en javascript.
	 * 
	 * @param entrada
	 *            Cadena a tratar para poder ser modificada para visualizacion
	 *            en la vista
	 * @return Cadena resultante lista para ser vista
	 */

	private String prepareAdditionalInfo(String entrada) {
		String res = null;
		res = entrada.replaceAll("\\[", "-");
		res = res.replaceAll("]", "-");
		res = res.replaceAll("'", "-");
		return res;
	}

	/**
	 * obtiene el map de la lista de operaciones de auditorias y sus eventos.
	 * 
	 * @param lsitAuditOp
	 *            Listado de las operaciones de auditoria y sus eventos
	 *            asociados
	 * @return Map con la informacion asociada por operaciones y eventos
	 */

	private Map<AuditOperationVO, List<AuditEventDTO>> getMapAuditOperationToListOperation(List<AuditOperationVO> lsitAuditOp) {
		Map<AuditOperationVO, List<AuditEventDTO>> auditLogs = new HashMap<AuditOperationVO, List<AuditEventDTO>>();

		if (lsitAuditOp != null) {
			// creamos el map
			for (Iterator<AuditOperationVO> it = lsitAuditOp.iterator(); it.hasNext();) {
				AuditOperationVO auditOp = it.next();

				/*AuditOperationDTO aux = new AuditOperationDTO();
				aux.setAuditScannedDocId(auditOp.getAuditScannedDocId());
				aux.setLastUpdateDate(auditOp.getLastUpdateDate());
				aux.setName(auditOp.getName());
				aux.setStartDate(auditOp.getStartDate());
				aux.setStatus(auditOp.getStatus());
				*/
				auditLogs.put(auditOp, auditOp.getListAuditEvent());
			}
		}
		return auditLogs;
	}

	/**
	 * Controlador que habilita la generación de informes y listados de trazas
	 * de auditoría.
	 * 
	 * @param fileName
	 *            nombre del archivo que contiene el informe.
	 * @param fileType
	 *            tipo de formato del informe (CSV, Excel o PDF).
	 * @param request
	 *            Petición web.
	 * @param response
	 *            Respuesta web.
	 */
	@RequestMapping(value = "/audit/getReportFile", method = RequestMethod.POST)
	public void getFile(@RequestParam("file_name") String fileName, @RequestParam("file_tipo") String fileType, HttpServletRequest request, HttpServletResponse response) {
		byte[ ] reportContent;
		HttpSession session;
		List<AuditOperationVO> listOperation = new ArrayList<AuditOperationVO>();
		try {

			// obtenemos la informacion de session de la ultima lista filtrada
			session = request.getSession(false);
			// Inicializamos el atributo de session
			listOperation = (List<AuditOperationVO>) session.getAttribute(WebscanBaseConstants.LIST_OP_EVENT_SESSION_ATT_NAME);

			// Se genera el documento que incluye el informe
			OutputStream report = auditConsoleService.generateAuditLogReport(listOperation, fileType, request.getLocale());
			reportContent = ((ByteArrayOutputStream) report).toByteArray();

			downloadReport(reportContent, fileName, fileType, response);

		} catch (WebscanException e) {
			setWebscanExceptionInRequest(e, request, messages);
		} catch (IOException e) {
			LOG.error("[WEBSCAN-AUDIT] Se produjo un error en la descarga del informe de trazas de auditoría. Mensaje: ", e.getMessage(), e);
			BaseControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.TRUE);
		} catch (Exception e) {
			LOG.error("[WEBSCAN-AUDIT] Se produjo un error en la descarga del informe de trazas de auditoría. Mensaje: ", e.getMessage(), e);
			BaseControllerUtils.setDefaultExceptionInRequest(e, request, messages, Boolean.TRUE);
		}

	}

	/**
	 * Funcion para enviar en el response el fichero de descarga del tipo
	 * seleccionado y nombre.
	 * 
	 * @param content
	 *            byte[] con el contenido de la información a enviar para
	 * @param fileName
	 *            nombre del fichero
	 * @param fileType
	 *            tipo del fichero a enviar
	 * @param response
	 *            objecto que corresponde al response
	 * @throws IOException
	 *             Excepciónn en el tratamiento para crear el fichero
	 */

	private void downloadReport(byte[ ] content, String fileName, String fileType, HttpServletResponse response) throws IOException {

		// Se establecen las cabeceras de la respuesta
		response.setContentType("application/force-download");
		response.setHeader("Content-Transfer-Encoding", "binary");

		// Se establecen las cabeceras de la respuesta
		if (fileType.equals("pdf")) {
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + fileName + ".pdf\""));
			response.setContentType(MimeType.PDF_CONTENT.getType());
		} else if (fileType.equals("excel")) {
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + fileName + ".xlsx\""));
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		} else if (fileType.equals("csv")) {
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + fileName + ".csv\""));
			response.setContentType("text/plain");
		}

		response.setContentLength(content.length);
		// Se incorpora el informe a la respuesta
		FileCopyUtils.copy(content, response.getOutputStream());

		response.flushBuffer();
	}

	/**
	 * Obtiene y establece como atributo de una petición un mensaje de error
	 * asociado a una excepción WebscanException.
	 * 
	 * @param e
	 *            Excepción WebscanException
	 * @param request
	 *            Petición web.
	 * @param messageSource
	 *            Clase que agrupa los mensajes paramterizados en el sistema.
	 */
	private static void setWebscanExceptionInRequest(WebscanException e, HttpServletRequest request, MessageSource messageSource) {
		String errorPropName;
		String[ ] args = new String[1];

		errorPropName = WebscanBaseConstants.DEFAULT_ERROR_MESSAGE;

		if (e.getCode().equals(WebscanException.CODE_994)) {
			errorPropName = "audit.error.bbdd";
			args[0] = e.getCause().getMessage();
		} else if (e.getCode().equals(WebscanException.CODE_995)) {
			errorPropName = "audit.error.report.notSupportedFormat";
		} else if (e.getCode().equals(WebscanException.CODE_996)) {
			errorPropName = "audit.error.internal";
			args[0] = e.getCause().getMessage();
		} else {
			args[0] = e.getMessage();
		}

		BaseControllerUtils.addCommonMessage(errorPropName, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
	}

	/**
	 * Obtiene y establece como atributo de una petición un mensaje de error
	 * asociado a una excepción DAOException.
	 * 
	 * @param e
	 *            Excepción DAOException.
	 * @param request
	 *            Petición web.
	 * @param messageSource
	 *            Clase que agrupa los mensajes paramterizados en el sistema.
	 */
	public static void setDAOExceptionInRequest(DAOException e, HttpServletRequest request, MessageSource messageSource) {
		String errorPropName;
		String[ ] args = new String[1];

		errorPropName = WebscanBaseConstants.DEFAULT_ERROR_MESSAGE;
		args[0] = e.getMessage();
		BaseControllerUtils.addCommonMessage(errorPropName, args, ViewMessageLevel.ERROR, WebScope.REQUEST, request);
	}

	/**
	 * Recupera los datos de la ultima consulta realizada de auditoria.
	 * 
	 * @param request
	 *            Petición web.
	 * @return listado de las operaciones y eventos.
	 */

	public static AuditOperationVO getDataAuditoria(HttpServletRequest request) {
		AuditOperationVO res;

		// obtenemos la info para auditoria
		res = (AuditOperationVO) request.getSession(false).getAttribute(WebscanBaseConstants.LIST_OP_EVENT_SESSION_ATT_NAME);

		return res;

	}

}
