/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.auditconsole.reports.CSVAuditReport.java.</p>
* <b>Descripción:</b><p> Clase que imlementa la generación de informes de la utilidad de auditoría en
* formato CSV.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.auditconsole.reports;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.audit.console.controller.vo.AuditOperationVO;
import es.ricoh.webscan.model.dto.AuditEventDTO;
import es.ricoh.webscan.utilities.Utils;

/**
 * Clase que imlementa la generación de informes de la utilidad de auditoría en
 * formato CSV.
 * <p>
 * Clase CSVAuditReport.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @see es.ricoh.webscan.auditconsole.reports.AuditReports
 * @version 2.0.
 */
public class CSVAuditReport extends AuditReports {

	/**
	 * Objeto empleado para el registro de trazas de ejecución.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(XLSXAuditReport.class);

	/**
	 * Juego de caracteres configurado en la generación del informe.
	 */
	public static final String CODEPAGE = "UTF8";

	/**
	 * Clase de utilidades que habilita el acceso a los parámetros de
	 * configuración del sistema.
	 */
	private Utils webscanUtilParams;

	/**
	 * Objeto que recopila los mensajes, sujetos a multidioma, que son
	 * presentados al usuario.
	 */
	private MessageSource messages;

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.auditconsole.reports.AuditReports#createReport(java.util.Map,
	 *      java.util.Locale)
	 */
	@Override
	public OutputStream createReport(final List<AuditOperationVO> operations, Locale locale) throws WebscanException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String excMsg;

		Properties style = loadStyleSheet(webscanUtilParams);

		/**
		 * TITLE_OPERATION_NAME,TITLE_OPERATION_STARTDATE,TITLE_OPERATION_STATUS,TITLE_EVENT_NAME,TITLE_EVENT_STARTDATE,TITLE_EVENT_RESULT
		 */
		try {
			baos.write((messages.getMessage(TITLE_OPERATION_NAME, null, locale) + "," + messages.getMessage(TITLE_OPERATION_STARTDATE, null, locale) + "," + messages.getMessage(TITLE_OPERATION_STATUS, null, locale) + "," + messages.getMessage(TITLE_EVENT_NAME, null, locale) + "," + messages.getMessage(TITLE_EVENT_USER, null, locale) + "," + messages.getMessage(TITLE_EVENT_STARTDATE, null, locale) + "," + messages.getMessage(TITLE_EVENT_RESULT, null, locale)).getBytes(style.getProperty(PROP_REPORT_CODE_PAGE)));
			baos.write(" \r\n".getBytes());

			for (AuditOperationVO op: operations) {
				AuditOperationVO key = op;
				List<AuditEventDTO> events = op.getListAuditEvent();

				for (Iterator<AuditEventDTO> eventIt = events.iterator(); eventIt.hasNext();) {
					AuditEventDTO event = eventIt.next();
					writeLine(baos, key, event, style.getProperty(PROP_REPORT_CODE_PAGE), locale);
				}
			}

		} catch (IOException e) {
			excMsg = "Se produjo un error al generar un informe de auditoría en formato CSV: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] IOException:", excMsg);
			throw new WebscanException(WebscanException.CODE_996, excMsg, e);
		} catch (NoSuchMessageException e) {
			excMsg = "Se produjo un error al generar un informe de auditoría en formato CSV: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] NoSuchMessageException:", excMsg);
			throw new WebscanException(WebscanException.CODE_996, excMsg, e);
		} catch (Exception e) {
			excMsg = "Se produjo un error al generar un informe de auditoría en formato CSV: " + e.getMessage();
			LOG.error("[WEBSCAN-AUDIT] Exception", excMsg);
			throw new WebscanException(WebscanException.CODE_996, excMsg, e);
		}

		return baos;
	}

	/**
	 * Escribe una fila del informe.
	 * 
	 * @param os
	 *            stream de salida.
	 * @param op
	 *            Traza de auditoría.
	 * @param event
	 *            Evento de auditoría.
	 * @param codePage
	 *            Code Page configurado en el properties
	 * @param locale
	 *            Identificador del language de la petición http.
	 */
	private void writeLine(OutputStream os, final AuditOperationVO op, final AuditEventDTO event, String codePage, Locale locale) {
		try {

			os.write((messages.getMessage(op.getName().toString(), null, locale) + ",").getBytes(codePage));
			os.write((webscanUtilParams.formatDateTime(op.getStartDate()) + ",").getBytes(codePage));
			os.write((messages.getMessage(op.getStatus().toString(), null, locale) + ",").getBytes(codePage));

			os.write((messages.getMessage(event.getEventName().toString(), null, locale) + ",").getBytes(codePage));
			os.write((event.getEventUser().toString() + ",").getBytes(codePage));
			os.write((webscanUtilParams.formatDateTime(event.getEventDate()) + ",").getBytes(codePage));
			os.write((messages.getMessage(event.getEventResult().name(), null, locale)).getBytes(codePage));
			os.write(" \r\n".getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see es.ricoh.webscan.auditconsole.reports.AuditReports#getMimeType()
	 */
	@Override
	public String getMimeType() {
		return "text/csv";
	}

	/**
	 * Establece la clase de utilidades que habilita el acceso a los parámetros
	 * de configuración del sistema.
	 * 
	 * @param aWebscanUtilParams
	 *            clase de utilidades que habilita el acceso a los parámetros de
	 *            configuración del sistema.
	 */
	public void setWebscanUtilParams(Utils aWebscanUtilParams) {
		this.webscanUtilParams = aWebscanUtilParams;
	}

	/**
	 * Establece el objeto que recopila los mensajes, sujetos a multidioma, que
	 * son presentados al usuario.
	 * 
	 * @param anyMessages
	 *            Objeto que recopila los mensajes, sujetos a multidioma, que
	 *            son presentados al usuario.
	 */
	public void setMessages(MessageSource anyMessages) {
		this.messages = anyMessages;
	}

}
