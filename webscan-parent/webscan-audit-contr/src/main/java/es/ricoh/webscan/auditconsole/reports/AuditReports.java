/*-
 * #%L
 * Webscan 2 - Solución Base de Digitalización Certificada y Copia Auténtica
 * %%
 * Copyright (C) 2017 Ricoh Spain IT Services
 * %%
 * Este fichero forma parte de la solución base de digitalización certificada y 
 * copia auténtica, Webscan 2. 
 * 
 * Este fichero se distribuye bajo licencia propia Webscan 2, estando las
 * condiciones recogidas en el fichero 'LICENSE.txt' que se acompaña. Si se distribuyera este 
 * fichero individualmente, deben incluirse aquí las condiciones expresadas allí.
 * #L%
 */

/** 
* <b>Archivo:</b><p>es.ricoh.webscan.auditconsole.reports.AuditReports.java.</p>
* <b>Descripción:</b><p> Clase abstracta base de la implementación de las utilidades de generación de
* informes de auditorías en diferentes formatos desde la consola de auditoría
* del Sistema.</p>
* <b>Proyecto:</b><p>Webscan - Plataforma de digitalización certificada y copia auténtica.</p>
* @author RICOH Spain IT Services.
* @version 2.0.
*/
package es.ricoh.webscan.auditconsole.reports;

import java.awt.Color;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import es.ricoh.webscan.WebscanException;
import es.ricoh.webscan.audit.console.controller.vo.AuditOperationVO;
import es.ricoh.webscan.utilities.Utils;

/**
 * Clase abstracta base de la implementación de las utilidades de generación de
 * informes de auditorías en diferentes formatos desde la consola de auditoría
 * del Sistema.
 * <p>
 * Clase AuditReports.
 * </p>
 * <b>Proyecto:</b>
 * <p>
 * Webscan - Plataforma de digitalización certificada y copia auténtica.
 * </p>
 * 
 * @version 2.0.
 */
public abstract class AuditReports {

	/**
	 * Texto de la columna operación.
	 */
	protected static final String TITLE_OPERATION_NAME = "audit.report.table.columns.operation.title";

	/**
	 * Texto de la columna fecha de inicio.
	 */
	protected static final String TITLE_OPERATION_STARTDATE = "audit.report.table.columns.startDate.title";

	/**
	 * Texto de la columna estado.
	 */
	protected static final String TITLE_OPERATION_STATUS = "audit.report.table.columns.status.title";

	/**
	 * Texto de la columna eventos.
	 */
	protected static final String TITLE_EVENT_NAME = "audit.report.table.columns.event.title";

	/**
	 * Texto de la columna usuario.
	 */
	protected static final String TITLE_EVENT_USER = "audit.report.table.columns.user.title";

	/**
	 * Texto de la columna fecha evento.
	 */
	// protected static final String TITLE_EVENT_STARTDATE = "Fecha de evento";
	protected static final String TITLE_EVENT_STARTDATE = "audit.report.table.columns.eventDate.title";

	/**
	 * Texto de la columna resultado.
	 */
	// protected static final String TITLE_EVENT_RESULT = "Resultado";
	protected static final String TITLE_EVENT_RESULT = "audit.report.table.columns.result.title";

	/**
	 * Parámetro de configuración que especifica el tipo de fuente configurada
	 * para la sección título de informes y listados (por ejemplo, Arial).
	 */
	protected static final String PROP_STYLE_TITLE_FONT = "reports.style.title.font";

	/**
	 * Parámetro de configuración que especifica el tamaño de la fuente
	 * configurada para la sección título de informes y listados (por ejemplo,
	 * 12).
	 */
	protected static final String PROP_STYLE_TITLE_FONTSIZE = "reports.style.title.fontSize";

	/**
	 * Parámetro de configuración que especifica el color de la fuente
	 * configurada para la sección título de informes y listados, en notación
	 * RGB hexadecimal (por ejemplo, #000000).
	 */
	protected static final String PROP_STYLE_TITLE_FOREGROUND = "reports.style.title.foregroundColor";

	/**
	 * Parámetro de configuración que especifica el color de fondo de la sección
	 * título de informes y listados, en notación RGB hexadecimal (por ejemplo,
	 * #444444).
	 */
	protected static final String PROP_STYLE_TITLE_BACKGROUND = "reports.style.title.backgroundColor";

	/**
	 * Parámetro de configuración que especifica el efecto aplicado a la fuente
	 * configurada para la sección título de informes y listados (por ejemplo,
	 * bold).
	 */
	protected static final String PROP_STYLE_TITLE_FONTFACE = "reports.style.title.fontFace";

	/**
	 * Parámetro de configuración que especifica como es alineado la sección
	 * título de informes y listados (por ejemplo, center).
	 */
	protected static final String PROP_STYLE_TITLE_ALIGNMENT = "reports.style.title.alignment";

	/**
	 * Parámetro de configuración que especifica la fuente configurada para la
	 * sección cuerpo de informes y listados (por ejemplo, Arial).
	 */
	protected static final String PROP_STYLE_ROWS_FONT = "reports.style.rows.font";

	/**
	 * Parámetro de configuración que especifica el tamaño de la fuente
	 * configurada para la sección cuerpo de informes y listados (por ejemplo,
	 * 12).
	 */
	protected static final String PROP_STYLE_ROWS_FONTSIZE = "reports.style.rows.fontSize";

	/**
	 * Parámetro de configuración que especifica el color de la fuente
	 * configurada para las filas impares de la tabla incluida en la sección
	 * cuerpo de informes y listados, en notación RGB hexadecimal (por ejemplo,
	 * #000000).
	 */
	protected static final String PROP_STYLE_ROWS_FOREGROUND1 = "reports.style.rows.foregroundColor1";

	/**
	 * Parámetro de configuración que especifica el color de fondo de las filas
	 * impares de la tabla incluida en la sección cuerpo de informes y listados,
	 * en notación RGB hexadecimal (por ejemplo, #444444).
	 */
	protected static final String PROP_STYLE_ROWS_BACKGROUND1 = "reports.style.rows.backgroundColor1";

	/**
	 * Parámetro de configuración que especifica el color de la fuente
	 * configurada para las filas pares de la tabla incluida en la sección
	 * cuerpo de informes y listados, en notación RGB hexadecimal (por ejemplo,
	 * #000000).
	 */
	protected static final String PROP_STYLE_ROWS_FOREGROUND2 = "reports.style.rows.foregroundColor2";

	/**
	 * Parámetro de configuración que especifica el color de fondo de las filas
	 * pares de la tabla incluida en la sección cuerpo de informes y listados,
	 * en notación RGB hexadecimal (por ejemplo, #444444).
	 */
	protected static final String PROP_STYLE_ROWS_BACKGROUND2 = "reports.style.rows.backgroundColor2";

	/**
	 * Parámetro de configuración que especifica como es alineado la sección
	 * cuerpo de informes y listados (por ejemplo, center).
	 */
	protected static final String PROP_STYLE_ROWS_ALIGNMENT = "reports.style.rows.alignment";

	/**
	 * Parámetro de configuración que especifica el efecto aplicado a la fuente
	 * configurada para la sección cuerpo de informes y listados (por ejemplo,
	 * plain).
	 */
	protected static final String PROP_STYLE_ROWS_FONTFACE = "reports.style.rows.fontFace";

	/**
	 * Parámetro de configuración que especifica el code page que vamos a
	 * generar la informacion de los reportes.
	 */
	protected static final String PROP_REPORT_CODE_PAGE = "reports.code.page";

	/**
	 * Genera un informe de auditoría en un determinado formato (CSV, Excel o
	 * PDF).
	 * 
	 * @param operations
	 *            Trazas y eventos de auditoría incluidos en el informe.
	 * @param locale
	 *            Idioma empleado en la generación del informe.
	 * @return informe de auditoría en un determinado formato (CSV, Excel o
	 *         PDF).
	 * @throws WebscanException
	 *             si se produce algún error en la generación del informe.
	 */
	public abstract OutputStream createReport(List<AuditOperationVO> operations, Locale locale) throws WebscanException;

	/**
	 * Obtiene la configuración establecida en el Sistema para la generación de
	 * los informes de auditoría.
	 * 
	 * @param webscanUtilParams
	 *            clase de utilidades que habilita el acceso a los parámetros de
	 *            configuración del sistema.
	 * @return la configuración establecida en el Sistema para la generación de
	 *         los informes de auditoría.
	 */
	protected Properties loadStyleSheet(Utils webscanUtilParams) {
		Properties style = new Properties();

		style.setProperty(PROP_STYLE_TITLE_FONT, webscanUtilParams.getProperty(PROP_STYLE_TITLE_FONT, "Arial"));
		style.setProperty(PROP_STYLE_TITLE_FONTSIZE, webscanUtilParams.getProperty(PROP_STYLE_TITLE_FONTSIZE, "12"));
		style.setProperty(PROP_STYLE_TITLE_FOREGROUND, webscanUtilParams.getProperty(PROP_STYLE_TITLE_FOREGROUND, "#000000"));
		style.setProperty(PROP_STYLE_TITLE_BACKGROUND, webscanUtilParams.getProperty(PROP_STYLE_TITLE_BACKGROUND, "#888888"));
		style.setProperty(PROP_STYLE_TITLE_FONTFACE, webscanUtilParams.getProperty(PROP_STYLE_TITLE_FONTFACE, "bold"));
		style.setProperty(PROP_STYLE_TITLE_ALIGNMENT, webscanUtilParams.getProperty(PROP_STYLE_TITLE_ALIGNMENT, "center"));

		style.setProperty(PROP_STYLE_ROWS_FONT, webscanUtilParams.getProperty(PROP_STYLE_ROWS_FONT, "Arial"));
		style.setProperty(PROP_STYLE_ROWS_FONTSIZE, webscanUtilParams.getProperty(PROP_STYLE_ROWS_FONTSIZE, "10"));
		style.setProperty(PROP_STYLE_ROWS_FOREGROUND1, webscanUtilParams.getProperty(PROP_STYLE_ROWS_FOREGROUND1, "#000000"));
		style.setProperty(PROP_STYLE_ROWS_BACKGROUND1, webscanUtilParams.getProperty(PROP_STYLE_ROWS_BACKGROUND1, "#ffffff"));
		style.setProperty(PROP_STYLE_ROWS_FOREGROUND2, webscanUtilParams.getProperty(PROP_STYLE_ROWS_FOREGROUND2, style.getProperty(PROP_STYLE_ROWS_FOREGROUND1)));
		style.setProperty(PROP_STYLE_ROWS_BACKGROUND2, webscanUtilParams.getProperty(PROP_STYLE_ROWS_BACKGROUND2, style.getProperty(PROP_STYLE_ROWS_BACKGROUND1)));
		style.setProperty(PROP_STYLE_ROWS_FONTFACE, webscanUtilParams.getProperty(PROP_STYLE_ROWS_FONTFACE, "normal"));
		style.setProperty(PROP_STYLE_ROWS_ALIGNMENT, webscanUtilParams.getProperty(PROP_STYLE_ROWS_ALIGNMENT, "center"));

		style.setProperty(PROP_REPORT_CODE_PAGE, webscanUtilParams.getProperty(PROP_REPORT_CODE_PAGE, "ISO-8859-15"));

		return style;
	}

	/**
	 * Convierte una cadena de caracteres, que representa un color en notación
	 * hexadecimal, en un objeto Color en notación RGB.
	 * 
	 * @param hexColor
	 *            cadena de caracteres que representa un color en notación
	 *            hexadecimal.
	 * @return objeto Color en notación RGB.
	 */
	protected static Color hex2Rgb(String hexColor) {
		return new Color(Integer.valueOf(hexColor.substring(1, 3), 16), Integer.valueOf(hexColor.substring(3, 5), 16), Integer.valueOf(hexColor.substring(5, 7), 16));
	}

	/**
	 * Obtiene el tipo MIME del informe generado.
	 * 
	 * @return el tipo MIME del informe generado.
	 */
	public abstract String getMimeType();

}
