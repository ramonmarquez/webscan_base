-- **********************************************************************
-- ************************ CREATE DATA MODEL ***************************
-- **********************************************************************

-- ******************** DIGITALIZATION DATA MODEL ***********************

-- TABLES
--  Proceso de digitalización (Lotes).
CREATE TABLE BATCH
  (
    --  Identificador del proceso.
    ID NUMBER (32) NOT NULL ,
	--  Identificador normalizado del proceso de digitalización (UUID).
	BATCH_NAME_ID VARCHAR2(128 CHAR) NOT NULL ,
	-- 	Fecha de inicio del proceso.
	START_DATE DATE,
	--  Fecha de modificación del proceso.
    UPDATE_DATE DATE,
	--  Usuario que inicia el proceso de digitalización del documento.
    USERNAME VARCHAR2 (128 CHAR) NOT NULL ,
	-- Unidades orgánicas a las que pertenece el usuario que inicia el proceso de digitalización.
	USER_DPTOS VARCHAR2 (512 CHAR),
	--  Identificador de la  definición de documento configurada en el perfil de
    --  digitalización y unidad organizativa que especifica la composición del
    --  documento.
    SPROF_ORG_DSPEC_ID NUMBER (32) NOT NULL 

  )
	
COMMENT ON TABLE BATCH
IS
  'Proceso de digitalización.' ;
  COMMENT ON COLUMN BATCH.ID
IS
  'Identificador del Proceso de digitalización.' ;
  COMMENT ON COLUMN BATCH.BATCH_NAME_ID
IS
  'Identificador normalizado del proceso de digitalización (UUID).' ;
  COMMENT ON COLUMN BATCH.START_DATE
IS
  'Fecha de inicio del proceso de digitalización.';
  COMMENT ON COLUMN BATCH.UPDATE_DATE
IS
  'Fecha de última modificación del proceso de digitalización.';
    COMMENT ON COLUMN BATCH.USERNAME
IS
  'Usuarios que han digitalizado algún documento en el proceso.';
    COMMENT ON COLUMN BATCH.USER_DPTOS
IS
  'Unidades orgánicas a las que pertenece el usuario que inicia el proceso de digitalización.';
    COMMENT ON COLUMN SCANNED_DOC.SPROF_ORG_DSPEC_ID
IS
  'Identificador de la  definición de documento configurada en el perfil de digitalización y unidad organizativa que especifica la composición del documento.' ;
ALTER TABLE BATCH ADD CONSTRAINT BATCH_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Fase del proceso de digitalización
CREATE TABLE DIGIT_PROC_STAGE
  (
    --  Identificador de fase del proceso de digitalización.
    ID NUMBER (32) NOT NULL ,
    --  Denominación de la fase.
    NAME VARCHAR2 (50 CHAR) NOT NULL ,
    --  Descripción de la fase.
    DESCRIPTION VARCHAR2 (200 CHAR)
  ) ;
COMMENT ON TABLE DIGIT_PROC_STAGE
IS
  'Fase del proceso de digitalización' ;
  COMMENT ON COLUMN DIGIT_PROC_STAGE.ID
IS
  'Identificador de fase del proceso de digitalización.' ;
  COMMENT ON COLUMN DIGIT_PROC_STAGE.NAME
IS
  'Denominación de la fase.' ;
  COMMENT ON COLUMN DIGIT_PROC_STAGE.DESCRIPTION
IS
  'Descripción de la fase.' ;
  ALTER TABLE DIGIT_PROC_STAGE ADD CONSTRAINT DIGIT_PROC_STAGE_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE DIGIT_PROC_STAGE ADD CONSTRAINT DIGIT_PROC_STAGE_UN UNIQUE ( NAME ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Metadatos de un documento escaneado.
CREATE TABLE DOC_METADATA
  (
    --  Identificador de un metadato de un documento escaneado.
    ID NUMBER (32) NOT NULL ,
    --  Valor de un metadato de un documento escaneado.
    VALUE VARCHAR2 (1024 CHAR) ,
    --  Especificación de un metadato de un documento escaneado.
    METADATA_SPEC_ID NUMBER (32) NOT NULL ,
    --  Documento escaneado al cual pertenece el metadato.
    SCANNED_DOC_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE DOC_METADATA
IS
  'Metadatos de un documento escaneado.' ;
  COMMENT ON COLUMN DOC_METADATA.ID
IS
  'Identificador de un metadato de un documento escaneado.' ;
  COMMENT ON COLUMN DOC_METADATA.VALUE
IS
  'Valor de un metadato de un documento escaneado.' ;
  COMMENT ON COLUMN DOC_METADATA.METADATA_SPEC_ID
IS
  'Especificación de un metadato de un documento escaneado.' ;
  COMMENT ON COLUMN DOC_METADATA.SCANNED_DOC_ID
IS
  'Documento escaneado al cual pertenece el metadato.' ;
  ALTER TABLE DOC_METADATA ADD CONSTRAINT DOC_METADATA_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE DOC_METADATA ADD CONSTRAINT DOC_METADATA_UN UNIQUE ( METADATA_SPEC_ID , SCANNED_DOC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Especificación de un tipo de documento.
CREATE TABLE DOC_SPEC
  (
    --  Identificador de una especificación de tipo de documento.
    ID   NUMBER (32) NOT NULL ,
    NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Descripción de un tipo de documentos.
    DESCRIPTION VARCHAR2 (256 CHAR) ,
    --  Indica si un tipo de documentos está activo.
    ACTIVE CHAR (1) DEFAULT '1' NOT NULL
  ) ;
COMMENT ON TABLE DOC_SPEC
IS
  'Especificación de un tipo de documento.' ;
  COMMENT ON COLUMN DOC_SPEC.ID
IS
  'Identificador de una especificación de tipo de documento.' ;
  COMMENT ON COLUMN DOC_SPEC.NAME
IS
  'Nombre de una especificación de tipo de documento.' ;
  COMMENT ON COLUMN DOC_SPEC.DESCRIPTION
IS
  'Descripción de un tipo de documentos.' ;
  COMMENT ON COLUMN DOC_SPEC.ACTIVE
IS
  'Indica si un tipo de documentos está activo.' ;
  ALTER TABLE DOC_SPEC ADD CONSTRAINT DOC_TYPE_SPEC_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE DOC_SPEC ADD CONSTRAINT DOC_TYPE_SPEC_NAME_UN UNIQUE ( NAME ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Relación de especificaciones de plugins que implementan diferentes fases
--  del proceso de digitalización.
CREATE TABLE DPROC_STAGE_PLSPEC
  (
    --  Fase de procesos de digitalización.
    DPROC_STAGE_ID NUMBER (32) NOT NULL ,
    --  Especificación de plugins.
    PLUGIN_SPEC_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE DPROC_STAGE_PLSPEC
IS
  'Relación de especificaciones de plugins que implementan diferentes fases del proceso de digitalización.' ;
  COMMENT ON COLUMN DPROC_STAGE_PLSPEC.DPROC_STAGE_ID
IS
  'Fase de procesos de digitalización.' ;
  COMMENT ON COLUMN DPROC_STAGE_PLSPEC.PLUGIN_SPEC_ID
IS
  'Especificación de plugins.' ;
  ALTER TABLE DPROC_STAGE_PLSPEC ADD CONSTRAINT DPROC_STAGE_PLSPEC_PK PRIMARY KEY ( DPROC_STAGE_ID , PLUGIN_SPEC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  
--  Relación de herencia de especificaciones de tipos de documentos.
CREATE TABLE INHERITED_DOC_TYPE_SPEC
  (
    --  Identificador de la especificación de tipo de documento hijo de la relación
    --  de herencia.
    CHILD_DOC_TYPE_SPEC NUMBER (32) NOT NULL ,
    --  Identificador de la especificación de tipo de documento padre de la
    --  relación de herencia.
    PARENT_DOC_TYPE_SPEC NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE INHERITED_DOC_TYPE_SPEC
IS
  'Relación de herencia de especificaciones de documentos.' ;
  COMMENT ON COLUMN INHERITED_DOC_TYPE_SPEC.CHILD_DOC_TYPE_SPEC
IS
  'Identificador de la especificación de documentos hija de la relación de herencia.' ;
  COMMENT ON COLUMN INHERITED_DOC_TYPE_SPEC.PARENT_DOC_TYPE_SPEC
IS
  'Identificador de la especificación de documentos padre de la relación de herencia.' ;
  ALTER TABLE INHERITED_DOC_TYPE_SPEC ADD CONSTRAINT INHERITED_DOC_TYPE_SPEC_PK PRIMARY KEY ( CHILD_DOC_TYPE_SPEC, PARENT_DOC_TYPE_SPEC ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Especificación de un metadato perteneciente a un tipo documental.
CREATE TABLE METADATA_SPEC
  (
    --  Identificador de la especificación de un metadato perteneciente a un tipo
    --  documental.
    ID NUMBER (32) NOT NULL ,
    --  Nombre de la especificación de un metadato perteneciente a un tipo
    --  documental.
    NAME VARCHAR2 (100 CHAR) NOT NULL ,
    --  Descripción de la especificación de un metadato perteneciente a un tipo
    --  documental.
    DESCRIPTION VARCHAR2 (256 CHAR) ,
	--  Descripción de la especificación de un metadato perteneciente a un tipo
    --  documental.
    DEFAULT_VALUE VARCHAR2 (1024 CHAR) ,
    --  Indica si el metadato debe ser informado obligatoriamente para un documento
    --  digitalizado.
    MANDATORY CHAR (1) DEFAULT '0' NOT NULL ,
	--  Indica si el metadato puede ser editado.
    EDITABLE  CHAR (1) DEFAULT '1' NOT NULL ,
	--  Indica si el metadato puede ser incluido en carátulas separadoras de lotes de documentos.
	INC_IN_BATCH_SEP CHAR (1) DEFAULT '0' ,
	-- Indica si el metadato actúa como identificador de lotes de documentos.
    IS_BATCH_ID      CHAR (1) DEFAULT '0' ,
	--  Conjunto finito de valores que pueden ser asignados a los metadatos 
	--  definidos mediante esta especificación.
	SRC_VALUES BLOB ,
    --  Tipo documental para el cual es definido el metadato.
    DOC_TYPE_SPEC_ID NUMBER (32) NOT NULL
  )
  LOB ("SRC_VALUES") STORE AS BASICFILE "METSPEC_SRCVALUES_LOB_SEG" (TABLESPACE WEBSCAN_LOB_TS ENABLE STORAGE IN ROW CHUNK 4096 RETENTION 
	NOCACHE LOGGING );
	
COMMENT ON TABLE METADATA_SPEC
IS
  'Especificación de un metadato perteneciente a un tipo documental.' ;
  COMMENT ON COLUMN METADATA_SPEC.ID
IS
  'Identificador de la especificación de un metadato perteneciente a un tipo documental.' ;
  COMMENT ON COLUMN METADATA_SPEC.NAME
IS
  'Nombre de la especificación de un metadato perteneciente a un tipo documental.' ;
  COMMENT ON COLUMN METADATA_SPEC.DESCRIPTION
IS
  'Descripción de la especificación de un metadato perteneciente a un tipo documental.' ;
  COMMENT ON COLUMN METADATA_SPEC.DEFAULT_VALUE
IS
  'Valor por defecto asignado a un metadato.' ;
  COMMENT ON COLUMN METADATA_SPEC.MANDATORY
IS
  'Indica si el metadato debe ser informado obligatoriamente para un documento digitalizado.' ;
  COMMENT ON COLUMN METADATA_SPEC.EDITABLE
IS
  'Indica si el metadato puede ser editado.' ;
  COMMENT ON COLUMN METADATA_SPEC.INC_IN_BATCH_SEP
IS
  'Indica si el metadato puede ser incluido en carátulas separadoras de lotes de documentos.' ;
  COMMENT ON COLUMN METADATA_SPEC.IS_BATCH_ID
IS
  'Indica si el metadato actúa como identificador de lotes de documentos.' ;
  COMMENT ON COLUMN METADATA_SPEC.SRC_VALUES
IS
  'Conjunto finito de valores que pueden ser asignados a los metadato definidos mediante esta especificación.' ;
  COMMENT ON COLUMN METADATA_SPEC.DOC_TYPE_SPEC_ID
IS
  'Tipo documental para el cual es definido el metadato.' ;
  ALTER TABLE METADATA_SPEC ADD CONSTRAINT METADATA_SPEC_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE METADATA_SPEC ADD CONSTRAINT METADATA_SPEC_NAME_UN UNIQUE ( NAME , DOC_TYPE_SPEC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Unidad organizativa.
CREATE TABLE ORG_UNIT
  (
    --  Identificador interno de unidad organizativa.
    ID NUMBER (32) NOT NULL ,
    --  Identificador externo de unidad organizativa.
    EXT_ID VARCHAR2 (128 CHAR) NOT NULL ,
    --  Razón social o denominación de la unidad organizativa.
    NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Identificador interno de la unidad organizativa padre.
    PARENT_ORG_ID NUMBER (32)
  ) ;
COMMENT ON TABLE ORG_UNIT
IS
  'Unidad organizativa.' ;
  COMMENT ON COLUMN ORG_UNIT.ID
IS
  'Identificador interno de unidad organizativa.' ;
  COMMENT ON COLUMN ORG_UNIT.EXT_ID
IS
  'Identificador externo de unidad organizativa.' ;
  COMMENT ON COLUMN ORG_UNIT.NAME
IS
  'Razón social o denominación de la unidad organizativa.' ;
  COMMENT ON COLUMN ORG_UNIT.PARENT_ORG_ID
IS
  'Identificador interno de la unidad organizativa padre.' ;
  ALTER TABLE ORG_UNIT ADD CONSTRAINT ORG_UNIT_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE ORG_UNIT ADD CONSTRAINT ORG_UNIT_EXT_ID_UN UNIQUE ( EXT_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Parámetro de un plugin.
CREATE TABLE PARAM
  (
    --  Identificador de un parámetro de un plugin.
    ID NUMBER (32) NOT NULL ,
    --  Valor de un parámetro de un plugin.
    VALUE VARCHAR2 (512 CHAR) ,
    --  Plugin en el cual es establecido el parámetro.
    PLUGIN_ID NUMBER (32) NOT NULL ,
    --  Especificación que define el parámetro.
    PARAM_SPEC_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE PARAM
IS
  'Parámetro de un plugin.' ;
  COMMENT ON COLUMN PARAM.ID
IS
  'Identificador de un parámetro de un plugin.' ;
  COMMENT ON COLUMN PARAM.VALUE
IS
  'Valor de un parámetro de un plugin.' ;
  COMMENT ON COLUMN PARAM.PLUGIN_ID
IS
  'Plugin en el cual es establecido el parámetro.' ;
  COMMENT ON COLUMN PARAM.PARAM_SPEC_ID
IS
  'Especificación que define el parámetro.' ;
  ALTER TABLE PARAM ADD CONSTRAINT PARAM_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE PARAM ADD CONSTRAINT PARAM_UN UNIQUE ( PLUGIN_ID , PARAM_SPEC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Tipo de datos asignable a una definición parámetros.
CREATE TABLE PARAM_DATA_TYPE_SPEC
  (
    --  Identificador de un tipo de datos asignable a una definción parámetros.
    ID   NUMBER (32) NOT NULL ,
	-- Denominación de un tipo de datos asignable a una definición de parámetros.
    NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Patrón de conversión del tipo de datos a y desde cadena de caracteres.
    STRING_PATTERN VARCHAR2 (512 CHAR)
  ) ;
COMMENT ON TABLE PARAM_DATA_TYPE_SPEC
IS
  'Tipo de datos asignable a una definción parámetros.' ;
  COMMENT ON COLUMN PARAM_DATA_TYPE_SPEC.ID
IS
  'Identificador de un tipo de datos asignable a un parámetros.' ;
  COMMENT ON COLUMN PARAM_DATA_TYPE_SPEC.NAME
IS
  'Denominación de un tipo de datos asignable a una definición de parámetros.' ;
  COMMENT ON COLUMN PARAM_DATA_TYPE_SPEC.STRING_PATTERN
IS
  'Patrón de conversión del tipo de datos a y desde cadena de caracteres.' ;
  ALTER TABLE PARAM_DATA_TYPE_SPEC ADD CONSTRAINT PARAM_DATA_TYPE_SPEC_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE PARAM_DATA_TYPE_SPEC ADD CONSTRAINT PARAM_DATA_TYPE_SPEC_UN UNIQUE ( NAME ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Especificación o definición de un parámetro.
CREATE TABLE PARAM_SPEC
  (
    --  Identificador de una especificación o definición de un parámetro.
    ID NUMBER (32) NOT NULL ,
    --  Nombre de un parámetro.
    NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Descripción de un parámetro.
    DESCRIPTION VARCHAR2 (256 CHAR) ,
    --  Especifica si un parámetro es requerido.
    MANDATORY CHAR (1) DEFAULT '0' NOT NULL ,
    --  Tipo de datos de un parámetro.
    DATA_TYPE_ID NUMBER (32) NOT NULL ,
    --  Especificación de plugin a la que pertenece la definción de un parámetro.
    PLUGIN_SPEC_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE PARAM_SPEC
IS
  'Especificación o definición de un parámetro.' ;
  COMMENT ON COLUMN PARAM_SPEC.ID
IS
  'Identificador de una especificación o definición de un parámetro.' ;
  COMMENT ON COLUMN PARAM_SPEC.NAME
IS
  'Nombre de un parámetro.' ;
  COMMENT ON COLUMN PARAM_SPEC.DESCRIPTION
IS
  'Descripción de un parámetro.' ;
  COMMENT ON COLUMN PARAM_SPEC.MANDATORY
IS
  'Especifica si un parámetro es requerido.' ;
  COMMENT ON COLUMN PARAM_SPEC.DATA_TYPE_ID
IS
  'Tipo de datos de un parámetro.' ;
  COMMENT ON COLUMN PARAM_SPEC.PLUGIN_SPEC_ID
IS
  'Especificación de plugin a la que pertenece la definción de un parámetro.' ;
  ALTER TABLE PARAM_SPEC ADD CONSTRAINT PARAM_SPEC_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE PARAM_SPEC ADD CONSTRAINT PARAM_SPEC_NAME_UN UNIQUE ( NAME , PLUGIN_SPEC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Plugin.
CREATE TABLE PLUGIN
  (
    --  Identificador de un plugin.
    ID NUMBER (32) NOT NULL ,
    --  Nombre de un plugin.
    NAME VARCHAR2 (128 CHAR) NOT NULL ,
	--  Descripción del plugin.
	DESCRIPTION VARCHAR2 (256 CHAR) ,
    --  Indica si un plugin está activo.
    ACTIVE CHAR (1) DEFAULT '1' NOT NULL ,
    --  Especificación que define un plugin.
    PLUGIN_SPEC_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE PLUGIN
IS
  'Plugin.' ;
  COMMENT ON COLUMN PLUGIN.ID
IS
  'Identificador de un plugin.' ;
  COMMENT ON COLUMN PLUGIN.NAME
IS
  'Nombre de un plugin.' ;
  COMMENT ON COLUMN PLUGIN.DESCRIPTION
IS
  'Descripción del plugin.' ;
  COMMENT ON COLUMN PLUGIN.ACTIVE
IS
  'Indica si un plugin está activo.' ;
  COMMENT ON COLUMN PLUGIN.PLUGIN_SPEC_ID
IS
  'Especificación que define un plugin.' ;
  ALTER TABLE PLUGIN ADD CONSTRAINT PLUGIN_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE PLUGIN ADD CONSTRAINT PLUGIN_NAME_UN UNIQUE ( NAME , PLUGIN_SPEC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Definición de plugins.
CREATE TABLE PLUGIN_SPEC
  (
    --  Identificador de una especificación o definición de plugins.
    ID NUMBER (32) NOT NULL ,
    --  Nombre de una especificación o definición de plugins.
    NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Descripción de una especificación o definición de plugins.
    DESCRIPTION VARCHAR2 (256 CHAR) ,
    --  Indica si una especificación de plugins está activa.
    ACTIVE CHAR (1) DEFAULT '1' NOT NULL
  ) ;
COMMENT ON COLUMN PLUGIN_SPEC.ID
IS
  'Identificador de una especificación o definición de plugins.' ;
  COMMENT ON COLUMN PLUGIN_SPEC.NAME
IS
  'Nombre de una especificación o definición de plugins.' ;
  COMMENT ON COLUMN PLUGIN_SPEC.DESCRIPTION
IS
  'Descripción de una especificación o definición de plugins.' ;
  COMMENT ON COLUMN PLUGIN_SPEC.ACTIVE
IS
  'Indica si una especificación de plugins está activa.' ;
  ALTER TABLE PLUGIN_SPEC ADD CONSTRAINT PLUGIN_SPEC_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE PLUGIN_SPEC ADD CONSTRAINT PLUGIN_SPEC_NAME_UN UNIQUE ( NAME ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Documento escaneado.
CREATE TABLE SCANNED_DOC
  (
    --  Identificador de documento escaneado.
    ID NUMBER (32) NOT NULL ,
	--  Denominación del documento.
	NAME VARCHAR2(200 CHAR) NOT NULL ,
    --  Hash del documento, una vez firmado.
    HASH BLOB ,
    --  Imagen electrónica capturada que representa el documento en formato papel.
    CONTENT BLOB ,
    --  Ruta del sistema de archivos donde es almacenada temporalmente la 
	--  imagen electrónica capturada, que representa el documento en formato papel.
    TEMP_PATH VARCHAR2 (1024 CHAR) ,
    --  Firma electrónica del documento.
    SIGNATURE BLOB ,
	--  Ruta del sistema de archivos donde es almacenada temporalmente la firma 
	--  electrónica del documento.
    SIGN_TEMP_PATH VARCHAR2 (1024 CHAR) ,
    --  Texto obtenido del documento digitalizado. Opcional, PDF lo incluirán como
    --  capa del propio archivo.
    OCR_CONTENT BLOB ,
    --  Ruta del sistema de archivos donde es almacenado temporalmente el texto obtenido 
	--  del documento digitalizado.
    OCR_TEMP_PATH VARCHAR2 (1024 CHAR) ,
    --  Identificador de un lote de documentos digitalizados.
    BATCH_ID NUMBER (32) ,
	--  Número de orden del documento en el lote, comenzando por 1.
	BATCH_ORDER_NUM NUMBER (5) NOT NULL ,
	--  Identificador de reserva de trabajo de digitalización o carátula asociado a la digitalización del documento.
    BATCH_REQ_ID VARCHAR2 (128 CHAR) ,
    --  Instante en el que se inicia el proceso de digitalización de un documento.
    DIG_PROC_START_DATE DATE NOT NULL ,
    --  Instante en el que se finaliza el proceso completo de digitalización de un
    --  documento.
    DIG_PROC_END_DATE DATE ,
	--  Indica si el documento ha sido verificado y validado por el personal de digitalización.
	CHECKED CHAR(1) DEFAULT '0' NOT NULL,
	--  Fecha en la que ha sido verificado y validado el documento por el personal de digitalización.
    CHECK_DATE DATE ,
	--  Tipo MIME de la imagen capturada del documento.
	MIME_TYPE VARCHAR2 (128 CHAR) NOT NULL ,
    --  Tipo de firma electrónica del documento.
    SIGNATURE_TYPE VARCHAR2 (128 CHAR) ,
    --  Usuario que inicia el proceso de digitalización del documento.
    USERNAME VARCHAR2 (128 CHAR) NOT NULL ,
	-- Unidades orgánicas a las que pertenece el usuario que inicia el proceso de digitalización.
	USER_DPTOS          VARCHAR2 (512 CHAR) ,
    --  Identificador de la  definición de documento configurada en el perfil de
    --  digitalización y unidad organizativa que especifica la composición del
    --  documento.
    SPROF_ORG_DSPEC_ID NUMBER (32) NOT NULL ,
    --  Identificador de la traza de auditoría asociada al proceso de
    --  digitalización del un documento.
    AUDIT_OP_ID NUMBER (32)
  )
  LOB ("HASH") STORE AS BASICFILE "SDOC_HASH_LOB_SEG" (TABLESPACE WEBSCAN_LOB_TS ENABLE STORAGE IN ROW CHUNK 4096 RETENTION 
	NOCACHE LOGGING )  
  LOB ("CONTENT") STORE AS BASICFILE "SDOC_CONT_LOB_SEG" (TABLESPACE WEBSCAN_LOB_TS ENABLE STORAGE IN ROW CHUNK 4096 RETENTION 
	NOCACHE LOGGING )
  LOB ("SIGNATURE") STORE AS BASICFILE "SDOC_SIGN_LOB_SEG" (TABLESPACE WEBSCAN_LOB_TS ENABLE STORAGE IN ROW CHUNK 4096 RETENTION 
	NOCACHE LOGGING )
  LOB ("OCR_CONTENT") STORE AS BASICFILE "SDOC_OCR_LOB_SEG" (TABLESPACE WEBSCAN_LOB_TS ENABLE STORAGE IN ROW CHUNK 4096 RETENTION 
	NOCACHE LOGGING );
	
COMMENT ON TABLE SCANNED_DOC
IS
  'Documento digitalizado.' ;
  COMMENT ON COLUMN SCANNED_DOC.ID
IS
  'Identificador de documento digitalizado.' ;
  COMMENT ON COLUMN SCANNED_DOC.NAME 
IS
  'Denominación del documento.';
  COMMENT ON COLUMN SCANNED_DOC.HASH
IS
  'Hash del documento, una vez firmado.' ;
  COMMENT ON COLUMN SCANNED_DOC.CONTENT
IS
  'Imagen electrónica capturada que representa el documento en formato papel.' ;
  COMMENT ON COLUMN SCANNED_DOC.TEMP_PATH
IS
  'Ruta del sistema de archivos donde es almacenada temporalmente la imagen electrónica capturada, que representa el documento en formato papel.' ;
  COMMENT ON COLUMN SCANNED_DOC.SIGNATURE
IS
  'Firma electrónica del documento.' ;
  COMMENT ON COLUMN SCANNED_DOC.SIGN_TEMP_PATH
IS
  'Ruta del sistema de archivos donde es almacenada temporalmente la firma electrónica del documento.' ;
  COMMENT ON COLUMN SCANNED_DOC.OCR_CONTENT
IS
  'Texto obtenido del documento digitalizado. Opcional, PDF lo incluiran como capa del propio archivo.' ;
  COMMENT ON COLUMN SCANNED_DOC.OCR_TEMP_PATH
IS
  'Ruta del sistema de archivos donde es almacenado temporalmente el texto obtenido del documento digitalizado.' ;
  COMMENT ON COLUMN SCANNED_DOC.BATCH_ID
IS
  'Identificador de un lote de documentos digitalizados.' ;
  COMMENT ON COLUMN SCANNED_DOC.BATCH_ORDER_NUM 
IS
  'Número de orden del documento en el lote, comenzando por 1.';
  COMMENT ON COLUMN SCANNED_DOC.BATCH_REQ_ID 
IS
  'Identificador de reserva de trabajo de digitalización o carátula asociado a la digitalización del documento.';
  COMMENT ON COLUMN SCANNED_DOC.DIG_PROC_START_DATE
IS
  'Instante en el que se inicia el proceso de digitalización de un documento.' ;
  COMMENT ON COLUMN SCANNED_DOC.MIME_TYPE
IS
  'Tipo MIME de la imagen capturada del documento.' ;
  COMMENT ON COLUMN SCANNED_DOC.DIG_PROC_END_DATE
IS
  'Instante en el que se finaliza el proceso completo de digitalización de un documento.' ;
  COMMENT ON COLUMN SCANNED_DOC.CHECKED 
IS
  'Indica si el documento ha sido verificado y validado por el personal de digitalización.';
  COMMENT ON COLUMN SCANNED_DOC.CHECK_DATE 
IS
  'Fecha en la que ha sido verificado y validado el documento por el personal de digitalización.';
  COMMENT ON COLUMN SCANNED_DOC.SIGNATURE_TYPE
IS
  'Tipo de firma electrónica del documento.' ;
  COMMENT ON COLUMN SCANNED_DOC.USERNAME
IS
  'Usuario que inicia el proceso de digitalización del documento.' ;
  COMMENT ON COLUMN SCANNED_DOC.USER_DPTOS
IS
  'Unidades orgánicas a las que pertenece el usuario que inicia el proceso de digitalización.' ;
  COMMENT ON COLUMN SCANNED_DOC.SPROF_ORG_DSPEC_ID
IS
  'Identificador de la  definición de documento configurada en el perfil de digitalización y unidad organizativa que especifica la composición del documento.' ;
  COMMENT ON COLUMN SCANNED_DOC.AUDIT_OP_ID
IS
  'Identificador de la traza de auditoría asociada al proceso de digitalización del un documento.' ;  
  ALTER TABLE SCANNED_DOC ADD CONSTRAINT SCANNED_DOC_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Traza de ejecución o fase ejecutada en el proceso de digitalización de un documento.
CREATE TABLE SCANNED_DOC_LOG
  (
    --  Identificador de traza de ejecución de un documento.
    ID NUMBER (32) NOT NULL ,
	--  Estado de ejecución de la fase de digitalización para un documento.
	DOC_LOG_STATUS VARCHAR2 (50 CHAR) NOT NULL ,
    --  Identificador de fase de proceso de digitalización.
    DPROC_STAGE_ID NUMBER (32) NOT NULL ,
    --  Identificador de documento.
    SCANNED_DOC_ID NUMBER (32) NOT NULL ,
    --  Fecha en la que es iniciada la ejecución de la fase.
    START_DATE DATE NOT NULL ,
    --  Fecha en la que es finalizada la ejecución de la fase.
    END_DATE DATE
  ) ;
COMMENT ON TABLE SCANNED_DOC_LOG
IS
  'Traza de ejecución o fase ejecutada en el proceso de digitalización de un documento.' ;
  COMMENT ON COLUMN SCANNED_DOC_LOG.ID
IS
  'Identificador de traza de ejecución de un documento.' ;
COMMENT ON COLUMN SCANNED_DOC_LOG.DOC_LOG_STATUS
IS
  'Estado de ejecución de la fase de digitalización para un documento.' ;
  COMMENT ON COLUMN SCANNED_DOC_LOG.DPROC_STAGE_ID
IS
  'Identificador de fase de proceso de digitalización.' ;
  COMMENT ON COLUMN SCANNED_DOC_LOG.SCANNED_DOC_ID
IS
  'Identificador de documento.' ;
  COMMENT ON COLUMN SCANNED_DOC_LOG.START_DATE
IS
  'Fecha en la que es iniciada la ejecución de la fase.' ;
  COMMENT ON COLUMN SCANNED_DOC_LOG.END_DATE
IS
  'Fecha en la que es finalizada la ejecución de la fase.' ;
  ALTER TABLE SCANNED_DOC_LOG ADD CONSTRAINT SCANNED_DOC_LOG_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE SCANNED_DOC_LOG ADD CONSTRAINT SCANNED_DOC_LOG_UN UNIQUE ( DPROC_STAGE_ID , SCANNED_DOC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  
--  Perfil de digitalización.
CREATE TABLE SCAN_PROFILE
  (
    --  Identificador de un perfil de digitalización.
    ID NUMBER (32) NOT NULL ,
    --  Denominación de un perfil de digitalización.
    NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Descripción de un perfil de digitalización.
    DESCRIPTION VARCHAR2 (256 CHAR) ,
    --  Indica si un perfil de digitalización está activo.
    ACTIVE CHAR (1) DEFAULT '1' NOT NULL ,
    --  Definición del perfil de digitalización.
    SPROFILE_SPEC_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE SCAN_PROFILE
IS
  'Perfil de digitalización.' ;
  COMMENT ON COLUMN SCAN_PROFILE.ID
IS
  'Identificador de un perfil de digitalización.' ;
  COMMENT ON COLUMN SCAN_PROFILE.NAME
IS
  'Denominación de un perfil de digitalización.' ;
  COMMENT ON COLUMN SCAN_PROFILE.DESCRIPTION
IS
  'Descripción de un perfil de digitalización.' ;
  COMMENT ON COLUMN SCAN_PROFILE.ACTIVE
IS
  'Indica si un perfil de digitalización está activo.' ;
  COMMENT ON COLUMN SCAN_PROFILE.SPROFILE_SPEC_ID
IS
  'Definición del perfil de digitalización.' ;
  ALTER TABLE SCAN_PROFILE ADD CONSTRAINT SCAN_PROFILE_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE SCAN_PROFILE ADD CONSTRAINT SCAN_PROFILE_NAME_UN UNIQUE ( NAME ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Configuración de perfil de digitalización en unidad organizativa.
CREATE TABLE SCAN_PROFILE_ORG
  (
    --  Identificador de configuración de perfil de digitalización en unidad organizativa.
    ID NUMBER (32) NOT NULL ,
    --  Identificador de un perfil de digitalización.
    SCAN_PROFILE_ID NUMBER (32) NOT NULL ,
    --  Identificador de unidad organizativa.
    ORG_UNIT_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE SCAN_PROFILE_ORG
IS
  'Configuración de perfil de digitalización en unidad organizativa.' ;
  COMMENT ON COLUMN SCAN_PROFILE_ORG.ID
IS
  'Identificador de configuración de perfil de digitalización en unidad organizativa.' ;
  COMMENT ON COLUMN SCAN_PROFILE_ORG.SCAN_PROFILE_ID
IS
  'Identificador de un perfil de digitalización.' ;
  COMMENT ON COLUMN SCAN_PROFILE_ORG.ORG_UNIT_ID
IS
  'Identificador de unidad organizativa.' ;
  ALTER TABLE SCAN_PROFILE_ORG ADD CONSTRAINT SCAN_PROFILE_ORG_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE SCAN_PROFILE_ORG ADD CONSTRAINT SCAN_PROFILE_ORG_UN UNIQUE ( SCAN_PROFILE_ID , ORG_UNIT_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Definición de perfiles de digitalización.
CREATE TABLE SCAN_PROFILE_SPEC
  (
    --  Identificador de definición de perfiles de digitalización.
    ID NUMBER (32) NOT NULL ,
    --  Denominación de definición perfiles de digitalización.
    NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Breve descripción de definición perfiles de digitalización.
    DESCRIPTION VARCHAR2 (256 CHAR) NOT NULL
  ) ;
COMMENT ON TABLE SCAN_PROFILE_SPEC
IS
  'Definición de perfiles de digitalización.' ;
  COMMENT ON COLUMN SCAN_PROFILE_SPEC.ID
IS
  'Identificador de definición de perfiles de digitalización.' ;
  COMMENT ON COLUMN SCAN_PROFILE_SPEC.NAME
IS
  'Denominación de definición de perfiles de digitalización.' ;
  COMMENT ON COLUMN SCAN_PROFILE_SPEC.DESCRIPTION
IS
  'Breve descripción de definición de perfiles de digitalización.' ;
  ALTER TABLE SCAN_PROFILE_SPEC ADD CONSTRAINT SCAN_PROFILE_SPEC_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE SCAN_PROFILE_SPEC ADD CONSTRAINT SCAN_PROFILE_SPEC_NAME_UN UNIQUE ( NAME ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Configuración de una especificación de documento para un perfil de
--  digitalización empleado en una unidad organizativa.
CREATE TABLE SPROF_ORG_DOC_SPEC
  (
    --  Identificador de configuración de una especificación de documento para un
    --  perfil de digitalización empleado en una unidad organizativa.
    ID NUMBER (32) NOT NULL ,
    --  Indica si la definición de documento puede ser empleada por la
    --  configuración de perfil de digitalización operativa sobre una unidad
    --  organizativa.
    ACTIVE CHAR (1) DEFAULT '1' NOT NULL ,
    --  Identificador de un perfil de digitalización empleado en una unidad
    --  organizativa.
    SPROF_ORG_ID NUMBER (32) NOT NULL ,
    --  Identificador de una definición de documentos.
    DOC_SPEC_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE SPROF_ORG_DOC_SPEC
IS
  'Configuración de una especificación de documento para un perfil de digitalización empleado en una unidad organizativa.' ;
  COMMENT ON COLUMN SPROF_ORG_DOC_SPEC.ID
IS
  'Identificador de configuración de una especificación de documento para un perfil de digitalización empleado en una unidad organizativa.' ;
  COMMENT ON COLUMN SPROF_ORG_DOC_SPEC.ACTIVE
IS
  'Indica si la definición de documento puede ser empleada por la configuración de perfil de digitalización operativa sobre una unidad organizativa.' ;
  COMMENT ON COLUMN SPROF_ORG_DOC_SPEC.SPROF_ORG_ID
IS
  'Identificador de un perfil de digitalización empleado en una unidad organizativa.' ;
  COMMENT ON COLUMN SPROF_ORG_DOC_SPEC.DOC_SPEC_ID
IS
  'Identificador de una definición de documentos.' ;
  ALTER TABLE SPROF_ORG_DOC_SPEC ADD CONSTRAINT SPROF_ORG_DS_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE SPROF_ORG_DOC_SPEC ADD CONSTRAINT SPROF_ORG_DOC_SPEC_UN UNIQUE ( SPROF_ORG_ID , DOC_SPEC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Relación de plugins parametrizados para las diferentes fases del proceso de digitalización de los perfiles de digitalización configurados para unidades organizativas.
CREATE TABLE SPROF_ORG_PLUGIN
  (
    --  Identificador de una configuración de un plugin para un perfil de
    --  digitalización en una unidad organizativa.
    ID NUMBER (32) NOT NULL ,
    --  Indica si un plugin puede ser empleado por la configuración de perfil de
    --  digitalización operativa sobre una unidad organizativa.
    ACTIVE CHAR (1) NOT NULL ,
    --  Identificador de una relación de uso de un perfil de digitalización en una
    --  organización.
    SPROF_ORG_ID NUMBER (32) NOT NULL ,
    --  Identificador de un plugin.
    PLUGIN_ID NUMBER (32) NOT NULL,
	--  Identificador de fase de procesos de digitalización.
	DPROC_STAGE_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE SPROF_ORG_PLUGIN
IS
  'Relación de plugins parametrizados para las diferentes fases del proceso de digitalización de los perfiles de digitalización configurados para unidades organizativas.' ;
  COMMENT ON COLUMN SPROF_ORG_PLUGIN.ID
IS
  'Identificador de una configuración de un plugin para un perfil de digitalización en una unidad organizativa.' ;
  COMMENT ON COLUMN SPROF_ORG_PLUGIN.ACTIVE
IS
  'Indica si un plugin puede ser empleado por la configuración de perfil de digitalización operativa sobre una unidad organizativa.' ;
  COMMENT ON COLUMN SPROF_ORG_PLUGIN.SPROF_ORG_ID
IS
  'Identificador de una relación de uso de un perfil de digitalización en una organización.' ;
  COMMENT ON COLUMN SPROF_ORG_PLUGIN.PLUGIN_ID
IS
  'Identificador de un plugin.' ;
  COMMENT ON COLUMN SPROF_ORG_PLUGIN.DPROC_STAGE_ID
IS
  'Identificador de fase de procesos de digitalización.' ;
  ALTER TABLE SPROF_ORG_PLUGIN ADD CONSTRAINT SPROF_ORG_PLUGIN_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE SPROF_ORG_PLUGIN ADD CONSTRAINT SPROF_ORG_PLUGIN_UN UNIQUE ( SPROF_ORG_ID , PLUGIN_ID , DPROC_STAGE_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Fases configuradas para especificaciones de perfil de digitalización.
CREATE TABLE SPROF_SPEC_DPROC_STAGE
  (
    --  Identifica una fase configurada para un especificación de perfil de
    --  digitalización.
    ID NUMBER (32) NOT NULL ,
    --  Orden de ejecución de la fase para una familia de perfiles de
    --  digitalización.
    EXEC_ORDER INTEGER NOT NULL ,
    --  Indica si una fase debe estar configurada obligatoriamente para un perfil
    --  de digitalización.
    MANDATORY CHAR (1) DEFAULT '0' NOT NULL ,
	--  Indica si una fase puede ser reanudada para una definición de proceso 
	--  de digitalización.
    RENEWABLE CHAR (1) DEFAULT '0' NOT NULL ,
    --  Fase del proceso de digitalización.
    DPROC_STAGE_ID NUMBER (32) NOT NULL ,
    --  Definición de perfil de digitalización.
    SPROFILE_SPEC_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE SPROF_SPEC_DPROC_STAGE
IS
  'Fases configuradas para especificaciones de perfil de digitalización.' ;
  COMMENT ON COLUMN SPROF_SPEC_DPROC_STAGE.EXEC_ORDER
IS
  'Orden de ejecución de la fase para una familia de perfiles de digitalización.' ;
  COMMENT ON COLUMN SPROF_SPEC_DPROC_STAGE.MANDATORY
IS
  'Indica si una fase debe estar configurada obligatoriamente para un perfil de digitalización.' ;
  COMMENT ON COLUMN SPROF_SPEC_DPROC_STAGE.RENEWABLE
IS
  'Indica si una fase puede ser reanudada para una definición de proceso de digitalización.' ;
  COMMENT ON COLUMN SPROF_SPEC_DPROC_STAGE.DPROC_STAGE_ID
IS
  'Fase del proceso de digitalización.' ;
  COMMENT ON COLUMN SPROF_SPEC_DPROC_STAGE.SPROFILE_SPEC_ID
IS
  'Definición de perfil de digitalización.' ;
  ALTER TABLE SPROF_SPEC_DPROC_STAGE ADD CONSTRAINT SPROF_SPEC_DPROC_STAGE_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE SPROF_SPEC_DPROC_STAGE ADD CONSTRAINT SPROF_SPEC_DPROC_STAGE_UN UNIQUE ( DPROC_STAGE_ID , SPROFILE_SPEC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  
-- FOREIGN KEYS
ALTER TABLE BATCH ADD CONSTRAINT BATCH_SPROF_ORG_DS_FK FOREIGN KEY ( SPROF_ORG_DSPEC_ID ) REFERENCES SPROF_ORG_DOC_SPEC ( ID );

ALTER TABLE DOC_METADATA ADD CONSTRAINT DOC_METADATA_METADATA_SPEC_FK FOREIGN KEY ( METADATA_SPEC_ID ) REFERENCES METADATA_SPEC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE DOC_METADATA ADD CONSTRAINT DOC_METADATA_SCANNED_DOC_FK FOREIGN KEY ( SCANNED_DOC_ID ) REFERENCES SCANNED_DOC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE DPROC_STAGE_PLSPEC ADD CONSTRAINT DPROC_STAGE_PLSPEC_DPSTG_FK FOREIGN KEY ( DPROC_STAGE_ID ) REFERENCES DIGIT_PROC_STAGE ( ID ) ;

ALTER TABLE INHERITED_DOC_TYPE_SPEC ADD CONSTRAINT INHER_DTYPE_CHILD_DTYPE_FK FOREIGN KEY ( CHILD_DOC_TYPE_SPEC ) REFERENCES DOC_SPEC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE INHERITED_DOC_TYPE_SPEC ADD CONSTRAINT INHER_DTYPE_PARENT_DTYPE_FK FOREIGN KEY ( PARENT_DOC_TYPE_SPEC ) REFERENCES DOC_SPEC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE METADATA_SPEC ADD CONSTRAINT METADATA_SPEC_DOC_TYPE_SPEC_FK FOREIGN KEY ( DOC_TYPE_SPEC_ID ) REFERENCES DOC_SPEC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE ORG_UNIT ADD CONSTRAINT ORG_UNIT_ORG_UNIT_FK FOREIGN KEY ( PARENT_ORG_ID ) REFERENCES ORG_UNIT ( ID ) ON
DELETE CASCADE ;

ALTER TABLE SCAN_PROFILE_ORG ADD CONSTRAINT ORG_SPROF_ORG_FK FOREIGN KEY ( ORG_UNIT_ID ) REFERENCES ORG_UNIT ( ID ) ON
DELETE CASCADE ;

ALTER TABLE SCAN_PROFILE_ORG ADD CONSTRAINT ORG_SPROF_SPROF_FK FOREIGN KEY ( SCAN_PROFILE_ID ) REFERENCES SCAN_PROFILE ( ID ) ON
DELETE CASCADE ;

ALTER TABLE PARAM ADD CONSTRAINT PARAM_PARAM_SPEC_FK FOREIGN KEY ( PARAM_SPEC_ID ) REFERENCES PARAM_SPEC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE PARAM ADD CONSTRAINT PARAM_PLUGIN_FK FOREIGN KEY ( PLUGIN_ID ) REFERENCES PLUGIN ( ID ) ON
DELETE CASCADE ;

ALTER TABLE PARAM_SPEC ADD CONSTRAINT PARAM_SPEC_DATA_TYPE_SPEC_FK FOREIGN KEY ( DATA_TYPE_ID ) REFERENCES PARAM_DATA_TYPE_SPEC ( ID ) ;

ALTER TABLE PARAM_SPEC ADD CONSTRAINT PARAM_SPEC_PLUGIN_SPEC_FK FOREIGN KEY ( PLUGIN_SPEC_ID ) REFERENCES PLUGIN_SPEC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE PLUGIN ADD CONSTRAINT PLUGIN_PLUGIN_SPEC_FK FOREIGN KEY ( PLUGIN_SPEC_ID ) REFERENCES PLUGIN_SPEC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE SCANNED_DOC ADD CONSTRAINT SCANNED_DOC_SPROF_ORG_DS_FK FOREIGN KEY ( SPROF_ORG_DSPEC_ID ) REFERENCES SPROF_ORG_DOC_SPEC ( ID ) ;

ALTER TABLE SCANNED_DOC ADD CONSTRAINT SCANNED_DOC_BATCH_FK FOREIGN KEY ( BATCH_ID ) REFERENCES BATCH ( ID ) ON
DELETE CASCADE ;

ALTER TABLE SCAN_PROFILE ADD CONSTRAINT SCAN_PROFILE_SPROF_SPEC_FK FOREIGN KEY ( SPROFILE_SPEC_ID ) REFERENCES SCAN_PROFILE_SPEC ( ID ) ;

ALTER TABLE SCANNED_DOC_LOG ADD CONSTRAINT SCDOC_LOG_DPROC_STG_FK FOREIGN KEY ( DPROC_STAGE_ID ) REFERENCES DIGIT_PROC_STAGE ( ID ) ON
DELETE CASCADE ;

ALTER TABLE SCANNED_DOC_LOG ADD CONSTRAINT SCDOC_LOG_SCANNED_DOC_FK FOREIGN KEY ( SCANNED_DOC_ID ) REFERENCES SCANNED_DOC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE SPROF_ORG_DOC_SPEC ADD CONSTRAINT SPROF_ORG_DS_DOC_SPEC_FK FOREIGN KEY ( DOC_SPEC_ID ) REFERENCES DOC_SPEC ( ID ) ON
DELETE CASCADE ;

ALTER TABLE SPROF_ORG_DOC_SPEC ADD CONSTRAINT SPROF_ORG_DS_SPROF_ORG_FK FOREIGN KEY ( SPROF_ORG_ID ) REFERENCES SCAN_PROFILE_ORG ( ID ) ON
DELETE CASCADE ;

ALTER TABLE DPROC_STAGE_PLSPEC ADD CONSTRAINT SPROF_PSPEC_PSPEC_FK FOREIGN KEY ( PLUGIN_SPEC_ID ) REFERENCES PLUGIN_SPEC ( ID ) ;

ALTER TABLE SPROF_SPEC_DPROC_STAGE ADD CONSTRAINT SPROF_SPEC_DPSTG_DPSTG_FK FOREIGN KEY ( DPROC_STAGE_ID ) REFERENCES DIGIT_PROC_STAGE ( ID ) ;

ALTER TABLE SPROF_SPEC_DPROC_STAGE ADD CONSTRAINT SPROF_SPEC_DPSTG_SPROF_SPEC_FK FOREIGN KEY ( SPROFILE_SPEC_ID ) REFERENCES SCAN_PROFILE_SPEC ( ID ) ;

ALTER TABLE SPROF_ORG_PLUGIN ADD CONSTRAINT SP_ORG_PLUGIN_PLUGIN_FK FOREIGN KEY ( PLUGIN_ID ) REFERENCES PLUGIN ( ID ) ON
DELETE CASCADE ;

ALTER TABLE SPROF_ORG_PLUGIN ADD CONSTRAINT SP_ORG_PLUGIN_SP_ORG_FK FOREIGN KEY ( SPROF_ORG_ID ) REFERENCES SCAN_PROFILE_ORG ( ID ) ON
DELETE CASCADE ;

ALTER TABLE SPROF_ORG_PLUGIN ADD CONSTRAINT SP_ORG_PLUGIN_DPSTG_FK FOREIGN KEY ( DPROC_STAGE_ID ) REFERENCES DIGIT_PROC_STAGE ( ID ) ON 
DELETE CASCADE ;
  
-- SEQUENCES
CREATE SEQUENCE BATCH_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE DOC_METADATA_ID_SEQ START WITH 1 INCREMENT BY 2 NOCACHE ORDER ;

CREATE SEQUENCE DOC_SPEC_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE METADATA_SPEC_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE ORG_UNIT_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE PARAM_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE PARAM_SPEC_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE PLUGIN_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE SCANNED_DOC_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE SCANNED_DOC_LOG_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE SCAN_PROFILE_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE SCAN_PROFILE_ORG_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE SPROF_ORG_DSPEC_ID_SEQ START WITH 1 INCREMENT BY 2 NOCACHE ORDER ;

CREATE SEQUENCE SPROF_ORG_PLUGIN_ID_SEQ START WITH 1 INCREMENT BY 2 NOCACHE ORDER ;

CREATE SEQUENCE SPROF_SPEC_DPSTG_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

-- ************************ AUDIT DATA MODEL ***************************

--  Histórico de documentos digitalizados.
CREATE TABLE AUDIT_DOC
  (
    --  Identificador del histórico de un documento.
    ID NUMBER (32) NOT NULL ,
    --  Hash del documento, una vez firmado.
    HASH BLOB NOT NULL ,
    --  Identificador de un lote de documentos digitalizados.
    BATCH_ID VARCHAR2 (128 CHAR) ,
	--  Identificador de reserva de trabajo de digitalización o carátula asociado a la digitalización del documento.
    BATCH_REQ_ID VARCHAR2 (128 CHAR) ,
    --  Instante en el que se inicia el proceso de digitalización de un documento.
    DIG_PROC_START_DATE DATE NOT NULL ,
    --  Instante en el que se finaliza el proceso completo de digitalización de un
    --  documento.
    DIG_PROC_END_DATE DATE NOT NULL ,
    --  Tipo MIME de la imagen capturada del documento.
    MIME_TYPE VARCHAR2 (128 CHAR) NOT NULL ,
    --  Tipo de firma electrónica del documento.
    SIGNATURE_TYPE VARCHAR2 (128 CHAR) ,
    --  Usuario que inicia el proceso de digitalización del documento.
    USERNAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Unidades orgánicas a las que pertenece el usuario que inicia el proceso de
    --  digitalización.
    USER_DPTOS VARCHAR2 (512 CHAR) ,
    --  Denominación de la definición que especifica los metadatos del documento
    --  digitalizado.
    DOC_SPEC_NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Identificador de la traza de auditoría del proceso de digitalización del
    --  documento.
    AUDIT_OP_ID NUMBER (32) NOT NULL
  ) 
  LOB ("HASH") STORE AS BASICFILE "AUDITDOC_HASH_LOB_SEG" (TABLESPACE WEBSCAN_LOB_TS ENABLE STORAGE IN ROW CHUNK 4096 RETENTION 
	NOCACHE LOGGING ) ;
	
COMMENT ON TABLE AUDIT_DOC
IS
  'Histórico de documentos digitalizados.' ;
  COMMENT ON COLUMN AUDIT_DOC.ID
IS
  'Identificador del histórico de un documento.' ;
  COMMENT ON COLUMN AUDIT_DOC.HASH
IS
  'Hash del documento, una vez firmado.' ;
  COMMENT ON COLUMN AUDIT_DOC.BATCH_ID
IS
  'Identificador de un lote de documentos digitalizados.' ;
  COMMENT ON COLUMN AUDIT_DOC.BATCH_REQ_ID 
IS
  'Identificador de reserva de trabajo de digitalización o carátula asociado a la digitalización del documento.';
  COMMENT ON COLUMN AUDIT_DOC.DIG_PROC_START_DATE
IS
  'Instante en el que se inicia el proceso de digitalización de un documento.' ;
  COMMENT ON COLUMN AUDIT_DOC.DIG_PROC_END_DATE
IS
  'Instante en el que se finaliza el proceso completo de digitalización de un documento.' ;
  COMMENT ON COLUMN AUDIT_DOC.MIME_TYPE
IS
  'Tipo MIME de la imagen capturada del documento.' ;
  COMMENT ON COLUMN AUDIT_DOC.SIGNATURE_TYPE
IS
  'Tipo de firma electrónica del documento.' ;
  COMMENT ON COLUMN AUDIT_DOC.USERNAME
IS
  'Usuario que inicia el proceso de digitalización del documento.' ;
  COMMENT ON COLUMN AUDIT_DOC.USER_DPTOS
IS
  'Unidades orgánicas a las que pertenece el usuario que inicia el proceso de digitalización.' ;
  COMMENT ON COLUMN AUDIT_DOC.DOC_SPEC_NAME
IS
  'Denominación de la  definición que especifica los metadatos del documento digitalizado.' ;
  COMMENT ON COLUMN AUDIT_DOC.AUDIT_OP_ID
IS
  'Identificador de la traza de auditoría del proceso de digitalización del documento.' ;
  ALTER TABLE AUDIT_DOC ADD CONSTRAINT AUDIT_DOC_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Registro de eventos de auditoría asociados a operaciones efectuadas en el
--  módulo de digitalización.
CREATE TABLE AUDIT_EVENT
  (
    --  Identificador de una traza de evento de auditoría.
    ID NUMBER (32) NOT NULL ,
    --  Entidad o usuario que genera o es objeto del evento registrado.
    EVENT_USER VARCHAR2 (128 CHAR) NOT NULL ,
    --  Instante en el que se produce el evento registrado.
    EVENT_DATE DATE NOT NULL ,
    --  Evento que es registrado.
    EVENT_NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Identificador de la operación en la que se produce el evento.
    AUDIT_OP_ID NUMBER (32) NOT NULL ,
    --  Información adicional de la traza.
    EVENT_DATA VARCHAR2 (512 CHAR) ,
    --  Resultado del evento auditado.
    EVENT_RESULT VARCHAR2 (8 CHAR) NOT NULL
  ) ;
COMMENT ON TABLE AUDIT_EVENT
IS
  'Registro de eventos de auditoría asociados a operaciones efectuadas en el módulo de digitalización.' ;
  COMMENT ON COLUMN AUDIT_EVENT.ID
IS
  'Identificador de una traza de evento de auditoría.' ;
  COMMENT ON COLUMN AUDIT_EVENT.EVENT_USER
IS
  'Entidad o usuario que genera o es objeto del evento registrado.' ;
  COMMENT ON COLUMN AUDIT_EVENT.EVENT_DATE
IS
  'Instante en el que se produce el evento registrado.' ;
  COMMENT ON COLUMN AUDIT_EVENT.EVENT_NAME
IS
  'Evento que es registrado.' ;
  COMMENT ON COLUMN AUDIT_EVENT.AUDIT_OP_ID
IS
  'Identificador de transacción de operación.' ;
  COMMENT ON COLUMN AUDIT_EVENT.EVENT_DATA
IS
  'Información adicional de la traza.' ;
  COMMENT ON COLUMN AUDIT_EVENT.EVENT_RESULT
IS
  'Resultado del evento auditado.' ;
  ALTER TABLE AUDIT_EVENT ADD CONSTRAINT AUDIT_EVENT_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Auditoría de operaciones del módulo de digitalización.
CREATE TABLE AUDIT_OPERATION
  (
    --  Identificador de la operación auditada.
    ID NUMBER (32) NOT NULL ,
    --  Nombre de la operación auditada.
    OPERATION_NAME VARCHAR2 (128 CHAR) NOT NULL ,
	--  Estado de la operación (iniciada, finalizada, etc.)
    OPERATION_STATUS VARCHAR2 (128 CHAR) NOT NULL ,
	--  Fecha en la que se creo la traza de auditoría.
	OPERATION_START_DATE  DATE NOT NULL ,
	--  Fecha en la que fue modificada la traza por última vez.
    OPERATION_STATUS_DATE DATE NOT NULL
  ) ;
COMMENT ON TABLE AUDIT_OPERATION
IS
  'Auditoría de operaciones del módulo de digitalización.' ;
  COMMENT ON COLUMN AUDIT_OPERATION.ID
IS
  'Identificador de la operación auditada.' ;
  COMMENT ON COLUMN AUDIT_OPERATION.OPERATION_NAME
IS
  'Nombre de la operación auditada.' ;
  COMMENT ON COLUMN AUDIT_OPERATION.OPERATION_STATUS
IS
  'Estado de la operación (iniciada, finalizada, etc.)' ;
  COMMENT ON COLUMN AUDIT_OPERATION.OPERATION_START_DATE
IS
  'Fecha en la que se creo la traza de auditoría.' ;
  COMMENT ON COLUMN AUDIT_OPERATION.OPERATION_STATUS_DATE
IS
  'Fecha en la que fue modificada la traza por última vez.' ;

ALTER TABLE AUDIT_OPERATION ADD CONSTRAINT AUDIT_OPERATION_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

ALTER TABLE AUDIT_DOC ADD CONSTRAINT AUDIT_DOC_AUDIT_OPERATION_FK FOREIGN KEY ( AUDIT_OP_ID ) REFERENCES AUDIT_OPERATION ( ID ) ON
DELETE CASCADE ;
  
ALTER TABLE AUDIT_EVENT ADD CONSTRAINT AUDIT_EVENT_AUDIT_OP_FK FOREIGN KEY ( AUDIT_OP_ID ) REFERENCES AUDIT_OPERATION ( ID ) ON
DELETE CASCADE ;

CREATE SEQUENCE AUDIT_DOC_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE AUDIT_EVENT_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

CREATE SEQUENCE AUDIT_OPERATION_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

/