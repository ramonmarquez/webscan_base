-- **********************************************************************
-- ************************ CREATE DATA MODEL ***************************
-- **********************************************************************

-- ************************ USERS DATA MODEL ***************************

-- TABLES

--  Información de usuario.
CREATE TABLE APP_USER
  (
    --  Identificador interno de usuario.
    ID NUMBER (32) NOT NULL ,
    --  Nombre de usuario.
    USERNAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Password de usuario.
    PASSWORD VARCHAR2 (512 CHAR) NOT NULL ,
    --  Nombre de pila del usuario.
    NAME VARCHAR2 (128 CHAR) NOT NULL ,
    --  Primer apellido del usuario.
    SURNAME_1 VARCHAR2 (128 CHAR) NOT NULL ,
    --  Segundo apellido del usuario.
    SURNAME_2 VARCHAR2 (128 CHAR) ,
    --  Documento de identificación del usuario.
    DOC_ID VARCHAR2 (50 CHAR) NOT NULL ,
    --  Dirección de correo del usuario.
    EMAIL  VARCHAR2 (256 CHAR) NOT NULL ,
	--  Indicador de bloqueo o inhabilitación del usuario.
    LOCKED CHAR (1) NOT NULL ,
    --  Fecha de creación del usuario en el sistema.
    CREATION_DATE DATE NOT NULL ,
    --  Fecha de la última modificación de la información del usuario registrada en
    --  el sistema.
    LAST_UPDATE_DATE DATE NOT NULL ,
    --  Fecha límite para modificar el password del usuario.
    NEXT_PASS_UPD_DATE DATE NOT NULL ,
	-- Número de intentos fallidos consecutivos realizados por el usuario.
	FAILED_ATTEMPTS NUMBER (4) DEFAULT 0 NOT NULL
  ) ;
COMMENT ON TABLE APP_USER
IS
  'Información de usuario.' ;
  COMMENT ON COLUMN APP_USER.ID
IS
  'Identificador interno de usuario.' ;
  COMMENT ON COLUMN APP_USER.USERNAME
IS
  'Nombre de usuario.' ;
  COMMENT ON COLUMN APP_USER.PASSWORD
IS
  'Password de usuario.' ;
  COMMENT ON COLUMN APP_USER.NAME
IS
  'Nombre de pila del usuario.' ;
  COMMENT ON COLUMN APP_USER.SURNAME_1
IS
  'Primer apellido del usuario.' ;
  COMMENT ON COLUMN APP_USER.SURNAME_2
IS
  'Segundo apellido del usuario.' ;
  COMMENT ON COLUMN APP_USER.DOC_ID
IS
  'Documento de identificación del usuario.' ;
  COMMENT ON COLUMN APP_USER.EMAIL
IS
  'Dirección de correo del usuario.' ;
  COMMENT ON COLUMN APP_USER.LOCKED
IS
  'Indicador de bloqueo o inhabilitación del usuario.' ;
  COMMENT ON COLUMN APP_USER.CREATION_DATE
IS
  'Fecha de creación del usuario en el sistema.' ;
  COMMENT ON COLUMN APP_USER.LAST_UPDATE_DATE
IS
  'Fecha de la última modificación de la información del usuario registrada en el sistema.' ;
  COMMENT ON COLUMN APP_USER.NEXT_PASS_UPD_DATE
IS
  'Fecha límite para modificar el password del usuario.' ;
  COMMENT ON COLUMN APP_USER.FAILED_ATTEMPTS
IS
  'Número de intentos fallidos consecutivos realizados por el usuario.' ;
  ALTER TABLE APP_USER ADD CONSTRAINT USER_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE APP_USER ADD CONSTRAINT USER_DOC_ID_UN UNIQUE ( DOC_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE APP_USER ADD CONSTRAINT USER_USERNAME_UN UNIQUE ( USERNAME ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE APP_USER ADD CONSTRAINT USER_EMAIL_UN UNIQUE ( EMAIL ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Perfiles asignados a usuarios.
CREATE TABLE APP_USER_PROFILE
  (
    --  Identificador de un usuario.
    APP_USER_ID NUMBER (32) NOT NULL ,
    --  Identificador de un perfil de usuario.
    PROFILE_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE APP_USER_PROFILE
IS
  'Perfiles asignados a usuarios.' ;
  COMMENT ON COLUMN APP_USER_PROFILE.APP_USER_ID
IS
  'Identificador de un usuario.' ;
  COMMENT ON COLUMN APP_USER_PROFILE.PROFILE_ID
IS
  'Identificador de un perfil de usuario.' ;
  ALTER TABLE APP_USER_PROFILE ADD CONSTRAINT APP_USER_PROFILE_PK PRIMARY KEY ( APP_USER_ID, PROFILE_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Peticiones de recuperación de password de usuario.
CREATE TABLE REC_USER_PASS_REQ
  (
    --  Identificador de petición de recuperación de password de usuario.
    ID NUMBER (32) NOT NULL ,
    --  Usuario que solicita la recuperación de su password.
    APP_USER_ID NUMBER (32) NOT NULL ,
    --  Token que securiza el establecimiento de la nueva password.
    TOKEN VARCHAR2 (128 CHAR) NOT NULL ,
    --  Fecha de emisión de la comunicación de recuperación de password.
    SEND_COM_DATE DATE NOT NULL ,
    --  Fecha en la que es restablecida el password para el que se solicito su
    --  recuperación.
    RECOVERY_DATE DATE ,
	-- Estado de la petición de recuperación de password.
	RECOVERY_STATUS VARCHAR2 (20 CHAR) NOT NULL ,
	-- Identificador de la traza de auditoría de la petición de recuperación de password.
    AUDIT_OP_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE REC_USER_PASS_REQ
IS
  'Peticiones de recuperación de password de usuario.' ;
  COMMENT ON COLUMN REC_USER_PASS_REQ.ID
IS
  'Identificador de petición de recuperación de password de usuario.' ;
  COMMENT ON COLUMN REC_USER_PASS_REQ.APP_USER_ID
IS
  'Usuario que solicita la recuperación de su password.' ;
  COMMENT ON COLUMN REC_USER_PASS_REQ.TOKEN
IS
  'Token que securiza el establecimiento de la nueva password.' ;
  COMMENT ON COLUMN REC_USER_PASS_REQ.SEND_COM_DATE
IS
  'Fecha de emisión de la comunicación de recuperación de password.' ;
  COMMENT ON COLUMN REC_USER_PASS_REQ.RECOVERY_DATE
IS
  'Fecha en la que es restablecida el password para el que se solicito su recuperación.' ;
  COMMENT ON COLUMN REC_USER_PASS_REQ.RECOVERY_STATUS
IS
  'Estado de la petición de recuperación de password.' ;
  COMMENT ON COLUMN REC_USER_PASS_REQ.AUDIT_OP_ID
IS
  'Identificador de la traza de auditoría de la petición de recuperación de password.' ;
  ALTER TABLE REC_USER_PASS_REQ ADD CONSTRAINT REC_USER_PASS_REQ_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Relación de usuarios pertenecientes a unidades organizativas.
CREATE TABLE ORG_UNIT_APP_USER
  (
    --  Identificador interno de unidad organizativa.
    ORG_UNIT_ID NUMBER (32) NOT NULL ,
    --  Identificador interno de usuario.
    APP_USER_ID NUMBER (32) NOT NULL
  ) ;
COMMENT ON TABLE ORG_UNIT_APP_USER
IS
  'Relación de usuarios pertenecientes a unidades organizativas.' ;
  COMMENT ON COLUMN ORG_UNIT_APP_USER.ORG_UNIT_ID
IS
  'Identificador interno de unidad organizativa.' ;
  COMMENT ON COLUMN ORG_UNIT_APP_USER.APP_USER_ID
IS
  'Identificador interno de usuario.' ;
  ALTER TABLE ORG_UNIT_APP_USER ADD CONSTRAINT ORG_UNIT_APP_USER_PK PRIMARY KEY ( APP_USER_ID, ORG_UNIT_ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

--  Perfil de usuarios.
CREATE TABLE PROFILE
  (
    --  Identificador de perfil de usuarios.
    ID NUMBER (32) NOT NULL ,
    --  Denominación de un perfil de usuarios.
    NAME VARCHAR2 (50 CHAR) NOT NULL
  ) ;
COMMENT ON TABLE PROFILE
IS
  'Perfil de usuarios.' ;
  COMMENT ON COLUMN PROFILE.ID
IS
  'Identificador de perfil de usuarios.' ;
  COMMENT ON COLUMN PROFILE.NAME
IS
  'Denominación de un perfil de usuarios.' ;
  ALTER TABLE PROFILE ADD CONSTRAINT PROFILE_PK PRIMARY KEY ( ID ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;
  ALTER TABLE PROFILE ADD CONSTRAINT PROFILE_NAME_UN UNIQUE ( NAME ) USING INDEX TABLESPACE WEBSCAN_INDEX_TS ;

ALTER TABLE REC_USER_PASS_REQ ADD CONSTRAINT REC_PASS_REQ_APP_USER_FK FOREIGN KEY ( APP_USER_ID ) REFERENCES APP_USER ( ID ) ON
DELETE CASCADE ;

ALTER TABLE ORG_UNIT_APP_USER ADD CONSTRAINT ORG_APP_USER_APP_USER_FK FOREIGN KEY ( APP_USER_ID ) REFERENCES APP_USER ( ID ) ON
DELETE CASCADE ;

ALTER TABLE ORG_UNIT_APP_USER ADD CONSTRAINT ORG_APP_USER_ORG_FK FOREIGN KEY ( ORG_UNIT_ID ) REFERENCES ORG_UNIT ( ID ) ON
DELETE CASCADE ;

ALTER TABLE APP_USER_PROFILE ADD CONSTRAINT APP_USER_PROF_PROF_FK FOREIGN KEY ( PROFILE_ID ) REFERENCES PROFILE ( ID ) ON
DELETE CASCADE ;

ALTER TABLE APP_USER_PROFILE ADD CONSTRAINT APP_USER_PROF_APP_USER_FK FOREIGN KEY ( APP_USER_ID ) REFERENCES APP_USER ( ID ) ON
DELETE CASCADE ;

-- SEQUENCES

CREATE SEQUENCE APP_USER_ID_SEQ START WITH 3 INCREMENT BY 2 MINVALUE 3 NOCACHE ORDER ;

CREATE SEQUENCE REC_USER_PASS_REQ_ID_SEQ START WITH 1 INCREMENT BY 2 MINVALUE 1 NOCACHE ORDER ;

/