-- **********************************************************************
-- ************************* CREATE APP USER ****************************
-- **********************************************************************

CREATE USER "WEBSCAN"  PROFILE "DEFAULT" 
    IDENTIFIED BY "12345" DEFAULT TABLESPACE "WEBSCAN_TS" 
        ACCOUNT UNLOCK;

-- ROLES
GRANT CREATE SYNONYM TO "WEBSCAN";
GRANT "CONNECT" TO "WEBSCAN";
		
-- QUOTAS
ALTER USER WEBSCAN QUOTA UNLIMITED ON WEBSCAN_INDEX_TS;
ALTER USER WEBSCAN QUOTA UNLIMITED ON WEBSCAN_LOB_TS;
ALTER USER WEBSCAN QUOTA UNLIMITED ON WEBSCAN_TS;


-- DISTRIBUTED TRANSACTIONS
GRANT SELECT ON SYS.DBA_PENDING_TRANSACTIONS TO WEBSCAN;
GRANT SELECT ON SYS.PENDING_TRANS$ TO WEBSCAN;
GRANT SELECT ON SYS.DBA_2PC_PENDING TO WEBSCAN;
GRANT EXECUTE ON SYS.DBMS_SYSTEM TO WEBSCAN;

-- ******************* TABLES PERMISSIONS *******************

-- DIGITALIZATION TABLES
GRANT INSERT ON "WEBSCAN_OWNER"."BATCH" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."BATCH" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."BATCH" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."BATCH" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."DIGIT_PROC_STAGE" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."DIGIT_PROC_STAGE" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."DIGIT_PROC_STAGE" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."DIGIT_PROC_STAGE" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."DOC_METADATA" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."DOC_METADATA" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."DOC_METADATA" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."DOC_METADATA" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."DOC_SPEC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."DOC_SPEC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."DOC_SPEC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."DOC_SPEC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."DPROC_STAGE_PLSPEC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."DPROC_STAGE_PLSPEC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."DPROC_STAGE_PLSPEC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."DPROC_STAGE_PLSPEC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."INHERITED_DOC_TYPE_SPEC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."INHERITED_DOC_TYPE_SPEC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."INHERITED_DOC_TYPE_SPEC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."INHERITED_DOC_TYPE_SPEC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."METADATA_SPEC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."METADATA_SPEC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."METADATA_SPEC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."METADATA_SPEC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."ORG_UNIT" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."ORG_UNIT" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."ORG_UNIT" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."ORG_UNIT" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."PARAM" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."PARAM" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."PARAM" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."PARAM" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."PARAM_DATA_TYPE_SPEC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."PARAM_DATA_TYPE_SPEC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."PARAM_DATA_TYPE_SPEC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."PARAM_DATA_TYPE_SPEC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."PARAM_SPEC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."PARAM_SPEC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."PARAM_SPEC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."PARAM_SPEC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."PLUGIN" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."PLUGIN" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."PLUGIN" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."PLUGIN" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."PLUGIN_SPEC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."PLUGIN_SPEC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."PLUGIN_SPEC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."PLUGIN_SPEC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."SCANNED_DOC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."SCANNED_DOC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."SCANNED_DOC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."SCANNED_DOC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."SCANNED_DOC_LOG" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."SCANNED_DOC_LOG" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."SCANNED_DOC_LOG" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."SCANNED_DOC_LOG" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."SCAN_PROFILE" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."SCAN_PROFILE" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."SCAN_PROFILE" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."SCAN_PROFILE" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."SCAN_PROFILE_ORG" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."SCAN_PROFILE_ORG" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."SCAN_PROFILE_ORG" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."SCAN_PROFILE_ORG" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."SCAN_PROFILE_SPEC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."SCAN_PROFILE_SPEC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."SCAN_PROFILE_SPEC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."SCAN_PROFILE_SPEC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."SPROF_ORG_DOC_SPEC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."SPROF_ORG_DOC_SPEC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."SPROF_ORG_DOC_SPEC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."SPROF_ORG_DOC_SPEC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."SPROF_ORG_PLUGIN" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."SPROF_ORG_PLUGIN" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."SPROF_ORG_PLUGIN" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."SPROF_ORG_PLUGIN" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."SPROF_SPEC_DPROC_STAGE" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."SPROF_SPEC_DPROC_STAGE" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."SPROF_SPEC_DPROC_STAGE" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."SPROF_SPEC_DPROC_STAGE" TO "WEBSCAN";

-- AUDIT TABLES

GRANT INSERT ON "WEBSCAN_OWNER"."AUDIT_DOC" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."AUDIT_DOC" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."AUDIT_DOC" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."AUDIT_DOC" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."AUDIT_EVENT" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."AUDIT_EVENT" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."AUDIT_EVENT" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."AUDIT_EVENT" TO "WEBSCAN";

GRANT INSERT ON "WEBSCAN_OWNER"."AUDIT_OPERATION" TO "WEBSCAN";
GRANT SELECT ON "WEBSCAN_OWNER"."AUDIT_OPERATION" TO "WEBSCAN";
GRANT UPDATE ON "WEBSCAN_OWNER"."AUDIT_OPERATION" TO "WEBSCAN";
GRANT DELETE ON "WEBSCAN_OWNER"."AUDIT_OPERATION" TO "WEBSCAN";

-- ******************* SEQUENCES PERMISSIONS *******************

-- DIGITALIZATION SEQUENCES
GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."BATCH_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."DOC_METADATA_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."DOC_SPEC_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."METADATA_SPEC_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."ORG_UNIT_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."PARAM_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."PARAM_SPEC_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."PLUGIN_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."SCANNED_DOC_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."SCANNED_DOC_LOG_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."SCAN_PROFILE_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."SCAN_PROFILE_ORG_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."SPROF_ORG_DSPEC_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."SPROF_ORG_PLUGIN_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."SPROF_SPEC_DPSTG_ID_SEQ" TO "WEBSCAN";

-- AUDIT SEQUENCES

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."AUDIT_DOC_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."AUDIT_EVENT_ID_SEQ" TO "WEBSCAN";

GRANT SELECT,ALTER ON "WEBSCAN_OWNER"."AUDIT_OPERATION_ID_SEQ" TO "WEBSCAN";

/