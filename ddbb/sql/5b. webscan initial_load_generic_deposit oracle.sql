-- Plugin de depósito genérico
INSERT INTO PLUGIN_SPEC(ID,NAME,DESCRIPTION,ACTIVE) VALUES (6,'deposit','Plugin de depósito genérico.','1');
INSERT INTO DPROC_STAGE_PLSPEC(DPROC_STAGE_ID,PLUGIN_SPEC_ID) VALUES (4,6);

-- Establecimiento comunicación
INSERT INTO PARAM_SPEC(ID,NAME,DESCRIPTION,MANDATORY,DATA_TYPE_ID,PLUGIN_SPEC_ID) VALUES (PARAM_SPEC_ID_SEQ.NEXTVAL,'protocol.name','Parámetro que especifica el protocolo a usar para el depósito (LOCAL,FTP,SFTP y CIFS).','1',6,6);
INSERT INTO PARAM_SPEC(ID,NAME,DESCRIPTION,MANDATORY,DATA_TYPE_ID,PLUGIN_SPEC_ID) VALUES (PARAM_SPEC_ID_SEQ.NEXTVAL,'path.root','Parámetro que especifica el path base donde se deposita el documento.','1',6,6);
INSERT INTO PARAM_SPEC(ID,NAME,DESCRIPTION,MANDATORY,DATA_TYPE_ID,PLUGIN_SPEC_ID) VALUES (PARAM_SPEC_ID_SEQ.NEXTVAL,'path.relative','Parámetro que especifica la ruta relativa de depósito.','0',6,6);
INSERT INTO PARAM_SPEC(ID,NAME,DESCRIPTION,MANDATORY,DATA_TYPE_ID,PLUGIN_SPEC_ID) VALUES (PARAM_SPEC_ID_SEQ.NEXTVAL,'conn.host','Parámetro que especifica el host de conexión.','0',6,6);
INSERT INTO PARAM_SPEC(ID,NAME,DESCRIPTION,MANDATORY,DATA_TYPE_ID,PLUGIN_SPEC_ID) VALUES (PARAM_SPEC_ID_SEQ.NEXTVAL,'conn.port','Parámetro que especifica el puerto por donde se realiza la conexión.','0',6,6);
INSERT INTO PARAM_SPEC(ID,NAME,DESCRIPTION,MANDATORY,DATA_TYPE_ID,PLUGIN_SPEC_ID) VALUES (PARAM_SPEC_ID_SEQ.NEXTVAL,'conn.auth.domain','Parámetro que especifica el dominio de conexión.','0',6,6);
INSERT INTO PARAM_SPEC(ID,NAME,DESCRIPTION,MANDATORY,DATA_TYPE_ID,PLUGIN_SPEC_ID) VALUES (PARAM_SPEC_ID_SEQ.NEXTVAL,'conn.auth.user','Parámetro que especifica el usuario de conexión.','0',6,6);
INSERT INTO PARAM_SPEC(ID,NAME,DESCRIPTION,MANDATORY,DATA_TYPE_ID,PLUGIN_SPEC_ID) VALUES (PARAM_SPEC_ID_SEQ.NEXTVAL,'conn.auth.password','Parámetro que especifica el password del usuario de conexión.','0',6,6);

COMMIT;